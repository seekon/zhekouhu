(defproject backend "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.8"]
                 [org.clojure/data.json "0.2.4"]
								 [org.clojure/java.jdbc "0.3.5"]
								 [korma "0.4.0"]
								 [mysql/mysql-connector-java "5.1.25"]
                 [com.notnoop.apns/apns "1.0.0.Beta4"]
                 [pandect "0.3.0"]
                 [clojurewerkz/machine_head "1.0.0-beta8"]
                 [bk/ring-gzip "0.1.1"]
                 [enlive "1.1.5"]
                 [org.clojure/tools.logging "0.2.6"]
                 [log4j/log4j "1.2.17"]
                 [clj-http-lite "0.2.0"]
                 ]
  ;:repositories {"eclipse-paho" {:url "https://repo.eclipse.org/content/repositories/paho-releases/"
  ;                               :snapshots false
  ;                               :releases {:checksum :fail}}}
  :plugins [[lein-ring "0.8.11"]]
  :ring {:handler backend.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}}

  :test-selectors
  {:ntf :test-ntf})

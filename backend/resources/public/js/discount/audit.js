var discount;
var selectedCity;
$(function(){
  //获取活动数据
  $.ajax({
    url: 'getUncheckDiscountById/' + $.getUrlVar('uuid'),
    dataType: 'json',
    success: function(data){
      discount = data;

      if(discount.status != "2"){
        $('#btnValid').linkbutton('disable');
        $('#btnInvalid').linkbutton('disable');
      }else{
        $('#btnValid').bind('click', function(){
          $.messager.confirm('提醒', '是否审核通过?', function(r){
            if (r){
              $('#ff').form('submit',{
                url: 'validDiscount',
				        onSubmit:function(params){
                  var parVal = $('#parValue').numberbox('getValue');
                  if(parVal > 0){
                    params.type = "1";
                  }else{
                    params.type = "0";
                  }

                  var selections = $('#grid_storefronts').datagrid('getData');
                  if(selections.total == 0){
                    $.messager.alert('提示','至少选个一家适用门店.','info');
                    return false;
                  }
                  var items = $.map(selections.rows, function(item){
                        return {
                            uuid: item.uuid,
                            store_id:item.store_id
                        };
                    });
                  params.storefronts = $.toJSON(items);
					        return $(this).form('enableValidation').form('validate');
				        },
                success: function(data){
                  var data = eval('(' + data + ')');
                  if(data.uuid){
                    $.messager.alert('成功','审核活动信息成功.','info');
                    disableButtons();
                  }else{
                    $.messager.alert('失败','审核活动信息失败.','error');
                  }
                }
			        });
            }
          });
		  });

      }
      //discount
      var d = new Date();
      d.setTime(discount.start_date);
      $('#startDate').datebox('setValue', d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());
      d.setTime(discount.end_date);
      $('#endDate').datebox('setValue', d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());
      $('#content').textbox('setValue', discount.content);
      $('#parValue').numberbox('setValue', discount.par_value);
      $('#uuid').attr('value', discount.uuid);
      $('#categoryId').combobox('setValue', discount.category_id);

      if(discount.location){
        try{
          var locJson = eval('(' + discount.location + ')');
          var locValue = "城市:" + (locJson.city_name? locJson.city_name : "")
              + "\n商家:" + locJson.name + "\n地址:" + locJson.addr + "\n电话:" + locJson.phone;
          $('#loc').textbox('setValue', locValue);
        }catch(e){
        }
      }

      var images = [];
      for(var i = 0; i < discount.images.length; i++){
        var img = discount.images[i];
        if(img.is_deleted != '1'){
          images.push(img);
        }
      }
      //images
      $('#grid_images').datagrid('loadData', images);
    },
    error: function(){
      error.apply(this, arguments);
    }
  });

    $('#sf_city').combobox({
	  onSelect : function(record){
      selectedCity = record.name;
	  }
  });

  $('#btnSearch').bind('click', function(){
    if(!selectedCity){
      $.messager.alert('提示','请先选择城市.','info');
      return;
    }
    var keyword = $('#keyword').textbox('getValue');
    if(!keyword){
      $.messager.alert('提示','请输入店铺、门店的搜索关键字.','info');
      return;
    }

    $.ajax({
      url: 'getStorefrontsByKeyword/' + selectedCity + "/" + keyword,
      dataType: 'json',
      success: function(data){
        $('#grid_sel_storefronts').datagrid('loadData',data);
      },
      error: function(){
        error.apply(this, arguments);
      }
    });
  });

});

function disableButtons(){
  $('#btnValid').linkbutton('disable');
  $('#btnValid').unbind();
  $('#btnInvalid').linkbutton('disable');
  $('#btnInvalid').unbind();
}
  var storeToolbar = [{
                text:'从位置信息生成',
                iconCls:'icon-add',
                handler:function(){
                  var store = new Object();
                  if(discount.location){
                    try{
                      var locJson = eval('(' + discount.location + ')');
                      var sfData = $('#grid_storefronts').datagrid('getData');
                      if(sfData.total > 0){
                        for(var rowIndex in sfData.rows){
                          var row = sfData.rows[rowIndex];
                          if(row.uuid == locJson.uuid){
                            $.messager.alert('提示','已从位置信息中添加门店.','info');
                            return;
                          }
                        }
                        sfData.rows.push(locJson);
                      }else{
                        sfData.rows = [];
                        sfData.rows.push(locJson);
                      }

                      $('#grid_storefronts').datagrid('loadData', sfData.rows);
                    }catch(e){
                      $.messager.alert('失败','从位置信息中提取数据失败.','error');
                    }
                  }else{
                    $.messager.alert('提示','位置信息数据为空.','info');
                  }


                }
            },{
                text:'从已有门店中选择',
                iconCls:'icon-add',
                handler:function(){
                    $('#win_sf').window('open');
                }
            },{
                text:'删除',
                iconCls:'icon-remove',
                handler:function(){
                    var selections = $('#grid_storefronts').datagrid('getChecked');
                    if(selections.length == 0){
                      $.messager.alert('提示','请先选择门店.','info');
                      return;
                    }
                    for(var i = 0; i < selections.length; i++){
                        var rowData = selections[i];
                        var rowIndex = $('#grid_storefronts').datagrid('getRowIndex', rowData);
                        $('#grid_storefronts').datagrid('deleteRow', rowIndex);
                    }
                }
            }
            ];

        function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
        }

        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }

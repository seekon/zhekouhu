$(function(){

  $('#grid_discounts_uncheck').datagrid({
	  onDblClickRow: function(rowIndex, rowData){
		  location.href = 'auditDiscount?uuid=' + rowData.uuid;
	  }
  });

});

function auditDateFormatter(value,row,index){
  if(row.last_modify_time){
    return milliTimesToYYMMDD(row.last_modify_time);;
  }else{
    return "";
  }
}

function termFormatter(value,row,index){
  return milliTimesToYYMMDD(row.start_date) + " 到 " + milliTimesToYYMMDD(row.end_date);
}

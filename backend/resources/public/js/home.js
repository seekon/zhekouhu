$(function(){
  $('#btnLogout').on('click', function(){
			$.ajax({
        url: '/logout',
        type: 'post',
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.success){
            $.messager.alert('成功','退出成功.\n页面将返回.','info');
            window.location.href = "login";
          }else{
            $.messager.alert('失败','退出系统失败.','error');
          }
        },
        error: function(){
          $.messager.alert('失败','退出系统失败.','error');
        }
      });
		});
});

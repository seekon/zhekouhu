var selectedCity = '';
jQuery(function($){
  //监听发布版本更新通知
  $('#btnSave-v').on('click', function(){
			$('#form-v').form('submit',{
				onSubmit:function(params){
          params.type = "2";
          params.data_id = "version_" + Math.uuid();
					return $(this).form('enableValidation').form('validate');
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','发布通知成功.','info');
            $('#content-v').textbox('setText', '');
            $('#w-version').window('close')
          }else{
            $.messager.alert('失败','发布通知失败.','error');
          }
        }
			});
		});

  $('#btnClear-v').on('click', function(){
			$('#form-v').form('clear');
		});

  //监听发布推荐活动通知
  $('#btnSave-d').on('click', function(){
			$('#form-d').form('submit',{
				onSubmit:function(params){
          params.type = "3";
          var discounts = $('#grid_discounts').datagrid('getChecked');
          if(!discounts || discounts.length == 0){
            $.messager.alert('提示','请至少选择一条活动记录.','info');
            return false;
          }
          params.data_id = discounts[0].uuid;
					return $(this).form('enableValidation').form('validate');
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','发布通知成功.','info');
            $('#grid_discounts').datagrid('clearChecked');
            $('#content-d').textbox('setText', '');
            $('#w-discount').window('close')
          }else{
            $.messager.alert('失败','发布通知失败.','error');
          }
        }
			});
		});

  $('#btnClear-d').on('click', function(){
			$('#form-d').form('clear');
      $('#grid_discounts').datagrid('clearChecked');
		});

  $('#sform_city').combobox({
	  onSelect : function(record){
		  //重新加载活动数据
      selectedCity = record.name;
      loadTopDiscounts(record.name, $("#showCount").val());
	  }
  });
  $('#grid_discounts').datagrid({
    onCheck:function(rowIndex,rowData){
      $('#content-d').textbox('setText', rowData.content);
    }
  });

});

function changeShowCount(num){
  if(selectedCity){
    loadTopDiscounts(selectedCity, num);
  }else{
    $.messager.alert('提示','请先选择城市.','info');
  }
}

function loadTopDiscounts(city, num){
  $.ajax({
    url: '/getVisitTopDiscounts/' + city + "/" + num,
    dataType: 'json',
    success: function(data){
      //var data = eval('(' + data + ')');
      var items = $.map(data, function(item){
                        var d = new Date();
                        d.setTime(item.start_date);
                        var term = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                        d.setTime(item.end_date);
                        term += " 到 " + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                        return {
                            uuid: item.uuid,
                            publish_date: item.publish_date,
                            content:item.content,
                            category_name:item.category_name,
                            visit_count:item.visit_count,
                            img:item.img,
                            term: term
                        };
                    });
      $('#grid_discounts').datagrid('loadData',items);
    },
    error: function(){
      error.apply(this, arguments);
    }
  });
}

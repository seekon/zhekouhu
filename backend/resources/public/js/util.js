//var web_server_url = "http://127.0.0.1:3000";
var web_server_url = "http://www.zhekouhu.com";

$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  },
  validateEmail: function(str){
    var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    return reg.test(str);
  },
  validatePhone:function(str){
    var reg = /^0?1[3|4|5|8][0-9]\d{8}$/;
    return reg.test(str);
  }
});

function locationFormatter(value,row,index){
  try{
    if(row.location){
      var locJson = eval('(' + row.location + ')');
      return "城市:" + (locJson.city_name? locJson.city_name : "")
             + "<br>商家:" + locJson.name + "<br>地址:" + locJson.addr + "<br>电话:" + locJson.phone;
    }
    }catch(e){
  }
  return "";
}

function imgFormatter(value,row,index){
  if(row.img){
    return '<img src=' + web_server_url + '/getImageFile/'+ row.img +' / height="80px">';
  }else{
    return "";
  }
}

function milliTimesToYYMMDD (timeVal){
  var d = new Date();
  d.setTime(timeVal);
  return d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
}

var cityloader = function(param,success,error){
            $.ajax({
                url: 'getCities',
                dataType: 'json',
                success: function(data){
                    var items = $.map(data.data, function(item){
                        return item;
                    });
                    success(items);
                },
                error: function(){
                    error.apply(this, arguments);
                }
            });
        }

var categriesLoader = function(param,success,error){
            $.ajax({
                url: 'getCategories',
                dataType: 'json',
                success: function(data){
                    var items = $.map(data, function(item){
                        return item;
                    });
                    success(items);
                },
                error: function(){
                    error.apply(this, arguments);
                }
            });
        }

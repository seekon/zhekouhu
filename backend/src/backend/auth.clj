(ns backend.auth
  (:use [backend.db]
        [korma.core])
  (:require [ring.util.response :as resp]
            [pandect.core :as pandect]
            [clojure.data.json :as json]
            [net.cgrand.enlive-html :as html]
  ))

(defn login-path [context]
  ;(println context)
  (if context
    (str context "/login")
    "login"
    )
  )
(defn- home-path [context]
  (if context
    (str context "/home")
    "home"
    )
  )

(defn- logged-in? [request]
  (get-in request [:session :user] false))

(defn authenticated? [handler]
  (fn [request]
    ;(println request)
    (let [uri (:uri request)]
      (if (or (logged-in? request)
              (.endsWith uri ".png")
              (.endsWith uri ".jpg")
              (.endsWith uri ".css")
              (.endsWith uri ".js")
              (.endsWith uri ".ico"))
        (handler request)
        (resp/redirect (login-path (:context request)))
        )
      )
    )
  )

(defn- encrypt [password] (pandect/md5 password))

(defn- password-is-valid? [password-login password-in-db]
  (or (= password-login password-in-db) (= (pandect/md5 password-login) password-in-db))
  )

(defn- get-user [code]
  (if-let [user (first (select users (where {:code code})))] user {} ))

(defn login [request]
  (let [{{code :user, password :pass} :params} request
        user (get-user code)
        no-error (and (:uuid user) (password-is-valid? password (:pwd user)))]

     (if no-error
       (assoc-in (resp/redirect (home-path (:context request))) [:session :user] user)
       (assoc (resp/redirect (login-path (:context request)))
          :flash {:error-type (if (:uuid user) :pass-error :user-error) :user-id code :pwd password})
       )
  )
)

(html/deftemplate index "public/index.html"
                  [{:keys [error-type user-id pwd]}]
                  [:#userid] (if user-id (html/set-attr :value user-id) identity)
                  [:#passid] (if pwd (html/set-attr :value pwd) identity)
                  [:#user-error] (if (= error-type :user-error) (html/remove-class "visi") identity)
                  [:#pass-error] (if (= error-type :pass-error) (html/remove-class "visi") identity)
                  )
(defn login-page [req]
  (resp/content-type (resp/response (index (:flash req))) "text/html;charset=utf-8")
  )

(ns backend.award
  (:use [backend.db]
        [korma.db]
        [korma.core])
  (:require [backend.date :as date])
  )

(def discount-award-comment "发优惠奖励积分")

(defn discount-award-user [dis-id user-id awarder]
  (binding [*current-conn* (get-connection db-zkh)]
   (let [uuid (str (java.util.UUID/randomUUID))
        current-time (System/currentTimeMillis)
        exp-time (date/get-time-after-years 1)]
    (insert zkh-discount-awards
            (values {:uuid uuid :discount_id dis-id :user_id user-id :awarder awarder :award_time current-time
                     :exp_time exp-time :coin_val 1 :comment discount-award-comment :last_modify_time current-time}))
    (exec-raw [" update z_user_profile set award_sum = award_sum + 1 where user_id = ? " [user-id]])
    {:uuid uuid}
    ))
  )

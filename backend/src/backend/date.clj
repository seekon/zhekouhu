(ns backend.date
  (:import [java.util Calendar])
)

(defn formatDate [time]
  (if time
    (cond
     (instance? java.lang.String time) (.format (java.text.SimpleDateFormat. "yyyy-MM-dd") (java.util.Date. (Long/parseLong time)))
     :else (.format (java.text.SimpleDateFormat. "yyyy-MM-dd") (java.util.Date. time))
    )

	)
)

(defn formatTime [time]
  (.format (java.text.SimpleDateFormat. "yyyy-MM-dd HH:mm:ss") (java.util.Date. time)
	)
 )

(defn parse-date [date-str]
  (.parse (java.text.SimpleDateFormat. "yyyy-MM-dd") date-str)
  )

(defn parse-time [date-str]
  (.getTime (parse-date date-str))
  )

(defn get-time-after-years [years]
  (let [c (Calendar/getInstance)]
   (.setTimeInMillis c (System/currentTimeMillis))
   (.add c Calendar/YEAR 1)
   (.getTimeInMillis c)
   )
  )


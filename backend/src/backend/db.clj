(ns backend.db
  (:use [korma.db]
        [korma.core]))

(defdb db-zkh (mysql {:host "localhost" :port 3306 :db "zhekouhu" :user "liuxy" :password "zhekouhu<123>"}))

(defdb db-admin (mysql {:host "localhost" :port 3306 :db "zkh_manager" :user "zkh_admin" :password "zkh_admin<456>"}))

(def def-page-size 15)

(declare  users devices messages)

;manager数据库表
(defentity users
  (table :sys_user)
  (pk :uuid)
  (database db-admin)
)

(defentity devices
  (table :ntf_device)
  (pk :uuid)
  (database db-admin)
)

(defentity messages
  (table :ntf_message)
  (pk :uuid)
  (database db-admin)
)

;zhekouhu数据库表，主要为审核优惠活动服务
(defentity zkh-categories
  (table :z_category)
  (pk :uuid)
  (database db-zkh)
)

(defentity zkh-cities
  (table :z_city)
  (pk :uuid)
  (database db-zkh)
  )

(defentity zkh-storefronts
  (table :z_storefront)
  (pk :uuid)
  (database db-zkh)
  )

(defentity zkh-stores
  (table :z_store)
  (pk :uuid)
  (has-many zkh-storefronts {:fk :store_id})
  (database db-zkh)
)

(defentity zkh-discount-imgs
  (table :z_discount_img)
  (pk :uuid)
  (database db-zkh)
  )

(defentity zkh-discount-storefronts
  (table :z_discount_storefront)
  (pk :uuid)
  (database db-zkh)
  )

(defentity zkh-discounts
  (table :z_discount)
  (pk :uuid)
  (many-to-many zkh-storefronts :z_discount_storefront {:lfk :discount_id :rfk :storefront_id})
  (database db-zkh)
)

(defentity zkh-discount-awards
  (table :z_discount_award)
  (pk :uuid)
  (database db-zkh)
  )

(defentity zkh-users
  (table :z_user)
  (pk :uuid)
  (database db-zkh)
)

(defentity zkh-user-profiles
  (table :z_user_profile)
  (pk :uuid)
  (database db-zkh)
)

(ns backend.discount
  (:use [korma.core]
        [backend.db]
        [korma.db]
   )
  (:require [backend.store :as store]
            [backend.award :as award]
            [backend.ntf :as ntf]
            [backend.web :as web]
            [clj-http.lite.client :as client]

  ))

(defn- get-total-pub-discounts [status]
  (binding [*current-conn* (get-connection db-zkh)]
    (exec-raw [" select count(1) as total from z_discount where status = ? and origin = '1' " [status]] :results)
    )
  )

(defn- get-pub-discounts-by-status [status limit offset-val]
  ;(select zkh-discounts (where {:status status :origin "1"}) (order :publish_time) (limit def-page-size) (offset offset-val))
  (binding [*current-conn* (get-connection db-zkh)]
   (let [sql (str " select DISTINCT d.*, "
                 " (select img from ( "
                 " select discount_id, img, min(ord_index) from z_discount_img di where di.is_deleted = '0' group by di.discount_id) as a "
                 " where a.discount_id = d.uuid) as img "
                 " ,c.name as category_name "
                 " from z_discount d "
                 " LEFT join z_category c on c.uuid = d.category_id "
                 " where status = ? and origin = '1'"
                 " order by publish_time "
                 " limit " limit " offset " offset-val)
        params [status]]
    (assoc (first (get-total-pub-discounts status)) :rows (exec-raw [sql params] :results))
    ))
  )

;查询待审核的活动列表
(defn get-uncheck-discounts [limit offset-val]
  (get-pub-discounts-by-status "2" limit offset-val)
  )

;查询已审核生效的活动列表
(defn get-valid-discounts [limit offset-val]
  (get-pub-discounts-by-status "1" limit offset-val)
  )

;查询已审核未生效的活动列表
(defn get-invalid-discounts [limit offset-val]
  (get-pub-discounts-by-status "8" limit offset-val)
  )

(defn get-uncheck-discount-by-id [uuid]
  (if-let [discount (first (select zkh-discounts (where {:uuid uuid})))]
    (assoc discount :images (select zkh-discount-imgs (where {:discount_id uuid})))
    {}
   )
  )

(defn- save-dis-storefront [dis-id front]
  (let [params {:discount_id dis-id
                :store_id (:store_id front)
                :storefront_id (:uuid front)}]
   (if-let [dis-front (first (select zkh-discount-storefronts (fields :uuid) (where params)))]
     (update zkh-discount-storefronts (set-fields {:is_deleted "0"}) (where dis-front))
     (insert zkh-discount-storefronts (values (assoc params :uuid (str (java.util.UUID/randomUUID)))))
     )
    )
  )

;保存活动门店
(defn- save-dis-storefronts [dis-id storefronts]
  ;(println storefronts)
  (loop [sfs storefronts]
    (if (> (count sfs) 0)
      (let [front (first sfs)]
        (if (:store_id front)
          (save-dis-storefront dis-id front)
          (let [loc (:location (first (select zkh-discounts (fields :location) (where {:uuid dis-id}))))]
            (store/save-store-by-location loc)
            (save-dis-storefront dis-id (assoc front :store_id (:uuid front)))
            )
          )
        (recur (rest sfs))
        )
      )
    )
  )

;生效活动
(defn valid-discount [uuid content start-date end-date cate-id par-val type storefronts auditor]
  (binding [*current-conn* (get-connection db-zkh)]
  (let [current-time (System/currentTimeMillis)]
    (transaction
      (update zkh-discounts
            (set-fields {:content content :start_date start-date :end_date end-date :category_id cate-id
                         :par_value par-val :type type :last_modify_time current-time :status "1"})
            (where {:uuid uuid}))
     (if storefronts
      (save-dis-storefronts uuid storefronts)
      )
    (if-let [publisher (:publisher (first (select zkh-discounts (fields :publisher) (where {:uuid uuid}))))]
      ;奖励积分
      (award/discount-award-user uuid publisher (:uuid auditor))
      ;发送推送通知

      ))
     ;重建索引和缓存
     ;(println (str web/web-server "/recacheAndIndexDiscount/" uuid))
     (client/get (str web/web-server "/recacheAndIndexDiscount/" uuid) )
     )
    {:uuid uuid}
    )
  )

;失效活动
(defn invalid-discount [uuid audit-idea]
  (let [current-time (System/currentTimeMillis)]
    (update zkh-discounts
            (set-fields {:audit_idea audit-idea :last_modify_time current-time :status "8"})
            (where {:uuid uuid}))
    {:uuid uuid}
    )
  )

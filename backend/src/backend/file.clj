(ns backend.file
  )

(defn get-resource-as-stream [res-name]
  (->
   (Thread/currentThread)
   (.getContextClassLoader)
   (.getResourceAsStream res-name))
  )

(defn load-conf []
  (with-open [r (java.io.PushbackReader. (clojure.java.io/reader (get-resource-as-stream "conf.clj")))]
    (binding [*read-eval* false]
      (read r)))
  )

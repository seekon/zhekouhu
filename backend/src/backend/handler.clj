(ns backend.handler
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [ring.middleware.gzip :as gzip]
            [ring.util.response :as response]
            [compojure.route :as route]
            [clojure.data.json :as json]
            [clj-http.lite.client :as client]
            [backend.ntf :as ntf]
            [backend.auth :as auth]
            [backend.web :as web]
            [backend.file :as file]
            [backend.discount :as discount]
            [backend.date :as date]
            [backend.store :as store]
            [backend.user :as user]
            ))

;统一的json输出
(defn- json-out [json-str]
  (json/write-str json-str)
  )

(defroutes ntf-routes
  (GET "/manageMessage" [] (web/html-manage-msg))
  (GET "/listMessage" [] (web/html-list-msg))
  (POST "/sendMessage" {{content :content type :type data-id :data_id} :params :as request}
        (json-out (ntf/send-msg content type data-id))
        )
  (GET "/getVisitTopDiscounts/:city/:num" [city num]
       (let [data (client/get (str web/web-server "/getVisitTopDiscounts/" city "/" num))]
         ;(println data)
         (json-out (:body data))
         )
       )
  (GET "/getMessages" {{page :page rows :rows} :params :as params}
       (let [page (if page (Integer/parseInt page) 1)
             rows (if rows (Integer/parseInt rows) 10)]
         ;(println "page:" page ",rows:" rows)
        (json-out (ntf/get-messages rows (* rows (- page 1)))))
       )
  )

(defroutes web-routes
  ;退出
  (POST "/logout" request
        (dissoc (request :session) :user)
        {
          :status 200
          :session nil
          :body (json-out {:success true})
          }
        )
  ;主页
  (GET "/home" [] (web/html-home))
  (GET "/getCities" []
       (client/get (str web/web-server "/getCities/-1") ))
  (GET "/getCategories" []
       (client/get (str web/web-server "/getCategories/-1") ))
  )

(defroutes discount-routes
  ;获取待审核活动列表
  (GET "/getUncheckDiscounts" {{page :page rows :rows} :params :as params}
       (json-out (discount/get-uncheck-discounts rows (* (Integer/parseInt rows) (- (Integer/parseInt page) 1)))))
  ;获取已审核生效活动列表
  (GET "/getValidDiscounts" {{page :page rows :rows} :params :as params}
       (json-out (discount/get-valid-discounts rows (* (Integer/parseInt rows) (- (Integer/parseInt page) 1)))))
  ;获取已审核未通过活动列表
  (GET "/getInvalidDiscounts" {{page :page rows :rows} :params :as params}
       (json-out (discount/get-invalid-discounts rows (* (Integer/parseInt rows) (- (Integer/parseInt page) 1)))))

  (GET "/auditDiscountList" [] (web/html-audit-discount-list))
  (GET "/auditDiscount" [] (web/html-audit-discount))
  ;根据id获取待审核活动信息
  (GET "/getUncheckDiscountById/:id" [id]
       (json-out (discount/get-uncheck-discount-by-id id)))
  ;审核通过活动
  (POST "/validDiscount" {{uuid :uuid content :content start-date :startDate end-date :endDate cate-id :categoryId
                           par-val :parValue type :type storefronts :storefronts} :params {auditor :user} :session}
        ;(println params)
        (json-out (discount/valid-discount uuid content (date/parse-time start-date) (date/parse-time end-date)
                                           cate-id par-val type (json/read-json storefronts) auditor))
        )
  ;审核不通过活动
  (POST "/invalidDiscount" {{uuid :uuid audit-idea :auditIdea } :params}
        (json-out (discount/invalid-discount uuid audit-idea))
        )
  )

(defroutes user-routes
  (GET "/managePubUser" [] (web/html-list-pub-user))
  (GET "/getPubUsers" {{page :page rows :rows} :params :as params}
       (let [page (if page (Integer/parseInt page) 1)
             rows (if rows (Integer/parseInt rows) 10)]
         ;(println "page:" page ",rows:" rows)
        (json-out (user/get-pub-users rows (* rows (- page 1)))))
       )
  )

(def app-routes
  (routes web-routes ntf-routes discount-routes user-routes)
  )

(defroutes auth-routes
  (POST "/login" request (auth/login request))
  (auth/authenticated? app-routes)
  (route/resources "/")
  (route/not-found "Not Found"))

(defroutes anonymous-routes
  (GET "/" request ;(println request)
       (response/redirect (auth/login-path (:context request))))
  (GET "/login" request (auth/login-page request))
  ;注册移动设备
  (GET "/registerDevice/:dev-id/:type" [dev-id type];{{dev-id :dev_id type :type } :params :as params}
       (json-out (ntf/register-device dev-id type "1")))
  ;设置移动设备不提醒消息
  (GET "/setNotifyState/:dev-id/:status" [dev-id status]
       (json-out (ntf/update-device-status dev-id status)))
  ;获取门店列表
  (GET "/getStorefrontsByKeyword/:city/:keyword" [city keyword]
       (json-out (store/get-storefront-by-keyword city keyword))
       )
  )

(def app
  (-> (routes anonymous-routes auth-routes )
      (handler/site :session)
      (ring.middleware.cookies/wrap-cookies)
      ;(gzip/wrap-gzip)
      ))

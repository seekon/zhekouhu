(ns backend.ntf
  (:use
		[korma.core]
    [korma.db]
		[backend.db]
    [clojure.tools.logging]
	)
  (:require [backend.file :as file]
            [clojure.data.json :as json]
            [clojurewerkz.machine-head.client :as mh]
            )
  (:import (com.notnoop.apns APNS ApnsDelegate ApnsNotification ApnsService)
           )
)

(declare send-welcome-msg)

;更新设备状态，发送消息时将根据状态决定是否发送给此设备
(defn update-device-status [dev-id status]
  (update devices (set-fields {:status status :last_modify_time (System/currentTimeMillis)}) (where {:device_id dev-id}))
  {:dev_id dev-id}
  )

;注册设备
(defn register-device [dev-id type status]
  (if-let [device (first (select devices (fields :uuid) (where {:device_id dev-id})))]
    (update-device-status dev-id status)
    (let [uuid (str (java.util.UUID/randomUUID))
			    register-time (str (System/currentTimeMillis))]
      (insert devices (values {:uuid uuid :device_id dev-id :type type :status status
                               :register_time register-time :last_modify_time register-time}))
     )
    )
  ;(println "发送欢迎消息.");

  (if (= "1" status)
    (send-welcome-msg dev-id type);发送欢迎消息
    {:dev_id dev-id}
    )
  )

;根据msg-id获取消息
(defn- get-msg-by-id [msg-id]
  (first (select messages (where {:msg_id msg-id})))
  )

;保存发送的消息
(defn- save-msg [msg-id content type topic send-time status]
    (if-let [msg (get-msg-by-id msg-id)]
      {:uuid (:uuid msg)}
      (let [uuid (str (java.util.UUID/randomUUID))]
        (insert messages (values {:uuid uuid
                                 :msg_id msg-id
                                 :content content
                                 :type type
                                 :topic topic
                                 :send_time send-time
                                 :status status}))
        {:uuid uuid}
      ))
  )

;更新消息状态
(defn- update-msg-status [uuid status]
  (update messages (set-fields {:status status}) (where {:uuid uuid}) )
  )

;发送android的消息，使用mqtt服务发送
(defn- send-msg-android [msg]
  (let [mqtt-conf (:mqtt (file/load-conf))
        conn (mh/connect (:host mqtt-conf) "zkh-server" mqtt-conf)
        ]
    ;(println "开始发布android消息")
    (mh/publish conn (msg "topic") (json/write-str msg) 1 false)
    ;(println "发布android消息完成")
    (mh/disconnect-and-close conn)
    )
  )

;获取ios设备id列表
(defn- get-ios-device-ids ^java.util.Collection []
  (loop [dev-ids (select devices (fields :device_id) (where {:status "1" :type "1"})) result []]
    (if (= (count dev-ids) 0)
      result
      (recur (rest dev-ids) (conj result (:device_id (first dev-ids))))
      )
    )
  )

;发送ios消息
(defn- send-msg-ios
  ([msg device-ids]
   (let [content (msg "content")
         customFieldMap (dissoc msg "content")
         payload (-> (APNS/newPayload)
                    (.customFields customFieldMap)
                    (.alertBody content)
                    (.build)
                )
         service (->
                   (APNS/newService)
                   ;(.withCert (file/get-resource-as-stream "npsTest.p12") "123456");测试用的证书
                   ;(.withSandboxDestination)
                   (.withCert (file/get-resource-as-stream "nps_Certificates.p12") "zhekouhu123456")
                   (.withProductionDestination)
                   (.withDelegate (reify ApnsDelegate
                                (messageSent [this message resent]
                                   (info (str "msg send ok." message))
                                   )
                                (messageSendFailed [this message ex]
                                         (info (str "msg send failed." message))
                                         )
                      ))
                   (.build)

     )]
     ;(println content)
     ;(println customFieldMap)
     (if (instance? java.util.Collection device-ids)
       (loop [devices device-ids]
         (when (> (count devices) 0)
           (.push service (first devices) payload)
           (recur (rest devices))
           )
         )
       (.push service device-ids payload)
       )
    )
   )
  ([msg]
   (send-msg-ios msg (get-ios-device-ids))
   )

  )

;构建消息
(defn- build-msg [content type msg-id topic]
  (let [send-time (str (System/currentTimeMillis))
        uuid (:uuid (save-msg msg-id content type topic send-time "0"))
        msg {"msg_id" msg-id "type" type "topic" topic "send_time" send-time "uuid" uuid "content" content}]
    (debug msg)
    ;(println msg)
    msg
    )
  )

;发送消息接口
;type: (1:系统, 2:版本, 3:推荐活动)
(defn send-msg [content type msg-id]
  (try
   (transaction
    (let [c-topic (:topic (:mqtt (file/load-conf)))
         topic (condp = type
                 "1" (str c-topic "/system")
                 "2" (str c-topic "/version")
                 (str c-topic "/discount")
                 )
         msg (build-msg content type msg-id topic)]
    (send-msg-android msg)
    (send-msg-ios msg)
    msg
    ))
    (catch Exception ex
      (error ex)
      (throw ex)
      )
    )
  )

;首次注册设备发送欢迎消息
;dev-type:(0:android, 1:ios)
(defn- send-welcome-msg [dev-id dev-type]
  (try
    (transaction
     (let [msg (build-msg (:msg-welcome (:mqtt (file/load-conf))) "1" "def_welcome_msg" "msg/devices")]
      (if (= (str dev-type) "1")
        (send-msg-ios msg dev-id)
        (send-msg-android (assoc msg "topic" (str "msg/" dev-id "/register")))
      )
      (assoc msg :dev_id dev-id)
      ))
    (catch Exception ex
      (println ex)
      (error ex)
      )
    )
  )

(def sql-get-total-messages (str " select count(1) as total from ntf_message "))
(defn- get-total-messages []
    (exec-raw [sql-get-total-messages []] :results)
  )

(def sql-get-messages (str " select uuid, content, FROM_UNIXTIME(send_time/1000,'%Y-%m-%d %h:%i:%s') as send_time "
                           " from ntf_message "))
(defn get-messages [rows offset-val]
    (assoc (first (get-total-messages))
      :rows (select messages (limit rows) (offset offset-val)))
  )

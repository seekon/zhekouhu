(ns backend.store
  (:use [backend.db]
        [korma.core]
        [korma.db])
  (:require [clojure.data.json :as json])
  )

(defn save-store-by-location [location]
  (let [loc (json/read-json location)
        name (:name loc)
        uuid (:uuid loc)
			  current-time (System/currentTimeMillis)
        ]
     (if (not (first (select zkh-stores (fields :uuid) (where {:uuid uuid}))))
       (insert zkh-stores (values {:uuid uuid :name name :owner "" :logo ""
                                      :register_time current-time :last_modify_time current-time}))
       (update zkh-stores (set-fields {:is_deleted "0"}) (where {:uuid uuid}))
       )
     (if (not (first (select zkh-storefronts (fields :uuid) (where {:uuid uuid}))))
       (insert zkh-storefronts (values (assoc (dissoc loc :name) :name "" :store_id uuid :last_modify_time current-time)))
       (update zkh-storefronts (set-fields {:is_deleted "0"}) (where {:uuid uuid}))
       )
     {:store_id uuid :storefront_id uuid}
    )
  )

(defn get-storefront-by-keyword [city keyword]
  (let [word (str "%" keyword "%")]
   (binding [*current-conn* (get-connection db-zkh)]
    (select zkh-storefronts
           (fields :uuid [:name :sf_name] :addr :phone :city :city_name :store_id :z_store.name)
           (modifier "DISTINCT")
           (join :inner zkh-stores {:z_store.uuid :store_id})
           (where (and (= :city_name city) (or (like :name word) (like :z_store.name word))) )
           )))
  )

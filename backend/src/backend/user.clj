(ns backend.user
  (:use [backend.db]
        [korma.db]
        [korma.core])
  (:require [backend.date :as date])
  )

(def sql-get-total-pub-users (str " select count(1) as total from z_user "))

(defn- get-total-pub-users []
  (binding [*current-conn* (get-connection db-zkh)]
    (exec-raw [sql-get-total-pub-users []] :results)
    )
  )

(def sql-get-pub-users (str " select distinct u.uuid, u.code, s.store_name, u.register_time from z_user u "
                            " left join (select owner, group_concat(name) as store_name from z_store group by owner) s on u.uuid = s.owner "
                            " order by register_time "))

(defn get-pub-users [rows offset-val]
  (binding [*current-conn* (get-connection db-zkh)]
    (let [sql (str sql-get-pub-users " limit " rows " offset " offset-val)
          total (first (get-total-pub-users))]
      (assoc total :rows (exec-raw [sql []] :results))
      )
    )
  )

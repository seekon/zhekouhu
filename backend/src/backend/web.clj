(ns backend.web
  (:require
    [net.cgrand.enlive-html :as html]
    [backend.file :as file]
	)
)

(def web-server (:web-server (file/load-conf)))

(html/deftemplate html-home "public/home.html" [])

(html/deftemplate html-manage-msg "public/ntf/manage.html" [])

(html/deftemplate html-list-msg "public/ntf/list.html" [])

(html/deftemplate html-audit-discount "public/discount/audit.html" [])

(html/deftemplate html-audit-discount-list "public/discount/audit_list.html" [])

(html/deftemplate html-list-pub-user "public/user/list.html" [])

(ns backend.test.handler
  (:require [clojure.test :refer :all]
            [backend.handler :refer :all]
            [ring.mock.request :as mock]
            [clojure.data.json :as json]
            ))

(deftest test-app
  (testing "main route"
    (let [response (app (mock/request :get "/"))]
      (is (or (= (:status response) 302)
              (= (:status response) 200)))))
  )

(deftest test-auth
  (testing "login"
    (let [response (app (mock/request :post "/login" {:user "admin" :pass "1"}))]
      (is (or (= (:status response) 302)
              (= (:status response) 200)))))
  )

(deftest test-ntf
  (testing "register-device"
    (let [response (app (mock/request :get "/registerDevice/1/1"))]
      (is (= (:status response) 200))
      (is (= (:dev_id (json/read-json (:body response))) "1"))))

  (testing "set-unNotify"
    (let [response (app (mock/request :get "/setUnNotify/1"))]
      (is (= (:status response) 200))
      (is (= (:dev_id (json/read-json (:body response))) "1"))))

  (testing "send notification"
    (let [response (app (mock/request :post "/sendMessage" {:content "测试消息" :type "1" :data_id ""}))]
      (is (= (:status response) 200))
      (is (not= (:send_time (json/read-json (:body response))) nil))))
  )


(ns backend.test.ntf
  (:use clojure.test
        backend.ntf)
  )

;(def dev-id "0d8021bf6676694d032068c95b589aff5750be9d3b7f6a844aa111d50a6467e8")
(def ios-dev-id "f7de5191fef52a58304ad5237aff325b418e5936212aa6a240c854f74459ef88");for test ios

(def android-dev-id "andr_a9688d577315b86d");for test android

(deftest ^:test-ntf test-ntf
  ;(testing "test register ios device "
  ;  (let [dev-id (register-device ios-dev-id "1" "1")]
  ;    (is (not= dev-id nil))
  ;    )
  ;  )

  (testing "test register android device"
    (let [dev-id (register-device android-dev-id "0" "1")]
      (is (not= dev-id nil))
      )
    )
  )

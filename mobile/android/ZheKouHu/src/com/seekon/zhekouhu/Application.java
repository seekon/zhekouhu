package com.seekon.zhekouhu;

import android.content.Context;

import com.seekon.zhekouhu.utils.Logger;

public class Application extends android.app.Application {

	private static Context mAppContext;

	@Override
	public void onCreate() {
		super.onCreate();

		mAppContext = getApplicationContext();

		Logger.setAppTag(getString(R.string.app_log_tag));
		Logger.setLevel(Logger.DEBUG);

		// //FileCache.getInstance().clear();
	}

	public static Context getAppContext() {
		return mAppContext;
	}

}

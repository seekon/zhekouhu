package com.seekon.zhekouhu;

public interface Const {

	public static final String SERVER_APP_URL = "http://www.zhekouhu.com";
	//public static final String SERVER_APP_URL = "http://192.168.253.2:3000";
	// public static final String SERVER_APP_URL = "http://127.0.0.1:3000";
	// public static final String SERVER_APP_URL = "http://192.168.1.104:3000";

	// public static final String MQTT_URL = "tcp://192.168.253.2:1883";
	public static final String MQTT_URL = "tcp://www.zhekouhu.com:443";
	public static final String MQTT_USER_NAME = "zkh_mob_nfs";
	public static final String MQTT_PASSWORD = "zkh_pwd_1234";

	// 一次加载的数据条数
	public static final int PAGE_SIZE = 15;

	public static final String KEY_BROAD_LOCATION = "key.location";

	public static final String DATA_BROAD_LOCATION = "location";

	public static final String PACKAGE_NAME = "com.seekon.zhekouhu";

	public static final String KEY_BAIDU_AK = "Tg44W5KKibdXbVDCdjA7xWL1";

}

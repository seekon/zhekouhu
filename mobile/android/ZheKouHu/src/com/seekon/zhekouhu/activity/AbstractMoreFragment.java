package com.seekon.zhekouhu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.more.FeedbackActivity;
import com.seekon.zhekouhu.activity.more.MessageListActivity;
import com.seekon.zhekouhu.activity.more.VersionActivity;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.setting.SettingEntity;
import com.seekon.zhekouhu.func.widget.SwitchView;
import com.seekon.zhekouhu.func.widget.SwitchView.OnChangedListener;
import com.seekon.zhekouhu.utils.ViewUtils;

public abstract class AbstractMoreFragment extends CustomActionBarFragment {

	private SettingEntity messageSetting;

	private SettingEntity versionSetting;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		messageSetting = ProcessorFactory.getSettingProcessor().getSetting(
				FuncConst.KEY_SETTING_MESSAGE_REMIND);
		if (messageSetting == null) {
			messageSetting = new SettingEntity(FuncConst.KEY_SETTING_MESSAGE_REMIND,
					"1");
		}

		versionSetting = ProcessorFactory.getSettingProcessor().getSetting(
				FuncConst.KEY_SETTING_VERSION);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		updateActionBar();
		View view = inflater.inflate(getContentViewResourceId(), container, false);
		initViews(view);
		return view;
	}

	protected void initViews(View view) {

		((TextView) view.findViewById(R.id.t_message_remind))
				.setText(R.string.label_message_remind);
		((TextView) view.findViewById(R.id.t_sys_message))
				.setText(R.string.title_sys_message);
		((TextView) view.findViewById(R.id.t_version_label))
				.setText(R.string.label_version_update);
		((TextView) view.findViewById(R.id.t_feedback))
				.setText(R.string.title_feedback);
		((TextView) view.findViewById(R.id.t_user_help))
				.setText(R.string.title_user_help);

		View message = view.findViewById(R.id.row_message);
		message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), MessageListActivity.class);
				startActivity(intent);
			}
		});

		View version = view.findViewById(R.id.row_version);
		version.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				checkVersion();
			}
		});

		View feedback = view.findViewById(R.id.row_feedback);
		feedback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), FeedbackActivity.class);
				startActivity(intent);
			}
		});

		View help = view.findViewById(R.id.row_help);
		help.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), WebViewActivity.class);
				intent.putExtra(FuncConst.EXTRA_TITLE,
						getActivity().getString(R.string.title_user_help));
				intent.putExtra(FuncConst.EXTRA_URI, "/user_help.html");
				startActivity(intent);
			}
		});

		SwitchView switchItem = (SwitchView) view
				.findViewById(R.id.switch_message_remind);
		switchItem.setChecked(messageSetting.getValue().equals("1"));
		switchItem.setOnChangedListener(new OnChangedListener() {

			@Override
			public void OnChanged(SwitchView wiperSwitch, final boolean checkState) {
				final String devId = ViewUtils.getDeviceId();
				if (devId != null) {
					AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
							getActivity(), new ProcessorTaskCallback<Boolean>() {

								@Override
								public ProcessResult<Boolean> doInBackground() {
									return ProcessorFactory.getSettingProcessor().setNotifyState(
											devId, checkState ? "1" : "0");
								}

								@Override
								public void onSuccess(Boolean result) {
									if (result) {
										messageSetting.setValue(checkState ? "1" : "0");
										ProcessorFactory.getSettingProcessor().saveSetting(
												messageSetting);
									}
								}
							});
					task.execute();
				}
			}
		});

		TextView versionField = (TextView) view.findViewById(R.id.t_version);
		versionField.setText("当前版本 " + versionSetting.getValue());
	}

	private void checkVersion() {
		AsyncProcessorTask<String> task = new AsyncProcessorTask<String>(
				getActivity(), "正在检查版本", new ProcessorTaskCallback<String>() {

					@Override
					public ProcessResult<String> doInBackground() {
						return ProcessorFactory.getVersionProcessor().getNewestVersion();
					}

					@Override
					public void onSuccess(String result) {
						if (result != null && result.trim().length() > 0) {
							String version = versionSetting.getValue();
							if (version == null || !result.equals(version)) {
								// ViewUtils.showToast("存在新版本，请更新.", 100);
								Intent intent = new Intent(getActivity(), VersionActivity.class);
								startActivity(intent);
							} else {
								ViewUtils.showToast("已经是最新版，不需要更新.", 100);
							}
						}
					}
				});
		task.execute();
	}

	protected abstract void updateActionBar();

	protected abstract int getContentViewResourceId();
}

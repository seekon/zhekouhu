package com.seekon.zhekouhu.activity;

import static com.seekon.zhekouhu.func.FuncConst.EXTRA_FLAG_HOME_AS_UP;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.store.StoreEditActivity;
import com.seekon.zhekouhu.activity.user.UserLoginActivity;
import com.seekon.zhekouhu.activity.user.UserRegisterActivity;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.widget.ImageViewClickDelegate;

public abstract class AbstractPersonalMainFragment extends
		CustomActionBarFragment {

	private static final int STORE_REQUEST_CODE = 12;
	protected static final int DISCOUNT_REQUEST_CODE = 13;
	protected static final int DISCOUNT_LIST_REQUEST_CODE = 14;
	public static final int LOGIN_REQUEST_CODE = 15;
	public static final int REGISTER_REQUEST_CODE = 16;

	Boolean homeAsUp = false;

	protected View mainView;

	private ViewPager logoViewPager;
	private ViewGroup logoDotViewGroup;
	protected ArrayList<StoreEntity> storeList;

	private ImageViewClickDelegate actionDelegate;

	private Handler mHandler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			homeAsUp = args.getBoolean(EXTRA_FLAG_HOME_AS_UP, false);
		}

		mHandler = new Handler();
		// getActivity().getActionBar().setDisplayHomeAsUpEnabled(homeAsUp);

		// setHasOptionsMenu(true);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		actionDelegate = new LogoImageViewClickDelegate();

		mainView = inflater.inflate(getContentViewResourceId(), container, false);

		mHandler.post(new Runnable() {

			@Override
			public void run() {
				mainView.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});

		if (RunEnv.getInstance().getUser() != null) {
			updateViews();
		} else {
			AsyncProcessorTask<UserEntity> task = new AsyncProcessorTask<UserEntity>(
					getActivity(), new ProcessorTaskCallback<UserEntity>() {

						@Override
						public ProcessResult<UserEntity> doInBackground() {
							return ProcessorFactory.getUserProcessor().autoLogin();
						}

						@Override
						public void onSuccess(UserEntity result) {
							updateViews();
						}

						@Override
						public void onFailed(Error error) {
							updateViews();
						}

						@Override
						public void onCancelled() {
							updateViews();
						}
					});
			task.execute((Void) null);
		}
		return mainView;
	}

	protected void updateActionBar() {
		View actionBarView = getActionBarView();
		actionBarView.findViewById(R.id.img_logo_title).setVisibility(View.GONE);
		actionBarView.findViewById(R.id.t_title).setVisibility(View.VISIBLE);

		actionBarView.findViewById(R.id.img_action).setVisibility(View.GONE);

		if (homeAsUp) {
			actionBarView.findViewById(R.id.img_home_up).setVisibility(View.VISIBLE);
		}

		TextView actionView = (TextView) actionBarView.findViewById(R.id.t_action);
		actionView.setVisibility(View.GONE);
		if (RunEnv.getInstance().getUser() == null) {
			actionView.setVisibility(View.VISIBLE);
			actionView.setText(R.string.action_register);
			actionView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), UserRegisterActivity.class);
					startActivityForResult(intent, REGISTER_REQUEST_CODE);
				}
			});
		}
	}

	protected void updateViews() {
		updateActionBar();
		if (mainView == null) {
			return;
		}

		// getActivity().invalidateOptionsMenu();

		final boolean isLogined = RunEnv.getInstance().getUser() != null;
		if (isLogined) {
			logoViewPager = (ViewPager) mainView.findViewById(R.id.v_store_img);
			logoDotViewGroup = (ViewGroup) mainView.findViewById(R.id.v_dot_layout);

			updateTopView();

			updateDiscountCountView(true);
		} else {
			updateTopViewLabel();

			updateDiscountCountView(false);

			storeList = null;
			if (logoViewPager != null) {
				logoViewPager.getAdapter().notifyDataSetChanged();
			}

			Button loginButton = (Button) mainView.findViewById(R.id.b_please_login);
			loginButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					openLoginActivity();
				}
			});
		}

		updateFuncView(isLogined);
	}

	protected void updateTopView(){
		if (storeList == null || storeList.isEmpty()) {
			AsyncProcessorTask<List<StoreEntity>> task = new AsyncProcessorTask<List<StoreEntity>>(
					getActivity(), new ProcessorTaskCallback<List<StoreEntity>>() {

						@Override
						public ProcessResult<List<StoreEntity>> doInBackground() {
							UserEntity user = RunEnv.getInstance().getUser();
							return ProcessorFactory.getStoreProcessor().storesForOwner(
									user.getUuid());
						}

						@Override
						public void onSuccess(List<StoreEntity> result) {
							storeList = (ArrayList<StoreEntity>) result;
							setLogoViewPagerAdapter();
							updateTopViewLabel();
						}

						@Override
						public void onFailed(Error error) {
							storeList = (ArrayList<StoreEntity>) ProcessorFactory
									.getStoreProcessor().storesForOwnerFromLocal(
											RunEnv.getInstance().getUser().getUuid());
							setLogoViewPagerAdapter();
							updateTopViewLabel();
							super.onFailed(error);
						}
					});
			task.execute();
		} else {
			updateTopViewLabel();
			setLogoViewPagerAdapter();
		}
	}
	
	protected void updateTopViewLabel() {
		if (mainView == null) {
			return;
		}
		final boolean storeLogined = RunEnv.getInstance().getUser() != null;
		if (storeLogined) {
			mainView.findViewById(R.id.v_store_login).setVisibility(View.GONE);
			if (storeList == null || storeList.isEmpty()) {
				mainView.findViewById(R.id.v_store_logo).setVisibility(View.GONE);
				mainView.findViewById(R.id.v_no_stores).setVisibility(View.VISIBLE);
			} else {
				mainView.findViewById(R.id.v_store_logo).setVisibility(View.VISIBLE);
				mainView.findViewById(R.id.v_no_stores).setVisibility(View.GONE);
			}
		} else {
			mainView.findViewById(R.id.v_no_stores).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_store_logo).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_store_login).setVisibility(View.VISIBLE);
		}

		mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
		mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
	}

	protected void updateFuncView(final boolean isLogined) {
		View newStoreRow = mainView.findViewById(R.id.row_store);

		newStoreRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined) {
					openLoginActivity();
					return;
				}
				openStoreEditActivity();
			}
		});
	}

	protected void openLoginActivity() {
		Intent intent = new Intent(getActivity(), UserLoginActivity.class);
		startActivityForResult(intent, LOGIN_REQUEST_CODE);
	}

	protected void openStoreEditActivity() {
		Intent intent = new Intent(getActivity(), StoreEditActivity.class);
		startActivityForResult(intent, STORE_REQUEST_CODE);
	}

	private void setLogoViewPagerAdapter() {
		// PagerAdapter adapter = logoViewPager.getAdapter();
		// if (adapter != null) {
		// adapter.notifyDataSetChanged();
		// return;
		// }
		logoDotViewGroup.removeAllViews();
		if (storeList != null && storeList.size() > 1) {
			for (int j = 0; j < storeList.size(); j++) {
				LinearLayout.LayoutParams margin = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				margin.setMargins(10, 0, 0, 0);
				ImageView imageView = new ImageView(getActivity());
				if (j == 0) {
					imageView.setBackgroundResource(R.drawable.dot_focus);
				} else {
					imageView.setBackgroundResource(R.drawable.dot);
				}
				logoDotViewGroup.addView(imageView, margin);
			}
		}

		FragmentManager fm = this.getChildFragmentManager();
		logoViewPager.setAdapter(new FragmentPagerAdapter(fm) {

			@Override
			public int getCount() {
				return storeList == null ? 0 : storeList.size();
			}

			@Override
			public Fragment getItem(int pos) {
				StoreEntity store = storeList.get(pos);
				ImagePreviewFragment fragment = ImagePreviewFragment.newInstance(
						store.getLogo(), ScaleType.CENTER_CROP);
				fragment.setActionDelegate(actionDelegate);
				return fragment;
			}
		});

		logoViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				int count = logoDotViewGroup.getChildCount();
				for (int i = 0; i < count; i++) {
					ImageView imageView = (ImageView) logoDotViewGroup.getChildAt(i);
					if (i == pos) {
						imageView.setBackgroundResource(R.drawable.dot_focus);
					} else {
						imageView.setBackgroundResource(R.drawable.dot);
					}
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.store_main, menu);
	// }
	//
	// @Override
	// public void onPrepareOptionsMenu(Menu menu) {
	// super.onPrepareOptionsMenu(menu);
	//
	// if (RunEnv.getInstance().getUser() == null) {
	// menu.findItem(R.id.action_setting).setVisible(false);
	// } else {
	// menu.findItem(R.id.action_register).setVisible(false);
	// }
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case android.R.id.home:
	// boolean storeLogined = RunEnv.getInstance().getUser() != null;
	// if (storeLogined) {
	// getActivity().setResult(Activity.RESULT_OK);
	// }
	// getActivity().finish();
	// break;
	// case R.id.action_setting: {
	// Intent intent = new Intent(getActivity(), StoreSettingActivity.class);
	// this.startActivityForResult(intent, SETTING_REQUEST_CODE);
	// break;
	// }
	// case R.id.action_register: {
	// Intent intent = new Intent(getActivity(), UserRegisterActivity.class);
	// this.startActivityForResult(intent, REGISTER_REQUEST_CODE);
	// break;
	// }
	// default:
	// break;
	// }
	//
	// return super.onOptionsItemSelected(item);
	// }

	@Override
	protected void back() {
		boolean storeLogined = RunEnv.getInstance().getUser() != null;
		if (storeLogined) {
			getActivity().setResult(Activity.RESULT_OK);
		}
		super.back();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		switch (requestCode) {
		case REGISTER_REQUEST_CODE:
		case LOGIN_REQUEST_CODE:
			mainView.findViewById(R.id.v_loading).setVisibility(View.VISIBLE);
			mainView.findViewById(R.id.v_data).setVisibility(View.GONE);
			this.updateViews();
			break;

		case STORE_REQUEST_CODE:
			if (data != null) {
				StoreEntity newStore = (StoreEntity) data
						.getSerializableExtra(FuncConst.NAME_STORE);
				if (newStore.getUuid() == null) {
					return;
				}
				boolean isDeleted = data.getBooleanExtra(FuncConst.NAME_IS_DELETED,
						false);
				if (isDeleted) {// 是否删除
					if (storeList.contains(newStore)) {
						storeList.remove(newStore);
					}
				} else {// 保存操作
					if (storeList != null && storeList.size() > 0) {
						if (storeList.contains(newStore)) {
							StoreEntity store = storeList.get(logoViewPager.getCurrentItem());
							store.setName(newStore.getName());
							store.setLogo(newStore.getLogo());
							store.setStorefronts(newStore.getStorefronts());
						} else {
							storeList.add(newStore);
						}
					} else {
						storeList = new ArrayList<StoreEntity>();
						storeList.add(newStore);
					}
				}
			}

			this.updateTopView();
			break;

		case DISCOUNT_REQUEST_CODE:
			this.updateDiscountCountView(true);
			break;

		case DISCOUNT_LIST_REQUEST_CODE:
			this.updateDiscountCountView(true);
			break;

		default:
			break;
		}
	}

	protected abstract int getContentViewResourceId();

	protected abstract void updateDiscountCountView(boolean logined);

	class LogoImageViewClickDelegate implements ImageViewClickDelegate {

		private static final long serialVersionUID = -552966306558755992L;

		@Override
		public void doClick(FileEntity imageFile) {
			StoreEntity store = storeList.get(logoViewPager.getCurrentItem());
			Intent intent = new Intent(getActivity(), StoreEditActivity.class);
			intent.putExtra(FuncConst.NAME_STORE, store);
			AbstractPersonalMainFragment.this.startActivityForResult(intent,
					STORE_REQUEST_CODE);
		}

	}
}

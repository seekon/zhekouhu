package com.seekon.zhekouhu.activity;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Set;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.FuncConst;

public class CustomActionBarFragment extends Fragment {

	private View actionBarView;
	private boolean saveFieldState;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.createCustomActionBar();
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			saveFieldState = args.getBoolean(FuncConst.KEY_SAVE_FIELD_STATE, false);
		}

		setRetainInstance(true);
		// setHasOptionsMenu(true);
		// if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
		// getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		// }

		if (saveFieldState && savedInstanceState != null) {
			Set<String> keys = savedInstanceState.keySet();
			for (String key : keys) {
				if (key.startsWith(FuncConst.KEY_STATE_FIELD_PREFIX)) {
					String name = key
							.substring(FuncConst.KEY_STATE_FIELD_PREFIX.length());
					try {
						Field field = this.getClass().getDeclaredField(name);
						field.setAccessible(true);
						field.set(this, savedInstanceState.get(key));
					} catch (Throwable e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		if (!saveFieldState) {
			return;
		}

		Field[] fields = this.getClass().getDeclaredFields();
		if (fields != null) {
			for (Field field : fields) {
				if ((field.getModifiers() & java.lang.reflect.Modifier.STATIC) == java.lang.reflect.Modifier.STATIC) {
					continue;// 不保存static类型的成员
				}

				Class<?> type = field.getType();
				if (Serializable.class.isAssignableFrom(type) || type.isPrimitive()) {
					try {
						field.setAccessible(true);
						Object value = field.get(this);
						if (value != null) {
							String name = FuncConst.KEY_STATE_FIELD_PREFIX + field.getName();
							if (type.isPrimitive()) {
								if (type.equals(int.class)) {
									outState.putInt(name, (Integer) value);
								} else if (type.equals(long.class)) {
									outState.putLong(name, (Long) value);
								} else if (type.equals(float.class)) {
									outState.putFloat(name, (Float) value);
								} else if (type.equals(double.class)) {
									outState.putDouble(name, (Double) value);
								} else if (type.equals(boolean.class)) {
									outState.putBoolean(name, (Boolean) value);
								} else if (type.equals(char.class)) {
									outState.putChar(name, (Character) value);
								} else if (type.equals(byte.class)) {
									outState.putByte(name, (Byte) value);
								} else if (type.equals(short.class)) {
									outState.putShort(name, (Short) value);
								}
							} else {
								outState.putSerializable(name, (Serializable) value);
							}
						}
					} catch (Throwable e) {
						e.printStackTrace();
					}
				}
			}
		}

	}

	protected void createCustomActionBar() {
		ActionBar actionBar = getActivity().getActionBar();
		actionBarView = actionBar.getCustomView();
		if (actionBarView != null) {
			return;
		}

		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

		actionBarView = LayoutInflater.from(getActivity()).inflate(
				R.layout.custom_actionbar, null);

		CharSequence title = actionBar.getTitle() == null ? getActivity()
				.getTitle() : actionBar.getTitle();
		TextView titleView = (TextView) actionBarView.findViewById(R.id.t_title);
		titleView.setText(title);

		View homeupView = actionBarView.findViewById(R.id.img_home_up);
		homeupView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				back();
			}
		});

		ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.MATCH_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT);
		actionBar.setCustomView(actionBarView, lp);
	}

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case android.R.id.home:
	// back();
	// return true;
	//
	// default:
	// return super.onOptionsItemSelected(item);
	// }
	// }

	protected View getActionBarView() {
		return actionBarView;
	}

	protected void setHomeAsUpEnabled(boolean enable) {
		if (actionBarView != null) {
			actionBarView.findViewById(R.id.img_home_up).setVisibility(View.GONE);
		}
	}

	protected void setTitle(int resId) {
		this.setTitle(getActivity().getResources().getString(resId));
	}

	protected void setTitle(String title) {
		if (actionBarView != null) {
			TextView titleView = (TextView) actionBarView.findViewById(R.id.t_title);
			titleView.setText(title);
		}
	}

	protected void back() {
		// if (NavUtils.getParentActivityName(getActivity()) != null) {
		// NavUtils.navigateUpFromSameTask(getActivity());
		// }
		getActivity().finish();
	}

}

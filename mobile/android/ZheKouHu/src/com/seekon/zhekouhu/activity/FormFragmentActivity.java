package com.seekon.zhekouhu.activity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.seekon.zhekouhu.utils.ViewUtils;

public abstract class FormFragmentActivity extends SingleFragmentActivity {

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		View decorView = getWindow().getDecorView();
		decorView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ViewUtils.hideInputMethodWindow(FormFragmentActivity.this);
				}
				return true;
			}
		});
	}

	// @Override
	// public boolean dispatchTouchEvent(MotionEvent ev) {
	// boolean result = super.dispatchTouchEvent(ev);
	//
	// if (ev.getAction() == MotionEvent.ACTION_DOWN) {
	//
	// View v = getCurrentFocus();
	//
	// if (isShouldHideInput(v, ev)) {
	// ViewUtils.hideInputMethodWindow(this);
	// }
	// }
	//
	// return result;
	// }
	//
	//
	// /**
	// * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
	// *
	// * @param v
	// * @param event
	// * @return
	// */
	// private boolean isShouldHideInput(View v, MotionEvent event) {
	// if (v != null && (v instanceof EditText)) {
	// int[] l = { 0, 0 };
	// v.getLocationInWindow(l);
	// int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
	// + v.getWidth();
	// if (event.getX() > left && event.getX() < right && event.getY() > top
	// && event.getY() < bottom) {
	// // 点击EditText的事件，忽略它。
	// return false;
	// } else {
	// return true;
	// }
	// }
	// // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
	// return false;
	// }
}

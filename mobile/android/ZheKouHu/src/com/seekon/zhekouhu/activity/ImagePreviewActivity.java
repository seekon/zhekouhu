package com.seekon.zhekouhu.activity;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.FuncConst;

public class ImagePreviewActivity extends FragmentActivity {

	private ViewPager mViewPager;
	private ViewGroup imgDotViewGroup;

	private ArrayList<FileEntity> imageFiles;
	private ArrayList<FileEntity> deletedImages = new ArrayList<FileEntity>();

	private boolean readonly;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.activity_image_preview);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		initViews();
		createCustomActionBar();
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// if (!readonly) {
	// getMenuInflater().inflate(R.menu.image_preview, menu);
	// return true;
	// }
	// return super.onCreateOptionsMenu(menu);
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.action_delete:
	// int index = mViewPager.getCurrentItem();
	// FileEntity file = imageFiles.remove(index);
	// deletedImages.add(file);
	// if (imageFiles.isEmpty()) {
	// setAdapter(0);
	// }else if(index < imageFiles.size() - 1){
	// setAdapter(index);
	// }else{
	// setAdapter(index - 1);
	// }
	// return true;
	// case android.R.id.home:
	// Intent intent = new Intent();
	// intent.putExtra(FuncConst.NAME_IMAGES, imageFiles);
	// intent.putExtra(FuncConst.NAME_DEL_IMAGES, deletedImages);
	// setResult(Activity.RESULT_OK, intent);
	// finish();
	// return true;
	// default:
	// return super.onOptionsItemSelected(item);
	// }
	// }

	private void initViews() {
		mViewPager = (ViewPager) findViewById(R.id.v_viewPager);
		imgDotViewGroup = (ViewGroup) findViewById(R.id.v_dot_layout);

		imageFiles = (ArrayList<FileEntity>) getIntent().getSerializableExtra(
				FuncConst.NAME_IMAGES);
		readonly = getIntent().getBooleanExtra(FuncConst.EXTRA_FLAG_READONLY, true);

		int currentIndex = getIntent()
				.getIntExtra(FuncConst.EXTRA_CURRENT_INDEX, 0);
		setAdapter(currentIndex);
	}

	private void setAdapter(int currentIndex) {
		FragmentManager fm = getSupportFragmentManager();
		PagerAdapter pageAdapter = mViewPager.getAdapter();
		if (pageAdapter != null) {
			pageAdapter.notifyDataSetChanged();
		} else {
			mViewPager.setAdapter(new FragmentPagerAdapter(fm) {

				@Override
				public int getCount() {
					return imageFiles.size();
				}

				@Override
				public Fragment getItem(int pos) {
					return ImagePreviewFragment.newInstance(imageFiles.get(pos),
							ScaleType.FIT_CENTER);
				}
			});
		}
		if (currentIndex > -1 && currentIndex < imageFiles.size()) {
			mViewPager.setCurrentItem(currentIndex);
		}

		imgDotViewGroup.removeAllViews();
		if (imageFiles != null && imageFiles.size() > 0) {
			for (int j = 0; j < imageFiles.size(); j++) {
				LinearLayout.LayoutParams margin = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.WRAP_CONTENT,
						LinearLayout.LayoutParams.WRAP_CONTENT);
				margin.setMargins(10, 0, 0, 0);
				ImageView imageView = new ImageView(this);
				if (j == currentIndex) {
					imageView.setBackgroundResource(R.drawable.dot_focus);
				} else {
					imageView.setBackgroundResource(R.drawable.dot);
				}
				imgDotViewGroup.addView(imageView, margin);
			}
		}

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				int count = imgDotViewGroup.getChildCount();
				for (int i = 0; i < count; i++) {
					ImageView imageView = (ImageView) imgDotViewGroup.getChildAt(i);
					if (i == pos) {
						imageView.setBackgroundResource(R.drawable.dot_focus);
					} else {
						imageView.setBackgroundResource(R.drawable.dot);
					}
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	protected void createCustomActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

		View actionBarView = LayoutInflater.from(this).inflate(
				R.layout.custom_actionbar, null);

		CharSequence title = actionBar.getTitle() == null ? getTitle() : actionBar
				.getTitle();
		TextView titleView = (TextView) actionBarView.findViewById(R.id.t_title);
		titleView.setText(title);

		View homeupView = actionBarView.findViewById(R.id.img_home_up);
		homeupView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				back();
			}
		});

		if (!readonly) {
			ImageView actionImage = (ImageView) actionBarView
					.findViewById(R.id.img_action);
			actionImage.setImageResource(R.drawable.delete);
			actionImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					delete();
				}
			});
		}

		ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
				ActionBar.LayoutParams.MATCH_PARENT,
				ActionBar.LayoutParams.WRAP_CONTENT);
		actionBar.setCustomView(actionBarView, lp);
	}

	private void delete() {
		int index = mViewPager.getCurrentItem();
		FileEntity file = imageFiles.remove(index);
		deletedImages.add(file);
		if (imageFiles.isEmpty()) {
			// setAdapter(0);
			back();
		} else if (index < imageFiles.size() - 1) {
			setAdapter(index);
		} else {
			setAdapter(index - 1);
		}
	}

	private void back() {
		Intent intent = new Intent();
		intent.putExtra(FuncConst.NAME_IMAGES, imageFiles);
		intent.putExtra(FuncConst.NAME_DEL_IMAGES, deletedImages);
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
}

package com.seekon.zhekouhu.activity;

import static com.seekon.zhekouhu.func.FuncConst.EXTRA_SCALE_TYPE;
import static com.seekon.zhekouhu.func.FuncConst.NAME_FILE;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.widget.ImageViewClickDelegate;
import com.seekon.zhekouhu.utils.ViewUtils;

public class ImagePreviewFragment extends Fragment {

	private FileEntity imageFile;
	private ImageViewClickDelegate actionDelegate;

	private ImageView fileView;
	private ScaleType scaleType;

	public static ImagePreviewFragment newInstance(FileEntity imageFile,
			ScaleType scaleType) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_FILE, imageFile);
		args.putSerializable(EXTRA_SCALE_TYPE, scaleType);

		ImagePreviewFragment fragment = new ImagePreviewFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		imageFile = (FileEntity) args.getSerializable(NAME_FILE);
		scaleType = (ScaleType) args.getSerializable(EXTRA_SCALE_TYPE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_image_preview, container,
				false);
		initViews(view);
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();

		String uri = imageFile.getFileUri();
		if (uri != null && uri.trim().length() > 0) {
			fileView.setImageBitmap(FileHelper.decodeFileByDisplaySize(imageFile
					.getFileUri()));
		} else {
			ImageLoader.getInstance().displayImage(imageFile.getAliasName(),
					fileView, false);
		}
	}

	@Override
	public void onStop() {
		super.onStop();

		ViewUtils.cleanImageView(fileView);
	}

	private void initViews(View view) {
		fileView = (ImageView) view.findViewById(R.id.image_file);
		fileView.setScaleType(scaleType);

		fileView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (actionDelegate != null) {
					actionDelegate.doClick(imageFile);
				}
			}
		});

		fileView.setOnCreateContextMenuListener(new OnCreateContextMenuListener() {

			@Override
			public void onCreateContextMenu(ContextMenu menu, View v,
					ContextMenuInfo menuInfo) {
				menu.add(0, 0, 0, "保存到相册");
			}
		});
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			try {
				FileHelper.saveImageToPhotoDir(getActivity(), imageFile);
				ViewUtils.showToast("保存成功.");
			} catch (Exception e) {
				ViewUtils.showToast("保存失败.");
				// e.printStackTrace();
			}
			break;

		default:
			break;
		}
		return super.onContextItemSelected(item);
	}

	public ImageViewClickDelegate getActionDelegate() {
		return actionDelegate;
	}

	public void setActionDelegate(ImageViewClickDelegate actionDelegate) {
		this.actionDelegate = actionDelegate;
	}

}

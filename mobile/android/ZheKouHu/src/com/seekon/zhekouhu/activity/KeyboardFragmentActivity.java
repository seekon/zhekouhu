package com.seekon.zhekouhu.activity;

import android.view.MotionEvent;
import android.view.View;

import com.seekon.zhekouhu.utils.ViewUtils;

public abstract class KeyboardFragmentActivity extends SingleFragmentActivity {

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean result = super.dispatchTouchEvent(ev);

		if (ev.getAction() == MotionEvent.ACTION_DOWN) {

			View v = getCurrentFocus();

			if (ViewUtils.isShouldHideInput(v, ev)) {
				ViewUtils.hideInputMethodWindow(this);
			}
		}

		return result;
	}

}

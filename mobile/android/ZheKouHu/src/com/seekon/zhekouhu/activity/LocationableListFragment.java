package com.seekon.zhekouhu.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.utils.LocationUtils;

public abstract class LocationableListFragment extends
		CustomActionBarListFragment {

	private BroadcastReceiver locationReceiver;
	private LocationEntity loc;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (RunEnv.getInstance().getLocation() != null) {
			loc = RunEnv.getInstance().getLocation();
			doLocationValidate(loc);
		} else {
			LocationUtils.startLocate();
		}

		locationReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				LocationEntity location = RunEnv.getInstance().getLocation();

				if (loc == null || (location != null && !loc.equals(location))) {
					loc = (LocationEntity) intent
							.getSerializableExtra(Const.DATA_BROAD_LOCATION);
					doLocationValidate(loc);
				}
			}
		};

		getActivity().registerReceiver(locationReceiver,
				new IntentFilter(Const.KEY_BROAD_LOCATION));
	}

	@Override
	public void onDestroy() {
		getActivity().unregisterReceiver(locationReceiver);
		super.onDestroy();
	}

	public abstract void doLocationValidate(LocationEntity loc);
}

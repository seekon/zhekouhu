package com.seekon.zhekouhu.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.coupon.CouponMainFragment;
import com.seekon.zhekouhu.activity.discount.DiscountMainFragment;
import com.seekon.zhekouhu.activity.store.StorePubDiscountFragment;
import com.seekon.zhekouhu.activity.user.UserMainFragment;
import com.seekon.zhekouhu.activity.user.UserPubDiscountFragment;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.utils.ViewUtils;

public class MainActivity extends FragmentActivity {

	private static final String TITLE_ACTIVITY = "活动";
	private static final String TITLE_COUPON = "优惠券";
	// private static final String TITLE_STORE = "店铺";
	// private static final String TITLE_MORE = "更多";
	private static final String TITLE_MINE = "我的";
	private static final String TITLE_PUB_DISCOUNT = "发优惠";

	private int backCount = 0;

	// private static Class<?>[] fragmentArray = new Class[] {
	// DiscountMainFragment.class, CouponMainFragment.class,
	// StoreMainFragment.class, MoreMainFragment.class };
	// private static int[] imgArray = new int[] { R.drawable.tab_activity_btn,
	// R.drawable.tab_coupon_btn, R.drawable.tab_store_btn,
	// R.drawable.tab_more_btn };
	// private static String[] titleArray = new String[] { TITLE_ACTIVITY,
	// TITLE_COUPON, TITLE_STORE, TITLE_MORE };
	// private int version = 2;

	private static Class<?>[] fragmentArray = new Class[] {
			DiscountMainFragment.class, CouponMainFragment.class,
			UserMainFragment.class, UserPubDiscountFragment.class,
			StorePubDiscountFragment.class };
	private static int[] imgArray = new int[] { R.drawable.tab_activity_btn,
			R.drawable.tab_coupon_btn, R.drawable.tab_user_btn,
			R.drawable.tab_publish_btn, R.drawable.tab_publish_btn };
	private static String[] titleArray = new String[] { TITLE_ACTIVITY,
			TITLE_COUPON, TITLE_MINE, TITLE_PUB_DISCOUNT, TITLE_PUB_DISCOUNT };

	private FragmentTabHost mTabHost;
	
	private int preSelectedIndex = 0;
	
	// private ViewPager mViewPager;
	// private ArrayList<Fragment> fragmentList = new ArrayList<Fragment>();

	// private boolean storeRegistered;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		// mViewPager = (ViewPager) findViewById(R.id.pager);

		// float scale = this.getResources().getDisplayMetrics().density;
		// int aa = this.getResources().getDisplayMetrics().densityDpi;
		// int hpx = this.getResources().getDisplayMetrics().heightPixels;
		// DisplayMetrics dm = this.getResources().getDisplayMetrics();
		// Paint sPaint = new Paint();
		// float f = sPaint.getTextSize();
		// FontMetrics sF = sPaint.getFontMetrics();

		initView();

	}

	private void initView() {
		// if (version < 2) {
		// storeRegistered = ProcessorFactory.getSettingProcessor()
		// .isStoreRegistered();
		// }

		initTabs();
		// initViewPager();
	}

	private void initTabs() {
		for (int i = 0; i < fragmentArray.length; i++) {
			TabSpec tabSpec = mTabHost.newTabSpec(fragmentArray[i].getName())
					.setIndicator(getTabItemView(imgArray[i], titleArray[i]));
			mTabHost.addTab(tabSpec, fragmentArray[i], null);
		}
		// if (!storeRegistered && version < 2) {// 如果店铺未注册，tab页签不显示
		// mTabHost.getTabWidget().getChildAt(2).setVisibility(View.GONE);
		// }

		changePubDiscountTab();

		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				int position = mTabHost.getCurrentTab();
				if (!tabId.equals(UserPubDiscountFragment.class.getName())) {
					preSelectedIndex = position;
				}
//				 if (position > 1 && !storeRegistered) {
//				 mViewPager.setCurrentItem(position + 1);
//				 }else{
//				 mViewPager.setCurrentItem(position);
//				 }
			}
		});
	}

	public void backSelected(){
		mTabHost.setCurrentTab(preSelectedIndex);
	}
	
	private View getTabItemView(int img, String text) {
		LayoutInflater layoutInflater = LayoutInflater.from(this);
		View view = layoutInflater.inflate(R.layout.activity_main_tab_item, null);

		ImageView imageView = (ImageView) view.findViewById(R.id.image_tab);
		imageView.setImageResource(img);

		TextView textView = (TextView) view.findViewById(R.id.text_tab);
		textView.setText(text);

		return view;
	}

	// private void initViewPager() {
	// fragmentList.add(new DiscountMainFragment());
	// fragmentList.add(new CouponMainFragment());
	// fragmentList.add(new StoreMainFragment());
	// fragmentList.add(new MoreMainFragment());
	//
	// mViewPager
	// .setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
	//
	// @Override
	// public int getCount() {
	// return storeRegistered ? fragmentList.size()
	// : fragmentList.size() - 1;
	// }
	//
	// @Override
	// public Fragment getItem(int index) {
	// if (index > 1 && !storeRegistered) {
	// return fragmentList.get(index + 1);
	// }
	// return fragmentList.get(index);
	// }
	// });
	//
	// mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
	//
	// @Override
	// public void onPageSelected(int index) {
	// TabWidget widget = mTabHost.getTabWidget();
	// int oldFocusability = widget.getDescendantFocusability();
	// widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
	// mTabHost.setCurrentTab(index);
	// widget.setDescendantFocusability(oldFocusability);
	// }
	//
	// @Override
	// public void onPageScrolled(int arg0, float arg1, int arg2) {
	//
	// }
	//
	// @Override
	// public void onPageScrollStateChanged(int arg0) {
	//
	// }
	// });
	//
	// mViewPager.setCurrentItem(0);
	// }

	private void changePubDiscountTab() {
		int i = 3;
		UserEntity user = RunEnv.getInstance().getUser();
		if (user != null && user.getType().equals(FuncConst.VAL_USER_TYPE_STORER)) {
			mTabHost.getTabWidget().getChildAt(i++).setVisibility(View.GONE);
			mTabHost.getTabWidget().getChildAt(i++).setVisibility(View.VISIBLE);
		} else {
			mTabHost.getTabWidget().getChildAt(i++).setVisibility(View.VISIBLE);
			mTabHost.getTabWidget().getChildAt(i++).setVisibility(View.GONE);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode & 0xffff) {
			// case MoreMainFragment.STORE_REQUEST_CODE:
			// mTabHost.getTabWidget().getChildAt(2).setVisibility(View.VISIBLE);
			// break;
			case UserMainFragment.OTHER_REQUEST_CODE:
			case AbstractPersonalMainFragment.LOGIN_REQUEST_CODE:
			case AbstractPersonalMainFragment.REGISTER_REQUEST_CODE:
				changePubDiscountTab();
				break;
			default:
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onDestroy() {
		// if (mLocationClient != null) {
		// mLocationClient = null;
		// }
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		backCount = 0;
		super.onResume();
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && backCount == 0) {
			backCount++;
			ViewUtils.showToast(getString(R.string.msg_app_logout), 100);
			return false;
		}
		backCount = 0;
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean result = super.dispatchTouchEvent(ev);

		if (ev.getAction() == MotionEvent.ACTION_DOWN) {

			View v = getCurrentFocus();

			if (ViewUtils.isShouldHideInput(v, ev)) {
				ViewUtils.hideInputMethodWindow(this);
			}
		}

		return result;
	}
}

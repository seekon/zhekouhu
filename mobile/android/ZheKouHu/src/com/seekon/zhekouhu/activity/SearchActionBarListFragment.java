package com.seekon.zhekouhu.activity;

import java.lang.reflect.Field;
import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;
import com.seekon.zhekouhu.utils.ViewUtils;

public abstract class SearchActionBarListFragment<T> extends
		CustomActionBarListFragment implements IXListViewListener {

	protected AbstractListAdapter<T> adapter;
	protected ArrayList<T> dataList = new ArrayList<T>();
	protected int offset;

	private Handler mHandler;
	private XListView xListView;
	private View mainView;
	protected String searchWord;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setHasOptionsMenu(true);

		mHandler = new Handler();
		setAdapter();
	}

	@Override
	protected int getCunstorActionBarResourceId() {
		return R.layout.custom_actionbar_search;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.base_loading_xlistview, container,
				false);
		initViews(mainView);
		return mainView;
	}

	protected void initViews(View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		showDataView();
	}

	// @Override
	// public void onPrepareOptionsMenu(Menu menu) {
	// super.onPrepareOptionsMenu(menu);
	//
	// MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
	// SearchView mSearchView = (SearchView) searchViewMenuItem.getActionView();
	// setSearchViewStyle(mSearchView);
	// setSearchListeners(mSearchView);
	// }
	//
	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.discount_search, menu);
	// }
	//
	// private void setSearchViewStyle(SearchView mSearchView) {
	// try {
	// mSearchView
	// .setMaxWidth(getActivity().getResources().getDisplayMetrics().widthPixels -
	// 80);
	//
	// int closeButtonId = getResources().getIdentifier(
	// "android:id/search_close_btn", null, null);
	// ImageView closeButtonImage = (ImageView) mSearchView
	// .findViewById(closeButtonId);
	// closeButtonImage.setImageResource(R.drawable.close);
	// closeButtonImage.setBackgroundResource(R.drawable.action_img_selector);
	//
	// int searchPlateId = getResources().getIdentifier(
	// "android:id/search_plate", null, null);
	// mSearchView.findViewById(searchPlateId).setBackgroundResource(
	// R.drawable.textfield_white);
	//
	// int queryTextViewId = getResources().getIdentifier(
	// "android:id/search_src_text", null, null);
	// TextView autoComplete = (TextView) mSearchView
	// .findViewById(queryTextViewId);
	// autoComplete.setTextColor(getResources().getColor(R.color.white));
	// autoComplete.setHintTextColor(getResources().getColor(R.color.white));
	// autoComplete
	// .setTextSize(this.getResources().getDisplayMetrics().density * 10);
	// try {
	// Field mCursorDrawableRes = TextView.class
	// .getDeclaredField("mCursorDrawableRes");
	// mCursorDrawableRes.setAccessible(true);
	// mCursorDrawableRes.set(autoComplete, 0);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// if (searchWord != null) {
	// autoComplete.setText(searchWord);
	// }
	//
	// Class<?> clazz = Class
	// .forName("android.widget.SearchView$SearchAutoComplete");
	//
	// SpannableStringBuilder stopHint = new SpannableStringBuilder("   ");
	// stopHint.append(getSearchHit());
	//
	// // Add the icon as an spannable
	// Drawable searchIcon = getResources().getDrawable(R.drawable.search);
	// Method textSizeMethod = clazz.getMethod("getTextSize");
	// Float rawTextSize = (Float) textSizeMethod.invoke(autoComplete);
	// int textSize = (int) (rawTextSize * 1.2);
	// searchIcon.setBounds(0, 0, textSize, textSize);
	// stopHint.setSpan(new ImageSpan(searchIcon), 1, 2,
	// Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	//
	// // Set the new hint text
	// Method setHintMethod = clazz.getMethod("setHint", CharSequence.class);
	// setHintMethod.invoke(autoComplete, stopHint);
	//
	// try {
	// mSearchView.setIconified(false);
	// closeButtonImage.setVisibility(View.GONE);
	//
	// Field field = SearchView.class.getDeclaredField("mIconifiedByDefault");
	// field.setAccessible(true);
	// field.set(mSearchView, true);
	//
	// int searchImgId = getResources().getIdentifier(
	// "android:id/search_button", null, null);
	// ImageView v = (ImageView) mSearchView.findViewById(searchImgId);
	// v.setVisibility(View.GONE);
	// v.setImageResource(R.drawable.search);
	// v.setBackgroundResource(R.drawable.action_img_selector);
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
//	private void setSearchListeners(SearchView mSearchView) {
//		mSearchView.setOnQueryTextListener(new OnQueryTextListener() {
//
//			@Override
//			public boolean onQueryTextSubmit(final String query) {
//				ViewUtils.hideInputMethodWindow(getActivity());
//				if (query.equals(searchWord)) {
//					return true;
//				}
//
//				searchWord = query;
//				loadMoreData(true);
//				return true;
//			}
//
//			@Override
//			public boolean onQueryTextChange(String newText) {
//				return false;
//			}
//		});
//	}
	
	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		View view = getActionBarView();

		final ImageView searchImageView = (ImageView) view
				.findViewById(R.id.img_search);
		final ImageView closeImageView = (ImageView) view
				.findViewById(R.id.img_close);

		final TextView hintView = (TextView) view.findViewById(R.id.t_search_hint);
		hintView.setText(getSearchHit());

		final EditText searchEdit = (EditText) view.findViewById(R.id.e_search);
		try {
			Field mCursorDrawableRes = TextView.class
					.getDeclaredField("mCursorDrawableRes");
			mCursorDrawableRes.setAccessible(true);
			mCursorDrawableRes.set(searchEdit, 0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		searchEdit.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				searchImageView.setVisibility(View.GONE);
				hintView.setVisibility(View.GONE);
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(searchEdit.getText())) {
					searchImageView.setVisibility(View.VISIBLE);
					hintView.setVisibility(View.VISIBLE);
					closeImageView.setVisibility(View.GONE);
				} else {
					closeImageView.setVisibility(View.VISIBLE);
				}
			}
		});

		searchEdit.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					String query = searchEdit.getText().toString();
					ViewUtils.hideInputMethodWindow(getActivity());
					if (query.equals(searchWord)) {
						return true;
					}

					searchWord = query;
					loadMoreData(true);
					return true;
				}
				return false;
			}
		});

		closeImageView.setVisibility(View.GONE);
		closeImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				searchEdit.setText("");
				closeImageView.setVisibility(View.GONE);
				searchEdit.requestFocus();
			}
		});
	}
	
	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateFooter();
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData(false);
				onPostLoad();
			}
		}, 2000);
	}

	protected void loadMoreData(final boolean newSearch) {
		if (searchWord == null || searchWord.trim().length() == 0) {
			ViewUtils.showToast("请先输入关键字");
			return;
		}

		if (newSearch) {
			hideDataView();
			offset = 0;
			dataList.clear();
			setAdapter();
		}

		doSearch(newSearch);
	}

	protected void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}

		updateFooter();
	}

	private void updateFooter() {
		if (xListView != null) {
			if (dataList.isEmpty()) {
				xListView.getFooterView().setHintText("没有匹配的数据");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setHintText(null);
				xListView.getFooterView().setEnabled(true);
			}
		}
	}

	protected void hideDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.VISIBLE);
			mainView.findViewById(R.id.v_data).setVisibility(View.GONE);

			mHandler.post(new Runnable() {

				@Override
				public void run() {
					mainView.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
				}
			});
		}
	}

	public void setAdapter() {
		adapter = (AbstractListAdapter<T>) getListAdapter();
		if (adapter == null) {
			setListAdapter(createListAdapter());
		} else {
			adapter.updateData(dataList);
		}
		updateFooter();
	}

	@Override
	protected void back() {
		ViewUtils.hideInputMethodWindow(getActivity());
		super.back();
	}
	
	protected abstract String getSearchHit();

	protected abstract void doSearch(final boolean newSearch);

	protected abstract AbstractListAdapter<T> createListAdapter();
}

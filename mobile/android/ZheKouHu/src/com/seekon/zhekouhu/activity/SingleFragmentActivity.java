package com.seekon.zhekouhu.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.store.LocationEntity;

public abstract class SingleFragmentActivity extends FragmentActivity {

	protected abstract Fragment createFragment();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		setContentView(R.layout.activity_fragment);

		LocationEntity location = (LocationEntity) getIntent()
				.getSerializableExtra(FuncConst.KEY_LOCATION);
		LocationEntity loc = RunEnv.getInstance().getLocation();
		if (location != null
				&& (loc == null || (loc != null && !location.equals(loc)))) {
			RunEnv.getInstance().setLocation(location);
		}

		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
		if (fragment == null) {
			fragment = createFragment();
			fm.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
		}
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

}

package com.seekon.zhekouhu.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.mqtt.MQTTUtils;
import com.seekon.zhekouhu.utils.LocationUtils;

/**
 * 启动界面
 * 
 * 可以做一些初始化的动作
 * 
 */
public class SplashActivity extends Activity {

	private Class<Activity> launchClass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_splash);

		launchClass = (Class<Activity>) getIntent().getSerializableExtra(
				FuncConst.NAME_ACTIVITY_CLASS);
		init();
	}

	private void init() {
		startMqttService();
		LocationUtils.startLocate();

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(2 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					Intent intent = null;
					boolean userGuidLaunched = PreferenceManager
							.getDefaultSharedPreferences(
									SplashActivity.this.getApplicationContext()).getBoolean(
									FuncConst.KEY_USER_GUID_LAUNCHED, false);
					if (userGuidLaunched) {
						if (launchClass != null) {
							intent = new Intent(SplashActivity.this, launchClass);
							Bundle bundle = getIntent().getExtras();
							if (bundle != null) {
								intent.putExtras(bundle);
							}
						} else {
							intent = new Intent(SplashActivity.this, MainActivity.class);
						}
					} else {
						intent = new Intent(SplashActivity.this, UserGuideActivity.class);
					}
					SplashActivity.this.finish();
					startActivity(intent);

				}
			}
		}).start();
	}

	private void startMqttService() {
		MQTTUtils.startMqttService(this);
	}
}

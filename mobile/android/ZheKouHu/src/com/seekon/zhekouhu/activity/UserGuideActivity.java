package com.seekon.zhekouhu.activity;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.FuncConst;

public class UserGuideActivity extends FragmentActivity {

	private ViewPager mViewPager;
	private List<String> imageFileNames = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		setContentView(R.layout.activity_image_preview);
		initViews();
	}

	private void initViews() {
		imageFileNames.add("user_guide_1");
		imageFileNames.add("user_guide_2");
		imageFileNames.add("user_guide_3");

		mViewPager = (ViewPager) findViewById(R.id.v_viewPager);

		int currentIndex = getIntent()
				.getIntExtra(FuncConst.EXTRA_CURRENT_INDEX, 0);
		setAdapter(currentIndex);
	}

	private void setAdapter(int currentIndex) {
		FragmentManager fm = getSupportFragmentManager();
		mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {

			@Override
			public int getCount() {
				return imageFileNames.size();
			}

			@Override
			public Fragment getItem(int pos) {
				boolean last = pos == imageFileNames.size() - 1;
				return UserGuideFragment.newInstance(imageFileNames.get(pos), last);
			}
		});

		if (currentIndex > -1 && currentIndex < imageFileNames.size()) {
			mViewPager.setCurrentItem(currentIndex);
		}
	}
}

package com.seekon.zhekouhu.activity;

import static com.seekon.zhekouhu.func.FuncConst.EXTRA_LAST;
import static com.seekon.zhekouhu.func.FuncConst.NAME_FILE;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.utils.RUtils;

public class UserGuideFragment extends Fragment {

	private String imageFileName;
	private boolean last;

	public static UserGuideFragment newInstance(String imageFileName, boolean last) {
		Bundle args = new Bundle();
		args.putString(NAME_FILE, imageFileName);
		args.putBoolean(EXTRA_LAST, last);

		UserGuideFragment fragment = new UserGuideFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		imageFileName = args.getString(NAME_FILE);
		last = args.getBoolean(EXTRA_LAST, false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_user_guide, container, false);
		initViews(view);
		return view;
	}

	private void initViews(View view) {
		ImageView fileView = (ImageView) view.findViewById(R.id.image_file);
		fileView.setImageResource(RUtils.getDrawableImg(imageFileName));

		if (last) {
			View startView = view.findViewById(R.id.b_start);
			startView.setVisibility(View.VISIBLE);

			startView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					PreferenceManager
							.getDefaultSharedPreferences(
									getActivity().getApplicationContext()).edit()
							.putBoolean(FuncConst.KEY_USER_GUID_LAUNCHED, true).commit();
					getActivity().finish();
					Intent intent = new Intent(getActivity(), MainActivity.class);
					startActivity(intent);
				}
			});
		}
	}
}

package com.seekon.zhekouhu.activity;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.func.FuncConst;

public class WebViewActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		String title = getIntent().getStringExtra(FuncConst.EXTRA_TITLE);
		String uri = getIntent().getStringExtra(FuncConst.EXTRA_URI);
		return WebViewFragment.newInstance(title, uri);
	}

}

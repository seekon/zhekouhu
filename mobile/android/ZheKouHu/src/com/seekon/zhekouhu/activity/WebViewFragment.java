package com.seekon.zhekouhu.activity;

import java.lang.ref.WeakReference;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.utils.ViewUtils;

public class WebViewFragment extends CustomActionBarFragment {

	private WebView webView;
	private Handler handler;
	private String uri;

	public static WebViewFragment newInstance(String title, String uri) {
		Bundle args = new Bundle();
		args.putString(FuncConst.EXTRA_TITLE, title);
		args.putString(FuncConst.EXTRA_URI, uri);

		WebViewFragment fragment = new WebViewFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		String title = getArguments().getString(FuncConst.EXTRA_TITLE);
		setTitle(title);

		uri = getArguments().getString(FuncConst.EXTRA_URI);

		handler = new MyHandler(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_webview, container, false);
		initViews(view);

		return view;
	}

	@SuppressLint({ "SetJavaScriptEnabled" })
	private void initViews(View view) {
		webView = (WebView) view.findViewById(R.id.webView);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);// 不使用缓存
		webView.setScrollBarStyle(0);// 滚动条风格，为0就是不给滚动条留空间，滚动条覆盖在网页上

		webView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(final WebView view,
					final String url) {
				loadurl(view, url);
				return true;
			}

		});
		webView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {
				if (progress == 100) {
					handler.sendEmptyMessage(1);
				}
				super.onProgressChanged(view, progress);
			}
		});

		loadurl(webView, Const.SERVER_APP_URL + uri);
	}

	private void loadurl(final WebView view, final String url) {
		view.post(new Runnable() {

			@Override
			public void run() {
				handler.sendEmptyMessage(0);
				view.loadUrl(url);// 载入网页
			}
		});
	}

	static class MyHandler extends Handler {
		private WeakReference<WebViewFragment> mOuter;
		private Dialog progressDialog;

		public MyHandler(WebViewFragment activity) {
			mOuter = new WeakReference<WebViewFragment>(activity);
			progressDialog = ViewUtils.createProgreddDialog(activity.getActivity(),
					"数据载入中，请稍候...");
		}

		public void handleMessage(Message msg) {
			WebViewFragment outer = mOuter.get();
			if (outer != null) {
				if (!Thread.currentThread().isInterrupted()) {
					switch (msg.what) {
					case 0:
						if (progressDialog != null) {
							progressDialog.show();
						}
						break;
					case 1:
						if (progressDialog != null) {
							progressDialog.hide();
						}
						break;
					}
				}
				super.handleMessage(msg);
			}
		}

	}
}

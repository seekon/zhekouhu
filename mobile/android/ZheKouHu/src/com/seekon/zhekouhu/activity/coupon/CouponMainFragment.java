package com.seekon.zhekouhu.activity.coupon;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.LocationableListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.discount.widget.CouponMainAdapter;
import com.seekon.zhekouhu.func.store.CouponStoreEntity;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.sync.SyncEntity;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;
import com.seekon.zhekouhu.utils.DateUtils;

public class CouponMainFragment extends LocationableListFragment implements
		IXListViewListener {

	private Handler mHandler;
	private XListView xListView;
	private View mainView;
	private boolean dataLoaded = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mHandler = new Handler();
	}

	private void initData() {
//		AsyncProcessorTask<Map<CategoryEntity, List<CouponStoreEntity>>> task = new AsyncProcessorTask<Map<CategoryEntity, List<CouponStoreEntity>>>(
//				getActivity(),
//				new ProcessorTaskCallback<Map<CategoryEntity, List<CouponStoreEntity>>>() {
//
//					@Override
//					public ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>> doInBackground() {
//						return ProcessorFactory.getDiscountProcessor()
//								.storesHasCouponFromLocal();
//					}
//
//					@Override
//					public void onSuccess(
//							Map<CategoryEntity, List<CouponStoreEntity>> result) {
//						if (result == null || result.isEmpty()) {
//							refreshDataFromRemote();
//						} else {
//							setAdapter(result);
//							showDataView();
//						}
//					}
//				});
//		task.execute();
		refreshDataFromRemote();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		updateActionBar();
		mainView = inflater
				.inflate(R.layout.fragment_coupon_main, container, false);
		initViews(mainView);
		return mainView;
	}

	private void updateActionBar() {
		View actionBarView = getActionBarView();

		actionBarView.findViewById(R.id.img_logo_title).setVisibility(View.GONE);
		actionBarView.findViewById(R.id.t_title).setVisibility(View.VISIBLE);
		setTitle(R.string.title_coupon);

		actionBarView.findViewById(R.id.img_action).setVisibility(View.GONE);
		actionBarView.findViewById(R.id.t_action).setVisibility(View.GONE);
	}

	private void initViews(View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);
		// xListView.getFooterView().setVisibility(View.GONE);

		mHandler.post(new Runnable() {

			@Override
			public void run() {
				mainView.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});

		updateRefreshTime();
		if (dataLoaded) {
			showDataView();
		} else if (RunEnv.getInstance().getLocation() != null) {
			initData();
		}
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateRefreshTime();
		}
	}

	private void updateRefreshTime() {
		SyncEntity sync = ProcessorFactory.getSyncProcessor()
				.couponMainSyncEntity();
		String updateTime = null;
		if (sync != null) {
			updateTime = sync.getUpdateTime();
		}
		if (updateTime != null) {
			try {
				xListView
						.setRefreshTime(DateUtils.formartTime(Long.valueOf(updateTime)));
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void onRefresh() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				refreshDataFromRemote();
			}
		}, 2000);
	}

	@Override
	public void onLoadMore() {
		onPostLoad();
	}

	private void refreshDataFromRemote() {
		AsyncProcessorTask<Map<CategoryEntity, List<CouponStoreEntity>>> task = new AsyncProcessorTask<Map<CategoryEntity, List<CouponStoreEntity>>>(
				getActivity(),
				new ProcessorTaskCallback<Map<CategoryEntity, List<CouponStoreEntity>>>() {

					@Override
					public ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>> doInBackground() {
						return ProcessorFactory.getDiscountProcessor()
								.storesHasCouponRemoteRefresh();
					}

					@Override
					public void onSuccess(
							Map<CategoryEntity, List<CouponStoreEntity>> result) {
						if(result != null && result.size() > 0){
							setAdapter(result);
						}
						onPostLoad();
						showDataView();
					}

					@Override
					public void onFailed(Error error) {
						super.onFailed(error);
						onPostLoad();
						showDataView();
					}
				});
		task.execute();
	}

	private void showDataView() {
		dataLoaded = true;
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
		updateFooter();
	}

	private void setAdapter(Map<CategoryEntity, List<CouponStoreEntity>> data) {
		CouponMainAdapter adapter = (CouponMainAdapter) getListAdapter();
		if (adapter == null) {
			setListAdapter(new CouponMainAdapter(getActivity(), data));
		} else {
			adapter.updateData(data);
		}
		updateFooter();
	}

	private void updateFooter() {
		CouponMainAdapter adapter = (CouponMainAdapter) getListAdapter();
		int count = adapter == null ? 0 : adapter.getCount();
		if (xListView != null) {
			if (count == 0) {
				xListView.getFooterView().setVisibility(View.VISIBLE);
				xListView.getFooterView().setHintText("暂时没有数据");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setVisibility(View.GONE);
			}
		}
	}

	@Override
	public void doLocationValidate(LocationEntity loc) {
		if (dataLoaded) {
			return;
		}
		initData();
	}
}

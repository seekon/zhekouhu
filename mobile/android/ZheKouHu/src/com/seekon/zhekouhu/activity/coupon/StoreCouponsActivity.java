package com.seekon.zhekouhu.activity.coupon;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.store.CouponStoreEntity;

public class StoreCouponsActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		CouponStoreEntity store = (CouponStoreEntity) getIntent()
				.getSerializableExtra(FuncConst.NAME_STORE);
		if (store == null) {
			return new StoreCouponsFragment();
		}
		return StoreCouponsFragment.newInstance(store);
	}

}

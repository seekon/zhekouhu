package com.seekon.zhekouhu.activity.coupon;

import static com.seekon.zhekouhu.func.FuncConst.NAME_STORE;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.LocationableListFragment;
import com.seekon.zhekouhu.activity.discount.DiscountDetailActivity;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.DiscountMainListAdapter;
import com.seekon.zhekouhu.func.store.CouponStoreEntity;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.sync.SyncEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;
import com.seekon.zhekouhu.utils.DateUtils;

public class StoreCouponsFragment extends LocationableListFragment implements
		IXListViewListener {

	private CouponStoreEntity store;
	private int offset;

	private ArrayList<DiscountEntity> dataList = new ArrayList<DiscountEntity>();

	private Handler mHandler;
	private XListView xListView;
	private View mainView;

	public static StoreCouponsFragment newInstance(CouponStoreEntity store) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_STORE, store);

		StoreCouponsFragment fragment = new StoreCouponsFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		mHandler = new Handler();

		Bundle args = getArguments();
		if (args != null) {
			store = (CouponStoreEntity) args.getSerializable(NAME_STORE);
		}

		super.onCreate(savedInstanceState);

		if (store != null) {
			setTitle(store.getName());
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.base_loading_xlistview, container,
				false);
		initViews(mainView);
		return mainView;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		DiscountEntity item = dataList.get(position - 1);
		Intent intent = new Intent(getActivity(), DiscountDetailActivity.class);
		intent.putExtra(FuncConst.NAME_DISCOUNT, (DiscountEntity) item);
		startActivity(intent);
	}

	private void initViews(final View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		mHandler.post(new Runnable() {

			@Override
			public void run() {
				view.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});

		updateRefreshTime();
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateRefreshTime();
		}
	}

	private void updateRefreshTime() {
		SyncEntity sync = ProcessorFactory.getSyncProcessor()
				.storeCouponSyncEntity(store.getStoreId());
		String updateTime = null;
		if (sync != null) {
			updateTime = sync.getUpdateTime();
		}
		if (updateTime != null) {
			try {
				xListView
						.setRefreshTime(DateUtils.formartTime(Long.valueOf(updateTime)));
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
		// mHandler.postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// refreshDataFromRemote();
		// }
		// }, 2000);
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData();
				onPostLoad();
			}
		}, 2000);
	}

	private void loadMoreData() {
		AsyncProcessorTask<List<DiscountEntity>> task = new AsyncProcessorTask<List<DiscountEntity>>(
				getActivity(), new ProcessorTaskCallback<List<DiscountEntity>>() {

					@Override
					public ProcessResult<List<DiscountEntity>> doInBackground() {
						return ProcessorFactory.getDiscountProcessor()
								.couponsForStoreRemoteRefresh(store.getStoreId(),
										store.getCategoryId(), offset);
					}

					@Override
					public void onSuccess(List<DiscountEntity> result) {
						if (result != null && result.size() > 0) {
							offset += result.size();
							dataList.addAll(result);
							setAdapter();
						} else if (getListAdapter() == null) {
							setAdapter();
						}
					}

					@Override
					public void onFailed(Error error) {
						super.onFailed(error);
						setAdapter();
					}

				});
		task.execute();
	}

	// private void refreshDataFromRemote() {
	// AsyncProcessorTask<List<DiscountEntity>> task = new
	// AsyncProcessorTask<List<DiscountEntity>>(
	// getActivity(), new ProcessorTaskCallback<List<DiscountEntity>>() {
	//
	// @Override
	// public ProcessResult<List<DiscountEntity>> doInBackground() {
	// return ProcessorFactory.getDiscountProcessor()
	// .couponsForStoreRemoteRefresh(store.getStoreId(), store.getCategoryId(),
	// 0);
	// }
	//
	// @Override
	// public void onSuccess(List<DiscountEntity> result) {
	// if (result || getListAdapter() == null) {
	// loadMoreData(true);
	// } else {
	// setAdapter();
	// }
	// }
	//
	// @Override
	// public void onFailed(Error error) {
	// super.onFailed(error);
	// setAdapter();
	// }
	//
	// });
	// task.execute();
	// }

	public void setAdapter() {
		AbstractListAdapter<DiscountEntity> adapter = (AbstractListAdapter<DiscountEntity>) getListAdapter();
		if (adapter == null) {
			setListAdapter(new DiscountMainListAdapter(getActivity(), dataList, false));
		} else {
			adapter.updateData(dataList);
		}

		onPostLoad();
		showDataView();
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void doLocationValidate(LocationEntity loc) {
		if (store != null) {
			loadMoreData();
		}
	}
}

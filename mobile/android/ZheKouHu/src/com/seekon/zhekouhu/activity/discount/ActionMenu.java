package com.seekon.zhekouhu.activity.discount;

import java.io.Serializable;

public class ActionMenu implements Serializable {

	private static final long serialVersionUID = -3585149163665514832L;

	private int iconResId;
	private String name;

	public ActionMenu(int iconResId, String name) {
		super();
		this.iconResId = iconResId;
		this.name = name;
	}

	public int getIconResId() {
		return iconResId;
	}

	public void setIconResId(int iconResId) {
		this.iconResId = iconResId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

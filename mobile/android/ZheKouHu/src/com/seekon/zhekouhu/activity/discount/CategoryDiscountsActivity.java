package com.seekon.zhekouhu.activity.discount;

import static com.seekon.zhekouhu.func.FuncConst.NAME_CATEGORY;
import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.func.category.CategoryEntity;

public class CategoryDiscountsActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		CategoryEntity category = (CategoryEntity) getIntent()
				.getSerializableExtra(NAME_CATEGORY);
		if (category == null) {
			return new CategoryDiscountsFragment();
		}
		return CategoryDiscountsFragment.newInstance(category);
	}

}

package com.seekon.zhekouhu.activity.discount;

import static com.seekon.zhekouhu.func.FuncConst.NAME_CATEGORY;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.LocationableListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.DiscountMainListAdapter;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;

public class CategoryDiscountsFragment extends LocationableListFragment
		implements IXListViewListener {

	private CategoryEntity category;
	private int offset;
	private ArrayList<DiscountEntity> dataList = new ArrayList<DiscountEntity>();

	private Handler mHandler;
	private XListView xListView;
	private View mainView;

	public static CategoryDiscountsFragment newInstance(CategoryEntity category) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_CATEGORY, category);

		CategoryDiscountsFragment fragment = new CategoryDiscountsFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Bundle args = getArguments();
		if (args != null) {
			category = (CategoryEntity) args.getSerializable(NAME_CATEGORY);
			setTitle(category.getName());
		}

		setRetainInstance(true);

		mHandler = new Handler();

		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.base_loading_xlistview, container,
				false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(final View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		// updateRefreshTime();
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				view.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});
	}

	public void setAdapter() {
		AbstractListAdapter<DiscountEntity> adapter = (AbstractListAdapter<DiscountEntity>) getListAdapter();
		if (adapter == null) {
			setListAdapter(new DiscountMainListAdapter(getActivity(), dataList));
		} else {
			adapter.updateData(dataList);
		}

		showDataView();
		updateFooter();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (dataList == null || dataList.isEmpty() || position > dataList.size()) {
			return;
		}
		DiscountEntity item = dataList.get(position - 1);
		Intent intent = new Intent(getActivity(), DiscountDetailActivity.class);
		intent.putExtra(FuncConst.NAME_DISCOUNT, (DiscountEntity) item);
		startActivity(intent);
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateFooter();
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
		// mHandler.postDelayed(new Runnable() {
		//
		// @Override
		// public void run() {
		// //refreshDataFromRemote(false);
		// }
		// }, 2000);
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData();
				onPostLoad();
			}
		}, 2000);
	}

	private void loadMoreData() {

		AsyncProcessorTask<List<DiscountEntity>> task = new AsyncProcessorTask<List<DiscountEntity>>(
				getActivity(), new ProcessorTaskCallback<List<DiscountEntity>>() {

					@Override
					public ProcessResult<List<DiscountEntity>> doInBackground() {
						return ProcessorFactory.getDiscountProcessor()
								.activitiesForCategory(category, offset);
					}

					@Override
					public void onSuccess(List<DiscountEntity> result) {
						if (result != null && !result.isEmpty()) {
							offset += result.size();
							dataList.addAll(result);
							setAdapter();
						} else if (getListAdapter() == null) {
							setAdapter();
						}
					}

					@Override
					public void onFailed(Error error) {
						setAdapter();
						super.onFailed(error);
					}
				});
		task.execute();
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}

	private void updateFooter() {
		if (xListView != null) {
			if (dataList.isEmpty()) {
				xListView.getFooterView().setHintText("暂时没有数据");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setHintText(null);
				xListView.getFooterView().setEnabled(true);
			}
		}
	}

	@Override
	public void doLocationValidate(LocationEntity loc) {
		if (category != null) {
			loadMoreData();
		}
	}
}

package com.seekon.zhekouhu.activity.discount;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TabHost.TabSpec;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.discount.CommentEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.utils.ViewUtils;

public class CommentsActivity extends FragmentActivity {

	private FragmentTabHost mTabHost;
	private EditText commentField;
	private String discountId;

	private CommentsFragment commentsFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		DiscountEntity discount = (DiscountEntity) getIntent()
				.getSerializableExtra(FuncConst.NAME_DISCOUNT);
		if (discount != null) {
			discountId = discount.getUuid();
		} else {
			discountId = getIntent().getStringExtra(DataConst.COL_NAME_UUID);
		}

		setContentView(R.layout.activity_main);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

		LayoutInflater layoutInflater = LayoutInflater.from(this);
		final View view = layoutInflater
				.inflate(R.layout.common_input_bottom, null);
		commentField = (EditText) view.findViewById(R.id.e_comment);
		commentField.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					view.findViewById(R.id.v_comment_hit).setVisibility(View.GONE);
				} else {
					view.findViewById(R.id.v_comment_hit).setVisibility(View.VISIBLE);
				}
			}
		});

		view.findViewById(R.id.b_send).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!TextUtils.isEmpty(commentField.getText().toString())) {
					AsyncProcessorTask<CommentEntity> task = new AsyncProcessorTask<CommentEntity>(
							CommentsActivity.this,
							new ProcessorTaskCallback<CommentEntity>() {

								@Override
								public ProcessResult<CommentEntity> doInBackground() {
									CommentEntity comment = new CommentEntity();
									comment.setDiscountId(discountId);
									comment.setContent(commentField.getText().toString());
									comment.setType(FuncConst.KEY_TYPE_DIS_COMMENT);
									UserEntity user = RunEnv.getInstance().getUser();
									if (user != null) {
										comment.setPublisher(user.getUuid());
									} else {
										comment.setPublisher(ViewUtils.getDeviceId());
									}
									return ProcessorFactory.getDiscountProcessor()
											.publishComment(comment);
								}

								@Override
								public void onSuccess(CommentEntity result) {
									if (result != null) {
										commentField.setText("");
										view.setFocusable(true);
										view.setFocusableInTouchMode(true);
										view.requestFocus();
										ViewUtils.hideInputMethodWindow(CommentsActivity.this);
										ViewUtils.showToast("评论发送成功.");
										CommentsFragment fragment = getCommentsFragment();
										if (fragment != null) {
											fragment.addComment(result);
										}
									} else {
										ViewUtils.showToast("评论发送失败.");
									}
								}
							});
					task.execute();
				}
			}
		});

		TabSpec tabSpec = mTabHost.newTabSpec(
				DiscountDetailFragment.class.getName()).setIndicator(view);
		mTabHost.addTab(tabSpec, CommentsFragment.class, null);
	}

	private CommentsFragment getCommentsFragment() {
		if (commentsFragment == null) {
			FragmentManager fm = getSupportFragmentManager();
			for (Fragment fragment : fm.getFragments()) {
				if (fragment instanceof CommentsFragment) {
					commentsFragment = (CommentsFragment) fragment;
					break;
				}
			}
		}
		return commentsFragment;
	}
}

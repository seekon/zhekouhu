package com.seekon.zhekouhu.activity.discount;

import static com.seekon.zhekouhu.func.FuncConst.NAME_DISCOUNT;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.discount.CommentEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.CommentsAdapter;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;

public class CommentsFragment extends CustomActionBarListFragment implements
		IXListViewListener {

	private DiscountEntity discount;
	private int offset;
	private ArrayList<CommentEntity> dataList = new ArrayList<CommentEntity>();

	private Handler mHandler;
	private XListView xListView;
	private View mainView;

	public static CommentsFragment newInstance(DiscountEntity discount) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_DISCOUNT, discount);

		CommentsFragment fragment = new CommentsFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			discount = (DiscountEntity) args.getSerializable(NAME_DISCOUNT);
		}
		if (discount == null) {
			FragmentActivity activity = this.getActivity();
			discount = (DiscountEntity) activity.getIntent().getSerializableExtra(
					FuncConst.NAME_DISCOUNT);
		}
		setRetainInstance(true);

		mHandler = new Handler();
		loadMoreData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.fragment_comments, container, false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(final View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		updateCommentCountView();

		// updateRefreshTime();
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				view.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});
	}

	public void setAdapter() {
		AbstractListAdapter<CommentEntity> adapter = (AbstractListAdapter<CommentEntity>) getListAdapter();
		if (adapter == null) {
			setListAdapter(new CommentsAdapter(getActivity(), dataList));
		} else {
			adapter.updateData(dataList);
		}

		showDataView();
		updateFooter();
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateFooter();
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData();
				onPostLoad();
			}
		}, 2000);
	}

	private void loadMoreData() {

		AsyncProcessorTask<List<CommentEntity>> task = new AsyncProcessorTask<List<CommentEntity>>(
				getActivity(), new ProcessorTaskCallback<List<CommentEntity>>() {

					@Override
					public ProcessResult<List<CommentEntity>> doInBackground() {
						return ProcessorFactory.getDiscountProcessor()
								.getCommentsByDiscount(discount, 10, offset);
					}

					@Override
					public void onSuccess(List<CommentEntity> result) {
						if (result != null && !result.isEmpty()) {
							offset += result.size();
							dataList.addAll(result);
							setAdapter();
						} else if (getListAdapter() == null) {
							setAdapter();
						}
					}

					@Override
					public void onFailed(Error error) {
						setAdapter();
						super.onFailed(error);
					}
				});
		task.execute();
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}

	private void updateFooter() {
		if (xListView != null) {
			if (dataList.isEmpty()) {
				xListView.getFooterView().setHintText("暂时没有数据");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setHintText(null);
				xListView.getFooterView().setEnabled(true);
			}
		}
	}

	private void updateCommentCountView() {
		TextView countView = (TextView) mainView.findViewById(R.id.t_comment_count);
		countView.setText(discount.getCommentCount() + "条评论");
	}

	public void addComment(CommentEntity comment) {
		dataList.add(0, comment);
		setAdapter();

		discount.setCommentCount(discount.getCommentCount() + 1);
		updateCommentCountView();
	}
}

package com.seekon.zhekouhu.activity.discount;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TabHost.TabSpec;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.discount.CommentEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.share.sina.WBConst;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.constant.WBConstants;

public class DiscountDetailActivity extends FragmentActivity implements
		IWeiboHandler.Response {

	private FragmentTabHost mTabHost;
	private EditText commentField;
	private String discountId;
	private DiscountDetailFragment discountDetailFragment;

	// private DiscountDetailFragment discountDetailFragment = null;
	//
	// @Override
	// protected Fragment createFragment() {
	// DiscountEntity discount = (DiscountEntity) getIntent()
	// .getSerializableExtra(FuncConst.NAME_DISCOUNT);
	// String uuid = getIntent().getStringExtra(DataConst.COL_NAME_UUID);
	// if (discount != null) {
	// discountDetailFragment = DiscountDetailFragment.newInstance(discount);
	// } else if (uuid != null) {
	// discountDetailFragment = DiscountDetailFragment.newInstance(uuid);
	// } else {
	// discountDetailFragment = new DiscountDetailFragment();
	// }
	// return discountDetailFragment;
	// }

	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		WeiboShareSDK.createWeiboAPI(this, WBConst.APP_KEY).handleWeiboResponse(
				intent, this); // 当前应用唤起微博分享后,返回当前应用
	}

	@Override
	public void onResponse(BaseResponse baseResp) {
		discountDetailFragment = getDiscountDetailFragment();
		if (discountDetailFragment != null) {
			discountDetailFragment.dismissSharePopupWindow();
		}
		switch (baseResp.errCode) {
		case WBConstants.ErrorCode.ERR_OK:
			ViewUtils.showToast("已成功分享到新浪微博.");
			break;
		case WBConstants.ErrorCode.ERR_CANCEL:

			break;
		case WBConstants.ErrorCode.ERR_FAIL:
			ViewUtils.showToast("分享到新浪微博失败.");
			break;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		DiscountEntity discount = (DiscountEntity) getIntent()
				.getSerializableExtra(FuncConst.NAME_DISCOUNT);
		if (discount != null) {
			discountId = discount.getUuid();
		} else {
			discountId = getIntent().getStringExtra(DataConst.COL_NAME_UUID);
		}

		setContentView(R.layout.activity_main);

		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

		LayoutInflater layoutInflater = LayoutInflater.from(this);
		final View view = layoutInflater
				.inflate(R.layout.common_input_bottom, null);
		commentField = (EditText) view.findViewById(R.id.e_comment);
		commentField.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					view.findViewById(R.id.v_comment_hit).setVisibility(View.GONE);
				} else {
					view.findViewById(R.id.v_comment_hit).setVisibility(View.VISIBLE);
				}
			}
		});
		view.findViewById(R.id.b_send).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!TextUtils.isEmpty(commentField.getText().toString())) {
					AsyncProcessorTask<CommentEntity> task = new AsyncProcessorTask<CommentEntity>(
							DiscountDetailActivity.this,
							new ProcessorTaskCallback<CommentEntity>() {

								@Override
								public ProcessResult<CommentEntity> doInBackground() {
									CommentEntity comment = new CommentEntity();
									comment.setDiscountId(discountId);
									comment.setContent(commentField.getText().toString());
									comment.setType(FuncConst.KEY_TYPE_DIS_COMMENT);
									UserEntity user = RunEnv.getInstance().getUser();
									if (user != null) {
										comment.setPublisher(user.getCode());
									} else {
										comment.setPublisher(ViewUtils.getDeviceId());
									}
									return ProcessorFactory.getDiscountProcessor()
											.publishComment(comment);
								}

								@Override
								public void onSuccess(CommentEntity result) {
									if (result != null) {
										commentField.setText("");
										view.setFocusable(true);
										view.setFocusableInTouchMode(true);
										view.requestFocus();
										ViewUtils
												.hideInputMethodWindow(DiscountDetailActivity.this);
										ViewUtils.showToast("评论发送成功.");
										DiscountDetailFragment fragment = getDiscountDetailFragment();
										if (fragment != null) {
											fragment.addComment(result);
										}
									} else {
										ViewUtils.showToast("评论发送失败.");
									}
								}
							});
					task.execute();
				}
			}
		});

		TabSpec tabSpec = mTabHost.newTabSpec(
				DiscountDetailFragment.class.getName()).setIndicator(view);
		mTabHost.addTab(tabSpec, DiscountDetailFragment.class, null);
	}

	private DiscountDetailFragment getDiscountDetailFragment() {
		if (discountDetailFragment == null) {
			FragmentManager fm = getSupportFragmentManager();
			for (Fragment fragment : fm.getFragments()) {
				if (fragment instanceof DiscountDetailFragment) {
					discountDetailFragment = (DiscountDetailFragment) fragment;
					break;
				}
			}
		}
		return discountDetailFragment;
	}
}

package com.seekon.zhekouhu.activity.discount;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.func.FuncConst.KEY_DEF_DISCOUNT_COMMENTS_SHOW_NUM;
import static com.seekon.zhekouhu.func.FuncConst.KEY_DEF_DISCOUNT_FRONTS_SHOW_NUM;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DISCOUNT;
import static com.seekon.zhekouhu.func.FuncConst.NAME_ORIGIN;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.ImagePreviewActivity;
import com.seekon.zhekouhu.activity.ImagePreviewFragment;
import com.seekon.zhekouhu.activity.LocationableFragment;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.discount.CommentEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.CommentsAdapter;
import com.seekon.zhekouhu.func.discount.widget.DiscountStorefrontsAdapter;
import com.seekon.zhekouhu.func.share.sina.WeiboUtils;
import com.seekon.zhekouhu.func.share.weixin.WeixinUtils;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.widget.ActionMenuPopWindow;
import com.seekon.zhekouhu.func.widget.ImageViewClickDelegate;
import com.seekon.zhekouhu.func.widget.SharePopupWindow;
import com.seekon.zhekouhu.utils.DateUtils;
import com.seekon.zhekouhu.utils.ViewUtils;

public class DiscountDetailFragment extends LocationableFragment {

	private DiscountEntity discount;

	private int origin = FuncConst.DISCOUNT_ORIGIN_DEFAULT;

	private ViewPager imageViewPager;
	private ViewGroup imgDotViewGroup;
	private ImageViewClickDelegate actionDelegate;
	private View mainView;

	private PopupWindow actionMenuWindow;
	private PopupWindow sharePopupWindow;
	private Dialog faultDialog;

	private List<ActionMenu> actionMenuList = new ArrayList<ActionMenu>();
	private boolean unfavorited = false;

	public static DiscountDetailFragment newInstance(DiscountEntity discount,
			int orgin) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_DISCOUNT, discount);
		args.putInt(NAME_ORIGIN, orgin);

		DiscountDetailFragment fragment = new DiscountDetailFragment();
		fragment.setArguments(args);

		return fragment;
	}

	public static DiscountDetailFragment newInstance(String uuid) {
		Bundle args = new Bundle();
		args.putSerializable(COL_NAME_UUID, uuid);

		DiscountDetailFragment fragment = new DiscountDetailFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		// this.setHasOptionsMenu(true);

		Bundle args = getArguments();
		if (args == null) {
			FragmentActivity activity = this.getActivity();
			origin = activity.getIntent().getIntExtra(NAME_ORIGIN, 0);
			discount = (DiscountEntity) activity.getIntent().getSerializableExtra(
					FuncConst.NAME_DISCOUNT);
			if (discount == null) {
				String uuid = activity.getIntent().getStringExtra(
						DataConst.COL_NAME_UUID);
				initDiscountById(uuid);
			} else {
				discount.setStorefronts(new ArrayList<StorefrontEntity>());
			}
		}
		if (args != null) {
			origin = args.getInt(NAME_ORIGIN);
			discount = (DiscountEntity) args.getSerializable(NAME_DISCOUNT);
			if (discount == null) {
				String uuid = args.getString(COL_NAME_UUID);
				initDiscountById(uuid);
			}

		}
		boolean isFavorit = false;
		if (discount != null) {
			isFavorit = ProcessorFactory.getDiscountProcessor().isUserFavorit(
					discount.getUuid());
		}

		actionDelegate = new DiscountImageViewClickDelegate();
		if (origin == FuncConst.DISCOUNT_ORIGIN_FAVORIT) {// 从收藏过来的，需获取额外的门店数和评论数据，设置actionbar的动作为取消收藏
			AsyncProcessorTask<DiscountEntity> task = new AsyncProcessorTask<DiscountEntity>(
					getActivity(), new ProcessorTaskCallback<DiscountEntity>() {

						@Override
						public ProcessResult<DiscountEntity> doInBackground() {
							return ProcessorFactory.getDiscountProcessor()
									.getDiscountExtWithFavorit(discount);
						}

						@Override
						public void onSuccess(DiscountEntity result) {
							if (mainView != null) {
								String frontCountLabel = discount.getFrontCount() + "家门店";
								LocationEntity location = RunEnv.getInstance().getLocation();
								if (location != null) {
									// frontCountLabel = location.getCityName() + frontCountLabel;
								}
								TextView frontCountView = (TextView) mainView
										.findViewById(R.id.t_storefront_count);
								frontCountView.setText(frontCountLabel);

								TextView commentCountView = (TextView) mainView
										.findViewById(R.id.t_comment_count);
								commentCountView.setText(discount.getCommentCount() + "条评论");
								initCommentsView();
							} else {
								discount = result;
							}
						}
					});
			task.execute();
			actionMenuList.add(FuncConst.ACTION_MENU_UNFAVORIT);
		} else {
			if (isFavorit) {
				actionMenuList.add(FuncConst.ACTION_MENU_UNFAVORIT);
			} else {
				actionMenuList.add(FuncConst.ACTION_MENU_FAVORIT);
			}
		}
		actionMenuList.add(FuncConst.ACTION_MENU_SHARE);
		actionMenuList.add(FuncConst.ACTION_MENU_FAULT);

		super.onCreate(savedInstanceState);
	}

	private void initDiscountById(final String uuid) {
		if (uuid != null) {
			AsyncProcessorTask<DiscountEntity> task = new AsyncProcessorTask<DiscountEntity>(
					getActivity(), new ProcessorTaskCallback<DiscountEntity>() {

						@Override
						public ProcessResult<DiscountEntity> doInBackground() {
							return ProcessorFactory.getDiscountProcessor().discountById(uuid);
						}

						@Override
						public void onSuccess(DiscountEntity result) {
							discount = result;
							updateViews(mainView);
						}
					});
			task.execute();
		}
	}

	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		View actionBarView = getActionBarView();
		ImageView actionImage = (ImageView) actionBarView
				.findViewById(R.id.img_action);
		actionImage.setImageResource(R.drawable.action_more);
		actionImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showActionMenuWindow(v);
			}
		});
	}

	private void showActionMenuWindow(View view) {
		if (actionMenuWindow == null) {
			actionMenuWindow = new ActionMenuPopWindow(getActivity(), actionMenuList,
					new OnItemClickListener() {

						@Override
						public void onItemClick(AdapterView<?> parent, View view,
								int position, long id) {
							actionMenuWindow.dismiss();
							ActionMenu menu = actionMenuList.get(position);
							switch (menu.getIconResId()) {
							case R.drawable.menu_share:
								showSharePopWindow();
								break;
							case R.drawable.menu_favorit:
								favoritDiscount();
								break;
							case R.drawable.menu_fault:
								faultDiscount();
								break;
							case R.drawable.menu_unfavorit:
								unfavoritDiscount();
								break;
							default:
								break;
							}
						}

					});
		}
		if (!actionMenuWindow.isShowing()) {
			actionMenuWindow.showAsDropDown(view);
		}
	}

	private void showSharePopWindow() {
		if (sharePopupWindow == null) {
			sharePopupWindow = new SharePopupWindow(getActivity(),
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							// sharePopupWindow.dismiss();
							switch (v.getId()) {
							case R.id.img_weixin:
								shareToWeixin();
								break;
							case R.id.img_friends:
								shareToFriends();
								break;
							case R.id.img_sina:
								shareToSina();
								break;
							default:
								break;
							}
						}
					});
		}

		if (!sharePopupWindow.isShowing()) {
			sharePopupWindow.showAtLocation(mainView, Gravity.BOTTOM
					| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
		}
	}

	private void updateActionBar() {
		String title = "";
		if (FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY.equals(discount.getType())) {
			title += getActivity().getString(R.string.title_discount_detail);
		} else {
			title += getActivity().getString(R.string.title_coupon_detail);
		}
		setTitle(title);
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.discount_detail, menu);
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.action_share:
	// showSharePopWindow();
	// return true;
	// case R.id.action_favorit:
	// favoritDiscount();
	// return true;
	// case R.id.aciton_fault:
	// faultDiscount();
	// return true;
	// default:
	// return super.onOptionsItemSelected(item);
	// }
	// }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.fragment_discount_detail, container,
				false);
		updateViews(mainView);
		return mainView;
	}

	private void updateViews(final View view) {
		if (discount == null || view == null) {
			return;
		}

		updateActionBar();
		view.findViewById(R.id.v_loading).setVisibility(View.GONE);
		view.findViewById(R.id.v_data).setVisibility(View.VISIBLE);

		TextView contentView = (TextView) view.findViewById(R.id.t_content);
		contentView.setText(discount.getContent());

		TextView termView = (TextView) view.findViewById(R.id.t_term);
		termView.setText("有效期："
				+ DateUtils.getDateString_yyyyMMdd(discount.getStartDate()) + " 到 "
				+ DateUtils.getDateString_yyyyMMdd(discount.getEndDate()));

		TextView palValView = (TextView) view.findViewById(R.id.t_pal_value);
		if (discount.getType().equals(FuncConst.VAL_DISCOUNT_TYPE_COUPON)) {
			palValView.setVisibility(View.VISIBLE);
			palValView.setText("￥" + discount.getParValue());
		} else {
			palValView.setVisibility(View.GONE);
		}

		// 设置来源信息
		View originView = view.findViewById(R.id.v_origin);
		String origin = discount.getOrigin();
		if (origin.equals(FuncConst.VAL_DISCOUNT_ORIGIN_CUSTOMER)) {
			originView.setVisibility(View.VISIBLE);
			TextView originText = (TextView) view.findViewById(R.id.t_origin);
			// TODO:获取发布者的昵称
			String publisher = discount.getPublisher();
			publisher = publisher.substring(0, 2) + "..."
					+ publisher.substring(publisher.length() - 2);
			originText.setText(getActivity().getString(R.string.label_origin_info,
					new Object[] { publisher }));
		} else {
			originView.setVisibility(View.GONE);
		}

		// 设置门店
		String frontCountLabel = discount.getFrontCount() + "家门店";
		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location != null) {
			// frontCountLabel = location.getCityName() + frontCountLabel;
		}
		TextView frontCountView = (TextView) view
				.findViewById(R.id.t_storefront_count);
		frontCountView.setText(frontCountLabel);

		int sfSize = discount.getStorefronts().size();
		if (sfSize > 0) {
			initStorefrontsView();
		} else {
			initStorefronts();
		}

		// 设置评论
		TextView commentCountView = (TextView) view
				.findViewById(R.id.t_comment_count);
		commentCountView.setText(discount.getCommentCount() + "条评论");
		if (discount.getCommentCount() == 0) {
			view.findViewById(R.id.row_show_all_comments).setVisibility(View.GONE);
		} else {
			if (discount.getComments().size() > 1) {
				initCommentsView();
			} else {
				AsyncProcessorTask<List<CommentEntity>> task = new AsyncProcessorTask<List<CommentEntity>>(
						getActivity(), new ProcessorTaskCallback<List<CommentEntity>>() {

							@Override
							public ProcessResult<List<CommentEntity>> doInBackground() {
								return ProcessorFactory.getDiscountProcessor()
										.getCommentsByDiscount(discount,
												KEY_DEF_DISCOUNT_COMMENTS_SHOW_NUM, 0);
							}

							@Override
							public void onSuccess(List<CommentEntity> result) {
								discount.setComments(result);
								initCommentsView();
							}
						});
				task.execute();
			}
		}
		imageViewPager = (ViewPager) view.findViewById(R.id.v_discount_img);
		imgDotViewGroup = (ViewGroup) view.findViewById(R.id.v_dot_layout);
		setImageViewPagerAdapter();

		imageViewPager.setFocusable(true);
		imageViewPager.setFocusableInTouchMode(true);
		imageViewPager.requestFocus();

		visitRecord();
	}

	private void initStorefronts() {
		AsyncProcessorTask<List<StorefrontEntity>> task = new AsyncProcessorTask<List<StorefrontEntity>>(
				getActivity(), new ProcessorTaskCallback<List<StorefrontEntity>>() {

					@Override
					public ProcessResult<List<StorefrontEntity>> doInBackground() {
						return ProcessorFactory.getDiscountProcessor()
								.getStoreFrontsByDiscount(discount,
										KEY_DEF_DISCOUNT_FRONTS_SHOW_NUM, 0);
					}

					@Override
					public void onSuccess(List<StorefrontEntity> result) {
						discount.setStorefronts(result);
						if (mainView != null) {
							initStorefrontsView();
						}
					}
				});
		task.execute();
	}

	/**
	 * 初始化门店视图
	 * 
	 * @param view
	 */
	private void initStorefrontsView() {
		int sfSize = discount.getStorefronts().size();
		final ListView storefrontsView = (ListView) mainView
				.findViewById(R.id.list_storefront);
		storefrontsView
				.setAdapter(new DiscountStorefrontsAdapter(
						getActivity(),
						discount
								.getStorefronts()
								.subList(
										0,
										(sfSize > KEY_DEF_DISCOUNT_FRONTS_SHOW_NUM ? KEY_DEF_DISCOUNT_FRONTS_SHOW_NUM
												: sfSize))));
		if (discount.getFrontCount() <= KEY_DEF_DISCOUNT_FRONTS_SHOW_NUM) {
			mainView.findViewById(R.id.row_show_all_fronts).setVisibility(View.GONE);
		} else {
			mainView.findViewById(R.id.row_show_all_fronts).setVisibility(
					View.VISIBLE);
			mainView.findViewById(R.id.t_front_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.t_show_all_fronts).setVisibility(View.VISIBLE);
			mainView.findViewById(R.id.img_show_all_fronts).setVisibility(
					View.VISIBLE);

			mainView.findViewById(R.id.t_show_all_fronts).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = new Intent(getActivity(),
									DiscountFrontsActivity.class);
							intent.putExtra(NAME_DISCOUNT, discount);
							startActivity(intent);
						}
					});
		}
	}

	/**
	 * 初始化评论视图
	 * 
	 * @param view
	 */
	private void initCommentsView() {
		int sfSize = discount.getComments().size();
		final ListView storefrontsView = (ListView) mainView
				.findViewById(R.id.list_comments);
		storefrontsView
				.setAdapter(new CommentsAdapter(
						getActivity(),
						discount
								.getComments()
								.subList(
										0,
										(sfSize > KEY_DEF_DISCOUNT_COMMENTS_SHOW_NUM ? KEY_DEF_DISCOUNT_COMMENTS_SHOW_NUM
												: sfSize))));
		if (discount.getCommentCount() <= KEY_DEF_DISCOUNT_COMMENTS_SHOW_NUM) {
			mainView.findViewById(R.id.row_show_all_comments)
					.setVisibility(View.GONE);
		} else {
			mainView.findViewById(R.id.row_show_all_comments).setVisibility(
					View.VISIBLE);
			mainView.findViewById(R.id.t_comment_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.t_show_all_comments).setVisibility(
					View.VISIBLE);
			mainView.findViewById(R.id.img_show_all_comments).setVisibility(
					View.VISIBLE);

			mainView.findViewById(R.id.t_show_all_comments).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							Intent intent = new Intent(getActivity(), CommentsActivity.class);
							intent.putExtra(NAME_DISCOUNT, discount);
							startActivity(intent);
						}
					});
		}
	}

	private void setImageViewPagerAdapter() {
		final List<FileEntity> images = discount.getImages();
		if (images != null && images.size() > 1) {
			imgDotViewGroup.removeAllViews();
			if (images != null && images.size() > 0) {
				for (int j = 0; j < images.size(); j++) {
					LinearLayout.LayoutParams margin = new LinearLayout.LayoutParams(
							LinearLayout.LayoutParams.WRAP_CONTENT,
							LinearLayout.LayoutParams.WRAP_CONTENT);
					margin.setMargins(10, 0, 0, 0);
					ImageView imageView = new ImageView(getActivity());
					if (j == 0) {
						imageView.setBackgroundResource(R.drawable.dot_focus);
					} else {
						imageView.setBackgroundResource(R.drawable.dot);
					}
					imgDotViewGroup.addView(imageView, margin);
				}
			}
		}

		FragmentManager fm = this.getChildFragmentManager();
		imageViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {

			@Override
			public int getCount() {
				return images == null ? 0 : images.size();
			}

			@Override
			public Fragment getItem(int pos) {
				ImagePreviewFragment fragment = ImagePreviewFragment.newInstance(
						images.get(pos), ScaleType.CENTER_CROP);
				fragment.setActionDelegate(actionDelegate);
				return fragment;
			}
		});

		imageViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int pos) {
				int count = imgDotViewGroup.getChildCount();
				for (int i = 0; i < count; i++) {
					ImageView imageView = (ImageView) imgDotViewGroup.getChildAt(i);
					if (i == pos) {
						imageView.setBackgroundResource(R.drawable.dot_focus);
					} else {
						imageView.setBackgroundResource(R.drawable.dot);
					}
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}

	private void shareToWeixin() {
		WeixinUtils.shareDiscountToWeixin(getActivity(), discount);
		sharePopupWindow.dismiss();
	}

	private void shareToFriends() {
		WeixinUtils.shareDiscountToFriends(getActivity(), discount);
		sharePopupWindow.dismiss();
	}

	private void shareToSina() {
		WeiboUtils.shareDiscount(getActivity(), discount);
	}

	private void visitRecord() {
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getDiscountProcessor().visitDiscount(
								discount);
					}

					@Override
					public void onSuccess(Boolean result) {
						// do nothing
					}

					@Override
					public void onFailed(Error error) {
						// do nothing
					}
				});
		task.execute();
	}

	private void favoritDiscount() {
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getDiscountProcessor().favoritDiscount(
								discount);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							unfavorited = false;
							actionMenuList.remove(0);
							actionMenuList.add(0, FuncConst.ACTION_MENU_UNFAVORIT);
							ViewUtils.showToast("收藏成功.");
						} else {
							ViewUtils.showToast("收藏失败.");
						}
					}
				});
		task.execute();
	}

	private void unfavoritDiscount() {
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getDiscountProcessor().unfavoritDiscount(
								discount);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							unfavorited = true;
							actionMenuList.remove(0);
							actionMenuList.add(0, FuncConst.ACTION_MENU_FAVORIT);
							ViewUtils.showToast("取消收藏成功.");
						} else {
							ViewUtils.showToast("取消收藏失败.");
						}
					}
				});
		task.execute();
	}

	private void faultDiscount() {
		if (faultDialog == null) {
			faultDialog = new Dialog(getActivity(), R.style.discount_fault_dialog) {
				private EditText commentField;

				@Override
				protected void onCreate(Bundle savedInstanceState) {
					super.onCreate(savedInstanceState);
					// setContentView(R.layout.pop_fault_discount);
					View view = LayoutInflater.from(getActivity()).inflate(
							R.layout.pop_fault_discount, null);
					Display d = getActivity().getWindowManager().getDefaultDisplay(); // 为获取屏幕宽、高
					LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
							(int) (d.getWidth() * 0.9),
							LinearLayout.LayoutParams.WRAP_CONTENT);
					setContentView(view, lp);

					commentField = (EditText) view.findViewById(R.id.e_comment);

					Button btnOk = (Button) findViewById(R.id.b_ok);
					btnOk.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							final String content = commentField.getText().toString();
							if (!TextUtils.isEmpty(content)) {
								AsyncProcessorTask<CommentEntity> task = new AsyncProcessorTask<CommentEntity>(
										getActivity(), new ProcessorTaskCallback<CommentEntity>() {

											@Override
											public ProcessResult<CommentEntity> doInBackground() {
												CommentEntity comment = new CommentEntity();
												comment.setDiscountId(discount.getUuid());
												comment.setContent(content);
												comment.setType(FuncConst.KEY_TYPE_DIS_FAULT);
												UserEntity user = RunEnv.getInstance().getUser();
												if (user != null) {
													comment.setPublisher(user.getUuid());
												} else {
													comment.setPublisher(ViewUtils.getDeviceId());
												}
												return ProcessorFactory.getDiscountProcessor()
														.publishComment(comment);
											}

											@Override
											public void onSuccess(CommentEntity result) {
												if (result != null) {
													commentField.setText("");
													faultDialog.dismiss();
													ViewUtils.showToast("纠错发送成功.");
												} else {
													ViewUtils.showToast("纠错发送失败.");
												}
											}
										});
								task.execute();
							}

						}
					});

					Button btnCancel = (Button) findViewById(R.id.b_cancel);
					btnCancel.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							faultDialog.dismiss();
						}
					});
				}
			};
		}
		faultDialog.show();
	}

	public void dismissSharePopupWindow() {
		if (sharePopupWindow != null) {
			sharePopupWindow.dismiss();
		}
	}

	public void addComment(CommentEntity comment) {
		discount.addComment(0, comment);
		initCommentsView();

		discount.setCommentCount(discount.getCommentCount() + 1);
		TextView commentCountView = (TextView) mainView
				.findViewById(R.id.t_comment_count);
		commentCountView.setText(discount.getCommentCount() + "条评论");
	}

	class DiscountImageViewClickDelegate implements ImageViewClickDelegate {

		@Override
		public void doClick(FileEntity imageFile) {
			ArrayList<FileEntity> images = (ArrayList<FileEntity>) discount
					.getImages();

			Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
			intent.putExtra(FuncConst.NAME_IMAGES, images);
			intent.putExtra(FuncConst.EXTRA_CURRENT_INDEX,
					imageViewPager.getCurrentItem());

			startActivity(intent);
		}

	}

	@Override
	public void doLocationValidate(LocationEntity loc) {
		if (discount != null) {
			initStorefronts();
		}
	}

	@Override
	protected void back() {
		if (unfavorited) {
			Intent intent = new Intent();
			intent.putExtra(NAME_DISCOUNT, discount);
			getActivity().setResult(Activity.RESULT_OK, intent);
		}
		super.back();
	}
}

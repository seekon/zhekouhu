package com.seekon.zhekouhu.activity.discount;

import static com.seekon.zhekouhu.func.FuncConst.NAME_DISCOUNT;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.DiscountStorefrontsAdapter;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;

public class DiscountFrontsFragment extends CustomActionBarListFragment
		implements IXListViewListener {

	private DiscountEntity discount;
	private int offset;
	private ArrayList<StorefrontEntity> dataList = new ArrayList<StorefrontEntity>();

	private Handler mHandler;
	private XListView xListView;
	private View mainView;

	public static DiscountFrontsFragment newInstance(DiscountEntity discount) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_DISCOUNT, discount);

		DiscountFrontsFragment fragment = new DiscountFrontsFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			discount = (DiscountEntity) args.getSerializable(NAME_DISCOUNT);
		}

		setRetainInstance(true);

		mHandler = new Handler();
		loadMoreData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.fragment_discount_storefronts,
				container, false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(final View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		String frontCountLabel = discount.getFrontCount() + "家门店";
		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location != null) {
			// frontCountLabel = location.getCityName() + frontCountLabel;
		}

		TextView countView = (TextView) mainView.findViewById(R.id.t_front_count);
		countView.setText(frontCountLabel);

		// updateRefreshTime();
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				view.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});
	}

	public void setAdapter() {
		AbstractListAdapter<StorefrontEntity> adapter = (AbstractListAdapter<StorefrontEntity>) getListAdapter();
		if (adapter == null) {
			setListAdapter(new DiscountStorefrontsAdapter(getActivity(), dataList));
		} else {
			adapter.updateData(dataList);
		}

		showDataView();
		updateFooter();
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateFooter();
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData();
				onPostLoad();
			}
		}, 2000);
	}

	private void loadMoreData() {

		AsyncProcessorTask<List<StorefrontEntity>> task = new AsyncProcessorTask<List<StorefrontEntity>>(
				getActivity(), new ProcessorTaskCallback<List<StorefrontEntity>>() {

					@Override
					public ProcessResult<List<StorefrontEntity>> doInBackground() {
						return ProcessorFactory.getDiscountProcessor()
								.getStoreFrontsByDiscount(discount, 10, offset);
					}

					@Override
					public void onSuccess(List<StorefrontEntity> result) {
						if (result != null && !result.isEmpty()) {
							offset += result.size();
							dataList.addAll(result);
							setAdapter();
						} else if (getListAdapter() == null) {
							setAdapter();
						}
					}

					@Override
					public void onFailed(Error error) {
						setAdapter();
						super.onFailed(error);
					}
				});
		task.execute();
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}

	private void updateFooter() {
		if (xListView != null) {
			if (dataList.isEmpty()) {
				xListView.getFooterView().setHintText("暂时没有数据");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setHintText(null);
				xListView.getFooterView().setEnabled(true);
			}
		}
	}
}

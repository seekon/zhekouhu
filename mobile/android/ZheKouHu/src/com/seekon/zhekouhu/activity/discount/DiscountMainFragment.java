package com.seekon.zhekouhu.activity.discount;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.LocationableListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.DiscountMainListAdapter;
import com.seekon.zhekouhu.func.store.CityEntity;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;
import com.seekon.zhekouhu.func.widget.XListViewFooter;

public class DiscountMainFragment extends LocationableListFragment implements
		IXListViewListener {

	private int offset;

	private ArrayList<Object> dataList = new ArrayList<Object>();

	private ArrayList<CategoryEntity> categories = new ArrayList<CategoryEntity>();

	private Handler mHandler;
	private XListView xListView;
	private View mainView;
	private boolean dataLoaded = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		dataList.add(getActivity().getString(R.string.label_newest_discounts));

		mHandler = new Handler();

		if (categories == null || categories.isEmpty()) {
			AsyncProcessorTask<List<CategoryEntity>> task = new AsyncProcessorTask<List<CategoryEntity>>(
					getActivity(), new ProcessorTaskCallback<List<CategoryEntity>>() {

						@Override
						public ProcessResult<List<CategoryEntity>> doInBackground() {
							return ProcessorFactory.getCategoryProcessor().categories();
						}

						@Override
						public void onSuccess(List<CategoryEntity> result) {
							categories = (ArrayList<CategoryEntity>) result;
							dataList.add(0, result);
							setAdapter();
						}

						@Override
						public void onFailed(Error error) {
							setAdapter();
							super.onFailed(error);
						}
					});
			task.execute();
		}

		AsyncProcessorTask<List<CityEntity>> task1 = new AsyncProcessorTask<List<CityEntity>>(
				getActivity(), new ProcessorTaskCallback<List<CityEntity>>() {

					@Override
					public ProcessResult<List<CityEntity>> doInBackground() {
						return ProcessorFactory.getStoreProcessor().cities();
					}

					@Override
					public void onSuccess(List<CityEntity> result) {
						// do nothing
					}

					@Override
					public void onFailed(Error error) {
						// super.onFailed(error);
					}
				});
		task1.execute();

		super.onCreate(savedInstanceState);
	}

	private void setAdapter() {
		AbstractListAdapter<Object> adapter = (AbstractListAdapter<Object>) getListAdapter();
		if (adapter == null) {
			setListAdapter(new DiscountMainListAdapter(getActivity(), dataList));
		} else {
			adapter.updateData(dataList);
		}
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.main, menu);
	//
	// MenuItem searchViewMenuItem = menu.findItem(R.id.action_search);
	// SearchView mSearchView = (SearchView) searchViewMenuItem.getActionView();
	// setSearchViewStyle(mSearchView);
	// }
	//
	// private void setSearchViewStyle(SearchView mSearchView) {
	// try {
	// int searchImgId = getResources().getIdentifier(
	// "android:id/search_button", null, null);
	// ImageView v = (ImageView) mSearchView.findViewById(searchImgId);
	// v.setImageResource(R.drawable.search);
	// v.setBackgroundResource(R.drawable.action_img_selector);
	//
	// int closeButtonId = getResources().getIdentifier(
	// "android:id/search_close_btn", null, null);
	// ImageView closeButtonImage = (ImageView) mSearchView
	// .findViewById(closeButtonId);
	// closeButtonImage.setImageResource(R.drawable.close);
	// closeButtonImage.setBackgroundResource(R.drawable.action_img_selector);
	//
	// int searchPlateId = getResources().getIdentifier(
	// "android:id/search_plate", null, null);
	// mSearchView.findViewById(searchPlateId).setBackgroundResource(
	// R.drawable.textfield_white);
	//
	// int queryTextViewId = getResources().getIdentifier(
	// "android:id/search_src_text", null, null);
	// TextView autoComplete = (TextView) mSearchView
	// .findViewById(queryTextViewId);
	// autoComplete.setTextColor(getResources().getColor(R.color.white));
	// try {
	// Field mCursorDrawableRes = TextView.class
	// .getDeclaredField("mCursorDrawableRes");
	// mCursorDrawableRes.setAccessible(true);
	// mCursorDrawableRes.set(autoComplete, 0);
	// } catch (Exception e) {
	// }
	//
	// Class<?> clazz = Class
	// .forName("android.widget.SearchView$SearchAutoComplete");
	//
	// SpannableStringBuilder stopHint = new SpannableStringBuilder("   ");
	// // stopHint.append(getString(R.string.your_new_text));
	//
	// // Add the icon as an spannable
	// Drawable searchIcon = getResources().getDrawable(R.drawable.search);
	// Method textSizeMethod = clazz.getMethod("getTextSize");
	// Float rawTextSize = (Float) textSizeMethod.invoke(autoComplete);
	// int textSize = (int) (rawTextSize * 1);
	// searchIcon.setBounds(0, 0, textSize, textSize);
	// stopHint.setSpan(new ImageSpan(searchIcon), 1, 2,
	// Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	//
	// // Set the new hint text
	// Method setHintMethod = clazz.getMethod("setHint", CharSequence.class);
	// setHintMethod.invoke(autoComplete, stopHint);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		updateActionBar();
		mainView = inflater.inflate(R.layout.base_loading_xlistview, container,
				false);
		initViews(mainView);
		return mainView;
	}

	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		View actionBarView = getActionBarView();
		ImageView logoTitleView = (ImageView) actionBarView
				.findViewById(R.id.img_logo_title);
		logoTitleView.setImageResource(R.drawable.logo_title);

		actionBarView.findViewById(R.id.img_home_up).setVisibility(View.GONE);

		ImageView actionImage = (ImageView) actionBarView
				.findViewById(R.id.img_action);
		actionImage.setImageResource(R.drawable.search);
		actionImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), SearchDiscountsActivity.class);
				if (RunEnv.getInstance().getLocation() != null) {
					intent.putExtra(FuncConst.KEY_LOCATION, RunEnv.getInstance()
							.getLocation());
				}
				startActivity(intent);
			}
		});
	}

	private void updateActionBar() {
		View actionBarView = getActionBarView();
		actionBarView.findViewById(R.id.img_logo_title).setVisibility(View.VISIBLE);
		actionBarView.findViewById(R.id.t_title).setVisibility(View.GONE);

		actionBarView.findViewById(R.id.img_action).setVisibility(View.VISIBLE);
		actionBarView.findViewById(R.id.t_action).setVisibility(View.GONE);
	}

	private void initViews(View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		// if (dataLoaded) {
		showDataView();
		// } else {
		// mHandler.post(new Runnable() {
		//
		// @Override
		// public void run() {
		// mainView.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
		// }
		// });
		// }
		if (!dataLoaded) {
			xListView.getFooterView().setState(XListViewFooter.STATE_LOADING);
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (dataList == null || dataList.isEmpty()) {
			return;
		}
		Object item = dataList.get(position - 1);
		if (item instanceof DiscountEntity) {
			Intent intent = new Intent(getActivity(), DiscountDetailActivity.class);
			intent.putExtra(FuncConst.NAME_DISCOUNT, (DiscountEntity) item);
			startActivity(intent);
		}
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			// updateRefreshTime();
		}
	}

	@Override
	public void onRefresh() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData(true);
			}
		}, 2000);
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData(false);
			}
		}, 2000);
	}

	private void loadMoreData(final boolean refresh) {
		AsyncProcessorTask<List<DiscountEntity>> task1 = new AsyncProcessorTask<List<DiscountEntity>>(
				getActivity(), new ProcessorTaskCallback<List<DiscountEntity>>() {

					@Override
					public ProcessResult<List<DiscountEntity>> doInBackground() {
						if (refresh) {
							return ProcessorFactory.getDiscountProcessor()
									.newestActivities(0);
						}
						return ProcessorFactory.getDiscountProcessor().newestActivities(
								offset);
					}

					@Override
					public void onSuccess(List<DiscountEntity> result) {
						if (refresh) {
							offset = 0;
							dataList.clear();
							if (!categories.isEmpty()) {
								dataList.add(categories);
							}
							dataList.add(getActivity().getString(
									R.string.label_newest_discounts));
						}
						if (result != null && !result.isEmpty()) {
							offset += result.size();
							dataList.addAll(result);
							setAdapter();
						}
						dataLoaded = true;
						onPostLoad();
						showDataView();
					}

					@Override
					public void onFailed(Error error) {
						dataLoaded = true;
						setAdapter();
						super.onFailed(error);
						onPostLoad();
						showDataView();
					}

				});
		task1.execute();
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void doLocationValidate(LocationEntity loc) {
		loadMoreData(true);
	}
}

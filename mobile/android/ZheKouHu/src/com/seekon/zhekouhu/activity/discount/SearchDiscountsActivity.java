package com.seekon.zhekouhu.activity.discount;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.func.FuncConst;

public class SearchDiscountsActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		Fragment fragment = new SearchDiscountsFragment();
		Bundle args = new Bundle();
		args.putBoolean(FuncConst.KEY_SAVE_FIELD_STATE, true);
		fragment.setArguments(args);
		return fragment;
	}

}

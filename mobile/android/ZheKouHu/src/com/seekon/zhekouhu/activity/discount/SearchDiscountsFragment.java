package com.seekon.zhekouhu.activity.discount;

import java.util.List;

import android.content.Intent;
import android.view.View;
import android.widget.ListView;

import com.seekon.zhekouhu.activity.SearchActionBarListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.DiscountMainListAdapter;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.ViewUtils;

public class SearchDiscountsFragment extends
		SearchActionBarListFragment<DiscountEntity> {

	private static final int DISCOUNT_REQUEST_CODE = 1;

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (dataList == null || dataList.isEmpty()) {
			return;
		}
		DiscountEntity item = dataList.get(position - 1);
		Intent intent = new Intent(getActivity(), DiscountDetailActivity.class);
		intent.putExtra(FuncConst.NAME_DISCOUNT, (DiscountEntity) item);
		startActivityForResult(intent, DISCOUNT_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DISCOUNT_REQUEST_CODE) {
			ViewUtils.hideInputMethodWindow(getActivity());
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void doSearch(final boolean newSearch) {
		AsyncProcessorTask<List<DiscountEntity>> task = new AsyncProcessorTask<List<DiscountEntity>>(
				getActivity(), new ProcessorTaskCallback<List<DiscountEntity>>() {

					@Override
					public ProcessResult<List<DiscountEntity>> doInBackground() {
						return ProcessorFactory.getDiscountProcessor().discountsByKeyword(
								searchWord, offset);
					}

					@Override
					public void onSuccess(List<DiscountEntity> result) {
						if (result != null) {
							offset += result.size();
							dataList.addAll(result);
						}
						setAdapter();
						if (newSearch) {
							showDataView();
						}
					}

					@Override
					public void onFailed(Error error) {
						if (newSearch) {
							showDataView();
						}
						super.onFailed(error);
					}

				});
		task.execute();
	}

	@Override
	protected AbstractListAdapter<DiscountEntity> createListAdapter() {
		return new DiscountMainListAdapter(getActivity(), dataList);
	}

	@Override
	protected String getSearchHit() {
		return "请输入店铺、活动";
	}
}

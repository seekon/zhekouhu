package com.seekon.zhekouhu.activity.more;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.FormFragmentActivity;

public class FeedbackActivity extends FormFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new FeedbackFragment();
	}

}

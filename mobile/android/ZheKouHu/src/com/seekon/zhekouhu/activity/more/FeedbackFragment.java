package com.seekon.zhekouhu.activity.more;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.LengthValidator;

public class FeedbackFragment extends CustomActionBarFragment {

	private EditText contentText;
	private EditText contactText;

	private ValidationManager mValidationManager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_feedback, container, false);
		initViews(view);

		return view;
	}

	private void initViews(View view) {
		contentText = (EditText) view.findViewById(R.id.e_content);
		contactText = (EditText) view.findViewById(R.id.e_contact);

		Button submitButton = (Button) view.findViewById(R.id.b_submit);
		submitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				submitFeedback();
			}
		});

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("content", new LengthValidator(contentText,
				"意见或建议内容在500字以内", 1, 500));
	}

	private void submitFeedback() {
		final String content = contentText.getText().toString();
		String contact = contactText.getText().toString();

		// contentText.setError(null);
		// View focusView = null;
		// boolean cancel = false;
		// if (TextUtils.isEmpty(content)) {
		// contentText.setError(getActivity().getString(
		// R.string.error_field_required, "意见或建议内容"));
		// cancel = true;
		// focusView = contentText;
		// }

		if (TextUtils.isEmpty(contact)) {
			UserEntity user = RunEnv.getInstance().getUser();
			if (user != null) {
				contact = user.getCode();
			}
		}

		boolean cancel = !mValidationManager.validateAllAndSetError();
		if (cancel) {
			// focusView.requestFocus();
			return;
		}

		ViewUtils.hideInputMethodWindow(getActivity());

		final String contact1 = contact;
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "提交中，请稍后...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getFeedbackProcessor().submit(content,
								contact1);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							ViewUtils.showToast("提交成功\n感谢您提出的宝贵意见和建议.");
							getActivity().finish();
						}
					}
				});
		task.execute();
	}
}

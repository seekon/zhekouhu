package com.seekon.zhekouhu.activity.more;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.func.FuncConst.NAME_MESSAGE;
import static com.seekon.zhekouhu.func.FuncConst.NAME_POP_TO_ROOT;
import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.func.message.MessageEntity;

public class MessageDetailActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		boolean popToRoot = getIntent().getBooleanExtra(NAME_POP_TO_ROOT, false);
		MessageEntity message = (MessageEntity) getIntent().getSerializableExtra(
				NAME_MESSAGE);
		if (message != null) {
			return MessageDetailFragment.newInstance(message, popToRoot);
		}

		String uuid = getIntent().getStringExtra(COL_NAME_UUID);
		if (uuid != null) {
			return MessageDetailFragment.newInstance(uuid, popToRoot);
		}

		return new MessageDetailFragment();
	}

}

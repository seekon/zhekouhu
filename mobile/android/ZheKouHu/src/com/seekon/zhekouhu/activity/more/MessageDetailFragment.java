package com.seekon.zhekouhu.activity.more;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.func.FuncConst.NAME_MESSAGE;
import static com.seekon.zhekouhu.func.FuncConst.NAME_POP_TO_ROOT;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.activity.MainActivity;
import com.seekon.zhekouhu.activity.discount.DiscountDetailActivity;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.message.MessageEntity;

public class MessageDetailFragment extends CustomActionBarFragment {
	private MessageEntity message;
	private boolean popToRoot;

	public static MessageDetailFragment newInstance(MessageEntity message,
			boolean popToRoot) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_MESSAGE, message);
		args.putBoolean(NAME_POP_TO_ROOT, popToRoot);

		MessageDetailFragment fragment = new MessageDetailFragment();
		fragment.setArguments(args);

		return fragment;
	}

	public static MessageDetailFragment newInstance(String uuid, boolean popToRoot) {
		Bundle args = new Bundle();
		args.putSerializable(COL_NAME_UUID, uuid);
		args.putBoolean(NAME_POP_TO_ROOT, popToRoot);

		MessageDetailFragment fragment = new MessageDetailFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		Bundle args = getArguments();
		if (args != null) {
			popToRoot = args.getBoolean(NAME_POP_TO_ROOT);
			message = (MessageEntity) args.getSerializable(NAME_MESSAGE);
			if (message == null) {
				String uuid = args.getString(COL_NAME_UUID);
				if (uuid != null) {
					message = ProcessorFactory.getMessageProcessor().message(uuid);
				}
			}
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_message_detail, container,
				false);
		initViews(view);
		return view;
	}

	private void initViews(View view) {
		if (message != null) {
			TextView content = (TextView) view.findViewById(R.id.t_content);
			content.setText(message.getContent());

			View linkDiscountView = view.findViewById(R.id.v_discount);
			if (message.getType().equals(FuncConst.MSG_TYPE_DISCOUNT)) {
				linkDiscountView.setVisibility(View.VISIBLE);
				view.findViewById(R.id.t_discount_detail).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								Intent intent = new Intent(getActivity(),
										DiscountDetailActivity.class);
								intent.putExtra(DataConst.COL_NAME_UUID, message.getMsgId());
								startActivity(intent);
							}
						});
			} else {
				linkDiscountView.setVisibility(View.GONE);
			}
		}
	}

	@Override
	protected void back() {
		if (popToRoot) {
			Intent intent = new Intent(getActivity(), MainActivity.class);
			startActivity(intent);
			getActivity().finish();
			return;
		}
		super.back();
	}

}

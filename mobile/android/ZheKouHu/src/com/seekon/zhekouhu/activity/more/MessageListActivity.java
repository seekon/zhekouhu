package com.seekon.zhekouhu.activity.more;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;

public class MessageListActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new MessageListFragment();
	}

}

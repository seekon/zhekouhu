package com.seekon.zhekouhu.activity.more;

import static com.seekon.zhekouhu.func.FuncConst.NAME_MESSAGE;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarListFragment;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.message.MessageEntity;
import com.seekon.zhekouhu.func.message.widget.MessageListAdapter;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.AlertPopupWindow;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;

public class MessageListFragment extends CustomActionBarListFragment implements
		IXListViewListener {

	private int offset;
	private ArrayList<MessageEntity> dataList = new ArrayList<MessageEntity>();

	private Handler mHandler;
	private XListView xListView;
	private View mainView;

	private PopupWindow alertPopupWindow;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mHandler = new Handler();
		loadMoreData();
	}

	private void updateActionBar() {
		ImageView actionImage = (ImageView) getActionBarView().findViewById(
				R.id.img_action);
		actionImage.setImageResource(R.drawable.clean);
		if (dataList.isEmpty()) {
			actionImage.setVisibility(View.GONE);
			return;
		}

		actionImage.setVisibility(View.VISIBLE);
		actionImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (alertPopupWindow == null) {
					alertPopupWindow = new AlertPopupWindow(getActivity(), null,
							"是否清空所有消息?", new OnClickListener() {

								@Override
								public void onClick(View v) {
									alertPopupWindow.dismiss();
									switch (v.getId()) {
									case R.id.t_no:
										break;
									case R.id.t_yes:
										clean();
										break;
									default:
										break;
									}
								}
							});
				}
				alertPopupWindow.showAtLocation(v, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.base_loading_xlistview, container,
				false);
		initView(mainView);
		return mainView;
	}

	private void initView(final View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		mHandler.post(new Runnable() {

			@Override
			public void run() {
				view.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});

		updateFooter();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (dataList == null || dataList.isEmpty()) {
			return;
		}
		MessageEntity message = dataList.get(position - 1);
		Intent intent = new Intent(getActivity(), MessageDetailActivity.class);
		intent.putExtra(NAME_MESSAGE, message);
		startActivity(intent);
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateFooter();
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData();
				onPostLoad();
			}
		}, 2000);
	}

	public void setAdapter() {
		AbstractListAdapter<MessageEntity> adapter = (AbstractListAdapter<MessageEntity>) getListAdapter();
		if (adapter == null) {
			setListAdapter(new MessageListAdapter(getActivity(), dataList));
		} else {
			adapter.updateData(dataList);
		}
		showDataView();
		updateActionBar();
		updateFooter();
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}

	private void updateFooter() {
		if (xListView != null) {
			if (dataList.isEmpty()) {
				xListView.getFooterView().setHintText("还没有收到任何消息");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setHintText(null);
				xListView.getFooterView().setEnabled(true);
			}
		}
	}

	private void loadMoreData() {
		AsyncTask<Void, Void, List<MessageEntity>> task = new AsyncTask<Void, Void, List<MessageEntity>>() {

			@Override
			protected List<MessageEntity> doInBackground(Void... params) {
				return ProcessorFactory.getMessageProcessor().messages(offset);
			}

			@Override
			protected void onPostExecute(List<MessageEntity> result) {
				offset += result.size();
				dataList.addAll(result);
				setAdapter();
			}
		};

		task.execute((Void) null);
	}

	private void clean() {
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				ProcessorFactory.getMessageProcessor().clean();
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				offset = 0;
				dataList.clear();
				setAdapter();
				super.onPostExecute(result);
			}
		};

		task.execute((Void) null);
	}
}

package com.seekon.zhekouhu.activity.more;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.AbstractMoreFragment;
import com.seekon.zhekouhu.activity.store.StoreMainActivity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessorFactory;

public class MoreMainFragment extends AbstractMoreFragment {

	public static final int STORE_REQUEST_CODE = 1;

	private View storeMager;

	@Override
	protected int getContentViewResourceId() {
		return R.layout.fragment_more_main;
	}

	@Override
	protected void updateActionBar() {
		View actionBarView = getActionBarView();

		actionBarView.findViewById(R.id.img_logo_title).setVisibility(View.GONE);
		actionBarView.findViewById(R.id.t_title).setVisibility(View.VISIBLE);
		setTitle(R.string.title_more);

		actionBarView.findViewById(R.id.img_action).setVisibility(View.GONE);
		actionBarView.findViewById(R.id.t_action).setVisibility(View.GONE);
	}

	@Override
	protected void initViews(View view) {
		super.initViews(view);

		((TextView) view.findViewById(R.id.t_store))
				.setText(R.string.label_store_manager);

		storeMager = view.findViewById(R.id.row_store_manager);
		storeMager.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), StoreMainActivity.class);
				intent.putExtra(FuncConst.EXTRA_FLAG_HOME_AS_UP, true);
				startActivityForResult(intent, STORE_REQUEST_CODE);
			}
		});

		boolean storeRegistered = ProcessorFactory.getSettingProcessor()
				.isStoreRegistered();
		if (storeRegistered) {
			storeMager.setVisibility(View.GONE);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		switch (requestCode) {
		case STORE_REQUEST_CODE:
			boolean storeRegistered = ProcessorFactory.getSettingProcessor()
					.isStoreRegistered();
			if (storeRegistered) {
				storeMager.setVisibility(View.GONE);
			}
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}

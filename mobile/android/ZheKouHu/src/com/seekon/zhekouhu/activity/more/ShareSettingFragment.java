package com.seekon.zhekouhu.activity.more;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarListFragment;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.more.widget.ShareSettingAdapter;
import com.seekon.zhekouhu.func.setting.SettingEntity;
import com.seekon.zhekouhu.func.share.sina.AccessTokenKeeper;
import com.seekon.zhekouhu.func.share.sina.WBConst;
import com.seekon.zhekouhu.func.widget.AlertPopupWindow;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuth;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;

public class ShareSettingFragment extends CustomActionBarListFragment {

	private ArrayList<SettingEntity> shareSettings;
	private ShareSettingAdapter adapter;

	private PopupWindow alertPopupWindow;

	/** 注意：SsoHandler 仅当 SDK 支持 SSO 时有效 */
	private SsoHandler mSsoHandler;
	/** 微博 Web 授权类，提供登陆等功能 */
	private WeiboAuth mWeiboAuth;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		shareSettings = (ArrayList<SettingEntity>) ProcessorFactory
				.getSettingProcessor().getShareSettingList();
		for (SettingEntity setting : shareSettings) {
			try {
				JSONObject value = new JSONObject(setting.getValue());
				if ("sina".equalsIgnoreCase(value.getString(DataConst.COL_NAME_CODE))) {
					Oauth2AccessToken mAccessToken = AccessTokenKeeper
							.readAccessToken(getActivity());
					if (mAccessToken.isSessionValid()) {
						value.put(DataConst.COL_NAME_STATUS, "1");
						setting.setValue(value.toString());
						ProcessorFactory.getSettingProcessor().saveSetting(setting);
						setting.setValue(value.toString());
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		adapter = new ShareSettingAdapter(getActivity(), shareSettings);
		setListAdapter(adapter);

		mWeiboAuth = new WeiboAuth(getActivity(), WBConst.APP_KEY,
				WBConst.REDIRECT_URL, WBConst.SCOPE);
		mSsoHandler = new SsoHandler(getActivity(), mWeiboAuth);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		final SettingEntity setting = shareSettings.get(position);
		try {
			JSONObject value = new JSONObject(setting.getValue());
			String status = value.getString(DataConst.COL_NAME_STATUS);
			if (!status.equals("0")) {
				// 取消绑定
				if (alertPopupWindow == null) {
					alertPopupWindow = new AlertPopupWindow(getActivity(), null,
							"是否取消绑定?", new OnClickListener() {

								@Override
								public void onClick(View v) {
									alertPopupWindow.dismiss();
									switch (v.getId()) {
									case R.id.t_no:
										break;
									case R.id.t_yes:
										unSetShare(setting);
										break;
									default:
										break;
									}
								}
							});
				}
				alertPopupWindow.showAtLocation(v, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
			} else {
				String code = value.getString(DataConst.COL_NAME_CODE);
				if ("sina".equalsIgnoreCase(code)) {
					setSinaShare(setting);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		super.onListItemClick(l, v, position, id);
	}

	private void setSinaShare(final SettingEntity setting) {
		mSsoHandler.authorize(new WeiboAuthListener() {

			@Override
			public void onWeiboException(WeiboException e) {
				Toast.makeText(getActivity(), "认证异常 : " + e.getMessage(),
						Toast.LENGTH_LONG).show();
			}

			@Override
			public void onComplete(Bundle values) {
				Oauth2AccessToken mAccessToken = Oauth2AccessToken
						.parseAccessToken(values);

				if (mAccessToken.isSessionValid()) {

					try {
						JSONObject value = new JSONObject(setting.getValue());
						value.put(DataConst.COL_NAME_STATUS, "1");
						setting.setValue(value.toString());
						ProcessorFactory.getSettingProcessor().saveSetting(setting);

						updateEntity(setting);
					} catch (JSONException e) {
						e.printStackTrace();
					}

					AccessTokenKeeper.writeAccessToken(getActivity(), mAccessToken);
				} else {
				}
			}

			@Override
			public void onCancel() {
				ViewUtils.showToast("Oauth2AccessToken:onCancel");
			}
		});
	}

	private void unSetShare(SettingEntity setting) {
		try {
			JSONObject value = new JSONObject(setting.getValue());
			value.put(DataConst.COL_NAME_STATUS, "0");
			setting.setValue(value.toString());
			ProcessorFactory.getSettingProcessor().saveSetting(setting);

			if (value.getString(DataConst.COL_NAME_CODE).equalsIgnoreCase("sina")) {
				AccessTokenKeeper.clear(getActivity());
			}

			updateEntity(setting);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (mSsoHandler != null) {
			mSsoHandler.authorizeCallBack(requestCode, resultCode, data);
		}
	}

	public void updateEntity(SettingEntity setting) {
		boolean replaced = false;
		for (int i = 0; i < shareSettings.size(); i++) {
			SettingEntity settingData = shareSettings.get(i);
			if (settingData.equals(setting)) {
				shareSettings.remove(i);
				shareSettings.add(i, setting);
				replaced = true;
				break;
			}
		}
		if (!replaced) {
			shareSettings.add(setting);
		}

		adapter.updateData(shareSettings);
	}
}

package com.seekon.zhekouhu.activity.more;

import static com.seekon.zhekouhu.func.FuncConst.NAME_POP_TO_ROOT;
import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;

public class VersionActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		boolean popToRoot = getIntent().getBooleanExtra(NAME_POP_TO_ROOT, false);
		return VersionFragment.newInstance(popToRoot);
	}

}

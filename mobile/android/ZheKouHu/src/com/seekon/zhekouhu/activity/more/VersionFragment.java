package com.seekon.zhekouhu.activity.more;

import static com.seekon.zhekouhu.func.FuncConst.NAME_POP_TO_ROOT;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.activity.MainActivity;
import com.seekon.zhekouhu.func.version.UpdateManager;

public class VersionFragment extends CustomActionBarFragment {

	private boolean popToRoot;

	public static VersionFragment newInstance(boolean popToRoot) {
		Bundle args = new Bundle();
		args.putBoolean(NAME_POP_TO_ROOT, popToRoot);

		VersionFragment fragment = new VersionFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		if (args != null) {
			popToRoot = args.getBoolean(NAME_POP_TO_ROOT);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_version, container, false);
		initViews(view);
		return view;
	}

	private void initViews(View view) {
		view.findViewById(R.id.b_update).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new UpdateManager(getActivity()).update();
			}
		});
	}

	@Override
	protected void back() {
		if (popToRoot) {
			Intent intent = new Intent(getActivity(), MainActivity.class);
			startActivity(intent);
			getActivity().finish();
			return;
		}
		super.back();
	}

}

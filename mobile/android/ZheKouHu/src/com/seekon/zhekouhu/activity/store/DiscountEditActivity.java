package com.seekon.zhekouhu.activity.store;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.KeyboardFragmentActivity;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.discount.DiscountEntity;

public class DiscountEditActivity extends KeyboardFragmentActivity {

	@Override
	protected Fragment createFragment() {
		String type = getIntent().getStringExtra(DataConst.COL_NAME_TYPE);
		DiscountEntity discount = (DiscountEntity) getIntent()
				.getSerializableExtra(FuncConst.NAME_DISCOUNT);
		if (discount != null) {
			return DiscountEditFragment.newInstance(discount);
		} else if (type != null) {
			return DiscountEditFragment.newInstance(type);
		}
		return new DiscountEditFragment();
	}

}

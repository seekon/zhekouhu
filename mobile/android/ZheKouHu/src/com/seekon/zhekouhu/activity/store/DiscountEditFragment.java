package com.seekon.zhekouhu.activity.store;

import static com.seekon.zhekouhu.func.FuncConst.EXTRA_CURRENT_INDEX;
import static com.seekon.zhekouhu.func.FuncConst.EXTRA_FLAG_READONLY;
import static com.seekon.zhekouhu.func.FuncConst.NAME_IMAGES;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.activity.ImagePreviewActivity;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.DiscountImagesAdapter;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.store.widget.StorefrontListAdapter;
import com.seekon.zhekouhu.func.widget.AlertPopupWindow;
import com.seekon.zhekouhu.func.widget.SelectCategoryPopupWindow;
import com.seekon.zhekouhu.func.widget.SelectDatePopupWindow;
import com.seekon.zhekouhu.func.widget.SelectPicturePopupWindow;
import com.seekon.zhekouhu.func.widget.SelectStorefrontPopupWindow;
import com.seekon.zhekouhu.utils.DateUtils;
import com.seekon.zhekouhu.utils.Logger;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.ErrorAbleImageView;
import com.seekon.zhekouhu.validation.ErrorAbleTextView;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.LengthValidator;

public class DiscountEditFragment extends CustomActionBarFragment {

	private static final String TAG = DiscountEditFragment.class.getSimpleName();

	private static final int IMAGE_PREVIEW_REQUEST_CODE = 3;

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

	private static final int LOAD_IMAGE_ACTIVITY_REQUEST_CODE = 200;

	private String currentAddedFileUri = null;

	private PopupWindow picPopupWindow;
	private PopupWindow catePopupWindow;
	private PopupWindow storefrontPopupWindow;
	private PopupWindow startDatePopupWindow;
	private PopupWindow endDatePopupWindow;
	private PopupWindow alertPopupWindow;

	private ArrayList<StoreEntity> storeList;
	private ArrayList<CategoryEntity> categoryList;
	private String type;
	private DiscountEntity discount;

	private EditText contentText;
	private EditText parValueText;
	private ErrorAbleTextView startDateText;
	private ErrorAbleTextView endDateText;

	private ErrorAbleTextView categoryView;
	private ErrorAbleTextView storefrontTitleView;

	private StorefrontListAdapter storefrontListAdapter;
	private DiscountImagesAdapter imagesAdapter;
	private GridView imagesView;

	private ArrayList<FileEntity> addedImages = new ArrayList<FileEntity>();
	private ArrayList<FileEntity> deletedImages = new ArrayList<FileEntity>();

	private ValidationManager mValidationManager;

	private View mainView;

	public static DiscountEditFragment newInstance(DiscountEntity discount) {
		Bundle args = new Bundle();
		args.putSerializable(FuncConst.NAME_DISCOUNT, discount);

		DiscountEditFragment fragment = new DiscountEditFragment();
		fragment.setArguments(args);

		return fragment;
	}

	public static DiscountEditFragment newInstance(String type) {
		Bundle args = new Bundle();
		args.putString(DataConst.COL_NAME_TYPE, type);

		DiscountEditFragment fragment = new DiscountEditFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		Bundle args = getArguments();
		if (args != null) {
			discount = (DiscountEntity) args.getSerializable(FuncConst.NAME_DISCOUNT);
			if (discount != null) {
				type = discount.getType();
			} else {
				type = args.getString(DataConst.COL_NAME_TYPE);
			}
		}

		if (type == null) {
			type = FuncConst.VAL_DISCOUNT_TYPE_ALL;
		}

		int title = 0;
		if (discount != null) {
			if (type.equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
				title = R.string.title_edit_discount;
			} else if (type.equals(FuncConst.VAL_DISCOUNT_TYPE_COUPON)) {
				title = R.string.title_edit_coupon;
			} else {
				title = R.string.title_store_edit_discount;
			}
		} else {
			discount = new DiscountEntity();
			if (type.equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
				title = R.string.title_publish_discount;
			} else if (type.equals(FuncConst.VAL_DISCOUNT_TYPE_COUPON)) {
				title = R.string.title_publish_coupon;
			} else {
				title = R.string.title_store_pub_discount;
			}
		}

		getActivity().getActionBar().setTitle(title);

		super.onCreate(savedInstanceState);
	}

	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		if (discount != null && discount.getUuid() != null) {
			View actionBarView = getActionBarView();
			ImageView actionImage = (ImageView) actionBarView
					.findViewById(R.id.img_action);
			actionImage.setImageResource(R.drawable.delete);
			actionImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (alertPopupWindow == null) {
						alertPopupWindow = new AlertPopupWindow(getActivity(), null,
								"是否删除活动?", new OnClickListener() {

									@Override
									public void onClick(View v) {
										alertPopupWindow.dismiss();
										switch (v.getId()) {
										case R.id.t_no:
											break;
										case R.id.t_yes:
											delete();
											break;
										default:
											break;
										}
									}
								});
					}
					alertPopupWindow.showAtLocation(v, Gravity.BOTTOM
							| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
				}
			});
		}
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.discount_edit, menu);
	// }
	//
	// @Override
	// public void onPrepareOptionsMenu(Menu menu) {
	// if (discount == null || discount.getUuid() == null) {
	// menu.findItem(R.id.action_delete).setVisible(false);
	// } else {
	// menu.findItem(R.id.action_delete).setVisible(true);
	// }
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.action_delete:
	// delete();
	// return true;
	//
	// default:
	//
	// return super.onOptionsItemSelected(item);
	// }
	// }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.fragment_discount_edit, container,
				false);
		initView(mainView);
		return mainView;
	}

	private void initView(View view) {
		contentText = (EditText) view.findViewById(R.id.e_content);
		parValueText = (EditText) view.findViewById(R.id.e_par_value);
		startDateText = (ErrorAbleTextView) view.findViewById(R.id.e_start_date);
		endDateText = (ErrorAbleTextView) view.findViewById(R.id.e_end_date);

		categoryView = (ErrorAbleTextView) view.findViewById(R.id.t_category);
		storefrontTitleView = (ErrorAbleTextView) view
				.findViewById(R.id.t_title_storefronts);
		if (type.equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
			view.findViewById(R.id.row_par_value).setVisibility(View.GONE);
		}

		ViewUtils.setEditTextReadOnly(startDateText);
		startDateText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Date date = null;
				if (TextUtils.isEmpty(startDateText.getText().toString())) {
					date = new Date(System.currentTimeMillis());
				} else {
					date = DateUtils.getDate_yyyyMMdd(startDateText.getText().toString());
				}

				if (startDatePopupWindow == null) {
					startDatePopupWindow = new SelectDatePopupWindow(getActivity(), date,
							new SelectDatePopupWindow.PopupWindowCallback() {

								@Override
								public void doCallback(Date date) {
									startDateText.setText(DateUtils.getDateString_yyyyMMdd(date));
									startDateText.hideError();
								}
							});
				}
				startDatePopupWindow.showAtLocation(v, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, 0);
			}
		});

		ViewUtils.setEditTextReadOnly(endDateText);
		endDateText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Date date = null;
				if (TextUtils.isEmpty(endDateText.getText().toString())) {
					date = new Date(System.currentTimeMillis());
				} else {
					date = DateUtils.getDate_yyyyMMdd(endDateText.getText().toString());
				}

				if (endDatePopupWindow == null) {
					endDatePopupWindow = new SelectDatePopupWindow(getActivity(), date,
							new SelectDatePopupWindow.PopupWindowCallback() {

								@Override
								public void doCallback(Date date) {
									endDateText.setText(DateUtils.getDateString_yyyyMMdd(date));
									endDateText.hideError();
								}
							});
				}
				endDatePopupWindow.showAtLocation(v, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, 0);
			}
		});

		View rowCategoryView = view.findViewById(R.id.row_category);
		rowCategoryView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				if (catePopupWindow == null) {
					AsyncProcessorTask<List<CategoryEntity>> task = new AsyncProcessorTask<List<CategoryEntity>>(
							getActivity(), "正在加载，请稍后...",
							new ProcessorTaskCallback<List<CategoryEntity>>() {

								@Override
								public ProcessResult<List<CategoryEntity>> doInBackground() {
									return ProcessorFactory.getCategoryProcessor().categories();
								}

								@Override
								public void onSuccess(List<CategoryEntity> result) {
									categoryList = (ArrayList<CategoryEntity>) result;
									initAndShowChooseCategoryWindow(v);
								}

								@Override
								public void onFailed(Error error) {
									categoryList = null;
									initAndShowChooseCategoryWindow(v);
								}
							});
					task.execute();
				} else {
					catePopupWindow.showAtLocation(v, Gravity.BOTTOM
							| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
				}
			}
		});

		View addStorefrontView = view.findViewById(R.id.row_add_storefront);
		addStorefrontView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				if (storefrontPopupWindow == null) {
					AsyncProcessorTask<List<StoreEntity>> task = new AsyncProcessorTask<List<StoreEntity>>(
							getActivity(), "正在加载，请稍后...",
							new ProcessorTaskCallback<List<StoreEntity>>() {

								@Override
								public ProcessResult<List<StoreEntity>> doInBackground() {
									return ProcessorFactory.getStoreProcessor().storesForOwner(
											RunEnv.getInstance().getUser().getUuid());
								}

								@Override
								public void onSuccess(List<StoreEntity> result) {
									storeList = (ArrayList<StoreEntity>) result;
									initAndShowChooseStorefrontWindow(v);
								}

								@Override
								public void onFailed(Error error) {
									super.onFailed(error);
									initAndShowChooseStorefrontWindow(v);
								}
							});

					task.execute();
				} else {
					storefrontPopupWindow.showAtLocation(v, Gravity.BOTTOM
							| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
				}

			}
		});

		imagesView = (GridView) view.findViewById(R.id.gridView);

		ListView storefrontsView = (ListView) view
				.findViewById(R.id.list_storefront);

		storefrontListAdapter = new StorefrontListAdapter(getActivity(),
				discount.getStorefronts(), true, true);
		storefrontsView.setAdapter(storefrontListAdapter);

		updateImageView(discount.getImages().size() + 1);
		imagesAdapter = new DiscountImagesAdapter(getActivity(),
				discount.getImages());
		imagesView.setAdapter(imagesAdapter);

		imagesView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				if (position == imagesAdapter.getCount() - 1) {
					showPopupWindow(view);
				} else {
					Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
					ArrayList<FileEntity> images = new ArrayList<FileEntity>(addedImages);
					images.addAll(discount.getImages());
					intent.putExtra(NAME_IMAGES, images);
					intent.putExtra(EXTRA_CURRENT_INDEX, position);
					intent.putExtra(EXTRA_FLAG_READONLY, false);
					startActivityForResult(intent, IMAGE_PREVIEW_REQUEST_CODE);
				}
			}
		});

		Button publishButton = (Button) view.findViewById(R.id.b_publish);
		String status = discount.getStatus();
		if (status != null
				&& (status.equals(FuncConst.VAL_DISCOUNT_STATUS_INVALID) || status
						.equals(FuncConst.VAL_DISCOUNT_STATUS_VALID))) {
			publishButton.setEnabled(false);
		} else {
			publishButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					publish();
				}
			});
		}

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("content", new LengthValidator(contentText,
				"活动必须140字以内", 1, 140));
		if (type.equals(FuncConst.VAL_DISCOUNT_TYPE_COUPON)) {
			mValidationManager.add("parvalue", new LengthValidator(parValueText,
					"面值不能为空", 1, Integer.MAX_VALUE));
		}
		mValidationManager.add("startDate", new LengthValidator(startDateText,
				"请选择开始日期", 1, Integer.MAX_VALUE));
		mValidationManager.add("endDate", new LengthValidator(endDateText,
				"请选择结束日期", 1, Integer.MAX_VALUE));
		mValidationManager.add("category", new LengthValidator(categoryView,
				"请选择分类", 1, Integer.MAX_VALUE));

		initViewData();
	}

	private void initViewData() {
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				if (discount.getUuid() != null) {
					contentText.setText(discount.getContent());
					parValueText.setText(discount.getParValue() + "");
					startDateText.setText(DateUtils.getDateString_yyyyMMdd(discount
							.getStartDate()));
					endDateText.setText(DateUtils.getDateString_yyyyMMdd(discount
							.getEndDate()));
					categoryView.setText(discount.getCategory().getName());
					storefrontTitleView.setText(getActivity().getString(
							R.string.label_fit_storefronts)
							+ "(" + discount.getStorefronts().size() + "家)");
				} else {
					contentText.setText("");
					parValueText.setText("");
					startDateText.setText("");
					endDateText.setText("");
					categoryView.setText("");
					storefrontTitleView.setText(getActivity().getString(
							R.string.label_fit_storefronts));
				}
			}
		});
	}

	private void initAndShowChooseCategoryWindow(View view) {
		catePopupWindow = new SelectCategoryPopupWindow(getActivity(),
				discount.getCategory(), categoryList, new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						catePopupWindow.dismiss();
						categoryView.setText(categoryList.get(position).getName());
						discount.setCategory(categoryList.get(position));

						categoryView.hideError();
					}
				});
		catePopupWindow.showAtLocation(view, Gravity.BOTTOM
				| Gravity.CENTER_HORIZONTAL, 0, 0);
	}

	private void initAndShowChooseStorefrontWindow(View view) {
		storefrontPopupWindow = new SelectStorefrontPopupWindow(getActivity(),
				discount.getStorefronts(), storeList,
				new SelectStorefrontPopupWindow.PopupWindowCallback() {

					@Override
					public void doCallback(List<StorefrontEntity> selectedDataList) {
						if (selectedDataList != null) {
							discount.setStorefronts(selectedDataList);

							storefrontTitleView.setText(getActivity().getString(
									R.string.label_fit_storefronts)
									+ "(" + discount.getStorefronts().size() + "家)");
							storefrontListAdapter.updateData(selectedDataList);
							if (discount.getStorefronts().size() > 0) {
								storefrontTitleView.hideError();
							}
						}
					}
				});
		storefrontPopupWindow.showAtLocation(view, Gravity.BOTTOM
				| Gravity.CENTER_HORIZONTAL, 0, 0);
	}

	private void updateImageView(int numColumns) {
		if (numColumns > 1) {
			View view = imagesView.getChildAt(0);
			if (view != null) {
				((ErrorAbleImageView) view.findViewById(R.id.img_discount)).hideError();
			}
		}
		int width = (int) (getActivity().getResources().getDisplayMetrics().density * (65 + 18));
		LayoutParams params = new LayoutParams(numColumns * width,
				LayoutParams.WRAP_CONTENT);
		imagesView.setLayoutParams(params);
		imagesView.setNumColumns(numColumns);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		switch (requestCode) {
		case IMAGE_PREVIEW_REQUEST_CODE:
			if (data != null) {
				List<FileEntity> delImages = (List<FileEntity>) data
						.getSerializableExtra(FuncConst.NAME_DEL_IMAGES);
				for (FileEntity file : delImages) {
					if (addedImages.contains(file)) {
						addedImages.remove(file);
					} else if (discount.getImages().contains(file)) {
						discount.getImages().remove(file);
						if (!deletedImages.contains(file)) {
							deletedImages.add(file);
						}
					}
				}
				List<FileEntity> images = new ArrayList<FileEntity>(addedImages);
				images.addAll(discount.getImages());
				updateImageView(images.size() + 1);
				imagesAdapter.updateData(images);
			}
			break;
		case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
			updateDiscountImages();
			break;
		case LOAD_IMAGE_ACTIVITY_REQUEST_CODE:
			if (null != data) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = null;
				try {
					cursor = getActivity().getContentResolver().query(selectedImage,
							filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					currentAddedFileUri = cursor.getString(columnIndex);
					updateDiscountImages();
				} finally {
					cursor.close();
				}
			}
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 更新图片列表
	 */
	private void updateDiscountImages() {
		String name = RunEnv.getInstance().getUser().getCode() + "_discount_"
				+ System.currentTimeMillis() + ".png";
		addedImages.add(new FileEntity(currentAddedFileUri, name));
		List<FileEntity> images = new ArrayList<FileEntity>(addedImages);
		images.addAll(discount.getImages());
		updateImageView(images.size() + 1);
		imagesAdapter.updateData(images);
	}

	/**
	 * 弹出图片选择框
	 * 
	 * @param v
	 */
	private void showPopupWindow(View v) {
		if (picPopupWindow == null) {
			picPopupWindow = new SelectPicturePopupWindow(getActivity(),
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							picPopupWindow.dismiss();
							switch (v.getId()) {
							case R.id.v_camera:
								openCamera();
								break;
							case R.id.v_photo:
								openImageDir();
								break;
							default:
								break;
							}
						}
					});
		}
		picPopupWindow.showAtLocation(v,
				Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
	}

	private void openCamera() {
		Logger.debug(TAG, "openCamera");
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = FileHelper
				.getFileFromCache(System.currentTimeMillis() + ".png");
		currentAddedFileUri = file.getPath();
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

		startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	private void openImageDir() {
		Logger.debug(TAG, "openImageDir");
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, LOAD_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	private void delete() {
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在删除，请稍后...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getDiscountProcessor().deleteDiscount(
								discount);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							back();
						} else {
							ViewUtils.showToast("删除失败.");
						}
					}
				});
		task.execute();
	}

	private void publish() {
		String content = contentText.getText().toString();
		String startDate = startDateText.getText().toString();
		String endDate = endDateText.getText().toString();
		String parValue = parValueText.getText().toString();

		// TODO检查数据的完整性
		boolean cancel = !mValidationManager.validateAllAndSetError();

		if (addedImages.isEmpty() && (discount.getImages().isEmpty())) {
			View view = imagesView.getChildAt(0);
			((ErrorAbleImageView) view.findViewById(R.id.img_discount))
					.betterSetError("请至少添加一张活动图片.", !cancel);

			cancel = true;
		}

		if (discount.getStorefronts() == null
				|| discount.getStorefronts().isEmpty()) {
			storefrontTitleView.betterSetError("请选择适用门店", !cancel);
			cancel = true;
		}

		if (cancel) {
			return;
		}

		if (DateUtils.getDate_yyyyMMdd(startDate).getTime() > DateUtils
				.getDate_yyyyMMdd(endDate).getTime()) {
			endDateText.betterSetError("结束日期必须在开始日期之后", true);
			return;
		}

		discount.setContent(content);
		discount.setParValue(TextUtils.isEmpty(parValue) ? 0 : Double
				.valueOf(parValue));
		discount.setStartDate(DateUtils.getDate_yyyyMMdd(startDate).getTime());
		discount.setEndDate(DateUtils.getDate_yyyyMMdd(endDate).getTime());
		discount.setPublisher(RunEnv.getInstance().getUser().getUuid());
		if (discount.getParValue() > 0) {
			discount.setType(FuncConst.VAL_DISCOUNT_TYPE_COUPON);
		} else {
			discount.setType(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY);
		}

		if (discount.getUuid() != null) {// 更新数据
			AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
					getActivity(), "正在发布，请稍后...", new ProcessorTaskCallback<Boolean>() {

						@Override
						public ProcessResult<Boolean> doInBackground() {
							return ProcessorFactory.getDiscountProcessor().updateDiscount(
									discount, deletedImages, addedImages);
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								ViewUtils.showToast("发布成功.");
							} else {
								ViewUtils.showToast("发布失败.");
							}
						}
					});
			task.execute();
		} else {
			discount.setOrigin(FuncConst.VAL_DISCOUNT_ORIGIN_STORER);
			discount.setImages(addedImages);
			AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
					getActivity(), "正在发布，请稍后...", new ProcessorTaskCallback<Boolean>() {

						@Override
						public ProcessResult<Boolean> doInBackground() {
							return ProcessorFactory.getDiscountProcessor().publishDiscount(
									discount);
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								ViewUtils.showToast("发布成功.");
								clearData();
								initView(mainView);
							} else {
								ViewUtils.showToast("发布失败.");
							}
						}
					});
			task.execute();
		}
	}

	protected void clearData() {
		discount = new DiscountEntity();
		addedImages = new ArrayList<FileEntity>();
		deletedImages = new ArrayList<FileEntity>();

		mValidationManager = null;
		catePopupWindow = null;
		storefrontPopupWindow = null;
		startDatePopupWindow = null;
		endDatePopupWindow = null;
		storefrontListAdapter = null;
		imagesAdapter = null;

	}

	@Override
	protected void back() {
		getActivity().setResult(Activity.RESULT_OK);
		super.back();
	}
}

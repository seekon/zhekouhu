package com.seekon.zhekouhu.activity.store;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;

public class NearbyStoresActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new NearbyStoresFragment();
	}

}

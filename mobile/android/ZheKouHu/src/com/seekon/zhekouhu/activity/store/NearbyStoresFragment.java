package com.seekon.zhekouhu.activity.store;

import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.activity.SearchActionBarListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.store.NearbyStoreEntity;
import com.seekon.zhekouhu.func.store.widget.NearbyStoreListAdapter;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.LocationUtils;
import com.seekon.zhekouhu.utils.ViewUtils;

public class NearbyStoresFragment extends
		SearchActionBarListFragment<NearbyStoreEntity> {

	private BroadcastReceiver locationReceiver;
	private LocationEntity loc;

	private static int DEF_RADIUS = 1000;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (RunEnv.getInstance().getLocation() != null) {
			loc = RunEnv.getInstance().getLocation();
			loadMoreData(true);
		} else {
			LocationUtils.startLocate();
		}

		locationReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				LocationEntity location = RunEnv.getInstance().getLocation();

				if (loc == null || (location != null && !loc.equals(location))) {
					loc = (LocationEntity) intent
							.getSerializableExtra(Const.DATA_BROAD_LOCATION);
					loadMoreData(true);
				}
			}
		};

		getActivity().registerReceiver(locationReceiver,
				new IntentFilter(Const.KEY_BROAD_LOCATION));
	}

	@Override
	public void onDestroy() {
		getActivity().unregisterReceiver(locationReceiver);
		super.onDestroy();
	}

	@Override
	protected void initViews(View view) {
		super.initViews(view);
		hideDataView();
	}

	@Override
	protected void doSearch(final boolean newSearch) {
		AsyncProcessorTask<List<NearbyStoreEntity>> task = new AsyncProcessorTask<List<NearbyStoreEntity>>(
				getActivity(), new ProcessorTaskCallback<List<NearbyStoreEntity>>() {

					@Override
					public ProcessResult<List<NearbyStoreEntity>> doInBackground() {
						return ProcessorFactory.getStoreProcessor().getNearbyStores(
								searchWord, DEF_RADIUS, offset);
					}

					@Override
					public void onSuccess(List<NearbyStoreEntity> result) {
						if (result != null) {
							offset += result.size();
							dataList.addAll(result);
							setAdapter();
						}

						if (newSearch) {
							showDataView();
						}
					}

					@Override
					public void onFailed(Error error) {
						if (newSearch) {
							showDataView();
						}
						super.onFailed(error);
					}
				});

		task.execute();
	}

	@Override
	protected AbstractListAdapter<NearbyStoreEntity> createListAdapter() {
		return new NearbyStoreListAdapter(getActivity(), dataList);
	}

	@Override
	protected String getSearchHit() {
		return "请输入商家、地址";
	}

	@Override
	protected void loadMoreData(boolean newSearch) {
		if (newSearch) {
			hideDataView();
			offset = 0;
			dataList.clear();
			setAdapter();
		}
		if (offset > 0 && offset < Const.PAGE_SIZE) {
			return;
		}

		doSearch(newSearch);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		if (dataList == null || dataList.isEmpty()) {
			return;
		}

		ViewUtils.hideInputMethodWindow(getActivity());
		NearbyStoreEntity item = dataList.get(position - 1);
		Intent intent = new Intent();
		intent.putExtra(FuncConst.NAME_NEABY_STORE, item);
		getActivity().setResult(Activity.RESULT_OK, intent);
		getActivity().finish();
	}
	
}

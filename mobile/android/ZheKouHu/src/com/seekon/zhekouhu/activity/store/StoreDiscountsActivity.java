package com.seekon.zhekouhu.activity.store;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.db.DataConst;

public class StoreDiscountsActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		String type = getIntent().getStringExtra(DataConst.COL_NAME_TYPE);
		if (type == null) {
			return new StoreDiscountsFragment();
		}
		return StoreDiscountsFragment.newInstance(type);
	}

}

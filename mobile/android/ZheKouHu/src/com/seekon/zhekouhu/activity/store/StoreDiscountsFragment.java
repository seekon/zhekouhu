package com.seekon.zhekouhu.activity.store;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_TYPE;
import static com.seekon.zhekouhu.func.FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY;
import static com.seekon.zhekouhu.func.FuncConst.VAL_DISCOUNT_TYPE_ALL;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarListFragment;
import com.seekon.zhekouhu.activity.user.UserEditDiscountActivity;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.DateIndexedEntity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.store.widget.StoreDiscountListAdapter;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;

public class StoreDiscountsFragment extends CustomActionBarListFragment
		implements IXListViewListener {

	private static final int PUBLISH_DISCOUNT_REQUEST_CODE = 1;

	private static final int EDIT_DISCOUNT_REQUEST_CODE = 2;

	private ArrayList<DateIndexedEntity> dataList = new ArrayList<DateIndexedEntity>();

	private String type;

	private int offset = 0;

	private Handler mHandler;
	private XListView xListView;

	private View mainView;

	public static StoreDiscountsFragment newInstance(String type) {
		Bundle args = new Bundle();
		args.putString(COL_NAME_TYPE, type);

		StoreDiscountsFragment fragment = new StoreDiscountsFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		Bundle args = getArguments();
		if (args != null) {
			type = args.getString(COL_NAME_TYPE);
		}

		if (type == null) {
			type = VAL_DISCOUNT_TYPE_ACTIVITY;
		}

		if (type.equals(VAL_DISCOUNT_TYPE_ALL)) {
			getActivity().getActionBar().setTitle(R.string.label_my_pub_discounts);
		} else if (type.equals(VAL_DISCOUNT_TYPE_ACTIVITY)) {
			getActivity().getActionBar().setTitle(R.string.label_store_discounts);
		} else {
			getActivity().getActionBar().setTitle(R.string.label_store_coupons);
		}

		super.onCreate(savedInstanceState);

		mHandler = new Handler();
		refreshData(true);
	}

	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		View actionBarView = getActionBarView();
		ImageView actionImage = (ImageView) actionBarView
				.findViewById(R.id.img_action);
		actionImage.setImageResource(R.drawable.publish);
		actionImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				publish();
			}
		});
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.store_discounts, menu);
	// MenuItem item = menu.findItem(R.id.action_publish);
	// if (type == null || type.equals(VAL_DISCOUNT_TYPE_ACTIVITY)) {
	// item.setTitle(R.string.action_publish_discount);
	// } else {
	// item.setTitle(R.string.action_publish_coupon);
	// }
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.action_publish:
	// publish();
	// return true;
	//
	// default:
	// return super.onOptionsItemSelected(item);
	// }
	//
	// }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		switch (requestCode) {
		case PUBLISH_DISCOUNT_REQUEST_CODE:
			refreshData(true);
			return;
		case EDIT_DISCOUNT_REQUEST_CODE:
			refreshData(true);
			return;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mainView = inflater.inflate(R.layout.base_loading_xlistview, container,
				false);
		initViews(mainView);

		return mainView;
	}

	private void initViews(final View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		new Handler().post(new Runnable() {

			@Override
			public void run() {
				view.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});

		// updateRefreshTime();
	}

	@Override
	protected void back() {
		getActivity().setResult(Activity.RESULT_OK);
		super.back();
	}

	private void publish() {
		Intent intent = null;
		String userType = RunEnv.getInstance().getUser().getType();
		if (userType.equals(FuncConst.VAL_USER_TYPE_STORER)) {
			intent = new Intent(getActivity(), DiscountEditActivity.class);
			intent.putExtra(COL_NAME_TYPE, type);
		}else{
			intent = new Intent(getActivity(), UserEditDiscountActivity.class);
		}
		
		startActivityForResult(intent, PUBLISH_DISCOUNT_REQUEST_CODE);
	}

	private void refreshData(final boolean refresh) {

		AsyncProcessorTask<List<DateIndexedEntity>> task = new AsyncProcessorTask<List<DateIndexedEntity>>(
				getActivity(), new ProcessorTaskCallback<List<DateIndexedEntity>>() {

					@Override
					public ProcessResult<List<DateIndexedEntity>> doInBackground() {
						int vOffset = 0;
						if (!refresh) {
							vOffset = offset;
						}
						UserEntity user = RunEnv.getInstance().getUser();
						// if (type.equals(VAL_DISCOUNT_TYPE_ACTIVITY)) {
						// return ProcessorFactory.getDiscountProcessor()
						// .activitiesGroupByPublishDate(user.getUuid(), vOffset);
						// } else {
						// return ProcessorFactory.getDiscountProcessor()
						// .couponsGroupByPublishDate(user.getUuid(), vOffset);
						// }
						return ProcessorFactory.getDiscountProcessor()
								.discountsGroupByPublishDate(user.getUuid(), vOffset);
					}

					@Override
					public void onSuccess(List<DateIndexedEntity> result) {
						if (refresh) {
							offset = 0;
							dataList.clear();
						}
						if (result != null && !result.isEmpty()) {
							offset += result.size();
							dataList.addAll(result);
						} 
						setAdapter();
					}
				});
		task.execute();
	}

	private void setAdapter() {
		StoreDiscountListAdapter adapter = (StoreDiscountListAdapter) getListAdapter();
		if (adapter == null) {
			setListAdapter(new StoreDiscountListAdapter(this, dataList,
					EDIT_DISCOUNT_REQUEST_CODE));
		} else {
			adapter.updateData(dataList);
		}
		showDataView();
	}

	// private void updateRefreshTime() {
	// SyncEntity sync = ProcessorFactory.getSyncProcessor()
	// .discountSyncEntityByOwner(RunEnv.getInstance().getUser().getUuid(),
	// type);
	// String updateTime = null;
	// if (sync != null) {
	// updateTime = sync.getUpdateTime();
	// }
	// if (updateTime != null) {
	// try {
	// xListView
	// .setRefreshTime(DateUtils.formartTime(Long.valueOf(updateTime)));
	// } catch (Exception e) {
	// }
	// }
	// }

//	private void refreshDataFromRemote() {
//
//		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
//				getActivity(), new ProcessorTaskCallback<Boolean>() {
//
//					@Override
//					public ProcessResult<Boolean> doInBackground() {
//						return ProcessorFactory.getDiscountProcessor()
//								.discountsRemoteRefresh(
//										RunEnv.getInstance().getUser().getUuid(), type);
//					}
//
//					@Override
//					public void onSuccess(Boolean result) {
//						if (result) {// 存在数据
//							refreshData(true);
//						}else{
//							setAdapter();
//						}
//						onPostLoad();
//					}
//
//					@Override
//					public void onFailed(Error error) {
//						super.onFailed(error);
//						onPostLoad();
//						setAdapter();
//					}
//
//					@Override
//					public void onCancelled() {
//						super.onCancelled();
//						onPostLoad();
//					}
//
//				});
//		task.execute();
//	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			// updateRefreshTime();
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
//		mHandler.postDelayed(new Runnable() {
//
//			@Override
//			public void run() {
//				refreshDataFromRemote();
//			}
//		}, 2000);
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				refreshData(false);
				onPostLoad();
			}
		}, 2000);
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}
}

package com.seekon.zhekouhu.activity.store;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.KeyboardFragmentActivity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.store.StoreEntity;

public class StoreEditActivity extends KeyboardFragmentActivity {

	@Override
	protected Fragment createFragment() {
		StoreEntity store = (StoreEntity) getIntent().getSerializableExtra(
				FuncConst.NAME_STORE);
		if (store == null) {
			return new StoreEditFragment();
		}
		return StoreEditFragment.newInstance(store);
	}

}

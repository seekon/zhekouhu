package com.seekon.zhekouhu.activity.store;

import static com.seekon.zhekouhu.func.FuncConst.EXTRA_CURRENT_INDEX;
import static com.seekon.zhekouhu.func.FuncConst.NAME_IMAGES;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STORE;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STOREFRONTS;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.spi.IStoreProcessor;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.store.widget.StorefrontListAdapter;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.AlertPopupWindow;
import com.seekon.zhekouhu.func.widget.SelectPicturePopupWindow;
import com.seekon.zhekouhu.utils.Logger;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.ErrorAbleImageView;
import com.seekon.zhekouhu.validation.ErrorAbleTextView;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.LengthValidator;

public class StoreEditFragment extends CustomActionBarFragment {

	private static final String TAG = StoreEditFragment.class.getSimpleName();

	private static final int STOREFRONT_REQUEST_CODE = 400;

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

	private static final int LOAD_IMAGE_ACTIVITY_REQUEST_CODE = 200;

	private static final int PREVIEW_IMAGE_ACTIVITY_REQUEST_CODE = 300;

	private String selectedLogoFileUri = null;// 当前选择的logo图片文件存放的路径

	private PopupWindow popupWindow;

	private PopupWindow alertPopupWindow;

	private StoreEntity store;

	private boolean actionOk = false;

	private EditText nameText;
	private ImageView logoView;
	private View lineEnd;
	private TextView frontCountView;
	private AbstractListAdapter<StorefrontEntity> storefrontListAdapter;

	private ValidationManager mValidationManager;

	public static StoreEditFragment newInstance(StoreEntity store) {
		Bundle args = new Bundle();
		args.putSerializable(NAME_STORE, store);

		StoreEditFragment fragment = new StoreEditFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Bundle args = getArguments();
		if (args != null) {
			store = (StoreEntity) args.getSerializable(NAME_STORE);
		}

		if (store == null) {
			getActivity().getActionBar().setTitle(R.string.label_new_store);
			store = new StoreEntity();
		} else {
			getActivity().getActionBar().setTitle(R.string.title_edit_store);
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		View actionBarView = getActionBarView();
		ImageView actionImage = (ImageView) actionBarView
				.findViewById(R.id.img_action);
		actionImage.setImageResource(R.drawable.delete);
		actionImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (alertPopupWindow == null) {
					alertPopupWindow = new AlertPopupWindow(getActivity(), null,
							"是否删除店铺?", new OnClickListener() {

								@Override
								public void onClick(View v) {
									alertPopupWindow.dismiss();
									switch (v.getId()) {
									case R.id.t_no:
										break;
									case R.id.t_yes:
										deleteStore();
										break;
									default:
										break;
									}
								}
							});
				}
				alertPopupWindow.showAtLocation(v, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
			}
		});

		updateActionBar();
	}

	private void updateActionBar() {
		ImageView actionImage = (ImageView) getActionBarView().findViewById(
				R.id.img_action);
		if (store != null && store.getUuid() != null) {
			actionImage.setVisibility(View.VISIBLE);
		} else {
			actionImage.setVisibility(View.GONE);
		}
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.store_edit, menu);
	// }
	//
	// @Override
	// public void onPrepareOptionsMenu(Menu menu) {
	// super.onPrepareOptionsMenu(menu);
	// if (store != null && store.getUuid() != null) {
	// menu.findItem(R.id.action_delete).setVisible(true);
	// } else {
	// menu.findItem(R.id.action_delete).setVisible(false);
	// }
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.action_delete:
	// deleteStore();
	// return true;
	// default:
	// return super.onOptionsItemSelected(item);
	// }
	//
	// }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_store_edit, container, false);
		initViews(view);
		return view;
	}

	private void initViews(View view) {
		nameText = (EditText) view.findViewById(R.id.e_name);
		nameText.setText(store.getName());
		nameText.clearFocus();

		logoView = (ImageView) view.findViewById(R.id.img_logo);
		logoView.setAdjustViewBounds(false);
		logoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		logoView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				logoImageClicked();
			}
		});

		FileEntity logo = store.getLogo();
		if (logo != null) {
			if (logo.getAliasName() != null) {
				ImageLoader.getInstance().displayImage(logo.getAliasName(), logoView,
						true);
			}
		}

		View addFrontView = view.findViewById(R.id.row_storefront);
		addFrontView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), StorefrontEditActivity.class);
				intent.putExtra(NAME_STORE, store);
				startActivityForResult(intent, STOREFRONT_REQUEST_CODE);
			}
		});

		storefrontListAdapter = new StorefrontListAdapter(getActivity(),
				store.getStorefronts(), true, false);
		ListView storefrontListView = (ListView) view
				.findViewById(R.id.list_storefront);
		storefrontListView.setAdapter(storefrontListAdapter);
		storefrontListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				Intent intent = new Intent(getActivity(), StorefrontEditActivity.class);
				intent.putExtra(NAME_STORE, store);
				intent.putExtra(EXTRA_CURRENT_INDEX, pos);
				startActivityForResult(intent, STOREFRONT_REQUEST_CODE);
			}
		});

		Button saveButton = (Button) view.findViewById(R.id.b_save);
		saveButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveStore();
			}
		});

		frontCountView = (TextView) view.findViewById(R.id.t_storefront_count);
		lineEnd = view.findViewById(R.id.line_end);
		if (store.getStorefronts() != null && store.getStorefronts().size() > 0) {
			lineEnd.setVisibility(View.VISIBLE);
			frontCountView.setText("门店(" + store.getStorefronts().size() + "家)");
		} else {
			lineEnd.setVisibility(View.GONE);
			frontCountView.setText("添加门店");
		}

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("name", new LengthValidator(nameText, "名称必须100字以内",
				1, 100));
	}

	@Override
	public void onStart() {
		super.onStart();
		// if (selectedLogoFileUri != null) {
		// showLogo();
		// }else if (store.getLogo() != null) {
		// ImageLoader.getInstance().displayImage(store.getLogo().getAliasName(),
		// logoView, true);
		// }else if(logoView.getDrawable() == null){
		//
		// }
	}

	@Override
	public void onStop() {
		super.onStop();

		// ViewUtils.cleanImageView(logoView);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		switch (requestCode) {
		case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
			showLogo();
			break;
		case LOAD_IMAGE_ACTIVITY_REQUEST_CODE:
			if (null != data) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = null;
				try {
					cursor = getActivity().getContentResolver().query(selectedImage,
							filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					selectedLogoFileUri = cursor.getString(columnIndex);
					showLogo();
				} finally {
					cursor.close();
				}
			}
			break;
		case PREVIEW_IMAGE_ACTIVITY_REQUEST_CODE:
			if (data != null) {
				List<FileEntity> files = (List<FileEntity>) data
						.getSerializableExtra(NAME_IMAGES);
				if (files.size() == 0) {
					selectedLogoFileUri = null;
					ViewUtils.cleanImageView(logoView);

					logoView.setImageResource(R.drawable.add_image);
				}
			}
			break;
		case STOREFRONT_REQUEST_CODE:
			if (data != null) {
				ArrayList<StorefrontEntity> fronts = (ArrayList<StorefrontEntity>) data
						.getSerializableExtra(NAME_STOREFRONTS);
				store.setStorefronts(fronts);
				if (store.getStorefronts() != null && store.getStorefronts().size() > 0) {
					lineEnd.setVisibility(View.VISIBLE);
					frontCountView.setText("门店(" + store.getStorefronts().size() + "家)");

					((ErrorAbleTextView) frontCountView).hideError();
				} else {
					lineEnd.setVisibility(View.GONE);
					frontCountView.setText("添加门店");
				}
				storefrontListAdapter.updateData(fronts);
			}
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void logoImageClicked() {
		showPopupWindow(this.logoView);
	}

	private void showLogo() {
		if (selectedLogoFileUri != null || store.getLogo() != null) {
			((ErrorAbleImageView) logoView).hideError();
			ViewUtils.cleanImageView(logoView);
			logoView.setImageBitmap(FileHelper.decodeFile(selectedLogoFileUri, false,
					0, 0));
		}
	}

	private void showPopupWindow(View v) {
		if (popupWindow == null) {
			popupWindow = new SelectPicturePopupWindow(getActivity(),
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							popupWindow.dismiss();
							switch (v.getId()) {
							case R.id.v_camera:
								openCamera();
								break;
							case R.id.v_photo:
								openImageDir();
								break;
							default:
								break;
							}
						}
					});
		}
		popupWindow.showAtLocation(v, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
				0, 0); // 设置layout在PopupWindow中显示的位置
	}

	private void openCamera() {
		Logger.debug(TAG, "openCamera");
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = FileHelper
				.getFileFromCache(System.currentTimeMillis() + ".png");
		selectedLogoFileUri = file.getPath();
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

		startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	private void openImageDir() {
		Logger.debug(TAG, "openImageDir");
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, LOAD_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	/**
	 * 删除店铺
	 */
	private void deleteStore() {
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在删除店铺...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getStoreProcessor().deleteStore(store);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							ViewUtils.showToast("删除店铺成功.");
							actionOk = true;

							Intent intent = new Intent();
							intent.putExtra(NAME_STORE, store);
							intent.putExtra(FuncConst.NAME_IS_DELETED, true);

							getActivity().setResult(Activity.RESULT_OK, intent);
							getActivity().finish();
						} else {
							ViewUtils.showToast("删除店铺失败.");
						}
					}
				});
		task.execute();
	}

	/**
	 * 保存店铺
	 */
	private void saveStore() {
		ViewUtils.hideInputMethodWindow(getActivity());

		String name = nameText.getText().toString();

		boolean cancel = !mValidationManager.validateAllAndSetError();

		if (selectedLogoFileUri == null && store.getLogo() == null) {
			((ErrorAbleImageView) logoView).betterSetError("请设置商标logo", !cancel);
			cancel = true;
		}

		if (store.getStorefronts() == null || store.getStorefronts().size() == 0) {
			((ErrorAbleTextView) frontCountView).betterSetError("请添加门店", !cancel);
			cancel = true;
		}

		if (cancel) {
			return;
		}

		UserEntity user = RunEnv.getInstance().getUser();
		store.setName(name);
		store.setOwner(user.getUuid());
		if (selectedLogoFileUri != null) {
			store.setLogo(new FileEntity(selectedLogoFileUri, user.getCode()
					+ "_store_" + System.currentTimeMillis() + ".png"));
		}

		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在保存店铺...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						IStoreProcessor processor = ProcessorFactory.getStoreProcessor();
						if (store.getUuid() != null) {
							return processor.updateStore(store);
						}
						return processor.registerStore(store);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							ViewUtils.showToast("保存店铺成功.");
							// back();
							// getActivity().invalidateOptionsMenu();
							updateActionBar();
							actionOk = true;
						} else {
							ViewUtils.showToast("保存店铺失败.");
						}
					}
				});
		task.execute();
	}

	@Override
	protected void back() {
		if (actionOk) {
			Intent intent = new Intent();
			intent.putExtra(NAME_STORE, store);
			getActivity().setResult(Activity.RESULT_OK, intent);
		}
		super.back();
	}

}

package com.seekon.zhekouhu.activity.store;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.func.FuncConst;

public class StoreMainActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		boolean homeAsUp = getIntent().getBooleanExtra(
				FuncConst.EXTRA_FLAG_HOME_AS_UP, false);
		return StoreMainFragment.newInstance(homeAsUp);
	}
}

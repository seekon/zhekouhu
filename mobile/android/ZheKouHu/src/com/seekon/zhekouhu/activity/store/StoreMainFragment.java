package com.seekon.zhekouhu.activity.store;

import static com.seekon.zhekouhu.func.FuncConst.EXTRA_FLAG_HOME_AS_UP;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.AbstractPersonalMainFragment;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.user.UserEntity;

public class StoreMainFragment extends AbstractPersonalMainFragment {

	private static final int SETTING_REQUEST_CODE = 21;

	public static StoreMainFragment newInstance(Boolean homeAsUp) {
		Bundle args = new Bundle();
		args.putBoolean(EXTRA_FLAG_HOME_AS_UP, homeAsUp);

		StoreMainFragment fragment = new StoreMainFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	protected int getContentViewResourceId() {
		return R.layout.fragment_store_main;
	}

	protected void updateDiscountCountView(boolean logined) {
		if (logined) {
			UserEntity user = RunEnv.getInstance().getUser();
			TextView discountCountView = (TextView) mainView
					.findViewById(R.id.t_discount_count);
			discountCountView.setText(ProcessorFactory.getDiscountProcessor()
					.activityCountForOwner(user.getUuid()) + "笔");
			TextView couponCountView = (TextView) mainView
					.findViewById(R.id.t_coupon_count);
			couponCountView.setText(ProcessorFactory.getDiscountProcessor()
					.couponCountForOwner(user.getUuid()) + "张");
		} else {
			TextView discountCountView = (TextView) mainView
					.findViewById(R.id.t_discount_count);
			discountCountView.setText("");
			TextView couponCountView = (TextView) mainView
					.findViewById(R.id.t_coupon_count);
			couponCountView.setText("");
		}
	}

	@Override
	protected void updateActionBar() {
		super.updateActionBar();

		setTitle(R.string.title_store);

		View actionBarView = getActionBarView();
		TextView actionView = (TextView) actionBarView.findViewById(R.id.t_action);
		String actionText = actionView.getText().toString();
		if (RunEnv.getInstance().getUser() != null
				&& !actionText.equals(getActivity().getResources().getString(
						R.string.action_setting))) {
			actionView.setVisibility(View.VISIBLE);
			actionView.setText(R.string.action_setting);
			actionView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), StoreSettingActivity.class);
					startActivityForResult(intent, SETTING_REQUEST_CODE);
				}
			});
		}
	}

	@Override
	protected void updateFuncView(final boolean isLogined) {
		super.updateFuncView(isLogined);

		((TextView) mainView.findViewById(R.id.t_discount))
				.setText(R.string.label_store_discounts);
		((TextView) mainView.findViewById(R.id.t_coupon))
				.setText(R.string.label_store_coupons);
		((TextView) mainView.findViewById(R.id.t_store))
				.setText(R.string.label_new_store);

		Button publishDiscount = (Button) mainView
				.findViewById(R.id.b_publish_discount);

		publishDiscount.setEnabled(true);
		publishDiscount.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined) {
					openLoginActivity();
					return;
				}
				if (storeList == null || storeList.isEmpty()) {
					openStoreEditActivity();
					return;
				}

				Intent intent = new Intent(getActivity(), DiscountEditActivity.class);
				intent.putExtra(DataConst.COL_NAME_TYPE,
						FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY);
				startActivityForResult(intent, DISCOUNT_REQUEST_CODE);
			}
		});

		Button publishCoupon = (Button) mainView
				.findViewById(R.id.b_publish_coupon);

		publishCoupon.setEnabled(true);
		publishCoupon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined) {
					openLoginActivity();
					return;
				}
				if (storeList == null || storeList.isEmpty()) {
					openStoreEditActivity();
					return;
				}

				Intent intent = new Intent(getActivity(), DiscountEditActivity.class);
				intent.putExtra(DataConst.COL_NAME_TYPE,
						FuncConst.VAL_DISCOUNT_TYPE_COUPON);
				startActivityForResult(intent, DISCOUNT_REQUEST_CODE);
			}
		});

		View discountRow = mainView.findViewById(R.id.row_discount);

		discountRow.setEnabled(true);
		discountRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined) {
					openLoginActivity();
					return;
				}
				if (storeList == null || storeList.isEmpty()) {
					openStoreEditActivity();
					return;
				}

				Intent intent = new Intent(getActivity(), StoreDiscountsActivity.class);
				intent.putExtra(DataConst.COL_NAME_TYPE,
						FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY);
				startActivityForResult(intent, DISCOUNT_LIST_REQUEST_CODE);
			}
		});

		View couponRow = mainView.findViewById(R.id.row_coupon);

		couponRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined) {
					openLoginActivity();
					return;
				}
				if (storeList == null || storeList.isEmpty()) {
					openStoreEditActivity();
					return;
				}

				Intent intent = new Intent(getActivity(), StoreDiscountsActivity.class);
				intent.putExtra(DataConst.COL_NAME_TYPE,
						FuncConst.VAL_DISCOUNT_TYPE_COUPON);
				startActivityForResult(intent, DISCOUNT_LIST_REQUEST_CODE);
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		switch (requestCode) {
		case SETTING_REQUEST_CODE:
			this.updateViews();
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}

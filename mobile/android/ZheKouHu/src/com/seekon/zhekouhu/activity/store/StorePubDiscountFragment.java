package com.seekon.zhekouhu.activity.store;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seekon.zhekouhu.R;

public class StorePubDiscountFragment extends DiscountEditFragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		clearData();
		updateActionBar();
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	private void updateActionBar() {
		View actionBarView = getActionBarView();

		actionBarView.findViewById(R.id.img_logo_title).setVisibility(View.GONE);
		actionBarView.findViewById(R.id.t_title).setVisibility(View.VISIBLE);
		setTitle(R.string.title_store_pub_discount);

		actionBarView.findViewById(R.id.img_action).setVisibility(View.GONE);
		actionBarView.findViewById(R.id.t_action).setVisibility(View.GONE);
	}
}

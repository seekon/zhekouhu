package com.seekon.zhekouhu.activity.store;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarListFragment;
import com.seekon.zhekouhu.activity.WebViewActivity;
import com.seekon.zhekouhu.activity.user.ChangePwdActivity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.store.widget.StoreSettingAdapter;
import com.seekon.zhekouhu.utils.ViewUtils;

public class StoreSettingFragment extends CustomActionBarListFragment {

	private int logoutCount = 0;

	private static ArrayList<String> itemList = new ArrayList<String>();
	static {
		itemList.add("退出店铺");
		itemList.add("修改密码");
		itemList.add("帮助");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setListAdapter(new StoreSettingAdapter(getActivity(), itemList));
	}

	@Override
	public void onResume() {
		logoutCount = 0;
		super.onResume();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		switch (position) {
		case 0:
			this.logoutStore();
			break;
		case 1:
			this.changeUserPwd();
			break;
		case 2:
			this.help();
			break;
		default:
			super.onListItemClick(l, v, position, id);
			break;
		}

	}

	private void logoutStore() {
		if (logoutCount == 0) {
			logoutCount++;
			ViewUtils.showToast(
					getActivity().getResources().getString(R.string.msg_logout_conf), 50);
			return;
		}
		boolean result = ProcessorFactory.getUserProcessor().logout();
		if (result) {
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
		}
	}

	private void changeUserPwd() {
		Intent intent = new Intent(getActivity(), ChangePwdActivity.class);
		getActivity().startActivity(intent);
	}

	private void help() {
		Intent intent = new Intent(getActivity(), WebViewActivity.class);
		intent.putExtra(FuncConst.EXTRA_TITLE,
				getActivity().getString(R.string.title_store_help));
		intent.putExtra(FuncConst.EXTRA_URI, "/shop_help.html");
		getActivity().startActivity(intent);
	}
}

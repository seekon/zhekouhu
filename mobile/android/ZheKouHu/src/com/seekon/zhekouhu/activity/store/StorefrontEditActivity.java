package com.seekon.zhekouhu.activity.store;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.store.StoreEntity;

public class StorefrontEditActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		StoreEntity store = (StoreEntity) getIntent().getSerializableExtra(
				FuncConst.NAME_STORE);
		if (store == null) {
			return new StorefrontEditFragment();
		}
		int index = getIntent().getIntExtra(FuncConst.EXTRA_CURRENT_INDEX, -1);
		return StorefrontEditFragment.newInstance(store, index);
	}

}

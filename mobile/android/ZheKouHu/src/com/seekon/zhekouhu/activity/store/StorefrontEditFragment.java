package com.seekon.zhekouhu.activity.store;

import static com.seekon.zhekouhu.func.FuncConst.NAME_STOREFRONTS;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.spi.IStoreProcessor;
import com.seekon.zhekouhu.func.store.CityEntity;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.widget.AlertPopupWindow;
import com.seekon.zhekouhu.func.widget.SelectCityPopupWindow;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.LengthValidator;

public class StorefrontEditFragment extends CustomActionBarFragment {

	private EditText nameText;
	private EditText addrText;
	private EditText phoneText;
	private TextView cityText;

	private StoreEntity store;
	private int currentIndex = -1;
	private StorefrontEntity storefront;

	private PopupWindow cityPopupWindow;
	private ArrayList<CityEntity> cities;

	private PopupWindow alertPopupWindow;

	private ValidationManager mValidationManager;

	private OnClickListener chooseCityListener = new OnClickListener() {

		@Override
		public void onClick(final View v) {
			if (cityPopupWindow == null) {
				AsyncProcessorTask<List<CityEntity>> task = new AsyncProcessorTask<List<CityEntity>>(
						getActivity(), "正在加载，请稍候...",
						new ProcessorTaskCallback<List<CityEntity>>() {

							@Override
							public ProcessResult<List<CityEntity>> doInBackground() {
								return ProcessorFactory.getStoreProcessor().cities();
							}

							@Override
							public void onSuccess(List<CityEntity> result) {
								cities = (ArrayList<CityEntity>) result;
								initAndShowChooseCityWindow(v);
							}

							@Override
							public void onFailed(Error error) {
								cities = (ArrayList<CityEntity>) ProcessorFactory
										.getStoreProcessor().citiesFromLocal();
								// super.onFailed(error);
								initAndShowChooseCityWindow(v);
							}
						});
				task.execute();
			} else {
				cityPopupWindow.showAtLocation(v, Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, 0);
			}
		}
	};

	public static StorefrontEditFragment newInstance(StoreEntity store,
			int currentIndex) {
		Bundle args = new Bundle();
		args.putSerializable(FuncConst.NAME_STORE, store);
		args.putInt(FuncConst.EXTRA_CURRENT_INDEX, currentIndex);

		StorefrontEditFragment fragment = new StorefrontEditFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		Bundle args = getArguments();
		if (args != null) {
			store = (StoreEntity) args.getSerializable(FuncConst.NAME_STORE);
			currentIndex = args.getInt(FuncConst.EXTRA_CURRENT_INDEX, -1);
		}

		if (currentIndex < 0) {
			getActivity().getActionBar().setTitle(R.string.title_new_storefront);
			storefront = new StorefrontEntity();
		} else {
			getActivity().getActionBar().setTitle(R.string.title_edit_storefront);
			storefront = store.getStorefronts().get(currentIndex);
		}

		super.onCreate(savedInstanceState);
	}

	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		if (storefront != null && storefront.getName() != null) {
			View actionBarView = getActionBarView();
			ImageView actionImage = (ImageView) actionBarView
					.findViewById(R.id.img_action);
			actionImage.setImageResource(R.drawable.delete);
			actionImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (alertPopupWindow == null) {
						alertPopupWindow = new AlertPopupWindow(getActivity(), null,
								"是否删除门店?", new OnClickListener() {

									@Override
									public void onClick(View v) {
										alertPopupWindow.dismiss();
										switch (v.getId()) {
										case R.id.t_no:
											break;
										case R.id.t_yes:
											delete();
											break;
										default:
											break;
										}
									}
								});
					}
					alertPopupWindow.showAtLocation(v, Gravity.BOTTOM
							| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
				}
			});
		}
	}

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.store_edit, menu);
	// }
	//
	// @Override
	// public void onPrepareOptionsMenu(Menu menu) {
	// super.onPrepareOptionsMenu(menu);
	// if (storefront != null && storefront.getUuid() != null) {
	// menu.findItem(R.id.action_delete).setVisible(true);
	// } else {
	// menu.findItem(R.id.action_delete).setVisible(false);
	// }
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.action_delete:
	// delete();
	// return true;
	//
	// default:
	// break;
	// }
	// return super.onOptionsItemSelected(item);
	// }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_storefront_edit, container,
				false);
		initViews(view);

		return view;
	}

	private void initViews(View view) {
		view.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					ViewUtils.hideInputMethodWindow(getActivity());
				}
				return true;
			}
		});

		nameText = (EditText) view.findViewById(R.id.e_name);
		addrText = (EditText) view.findViewById(R.id.e_addr);
		phoneText = (EditText) view.findViewById(R.id.e_phone);
		cityText = (TextView) view.findViewById(R.id.e_city);

		view.findViewById(R.id.row_city).setOnClickListener(chooseCityListener);
		cityText.setOnClickListener(chooseCityListener);

		nameText.setText(storefront.getName());
		addrText.setText(storefront.getAddr());
		phoneText.setText(storefront.getPhone() != null ? storefront.getPhone()
				: "");
		cityText.setText(storefront.getCity() != null ? storefront.getCity()
				.getName() : "");

		if (storefront.getUuid() != null) {
			Button rightButton = (Button) view.findViewById(R.id.b_right);
			rightButton.setText(R.string.b_save);
			rightButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					ViewUtils.hideInputMethodWindow(getActivity());
					save();
				}
			});
		} else {
			Button rightButton = (Button) view.findViewById(R.id.b_right);
			rightButton.setText(R.string.b_add_continue);
			rightButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					ViewUtils.hideInputMethodWindow(getActivity());
					addContinue();
				}
			});
		}

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("name", new LengthValidator(nameText, "名称必须100字以内",
				1, 100));
		mValidationManager.add("address", new LengthValidator(addrText,
				"地址必须200字以内", 1, 200));
		mValidationManager.add("city", new LengthValidator(cityText, "请选择城市", 1,
				Integer.MAX_VALUE));
	}

	private void initAndShowChooseCityWindow(View view) {
		Context context = getActivity();
		if (context == null) {
			return;
		}
		cityPopupWindow = new SelectCityPopupWindow(getActivity(),
				storefront.getCity(), cities, new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						cityPopupWindow.dismiss();
						cityText.setText(cities.get(position).getName());
						storefront.setCity(cities.get(position));
					}
				});
		cityPopupWindow.showAtLocation(view, Gravity.BOTTOM
				| Gravity.CENTER_HORIZONTAL, 0, 0);
	}

	private void delete() {
		if (storefront.getUuid() != null) {
			AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
					getActivity(), "正在删除门店，请稍候...", new ProcessorTaskCallback<Boolean>() {

						@Override
						public ProcessResult<Boolean> doInBackground() {
							return ProcessorFactory.getStoreProcessor().deleteStorefront(
									storefront);
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								ViewUtils.showToast("删除门店成功.");
								store.getStorefronts().remove(currentIndex);
								back();
							} else {
								ViewUtils.showToast("删除门店失败.");
							}
						}
					});
			task.execute();
		} else {
			store.getStorefronts().remove(currentIndex);
			back();
		}
	}

	private void save() {
		if (!validateFieldValues()) {
			return;
		}

		storefront.setName(nameText.getText().toString());
		storefront.setAddr(addrText.getText().toString());
		storefront.setPhone(phoneText.getText().toString());

		if (storefront.getUuid() != null) {
			saveStorefrontToServer(storefront, true);
		} else {
			back();
		}
	}

	private void addContinue() {
		saveToStoreEntity(false);

		ViewUtils.showToast("门店已添加");

		nameText.setText("");
		addrText.setText("");
		phoneText.setText("");
	}

	// private void done() {
	// saveToStoreEntity(true);
	// }

	@Override
	protected void back() {
		Intent intent = new Intent();
		intent.putExtra(NAME_STOREFRONTS,
				(ArrayList<StorefrontEntity>) store.getStorefronts());
		getActivity().setResult(Activity.RESULT_OK, intent);
		super.back();
	}

	private boolean validateFieldValues() {
		return mValidationManager.validateAllAndSetError();
	}

	private void saveToStoreEntity(boolean back) {
		if (!validateFieldValues()) {
			return;
		}

		storefront.setName(nameText.getText().toString());
		storefront.setAddr(addrText.getText().toString());
		storefront.setPhone(phoneText.getText().toString());
		storefront.setStore(store);

		if (store.getUuid() != null) {
			saveStorefrontToServer(storefront, back);
		} else {
			store.getStorefronts().add(storefront);
			if (back) {
				back();
			}
		}
	}

	private void saveStorefrontToServer(final StorefrontEntity storefront,
			final boolean back) {
		ViewUtils.hideInputMethodWindow(getActivity());
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在保存门店，请稍候...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						IStoreProcessor processor = ProcessorFactory.getStoreProcessor();
						if (storefront.getUuid() != null) {
							return processor.updateStorefront(storefront);
						}

						return processor.addStorefront(storefront);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							ViewUtils.showToast("保存门店成功.");
							if (store.getStorefronts().contains(storefront)) {
								for (StorefrontEntity front : store.getStorefronts()) {
									if (front.equals(storefront)) {
										front = storefront;
									}
								}
							} else {
								store.getStorefronts().add(storefront);
							}

							if (back) {
								back();
							}
						} else {
							ViewUtils.showToast("保存门店失败.");
						}
					}
				});
		task.execute();
	}

}

package com.seekon.zhekouhu.activity.user;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarListFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.award.widget.UserAwardListAdapter;
import com.seekon.zhekouhu.func.user.UserAwardEntity;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.user.UserProfileEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.func.widget.XListView;
import com.seekon.zhekouhu.func.widget.XListView.IXListViewListener;

public class AwardDetailFragment extends CustomActionBarListFragment implements
		IXListViewListener {

	private int offset;
	private ArrayList<UserAwardEntity> dataList = new ArrayList<UserAwardEntity>();

	private Handler mHandler;
	private XListView xListView;
	private View mainView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

		mHandler = new Handler();
		loadMoreData();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.fragment_user_award_detail, container, false);
		initViews(mainView);
		return mainView;
	}

	private void initViews(final View view) {
		xListView = (XListView) view.findViewById(android.R.id.list);
		xListView.setPullLoadEnable(true);
		xListView.setXListViewListener(this);

		updateCommentCountView();

		// updateRefreshTime();
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				view.findViewById(R.id.img_hit).setVisibility(View.VISIBLE);
			}
		});
	}

	public void setAdapter() {
		AbstractListAdapter<UserAwardEntity> adapter = (AbstractListAdapter<UserAwardEntity>) getListAdapter();
		if (adapter == null) {
			setListAdapter(new UserAwardListAdapter(getActivity(), dataList));
		} else {
			adapter.updateData(dataList);
		}

		showDataView();
		updateFooter();
	}

	private void onPostLoad() {
		if (xListView != null) {
			xListView.stopRefresh();
			xListView.stopLoadMore();
			updateFooter();
		}
	}

	@Override
	public void onRefresh() {
		onPostLoad();
	}

	@Override
	public void onLoadMore() {
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				loadMoreData();
				onPostLoad();
			}
		}, 2000);
	}

	private void loadMoreData() {

		AsyncProcessorTask<List<UserAwardEntity>> task = new AsyncProcessorTask<List<UserAwardEntity>>(
				getActivity(), new ProcessorTaskCallback<List<UserAwardEntity>>() {

					@Override
					public ProcessResult<List<UserAwardEntity>> doInBackground() {
						return ProcessorFactory.getUserProcessor().getUserAwardDetail(offset);
					}

					@Override
					public void onSuccess(List<UserAwardEntity> result) {
						if (result != null && !result.isEmpty()) {
							offset += result.size();
							dataList.addAll(result);
							setAdapter();
						} else if (getListAdapter() == null) {
							setAdapter();
						}
					}

					@Override
					public void onFailed(Error error) {
						setAdapter();
						super.onFailed(error);
					}
				});
		task.execute();
	}

	private void showDataView() {
		if (mainView != null) {
			mainView.findViewById(R.id.v_loading).setVisibility(View.GONE);
			mainView.findViewById(R.id.v_data).setVisibility(View.VISIBLE);
		}
	}

	private void updateFooter() {
		if (xListView != null) {
			if (dataList.isEmpty()) {
				xListView.getFooterView().setHintText("暂时没有数据");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setHintText(null);
				xListView.getFooterView().setEnabled(true);
			}
		}
	}

	private void updateCommentCountView() {
		UserProfileEntity profile = RunEnv.getInstance().getUser().getProfile();
		TextView countView = (TextView) mainView.findViewById(R.id.t_award_sum);
		countView.setText("共有" + (profile.getAwardSum() - profile.getAwardUsed()) + "积分可用");
	}

}

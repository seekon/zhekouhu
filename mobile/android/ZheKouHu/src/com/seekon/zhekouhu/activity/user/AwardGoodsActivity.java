package com.seekon.zhekouhu.activity.user;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;

public class AwardGoodsActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new AwardGoodsFragment();
	}

}

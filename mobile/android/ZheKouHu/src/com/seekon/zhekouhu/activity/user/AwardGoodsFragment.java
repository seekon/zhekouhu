package com.seekon.zhekouhu.activity.user;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.activity.WebViewActivity;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.award.GoodsEntity;
import com.seekon.zhekouhu.func.award.widget.GoodsListAdapter;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.user.UserProfileEntity;
import com.seekon.zhekouhu.func.widget.XListView;

public class AwardGoodsFragment extends CustomActionBarFragment {

	private static final int LOGIN_REQUEST_CODE = 15;
	private static final int GOODS_DETAIL_REQUEST_CODE = 16;

	private static final String AWARD_INFO_FORMAT = "亲，您目前尚有%d积分可用，已使用%d积分.";

	private View mainView;
	private List<GoodsEntity> dataList = new ArrayList<GoodsEntity>();
	private boolean resultOk = false;
	XListView xListView = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater
				.inflate(R.layout.fragment_award_goods, container, false);
		initViews();
		return mainView;
	}

	private void initViews() {
		if (mainView == null) {
			return;
		}

		xListView = (XListView) mainView.findViewById(R.id.list_goods);
		xListView.setPullRefreshEnable(false);
		xListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				GoodsEntity goods = dataList.get(position - 1);
				Intent intent = new Intent(getActivity(), WebViewActivity.class);
				intent.putExtra(FuncConst.EXTRA_TITLE, goods.getName());
				intent.putExtra(FuncConst.EXTRA_URI,
						"/goods_detail.html?uuid=" + goods.getUuid());
				startActivityForResult(intent, GOODS_DETAIL_REQUEST_CODE);
			}

		});

		Button bWatch = (Button) mainView.findViewById(R.id.b_award_watch);
		bWatch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				watchUserAwards();
			}
		});

		TextView awardHelpView = (TextView) mainView
				.findViewById(R.id.t_award_help);
		awardHelpView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), WebViewActivity.class);
				intent.putExtra(FuncConst.EXTRA_TITLE, "积分帮助");
				intent.putExtra(FuncConst.EXTRA_URI, "/award_help.html");
				startActivity(intent);
			}
		});

		updateViewData();
	}

	private void updateViewData() {
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				TextView awardInfoView = (TextView) mainView
						.findViewById(R.id.t_award_info);
				Button bWatch = (Button) mainView.findViewById(R.id.b_award_watch);
				UserEntity user = RunEnv.getInstance().getUser();
				if (user == null) {
					awardInfoView.setText(R.string.hit_not_login);
				} else {
					awardInfoView.setText("");
					refreshUserAward();
					bWatch.setText(R.string.b_award_watch);
				}
			}
		});

		if (dataList.isEmpty()) {
			refreshGoodsList();
		}
	}

	private void refreshGoodsList() {
		AsyncProcessorTask<List<GoodsEntity>> task = new AsyncProcessorTask<List<GoodsEntity>>(
				getActivity(), "正在加载兑换商品...",
				new ProcessorTaskCallback<List<GoodsEntity>>() {

					@Override
					public ProcessResult<List<GoodsEntity>> doInBackground() {
						return ProcessorFactory.getAwardProcessor().getGoodsList();
					}

					@Override
					public void onSuccess(List<GoodsEntity> result) {
						dataList.addAll(result);
						setGoodsAdapter();
					}

					@Override
					public void onFailed(Error error) {
						super.onFailed(error);
						setGoodsAdapter();
					}

					@Override
					public void onCancelled() {
						super.onCancelled();
						setGoodsAdapter();
					}

				});
		task.execute();
	}

	private void refreshUserAward() {
		final TextView awardInfoView = (TextView) mainView
				.findViewById(R.id.t_award_info);
		AsyncProcessorTask<UserProfileEntity> task1 = new AsyncProcessorTask<UserProfileEntity>(
				getActivity(), new ProcessorTaskCallback<UserProfileEntity>() {

					@Override
					public ProcessResult<UserProfileEntity> doInBackground() {
						return ProcessorFactory.getUserProcessor().getUserAward();
					}

					@Override
					public void onSuccess(UserProfileEntity result) {
						setAwardInfoView(result);
					}

					@Override
					public void onFailed(Error error) {
						UserProfileEntity profile = null;
						UserEntity user = RunEnv.getInstance().getUser();
						if (user != null) {
							profile = user.getProfile();
						}
						setAwardInfoView(profile);
					}

					@Override
					public void onCancelled() {
						setAwardInfoView(null);
					}

					private void setAwardInfoView(UserProfileEntity profile) {
						if (profile == null) {
							profile = new UserProfileEntity();
						}
						awardInfoView.setText(String.format(AWARD_INFO_FORMAT,
								new Object[] { profile.getAwardSum(), profile.getAwardUsed() }));
					}
				});
		task1.execute();
	}

	private void watchUserAwards() {
		if (RunEnv.getInstance().getUser() == null) {
			Intent intent = new Intent(getActivity(), UserLoginActivity.class);
			startActivityForResult(intent, LOGIN_REQUEST_CODE);
		} else {
			openAwardDetail();
		}
	}

	private void setGoodsAdapter() {
		ListAdapter adapter = xListView.getAdapter();
		if (adapter == null) {
			adapter = new GoodsListAdapter(getActivity(), dataList);
			xListView.setAdapter(adapter);
		} else {
			if (adapter instanceof GoodsListAdapter) {
				((GoodsListAdapter)adapter).updateData(dataList);
			}
		}
		updateFooter();
	}

	private void updateFooter() {
		if (xListView != null) {
			if (dataList.isEmpty()) {
				xListView.getFooterView().setVisibility(View.VISIBLE);
				xListView.getFooterView().setHintText("暂时没有数据");
				xListView.getFooterView().setEnabled(false);
			} else {
				xListView.getFooterView().setVisibility(View.GONE);
			}
		}
	}

	private void openAwardDetail() {
		Intent intent = new Intent(getActivity(), AwardDetailActivity.class);
		startActivity(intent);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		switch (requestCode) {
		case LOGIN_REQUEST_CODE:
			resultOk = true;
			updateViewData();
			openAwardDetail();
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void back() {
		if (resultOk) {
			getActivity().setResult(Activity.RESULT_OK);
		}
		super.back();
	}

}

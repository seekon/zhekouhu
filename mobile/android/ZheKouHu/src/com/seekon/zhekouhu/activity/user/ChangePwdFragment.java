package com.seekon.zhekouhu.activity.user;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.ErrorAbleEditText;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.LengthValidator;

public class ChangePwdFragment extends CustomActionBarFragment {

	private EditText oldPwdText;
	private EditText newPwdText;
	private EditText newPwdConfText;

	private ValidationManager mValidationManager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_change_pwd, container, false);
		initViews(view);

		return view;
	}

	private void initViews(View view) {
		oldPwdText = (EditText) view.findViewById(R.id.e_old_pwd);
		newPwdText = (EditText) view.findViewById(R.id.e_new_pwd);
		newPwdConfText = (EditText) view.findViewById(R.id.e_new_pwd_conf);

		Button submitButton = (Button) view.findViewById(R.id.b_submit);
		submitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				changePwd();
			}
		});

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("pwd", new LengthValidator(newPwdText, "密码必须4~10位",
				4, 10));
	}

	private void changePwd() {
		final String oldPwd = oldPwdText.getText().toString();
		final String newPwd = newPwdText.getText().toString();
		String newPwdConf = newPwdConfText.getText().toString();

		boolean cancel = !mValidationManager.validateAllAndSetError();

		// 服务器端检查原密码是否正确
		// if (!oldPwd.equals(RunEnv.getInstance().getUser().getPwd())) {
		// ((ErrorAbleEditText) oldPwdText).betterSetError("原密码输入错误.", !cancel);
		// cancel = true;
		// }

		if (!newPwd.equals(newPwdConf)) {
			((ErrorAbleEditText) newPwdConfText)
					.betterSetError("两次输入密码不一致.", !cancel);
			cancel = true;
		}

		if (cancel) {
			return;
		}

		// if (newPwd.equals(oldPwd)) {
		// ViewUtils.showToast("新密码与原密码一致，不必修改.");
		// return;
		// }

		ViewUtils.hideInputMethodWindow(getActivity());
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在修改密码...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						UserEntity user = RunEnv.getInstance().getUser();
						return ProcessorFactory.getUserProcessor().changePwd(
								user.getUuid(), newPwd, oldPwd);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							ViewUtils.showToast("密码修改成功.");
							getActivity().finish();
						}
					}

					@Override
					public void onFailed(Error error) {
						if (error != null && error.type != null
								&& error.type.endsWith(FuncConst.VAL_ERROR_PWD)) {
							((ErrorAbleEditText) oldPwdText).betterSetError("原密码错误.", true);
						} else {
							super.onFailed(error);
						}
					}

				});
		task.execute();
	}
}

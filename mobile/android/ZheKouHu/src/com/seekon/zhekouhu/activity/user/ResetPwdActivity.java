package com.seekon.zhekouhu.activity.user;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.FormFragmentActivity;

public class ResetPwdActivity extends FormFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new ResetPwdFragment();
	}

}

package com.seekon.zhekouhu.activity.user;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.AbstractValueValidator;
import com.seekon.zhekouhu.validation.ErrorAbleEditText;
import com.seekon.zhekouhu.validation.LogicOrValidator;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.EmailValueValidator;
import com.seekon.zhekouhu.validation.validator.PhoneValueValidator;

public class ResetPwdFragment extends CustomActionBarFragment {

	private EditText codeField;

	private ValidationManager mValidationManager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_reset_pwd, container, false);
		initViews(view);
		return view;
	}

	private void initViews(View view) {
		codeField = (EditText) view.findViewById(R.id.e_userCode);

		Button button = (Button) view.findViewById(R.id.b_submit);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resetPwd();
			}
		});

		mValidationManager = new ValidationManager(getActivity());
		LogicOrValidator accountValidator = new LogicOrValidator(codeField,
				new AbstractValueValidator[] { new EmailValueValidator(codeField, ""),
						new PhoneValueValidator(codeField, "") }, "请正确输入电子邮箱/手机号");
		mValidationManager.add("account", accountValidator);
	}

	private void resetPwd() {
		boolean cancel = !mValidationManager.validateAllAndSetError();
		if (cancel) {
			return;
		}

		String userCode = codeField.getText().toString();
		if (TextUtils.isEmpty(userCode)) {
			codeField.setError(getActivity().getString(R.string.error_field_required,
					"电子邮箱或手机号"));
			codeField.requestFocus();
			return;
		}

		ViewUtils.hideInputMethodWindow(getActivity());

		ViewUtils.hideInputMethodWindow(getActivity());
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在提交...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getUserProcessor().resetPwd(
								codeField.getText().toString());
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							ViewUtils.showToast("提交成功，请注意查收密码.");
							getActivity().finish();
						}
					}

					@Override
					public void onFailed(Error error) {
						if (error != null && error.type != null
								&& error.type.endsWith(FuncConst.VAL_ERROR_USER)) {
							((ErrorAbleEditText) codeField).betterSetError(error.message,
									true);
						} else {
							super.onFailed(error);
						}
					}

				});
		task.execute();
	}
}

package com.seekon.zhekouhu.activity.user;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.SingleFragmentActivity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.discount.DiscountEntity;

public class UserEditDiscountActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		DiscountEntity discount = (DiscountEntity) getIntent()
				.getSerializableExtra(FuncConst.NAME_DISCOUNT);
		if (discount != null) {
			return UserEditDiscountFragment.newInstance(discount);
		}
		return new UserEditDiscountFragment();
	}

}

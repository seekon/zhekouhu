package com.seekon.zhekouhu.activity.user;

import static com.seekon.zhekouhu.func.FuncConst.EXTRA_CURRENT_INDEX;
import static com.seekon.zhekouhu.func.FuncConst.EXTRA_FLAG_READONLY;
import static com.seekon.zhekouhu.func.FuncConst.NAME_IMAGES;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.activity.ImagePreviewActivity;
import com.seekon.zhekouhu.activity.MainActivity;
import com.seekon.zhekouhu.activity.store.NearbyStoresActivity;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.discount.widget.DiscountImagesAdapter;
import com.seekon.zhekouhu.func.store.NearbyStoreEntity;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.widget.AlertPopupWindow;
import com.seekon.zhekouhu.func.widget.SelectPicturePopupWindow;
import com.seekon.zhekouhu.utils.Logger;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.ErrorAbleImageView;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.LengthValidator;

public class UserEditDiscountFragment extends CustomActionBarFragment {

	private static final String TAG = UserEditDiscountFragment.class.getName();

	private static final int IMAGE_PREVIEW_REQUEST_CODE = 3;

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

	private static final int LOAD_IMAGE_ACTIVITY_REQUEST_CODE = 200;

	private static final int LOAD_NEARBY_STORE_REQUEST_CODE = 4;

	private String currentAddedFileUri = null;

	private PopupWindow picPopupWindow;
	private PopupWindow alertPopupWindow;
	private Dialog anonymPubDialog;

	private DiscountEntity discount;
	private NearbyStoreEntity location;

	private EditText contentText;
	private TextView storeNameView;

	private DiscountImagesAdapter imagesAdapter;
	private GridView imagesView;

	private ArrayList<FileEntity> addedImages = new ArrayList<FileEntity>();
	private ArrayList<FileEntity> deletedImages = new ArrayList<FileEntity>();

	private ValidationManager mValidationManager;

	private View mainView;
	
	private Boolean showAuditIdea =  true;
	
	public static UserEditDiscountFragment newInstance(DiscountEntity discount) {
		Bundle args = new Bundle();
		args.putSerializable(FuncConst.NAME_DISCOUNT, discount);

		UserEditDiscountFragment fragment = new UserEditDiscountFragment();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Bundle args = getArguments();
		if (args != null) {
			discount = (DiscountEntity) args.getSerializable(FuncConst.NAME_DISCOUNT);
		}

		if (discount == null) {
			initDiscount();
		}

		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mainView = inflater.inflate(R.layout.fragment_pub_discount, container,
				false);
		initViews(mainView);
		return mainView;
	}

	@Override
	protected void createCustomActionBar() {
		super.createCustomActionBar();

		if (discount != null && discount.getUuid() != null) {
			View actionBarView = getActionBarView();
			ImageView actionImage = (ImageView) actionBarView
					.findViewById(R.id.img_action);
			actionImage.setImageResource(R.drawable.delete);
			actionImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (alertPopupWindow == null) {
						alertPopupWindow = new AlertPopupWindow(getActivity(), null,
								"是否删除活动?", new OnClickListener() {

									@Override
									public void onClick(View v) {
										alertPopupWindow.dismiss();
										switch (v.getId()) {
										case R.id.t_no:
											break;
										case R.id.t_yes:
											delete();
											break;
										default:
											break;
										}
									}
								});
					}
					alertPopupWindow.showAtLocation(v, Gravity.BOTTOM
							| Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
				}
			});
		}
	}

	private void initViews(View view) {
		imagesView = (GridView) view.findViewById(R.id.gridView);
		storeNameView = (TextView) view.findViewById(R.id.t_store_name);
		contentText = (EditText) view.findViewById(R.id.e_content);

		updateImageView(discount.getImages().size() + 1);
		imagesAdapter = new DiscountImagesAdapter(getActivity(),
				discount.getImages());
		imagesView.setAdapter(imagesAdapter);

		imagesView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				if (position == imagesAdapter.getCount() - 1) {
					showPopupWindow(view);
				} else {
					Intent intent = new Intent(getActivity(), ImagePreviewActivity.class);
					ArrayList<FileEntity> images = new ArrayList<FileEntity>(addedImages);
					images.addAll(discount.getImages());
					intent.putExtra(NAME_IMAGES, images);
					intent.putExtra(EXTRA_CURRENT_INDEX, position);
					intent.putExtra(EXTRA_FLAG_READONLY, false);
					startActivityForResult(intent, IMAGE_PREVIEW_REQUEST_CODE);
				}
			}
		});

		View placeRow = view.findViewById(R.id.row_storefront);
		placeRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), NearbyStoresActivity.class);
				startActivityForResult(intent, LOAD_NEARBY_STORE_REQUEST_CODE);
			}
		});

		Button publishButton = (Button) view.findViewById(R.id.b_publish);
		String status = discount.getStatus();
		if (status != null
				&& (status.equals(FuncConst.VAL_DISCOUNT_STATUS_INVALID) || status
						.equals(FuncConst.VAL_DISCOUNT_STATUS_VALID))) {
			int disableColor = getActivity().getResources().getColor(
					R.color.rgb_7f7f7f);
			publishButton.setEnabled(false);
			publishButton.setTextColor(disableColor);
			publishButton.setBackgroundResource(android.R.drawable.btn_default);
			contentText.setFocusable(false);
			contentText.setTextColor(disableColor);
			imagesView.setEnabled(false);
			placeRow.setEnabled(false);
		} else {
			publishButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					publish();
				}
			});
		}

		if (status != null) {
			final ErrorAbleImageView statusImgView = (ErrorAbleImageView) view
					.findViewById(R.id.img_status);
			if (status.equals(FuncConst.VAL_DISCOUNT_STATUS_CHECKING)) {
				statusImgView.setImageResource(R.drawable.text_checking);
			} else if (status.equals(FuncConst.VAL_DISCOUNT_STATUS_INVALID)) {
				statusImgView.setImageResource(R.drawable.text_invalid);
				statusImgView.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						if(showAuditIdea){
							statusImgView.betterSetError(discount.getAuditIdea(), showAuditIdea);
						}else{
							statusImgView.hideError();
						}
						showAuditIdea = !showAuditIdea;
					}
				});
				
				
			} else if (status.equals(FuncConst.VAL_DISCOUNT_STATUS_VALID)) {
				statusImgView.setImageResource(R.drawable.text_valid);
			}
		}

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("content", new LengthValidator(contentText,
				"活动必须140字以内", 0, 140));

		if ((discount == null || discount.getUuid() == null)
				&& RunEnv.getInstance().getUser() == null) {
			if (anonymPubDialog == null) {
				anonymPubDialog = new Dialog(getActivity(),
						R.style.anonym_pub_discount_dialog) {
					@Override
					protected void onCreate(Bundle savedInstanceState) {
						super.onCreate(savedInstanceState);

						View view = LayoutInflater.from(getActivity()).inflate(
								R.layout.pop_anonym_pub_discount, null);
						Display d = getActivity().getWindowManager().getDefaultDisplay(); // 为获取屏幕宽、高
						LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
								(int) (d.getWidth() * 0.9),
								LinearLayout.LayoutParams.WRAP_CONTENT);
						setContentView(view, lp);

						Button bContinue = (Button) findViewById(R.id.b_continue);
						bContinue.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								anonymPubDialog.dismiss();
							}
						});

						Button bCancel = (Button) findViewById(R.id.b_cancel);
						bCancel.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								anonymPubDialog.dismiss();
								if (getActivity() instanceof MainActivity) {
									((MainActivity) getActivity()).backSelected();
								} else {
									back();
								}
							}
						});
					}
				};

			}

			anonymPubDialog.show();
		}

		initViewData();
	}

	private void initViewData() {
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				if (discount.getUuid() != null) {
					contentText.setText(discount.getContent());
					String location = discount.getLocation();
					if (location != null && location.trim().length() > 0) {
						try {
							JSONObject json = new JSONObject(location);
							storeNameView.setText(json.getString(DataConst.COL_NAME_NAME));
						} catch (JSONException e) {
							// do nothing
						}
					}
				} else {
					contentText.setText("");
					storeNameView.setText("");
				}
			}
		});
	}

	private void updateImageView(int numColumns) {
		if (numColumns > 1) {
			View view = imagesView.getChildAt(0);
			if (view != null) {
				((ErrorAbleImageView) view.findViewById(R.id.img_discount)).hideError();
			}
		}
		int width = (int) (getActivity().getResources().getDisplayMetrics().density * (65 + 18));
		LayoutParams params = new LayoutParams(numColumns * width,
				LayoutParams.WRAP_CONTENT);
		imagesView.setLayoutParams(params);
		imagesView.setNumColumns(numColumns);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		switch (requestCode) {
		case IMAGE_PREVIEW_REQUEST_CODE:
			if (data != null) {
				List<FileEntity> delImages = (List<FileEntity>) data
						.getSerializableExtra(FuncConst.NAME_DEL_IMAGES);
				for (FileEntity file : delImages) {
					if (addedImages.contains(file)) {
						addedImages.remove(file);
					} else if (discount.getImages().contains(file)) {
						discount.getImages().remove(file);
						if (!deletedImages.contains(file)) {
							deletedImages.add(file);
						}
					}
				}
				List<FileEntity> images = new ArrayList<FileEntity>(addedImages);
				images.addAll(discount.getImages());
				updateImageView(images.size() + 1);
				imagesAdapter.updateData(images);
			}
			break;
		case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
			updateDiscountImages();
			break;
		case LOAD_IMAGE_ACTIVITY_REQUEST_CODE:
			if (null != data) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = null;
				try {
					cursor = getActivity().getContentResolver().query(selectedImage,
							filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					currentAddedFileUri = cursor.getString(columnIndex);
					updateDiscountImages();
				} finally {
					cursor.close();
				}
			}
			break;
		case LOAD_NEARBY_STORE_REQUEST_CODE:
			if (data != null) {
				NearbyStoreEntity store = (NearbyStoreEntity) data
						.getSerializableExtra(FuncConst.NAME_NEABY_STORE);
				storeNameView.setText(store.getName());
				location = store;
			}
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * 更新图片列表
	 */
	private void updateDiscountImages() {
		String name = "_discount_" + System.currentTimeMillis() + ".png";
		UserEntity user = RunEnv.getInstance().getUser();
		if (user != null) {
			name = user.getCode() + name;
		} else {
			name = ViewUtils.getDeviceId() + name;
		}
		addedImages.add(new FileEntity(currentAddedFileUri, name));
		List<FileEntity> images = new ArrayList<FileEntity>(addedImages);
		images.addAll(discount.getImages());
		updateImageView(images.size() + 1);
		imagesAdapter.updateData(images);
	}

	/**
	 * 弹出图片选择框
	 * 
	 * @param v
	 */
	private void showPopupWindow(View v) {
		if (picPopupWindow == null) {
			picPopupWindow = new SelectPicturePopupWindow(getActivity(),
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							picPopupWindow.dismiss();
							switch (v.getId()) {
							case R.id.v_camera:
								openCamera();
								break;
							case R.id.v_photo:
								openImageDir();
								break;
							default:
								break;
							}
						}
					});
		}
		picPopupWindow.showAtLocation(v,
				Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0); // 设置layout在PopupWindow中显示的位置
	}

	private void openCamera() {
		Logger.debug(TAG, "openCamera");
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = FileHelper
				.getFileFromCache(System.currentTimeMillis() + ".png");
		currentAddedFileUri = file.getPath();
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

		startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	private void openImageDir() {
		Logger.debug(TAG, "openImageDir");
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, LOAD_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	private void delete() {
		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在删除，请稍后...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getDiscountProcessor().deleteDiscount(
								discount);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							back();
						} else {
							ViewUtils.showToast("删除失败.");
						}
					}
				});
		task.execute();
	}

	private void publish() {
		String content = contentText.getText().toString();

		// TODO检查数据的完整性
		boolean cancel = !mValidationManager.validateAllAndSetError();

		if (addedImages.isEmpty() && (discount.getImages().isEmpty())) {
			View view = imagesView.getChildAt(0);
			((ErrorAbleImageView) view.findViewById(R.id.img_discount))
					.betterSetError("请至少添加一张活动图片.", !cancel);

			cancel = true;
		}

		if (cancel) {
			return;
		}

		discount.setContent(content);
		if (location != null) {
			discount.setLocation(location.toString());
		}

		if (discount.getUuid() != null) {// 更新数据
			AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
					getActivity(), "正在发布，请稍后...", new ProcessorTaskCallback<Boolean>() {

						@Override
						public ProcessResult<Boolean> doInBackground() {
							return ProcessorFactory.getDiscountProcessor().updateDiscount(
									discount, deletedImages, addedImages);
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								ViewUtils.showToast("发布成功.");
							} else {
								ViewUtils.showToast("发布失败.");
							}
						}
					});
			task.execute();
		} else {
			UserEntity user = RunEnv.getInstance().getUser();
			if (user != null) {
				discount.setPublisher(user.getUuid());
			} else {
				discount.setPublisher(ViewUtils.getDeviceId());
			}

			discount.setImages(addedImages);
			AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
					getActivity(), "正在发布，请稍后...", new ProcessorTaskCallback<Boolean>() {

						@Override
						public ProcessResult<Boolean> doInBackground() {
							return ProcessorFactory.getDiscountProcessor().publishDiscount(
									discount);
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								ViewUtils.showToast("发布成功.");
								clearData();
								initViewData();
							} else {
								ViewUtils.showToast("发布失败.");
							}
						}
					});
			task.execute();
		}
	}

	private void initDiscount() {
		discount = new DiscountEntity();
		discount.setType(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY);
		discount.setOrigin(FuncConst.VAL_DISCOUNT_ORIGIN_CUSTOMER);
		discount.setStartDate(System.currentTimeMillis());
		discount.setEndDate(System.currentTimeMillis());
		discount.setParValue(0);

		CategoryEntity cate = new CategoryEntity();
		cate.setUuid("100");
		discount.setCategory(cate);
	}

	protected void clearData() {
		initDiscount();

		location = null;
		addedImages = new ArrayList<FileEntity>();
		deletedImages = new ArrayList<FileEntity>();

		if (imagesAdapter != null) {
			imagesAdapter.updateData(discount.getImages());
		}
	}

	@Override
	protected void back() {
		getActivity().setResult(Activity.RESULT_OK);
		super.back();
	}

}

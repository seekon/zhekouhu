package com.seekon.zhekouhu.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.AbstractValueValidator;
import com.seekon.zhekouhu.validation.ErrorAbleEditText;
import com.seekon.zhekouhu.validation.LogicOrValidator;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.EmailValueValidator;
import com.seekon.zhekouhu.validation.validator.LengthValidator;
import com.seekon.zhekouhu.validation.validator.PhoneValueValidator;

public class UserLoginFragment extends CustomActionBarFragment {

	private static final int REGISTER_REQUEST_CODE = 1;

	private EditText userCodeField;
	private EditText pwdField;

	private ValidationManager mValidationManager;

	// @Override
	// public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// inflater.inflate(R.menu.user_login, menu);
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// switch (item.getItemId()) {
	// case R.id.action_register: {
	// Intent intent = new Intent(getActivity(), UserRegisterActivity.class);
	// startActivityForResult(intent, REGISTER_REQUEST_CODE);
	// break;
	// }
	// default:
	// break;
	// }
	//
	// return super.onOptionsItemSelected(item);
	// }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.fragment_user_login, container, false);
		initViews(view);
		return view;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		switch (requestCode) {
		case REGISTER_REQUEST_CODE:
			getActivity().setResult(Activity.RESULT_OK);
			back();
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void initViews(View mainView) {
		this.userCodeField = (EditText) mainView.findViewById(R.id.e_userCode);
		this.pwdField = (EditText) mainView.findViewById(R.id.e_password);

		Button loginButton = (Button) mainView.findViewById(R.id.b_login);
		loginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				login();
			}
		});

		View pwdView = mainView.findViewById(R.id.t_forget_pwd);
		pwdView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), ResetPwdActivity.class);
				startActivity(intent);
			}
		});

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("pwd", new LengthValidator(pwdField, "密码必须4~10位", 4,
				10));

		LogicOrValidator accountValidator = new LogicOrValidator(userCodeField,
				new AbstractValueValidator[] {
						new EmailValueValidator(userCodeField, ""),
						new PhoneValueValidator(userCodeField, "") }, "请正确输入电子邮箱/手机号");
		mValidationManager.add("account", accountValidator);
	}

	private void login() {
		this.userCodeField.endBatchEdit();
		this.pwdField.endBatchEdit();

		// ViewUtils.hideInputMethodWindow(getActivity());

		// this.userCodeField.setError(null);
		// this.pwdField.setError(null);
		// View focusView = null;
		// boolean cancel = false;
		//
		// final String pwd = this.pwdField.getText().toString();
		// if (TextUtils.isEmpty(pwd)) {
		// pwdField.setError(getActivity().getString(R.string.error_field_required,
		// "密码"));
		// focusView = pwdField;
		// cancel = true;
		// }
		//
		// final String code = this.userCodeField.getText().toString();
		// if (TextUtils.isEmpty(code)) {
		// userCodeField.setError(getActivity().getString(
		// R.string.error_field_required, "登录账号"));
		// focusView = userCodeField;
		// cancel = true;
		// }
		boolean cancel = !mValidationManager.validateAllAndSetError();
		if (cancel) {
			return;
		}

		ViewUtils.hideInputMethodWindow(getActivity());

		AsyncProcessorTask<UserEntity> task = new AsyncProcessorTask<UserEntity>(
				this.getActivity(), "登录中，请稍后...",
				new ProcessorTaskCallback<UserEntity>() {

					@Override
					public ProcessResult<UserEntity> doInBackground() {
						return ProcessorFactory.getUserProcessor().login(
								userCodeField.getText().toString(),
								pwdField.getText().toString());
					}

					@Override
					public void onSuccess(UserEntity result) {
						getActivity().setResult(Activity.RESULT_OK);
						back();
					}

					@Override
					public void onFailed(Error error) {
						String type = error.type;
						if (type.equals(FuncConst.VAL_ERROR_USER)) {
							((ErrorAbleEditText) userCodeField).betterSetError("此用户不存在.",
									true);
							// userCodeField.setError("此用户不存在.");
							// userCodeField.requestFocus();
						} else if (type.equals(FuncConst.VAL_ERROR_PWD)) {
							((ErrorAbleEditText) pwdField).betterSetError("密码错误.", true);
							// pwdField.setError("密码错误.");
							// pwdField.requestFocus();
						} else {
							super.onFailed(error);
						}
					}

				});

		task.execute();
	}

	@Override
	protected void back() {
		super.back();
	}

}

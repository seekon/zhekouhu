package com.seekon.zhekouhu.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.AbstractPersonalMainFragment;
import com.seekon.zhekouhu.activity.store.DiscountEditActivity;
import com.seekon.zhekouhu.activity.store.StoreDiscountsActivity;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.user.UserEntity;

public class UserMainFragment extends AbstractPersonalMainFragment {

	public static final int OTHER_REQUEST_CODE = 31;

	private static final int PUB_DISCOUNT_REQUEST_CODE = 32;

	private static final int AWARD_REQUEST_CODE = 33;
	
	@Override
	protected int getContentViewResourceId() {
		return R.layout.fragment_user_main;
	}

	@Override
	protected void updateActionBar() {
		super.updateActionBar();

		setTitle(R.string.title_user_main);
	}

	@Override
	protected void updateDiscountCountView(boolean logined) {
		if (logined) {
			UserEntity user = RunEnv.getInstance().getUser();
			TextView discountCountView = (TextView) mainView
					.findViewById(R.id.t_discount_count);
			discountCountView.setText(ProcessorFactory.getDiscountProcessor()
					.discountCountForOwner(user.getUuid()) + "笔");
		}else {
			TextView discountCountView = (TextView) mainView
					.findViewById(R.id.t_discount_count);
			discountCountView.setText("");
		}
	}

	@Override
	protected void updateFuncView(final boolean isLogined) {
		super.updateFuncView(isLogined);

		((TextView) mainView.findViewById(R.id.t_discount))
				.setText(R.string.label_my_pub_discounts);
		((TextView) mainView.findViewById(R.id.t_favorit))
				.setText(R.string.label_my_favorits);
		((TextView) mainView.findViewById(R.id.t_award_goods))
				.setText(R.string.label_award_goods);
		((TextView) mainView.findViewById(R.id.t_store))
				.setText(R.string.label_store_manager);
		((TextView) mainView.findViewById(R.id.t_other))
				.setText(R.string.label_more);

		// 发布促销活动
		Button publishDiscount = (Button) mainView
				.findViewById(R.id.b_publish_discount);
		publishDiscount.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined || storeList == null || storeList.isEmpty()) {
					openUserPublishDiscountActivity();
					return;
				}

				Intent intent = new Intent(getActivity(), DiscountEditActivity.class);
				intent.putExtra(DataConst.COL_NAME_TYPE,
						FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY);
				startActivityForResult(intent, DISCOUNT_REQUEST_CODE);
			}
		});

		// 发布优惠券
		Button publishCoupon = (Button) mainView
				.findViewById(R.id.b_publish_coupon);
		publishCoupon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined || storeList == null || storeList.isEmpty()) {
					openUserPublishDiscountActivity();
					return;
				}

				Intent intent = new Intent(getActivity(), DiscountEditActivity.class);
				intent.putExtra(DataConst.COL_NAME_TYPE,
						FuncConst.VAL_DISCOUNT_TYPE_COUPON);
				startActivityForResult(intent, DISCOUNT_REQUEST_CODE);
			}
		});

		// 我发的优惠
		View discountRow = mainView.findViewById(R.id.row_discount);
		discountRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isLogined) {
					openLoginActivity();
					return;
				}
				Intent intent = new Intent(getActivity(), StoreDiscountsActivity.class);
				intent.putExtra(DataConst.COL_NAME_TYPE,
						FuncConst.VAL_DISCOUNT_TYPE_ALL);
				startActivityForResult(intent, DISCOUNT_LIST_REQUEST_CODE);
			}
		});

		// 我的收藏
		View favoritRow = mainView.findViewById(R.id.row_favorit);
		favoritRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), UserFavoritsActivity.class);
				startActivity(intent);
			}
		});

		// 积分商城
		View awardGoodsRow = mainView.findViewById(R.id.row_award_goods);
		awardGoodsRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), AwardGoodsActivity.class);
				startActivityForResult(intent, AWARD_REQUEST_CODE);
			}
		});
		// 其他
		View otherRow = mainView.findViewById(R.id.row_other);
		otherRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), UserMoreActivity.class);
				startActivityForResult(intent, OTHER_REQUEST_CODE);
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}

		switch (requestCode) {
		case OTHER_REQUEST_CODE:
		case AWARD_REQUEST_CODE:
			mainView.findViewById(R.id.v_loading).setVisibility(View.VISIBLE);
			mainView.findViewById(R.id.v_data).setVisibility(View.GONE);
			this.updateViews();
			break;
		case PUB_DISCOUNT_REQUEST_CODE:
			if(RunEnv.getInstance().getUser() != null){
				this.updateDiscountCountView(true);
			}
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void openUserPublishDiscountActivity() {
		Intent intent = new Intent(getActivity(), UserEditDiscountActivity.class);
		startActivityForResult(intent, PUB_DISCOUNT_REQUEST_CODE);
	}
}

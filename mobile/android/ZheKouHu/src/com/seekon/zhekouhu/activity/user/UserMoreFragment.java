package com.seekon.zhekouhu.activity.user;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.AbstractMoreFragment;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.utils.ViewUtils;

public class UserMoreFragment extends AbstractMoreFragment {

	private int logoutCount = 0;

	@Override
	public void onResume() {
		logoutCount = 0;
		super.onResume();
	}

	@Override
	protected void updateActionBar() {

	}

	@Override
	protected int getContentViewResourceId() {
		return R.layout.fragment_user_other;
	}

	@Override
	protected void initViews(View view) {
		super.initViews(view);

		boolean logined = RunEnv.getInstance().getUser() != null;

		if (!logined) {
			view.findViewById(R.id.view_user).setVisibility(View.GONE);
		} else {
			View logoutRow = view.findViewById(R.id.row_logout);
			((TextView) view.findViewById(R.id.t_user_logout))
					.setText(R.string.label_logout);
			logoutRow.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					logout();
				}
			});

			View modifyPwdRow = view.findViewById(R.id.row_modify_pwd);
			modifyPwdRow.setVisibility(View.VISIBLE);
			((TextView) view.findViewById(R.id.t_modify_pwd))
					.setText(R.string.label_modify_pwd);
			modifyPwdRow.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), ChangePwdActivity.class);
					startActivity(intent);
				}
			});

			View profileRow = view.findViewById(R.id.row_profile);
			profileRow.setVisibility(View.VISIBLE);
			((TextView) view.findViewById(R.id.t_user_profile))
					.setText(R.string.label_user_profile);
			profileRow.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), UserProfileActivity.class);
					startActivity(intent);
				}
			});
		}
	}

	private void logout() {
		if (logoutCount == 0) {
			logoutCount++;
			ViewUtils
					.showToast(
							getActivity().getResources().getString(
									R.string.msg_user_logout_conf), 50);
			return;
		}
		boolean result = ProcessorFactory.getUserProcessor().logout();
		if (result) {
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
		}
	}
}

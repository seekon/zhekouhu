package com.seekon.zhekouhu.activity.user;

import android.support.v4.app.Fragment;

import com.seekon.zhekouhu.activity.KeyboardFragmentActivity;

public class UserProfileActivity extends KeyboardFragmentActivity{

	@Override
	protected Fragment createFragment() {
		return new UserProfileFragment();
	}

}

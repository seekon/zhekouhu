package com.seekon.zhekouhu.activity.user;

import static com.seekon.zhekouhu.func.FuncConst.NAME_IMAGES;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.user.UserProfileEntity;
import com.seekon.zhekouhu.func.widget.SelectPicturePopupWindow;
import com.seekon.zhekouhu.utils.Logger;
import com.seekon.zhekouhu.utils.ViewUtils;

public class UserProfileFragment extends CustomActionBarFragment {

	private static final String TAG = UserProfileFragment.class.getSimpleName();

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

	private static final int LOAD_IMAGE_ACTIVITY_REQUEST_CODE = 200;

	private static final int PREVIEW_IMAGE_ACTIVITY_REQUEST_CODE = 300;

	private String selectedLogoFileUri = null;// 当前选择的logo图片文件存放的路径

	private PopupWindow popupWindow;

	private View mainView = null;

	private ImageView photoView;
	private TextView codeView;
	private EditText nameView;
	private EditText phoneView;
	private EditText addrView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mainView = inflater.inflate(R.layout.fragment_user_profile, container,
				false);
		initViews();
		return mainView;
	}

	private void initViews() {
		if (mainView == null) {
			return;
		}

		photoView = (ImageView) mainView.findViewById(R.id.img_photo);
		photoView.setAdjustViewBounds(false);
		photoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		photoView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				logoImageClicked();
			}
		});

		codeView = (TextView) mainView.findViewById(R.id.e_user_code);
		codeView.setFocusable(false);

		nameView = (EditText) mainView.findViewById(R.id.e_user_name);
		phoneView = (EditText) mainView.findViewById(R.id.e_user_phone);
		addrView = (EditText) mainView.findViewById(R.id.e_deliver_addr);

		Button btnSave = (Button) mainView.findViewById(R.id.b_save);
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveUserProfile();
			}
		});

		updateViewData();
	}

	private void updateViewData() {
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				UserProfileEntity profile = null;
				UserEntity user = RunEnv.getInstance().getUser();
				if (user != null) {
					profile = user.getProfile();
					codeView.setText(user.getCode());
				} else {
					codeView.setText("");
				}

				if (profile == null) {
					profile = new UserProfileEntity();
				}
				String photo = profile.getPhoto().getAliasName();
				if (photo != null && photo.trim().length() > 0) {
					ImageLoader.getInstance().displayImage(photo, photoView, true);
				}
				nameView.setText(profile.getName());
				phoneView.setText(profile.getPhone());
				addrView.setText(profile.getDeliverAddr());
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_OK) {
			return;
		}
		switch (requestCode) {
		case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
			showLogo();
			break;
		case LOAD_IMAGE_ACTIVITY_REQUEST_CODE:
			if (null != data) {
				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = null;
				try {
					cursor = getActivity().getContentResolver().query(selectedImage,
							filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					selectedLogoFileUri = cursor.getString(columnIndex);
					showLogo();
				} finally {
					cursor.close();
				}
			}
			break;
		case PREVIEW_IMAGE_ACTIVITY_REQUEST_CODE:
			if (data != null) {
				List<FileEntity> files = (List<FileEntity>) data
						.getSerializableExtra(NAME_IMAGES);
				if (files.size() == 0) {
					selectedLogoFileUri = null;
					ViewUtils.cleanImageView(photoView);

					photoView.setImageResource(R.drawable.add_image);
				}
			}
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void logoImageClicked() {
		showPopupWindow(this.photoView);
	}

	private void showLogo() {
		if (selectedLogoFileUri != null) {
			ViewUtils.cleanImageView(photoView);
			photoView.setImageBitmap(FileHelper.decodeFile(selectedLogoFileUri,
					false, 0, 0));
		}
	}

	private void showPopupWindow(View v) {
		if (popupWindow == null) {
			popupWindow = new SelectPicturePopupWindow(getActivity(),
					new OnClickListener() {

						@Override
						public void onClick(View v) {
							popupWindow.dismiss();
							switch (v.getId()) {
							case R.id.v_camera:
								openCamera();
								break;
							case R.id.v_photo:
								openImageDir();
								break;
							default:
								break;
							}
						}
					});
		}
		popupWindow.showAtLocation(v, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL,
				0, 0); // 设置layout在PopupWindow中显示的位置
	}

	private void openCamera() {
		Logger.debug(TAG, "openCamera");
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File file = FileHelper
				.getFileFromCache(System.currentTimeMillis() + ".png");
		selectedLogoFileUri = file.getPath();
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

		startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	private void openImageDir() {
		Logger.debug(TAG, "openImageDir");
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, LOAD_IMAGE_ACTIVITY_REQUEST_CODE);
	}

	private void saveUserProfile() {
		UserEntity user = RunEnv.getInstance().getUser();
		final UserProfileEntity profile = user.getProfile();
		if (selectedLogoFileUri != null) {
			profile.setPhoto(new FileEntity(selectedLogoFileUri, user.getCode()
					+ "_profile_" + System.currentTimeMillis() + ".png"));
		}
		String name = nameView.getText().toString();
		if (!TextUtils.isEmpty(name)) {
			profile.setName(name);
		}
		String phone = phoneView.getText().toString();
		if (!TextUtils.isEmpty(phone)) {
			profile.setPhone(phone);
		}
		String addr = addrView.getText().toString();
		if (!TextUtils.isEmpty(addr)) {
			profile.setDeliverAddr(addr);
		}

		AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
				getActivity(), "正在保存个人信息...", new ProcessorTaskCallback<Boolean>() {

					@Override
					public ProcessResult<Boolean> doInBackground() {
						return ProcessorFactory.getUserProcessor().updateUserProfile(
								profile);
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							ViewUtils.showToast("保存成功.");
						} else {
							ViewUtils.showToast("保存失败.");
						}
					}
				});
		task.execute();
	}
}

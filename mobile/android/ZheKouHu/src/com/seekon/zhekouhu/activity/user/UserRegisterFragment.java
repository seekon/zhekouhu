package com.seekon.zhekouhu.activity.user;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.CustomActionBarFragment;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.seekon.zhekouhu.validation.AbstractValueValidator;
import com.seekon.zhekouhu.validation.ErrorAbleEditText;
import com.seekon.zhekouhu.validation.LogicOrValidator;
import com.seekon.zhekouhu.validation.ValidationManager;
import com.seekon.zhekouhu.validation.validator.EmailValueValidator;
import com.seekon.zhekouhu.validation.validator.LengthValidator;
import com.seekon.zhekouhu.validation.validator.PhoneValueValidator;

public class UserRegisterFragment extends CustomActionBarFragment {

	private EditText codeText;
	private EditText pwdText;
	private EditText pwdConfText;

	private ValidationManager mValidationManager;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_user_register, container,
				false);
		initViews(view);

		return view;
	}

	private void initViews(View view) {
		codeText = (EditText) view.findViewById(R.id.e_user_code);
		pwdText = (EditText) view.findViewById(R.id.e_pwd);
		pwdConfText = (EditText) view.findViewById(R.id.e_pwd_conf);

		Button registerButton = (Button) view.findViewById(R.id.b_register);
		registerButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				register();
			}
		});

		mValidationManager = new ValidationManager(getActivity());
		mValidationManager.add("pwd", new LengthValidator(pwdText, "密码必须4~10位", 4,
				10));

		LogicOrValidator accountValidator = new LogicOrValidator(codeText,
				new AbstractValueValidator[] { new EmailValueValidator(codeText, ""),
						new PhoneValueValidator(codeText, "") }, "请正确输入电子邮箱/手机号");
		mValidationManager.add("account", accountValidator);
	}

	private void register() {
		final String code = codeText.getText().toString();
		final String pwd = pwdText.getText().toString();
		String pwdConf = pwdConfText.getText().toString();

		boolean cancel = !mValidationManager.validateAllAndSetError();
		if (!pwd.equals(pwdConf)) {
			((ErrorAbleEditText) pwdConfText).betterSetError("两次输入密码不一致.", !cancel);
			cancel = true;
		}

		if (cancel) {
			return;
		}

		ViewUtils.hideInputMethodWindow(getActivity());
		AsyncProcessorTask<UserEntity> task = new AsyncProcessorTask<UserEntity>(
				getActivity(), "注册中...", new ProcessorTaskCallback<UserEntity>() {

					@Override
					public ProcessResult<UserEntity> doInBackground() {
						return ProcessorFactory.getUserProcessor().register(code, pwd);
					}

					@Override
					public void onSuccess(UserEntity result) {
						if (result != null) {
							getActivity().setResult(Activity.RESULT_OK);
							getActivity().finish();
						}
					}

					@Override
					public void onFailed(Error error) {
						if (error != null && error.type != null
								&& error.type.equals("user-type")) {
							((ErrorAbleEditText) codeText).betterSetError("此账户已注册.", true);
							return;
						}
						super.onFailed(error);
					}

				});
		task.execute();
	}

}

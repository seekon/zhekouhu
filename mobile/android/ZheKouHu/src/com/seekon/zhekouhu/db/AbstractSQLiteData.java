package com.seekon.zhekouhu.db;

import java.util.ArrayList;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.utils.Logger;

public abstract class AbstractSQLiteData<T> {

	protected final String TAG = AbstractSQLiteData.class.getSimpleName();

	private SQLiteOpenHelper dbHelper;

	public AbstractSQLiteData() {
		super();
		dbHelper = new ZKHDBHelper(Application.getAppContext());
	}

	public SQLiteDatabase getReadableDatabase() {
		return dbHelper.getReadableDatabase();
	}

	public SQLiteDatabase getWritableDatabase() {
		return dbHelper.getWritableDatabase();
	}

	protected ArrayList<T> query(String sql, String[] params) {
		ArrayList<T> result = new ArrayList<T>();
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = null;
		try {
			Logger.debug(TAG, sql);
			cursor = db.rawQuery(sql, params);
			while (cursor.moveToNext()) {
				T object = this.processRow(cursor);
				if (object != null) {
					result.add(object);
				}
			}
		} catch (Exception e) {
			Logger.error(TAG, e.getMessage(), e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}
		return result;
	}

	protected T queryOne(String sql, String[] params) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = null;
		try {
			Logger.debug(TAG, sql);
			cursor = db.rawQuery(sql, params);
			if (cursor.moveToNext()) {
				return this.processRow(cursor);
			}
		} catch (Exception e) {
			Logger.error(TAG, e.getMessage(), e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}
		return null;
	}

	protected int queryCount(String sql, String[] params) {
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = null;
		try {
			Logger.debug(TAG, sql);
			cursor = db.rawQuery(sql, params);
			if (cursor.moveToNext()) {
				return cursor.getInt(0);
			}
		} catch (Exception e) {
			Logger.error(TAG, e.getMessage(), e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}
		return 0;
	}

	protected void executeUpdate(String sql, Object[] params) {
		SQLiteDatabase db = null;
		try {
			Logger.debug(TAG, sql);
			db = getWritableDatabase();
			if (params == null) {
				db.execSQL(sql);
			} else {
				db.execSQL(sql, params);
			}
		} catch (Exception e) {
			Logger.error(TAG, e.getMessage(), e);
		} finally {
			if (db != null) {
				db.close();
			}
		}
	}

	protected abstract T processRow(Cursor cursor);
}

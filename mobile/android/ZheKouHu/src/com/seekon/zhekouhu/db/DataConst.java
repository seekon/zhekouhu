package com.seekon.zhekouhu.db;

public interface DataConst {

	public static final String TABLE_SYNC = "z_update";

	public static final String TABLE_CATEGORY = "z_category";

	public static final String TABLE_USER = "z_user";

	public static final String TABLE_USER_PROFILE = "z_user_profile";
	
	public static final String TABLE_USER_FAVORIT = "z_user_favorit";

	public static final String TABLE_ENV = "z_env";

	public static final String TABLE_MESSAGE = "z_message";

	public static final String TABLE_SETTING = "z_setting";

	public static final String TABLE_STORE = "z_store";

	public static final String TABLE_STOREFRONT = "z_storefront";

	public static final String TABLE_DISCOUNT = "z_discount";

	public static final String TABLE_DISCOUNT_IMG = "z_discount_img";

	public static final String TABLE_DISCOUNT_STOREFRONT = "z_discount_storefront";

	public static final String TABLE_CITY = "z_city";

	public static final String TABLE_COUPON_STORE = "z_coupon_store";

	public static final String COL_NAME_UUID = "uuid";

	public static final String COL_NAME_CODE = "code";

	public static final String COL_NAME_NAME = "name";

	public static final String COL_NAME_ICON = "icon";

	public static final String COL_NAME_PWD = "pwd";

	public static final String COL_NAME_VALUE = "value";

	public static final String COL_NAME_REGISTER_TIME = "register_time";

	public static final String COL_NAME_TYPE = "type";

	public static final String COL_NAME_LOGO = "logo";

	public static final String COL_NAME_OWNER = "owner";

	public static final String COL_NAME_STORE_ID = "store_id";

	public static final String COL_NAME_STORE_NAME = "store_name";

	public static final String COL_NAME_CITY = "city";

	public static final String COL_NAME_CITY_NAME = "city_name";

	public static final String COL_NAME_ADDR = "addr";

	public static final String COL_NAME_PHONE = "phone";

	public static final String COL_NAME_LATITUDE = "latitude";

	public static final String COL_NAME_LONGITUDE = "longitude";

	public static final String COL_NAME_ORD_INDEX = "ord_index";

	public static final String COL_NAME_PARENT_ID = "parent_id";

	public static final String COL_NAME_UPDATE_TIME = "update_time";

	public static final String COL_NAME_STATUS = "status";

	public static final String COL_NAME_START_DATE = "start_date";

	public static final String COL_NAME_END_DATE = "end_date";

	public static final String COL_NAME_VISIT_COUNT = "visit_count";

	public static final String COL_NAME_CATEGORY_ID = "category_id";

	public static final String COL_NAME_PUBLISHER = "publisher";

	public static final String COL_NAME_PUBLISH_DATE = "publish_date";

	public static final String COL_NAME_PUBLISH_TIME = "publish_time";

	public static final String COL_NAME_CONTENT = "content";

	public static final String COL_NAME_DISCOUNT_ID = "discount_id";

	public static final String COL_NAME_IMG = "img";

	public static final String COL_NAME_STOREFRONT_ID = "storefront_id";

	public static final String COL_NAME_FIRST_LETTER = "first_letter";

	public static final String COL_NAME_LAT_SIN_LNG_COS = "lat_sin_lng_cos";

	public static final String COL_NAME_LAT_SIN_LNG_SIN = "lat_sin_lng_sin";

	public static final String COL_NAME_LAT_COS = "lat_cos";

	public static final String COL_NAME_LAST_MODIFY_TIME = "last_modify_time";

	public static final String COL_NAME_PAR_VALUE = "par_value";

	public static final String COL_NAME_SEND_TIME = "send_time";

	public static final String COL_NAME_MSG_ID = "msg_id";

	public static final String COL_NAME_USER_ID = "user_id";

	public static final String COL_NAME_DEV_ID = "dev_id";
	
	public static final String COL_NAME_ORIGIN = "origin";
	
	public static final String COL_NAME_LOCATION = "location";
	
	public static final String COL_NAME_AUDIT_IDEA = "audit_idea";
	
	public static final String COL_NAME_REMAIN = "remain";
	
	public static final String COL_NAME_COIN_VAL = "coin_val";
	
	public static final String COL_NAME_DESC = "desc";
	
	public static final String COL_NAME_PHOTO = "photo";
	
	public static final String COL_NAME_SEX = "sex";
	
	public static final String COL_NAME_DELIVER_ADDR = "deliver_addr";
	
	public static final String COL_NAME_AWARD_SUM = "award_sum";
	
	public static final String COL_NAME_AWARD_USED = "award_used";
	
	public static final String COL_NAME_DATA_ID = "data_id";
	
	public static final String COL_NAME_COMMENT = "comment";
	
	public static final String COL_NAME_AWARD_TIME = "award_time";
}

package com.seekon.zhekouhu.db;

public interface DataUpdater<T> {

	public int save(T[] data);

}

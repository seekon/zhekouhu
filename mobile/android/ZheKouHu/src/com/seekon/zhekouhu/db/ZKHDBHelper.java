package com.seekon.zhekouhu.db;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.seekon.zhekouhu.utils.Logger;

public class ZKHDBHelper extends SQLiteOpenHelper {

	private static final String TAG = ZKHDBHelper.class.getSimpleName();

	private static final String CREATE_DB_SQL = "create_db.sql";

	private static final String DATABASE_NAME = "zhekouhu.sqlite";

	private static final int DATABASE_VERSION = 110;

	public ZKHDBHelper(Context context) {

		super(context, DATABASE_NAME, MyCursorFactory.getInstance(),
				DATABASE_VERSION);
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		List<String> sqls = this.getCreateSqlList();
		for (String sql : sqls) {
			db.execSQL(sql);
		}

		if (DATABASE_VERSION > 1) {
			exeUpgrade(db, 1, DATABASE_VERSION);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		exeUpgrade(db, oldVersion, newVersion);
	}

	private void exeUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		String fileName = "upgrade_" + newVersion + "_" + oldVersion + ".sql";
		List<String> sqls = this.getSqlList(fileName);
		for (String sql : sqls) {
			db.execSQL(sql);
		}
	}

	/**
	 * 从create_db.sql文件中获取见表语句
	 * 
	 * @return
	 */
	private List<String> getCreateSqlList() {
		return getSqlList(CREATE_DB_SQL);
	}

	private List<String> getSqlList(String fileName) {
		List<String> sqls = new ArrayList<String>();

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					ZKHDBHelper.class.getResourceAsStream(fileName)));
			String sql = null;
			while ((sql = reader.readLine()) != null) {
				if (sql.trim().length() > 0) {
					sqls.add(sql);
				}
			}
		} catch (Exception e) {
			Logger.error(TAG, e.getMessage(), e);
			throw new RuntimeException(e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Exception ee) {
					Logger.debug(TAG, ee.getMessage(), ee);
				}
			}
		}

		return sqls;
	}
}

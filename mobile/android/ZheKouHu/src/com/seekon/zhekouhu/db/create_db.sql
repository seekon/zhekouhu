CREATE TABLE IF NOT EXISTS z_category (uuid text primary key, name text, icon text, parent_id text, ord_index integer);

CREATE TABLE IF NOT EXISTS z_discount (uuid text primary key, content text, par_value real(16,2) DEFAULT 0, start_date text, end_date text, type text, category_id text, publisher text, publish_date text, publish_time text, visit_count text);

CREATE TABLE IF NOT EXISTS z_discount_img (uuid text primary key, discount_id text, img text, ord_index text);

CREATE TABLE IF NOT EXISTS z_discount_storefront (uuid text primary key, discount_id text, storefront_id text, store_id text, ord_index text);

CREATE TABLE IF NOT EXISTS z_env (code text primary key, login_setting text, last_modify_time text);

CREATE TABLE IF NOT EXISTS z_setting (code text primary key, value text);

CREATE TABLE IF NOT EXISTS z_store (uuid text primary key, name text, logo text, owner text, register_time text);

CREATE TABLE IF NOT EXISTS z_storefront (uuid text primary key, store_id text, city text, city_name text, name text, addr text, phone text, latitude text, longitude text, lat_sin_lng_cos text, lat_sin_lng_sin text, lat_cos text);

CREATE TABLE IF NOT EXISTS z_update (uuid text primary key, table_name text, item_id text, update_time text);

CREATE TABLE IF NOT EXISTS z_user (uuid text primary key, code text, pwd text, register_time text);

CREATE TABLE IF NOT EXISTS z_city (uuid text primary key, name text, first_letter text);

CREATE TABLE IF NOT EXISTS z_coupon_store (uuid text primary key, name text, logo text, store_id text, category_id text, city_name text);

CREATE TABLE IF NOT EXISTS z_message (uuid text primary key, msg_id text, content text, type text, send_time text, is_deleted text DEFAULT '0');

insert or replace into z_setting(code, value) values('version', '1.0.0');
insert or replace into z_setting(code, value) values('share_sina', '{"code":"sina","img":"logo_sina","name":"新浪微博","status":"0"}');

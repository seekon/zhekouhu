CREATE TABLE IF NOT EXISTS z_discount_comment (uuid text PRIMARY KEY,discount_id text,content text,publisher text,publish_time text,type text DEFAULT '0');

CREATE TABLE IF NOT EXISTS z_user_favorit (uuid text PRIMARY KEY,user_id text,discount_id text);

CREATE TABLE IF NOT EXISTS z_user_profile (uuid text PRIMARY KEY,user_id text,photo text ,name text,sex text,phone text,deliver_addr text,award_sum integer DEFAULT '0',award_used integer '0');

alter table z_discount add origin text default '0';
alter table z_discount add status text;
alter table z_discount add location text;
alter table z_discount add audit_idea text;

update z_discount set status = '1';
update z_setting set value='1.1.0' where code='version';

package com.seekon.zhekouhu.file;

import java.io.File;

import com.seekon.zhekouhu.Application;

public class FileCache {

	private File cacheDir;

	private static FileCache instance = null;

	private static Object lock = new Object();

	public static FileCache getInstance() {
		synchronized (lock) {
			if (instance == null) {
				instance = new FileCache();
			}
		}
		return instance;
	}

	private FileCache() {
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED)) {
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(),
					"zhekouhu/caches");
		} else {
			cacheDir = Application.getAppContext().getCacheDir();
		}
		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
	}

	public File getFile(String url) {
		String filename = String.valueOf(url.hashCode());
		File f = new File(cacheDir, filename);
		return f;

	}

	public File getCacheDir() {
		return this.cacheDir;
	}

	public void clear() {
		File[] files = cacheDir.listFiles();
		if (files == null)
			return;
		for (File f : files)
			f.delete();
	}
}

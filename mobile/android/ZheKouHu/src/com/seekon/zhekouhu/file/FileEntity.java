package com.seekon.zhekouhu.file;

import java.io.File;
import java.io.Serializable;

public class FileEntity implements Serializable {

	private static final long serialVersionUID = -6835455636138044256L;

	protected String fileUri;// 完整的文件路径（含文件名）

	protected String aliasName;// 文件的别名

	public FileEntity() {
		super();
	}

	public FileEntity(String fileUri, String aliasName) {
		this.fileUri = fileUri;
		this.aliasName = aliasName;
	}

	public String getFileUri() {
		return fileUri;
	}

	public void setFileUri(String fileUri) {
		this.fileUri = fileUri;
	}

	public String getAliasName() {
		if (aliasName == null && fileUri != null) {
			aliasName = new File(fileUri).getName();
		}
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aliasName == null) ? 0 : aliasName.hashCode());
		result = prime * result + ((fileUri == null) ? 0 : fileUri.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileEntity other = (FileEntity) obj;
		if (aliasName == null) {
			if (other.aliasName != null)
				return false;
		} else if (!aliasName.equals(other.aliasName))
			return false;
		if (fileUri == null) {
			if (other.fileUri != null)
				return false;
		} else if (!fileUri.equals(other.fileUri))
			return false;
		return true;
	}

}

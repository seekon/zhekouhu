package com.seekon.zhekouhu.func;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.rest.AsyncRestRequestTask;
import com.seekon.zhekouhu.rest.RestStatus;
import com.seekon.zhekouhu.utils.Logger;
import com.seekon.zhekouhu.utils.ViewUtils;

public class AsyncProcessorTask<T> extends
		AsyncTask<Void, Void, ProcessResult<T>> {

	private final static String TAG = AsyncRestRequestTask.class.getSimpleName();

	private Context context;

	private ProcessorTaskCallback<T> callback;

	private Dialog progressDialog;

	private String progressMessage;

	public AsyncProcessorTask(Context context, ProcessorTaskCallback<T> callback) {
		this(context, null, callback);
	}

	public AsyncProcessorTask(Context context, String progressMessage,
			ProcessorTaskCallback<T> callback) {
		super();
		this.context = context;
		this.callback = callback;
		this.progressMessage = progressMessage;
		if (callback == null) {
			throw new RuntimeException("AsyncRestRequestTask构造函数必须传递callback参数.");
		}
	}

	@Override
	protected void onPreExecute() {
		if (progressMessage != null && progressMessage.trim().length() > 0) {
			if (progressDialog == null) {
				progressDialog = ViewUtils.createProgreddDialog(context,
						progressMessage);
			}
			progressDialog.show();

		}

		super.onPreExecute();
	}

	@Override
	protected ProcessResult<T> doInBackground(Void... params) {
		try {
			return callback.doInBackground();
		} catch (Throwable e) {
			Logger.warn(TAG, e.getMessage());
			return new ProcessResult<T>(RestStatus.RUNTIME_ERROR + "",
					context.getString(R.string.runtime_error));
		}
	}

	@Override
	protected void onPostExecute(ProcessResult<T> result) {
		if (progressDialog != null) {
			try {
				progressDialog.dismiss();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (result.isSuccess()) {
			callback.onSuccess(result.getSuccessObj());
		} else {
			callback.onFailed(result.getError());
		}
	}

	@Override
	protected void onCancelled() {
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		callback.onCancelled();
	}

	public void execute() {
		this.execute((Void) null);
	}
}

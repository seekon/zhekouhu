package com.seekon.zhekouhu.func;

import java.util.ArrayList;
import java.util.List;

import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.rest.sercurity.RequestSigner;

public class DefaultRequestSigner implements RequestSigner {

	@Override
	public boolean authorize(Request request) {
		if (request.getMethod() == Method.GET) {
			return true;
		}

		String sessionId = RunEnv.getInstance().getSessionId();
		if (sessionId == null || sessionId.length() == 0) {
			UserEntity user = RunEnv.getInstance().getUser();
			if (user == null) {
				return false;
			}

			// 尝试先登录
			ProcessorFactory.getUserProcessor().remoteLogin(user.getCode(),
					user.getPwd());
			sessionId = RunEnv.getInstance().getSessionId();
			if (sessionId == null || sessionId.length() == 0) {
				return false;
			}
		}
		List<String> cookies = new ArrayList<String>();
		cookies.add(sessionId);
		request.addHeader("cookie", cookies);
		return true;
	}

}

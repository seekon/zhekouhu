package com.seekon.zhekouhu.func;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.discount.ActionMenu;

public interface FuncConst {

	public static final String NAME_ERROR_TYPE = "error-type";
	public static final String VAL_ERROR_USER = "user-error";
	public static final String VAL_ERROR_PWD = "pass-error";

	public static final String NAME_AUTHED = "authed";
	public static final String NAME_USER = "user";
	public static final String NAME_PROFILE = "profile";
	
	public static final String NAME_CATEGORY = "category";

	public static final String NAME_STORE = "store";
	public static final String NAME_STOREFRONT = "storefront";
	public static final String NAME_NEABY_STORE = "nearbyStore";

	public static final String NAME_CONTENT = "content";
	public static final String NAME_CONTACT = "contact";

	public static final String NAME_DISCOUNT = "discount";

	public static final String NAME_DISCOUNTS = "discounts";

	public static final String NAME_COMMENTS = "comments";

	public static final String NAME_FAVORIT = "favorit";

	public static final String NAME_KEY_ID = "key_id";

	public static final String NAME_DISCOUNT_STOREFRONTS = "discount_storefronts";

	public static final String NAME_IMAGES = "images";

	public static final String NAME_FILE = "file";

	public static final String NAME_MESSAGE = "message";

	public static final String NAME_STORES = "stores";
	public static final String NAME_STOREFRONTS = "storefronts";
	public static final String NAME_CITY = "city";
	public static final String NAME_CITIES = "cities";
	public static final String NAME_DEL_IMAGES = "del_images";
	public static final String NAME_DEL_STOREFRONTS = "del_storefronts";

	public static final String NAME_DATA = "data";
	public static final String NAME_IS_DELETED = "is_deleted";

	public static final String NAME_COMMENT_COUNT = "commentCount";
	public static final String NAME_FRONT_COUNT = "frontCount";
	public static final String NAME_DISTANCE = "distance";

	public static final String KEY_SETTING_STORE_REGISTERED = "store_registered";
	public static final String KEY_SETTING_MESSAGE_REMIND = "message_remind";
	public static final String KEY_SETTING_VERSION = "version";

	public static final String EXTRA_TITLE = "title";
	public static final String EXTRA_URI = "uri";

	public static final String VAL_DISCOUNT_TYPE_ACTIVITY = "0";
	public static final String VAL_DISCOUNT_TYPE_COUPON = "1";
	public static final String VAL_DISCOUNT_TYPE_ALL = "9";
	
	public static final String VAL_DISCOUNT_ORIGIN_STORER = "0";
	public static final String VAL_DISCOUNT_ORIGIN_CUSTOMER = "1";
	
	public static final String VAL_DISCOUNT_STATUS_VALID = "1";
	public static final String VAL_DISCOUNT_STATUS_CHECKING = "2";
	public static final String VAL_DISCOUNT_STATUS_INVALID = "8";
	public static final String VAL_DISCOUNT_STATUS_DELETED = "9";
	
	public static final String EXTRA_CURRENT_INDEX = "current.index";
	public static final String EXTRA_FLAG_READONLY = "flag.readonly";
	public static final String EXTRA_FLAG_HOME_AS_UP = "flag.homeAsUp";
	public static final String EXTRA_ACTION_DELEGATE = "action.delegate";
	public static final String EXTRA_DATE = "extra.date";
	public static final String EXTRA_SCALE_TYPE = "scaleType";
	public static final String EXTRA_LAST = "last";

	public static final String NAME_OFF_SET = "offset";
	public static final String NAME_LAT = "lat";
	public static final String NAME_LNG = "lng";

	public static final String SYNC_TABLE_COUPON_MAIN = "COUPON_MAIN";
	public static final String SYNC_TABLE_STORE_COUPON = "STORE_COUPON";
	public static final String SYNC_TABLE_ACTIVITIES_BY_OWNER = "ACTIVITIES_BY_OWNER";
	public static final String SYNC_TABLE_COUPONS_BY_OWNER = "COUPONS_BY_OWNER";
	public static final String SYNC_TABLE_DISCOUNTS_BY_OWNER = "DISCOUNTS_BY_OWNER";
	public static final String SYNC_TABLE_FAVORIT_DISCOUNT = "FAVORIT_DISCOUNT";

	public static final String MSG_TYPE_SYSTEM = "1";
	public static final String MSG_TYPE_VERSION = "2";
	public static final String MSG_TYPE_DISCOUNT = "3";

	public static final String NAME_ACTIVITY_CLASS = "activity.class";
	public static final String NAME_POP_TO_ROOT = "popToRoot";

	public static final String NAME_DEVICE_ID = "dev_id";

	public static final String KEY_SHARE_PREFER_DEV_ID = "key.sharePrefer.dev.id";

	public static final String KEY_STATE_FIELD_PREFIX = "zhk.object.field_";
	public static final String KEY_SAVE_FIELD_STATE = "key.save.field.state";

	public static final String KEY_LOCATION = "key.location";

	public static final String KEY_USER_GUID_LAUNCHED = "user.guid.launched";

	public static final int KEY_DEF_DISCOUNT_FRONTS_SHOW_NUM = 3;

	public static final int KEY_DEF_DISCOUNT_COMMENTS_SHOW_NUM = 3;

	public static final String KEY_TYPE_DIS_COMMENT = "0";

	public static final String KEY_TYPE_DIS_FAULT = "1";

	public static final String KEY_ACTION_FAVORIT = "0";

	public static final String KEY_ACTION_UNFAVORIT = "1";

	public static final String KEY_BD_UID = "uid";
	public static final String KEY_BD_NAME = "name";
	public static final String KEY_BD_ADDR = "address";
	public static final String KEY_BD_LOCATION = "location";
	public static final String KEY_BD_LAT = "lat";
	public static final String KEY_BD_LNG = "lng";
	public static final String KEY_BD_PHONE = "telephone";
	
	public static final String NAME_ORIGIN = "origin";

	public static final int DISCOUNT_ORIGIN_DEFAULT = 0;
	public static final int DISCOUNT_ORIGIN_FAVORIT = 1;

	public static final String VAL_USER_TYPE_STORER = "0";
	public static final String VAL_USER_TYPE_CUSTOMER = "1";
	
	public static final ActionMenu ACTION_MENU_UNFAVORIT = new ActionMenu(
			R.drawable.menu_unfavorit, "取消收藏");
	public static final ActionMenu ACTION_MENU_FAVORIT = new ActionMenu(
			R.drawable.menu_favorit, "收藏");
	public static final ActionMenu ACTION_MENU_SHARE = new ActionMenu(
			R.drawable.menu_share, "分享");
	public static final ActionMenu ACTION_MENU_FAULT = new ActionMenu(
			R.drawable.menu_fault, "纠错");
}

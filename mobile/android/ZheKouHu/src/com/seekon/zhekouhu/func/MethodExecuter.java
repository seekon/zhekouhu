package com.seekon.zhekouhu.func;

import com.seekon.zhekouhu.rest.RestMethod;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.RestStatus;
import com.seekon.zhekouhu.rest.resource.Resource;
import com.seekon.zhekouhu.utils.Logger;

public abstract class MethodExecuter<T> {

	private static final String TAG = MethodExecuter.class.getSimpleName();

	public ProcessResult<T> execute(RestMethod<? extends Resource> method) {
		ProcessResult<T> result = null;
		RestMethodResult<?> methodResult = method.execute();
		if (methodResult.getStatusCode() == RestStatus.SC_OK) {
			result = new ProcessResult<T>();
			try {
				result.setSuccess(true);
				this.doSuccess(methodResult, result);
			} catch (Exception e) {
				Logger.error(TAG, e.getMessage(), e);
				throw new RuntimeException(e);
			}
		} else {
			result = new ProcessResult<T>(methodResult.getStatusCode() + "",
					methodResult.getStatusMsg());
		}
		return result;
	}

	public abstract void doSuccess(RestMethodResult<?> mResult,
			ProcessResult<T> result) throws Exception;
}

package com.seekon.zhekouhu.func;

import java.io.Serializable;

public class ProcessResult<T> implements Serializable {

	private static final long serialVersionUID = -633411770606651770L;

	private boolean success;

	private Error error;

	private T successObj;

	public ProcessResult() {
		super();
	}

	public ProcessResult(Error error) {
		super();
		this.success = false;
		this.error = error;
	}

	public ProcessResult(String errorType, String errorMsg) {
		super();
		this.success = false;
		this.error = new Error(errorType, errorMsg);
	}

	public ProcessResult(T successObj) {
		super();
		this.success = true;
		this.successObj = successObj;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Error getError() {
		return error;
	}

	public void setError(String errorType, String errorMsg) {
		this.success = false;
		this.error = new Error(errorType, errorMsg);
	}

	public void setError(Error error) {
		this.error = error;
	}

	public T getSuccessObj() {
		return successObj;
	}

	public void setSuccessObj(T successObj) {
		this.successObj = successObj;
	}

	public static class Error {
		public String type;
		public String message;

		public Error(String type, String message) {
			super();
			this.type = type;
			this.message = message;
		}
	}
}

package com.seekon.zhekouhu.func;

import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.func.award.AwardProcessor;
import com.seekon.zhekouhu.func.category.CategoryProcessor;
import com.seekon.zhekouhu.func.discount.DiscountProcessor;
import com.seekon.zhekouhu.func.feedback.FeedbackProcessor;
import com.seekon.zhekouhu.func.message.MessageProcessor;
import com.seekon.zhekouhu.func.setting.SettingProcessor;
import com.seekon.zhekouhu.func.spi.IAwardProcessor;
import com.seekon.zhekouhu.func.spi.ICategoryProcessor;
import com.seekon.zhekouhu.func.spi.IDiscountProcessor;
import com.seekon.zhekouhu.func.spi.IFeedbackProcessor;
import com.seekon.zhekouhu.func.spi.IMessageProcessor;
import com.seekon.zhekouhu.func.spi.ISettingProcessor;
import com.seekon.zhekouhu.func.spi.IStoreProcessor;
import com.seekon.zhekouhu.func.spi.ISyncProcessor;
import com.seekon.zhekouhu.func.spi.IUserProcessor;
import com.seekon.zhekouhu.func.spi.IVersionProcessor;
import com.seekon.zhekouhu.func.store.StoreProcessor;
import com.seekon.zhekouhu.func.sync.SyncProcessor;
import com.seekon.zhekouhu.func.user.UserProcessor;
import com.seekon.zhekouhu.func.version.VersionProcessor;
import com.seekon.zhekouhu.utils.Logger;

public class ProcessorFactory {

	private static final String TAG = ProcessorFactory.class.getSimpleName();

	private static Map<Class<?>, Object> cache = new HashMap<Class<?>, Object>();

	public final static String TYPE_USER = "user";
	public final static String TYPE_SETTING = "setting";
	public final static String TYPE_FEEDBACK = "feedback";
	public final static String TYPE_STORE = "store";
	public final static String TYPE_CATEGORY = "category";
	public final static String TYPE_DISCOUNT = "discount";
	public final static String TYPE_VERSION = "version";
	public final static String TYPE_SYNC = "sync";
	public final static String TYPE_MESSAGE = "message";
	public final static String TYPE_AWARD = "award";
	
	@SuppressWarnings("unchecked")
	public static <T, C> T getProcessor(Class<T> t, String type) {
		T object = (T) cache.get(t);
		if (object == null) {
			try {
				Object obj = null;
				if (type.equals(TYPE_USER)) {
					obj = new UserProcessor();
				} else if (type.equals(TYPE_SETTING)) {
					obj = new SettingProcessor();
				} else if (type.equals(TYPE_FEEDBACK)) {
					obj = new FeedbackProcessor();
				} else if (type.equals(TYPE_STORE)) {
					obj = new StoreProcessor();
				} else if (type.equals(TYPE_CATEGORY)) {
					obj = new CategoryProcessor();
				} else if (type.equals(TYPE_DISCOUNT)) {
					obj = new DiscountProcessor();
				} else if (type.equals(TYPE_VERSION)) {
					obj = new VersionProcessor();
				} else if (type.equals(TYPE_SYNC)) {
					obj = new SyncProcessor();
				} else if (type.equals(TYPE_MESSAGE)) {
					obj = new MessageProcessor();
				}else if (type.equals(TYPE_AWARD)) {
					obj = new AwardProcessor();
				}

				if (obj != null) {
					ProcessorProxy proxy = new ProcessorProxy();
					object = (T) proxy.bind(obj);
					cache.put(t, object);
				} else {
					throw new RuntimeException("ProcessorFactory根据type创建Processor对象失败.");
				}

			} catch (Exception e) {
				Logger.error(TAG, e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}

		return object;
	}

	public static IUserProcessor getUserProcessor() {
		return getProcessor(IUserProcessor.class, TYPE_USER);
	}

	public static ISettingProcessor getSettingProcessor() {
		return getProcessor(ISettingProcessor.class, TYPE_SETTING);
	}

	public static IFeedbackProcessor getFeedbackProcessor() {
		return getProcessor(IFeedbackProcessor.class, TYPE_FEEDBACK);
	}

	public static IStoreProcessor getStoreProcessor() {
		return getProcessor(IStoreProcessor.class, TYPE_STORE);
	}

	public static ICategoryProcessor getCategoryProcessor() {
		return getProcessor(ICategoryProcessor.class, TYPE_CATEGORY);
	}

	public static IDiscountProcessor getDiscountProcessor() {
		return getProcessor(IDiscountProcessor.class, TYPE_DISCOUNT);
	}

	public static IVersionProcessor getVersionProcessor() {
		return getProcessor(IVersionProcessor.class, TYPE_VERSION);
	}

	public static ISyncProcessor getSyncProcessor() {
		return getProcessor(ISyncProcessor.class, TYPE_SYNC);
	}

	public static IMessageProcessor getMessageProcessor() {
		return getProcessor(IMessageProcessor.class, TYPE_MESSAGE);
	}
	
	public static IAwardProcessor getAwardProcessor(){
		return getProcessor(IAwardProcessor.class, TYPE_AWARD);
	}
}

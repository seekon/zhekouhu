package com.seekon.zhekouhu.func;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.rest.RestStatus;
import com.seekon.zhekouhu.utils.Logger;

public class ProcessorProxy implements InvocationHandler {

	private static final String TAG = ProcessorProxy.class.getSimpleName();

	private Object target;

	public Object bind(Object target) {
		this.target = target;
		return Proxy.newProxyInstance(target.getClass().getClassLoader(), target
				.getClass().getInterfaces(), this);
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		Object result = null;
		try {
			result = method.invoke(target, args);
		} catch (Throwable e) {// 处理运行过程中抛出的异常
			e.printStackTrace();
			Logger.warn(TAG, e.getMessage(), e);

			Class<?> clazz = method.getReturnType();
			if (clazz.isAssignableFrom(ProcessResult.class)) {
				result = new ProcessResult<Entity>(RestStatus.RUNTIME_ERROR + "",
						Application.getAppContext().getString(R.string.runtime_error));
			}
		}
		return result;
	}

}

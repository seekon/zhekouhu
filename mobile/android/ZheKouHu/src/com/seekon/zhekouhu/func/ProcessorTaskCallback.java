package com.seekon.zhekouhu.func;

import com.seekon.zhekouhu.utils.ViewUtils;

public abstract class ProcessorTaskCallback<T> {

	public abstract ProcessResult<T> doInBackground();

	public abstract void onSuccess(T result);

	public void onFailed(ProcessResult.Error error) {
		if (error == null) {
			return;
		}
		String errorMessage = error.message;
		if (errorMessage != null && errorMessage.length() > 0) {
			ViewUtils.showToast("处理原因:" + errorMessage);
		}
	}

	public void onCancelled() {

	}
}

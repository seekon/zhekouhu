package com.seekon.zhekouhu.func;

import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.user.UserEntity;

public class RunEnv {

	private static RunEnv instance = null;

	private static Object lock = new Object();

	private UserEntity user = null;

	private String sessionId = null;// 键值对格式:key=value

	private LocationEntity location;

	private RunEnv() {
	}

	public static RunEnv getInstance() {
		synchronized (lock) {
			if (instance == null) {
				instance = new RunEnv();
			}
		}
		return instance;
	}

	public UserEntity getUser() {
		if (user == null) {
			return ProcessorFactory.getUserProcessor().localLogin();
		}
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public LocationEntity getLocation() {
		return location;
	}

	public void setLocation(LocationEntity location) {
		this.location = location;
	}

}

package com.seekon.zhekouhu.func.award;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.spi.IAwardProcessor;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONArrayResource;

public class AwardProcessor implements IAwardProcessor{

	@Override
	public ProcessResult<List<GoodsEntity>> getGoodsList() {
		MethodExecuter<List<GoodsEntity>> executer = new MethodExecuter<List<GoodsEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<GoodsEntity>> result) throws Exception {
				List<GoodsEntity> goodsList = new ArrayList<GoodsEntity>();
				JSONArrayResource resource = (JSONArrayResource) mResult.getResource();
				for (int i = 0; i < resource.length(); i++) {
					JSONObject json = resource.getJSONObject(i);
					goodsList.add(new GoodsEntity(json));
				}
				result.setSuccessObj(goodsList);
			}
		};
		return executer.execute(new GetGoodsMethod());
	}

}

package com.seekon.zhekouhu.func.award;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONArrayResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetGoodsMethod extends JSONArrayResourceMethod{

	private static String GET_GOODS = Const.SERVER_APP_URL
			+ "/getGoods";

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, GET_GOODS, null, null);
	}
	
	
}

package com.seekon.zhekouhu.func.award;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_COIN_VAL;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_DESC;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_IMG;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_NAME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_REMAIN;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;

import org.json.JSONObject;

import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.Logger;

public class GoodsEntity extends Entity {

	private static final long serialVersionUID = 5988707647999357382L;
	private String name;
	private String desc;
	private int coinVal;
	private int remain;
	private String image;

	public GoodsEntity() {
		super();
	}

	public GoodsEntity(JSONObject json){
		this();
		try {
			uuid = json.getString(COL_NAME_UUID);
			name = json.getString(COL_NAME_NAME);
			desc = json.getString(COL_NAME_DESC);
			image = json.getString(COL_NAME_IMG);
			remain = json.getInt(COL_NAME_REMAIN);
			coinVal = json.getInt(COL_NAME_COIN_VAL);
		} catch (Exception e) {
			Logger.error(StorefrontEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getCoinVal() {
		return coinVal;
	}

	public void setCoinVal(int coinVal) {
		this.coinVal = coinVal;
	}

	public int getRemain() {
		return remain;
	}

	public void setRemain(int remain) {
		this.remain = remain;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}

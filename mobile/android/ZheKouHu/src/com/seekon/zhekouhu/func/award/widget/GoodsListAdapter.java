package com.seekon.zhekouhu.func.award.widget;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.discount.CategoryDiscountsActivity;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.award.GoodsEntity;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.category.widget.CategoryListAdapter;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.LocationUtils;

public class GoodsListAdapter extends AbstractListAdapter<GoodsEntity> {

	public GoodsListAdapter(Context context, List<GoodsEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView != null) {
			holder = (ViewHolder) convertView.getTag();
		}

		if (holder == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.award_goods_item, null);

			holder = new ViewHolder();

			holder.iconView = (ImageView) convertView.findViewById(R.id.img_goods);
			holder.iconView.setAdjustViewBounds(false);
			holder.iconView.setScaleType(ImageView.ScaleType.CENTER_CROP);

			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);
			holder.descView = (TextView) convertView.findViewById(R.id.t_desc);
			holder.remainView = (TextView) convertView.findViewById(R.id.t_remain);
			holder.coinValView = (TextView) convertView.findViewById(R.id.t_coin_val);

			convertView.setTag(holder);
		}

		GoodsEntity goods = (GoodsEntity) getItem(position);
		ImageLoader.getInstance().displayImage(goods.getImage(), holder.iconView,
				true);
		holder.nameView.setText(goods.getName());
		holder.descView.setText(goods.getDesc());
		holder.remainView.setText("剩余:" + goods.getRemain());
		holder.coinValView.setText("×" + goods.getCoinVal());
		
		return convertView;
	}

	class ViewHolder {
		ImageView iconView;
		TextView descView;
		TextView nameView;
		TextView remainView;
		TextView coinValView;
	}
}

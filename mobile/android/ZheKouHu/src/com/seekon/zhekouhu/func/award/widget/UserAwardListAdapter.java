package com.seekon.zhekouhu.func.award.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.user.UserAwardEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.DateUtils;

public class UserAwardListAdapter extends AbstractListAdapter<UserAwardEntity> {

	public UserAwardListAdapter(Context context, List<UserAwardEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.user_award_item, null);

			holder = new ViewHolder();
			holder.awardTimeView = (TextView) convertView
					.findViewById(R.id.t_award_time);
			holder.coinValView = (TextView) convertView.findViewById(R.id.t_award_coin);
			holder.commentView = (TextView) convertView.findViewById(R.id.t_award_comment);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		UserAwardEntity award = (UserAwardEntity) getItem(position);
		holder.awardTimeView.setText(DateUtils.getDateString_yyyyMMdd(Long
				.valueOf(award.getAwardTime())));
		int coinVal = award.getCoinVal();
		if (coinVal > 0) {
			holder.coinValView.setText("+" + coinVal);
		}else{
			holder.coinValView.setText(coinVal + "");
		}
		
		holder.commentView.setText(award.getComment());
		
		return convertView;
	}

	class ViewHolder {
		TextView awardTimeView;
		TextView coinValView;
		TextView commentView;
	}
}

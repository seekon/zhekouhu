package com.seekon.zhekouhu.func.category;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.func.store.LocationEntity;

public class CategoryData extends AbstractSQLiteData<CategoryEntity> implements
		DataUpdater<CategoryEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_CATEGORY
			+ " (uuid, name, icon, parent_id,  ord_index) values (?, ?, ?, ?, ?)";

	private static final String QUERY_BASE_SQL = " select uuid, name, icon, ord_index, parent_id from "
			+ DataConst.TABLE_CATEGORY + "  c ";

	@Override
	public int save(CategoryEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (CategoryEntity category : data) {
			executeUpdate(
					UPDATE_SQL,
					new Object[] { category.getUuid(), category.getName(),
							category.getIcon(), category.getParentId(),
							category.getOrdIndex() });
		}

		return data.length;
	}

	@Override
	protected CategoryEntity processRow(Cursor cursor) {
		CategoryEntity category = new CategoryEntity();

		int i = 0;
		category.setUuid(cursor.getString(i++));
		category.setName(cursor.getString(i++));
		category.setIcon(cursor.getString(i++));
		// category.setParentId(cursor.getString(i++));
		category.setOrdIndex(cursor.getInt(i++));

		return category;
	}

	public List<CategoryEntity> categories() {
		String sql = QUERY_BASE_SQL + " order by ord_index ";
		return query(sql, null);
	}

	public CategoryEntity categoryById(String uuid) {
		String sql = QUERY_BASE_SQL + " where uuid = ?";
		return queryOne(sql, new String[] { uuid });
	}

	public List<CategoryEntity> categoriesHasCoupons(LocationEntity location) {
		String sql = QUERY_BASE_SQL
				+ " WHERE EXISTS (select 1 from z_coupon_store cs where cs.category_id = c.uuid and cs.city_name = ?) "
				+ " order by c.ord_index ";
		return query(sql, new String[] { location.getCityName() });
	}
}

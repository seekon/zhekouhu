package com.seekon.zhekouhu.func.category;

import com.seekon.zhekouhu.func.Entity;

public class CategoryEntity extends Entity {

	private static final long serialVersionUID = 3279394988834858011L;

	private String name;
	private String parentId;
	private int ordIndex;
	private String icon;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public int getOrdIndex() {
		return ordIndex;
	}

	public void setOrdIndex(int ordIndex) {
		this.ordIndex = ordIndex;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}

package com.seekon.zhekouhu.func.category;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.spi.ICategoryProcessor;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONArrayResource;

public class CategoryProcessor implements ICategoryProcessor {

	private CategoryData categoryData;

	public CategoryProcessor() {
		super();
		categoryData = new CategoryData();
	}

	@Override
	public ProcessResult<List<CategoryEntity>> categories() {
		List<CategoryEntity> categories = categoryData.categories();
		if (categories != null && categories.size() > 0) {
			return new ProcessResult<List<CategoryEntity>>(categories);
		}

		String updateTime = "-1";
		MethodExecuter<List<CategoryEntity>> executer = new MethodExecuter<List<CategoryEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<CategoryEntity>> result) throws Exception {
				List<CategoryEntity> categories = new ArrayList<CategoryEntity>();
				JSONArrayResource resource = (JSONArrayResource) mResult.getResource();
				for (int i = 0; i < resource.length(); i++) {
					JSONObject jsonCategory = resource.getJSONObject(i);

					CategoryEntity category = new CategoryEntity();
					category.setUuid(jsonCategory.getString(DataConst.COL_NAME_UUID));
					category.setName(jsonCategory.getString(DataConst.COL_NAME_NAME));
					category.setIcon(jsonCategory.getString(DataConst.COL_NAME_ICON));
					category.setOrdIndex(jsonCategory
							.getInt(DataConst.COL_NAME_ORD_INDEX));

					String parentId = jsonCategory
							.getString(DataConst.COL_NAME_PARENT_ID);
					if ("null".equalsIgnoreCase(parentId)) {
						parentId = "";
					}
					category.setParentId(parentId);

					categories.add(category);
				}

				categoryData.save(categories.toArray(new CategoryEntity[categories
						.size()]));
				result.setSuccessObj(categories);
			}
		};
		return executer.execute(new GetCategoriesMethod(updateTime));
	}

}

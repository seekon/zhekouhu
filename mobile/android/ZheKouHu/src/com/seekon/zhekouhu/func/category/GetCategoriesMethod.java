package com.seekon.zhekouhu.func.category;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONArrayResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetCategoriesMethod extends JSONArrayResourceMethod {

	private static String GET_CATEGORIES = Const.SERVER_APP_URL
			+ "/getCategories/";

	private String updateTime;

	public GetCategoriesMethod(String updateTime) {
		super();
		this.updateTime = updateTime;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, GET_CATEGORIES + updateTime, null, null);
	}

}

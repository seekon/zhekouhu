package com.seekon.zhekouhu.func.category.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.RUtils;

public class CategoryListAdapter extends AbstractListAdapter<CategoryEntity> {

	public CategoryListAdapter(Context context, List<CategoryEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.category_list_item, null);

			holder = new ViewHolder();

			holder.categoryImgView = (ImageView) convertView
					.findViewById(R.id.img_category);
			holder.categoryImgView.setScaleType(ImageView.ScaleType.CENTER_CROP);

			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CategoryEntity category = (CategoryEntity) getItem(position);

		String icon = category.getIcon();
		if (icon != null && icon.trim().length() > 0) {
			holder.categoryImgView.setImageResource(RUtils.getDrawableImg(FileHelper
					.getFileName(icon)));
		}
		holder.nameView.setText(category.getName());

		return convertView;
	}

	class ViewHolder {
		ImageView categoryImgView;
		TextView nameView;
	}
}

package com.seekon.zhekouhu.func.discount;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.Logger;

public class CommentEntity extends Entity {

	private static final long serialVersionUID = -1196678712578169682L;

	private String discountId;
	private String content;
	private String publisher;
	private String publishTime;
	private String type;

	public CommentEntity() {
		super();
	}

	public CommentEntity(JSONObject json) {
		this();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			discountId = json.getString(DataConst.COL_NAME_DISCOUNT_ID);
			content = json.getString(DataConst.COL_NAME_CONTENT);
			publisher = json.getString(DataConst.COL_NAME_PUBLISHER);
			publishTime = json.getString(DataConst.COL_NAME_PUBLISH_TIME);
			type = json.getString(DataConst.COL_NAME_TYPE);
		} catch (Exception e) {
			Logger.error(StorefrontEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getDiscountId() {
		return discountId;
	}

	public void setDiscountId(String discountId) {
		this.discountId = discountId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

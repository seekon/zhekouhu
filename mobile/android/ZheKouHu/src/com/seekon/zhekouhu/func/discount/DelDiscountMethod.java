package com.seekon.zhekouhu.func.discount;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class DelDiscountMethod extends JSONObjResourceMethod {

	private static String DEL_DISCOUNT = Const.SERVER_APP_URL
			+ "/deleteDiscount/";

	private DiscountEntity discount;

	public DelDiscountMethod(DiscountEntity discount) {
		super();
		this.discount = discount;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.DELETE, DEL_DISCOUNT + discount.getUuid(),
				null, null);
	};

}

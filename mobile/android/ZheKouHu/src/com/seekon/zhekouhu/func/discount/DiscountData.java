package com.seekon.zhekouhu.func.discount;

import static com.seekon.zhekouhu.db.DataConst.TABLE_DISCOUNT;
import static com.seekon.zhekouhu.func.FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY;
import static com.seekon.zhekouhu.func.FuncConst.VAL_DISCOUNT_TYPE_COUPON;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.func.DateIndexedEntity;
import com.seekon.zhekouhu.func.category.CategoryData;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.DateUtils;
import com.seekon.zhekouhu.utils.LocationUtils;
import com.seekon.zhekouhu.utils.Logger;

public class DiscountData extends AbstractSQLiteData<DiscountEntity> implements
		DataUpdater<DiscountEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ TABLE_DISCOUNT
			+ " (uuid, content, par_value, start_date, end_date, type, category_id, publisher, publish_date, publish_time, visit_count, origin, status, location, audit_idea ) "
			+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String COUNT_BASE_SQL = " select count(1) from "
			+ TABLE_DISCOUNT;

	private static final String BASE_SELECT_FIELD = " select distinct d.uuid, d.content, d.par_value, d.start_date, d.end_date, d.type, d.category_id"
			+ ", d.publisher, d.publish_date, d.publish_time, d.visit_count, d.origin, d.status, d.location, d.audit_idea ";

	private static final String QUERY_BASE_SQL = BASE_SELECT_FIELD
			+ ", 1 as angle from " + TABLE_DISCOUNT + "  d ";
	private static final String DEL_BY_UUID_SQL = "delete from " + TABLE_DISCOUNT
			+ " where uuid = ? ";

	@Override
	public int save(DiscountEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (DiscountEntity discount : data) {
			executeUpdate(
					UPDATE_SQL,
					new Object[] { discount.getUuid(), discount.getContent(),
							discount.getParValue(), discount.getStartDate(),
							discount.getEndDate(), discount.getType(),
							discount.getCategory().getUuid(), discount.getPublisher(),
							discount.getPublishDate(), discount.getPublishTime(),
							discount.getVisitCount(), discount.getOrigin(),
							discount.getStatus(), discount.getLocation(),
							discount.getAuditIdea() });
		}
		return data.length;
	}

	@Override
	protected DiscountEntity processRow(Cursor cursor) {
		DiscountEntity discount = new DiscountEntity();

		int i = 0;
		String uuid = cursor.getString(i++);
		if (uuid == null) {
			return null;
		}

		discount.setUuid(uuid);
		discount.setContent(cursor.getString(i++));
		discount.setParValue(cursor.getDouble(i++));
		discount.setStartDate(cursor.getLong(i++));
		discount.setEndDate(cursor.getLong(i++));
		discount.setType(cursor.getString(i++));

		CategoryEntity category = new CategoryData().categoryById(cursor
				.getString(i++));
		discount.setCategory(category);

		discount.setPublisher(cursor.getString(i++));
		discount.setPublishDate(cursor.getString(i++));
		discount.setPublishTime(cursor.getLong(i++));
		discount.setVisitCount(cursor.getInt(i++));
		discount.setOrigin(cursor.getString(i++));
		discount.setStatus(cursor.getString(i++));
		discount.setLocation(cursor.getString(i++));
		discount.setAuditIdea(cursor.getString(i++));
		
		discount.setImages(new DiscountImageData().imagesByDiscount(discount
				.getUuid()));

		// double angle = cursor.getDouble(i++);
		// double distance = Math.acos(angle) * LocationUtils.EARTH_RADIUS;
		// System.out.println("distance:" + distance);
		return discount;
	}

	private int discountCountForOwner(String owner, String type) {
		String sql = COUNT_BASE_SQL + " where publisher = ?  ";
		String[] params = null;
		if (type != null) {
			sql += " and type = ?";
			params = new String[] { owner, type };
		} else {
			params = new String[] { owner };
		}
		return queryCount(sql, params);
	}

	public int activityCountForOwner(String owner) {
		return discountCountForOwner(owner, VAL_DISCOUNT_TYPE_ACTIVITY);
	}

	public int couponCountForOwner(String owner) {
		return discountCountForOwner(owner, VAL_DISCOUNT_TYPE_COUPON);
	}

	public int discountCountForOwner(String owner) {
		return discountCountForOwner(owner, null);
	}

	public List<DateIndexedEntity> activitiesGroupByPublishDate(String owner,
			int offset) {
		return discountsGroupByPublishDate(owner, VAL_DISCOUNT_TYPE_ACTIVITY,
				offset);
	}

	public List<DateIndexedEntity> couponsGroupByPublishDate(String owner,
			int offset) {
		return discountsGroupByPublishDate(owner, VAL_DISCOUNT_TYPE_COUPON, offset);
	}

	public List<DateIndexedEntity> discountsGroupByPublishDate(String owner,
			int offset) {
		return discountsGroupByPublishDate(owner, null, offset);
	}

	private List<DateIndexedEntity> discountsGroupByPublishDate(String owner,
			String type, int offset) {
		List<DateIndexedEntity> result = new ArrayList<DateIndexedEntity>();

		String[] params = null;
		StringBuffer sql = new StringBuffer();
		sql.append(" select publish_date, count(1) from ");
		sql.append(TABLE_DISCOUNT);
		sql.append(" where publisher = ? ");
		if (type != null) {
			sql.append(" and type = ? ");
			params = new String[] { owner, type };
		} else {
			params = new String[] { owner };
		}
		sql.append(" group by publish_date order by publish_date desc ");
		sql.append(" limit " + Const.PAGE_SIZE + " offset " + offset);

		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = null;
		try {
			cursor = db.rawQuery(sql.toString(), params);
			while (cursor.moveToNext()) {
				DateIndexedEntity entity = new DateIndexedEntity();

				int i = 0;
				String publishDate = cursor.getString(i++);
				entity.setDate(DateUtils.getDate_yyyyMMdd(publishDate));
				entity.setItemCount(cursor.getInt(i++));
				entity.setSubItemList(discountsByPublishdate(type, owner, publishDate));

				result.add(entity);
			}
		} catch (Exception e) {
			Logger.error(TAG, e.getMessage(), e);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			if (db != null) {
				db.close();
			}
		}

		return result;
	}

	private List<DiscountEntity> discountsByPublishdate(String type,
			String owner, String publishDate) {
		String[] params = null;
		StringBuffer sql = new StringBuffer(QUERY_BASE_SQL);
		sql.append(" where publish_date = ? and publisher = ? ");
		if (type != null) {
			sql.append(" and type = ? ");
			params = new String[] { publishDate, owner, type };
		} else {
			params = new String[] { publishDate, owner };
		}
		sql.append(" order by publish_time desc ");

		List<DiscountEntity> discounts = query(sql.toString(), params);
		setStorefronts(discounts, null);
		return discounts;
	}

	public void deleteDiscount(String uuid) {
		executeUpdate(DEL_BY_UUID_SQL, new Object[] { uuid });
	}

	public List<DiscountEntity> activitiesOrderByPublishDate(
			LocationEntity location, int offset) {
		StringBuffer sql = new StringBuffer(BASE_SELECT_FIELD);
		sql.append(" from z_discount d, z_discount_storefront df, z_storefront f ");
		sql.append(" where d.uuid = df.discount_id and df.storefront_id = f.uuid  ");
		sql.append(" and d.type = ? and  f.city_name = ? ");
		sql.append(" order by publish_time ");
		sql.append(" limit " + Const.PAGE_SIZE + " offset " + offset);

		List<DiscountEntity> discounts = query(sql.toString(), new String[] {
				VAL_DISCOUNT_TYPE_ACTIVITY, location.getCityName() });
		setStorefronts(discounts, location);
		return discounts;
	}

	public List<DiscountEntity> couponsForStore(String storeId, String cateId,
			LocationEntity location, int offset) {
		StringBuffer sql = new StringBuffer(BASE_SELECT_FIELD);
		sql.append(" from z_discount d, z_discount_storefront df, z_storefront f ");
		sql.append(" where d.uuid = df.discount_id and df.storefront_id = f.uuid  ");
		sql.append(" and d.type = ? and df.store_id = ? and f.city_name = ? and d.category_id = ? ");
		sql.append(" and d.start_date <= ? and d.end_date >= ? ");
		sql.append(" order by publish_time ");
		sql.append(" limit " + Const.PAGE_SIZE + " offset " + offset);

		String currentTiem = System.currentTimeMillis() + "";
		List<DiscountEntity> discounts = query(sql.toString(), new String[] {
				VAL_DISCOUNT_TYPE_COUPON, storeId, location.getCityName(), cateId,
				currentTiem, currentTiem });
		setStorefronts(discounts, location);
		return discounts;
	}

	public void increaseVisitCount(DiscountEntity discount) {
		String sql = " update z_discount set visit_count = visit_count + 1 where uuid = ?";
		executeUpdate(sql, new Object[] { discount.getUuid() });
	}

	public List<DiscountEntity> activitiesForCategory(String categoryId,
			LocationEntity location, int offset) {
		StringBuffer sb = new StringBuffer(BASE_SELECT_FIELD);
		sb.append(", " + angleSqlpart(location.getLat(), location.getLng())
				+ " as angle ");
		sb.append(" from z_discount d, z_discount_storefront df, z_storefront f ");
		sb.append(" where d.uuid = df.discount_id and df.storefront_id = f.uuid  ");
		sb.append(" and d.type = ? and d.category_id = ? and f.city_name = ? ");
		sb.append(" order by angle desc, publish_time ");
		sb.append(" limit " + Const.PAGE_SIZE + " offset " + offset);
		List<DiscountEntity> discounts = query(sb.toString(), new String[] {
				VAL_DISCOUNT_TYPE_ACTIVITY, categoryId, location.getCityName() });
		setStorefronts(discounts, location);
		return discounts;
	}

	private String angleSqlpart(double lat, double lng) {
		double lat1 = LocationUtils.translateLat(lat);// 先转换
		double lng1 = LocationUtils.translateLng(lng);
		double latSinLngCos = LocationUtils.latSinLngCos(lat1, lng1);
		double latSinLngSin = LocationUtils.latSinLngSin(lat1, lng1);
		double latCos = LocationUtils.latCos(lat1);

		String earthSqlpart = LocationUtils.EARTH_RADIUS + " * "
				+ LocationUtils.EARTH_RADIUS;
		String latSinLngCosSql = " (lat_sin_lng_cos - " + latSinLngCos + ") ";
		String latSinLngSinSql = " (lat_sin_lng_sin - " + latSinLngSin + ") ";
		String latCosSql = " (lat_cos - " + latCos + ") ";

		StringBuffer sql = new StringBuffer();
		sql.append(" max( ");
		sql.append("(" + earthSqlpart + " + " + earthSqlpart + " - (");
		sql.append(latSinLngCosSql + "*" + latSinLngCosSql);
		sql.append("+" + latSinLngSinSql + "*" + latSinLngSinSql);
		sql.append("+" + latCosSql + "*" + latCosSql);
		sql.append("))");
		sql.append(" / (2 * " + earthSqlpart + ")");
		sql.append(" )");
		return sql.toString();
	}

	/**
	 * 处理活动信息中的门店列表
	 * 
	 * @param discountList
	 * @param location
	 */
	private void setStorefronts(List<DiscountEntity> discountList,
			LocationEntity location) {
		if (discountList == null || discountList.isEmpty()) {
			return;
		}

		for (DiscountEntity discount : discountList) {
			setStorefronts(discount, location);
		}
	}

	private void setStorefronts(DiscountEntity discount, LocationEntity location) {
		if (discount == null) {
			return;
		}
		List<StorefrontEntity> storefronts = new DiscountStorefrontData()
				.storefrontsByDiscount(discount.getUuid(), location);
		Collections.sort(storefronts, new StorefrontDistanceComparator());
		discount.setStorefronts(storefronts);
	}

	public DiscountEntity discountById(String uuid, LocationEntity location) {
		String sql = QUERY_BASE_SQL + " where uuid = ?";
		DiscountEntity discount = queryOne(sql, new String[] { uuid });
		setStorefronts(discount, location);
		return discount;
	}
}

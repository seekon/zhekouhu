package com.seekon.zhekouhu.func.discount;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.Logger;

public class DiscountEntity extends Entity {

	private static final long serialVersionUID = -1412782805621767757L;

	private String content;
	private double parValue;
	private long startDate;
	private long endDate;
	private int visitCount;
	private String publisher;
	private String publishDate;
	private long publishTime;
	private String type;
	private String origin;
	private String status;
	private String location;
	private String auditIdea;
	
	private int commentCount;
	private int frontCount;
	private CategoryEntity category;
	private List<FileEntity> images = new ArrayList<FileEntity>();
	private List<StorefrontEntity> storefronts = new ArrayList<StorefrontEntity>();
	private List<CommentEntity> comments = new ArrayList<CommentEntity>();

	public DiscountEntity() {
		super();
	}

	public DiscountEntity(JSONObject json) {
		super();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			startDate = json.getLong(DataConst.COL_NAME_START_DATE);
			endDate = json.getLong(DataConst.COL_NAME_END_DATE);
			visitCount = json.getInt(DataConst.COL_NAME_VISIT_COUNT);
			publisher = json.getString(DataConst.COL_NAME_PUBLISHER);
			publishDate = json.getString(DataConst.COL_NAME_PUBLISH_DATE);
			publishTime = json.getLong(DataConst.COL_NAME_PUBLISH_TIME);
			type = json.getString(DataConst.COL_NAME_TYPE);
			content = json.getString(DataConst.COL_NAME_CONTENT);
			parValue = json.getDouble(DataConst.COL_NAME_PAR_VALUE);
			origin = json.getString(DataConst.COL_NAME_ORIGIN);
			if (json.has(DataConst.COL_NAME_CATEGORY_ID)) {
				category = new CategoryEntity();
				category.setUuid(json.getString(DataConst.COL_NAME_CATEGORY_ID));
			}
			if(json.has(DataConst.COL_NAME_STATUS)){
				status = json.getString(DataConst.COL_NAME_STATUS);
			}
			if (json.has(DataConst.COL_NAME_LOCATION)) {
				location = json.getString(DataConst.COL_NAME_LOCATION);
			}
			if (json.has(DataConst.COL_NAME_AUDIT_IDEA)) {
				auditIdea = json.getString(DataConst.COL_NAME_AUDIT_IDEA);
			}
		} catch (Exception e) {
			Logger.error(DiscountEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public double getParValue() {
		return parValue;
	}

	public void setParValue(double parValue) {
		this.parValue = parValue;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public int getVisitCount() {
		return visitCount;
	}

	public void setVisitCount(int visitCount) {
		this.visitCount = visitCount;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public long getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(long publishTime) {
		this.publishTime = publishTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAuditIdea() {
		return auditIdea;
	}

	public void setAuditIdea(String auditIdea) {
		this.auditIdea = auditIdea;
	}

	public CategoryEntity getCategory() {
		return category;
	}

	public void setCategory(CategoryEntity category) {
		this.category = category;
	}

	public List<FileEntity> getImages() {
		return images;
	}

	public void setImages(List<FileEntity> images) {
		this.images = images;
	}

	public void addImage(DiscountImageEntity image) {
		this.images.add(image);
	}

	public List<StorefrontEntity> getStorefronts() {
		return storefronts;
	}

	public void setStorefronts(List<StorefrontEntity> storefronts) {
		this.storefronts = storefronts;
	}

	public void addStorefront(StorefrontEntity front) {
		this.storefronts.add(front);
	}

	public int getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}

	public int getFrontCount() {
		return frontCount;
	}

	public void setFrontCount(int frontCount) {
		this.frontCount = frontCount;
	}

	public List<CommentEntity> getComments() {
		return comments;
	}

	public void setComments(List<CommentEntity> comments) {
		this.comments = comments;
	}

	public void addComment(int pos, CommentEntity comment) {
		this.comments.add(pos, comment);
	}

	public void addComment(CommentEntity comment) {
		this.comments.add(comment);
	}
}

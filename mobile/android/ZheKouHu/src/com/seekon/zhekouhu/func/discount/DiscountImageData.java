package com.seekon.zhekouhu.func.discount;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.file.FileEntity;

public class DiscountImageData extends AbstractSQLiteData<FileEntity> implements
		DataUpdater<DiscountImageEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_DISCOUNT_IMG
			+ " (uuid, discount_id, img, ord_index) values (?, ?, ?, ?)";

	private static final String QUERY_SQL = " select uuid,discount_id,img from "
			+ DataConst.TABLE_DISCOUNT_IMG
			+ " where discount_id = ? order by ord_index ";

	private static final String DEL_BASE_SQL = "delete from "
			+ DataConst.TABLE_DISCOUNT_IMG;

	@Override
	public int save(DiscountImageEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (DiscountImageEntity discountImage : data) {
			executeUpdate(UPDATE_SQL, new Object[] { discountImage.getUuid(),
					discountImage.getDiscountId(), discountImage.getAliasName(),
					discountImage.getOrdIndex() });
		}

		return data.length;
	}

	@Override
	protected DiscountImageEntity processRow(Cursor cursor) {
		DiscountImageEntity image = new DiscountImageEntity();
		int i = 0;
		image.setUuid(cursor.getString(i++));
		image.setDiscountId(cursor.getString(i++));
		image.setAliasName(cursor.getString(i++));
		return image;
	}

	public List<FileEntity> imagesByDiscount(String discountId) {
		return query(QUERY_SQL, new String[] { discountId });
	}

	public void deleteByDiscount(String discountId) {
		String sql = DEL_BASE_SQL + " where discount_id = ?";
		executeUpdate(sql, new Object[] { discountId });
	}

	public void deleteByDiscountImage(String discountId, String image) {
		String sql = DEL_BASE_SQL + " where discount_id = ? and img = ?";
		executeUpdate(sql, new Object[] { discountId, image });
	}

	public void deleteById(String uuid) {
		String sql = DEL_BASE_SQL + " where uuid = ?";
		executeUpdate(sql, new Object[] { uuid });
	}
}

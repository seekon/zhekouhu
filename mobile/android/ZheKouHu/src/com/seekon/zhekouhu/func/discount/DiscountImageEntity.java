package com.seekon.zhekouhu.func.discount;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.utils.Logger;

public class DiscountImageEntity extends FileEntity {

	private static final long serialVersionUID = -2811321352053398161L;

	private String uuid;
	private String discountId;
	private int ordIndex;

	public DiscountImageEntity() {
		super();
	}

	public DiscountImageEntity(JSONObject json) {
		super();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			aliasName = json.getString(DataConst.COL_NAME_IMG);
			if (json.has(DataConst.COL_NAME_DISCOUNT_ID)) {
				discountId = json.getString(DataConst.COL_NAME_DISCOUNT_ID);
			}
			if (json.has(DataConst.COL_NAME_ORD_INDEX)) {
				ordIndex = json.getInt(DataConst.COL_NAME_ORD_INDEX);
			}
		} catch (Exception e) {
			Logger
					.error(DiscountImageEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getDiscountId() {
		return discountId;
	}

	public void setDiscountId(String discountId) {
		this.discountId = discountId;
	}

	public int getOrdIndex() {
		return ordIndex;
	}

	public void setOrdIndex(int ordIndex) {
		this.ordIndex = ordIndex;
	}

}

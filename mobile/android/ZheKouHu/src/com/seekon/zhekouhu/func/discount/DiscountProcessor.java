package com.seekon.zhekouhu.func.discount;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_DISCOUNT_ID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_NAME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_PUBLISH_DATE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_PUBLISH_TIME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_STATUS;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_STOREFRONT_ID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_STORE_ID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_STORE_NAME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UPDATE_TIME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.func.FuncConst.NAME_COMMENTS;
import static com.seekon.zhekouhu.func.FuncConst.NAME_COMMENT_COUNT;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DATA;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DISCOUNT;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DISCOUNTS;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DISCOUNT_STOREFRONTS;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DISTANCE;
import static com.seekon.zhekouhu.func.FuncConst.NAME_FAVORIT;
import static com.seekon.zhekouhu.func.FuncConst.NAME_FRONT_COUNT;
import static com.seekon.zhekouhu.func.FuncConst.NAME_IMAGES;
import static com.seekon.zhekouhu.func.FuncConst.NAME_IS_DELETED;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STOREFRONT;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STOREFRONTS;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STORES;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.DateIndexedEntity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.category.CategoryData;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.spi.IDiscountProcessor;
import com.seekon.zhekouhu.func.store.CouponStoreData;
import com.seekon.zhekouhu.func.store.CouponStoreEntity;
import com.seekon.zhekouhu.func.store.GetStoresHasCouponMethod;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.store.StoreData;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontData;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.sync.SyncData;
import com.seekon.zhekouhu.func.sync.SyncEntity;
import com.seekon.zhekouhu.func.sync.SyncProcessor;
import com.seekon.zhekouhu.func.user.FavoritData;
import com.seekon.zhekouhu.func.user.FavoritEntity;
import com.seekon.zhekouhu.func.user.RefreshFavoritsMethod;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONArrayResource;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;
import com.seekon.zhekouhu.utils.LocationUtils;
import com.seekon.zhekouhu.utils.ViewUtils;

public class DiscountProcessor implements IDiscountProcessor {
	private DiscountData discountData;
	private DiscountImageData discountImageData;
	private DiscountStorefrontData discountStorefrontData;
	private FavoritData favoritData;
	private SyncData syncData;

	public DiscountProcessor() {
		super();
		discountData = new DiscountData();
		discountImageData = new DiscountImageData();
		discountStorefrontData = new DiscountStorefrontData();
		favoritData = new FavoritData();
		syncData = new SyncData();
	}

	private int saveReponseDiscountsData(JSONObject data) throws Exception {
		int result = 0;
		// 图片列表
		if (data.has(NAME_IMAGES)) {
			List<DiscountImageEntity> images = new ArrayList<DiscountImageEntity>();
			JSONArray jsonImages = data.getJSONArray(NAME_IMAGES);
			for (int i = 0; i < jsonImages.length(); i++) {
				JSONObject jsonImage = jsonImages.getJSONObject(i);
				String deleted = jsonImage.getString(NAME_IS_DELETED);
				if ("1".equals(deleted)) {
					discountImageData.deleteById(jsonImage.getString(COL_NAME_UUID));
					continue;
				}
				images.add(new DiscountImageEntity(jsonImage));
			}

			result += images.size();
			discountImageData.save(images.toArray(new DiscountImageEntity[images
					.size()]));
		}

		// 活动门店对应
		if (data.has(NAME_DISCOUNT_STOREFRONTS)) {
			List<DiscountStorefrontEntity> fronts = new ArrayList<DiscountStorefrontEntity>();
			JSONArray jsonFronts = data.getJSONArray(NAME_DISCOUNT_STOREFRONTS);
			for (int i = 0; i < jsonFronts.length(); i++) {
				JSONObject jsonFront = jsonFronts.getJSONObject(i);
				String deleted = jsonFront.getString(NAME_IS_DELETED);
				if ("1".equals(deleted)) {
					discountStorefrontData.deleteById(jsonFront.getString(COL_NAME_UUID));
					continue;
				}
				fronts.add(new DiscountStorefrontEntity(jsonFront));
			}

			result += fronts.size();
			discountStorefrontData.save(fronts
					.toArray(new DiscountStorefrontEntity[fronts.size()]));
		}

		// 店铺列表
		if (data.has(NAME_STORES)) {
			List<StoreEntity> stores = new ArrayList<StoreEntity>();
			JSONArray jsonStores = data.getJSONArray(NAME_STORES);
			for (int i = 0; i < jsonStores.length(); i++) {
				stores.add(new StoreEntity(jsonStores.getJSONObject(i)));
			}
			result += stores.size();
			new StoreData().save(stores.toArray(new StoreEntity[stores.size()]));
		}

		// 门店列表
		if (data.has(NAME_STOREFRONTS)) {
			List<StorefrontEntity> fronts = new ArrayList<StorefrontEntity>();
			JSONArray jsonFronts = data.getJSONArray(NAME_STOREFRONTS);
			for (int i = 0; i < jsonFronts.length(); i++) {
				fronts.add(new StorefrontEntity(jsonFronts.getJSONObject(i)));
			}
			result += fronts.size();
			new StorefrontData().save(fronts.toArray(new StorefrontEntity[fronts
					.size()]));
		}

		// 活动
		if (data.has(NAME_DISCOUNTS)) {
			List<DiscountEntity> discounts = new ArrayList<DiscountEntity>();
			JSONArray jsonDiscounts = data.getJSONArray(NAME_DISCOUNTS);
			for (int i = 0; i < jsonDiscounts.length(); i++) {
				JSONObject jsonDiscount = jsonDiscounts.getJSONObject(i);
				String status = jsonDiscount.getString(COL_NAME_STATUS);
				if ("9".equals(status)) {
					discountData.deleteDiscount(jsonDiscount.getString(COL_NAME_UUID));
					continue;
				}

				DiscountEntity discount = new DiscountEntity(jsonDiscount);

				CategoryEntity category = new CategoryEntity();
				category
						.setUuid(jsonDiscount.getString(DataConst.COL_NAME_CATEGORY_ID));
				discount.setCategory(category);

				discounts.add(discount);
			}
			if (discounts.size() > 0) {
				result += discounts.size();
				discountData
						.save(discounts.toArray(new DiscountEntity[discounts.size()]));
			}
		}
		return result;

	}

	/**
	 * 将json格式的数据组装成java对象形式
	 * 
	 * @deprecated
	 * @param data
	 * @return
	 */
	private List<DiscountEntity> _assembleDiscounts(JSONObject data)
			throws Exception {
		List<DiscountEntity> discounts = new ArrayList<DiscountEntity>();
		if (data.has(NAME_DISCOUNTS)) {
			// 图片
			Map<String, List<FileEntity>> imageMap = new HashMap<String, List<FileEntity>>();
			if (data.has(NAME_IMAGES)) {
				JSONArray jsonImages = data.getJSONArray(NAME_IMAGES);
				for (int i = 0; i < jsonImages.length(); i++) {
					JSONObject jsonImage = jsonImages.getJSONObject(i);
					String deleted = jsonImage.getString(NAME_IS_DELETED);
					if ("1".equals(deleted)) {
						discountImageData.deleteById(jsonImage.getString(COL_NAME_UUID));
						continue;
					}
					String discountId = jsonImage.getString(COL_NAME_DISCOUNT_ID);
					List<FileEntity> value = imageMap.get(discountId);
					if (value == null) {
						value = new ArrayList<FileEntity>();
					}
					DiscountImageEntity image = new DiscountImageEntity(jsonImage);
					value.add(image);
					imageMap.put(discountId, value);
				}
			}

			// 分类
			List<CategoryEntity> categories = new CategoryData().categories();
			Map<String, CategoryEntity> categoryMap = new HashMap<String, CategoryEntity>();
			for (CategoryEntity categoryEntity : categories) {
				categoryMap.put(categoryEntity.getUuid(), categoryEntity);
			}

			// 店铺
			Map<String, StoreEntity> storeMap = new HashMap<String, StoreEntity>();
			if (data.has(NAME_STORES)) {
				JSONArray jsonStores = data.getJSONArray(NAME_STORES);
				for (int i = 0; i < jsonStores.length(); i++) {
					StoreEntity store = new StoreEntity(jsonStores.getJSONObject(i));
					storeMap.put(store.getUuid(), store);
				}
			}

			// 门店
			Map<String, StorefrontEntity> storefrontMap = new HashMap<String, StorefrontEntity>();
			if (data.has(NAME_STOREFRONTS)) {
				JSONArray jsonFronts = data.getJSONArray(NAME_STOREFRONTS);
				for (int i = 0; i < jsonFronts.length(); i++) {
					StorefrontEntity front = new StorefrontEntity(
							jsonFronts.getJSONObject(i));
					front.setStore(storeMap.get(front.getStore().getUuid()));
					LocationEntity location = RunEnv.getInstance().getLocation();
					if (location != null) {
						front.setDistance(LocationUtils.latlngDist(location.getLat(),
								location.getLng(), front.getLatitude(), front.getLongitude()));
					}

					storefrontMap.put(front.getUuid(), front);
				}
			}

			// 门店活动对应
			Map<String, List<StorefrontEntity>> discountSFMap = new HashMap<String, List<StorefrontEntity>>();
			if (data.has(NAME_DISCOUNT_STOREFRONTS)) {
				JSONArray jsonFronts = data.getJSONArray(NAME_DISCOUNT_STOREFRONTS);
				for (int i = 0; i < jsonFronts.length(); i++) {
					JSONObject jsonFront = jsonFronts.getJSONObject(i);
					String deleted = jsonFront.getString(NAME_IS_DELETED);
					if ("1".equals(deleted)) {
						discountStorefrontData.deleteById(jsonFront
								.getString(COL_NAME_UUID));
						continue;
					}
					String discountId = jsonFront.getString(COL_NAME_DISCOUNT_ID);
					List<StorefrontEntity> value = discountSFMap.get(discountId);
					if (value == null) {
						value = new ArrayList<StorefrontEntity>();
					}
					StorefrontEntity storefront = storefrontMap.get(jsonFront
							.get(COL_NAME_STOREFRONT_ID));
					if (storefront != null) {
						value.add(storefront);
					}

					discountSFMap.put(discountId, value);
				}
			}

			JSONArray jsonDiscounts = data.getJSONArray(NAME_DISCOUNTS);
			for (int i = 0; i < jsonDiscounts.length(); i++) {
				JSONObject jsonDiscount = jsonDiscounts.getJSONObject(i);
				String status = jsonDiscount.getString(COL_NAME_STATUS);
				if ("9".equals(status)) {
					discountData.deleteDiscount(jsonDiscount.getString(COL_NAME_UUID));
					continue;
				}

				DiscountEntity discount = new DiscountEntity(jsonDiscount);

				// 分类
				discount.setCategory(categoryMap.get(jsonDiscount
						.getString(DataConst.COL_NAME_CATEGORY_ID)));
				// 图片
				discount.setImages(imageMap.get(discount.getUuid()));
				// 门店
				List<StorefrontEntity> storefronts = discountSFMap.get(discount
						.getUuid());
				Collections.sort(storefronts, new StorefrontDistanceComparator());
				discount.setStorefronts(storefronts);
				discount.setFrontCount(storefronts.size());

				discounts.add(discount);
			}
		}
		return discounts;
	}

	@Override
	public DiscountEntity assembleDiscount(JSONObject jsonDiscount)
			throws Exception {
		DiscountEntity discount = new DiscountEntity(jsonDiscount);

		// 图片
		if (jsonDiscount.has(NAME_IMAGES)) {
			JSONArray jsonImages = jsonDiscount.getJSONArray(NAME_IMAGES);
			for (int j = 0; j < jsonImages.length(); j++) {
				JSONObject jsonImage = jsonImages.getJSONObject(j);
				DiscountImageEntity image = new DiscountImageEntity(jsonImage);
				image.setDiscountId(discount.getUuid());
				discount.addImage(image);
			}
		}
		
		// 门店数目
		if (jsonDiscount.has(NAME_FRONT_COUNT)) {
			discount.setFrontCount(jsonDiscount.getInt(NAME_FRONT_COUNT));
		}
		// 评论数目
		if (jsonDiscount.has(NAME_COMMENT_COUNT)) {
			discount.setCommentCount(jsonDiscount.getInt(NAME_COMMENT_COUNT));
		}
		
		// 最新的门店
		if (jsonDiscount.has(NAME_STOREFRONT) && discount.getFrontCount() > 0) {
			JSONObject jsonFront = jsonDiscount.getJSONObject(NAME_STOREFRONT);
			StoreEntity store = new StoreEntity();
			store.setUuid(jsonFront.getString(COL_NAME_STORE_ID));
			store.setName(jsonFront.getString(COL_NAME_STORE_NAME));

			StorefrontEntity front = new StorefrontEntity();
			front.setUuid(jsonFront.getString(COL_NAME_UUID));
			front.setName(jsonFront.getString(COL_NAME_NAME));
			front.setDistance(jsonDiscount.getDouble(NAME_DISTANCE));
			front.setStore(store);
			discount.addStorefront(front);
		}
		
		if (discount.getFrontCount() <= 0) {
			return null;
		}
		
		return discount;
	}

	/**
	 * 组装discount对象，1.1版以后使用
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public List<DiscountEntity> assembleDiscounts(JSONArray data)
			throws Exception {
		List<DiscountEntity> discounts = new ArrayList<DiscountEntity>();
		for (int i = 0; i < data.length(); i++) {
			DiscountEntity discount = assembleDiscount(data.getJSONObject(i));
			if(discount != null){
				discounts.add(discount);
			}
		}
		return discounts;
	}

	@Override
	public ProcessResult<Boolean> discountsRemoteRefresh(final String owner,
			String type) {
		final String tableName = SyncProcessor.getSyncTableNameWithOwner(type);
		String updateTime = "-1";
		SyncEntity sync = syncData.syncEntity(tableName, owner);
		if (sync != null) {
			updateTime = sync.getUpdateTime();
		}

		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UPDATE_TIME)) {

					int updateCount = 0;
					// 更新记录
					JSONObject data = resource.getJSONObject(NAME_DATA);
					if (data != null && data.length() > 0) {
						updateCount = saveReponseDiscountsData(resource
								.getJSONObject(NAME_DATA));
					}

					// 记录同步更新记录
					SyncEntity sync = syncData.syncEntity(tableName, owner);
					if (sync == null) {
						sync = new SyncEntity();
						sync.setTableName(tableName);
						sync.setItemId(owner);
						sync.setUuid(System.currentTimeMillis() + "");
					}
					sync.setUpdateTime(resource.getString(COL_NAME_UPDATE_TIME));

					syncData.save(new SyncEntity[] { sync });

					if (updateCount > 0) {
						result.setSuccessObj(Boolean.TRUE);
					} else {
						result.setSuccessObj(Boolean.FALSE);
					}
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new GetDiscountsByOwnerMethod(owner, type,
				updateTime));
	}

	@Override
	public ProcessResult<List<DateIndexedEntity>> activitiesGroupByPublishDate(
			String owner, int offset) {
		List<DateIndexedEntity> resultList = discountData
				.activitiesGroupByPublishDate(owner, offset);
		return new ProcessResult<List<DateIndexedEntity>>(resultList);
	}

	@Override
	public ProcessResult<List<DateIndexedEntity>> couponsGroupByPublishDate(
			String owner, int offset) {
		List<DateIndexedEntity> resultList = discountData
				.couponsGroupByPublishDate(owner, offset);
		return new ProcessResult<List<DateIndexedEntity>>(resultList);
	}

	@Override
	public int activityCountForOwner(String owner) {
		return discountData.activityCountForOwner(owner);
	}

	@Override
	public int couponCountForOwner(String owner) {
		return discountData.couponCountForOwner(owner);
	}

	@Override
	public ProcessResult<List<DateIndexedEntity>> discountsGroupByPublishDate(
			String owner, int offset) {
		if (offset == 0) {
			try {
				discountsRemoteRefresh(owner, FuncConst.VAL_DISCOUNT_TYPE_ALL);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		List<DateIndexedEntity> resultList = discountData
				.discountsGroupByPublishDate(owner, offset);
		return new ProcessResult<List<DateIndexedEntity>>(resultList);
	}

	@Override
	public int discountCountForOwner(String owner) {
		return discountData.discountCountForOwner(owner);
	}

	@Override
	public ProcessResult<List<DiscountEntity>> newestActivities(int offset) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<List<DiscountEntity>>(
					new ArrayList<DiscountEntity>());
		}
		MethodExecuter<List<DiscountEntity>> executer = new MethodExecuter<List<DiscountEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<DiscountEntity>> result) throws Exception {
				List<DiscountEntity> discounts = new ArrayList<DiscountEntity>();
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource != null) {
					if (resource.has(NAME_DATA)) {
						JSONArray data = resource.getJSONArray(NAME_DATA);
						// 不保存数据，直接组装数据
						discounts = assembleDiscounts(data);
					}
				}
				result.setSuccessObj(discounts);
			}
		};
		return executer.execute(new GetNewestActivitiesMethod(offset, location));
	}

	@Override
	public ProcessResult<List<DiscountEntity>> newestActivitiesFromLocal(
			int offset) {
		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<List<DiscountEntity>>(
					new ArrayList<DiscountEntity>());
		}
		List<DiscountEntity> activities = discountData
				.activitiesOrderByPublishDate(location, offset);
		return new ProcessResult<List<DiscountEntity>>(activities);
	}

	@Override
	public ProcessResult<List<DiscountEntity>> activitiesForCategory(
			CategoryEntity category, int offset) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<List<DiscountEntity>>(
					new ArrayList<DiscountEntity>());
		}
		MethodExecuter<List<DiscountEntity>> executer = new MethodExecuter<List<DiscountEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<DiscountEntity>> result) throws Exception {
				List<DiscountEntity> discounts = new ArrayList<DiscountEntity>();
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource != null) {
					if (resource.has(NAME_DATA)) {
						JSONArray data = resource.getJSONArray(NAME_DATA);
						discounts = assembleDiscounts(data);
					}
				}
				result.setSuccessObj(discounts);
			}
		};
		return executer.execute(new GetActivitiesByDistanceMethod(location,
				category, offset));
	}

	/**
	 * v1.1版后不再本地存储，直接远程获取
	 * 
	 * @param storeId
	 * @param cateId
	 * @return
	 */
	@Override
	public ProcessResult<List<DiscountEntity>> couponsForStoreRemoteRefresh(
			final String storeId, final String cateId, int offset) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null) {
			List<DiscountEntity> data = new ArrayList<DiscountEntity>();
			return new ProcessResult<List<DiscountEntity>>(data);
		}

		// String updateTime = null;
		// SyncEntity syncEntity = ProcessorFactory.getSyncProcessor()
		// .storeCouponSyncEntity(storeId);
		// if (syncEntity != null) {
		// updateTime = syncEntity.getUpdateTime();
		// }
		// if (updateTime == null) {
		// updateTime = "-1";
		// }

		MethodExecuter<List<DiscountEntity>> executer = new MethodExecuter<List<DiscountEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<DiscountEntity>> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UPDATE_TIME)) {
					JSONArray data = resource.getJSONArray(NAME_DATA);
					result.setSuccessObj(assembleDiscounts(data));
					// JSONObject data = resource.getJSONObject(NAME_DATA);
					// int count = saveReponseDiscountsData(data);
					// if (count > 0) {
					// result.setSuccessObj(Boolean.TRUE);
					// }
					//
					// String updateTime = resource.getString(COL_NAME_UPDATE_TIME);
					// syncData.save(FuncConst.SYNC_TABLE_STORE_COUPON, storeId,
					// updateTime);
				}
				// if (result.getSuccessObj() == null) {
				// result.setSuccessObj(Boolean.FALSE);
				// }
			}
		};
		return executer.execute(new GetCouponsByStoreMethod(storeId, cateId,
				location, offset));
	}

	@Override
	public ProcessResult<List<DiscountEntity>> couponsForStore(String storeId,
			String cateId, int offset) {
		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null) {
			return new ProcessResult<List<DiscountEntity>>(
					new ArrayList<DiscountEntity>());
		}
		List<DiscountEntity> coupons = discountData.couponsForStore(storeId,
				cateId, location, offset);
		return new ProcessResult<List<DiscountEntity>>(coupons);
	}

	@Override
	public ProcessResult<Boolean> deleteDiscount(final DiscountEntity discount) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					discountData.deleteDiscount(discount.getUuid());
					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new DelDiscountMethod(discount));
	}

	@Override
	public ProcessResult<Boolean> publishDiscount(final DiscountEntity discount) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (!resource.has(COL_NAME_UUID)) {
					result.setSuccessObj(Boolean.FALSE);
				} else {
					if (RunEnv.getInstance().getUser() == null) {
						result.setSuccessObj(Boolean.TRUE);
						return;
					}
					
					String uuid = resource.getString(COL_NAME_UUID);
					discount.setUuid(uuid);
					discount.setPublishDate(resource.getString(COL_NAME_PUBLISH_DATE));
					discount.setPublishTime(resource.getLong(COL_NAME_PUBLISH_TIME));
					discount.setStatus(resource.getString(COL_NAME_STATUS));
					
					List<DiscountImageEntity> discountImages = new ArrayList<DiscountImageEntity>();
					JSONArray jsonImages = resource.getJSONArray(NAME_IMAGES);
					for (int i = 0; i < jsonImages.length(); i++) {
						discountImages.add(new DiscountImageEntity(jsonImages
								.getJSONObject(i)));
					}

					List<DiscountStorefrontEntity> storefronts = new ArrayList<DiscountStorefrontEntity>();
					JSONArray jsonFronts = resource.getJSONArray(NAME_STOREFRONTS);
					for (int i = 0; i < jsonFronts.length(); i++) {
						storefronts.add(new DiscountStorefrontEntity(jsonFronts
								.getJSONObject(i)));
					}

					discountImageData.save(discountImages
							.toArray(new DiscountImageEntity[discountImages.size()]));
					discountStorefrontData.save(storefronts
							.toArray(new DiscountStorefrontEntity[storefronts.size()]));
					discountData.save(new DiscountEntity[] { discount });

					result.setSuccessObj(Boolean.TRUE);
				}
			}
		};
		return executer.execute(new PublishDiscountMethod(discount));
	}

	@Override
	public ProcessResult<Boolean> updateDiscount(final DiscountEntity discount,
			final List<FileEntity> deletedImages, final List<FileEntity> addedImages) {
		final List<StorefrontEntity> deletedStorfronts = getDeletedStorefronts(discount);
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (!resource.has(COL_NAME_UUID)) {
					result.setSuccessObj(Boolean.FALSE);
				} else {
					// 删除活动图片
					for (FileEntity image : deletedImages) {
						discountImageData.deleteByDiscountImage(discount.getUuid(),
								image.getAliasName());
					}
					// 删除活动门店对应
					for (StorefrontEntity front : deletedStorfronts) {
						discountStorefrontData.deleteByDiscountStorefront(
								discount.getUuid(), front.getUuid());
					}

					// 新增的活动图片
					List<DiscountImageEntity> discountImages = new ArrayList<DiscountImageEntity>();
					JSONArray jsonImages = resource.getJSONArray(NAME_IMAGES);
					for (int i = 0; i < jsonImages.length(); i++) {
						discountImages.add(new DiscountImageEntity(jsonImages
								.getJSONObject(i)));
					}

					// 新增的活动门店对应
					List<DiscountStorefrontEntity> storefronts = new ArrayList<DiscountStorefrontEntity>();
					JSONArray jsonFronts = resource.getJSONArray(NAME_STOREFRONTS);
					for (int i = 0; i < jsonFronts.length(); i++) {
						storefronts.add(new DiscountStorefrontEntity(jsonFronts
								.getJSONObject(i)));
					}

					discountImageData.save(discountImages
							.toArray(new DiscountImageEntity[discountImages.size()]));
					discountStorefrontData.save(storefronts
							.toArray(new DiscountStorefrontEntity[storefronts.size()]));
					discountData.save(new DiscountEntity[] { discount });

					result.setSuccessObj(Boolean.TRUE);
				}
			}
		};
		return executer.execute(new UpdateDiscountMethod(discount, addedImages,
				deletedImages, getAddedStorefronts(discount), deletedStorfronts));
	}

	private List<StorefrontEntity> getAddedStorefronts(DiscountEntity discount) {
		List<StorefrontEntity> storefronts = discount.getStorefronts();
		List<StorefrontEntity> oldStorefronts = new StorefrontData()
				.storefrontsForDiscount(discount.getUuid());
		List<StorefrontEntity> addedStorefronts = new ArrayList<StorefrontEntity>();
		for (StorefrontEntity front : storefronts) {
			if (!oldStorefronts.contains(front)) {
				addedStorefronts.add(front);
			}
		}
		return addedStorefronts;
	}

	private List<StorefrontEntity> getDeletedStorefronts(DiscountEntity discount) {
		List<StorefrontEntity> storefronts = discount.getStorefronts();
		List<StorefrontEntity> oldStorefronts = new StorefrontData()
				.storefrontsForDiscount(discount.getUuid());
		List<StorefrontEntity> deletedStorefronts = new ArrayList<StorefrontEntity>();
		for (StorefrontEntity front : oldStorefronts) {
			if (!storefronts.contains(front)) {
				deletedStorefronts.add(front);
			}
		}
		return deletedStorefronts;
	}

	@Override
	public ProcessResult<Boolean> visitDiscount(final DiscountEntity discount) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					discountData.increaseVisitCount(discount);
					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new VisitDiscountMethod(discount));
	}

	@Override
	public ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>> storesHasCouponRemoteRefresh() {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null) {
			return new ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>>(
					new HashMap<CategoryEntity, List<CouponStoreEntity>>());
		}

		MethodExecuter<Map<CategoryEntity, List<CouponStoreEntity>>> executer = new MethodExecuter<Map<CategoryEntity, List<CouponStoreEntity>>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>> result)
					throws Exception {
				List<CouponStoreEntity> stores = new ArrayList<CouponStoreEntity>();
				JSONObject resource = (JSONObject) mResult.getResource();
				if (resource.has(COL_NAME_UPDATE_TIME)) {
					String updateTime = resource.getString(COL_NAME_UPDATE_TIME);
					if (resource.has(NAME_DATA)) {
						JSONArray jsons = resource.getJSONArray(NAME_DATA);
						if (jsons != null) {
							for (int i = 0; i < jsons.length(); i++) {
								JSONObject json = jsons.getJSONObject(i);
								json.put(DataConst.COL_NAME_CITY_NAME, location.getCityName());
								stores.add(new CouponStoreEntity(json));
							}
							if (stores.size() > 0) {
								CouponStoreData couponStoreData = new CouponStoreData();
								couponStoreData
										.deleteCouponStoresByCity(location.getCityName());
								couponStoreData.save(stores
										.toArray(new CouponStoreEntity[stores.size()]));
								result
										.setSuccessObj(storesHasCouponFromLocal().getSuccessObj());
							}
						}
					}

					syncData.save(FuncConst.SYNC_TABLE_COUPON_MAIN,
							location.getCityName(), updateTime);
				}
			}
		};
		return executer.execute(new GetStoresHasCouponMethod(location));
	}

	@Override
	public ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>> storesHasCouponFromLocal() {
		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null) {
			return new ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>>(
					new HashMap<CategoryEntity, List<CouponStoreEntity>>());
		}

		Map<CategoryEntity, List<CouponStoreEntity>> result = new HashMap<CategoryEntity, List<CouponStoreEntity>>();
		List<CategoryEntity> categories = new CategoryData()
				.categoriesHasCoupons(location);
		for (CategoryEntity category : categories) {
			result.put(category, new CouponStoreData().couponStoresByLocation(
					category.getUuid(), location));
		}
		return new ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>>(
				result);
	}

	@Override
	public ProcessResult<List<DiscountEntity>> discountsByKeyword(
			final String keyword, int offset) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<List<DiscountEntity>>(
					new ArrayList<DiscountEntity>());
		}
		MethodExecuter<List<DiscountEntity>> executer = new MethodExecuter<List<DiscountEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<DiscountEntity>> result) throws Exception {
				List<DiscountEntity> discounts = new ArrayList<DiscountEntity>();
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource != null) {
					if (resource.has(NAME_DATA)) {
						JSONArray data = resource.getJSONArray(NAME_DATA);
						discounts = assembleDiscounts(data);
					}
				}
				result.setSuccessObj(discounts);
			}
		};
		return executer.execute(new GetDiscountsByKeywordMethod(location, keyword,
				offset));
	}

	@Override
	public ProcessResult<DiscountEntity> discountById(final String uuid) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		DiscountEntity discount = discountData.discountById(uuid, location);
		if (discount != null) {
			return new ProcessResult<DiscountEntity>(discount);
		}
		MethodExecuter<DiscountEntity> executer = new MethodExecuter<DiscountEntity>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<DiscountEntity> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource != null && resource.length() > 0) {
					saveReponseDiscountsData(resource);
					DiscountEntity discount = discountData.discountById(uuid, location);
					result.setSuccessObj(discount);
				}
			}
		};
		return executer.execute(new GetDiscountByIdMethod(uuid));
	}

	@Override
	public ProcessResult<CommentEntity> publishComment(final CommentEntity comment) {
		MethodExecuter<CommentEntity> executer = new MethodExecuter<CommentEntity>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<CommentEntity> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource != null && resource.get(COL_NAME_UUID) != null) {
					comment.setUuid(resource.getString(COL_NAME_UUID));
					comment.setPublishTime(resource.getString(COL_NAME_PUBLISH_TIME));
					result.setSuccessObj(comment);
				}
			}
		};
		return executer.execute(new PublishCommentMethod(comment));
	}

	private ProcessResult<Boolean> favoritDiscount(final DiscountEntity discount,
			final String action) {
		String tempUserId = "";
		UserEntity user = RunEnv.getInstance().getUser();
		if (user != null) {
			tempUserId = user.getUuid();
		} else {
			tempUserId = ViewUtils.getDeviceId();
		}
		final String userId = tempUserId;

		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource != null && resource.has(COL_NAME_UPDATE_TIME)) {
					String uuid = resource.getJSONObject(NAME_DATA).getString(
							COL_NAME_UUID);
					// 保存到本地
					if (action.equals(FuncConst.KEY_ACTION_FAVORIT)) {
						FavoritEntity favorit = new FavoritEntity();
						favorit.setUserId(userId);
						favorit.setDiscount(discount);
						favorit.setUuid(uuid);
						favoritDiscountLocal(favorit);

						// 记录同步更新记录
						SyncEntity sync = syncData.syncEntity(
								FuncConst.SYNC_TABLE_FAVORIT_DISCOUNT, userId);
						if (sync == null) {
							sync = new SyncEntity();
							sync.setTableName(FuncConst.SYNC_TABLE_FAVORIT_DISCOUNT);
							sync.setItemId(userId);
							sync.setUuid(System.currentTimeMillis() + "");
						}
						sync.setUpdateTime(resource.getString(COL_NAME_UPDATE_TIME));

						syncData.save(new SyncEntity[] { sync });

					} else if (action.equals(FuncConst.KEY_ACTION_UNFAVORIT)) {
						// 仅删除收藏记录，不删除活动记录
						favoritData.deleteFavorit(userId, discount.getUuid());
					}
					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}

			}
		};

		return executer.execute(new FavoritDiscountMethod(userId, discount
				.getUuid(), action));
	}

	@Override
	public Boolean favoritDiscountLocal(FavoritEntity favorit) {
		DiscountEntity discount = favorit.getDiscount();
		favoritData.save(new FavoritEntity[] { favorit });
		if (discount != null) {
			// 保存活动
			discountData.save(new DiscountEntity[] { discount });
			// 保存图片
			discountImageData.save(discount.getImages().toArray(
					new DiscountImageEntity[discount.getImages().size()]));
			// 保存门店
			discountStorefrontData.save(discount.getStorefronts().toArray(
					new DiscountStorefrontEntity[discount.getStorefronts().size()]));
		}

		return Boolean.TRUE;
	}

	@Override
	public ProcessResult<Boolean> favoritDiscount(DiscountEntity discount) {
		return favoritDiscount(discount, FuncConst.KEY_ACTION_FAVORIT);
	}

	@Override
	public ProcessResult<Boolean> unfavoritDiscount(DiscountEntity discount) {
		return favoritDiscount(discount, FuncConst.KEY_ACTION_UNFAVORIT);
	}

	@Override
	public ProcessResult<List<StorefrontEntity>> getStoreFrontsByDiscount(
			final DiscountEntity discount, int limit, int offset) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<List<StorefrontEntity>>(
					new ArrayList<StorefrontEntity>());
		}

		MethodExecuter<List<StorefrontEntity>> executer = new MethodExecuter<List<StorefrontEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<StorefrontEntity>> result) throws Exception {
				List<StorefrontEntity> fronts = new ArrayList<StorefrontEntity>();
				JSONArrayResource resource = (JSONArrayResource) mResult.getResource();
				for (int i = 0; i < resource.length(); i++) {
					JSONObject jsonFront = resource.getJSONObject(i);
					DiscountStorefrontEntity front = new DiscountStorefrontEntity(
							jsonFront, true);
					front.setDiscountId(discount.getUuid());
					front.setDistance(jsonFront.getDouble(NAME_DISTANCE));
					fronts.add(front);
				}
				result.setSuccessObj(fronts);
			}
		};
		return executer.execute(new GetStoreFrontsByDiscountMethod(discount,
				location, limit, offset));
	}

	@Override
	public ProcessResult<List<CommentEntity>> getCommentsByDiscount(
			DiscountEntity discount, int limit, int offset) {
		MethodExecuter<List<CommentEntity>> executer = new MethodExecuter<List<CommentEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<CommentEntity>> result) throws Exception {
				List<CommentEntity> comments = new ArrayList<CommentEntity>();
				JSONArrayResource resource = (JSONArrayResource) mResult.getResource();
				for (int i = 0; i < resource.length(); i++) {
					JSONObject jsonComment = resource.getJSONObject(i);
					comments.add(new CommentEntity(jsonComment));
				}
				result.setSuccessObj(comments);
			}
		};
		return executer.execute(new GetCommentsByDiscountMethod(discount, limit,
				offset));
	}

	@Override
	public ProcessResult<List<DiscountEntity>> getUserFavorits(int offset) {
		String userId = null;
		UserEntity user = RunEnv.getInstance().getUser();
		if (user == null) {
			userId = ViewUtils.getDeviceId();
		} else {
			userId = user.getUuid();
		}

		// 远程刷新数据
		if (offset == 0) {
			refreshUserFavorits(userId);
		}

		List<DiscountEntity> favorits = favoritData.userFovarits(userId, offset);
		return new ProcessResult<List<DiscountEntity>>(favorits);
	}

	@Override
	public ProcessResult<Boolean> refreshUserFavorits(final String userId) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<Boolean>(Boolean.FALSE);
		}

		final String tableName = FuncConst.SYNC_TABLE_FAVORIT_DISCOUNT;
		String updateTime = "-1";
		SyncEntity sync = syncData.syncEntity(tableName, userId);
		if (sync != null) {
			updateTime = sync.getUpdateTime();
		}
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UPDATE_TIME)) {

					// 更新记录
					JSONArray data = resource.getJSONArray(NAME_DATA);
					for (int i = 0; i < data.length(); i++) {
						JSONObject row = data.getJSONObject(i);
						FavoritEntity favorit = new FavoritEntity(
								row.getJSONObject(NAME_FAVORIT));
						favorit.setUserId(userId);

						JSONObject jsonDiscount = row.getJSONObject(NAME_DISCOUNT);
						DiscountEntity discount = assembleDiscount(jsonDiscount);
						if (jsonDiscount.has(NAME_STOREFRONT)) {
							List<StorefrontEntity> sfs = new ArrayList<StorefrontEntity>();
							DiscountStorefrontEntity sf = new DiscountStorefrontEntity(
									jsonDiscount.getJSONObject(NAME_STOREFRONT), true);
							sf.setDiscountId(discount.getUuid());
							sfs.add(sf);
							discount.setStorefronts(sfs);
						}
						favorit.setDiscount(discount);
						favoritDiscountLocal(favorit);
					}

					// 记录同步更新记录
					SyncEntity sync = syncData.syncEntity(tableName, userId);
					if (sync == null) {
						sync = new SyncEntity();
						sync.setTableName(tableName);
						sync.setItemId(userId);
						sync.setUuid(System.currentTimeMillis() + "");
					}
					sync.setUpdateTime(resource.getString(COL_NAME_UPDATE_TIME));

					syncData.save(new SyncEntity[] { sync });
					if (data.length() > 0) {
						result.setSuccessObj(Boolean.TRUE);
					} else {
						result.setSuccessObj(Boolean.FALSE);
					}
				}
			}
		};
		return executer.execute(new RefreshFavoritsMethod(userId, updateTime,
				location));
	}

	@Override
	public ProcessResult<DiscountEntity> getDiscountExtWithFavorit(
			final DiscountEntity discount) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<DiscountEntity>(discount);
		}

		MethodExecuter<DiscountEntity> executer = new MethodExecuter<DiscountEntity>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<DiscountEntity> result) throws Exception {
				JSONObject resource = (JSONObject) mResult.getResource();
				// 门店数
				if (resource.has(NAME_FRONT_COUNT)) {
					discount.setFrontCount(resource.getInt(NAME_FRONT_COUNT));
				}
				// 评论数目
				if (resource.has(NAME_COMMENT_COUNT)) {
					discount.setCommentCount(resource.getInt(NAME_COMMENT_COUNT));
				}
				// 评论列表
				if (resource.has(NAME_COMMENTS)) {
					JSONArray jsonComments = resource.getJSONArray(NAME_COMMENTS);
					for (int i = 0; i < jsonComments.length(); i++) {
						CommentEntity comment = new CommentEntity(
								jsonComments.getJSONObject(i));
						discount.addComment(comment);
					}
				}
				result.setSuccessObj(discount);
			}
		};
		return executer.execute(new GetDiscountExtWithFavoritMethod(discount,
				location));
	}

	@Override
	public boolean isUserFavorit(String discountId) {
		String userId = null;
		UserEntity user = RunEnv.getInstance().getUser();
		if (user != null) {
			userId = user.getUuid();
		}else{
			userId = ViewUtils.getDeviceId();
		}
		return favoritData.isFavorited(userId, discountId);
	}
}

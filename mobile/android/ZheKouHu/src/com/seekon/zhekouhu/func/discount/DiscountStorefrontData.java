package com.seekon.zhekouhu.func.discount;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.func.store.StorefrontData;
import com.seekon.zhekouhu.func.store.StorefrontEntity;

public class DiscountStorefrontData extends StorefrontData {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_DISCOUNT_STOREFRONT
			+ " (uuid, discount_id, store_id, storefront_id, ord_index) "
			+ " values (?, ?, ?, ?, ?)";
	private static final String QUERY_BASE_SQL = " select ds.storefront_id from "
			+ DataConst.TABLE_DISCOUNT_STOREFRONT + " ds where ds.discount_id = ? ";
	private static final String DEL_BASE_SQL = "delete from "
			+ DataConst.TABLE_DISCOUNT_STOREFRONT;

	@Override
	public int save(StorefrontEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (StorefrontEntity front : data) {
			if (front.getName() != null && front.getCity() != null
					&& front.getStore() != null) {
				super.save(front);
			}

			if (front instanceof DiscountStorefrontEntity) {
				DiscountStorefrontEntity dsf = (DiscountStorefrontEntity) front;
				executeUpdate(
						UPDATE_SQL,
						new Object[] { dsf.getKeyId(), dsf.getDiscountId(),
								front.getStore().getUuid(), front.getUuid(), dsf.getOrdIndex() });
			}
		}
		return data.length;
	}

	@Override
	protected StorefrontEntity processRow(Cursor cursor) {
		int i = 0;
		String frontId = cursor.getString(i++);
		return new StorefrontData().storefrontById(frontId);
	}

	public List<StorefrontEntity> storefrontsByDiscount(String discountId,
			LocationEntity location) {
		String sql = QUERY_BASE_SQL;
		String[] params = null;
		if (location != null) {
			sql += " and EXISTS (select 1 from z_storefront f where ds.storefront_id = f.uuid and f.city_name = ?)";
			params = new String[] { discountId, location.getCityName() };
		} else {
			params = new String[] { discountId };
		}
		return query(sql, params);
	}

	public void deleteByDiscount(String discountId) {
		String sql = DEL_BASE_SQL + " where discount_id = ?";
		executeUpdate(sql, new Object[] { discountId });
	}

	public void deleteByDiscountStorefront(String discountId, String frontId) {
		String sql = DEL_BASE_SQL + " where discount_id = ? and storefront_id = ?";
		executeUpdate(sql, new Object[] { discountId, frontId });
	}

	public void deleteById(String uuid) {
		String sql = DEL_BASE_SQL + " where uuid = ?";
		executeUpdate(sql, new Object[] { uuid });
	}
}

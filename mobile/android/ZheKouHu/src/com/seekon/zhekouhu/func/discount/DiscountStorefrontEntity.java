package com.seekon.zhekouhu.func.discount;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.Logger;

public class DiscountStorefrontEntity extends StorefrontEntity {

	private static final long serialVersionUID = 6248742606989280409L;

	private String keyId;
	private String discountId;
	private int ordIndex;

	public DiscountStorefrontEntity() {
		super();
	}

	/**
	 * 兼容1.0的方式，仅初始化uuid（front_id），store（store_id），keyid（uuid），discount_id，
	 * order_index
	 * 
	 * @param json
	 */
	public DiscountStorefrontEntity(JSONObject json) {
		super();
		try {
			keyId = json.getString(DataConst.COL_NAME_UUID);
			discountId = json.getString(DataConst.COL_NAME_DISCOUNT_ID);
			ordIndex = json.getInt(DataConst.COL_NAME_ORD_INDEX);
			setUuid(json.getString(DataConst.COL_NAME_STOREFRONT_ID));
			if (getStore() == null && json.has(DataConst.COL_NAME_STORE_ID)) {
				StoreEntity store = new StoreEntity();
				store.setUuid(json.getString(DataConst.COL_NAME_STORE_ID));
				setStore(store);
			}
		} catch (Exception e) {
			Logger
					.error(DiscountImageEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * 1.1版本后，DiscountStorefrontEntity作为storefront的子类，增加key_id作为对应关系的主键
	 * 
	 * @param json
	 * @param initSuper
	 */
	public DiscountStorefrontEntity(JSONObject json, boolean initSuper) {
		super(json);
		try {
			keyId = json.getString(FuncConst.NAME_KEY_ID);
			if (json.has(DataConst.COL_NAME_DISCOUNT_ID)) {
				discountId = json.getString(DataConst.COL_NAME_DISCOUNT_ID);
			}
			if (json.has(DataConst.COL_NAME_ORD_INDEX)) {
				ordIndex = json.getInt(DataConst.COL_NAME_ORD_INDEX);
			}
		} catch (Exception e) {
			Logger
					.error(DiscountImageEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public String getDiscountId() {
		return discountId;
	}

	public void setDiscountId(String discountId) {
		this.discountId = discountId;
	}

	public int getOrdIndex() {
		return ordIndex;
	}

	public void setOrdIndex(int ordIndex) {
		this.ordIndex = ordIndex;
	}

}

package com.seekon.zhekouhu.func.discount;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class FavoritDiscountMethod extends JSONObjResourceMethod {

	private static final URI FAVORIT_DISCOUNT_URI = URI
			.create(Const.SERVER_APP_URL + "/favoritDiscount");

	private String userId;
	private String discountId;
	private String action;

	public FavoritDiscountMethod(String userId, String discountId, String action) {
		super();
		this.userId = userId;
		this.discountId = discountId;
		this.action = action;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_USER_ID, userId);
		params.put(DataConst.COL_NAME_DISCOUNT_ID, discountId);
		params.put(DataConst.COL_NAME_TYPE, action);

		return new BaseRequest(Method.POST, FAVORIT_DISCOUNT_URI, null, params);
	}

}

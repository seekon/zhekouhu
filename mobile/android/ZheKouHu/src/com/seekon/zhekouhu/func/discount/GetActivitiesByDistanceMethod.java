package com.seekon.zhekouhu.func.discount;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetActivitiesByDistanceMethod extends JSONObjResourceMethod {

	private static final String GET_ACTIVITIES_BY_DISTANCE_URL = Const.SERVER_APP_URL
			+ "/getActivitiesByDistance";

	private static final int VERSION = 110;
	private LocationEntity location;
	private CategoryEntity category;
	private int offset;

	public GetActivitiesByDistanceMethod(LocationEntity location,
			CategoryEntity category, int offset) {
		super();
		this.location = location;
		this.category = category;
		this.offset = offset;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer(GET_ACTIVITIES_BY_DISTANCE_URL);
		url.append("/" + category.getUuid());
		url.append("/" + offset);
		try {
			url.append("/" + URLEncoder.encode(location.getCityName(), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetActivitiesByDistanceMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		url.append("/" + location.getLat());
		url.append("/" + location.getLng());
		url.append("/" + VERSION);
		return new BaseRequest(Method.GET, url.toString(), null, null);
	}

}

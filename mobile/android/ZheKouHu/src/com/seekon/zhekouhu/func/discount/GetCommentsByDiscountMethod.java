package com.seekon.zhekouhu.func.discount;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONArrayResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetCommentsByDiscountMethod extends JSONArrayResourceMethod {

	private static final String GET_COMMENTS_BY_DISCOUNT_URL = Const.SERVER_APP_URL
			+ "/getCommentsByDiscount";
	private DiscountEntity discount;
	private int limit;
	private int offset;

	public GetCommentsByDiscountMethod(DiscountEntity discount, int limit,
			int offset) {
		super();
		this.discount = discount;
		this.limit = limit;
		this.offset = offset;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer(GET_COMMENTS_BY_DISCOUNT_URL);
		url.append("/" + discount.getUuid());
		url.append("/" + limit);
		url.append("/" + offset);

		return new BaseRequest(Method.GET, url.toString(), null, null);
	}
}

package com.seekon.zhekouhu.func.discount;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetCouponsByStoreMethod extends JSONObjResourceMethod {

	private static final String GET_COUPONS_BY_STORE_URL = Const.SERVER_APP_URL
			+ "/getCouponsByStore";

	private String storeId;
	private String cateId;
	private LocationEntity location;
	private int offset;

	public GetCouponsByStoreMethod(String storeId, String cateId,
			LocationEntity location, int offset) {
		super();
		this.storeId = storeId;
		this.cateId = cateId;
		this.location = location;
		this.offset = offset;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer(GET_COUPONS_BY_STORE_URL);
		url.append("/" + storeId);
		url.append("/" + cateId);
		try {
			url.append("/" + URLEncoder.encode(location.getCityName(), "utf-8"));
			url.append("/" + location.getLat());
			url.append("/" + location.getLng());
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetCouponsByStoreMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		url.append("/" + offset);

		return new BaseRequest(Method.GET, url.toString(), null, null);
	}

}

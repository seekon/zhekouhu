package com.seekon.zhekouhu.func.discount;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetDiscountByIdMethod extends JSONObjResourceMethod {

	private static final String GET_DISCOUNT_BY_ID_URL = Const.SERVER_APP_URL
			+ "/getDiscountById/";

	private String uuid;

	public GetDiscountByIdMethod(String uuid) {
		super();
		this.uuid = uuid;
	}

	@Override
	protected Request buildRequest() {

		return new BaseRequest(Method.GET, GET_DISCOUNT_BY_ID_URL + uuid, null,
				null);
	}

}

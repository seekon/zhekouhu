package com.seekon.zhekouhu.func.discount;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetDiscountExtWithFavoritMethod extends JSONObjResourceMethod{
	
	private static final String GET_DISCOUNT_EXT_WITH_FAVORIT_URL = Const.SERVER_APP_URL
			+ "/getDiscountExtWithFavorit";
	
	private DiscountEntity discount;
	
	private LocationEntity location;
	
	public GetDiscountExtWithFavoritMethod(DiscountEntity discount,
			LocationEntity location) {
		super();
		this.discount = discount;
		this.location = location;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer(GET_DISCOUNT_EXT_WITH_FAVORIT_URL);
		url.append("/" + discount.getUuid());
		try {
			url.append("/" + URLEncoder.encode(location.getCityName(), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetActivitiesByDistanceMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		
		return new BaseRequest(Method.GET, url.toString(), null, null);
	}
	
	
}

package com.seekon.zhekouhu.func.discount;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetDiscountsByKeywordMethod extends JSONObjResourceMethod {

	private static final String GET_DISCOUNTS_BY_KEYWORD_URL = Const.SERVER_APP_URL
			+ "/getDiscountsByKeyword";

	private LocationEntity location;
	private String keyword;
	private int offset;

	public GetDiscountsByKeywordMethod(LocationEntity location, String keyword,
			int offset) {
		super();
		this.location = location;
		this.keyword = keyword;
		this.offset = offset;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer(GET_DISCOUNTS_BY_KEYWORD_URL);
		try {
			url.append("/" + URLEncoder.encode(location.getCityName(), "utf-8"));
			url.append("/" + location.getLat());
			url.append("/" + location.getLng());
			url.append("/" + URLEncoder.encode(keyword, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetActivitiesByDistanceMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		url.append("/" + offset);

		return new BaseRequest(Method.GET, url.toString(), null, null);
	}

}

package com.seekon.zhekouhu.func.discount;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetDiscountsByOwnerMethod extends JSONObjResourceMethod {

//	private static final String GET_ACTIVITIES_BY_OWNER_URL = Const.SERVER_APP_URL
//			+ "/getActivitiesByOwner/";
//
//	private static final String GET_COUPONS_BY_OWNER_URL = Const.SERVER_APP_URL
//			+ "/getCouponsByOwner/";

	private static final String GET_DISCOUNTS_BY_OWNER_URL = Const.SERVER_APP_URL
			+ "/getDiscountsByOwner/";
	
	private String owner;
	private String type;
	private String updateTime;

	public GetDiscountsByOwnerMethod(String owner, String type, String updateTime) {
		super();
		this.owner = owner;
		this.type = type;
		this.updateTime = updateTime;
	}

	@Override
	protected Request buildRequest() {
//		if (type.equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
//			return new BaseRequest(Method.GET, GET_ACTIVITIES_BY_OWNER_URL + owner
//					+ "/" + updateTime, null, null);
//		}
		return new BaseRequest(Method.GET, GET_DISCOUNTS_BY_OWNER_URL + owner + "/"
				+ updateTime, null, null);
	}

}

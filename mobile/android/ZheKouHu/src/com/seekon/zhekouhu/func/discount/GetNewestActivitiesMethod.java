package com.seekon.zhekouhu.func.discount;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetNewestActivitiesMethod extends JSONObjResourceMethod {

	private static final String GET_NEWEST_ACTIVITIES_URL = Const.SERVER_APP_URL
			+ "/getNewestActivities";

	private int offset;
	private LocationEntity location;

	public GetNewestActivitiesMethod(int offset, LocationEntity location) {
		super();
		this.offset = offset;
		this.location = location;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer(GET_NEWEST_ACTIVITIES_URL);
		try {
			url.append("/" + URLEncoder.encode(location.getCityName(), "utf-8"));
			url.append("/" + location.getLat());
			url.append("/" + location.getLng());
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetActivitiesByDistanceMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		url.append("/" + offset);

		return new BaseRequest(Method.GET, url.toString(), null, null);
	}

}

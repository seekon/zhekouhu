package com.seekon.zhekouhu.func.discount;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONArrayResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetStoreFrontsByDiscountMethod extends JSONArrayResourceMethod {

	private static final String GET_STOREFRONTS_BY_DISCOUNT_URL = Const.SERVER_APP_URL
			+ "/getStorefrontsByDiscount";

	private int limit;
	private int offset;
	private LocationEntity location;
	private DiscountEntity discount;

	public GetStoreFrontsByDiscountMethod(DiscountEntity discount,
			LocationEntity location, int limit, int offset) {
		super();
		this.limit = limit;
		this.offset = offset;
		this.location = location;
		this.discount = discount;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer(GET_STOREFRONTS_BY_DISCOUNT_URL);
		url.append("/" + discount.getUuid());
		try {
			url.append("/" + URLEncoder.encode(location.getCityName(), "utf-8"));
			url.append("/" + location.getLat());
			url.append("/" + location.getLng());
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetActivitiesByDistanceMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		url.append("/" + limit);
		url.append("/" + offset);

		return new BaseRequest(Method.GET, url.toString(), null, null);
	}

}

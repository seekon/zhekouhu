package com.seekon.zhekouhu.func.discount;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class PublishCommentMethod extends JSONObjResourceMethod {

	private static final URI PUBLISH_COMMENT_URI = URI
			.create(Const.SERVER_APP_URL + "/publishComment");

	private CommentEntity comment;

	public PublishCommentMethod(CommentEntity comment) {
		super();
		this.comment = comment;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_DISCOUNT_ID, comment.getDiscountId());
		params.put(DataConst.COL_NAME_CONTENT, comment.getContent());
		params.put(DataConst.COL_NAME_PUBLISHER, comment.getPublisher());
		params.put(DataConst.COL_NAME_TYPE, comment.getType());
		return new BaseRequest(Method.POST, PUBLISH_COMMENT_URI, null, params);
	}

}

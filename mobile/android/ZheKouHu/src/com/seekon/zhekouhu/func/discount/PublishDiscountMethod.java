package com.seekon.zhekouhu.func.discount;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_CATEGORY_ID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_CONTENT;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_END_DATE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_LOCATION;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_ORIGIN;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_PAR_VALUE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_PUBLISHER;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_START_DATE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_STORE_ID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_TYPE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STOREFRONTS;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.rest.MultipartRequest;
import com.seekon.zhekouhu.rest.MultipartRestMethod;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;
import com.seekon.zhekouhu.utils.Logger;

public class PublishDiscountMethod extends MultipartRestMethod<JSONObjResource> {

	private static final URI PUBLISH_DISCOUNT_URI = URI
			.create(Const.SERVER_APP_URL + "/publishDiscount");

	private static final URI ANONYM_PUBLISH_DISCOUNT_URI = URI
			.create(Const.SERVER_APP_URL + "/anonymPublishDiscount");
	
	protected DiscountEntity discount;

	public PublishDiscountMethod(DiscountEntity discount) {
		super();
		this.discount = discount;
	}

	@Override
	protected Request buildRequest() {
		URI uri = PUBLISH_DISCOUNT_URI;
		if(RunEnv.getInstance().getUser() == null){
			uri = ANONYM_PUBLISH_DISCOUNT_URI;
		}
		return new MultipartRequest(uri, null,
				buildRequestParams(), discount.getImages());
	}

	protected Map<String, String> buildRequestParams() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(COL_NAME_CONTENT, discount.getContent());
		params.put(COL_NAME_START_DATE, discount.getStartDate() + "");
		params.put(COL_NAME_END_DATE, discount.getEndDate() + "");
		params.put(COL_NAME_TYPE, discount.getType());
		params.put(COL_NAME_PUBLISHER, discount.getPublisher());
		params.put(COL_NAME_PAR_VALUE, discount.getParValue() + "");
		params.put(COL_NAME_CATEGORY_ID, discount.getCategory().getUuid());
		params.put(NAME_STOREFRONTS,
				getStorefrontsJSONString(discount.getStorefronts()));
		if(discount.getLocation() != null){
			params.put(COL_NAME_LOCATION, discount.getLocation());
		}
		params.put(COL_NAME_ORIGIN, discount.getOrigin());
		
		return params;
	}

	@Override
	protected JSONObjResource parseResponseBody(String responseBody)
			throws Exception {
		return new JSONObjResource(responseBody);
	}

	protected String getStorefrontsJSONString(List<StorefrontEntity> fronts) {
		JSONArray jsons = new JSONArray();
		try {
			for (StorefrontEntity front : fronts) {
				JSONObject json = new JSONObject();
				json.put(COL_NAME_UUID, front.getUuid());
				json.put(COL_NAME_STORE_ID, front.getStore().getUuid());
				jsons.put(json);
			}
		} catch (Exception e) {
			Logger.error(PublishDiscountMethod.class.getSimpleName(), e.getMessage(),
					e);
			throw new RuntimeException(e);
		}
		return jsons.toString();
	}
}

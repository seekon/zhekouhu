package com.seekon.zhekouhu.func.discount;

import java.util.Comparator;

import com.seekon.zhekouhu.func.store.StorefrontEntity;

public class StorefrontDistanceComparator implements
		Comparator<StorefrontEntity> {

	@Override
	public int compare(StorefrontEntity lhs, StorefrontEntity rhs) {
		return (int) (lhs.getDistance() - rhs.getDistance());
	}

}

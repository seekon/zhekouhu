package com.seekon.zhekouhu.func.discount;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_IMG;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DEL_IMAGES;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DEL_STOREFRONTS;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STOREFRONTS;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.MultipartRequest;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class UpdateDiscountMethod extends PublishDiscountMethod {

	private static final URI UPDATE_DISCOUNT_URI = URI
			.create(Const.SERVER_APP_URL + "/updateDiscount");

	private List<FileEntity> addedImages;
	private List<FileEntity> deletedImages;
	private List<StorefrontEntity> addedStorefronts;
	private List<StorefrontEntity> deletedStorefronts;

	public UpdateDiscountMethod(DiscountEntity discount,
			List<FileEntity> addedImages, List<FileEntity> deletedImages,
			List<StorefrontEntity> addedStorefronts,
			List<StorefrontEntity> deletedStorefronts) {
		super(discount);
		this.addedImages = addedImages;
		this.deletedImages = deletedImages;
		this.addedStorefronts = addedStorefronts;
		this.deletedStorefronts = deletedStorefronts;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = buildRequestParams();
		params.put(COL_NAME_UUID, discount.getUuid());
		params.put(NAME_STOREFRONTS, getAddedStorefrontString());
		params.put(NAME_DEL_STOREFRONTS, getDeletedStorefrontString());
		params.put(NAME_DEL_IMAGES, getDeletedImageString());

		return new MultipartRequest(Method.PUT, UPDATE_DISCOUNT_URI, null, params,
				addedImages);
	}

	private String getDeletedImageString() {
		JSONArray jsons = new JSONArray();
		try {
			for (FileEntity image : deletedImages) {
				JSONObject json = new JSONObject();
				json.put(COL_NAME_IMG, image.getAliasName());
				jsons.put(json);
			}
		} catch (Exception e) {
			Logger.error(UpdateDiscountMethod.class.getSimpleName(), e.getMessage(),
					e);
			throw new RuntimeException(e);
		}
		return jsons.toString();
	}

	private String getDeletedStorefrontString() {
		JSONArray jsons = new JSONArray();
		try {
			for (StorefrontEntity front : deletedStorefronts) {
				JSONObject json = new JSONObject();
				json.put(COL_NAME_UUID, front.getUuid());
				jsons.put(json);
			}
		} catch (Exception e) {
			Logger.error(UpdateDiscountMethod.class.getSimpleName(), e.getMessage(),
					e);
			throw new RuntimeException(e);
		}
		return jsons.toString();
	}

	private String getAddedStorefrontString() {
		return getStorefrontsJSONString(addedStorefronts);
	}
}

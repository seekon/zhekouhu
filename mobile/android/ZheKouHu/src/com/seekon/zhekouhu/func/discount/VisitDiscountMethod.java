package com.seekon.zhekouhu.func.discount;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class VisitDiscountMethod extends JSONObjResourceMethod {

	private static final URI VISIT_DISCOUNT_URI = URI.create(Const.SERVER_APP_URL
			+ "/visitDiscount");

	private DiscountEntity discount;

	public VisitDiscountMethod(DiscountEntity discount) {
		super();
		this.discount = discount;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_DISCOUNT_ID, discount.getUuid());

		return new BaseRequest(Method.POST, VISIT_DISCOUNT_URI, null, params);
	}

}

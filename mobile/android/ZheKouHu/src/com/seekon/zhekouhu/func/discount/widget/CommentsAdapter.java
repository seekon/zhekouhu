package com.seekon.zhekouhu.func.discount.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.discount.CommentEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.DateUtils;

public class CommentsAdapter extends AbstractListAdapter<CommentEntity> {

	public CommentsAdapter(Context context, List<CommentEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.discount_comment_item, null);

			holder = new ViewHolder();
			holder.conentView = (TextView) convertView
					.findViewById(R.id.t_commont_content);
			holder.publisherView = (TextView) convertView
					.findViewById(R.id.t_publisher);
			holder.publishDateView = (TextView) convertView
					.findViewById(R.id.t_publish_date);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CommentEntity comment = (CommentEntity) getItem(position);
		holder.conentView.setText(comment.getContent());
		String publisher = comment.getPublisher();
		publisher = publisher.substring(0, 2) + "......"
				+ publisher.substring(publisher.length() - 1);
		holder.publisherView.setText(publisher);
		holder.publishDateView.setText(DateUtils.getDateString_yyyyMMdd(Long
				.valueOf(comment.getPublishTime())));

		return convertView;
	}

	class ViewHolder {
		TextView conentView;
		TextView publisherView;
		TextView publishDateView;
	}
}

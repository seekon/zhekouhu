package com.seekon.zhekouhu.func.discount.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.coupon.StoreCouponsActivity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.store.CouponStoreEntity;

public class CouponMainAdapter extends BaseAdapter {

	private Context context;
	private Map<CategoryEntity, List<CouponStoreEntity>> dataMap;
	private List<CategoryEntity> keys = new ArrayList<CategoryEntity>();

	public CouponMainAdapter(Context context,
			Map<CategoryEntity, List<CouponStoreEntity>> dataMap) {
		super();
		this.context = context;
		this.dataMap = dataMap;
		if (this.dataMap != null) {
			keys.addAll(dataMap.keySet());
		}
	}

	public void updateData(Map<CategoryEntity, List<CouponStoreEntity>> dataMap) {
		this.dataMap = dataMap;
		keys.clear();
		if (this.dataMap != null) {
			keys.addAll(dataMap.keySet());
		}
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return dataMap == null ? 0 : dataMap.size();
	}

	@Override
	public Object getItem(int position) {
		CategoryEntity category = keys.get(position);
		return dataMap.get(category);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.coupon_main_item, null);

			holder = new ViewHolder();
			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);
			holder.storesView = (GridView) convertView.findViewById(R.id.gridView);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CategoryEntity category = keys.get(position);
		holder.nameView.setText(category.getName());

		final List<CouponStoreEntity> storeList = dataMap.get(category);
		holder.storesView.setAdapter(new CouponStoresAdapter(context, storeList));
		holder.storesView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				Animation animation = (AnimationSet) AnimationUtils.loadAnimation(
						context, R.anim.image_scale);
				view.setAnimation(animation);
				animation.startNow();

				animation.setAnimationListener(new AnimationListener() {

					@Override
					public void onAnimationStart(Animation animation) {
					}

					@Override
					public void onAnimationRepeat(Animation animation) {
					}

					@Override
					public void onAnimationEnd(Animation animation) {
						CouponStoreEntity store = storeList.get(position);
						Intent intent = new Intent(context, StoreCouponsActivity.class);
						if (RunEnv.getInstance().getLocation() != null) {
							intent.putExtra(FuncConst.KEY_LOCATION, RunEnv.getInstance()
									.getLocation());
						}
						intent.putExtra(FuncConst.NAME_STORE, store);
						context.startActivity(intent);
					}
				});
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView nameView;
		GridView storesView;
	}
}

package com.seekon.zhekouhu.func.discount.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.store.CouponStoreEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;

public class CouponStoresAdapter extends AbstractListAdapter<CouponStoreEntity> {

	public CouponStoresAdapter(Context context, List<CouponStoreEntity> itemList) {
		super(context, itemList);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.coupon_store_item, null);

			holder = new ViewHolder();

			holder.imgView = (ImageView) convertView.findViewById(R.id.img_logo);
			holder.imgView.setAdjustViewBounds(false);
			holder.imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);

			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CouponStoreEntity store = (CouponStoreEntity) getItem(position);
		holder.nameView.setText(store.getName());

		FileEntity logoFile = store.getLogo();
		ImageLoader.getInstance().displayImage(logoFile.getAliasName(),
				holder.imgView, true);

		return convertView;
	}

	class ViewHolder {
		ImageView imgView;
		TextView nameView;
	}

}

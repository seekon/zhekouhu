package com.seekon.zhekouhu.func.discount.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;

public class DiscountImagesAdapter extends AbstractListAdapter<FileEntity> {

	public DiscountImagesAdapter(Context context, List<FileEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public int getCount() {
		return super.getCount() + 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.discount_image_item, null);
			holder.imgView = (ImageView) convertView.findViewById(R.id.img_discount);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (position == getCount() - 1) {
			holder.imgView.setImageResource(R.drawable.add_image);
		} else {
			FileEntity file = (FileEntity) getItem(position);
			if (file.getFileUri() != null) {
				holder.imgView.setImageBitmap(FileHelper.decodeFile(file.getFileUri(),
						true, holder.imgView.getLayoutParams().width,
						holder.imgView.getLayoutParams().height));
			} else {
				ImageLoader.getInstance().displayImage(file.getAliasName(),
						holder.imgView, true);
			}

		}
		return convertView;
	}

	class ViewHolder {
		ImageView imgView;
	}
}

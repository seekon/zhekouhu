package com.seekon.zhekouhu.func.discount.widget;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.discount.CategoryDiscountsActivity;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.category.widget.CategoryListAdapter;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.LocationUtils;

public class DiscountMainListAdapter extends AbstractListAdapter {

	private boolean showStoreName = true;

	public DiscountMainListAdapter(Context context, List itemList,
			boolean showStoreName) {
		super(context, itemList);
		this.showStoreName = showStoreName;
	}

	public DiscountMainListAdapter(Context context, List itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Object item = itemList.get(position);
		if (item instanceof List<?>) {// 分类列表
			View view = LayoutInflater.from(context).inflate(
					R.layout.category_grid_view, null);

			GridView mGridView = (GridView) view.findViewById(R.id.gridView);

			int columnWidth = (context.getResources().getDisplayMetrics().widthPixels - 72 * 2) / 3;
			mGridView.setColumnWidth(columnWidth);
			mGridView.setNumColumns(3);
			mGridView.setAdapter(new CategoryListAdapter(context,
					(List<CategoryEntity>) item));
			mGridView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						final int position, long id) {
					// View imageView = view.findViewById(R.id.img_category);
					Animation animation = (AnimationSet) AnimationUtils.loadAnimation(
							context, R.anim.image_scale);
					view.setAnimation(animation);
					animation.startNow();

					animation.setAnimationListener(new AnimationListener() {

						@Override
						public void onAnimationStart(Animation animation) {
						}

						@Override
						public void onAnimationRepeat(Animation animation) {
						}

						@Override
						public void onAnimationEnd(Animation animation) {
							CategoryEntity category = ((List<CategoryEntity>) item)
									.get(position);
							Intent intent = new Intent(context,
									CategoryDiscountsActivity.class);
							if (RunEnv.getInstance().getLocation() != null) {
								intent.putExtra(FuncConst.KEY_LOCATION, RunEnv.getInstance()
										.getLocation());
							}
							intent.putExtra(FuncConst.NAME_CATEGORY, category);
							context.startActivity(intent);
						}
					});

				}
			});

			return view;
		} else if (item instanceof String) {// lable文字
			View view = LayoutInflater.from(context).inflate(
					R.layout.discount_hot_label_item, null);
			TextView textView = (TextView) view.findViewById(R.id.t_name);
			textView.setText(item.toString());
			return view;
		} else {
			ViewHolder holder = null;
			if (convertView != null) {
				holder = (ViewHolder) convertView.getTag();
			}

			if (holder == null) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.discount_main_item, null);

				holder = new ViewHolder();

				holder.logoView = (ImageView) convertView.findViewById(R.id.img_logo);
				holder.logoView.setAdjustViewBounds(false);
				holder.logoView.setScaleType(ImageView.ScaleType.CENTER_CROP);

				holder.contentView = (TextView) convertView
						.findViewById(R.id.t_content);
				holder.distanceView = (TextView) convertView
						.findViewById(R.id.t_distance);
				holder.storeView = (TextView) convertView.findViewById(R.id.t_store);
				holder.palValView = (TextView) convertView
						.findViewById(R.id.t_pal_value);
				holder.originView = (TextView) convertView.findViewById(R.id.t_origin);
				
				convertView.setTag(holder);
			}

			DiscountEntity discount = (DiscountEntity) item;
			FileEntity imageFile = discount.getImages().get(0);
			ImageLoader.getInstance().displayImage(imageFile.getAliasName(),
					holder.logoView, true);

			if (discount.getStorefronts() != null
					&& discount.getStorefronts().size() > 0) {
				StorefrontEntity front = discount.getStorefronts().get(0);

				if (showStoreName) {
					holder.storeView.setText(front.getStore().getName() + "-"
							+ front.getName());
				} else {
					holder.storeView.setText(front.getName());
				}
				holder.distanceView.setText(LocationUtils.formatDistance(front
						.getDistance() / 1000) + "km");
			}
			
			holder.contentView.setText(discount.getContent());
			
			if (discount.getType().equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
				holder.palValView.setVisibility(View.GONE);
			} else {
				holder.palValView.setVisibility(View.VISIBLE);
				holder.palValView.setText("￥" + discount.getParValue());
			}
			
			String origin = discount.getOrigin();
			if (origin.equals(FuncConst.VAL_DISCOUNT_ORIGIN_CUSTOMER)) {
				holder.originView.setText(R.string.label_origin_customer);
			}else{
				holder.originView.setText(R.string.label_origin_storer);
			}
		}
		return convertView;
	}

	class ViewHolder {
		ImageView logoView;
		TextView contentView;
		TextView storeView;
		TextView palValView;
		TextView distanceView;
		TextView originView;
	}
}

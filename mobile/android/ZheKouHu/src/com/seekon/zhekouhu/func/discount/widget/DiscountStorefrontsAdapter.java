package com.seekon.zhekouhu.func.discount.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.LocationUtils;

public class DiscountStorefrontsAdapter extends
		AbstractListAdapter<StorefrontEntity> {

	public DiscountStorefrontsAdapter(Context context,
			List<StorefrontEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.discount_storefront_item, null);

			holder = new ViewHolder();
			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);
			holder.distanceView = (TextView) convertView
					.findViewById(R.id.t_distance);
			holder.addrView = (TextView) convertView.findViewById(R.id.t_addr);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		StorefrontEntity front = (StorefrontEntity) getItem(position);
		holder.nameView.setText(front.getStore().getName() + "-" + front.getName());
		holder.addrView.setText(front.getCity().getName() + front.getAddr());
		holder.distanceView.setText(LocationUtils.formatDistance(front
				.getDistance() / 1000) + "km");

		return convertView;
	}

	class ViewHolder {
		TextView nameView;
		TextView distanceView;
		TextView addrView;
	}

}

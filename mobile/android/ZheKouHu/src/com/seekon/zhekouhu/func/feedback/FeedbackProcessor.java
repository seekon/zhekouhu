package com.seekon.zhekouhu.func.feedback;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.spi.IFeedbackProcessor;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;

public class FeedbackProcessor implements IFeedbackProcessor {

	@Override
	public ProcessResult<Boolean> submit(String content, String contact) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(DataConst.COL_NAME_UUID)) {
					result.setSuccess(true);
					result.setSuccessObj(Boolean.TRUE);
				} else {
					result = new ProcessResult<Boolean>(null, "提交失败.");
				}
			}
		};
		return executer.execute(new SubmitFeedbackMethod(content, contact));
	}

}

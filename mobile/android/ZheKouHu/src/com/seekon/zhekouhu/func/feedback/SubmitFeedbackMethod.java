package com.seekon.zhekouhu.func.feedback;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class SubmitFeedbackMethod extends JSONObjResourceMethod {

	private static final URI SUBMIT_FEEDBACK_URI = URI
			.create(Const.SERVER_APP_URL + "/submitFeedback");

	private String content;
	private String contact;

	public SubmitFeedbackMethod(String content, String contact) {
		super();
		this.content = content;
		this.contact = contact;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(FuncConst.NAME_CONTENT, content);
		params.put(FuncConst.NAME_CONTACT, contact);
		return new BaseRequest(Method.POST, SUBMIT_FEEDBACK_URI, null, params);
	}

}

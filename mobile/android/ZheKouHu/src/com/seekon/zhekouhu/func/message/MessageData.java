package com.seekon.zhekouhu.func.message;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.func.FuncConst;

public class MessageData extends AbstractSQLiteData<MessageEntity> implements
		DataUpdater<MessageEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_MESSAGE
			+ "(uuid, msg_id, content, type, send_time) values (?, ?,  ?, ?, ?) ";

	private static final String BASE_QUERY_SQL = " select uuid, msg_id, content, type, send_time from "
			+ DataConst.TABLE_MESSAGE;

	private static final String BASE_DEL_SQL = " update "
			+ DataConst.TABLE_MESSAGE + " set is_deleted = '1' ";

	private static final String COUNT_SQL = " select count(1) from "
			+ DataConst.TABLE_MESSAGE + " where uuid = ?";

	@Override
	public int save(MessageEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (MessageEntity message : data) {
			executeUpdate(
					UPDATE_SQL,
					new Object[] { message.getUuid(), message.getMsgId(),
							message.getContent(), message.getType(), message.getSendTime() });
		}
		return data.length;
	}

	@Override
	protected MessageEntity processRow(Cursor cursor) {
		MessageEntity message = new MessageEntity();

		int i = 0;
		message.setUuid(cursor.getString(i++));
		message.setMsgId(cursor.getString(i++));
		message.setContent(cursor.getString(i++));
		message.setType(cursor.getString(i++));
		message.setSendTime(cursor.getLong(i++));

		return message;
	}

	public MessageEntity messageById(String uuid) {
		return queryOne(BASE_QUERY_SQL + " where uuid = ? ", new String[] { uuid });
	}

	public boolean messageExists(String uuid) {
		return queryCount(COUNT_SQL, new String[] { uuid }) > 0;
	}

	public void deleteMessages() {
		executeUpdate(BASE_DEL_SQL, null);
	}

	public void deleteMessage(String uuid) {
		executeUpdate(BASE_DEL_SQL + " where uuid = ? ", new String[] { uuid });
	}

	public List<MessageEntity> messages(int offset) {
		StringBuffer sql = new StringBuffer(BASE_QUERY_SQL);
		sql.append(" where type != ? and is_deleted == 0 ");
		sql.append(" order by send_time desc ");
		sql.append(" limit " + Const.PAGE_SIZE + " offset " + offset);

		return query(sql.toString(), new String[] { FuncConst.MSG_TYPE_VERSION });
	}
}

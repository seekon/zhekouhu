package com.seekon.zhekouhu.func.message;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.utils.Logger;

public class MessageEntity extends Entity {

	private static final long serialVersionUID = 707757356348563296L;
	private String type;
	private String content;
	private long sendTime;
	private String msgId;

	public MessageEntity() {
		super();
	}

	public MessageEntity(JSONObject json) {
		this();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			msgId = json.getString(DataConst.COL_NAME_MSG_ID);
			content = json.getString(DataConst.COL_NAME_CONTENT);
			type = json.getString(DataConst.COL_NAME_TYPE);
			sendTime = json.getLong(DataConst.COL_NAME_SEND_TIME);
		} catch (Exception e) {
			Logger.error(MessageEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getSendTime() {
		return sendTime;
	}

	public void setSendTime(long sendTime) {
		this.sendTime = sendTime;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

}

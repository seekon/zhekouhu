package com.seekon.zhekouhu.func.message;

import java.util.List;

import com.seekon.zhekouhu.func.spi.IMessageProcessor;

public class MessageProcessor implements IMessageProcessor {

	private MessageData messageData;

	public MessageProcessor() {
		super();
		messageData = new MessageData();
	}

	@Override
	public boolean messageExists(String uuid) {
		return messageData.messageExists(uuid);
	}

	@Override
	public void saveMessage(MessageEntity message) {
		messageData.save(new MessageEntity[] { message });
	}

	@Override
	public List<MessageEntity> messages(int offset) {
		return messageData.messages(offset);
	}

	@Override
	public MessageEntity message(String uuid) {
		return messageData.messageById(uuid);
	}

	@Override
	public void clean() {
		messageData.deleteMessages();
	}
}

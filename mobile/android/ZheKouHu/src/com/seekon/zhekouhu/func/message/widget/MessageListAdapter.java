package com.seekon.zhekouhu.func.message.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.message.MessageEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.DateUtils;

public class MessageListAdapter extends AbstractListAdapter<MessageEntity> {

	public MessageListAdapter(Context context, List<MessageEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.message_list_item, null);

			holder = new ViewHolder();
			holder.contentView = (TextView) convertView.findViewById(R.id.t_content);
			holder.timeView = (TextView) convertView.findViewById(R.id.t_send_time);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		MessageEntity message = (MessageEntity) getItem(position);
		holder.contentView.setText(message.getContent());
		holder.timeView.setText(DateUtils.getDateString_yyyyMMdd(message
				.getSendTime()));

		return convertView;
	}

	class ViewHolder {
		TextView contentView;
		TextView timeView;
	}
}

package com.seekon.zhekouhu.func.more.widget;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.setting.SettingEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.RUtils;

public class ShareSettingAdapter extends AbstractListAdapter<SettingEntity> {

	public ShareSettingAdapter(Context context, List<SettingEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.share_setting_item, null);

			holder = new ViewHolder();
			holder.logoView = (ImageView) convertView.findViewById(R.id.img_logo);
			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);
			holder.statusView = (TextView) convertView.findViewById(R.id.t_status);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		SettingEntity setting = (SettingEntity) getItem(position);
		try {
			JSONObject value = new JSONObject(setting.getValue());
			holder.logoView.setImageResource(RUtils.getDrawableImg(value
					.getString(DataConst.COL_NAME_IMG)));
			holder.nameView.setText(value.getString(DataConst.COL_NAME_NAME));
			String status = value.getString(DataConst.COL_NAME_STATUS);
			if (status.equals("0")) {
				holder.statusView.setText("未绑定");
			} else {
				holder.statusView.setText("已绑定");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return convertView;
	}

	class ViewHolder {
		ImageView logoView;
		TextView nameView;
		TextView statusView;
	}
}

package com.seekon.zhekouhu.func.setting;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class RegisterDeviceMethod extends JSONObjResourceMethod {

	private static final URI REGISTER_DEVICE_URI = URI
			.create(Const.SERVER_APP_URL + "/registerDevice");

	private String deviceId;

	public RegisterDeviceMethod(String deviceId) {
		super();
		this.deviceId = deviceId;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(FuncConst.NAME_DEVICE_ID, deviceId);
		params.put(DataConst.COL_NAME_TYPE, "0");

		return new BaseRequest(Method.POST, REGISTER_DEVICE_URI, null, params);
	}

}

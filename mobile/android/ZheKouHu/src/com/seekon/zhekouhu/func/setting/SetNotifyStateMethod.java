package com.seekon.zhekouhu.func.setting;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class SetNotifyStateMethod extends JSONObjResourceMethod {

	private static final URI SET_UN_NOTOFY_URI = URI.create(Const.SERVER_APP_URL
			+ "/setNotifyState");

	private String deviceId;

	private String status;

	public SetNotifyStateMethod(String deviceId, String status) {
		super();
		this.deviceId = deviceId;
		this.status = status;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(FuncConst.NAME_DEVICE_ID, deviceId);
		params.put(DataConst.COL_NAME_STATUS, status);
		return new BaseRequest(Method.PUT, SET_UN_NOTOFY_URI, null, params);
	}

}

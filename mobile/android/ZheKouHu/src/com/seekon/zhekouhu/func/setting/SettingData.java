package com.seekon.zhekouhu.func.setting;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;

public class SettingData extends AbstractSQLiteData<SettingEntity> implements
		DataUpdater<SettingEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_SETTING + "(code, value) values (?, ?) ";

	private static final String QUERY_BASE_SQL = " select code, value from "
			+ DataConst.TABLE_SETTING;

	@Override
	public int save(SettingEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (SettingEntity setting : data) {
			executeUpdate(UPDATE_SQL,
					new Object[] { setting.getUuid(), setting.getValue() });
		}

		return data.length;
	}

	public SettingEntity settingByCode(String code) {
		return this.queryOne(QUERY_BASE_SQL + " where code = ?",
				new String[] { code });
	}

	@Override
	protected SettingEntity processRow(Cursor cursor) {
		int i = 0;
		String uuid = cursor.getString(i++);
		String value = cursor.getString(i++);
		SettingEntity setting = new SettingEntity(uuid, value);
		return setting;
	}

	public List<SettingEntity> getShareSettingList() {
		return this.query(QUERY_BASE_SQL + " where code like 'share_%' ", null);
	}
}

package com.seekon.zhekouhu.func.setting;

import com.seekon.zhekouhu.func.Entity;

public class SettingEntity extends Entity {

	private static final long serialVersionUID = 2568289161282386890L;

	private String value;

	public SettingEntity(String uuid, String value) {
		super(uuid);
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}

package com.seekon.zhekouhu.func.setting;

import java.util.List;

import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.spi.ISettingProcessor;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;

public class SettingProcessor implements ISettingProcessor {

	private SettingData settingData;

	public SettingProcessor() {
		super();
		this.settingData = new SettingData();
	}

	@Override
	public void saveSetting(SettingEntity setting) {
		settingData.save(new SettingEntity[] { setting });
	}

	@Override
	public boolean isStoreRegistered() {
		SettingEntity setting = this.settingData
				.settingByCode(FuncConst.KEY_SETTING_STORE_REGISTERED);
		return setting != null && setting.getValue() != null
				&& setting.getValue().equals("1");
	}

	@Override
	public SettingEntity getSetting(String code) {
		return settingData.settingByCode(code);
	}

	@Override
	public List<SettingEntity> getShareSettingList() {
		return settingData.getShareSettingList();
	}

	@Override
	public ProcessResult<Boolean> registerDevice(String deviceId) {

		SettingEntity messageSetting = this
				.getSetting(FuncConst.KEY_SETTING_MESSAGE_REMIND);
		if (messageSetting != null && messageSetting.getValue().equals("1")) {
			return new ProcessResult<Boolean>(Boolean.TRUE);
		}

		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(FuncConst.NAME_DEVICE_ID)) {
					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new RegisterDeviceMethod(deviceId));
	}

	@Override
	public ProcessResult<Boolean> setNotifyState(String deviceId, String status) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(FuncConst.NAME_DEVICE_ID)) {
					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new SetNotifyStateMethod(deviceId, status));
	}

}

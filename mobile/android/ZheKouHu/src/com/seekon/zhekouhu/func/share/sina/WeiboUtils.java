package com.seekon.zhekouhu.func.share.sina;

import android.content.Context;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.sina.weibo.sdk.api.TextObject;
import com.sina.weibo.sdk.api.WebpageObject;
import com.sina.weibo.sdk.api.WeiboMessage;
import com.sina.weibo.sdk.api.WeiboMultiMessage;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.utils.Utility;

public class WeiboUtils {

	public static void shareDiscount(Context context, DiscountEntity discount) {
		IWeiboShareAPI mWeiboShareAPI = WeiboShareSDK.createWeiboAPI(context,
				WBConst.APP_KEY);
		mWeiboShareAPI.registerApp();
		if (mWeiboShareAPI.isWeiboAppInstalled()) {
			if (mWeiboShareAPI.isWeiboAppSupportAPI()) {
				int supportApi = mWeiboShareAPI.getWeiboAppSupportAPI();
				if (supportApi >= 10351) {
					shareMultiMessage(context, discount, mWeiboShareAPI);
				} else {
					shareSingleMessage(context, discount, mWeiboShareAPI);
				}
			} else {
				ViewUtils.showToast(context
						.getString(R.string.weibosdk_not_support_api_hint));
			}
		}
	}

	private static void shareMultiMessage(Context context,
			DiscountEntity discount, IWeiboShareAPI mWeiboShareAPI) {
		WeiboMultiMessage weiboMessage = new WeiboMultiMessage();

		WebpageObject webpageObject = getWebpageObj(context, discount);
		weiboMessage.mediaObject = webpageObject;
		weiboMessage.textObject = new TextObject();
		weiboMessage.textObject.text = "【" + webpageObject.defaultText + "】"
				+ discount.getContent();

		SendMultiMessageToWeiboRequest request = new SendMultiMessageToWeiboRequest();
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.multiMessage = weiboMessage;

		mWeiboShareAPI.sendRequest(request);
	}

	private static void shareSingleMessage(Context context,
			DiscountEntity discount, IWeiboShareAPI mWeiboShareAPI) {
		WeiboMessage weiboMessage = new WeiboMessage();
		weiboMessage.mediaObject = getWebpageObj(context, discount);

		SendMessageToWeiboRequest request = new SendMessageToWeiboRequest();
		request.transaction = String.valueOf(System.currentTimeMillis());
		request.message = weiboMessage;

		mWeiboShareAPI.sendRequest(request);
	}

	/**
	 * 创建多媒体（网页）消息对象。
	 * 
	 * @return 多媒体（网页）消息对象。
	 */
	private static WebpageObject getWebpageObj(Context context,
			DiscountEntity discount) {
		WebpageObject mediaObject = new WebpageObject();
		mediaObject.identify = Utility.generateGUID();

		StorefrontEntity front = discount.getStorefronts().get(0);
		mediaObject.title = front.getStore().getName();
		mediaObject.defaultText = "分享";

		if (discount.getType().equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
			mediaObject.title += "-促销活动";
			mediaObject.defaultText += "促销活动";
		} else {
			mediaObject.title += "-优惠券";
			mediaObject.defaultText += "优惠券";
		}

		mediaObject.description = discount.getContent();

		// 设置 Bitmap 类型的图片到视频对象里
		// mediaObject.setThumbImage(BitmapFactory.decodeResource(
		// context.getResources(), R.drawable.ic_launcher));
		String url = FileHelper.IMAGE_FILE_GET_URL
				+ discount.getImages().get(0).getAliasName();
		mediaObject.setThumbImage(FileHelper.decodeFile(
				FileHelper.getFileFromCacheByUrl(url), true, 60, 60));
		mediaObject.actionUrl = Const.SERVER_APP_URL + "/discount/"
				+ discount.getUuid();

		return mediaObject;
	}
}

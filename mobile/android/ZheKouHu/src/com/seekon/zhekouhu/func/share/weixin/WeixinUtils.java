package com.seekon.zhekouhu.func.share.weixin;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.file.FileHelper;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;

public class WeixinUtils {

	public static void shareDiscountToWeixin(Context context,
			DiscountEntity discount) {
		WeixinUtils.shareDiscount(context, discount,
				SendMessageToWX.Req.WXSceneSession);
	}

	public static void shareDiscountToFriends(Context context,
			DiscountEntity discount) {
		WeixinUtils.shareDiscount(context, discount,
				SendMessageToWX.Req.WXSceneTimeline);
	}

	private static void shareDiscount(Context context, DiscountEntity discount,
			int scene) {
		IWXAPI wxApi = WXAPIFactory.createWXAPI(context, WXConst.APP_ID);
		wxApi.registerApp(WXConst.APP_ID);
		try {
			WXMediaMessage msg = new WXMediaMessage();

			StorefrontEntity front = discount.getStorefronts().get(0);
			msg.title = front.getStore().getName();
			if (discount.getType().equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
				msg.title += "-促销活动";
			} else {
				msg.title += "-优惠券";
			}
			msg.description = discount.getContent();

			// Bitmap thumbBmp = BitmapFactory.decodeResource(
			// context.getResources(), R.drawable.ic_launcher);
			String url = FileHelper.IMAGE_FILE_GET_URL
					+ discount.getImages().get(0).getAliasName();
			Bitmap thumbBmp = FileHelper.decodeFile(
					FileHelper.getFileFromCacheByUrl(url), true, 60, 60);

			msg.thumbData = WeixinUtils.bmpToByteArray(thumbBmp, true);

			WXWebpageObject webObj = new WXWebpageObject();
			webObj.webpageUrl = Const.SERVER_APP_URL + "/discount/"
					+ discount.getUuid();
			msg.mediaObject = webObj;

			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = WeixinUtils.buildTransaction("img");
			req.message = msg;
			req.scene = scene;
			wxApi.sendReq(req);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] bmpToByteArray(final Bitmap bmp,
			final boolean needRecycle) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		bmp.compress(CompressFormat.PNG, 100, output);
		if (needRecycle) {
			bmp.recycle();
		}

		byte[] result = output.toByteArray();
		try {
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	private static String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis()) : type
				+ System.currentTimeMillis();
	}
}

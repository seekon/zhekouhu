package com.seekon.zhekouhu.func.spi;

import java.util.List;

import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.award.GoodsEntity;

public interface IAwardProcessor {

	public ProcessResult<List<GoodsEntity>> getGoodsList();
	
}

package com.seekon.zhekouhu.func.spi;

import java.util.List;

import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.category.CategoryEntity;

public interface ICategoryProcessor {

	public ProcessResult<List<CategoryEntity>> categories();

}

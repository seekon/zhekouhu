package com.seekon.zhekouhu.func.spi;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.DateIndexedEntity;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.discount.CommentEntity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.store.CouponStoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.user.FavoritEntity;

public interface IDiscountProcessor {

	/**
	 * 从远程服务刷新活动数据：店铺使用
	 * 
	 * @param owner
	 * @return
	 */
	public ProcessResult<Boolean> discountsRemoteRefresh(String owner, String type);

	/**
	 * 按时间分组获取店铺列表：店铺使用
	 * @deprecated v1.1
	 * @param owner
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<DateIndexedEntity>> activitiesGroupByPublishDate(
			String owner, int offset);

	/**
	 * 按时间分组获取店铺列表：店铺使用
	 * @deprecated v1.1
	 * @param owner
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<DateIndexedEntity>> couponsGroupByPublishDate(
			String owner, int offset);

	/**
	 * 获取店主所有发布的折扣活动数目：店铺使用
	 * @deprecated v1.1
	 * @param owner
	 * @return
	 */
	public int activityCountForOwner(String owner);

	/**
	 * 获取店主所发布的优惠券数目：店铺使用
	 * @deprecated v1.1
	 * @param owner
	 * @return
	 */
	public int couponCountForOwner(String owner);

	/**
	 * v1.1 获取个人所发的活动列表（含：优惠券）
	 * @param owner
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<DateIndexedEntity>> discountsGroupByPublishDate(
			String owner, int offset);
	
	/**
	 * v1.1 获取个人所发的活动数（含：优惠券）
	 * @param owner
	 * @return
	 */
	public int discountCountForOwner(String owner);

	/**
	 * 删除活动记录：店铺使用
	 * 
	 * @param discount
	 * @return
	 */
	public ProcessResult<Boolean> deleteDiscount(DiscountEntity discount);

	/**
	 * 发布活动：店铺使用
	 * 
	 * @param discount
	 * @return
	 */
	public ProcessResult<Boolean> publishDiscount(DiscountEntity discount);

	/**
	 * 编辑更新活动：店铺使用
	 * 
	 * @param discount
	 * @param deletedImages
	 * @param addedImages
	 * @return
	 */
	public ProcessResult<Boolean> updateDiscount(DiscountEntity discount,
			List<FileEntity> deletedImages, List<FileEntity> addedImages);

	/**
	 * 获取最新的活动列表
	 * 
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<DiscountEntity>> newestActivities(int offset);

	/**
	 * 从本地获取最新的活动列表
	 * 
	 * @deprecated
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<DiscountEntity>> newestActivitiesFromLocal(
			int offset);

	/**
	 * 按分类获取折扣活动数据
	 * 
	 * @param category
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<DiscountEntity>> activitiesForCategory(
			CategoryEntity category, int offset);

	/**
	 * 远程刷新店铺的优惠券列表
	 * 
	 * @param storeId
	 * @return
	 */
	public ProcessResult<List<DiscountEntity>> couponsForStoreRemoteRefresh(
			String storeId, String cateId, int offset);

	/**
	 * 根据storeId从本地获取优惠券列表
	 * 
	 * @deprecated
	 * @param storeId
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<DiscountEntity>> couponsForStore(String storeId,
			String cateId, int offset);

	/**
	 * 浏览活动记录
	 * 
	 * @param discount
	 * @return
	 */
	public ProcessResult<Boolean> visitDiscount(DiscountEntity discount);

	/**
	 * 远程更新有优惠券的店铺列表
	 * 
	 * @return
	 */
	public ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>> storesHasCouponRemoteRefresh();

	/**
	 * 从本地获取存在优惠券的店铺列表
	 * 
	 * @return
	 */
	public ProcessResult<Map<CategoryEntity, List<CouponStoreEntity>>> storesHasCouponFromLocal();

	/**
	 * 根据关键字获取活动（含优惠券）列表
	 * 
	 * @param keyword
	 * @return
	 */
	public ProcessResult<List<DiscountEntity>> discountsByKeyword(String keyword,
			int offset);

	/**
	 * 根据主键id获取活动记录
	 * 
	 * @param uuid
	 * @return
	 */
	public ProcessResult<DiscountEntity> discountById(String uuid);

	/**
	 * 发布评论
	 * 
	 * @param comment
	 * @return
	 */
	public ProcessResult<CommentEntity> publishComment(CommentEntity comment);

	/**
	 * 收藏活动
	 * 
	 * @param discount
	 * @return
	 */
	public ProcessResult<Boolean> favoritDiscount(DiscountEntity discount);

	/**
	 * 收藏活动，在本地存储
	 * @param favorit
	 * @return
	 */
	public Boolean favoritDiscountLocal(FavoritEntity favorit) ;
	
	/**
	 * 取消收藏活动
	 * 
	 * @param discount
	 * @return
	 */
	public ProcessResult<Boolean> unfavoritDiscount(DiscountEntity discount);

	/**
	 * 获取活动的门店记录，按距离排序
	 * 
	 * @param discount
	 * @param location
	 * @param limit
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<StorefrontEntity>> getStoreFrontsByDiscount(
			DiscountEntity discount, int limit, int offset);

	/**
	 * 获取活动的评论列表，按时间倒序
	 * 
	 * @param discount
	 * @param limit
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<CommentEntity>> getCommentsByDiscount(
			DiscountEntity discount, int limit, int offset);
	
	
	/**
	 * 1.1版组装活动对象
	 * @param data
	 * @return
	 */
	public List<DiscountEntity> assembleDiscounts(JSONArray data) throws Exception;
	
	/**
	 * 1.1版组装活动对象
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public DiscountEntity assembleDiscount(JSONObject data) throws Exception;
	
	/**
	 * 获取用户所收藏的记录，仅从本地查询
	 * 
	 * @return
	 */
	public ProcessResult<List<DiscountEntity>> getUserFavorits(int offset);

	/**
	 * 远程刷新用户的收藏记录
	 * @param userId
	 * @return
	 */
	public ProcessResult<Boolean> refreshUserFavorits(String userId);
	
	/**
	 * 获取收藏活动的额外信息
	 * @param discount
	 * @return
	 */
	public ProcessResult<DiscountEntity> getDiscountExtWithFavorit(DiscountEntity discount);
	
	/**
	 * 检查是否用户收藏的活动
	 * @param userId
	 * @param discountId
	 * @return
	 */
	public boolean isUserFavorit(String discountId);
}

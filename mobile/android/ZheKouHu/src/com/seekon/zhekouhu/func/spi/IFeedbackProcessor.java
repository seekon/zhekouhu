package com.seekon.zhekouhu.func.spi;

import com.seekon.zhekouhu.func.ProcessResult;

public interface IFeedbackProcessor {

	public ProcessResult<Boolean> submit(String content, String contact);
}

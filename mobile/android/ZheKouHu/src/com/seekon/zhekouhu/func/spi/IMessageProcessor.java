package com.seekon.zhekouhu.func.spi;

import java.util.List;

import com.seekon.zhekouhu.func.message.MessageEntity;

public interface IMessageProcessor {

	public boolean messageExists(String uuid);

	public void saveMessage(MessageEntity message);

	public List<MessageEntity> messages(int offset);

	public MessageEntity message(String uuid);

	public void clean();
}

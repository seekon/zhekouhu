package com.seekon.zhekouhu.func.spi;

import java.util.List;

import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.setting.SettingEntity;

public interface ISettingProcessor {

	public void saveSetting(SettingEntity setting);

	public boolean isStoreRegistered();

	public SettingEntity getSetting(String code);

	public List<SettingEntity> getShareSettingList();

	public ProcessResult<Boolean> registerDevice(String deviceId);

	public ProcessResult<Boolean> setNotifyState(String deviceId, String status);
}

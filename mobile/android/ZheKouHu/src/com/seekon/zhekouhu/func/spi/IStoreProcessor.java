package com.seekon.zhekouhu.func.spi;

import java.util.List;

import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.store.CityEntity;
import com.seekon.zhekouhu.func.store.NearbyStoreEntity;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;

public interface IStoreProcessor {

	/**
	 * 远程获取店主的店铺列表
	 * 
	 * @param owner
	 * @return
	 */
	public ProcessResult<List<StoreEntity>> storesForOwner(String owner);

	/**
	 * 本地查询店主的店铺列表
	 * 
	 * @param owner
	 * @return
	 */
	public List<StoreEntity> storesForOwnerFromLocal(String owner);

	/**
	 * 注册店铺（新增店铺）
	 * 
	 * @param store
	 * @return
	 */
	public ProcessResult<Boolean> registerStore(StoreEntity store);

	/**
	 * 更新店铺
	 * 
	 * @param store
	 * @return TODO：仅传递更新数据
	 */
	public ProcessResult<Boolean> updateStore(StoreEntity store);

	/**
	 * 删除店铺
	 * 
	 * @param store
	 * @return
	 */
	public ProcessResult<Boolean> deleteStore(StoreEntity store);

	/**
	 * 新增门店
	 * 
	 * @param storefront
	 * @return
	 */
	public ProcessResult<Boolean> addStorefront(StorefrontEntity storefront);

	/**
	 * 删除门店
	 * 
	 * @param storefront
	 * @return
	 */
	public ProcessResult<Boolean> deleteStorefront(StorefrontEntity storefront);

	/**
	 * 修改更新门店
	 * 
	 * @param storefront
	 * @return
	 */
	public ProcessResult<Boolean> updateStorefront(StorefrontEntity storefront);

	/**
	 * 远程获取城市列表
	 * 
	 * @return
	 */
	public ProcessResult<List<CityEntity>> cities();

	/**
	 * 本地获取城市列表
	 * 
	 * @return
	 */
	public List<CityEntity> citiesFromLocal();

	/**
	 * 获取指定范围内的商家
	 * 
	 * @param query
	 * @param radius
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<NearbyStoreEntity>> getNearbyStores(String query,
			int radius, int offset);

}

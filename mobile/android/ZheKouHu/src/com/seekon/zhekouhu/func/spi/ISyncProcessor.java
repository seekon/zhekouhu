package com.seekon.zhekouhu.func.spi;

import com.seekon.zhekouhu.func.sync.SyncEntity;

public interface ISyncProcessor {

	/**
	 * 从本地获取活动的更新记录信息：店铺使用
	 * 
	 * @param type
	 * @return
	 */
	public SyncEntity discountSyncEntityByOwner(String owner, String type);

	/**
	 * 获取优惠券首页的更新时间
	 * 
	 * @return
	 */
	public SyncEntity couponMainSyncEntity();

	/**
	 * 获取店铺优惠券列表的更新时间
	 * 
	 * @param storeId
	 * @return
	 */
	public SyncEntity storeCouponSyncEntity(String storeId);

}

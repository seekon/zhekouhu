package com.seekon.zhekouhu.func.spi;

import java.util.List;

import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.user.UserAwardEntity;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.user.UserProfileEntity;

public interface IUserProcessor {

	/**
	 * 登录 首先进行本地登录验证，若失败进行远程登录验证
	 * 
	 * @param code
	 * @param pwd
	 * @return
	 */
	public ProcessResult<UserEntity> login(String code, String pwd);

	/**
	 * 远程登录
	 * 
	 * @param code
	 * @param pwd
	 * @return
	 */
	public ProcessResult<UserEntity> remoteLogin(String code, String pwd);

	/**
	 * 自动登录
	 * 
	 * @return
	 */
	public ProcessResult<UserEntity> autoLogin();

	/**
	 * 退出
	 * 
	 * @return
	 */
	public boolean logout();

	/**
	 * 注册账户
	 * 
	 * @param code
	 * @param pwd
	 * @return
	 */
	public ProcessResult<UserEntity> register(String code, String pwd);

	/**
	 * 修改密码
	 * 
	 * @param uuid
	 * @param pwd
	 * @param oldPwd
	 * @return
	 */
	public ProcessResult<Boolean> changePwd(String uuid, String pwd, String oldPwd);

	/**
	 * 重置密码
	 * 
	 * @param code
	 * @return
	 */
	public ProcessResult<Boolean> resetPwd(String code);

	/**
	 * 本地登录
	 * 
	 * @return
	 */
	public UserEntity localLogin();

	/**
	 * 获取用户的积分情况
	 * @param userId
	 * @return
	 */
	public ProcessResult<UserProfileEntity> getUserAward();
	
	/**
	 * 获取用户的积分明细
	 * @param offset
	 * @return
	 */
	public ProcessResult<List<UserAwardEntity>> getUserAwardDetail(int offset);
	
	/**
	 * 更新用户资料
	 * @param profile
	 * @return
	 */
	public ProcessResult<Boolean> updateUserProfile(UserProfileEntity profile);
}

package com.seekon.zhekouhu.func.spi;

import com.seekon.zhekouhu.func.ProcessResult;

public interface IVersionProcessor {

	public ProcessResult<String> getNewestVersion();

}

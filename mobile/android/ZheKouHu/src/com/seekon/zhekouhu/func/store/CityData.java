package com.seekon.zhekouhu.func.store;

import static com.seekon.zhekouhu.db.DataConst.TABLE_CITY;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataUpdater;

public class CityData extends AbstractSQLiteData<CityEntity> implements
		DataUpdater<CityEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ TABLE_CITY + " (uuid, name, first_letter ) " + " values (?, ?, ?)";
	
	private static final String QUERY_BASE_SQL = " select uuid, name, first_letter "
			+ " from " + TABLE_CITY;

	@Override
	public int save(CityEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}
		for (CityEntity city : data) {
			if (city.getUuid() != null && city.getName() != null
					&& city.getFirstLetter() != null) {
				executeUpdate(UPDATE_SQL, new Object[] { city.getUuid(),
						city.getName(), city.getFirstLetter() });
			}
		}
		return 0;
	}

	public int save(String uuid, String name){
		CityEntity city = queryOne(QUERY_BASE_SQL + " where uuid = ?", new String[]{uuid});
		if (city == null) {
			city = new CityEntity();
			city.setUuid(uuid);
		}
		city.setName(name);
		return save(new CityEntity[]{city});
	}
	
	@Override
	protected CityEntity processRow(Cursor cursor) {
		CityEntity city = new CityEntity();

		int i = 0;
		city.setUuid(cursor.getString(i++));
		city.setName(cursor.getString(i++));
		city.setFirstLetter(cursor.getString(i++));

		return city;
	}

	public List<CityEntity> cities() {
		String sql = QUERY_BASE_SQL + " order by first_letter ";
		return query(sql, null);
	}
}

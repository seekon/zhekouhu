package com.seekon.zhekouhu.func.store;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_FIRST_LETTER;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.utils.Logger;

public class CityEntity extends Entity {

	private static final long serialVersionUID = -605787194312874548L;

	private String name;
	private String firstLetter;

	public CityEntity() {
		super();
	}

	public CityEntity(JSONObject json) {
		this();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			name = json.getString(DataConst.COL_NAME_NAME);
			firstLetter = json.getString(COL_NAME_FIRST_LETTER);
		} catch (Exception e) {
			Logger.error(CityEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstLetter() {
		return firstLetter;
	}

	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

}

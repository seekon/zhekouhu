package com.seekon.zhekouhu.func.store;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.file.FileEntity;

public class CouponStoreData extends AbstractSQLiteData<CouponStoreEntity>
		implements DataUpdater<CouponStoreEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_COUPON_STORE
			+ " (uuid, store_id, name, logo, city_name, category_id ) "
			+ " values (?, ?, ?, ?, ?, ?)";
	private static final String QUERY_BASE_SQL = " select uuid, store_id, name, logo, city_name, category_id"
			+ " from " + DataConst.TABLE_COUPON_STORE;

	@Override
	public int save(CouponStoreEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}
		for (CouponStoreEntity couponStore : data) {
			executeUpdate(UPDATE_SQL, new Object[] { couponStore.getUuid(),
					couponStore.getStoreId(), couponStore.getName(),
					couponStore.getLogo().getAliasName(), couponStore.getCityName(),
					couponStore.getCategoryId() });
		}
		return data.length;
	}

	@Override
	protected CouponStoreEntity processRow(Cursor cursor) {
		CouponStoreEntity store = new CouponStoreEntity();

		int i = 0;
		store.setUuid(cursor.getString(i++));
		store.setStoreId(cursor.getString(i++));
		store.setName(cursor.getString(i++));
		store.setLogo(new FileEntity(null, cursor.getString(i++)));
		store.setCityName(cursor.getString(i++));
		store.setCategoryId(cursor.getString(i++));

		return store;
	}

	public List<CouponStoreEntity> couponStoresByLocation(String categoryId,
			LocationEntity location) {
		String sql = QUERY_BASE_SQL + " where category_id = ? and city_name = ? ";
		return query(sql, new String[] { categoryId, location.getCityName() });
	}

	public void deleteCouponStoresByCity(String city) {
		String sql = " delete from " + DataConst.TABLE_COUPON_STORE
				+ " where city_name = ? ";
		executeUpdate(sql, new Object[] { city });
	}
}

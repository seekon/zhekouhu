package com.seekon.zhekouhu.func.store;

import java.util.UUID;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.utils.Logger;

public class CouponStoreEntity extends Entity {

	private static final long serialVersionUID = -7405299412966927482L;

	private String categoryId;
	private String storeId;
	private String name;
	private FileEntity logo;
	private String cityName;

	public CouponStoreEntity() {
		super();
	}

	public CouponStoreEntity(JSONObject json) {
		this();
		try {
			if (json.has(DataConst.COL_NAME_UUID)) {
				uuid = json.getString(DataConst.COL_NAME_UUID);
			} else {
				uuid = UUID.randomUUID().toString();
			}
			storeId = json.getString(DataConst.COL_NAME_STORE_ID);
			name = json.getString(DataConst.COL_NAME_NAME);
			logo = new FileEntity(null, json.getString(DataConst.COL_NAME_LOGO));
			cityName = json.getString(DataConst.COL_NAME_CITY_NAME);
			categoryId = json.getString(DataConst.COL_NAME_CATEGORY_ID);
		} catch (Exception e) {
			Logger.error(StoreEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public FileEntity getLogo() {
		return logo;
	}

	public void setLogo(FileEntity logo) {
		this.logo = logo;
	}

}

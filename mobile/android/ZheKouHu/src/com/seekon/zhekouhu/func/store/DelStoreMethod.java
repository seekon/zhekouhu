package com.seekon.zhekouhu.func.store;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class DelStoreMethod extends JSONObjResourceMethod {

	private static String DEL_STORE = Const.SERVER_APP_URL + "/deleteStore/";

	private String uuid;

	public DelStoreMethod(String uuid) {
		super();
		this.uuid = uuid;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.DELETE, DEL_STORE + uuid, null, null);
	}
}

package com.seekon.zhekouhu.func.store;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class DelStorefrontMethod extends JSONObjResourceMethod {

	private static String DEL_STOREFRONT = Const.SERVER_APP_URL
			+ "/deleteStorefront/";

	private String uuid;

	public DelStorefrontMethod(String uuid) {
		super();
		this.uuid = uuid;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.DELETE, DEL_STOREFRONT + uuid, null, null);
	}

}

package com.seekon.zhekouhu.func.store;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetCitiesMethod extends JSONObjResourceMethod {

	private static final String GET_CITIES_URL = Const.SERVER_APP_URL
			+ "/getCities/";

	private String updateTime;

	public GetCitiesMethod(String updateTime) {
		super();
		this.updateTime = updateTime;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, GET_CITIES_URL + updateTime, null, null);
	}

}

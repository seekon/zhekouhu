package com.seekon.zhekouhu.func.store;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetNearbyStoresMethod extends JSONObjResourceMethod {

	private static final String GET_NEARBY_STORES = "http://api.map.baidu.com/place/v2/search"
			+ "?output=json&scope=1"
			+ "&ak="
			+ Const.KEY_BAIDU_AK
			+ "&page_size="
			+ Const.PAGE_SIZE;

	private static final String[] QUERY_KEYS = new String[] { "餐馆", "宾馆", "娱乐" };

	private String query;
	private LocationEntity location;
	private int radius;
	private int offset;

	public GetNearbyStoresMethod(String query, LocationEntity location,
			int radius, int offset) {
		super();
		this.query = query;
		this.location = location;
		this.radius = radius;
		this.offset = offset;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer();
		url.append(GET_NEARBY_STORES);
		url.append("&page_num=" + (offset / Const.PAGE_SIZE));
		url.append("&location=" + location.getLat() + "," + location.getLng());
		url.append("&radius=" + radius);
		url.append("&query=");
		try {
			if (query == null || query.trim().length() == 0) {
				for (String key : QUERY_KEYS) {
					url.append(URLEncoder.encode(key, "utf-8") + "$");
				}
			} else {
				url.append(URLEncoder.encode(query, "utf-8"));
			}
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetNearbyStoresMethod.class.getName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
		return new BaseRequest(Method.GET, url.substring(0, url.length()), null,
				null);
	}

}

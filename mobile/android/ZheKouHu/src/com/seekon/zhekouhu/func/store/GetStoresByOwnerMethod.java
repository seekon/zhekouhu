package com.seekon.zhekouhu.func.store;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetStoresByOwnerMethod extends JSONObjResourceMethod {

	private static String GET_STORES_BY_OWNER = Const.SERVER_APP_URL
			+ "/getStores/";

	private String owner;
	private String updateTime;

	public GetStoresByOwnerMethod(String owner, String updateTime) {
		super();
		this.owner = owner;
		this.updateTime = updateTime;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, GET_STORES_BY_OWNER + owner + "/"
				+ updateTime, null, null);
	}

}

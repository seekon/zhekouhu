package com.seekon.zhekouhu.func.store;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class GetStoresHasCouponMethod extends JSONObjResourceMethod {

	private static final String GET_STORES_HAS_COUPON_URL = Const.SERVER_APP_URL
			+ "/getStoresHasCoupon/";

	private LocationEntity location;

	public GetStoresHasCouponMethod(LocationEntity location) {
		super();
		this.location = location;
	}

	@Override
	protected Request buildRequest() {
		String url = GET_STORES_HAS_COUPON_URL;
		try {
			url += URLEncoder.encode(location.getCityName(), "utf-8");
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetStoresHasCouponMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		return new BaseRequest(Method.GET, url, null, null);
	}

}

package com.seekon.zhekouhu.func.store;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_NAME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_ADDR;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_PHONE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_LATITUDE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_LONGITUDE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_CITY;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_CITY_NAME;
import org.json.JSONObject;

import com.seekon.zhekouhu.func.Entity;

public class NearbyStoreEntity extends Entity {

	private static final long serialVersionUID = 6345617498618268455L;

	private String name;
	private String addr;
	private String phone;
	private double lat;
	private double lng;
	private double distance;
	private String city;
	private String cityName;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	@Override
	public String toString() {
		try {
			JSONObject json = new JSONObject();
			json.put(COL_NAME_UUID, uuid);
			json.put(COL_NAME_ADDR, addr);
			json.put(COL_NAME_NAME, name);
			if (phone == null) {
				json.put(COL_NAME_PHONE, "");
			}else{
				json.put(COL_NAME_PHONE, phone);
			}
			json.put(COL_NAME_LATITUDE, lat);
			json.put(COL_NAME_LONGITUDE, lng);
			json.put(COL_NAME_CITY, city);
			json.put(COL_NAME_CITY_NAME, cityName);
			return json.toString();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}

package com.seekon.zhekouhu.func.store;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.file.FileEntity;

public class StoreData extends AbstractSQLiteData<StoreEntity> implements
		DataUpdater<StoreEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_STORE + " (uuid, name, logo, owner, register_time ) "
			+ " values (?, ?, ?, ?, ?)";
	private static final String QUERY_BASE_SQL = " select uuid, name, logo, owner, register_time"
			+ " from " + DataConst.TABLE_STORE + " s ";

	private static final String QUERY_CHECK_OWNER_SQL = " select count(1) "
			+ " from " + DataConst.TABLE_STORE + " s where owner = ? ";

	@Override
	public int save(StoreEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		StorefrontData frontData = new StorefrontData();

		for (StoreEntity store : data) {
			FileEntity logo = store.getLogo();
			executeUpdate(UPDATE_SQL, new Object[] { store.getUuid(),
					store.getName(), (logo != null ? logo.getAliasName() : null), store.getOwner(),
					store.getRegisterTime() });
			List<StorefrontEntity> fronts = store.getStorefronts();
			if (fronts.size() > 0) {
				frontData.save(fronts.toArray(new StorefrontEntity[fronts.size()]));
			}
		}
		return data.length;
	}

	public int save(String uuid, String name) {
		int count = queryCount("select count(1) from z_store where uuid = ?",
				new String[] { uuid });
		if (count > 0) {
			executeUpdate(" update z_store set name = ? where uuid = ? ", new String[]{name, uuid});
		} else {
			StoreEntity store = new StoreEntity();
			store.setUuid(uuid);
			store.setName(name);
			save(new StoreEntity[] { store });
		}

		return 1;
	}

	@Override
	protected StoreEntity processRow(Cursor cursor) {
		StoreEntity store = new StoreEntity();

		int i = 0;
		store.setUuid(cursor.getString(i++));
		store.setName(cursor.getString(i++));
		store.setLogo(new FileEntity(null, cursor.getString(i++)));
		store.setOwner(cursor.getString(i++));
		store.setRegisterTime(cursor.getString(i++));

		store.setStorefronts(new StorefrontData().storefrontsByStore(store));

		return store;
	}

	public List<StoreEntity> storesForOwner(String owner) {
		String sql = QUERY_BASE_SQL + " where owner = ? order by register_time ";
		return query(sql, new String[] { owner });
	}

	public void deleteStore(StoreEntity store) {
		String sql = " delete from " + DataConst.TABLE_STORE + " where uuid = ? ";
		String sql2 = " delete from " + DataConst.TABLE_STOREFRONT
				+ " where store_id = ? ";
		executeUpdate(sql, new Object[] { store.getUuid() });
		executeUpdate(sql2, new Object[] { store.getUuid() });
	}

	public boolean ownStores(String userId) {
		return queryCount(QUERY_CHECK_OWNER_SQL, new String[] { userId }) > 0;
	}
}

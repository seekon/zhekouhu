package com.seekon.zhekouhu.func.store;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.utils.Logger;

public class StoreEntity extends Entity {

	private static final long serialVersionUID = -7249561516477980362L;

	private String name;
	private FileEntity logo;
	private String owner;
	private String registerTime;
	private ArrayList<StorefrontEntity> storefronts;

	public StoreEntity() {
		super();
	}

	public StoreEntity(JSONObject json) {
		this();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			name = json.getString(DataConst.COL_NAME_NAME);
			logo = new FileEntity(null, json.getString(DataConst.COL_NAME_LOGO));
			owner = json.getString(DataConst.COL_NAME_OWNER);
			registerTime = json.getString(DataConst.COL_NAME_REGISTER_TIME);
		} catch (Exception e) {
			Logger.error(StoreEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FileEntity getLogo() {
		return logo;
	}

	public void setLogo(FileEntity logo) {
		this.logo = logo;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(String registerTime) {
		this.registerTime = registerTime;
	}

	public ArrayList<StorefrontEntity> getStorefronts() {
		if (storefronts == null) {
			storefronts = new ArrayList<StorefrontEntity>();
		}
		return storefronts;
	}

	public void setStorefronts(ArrayList<StorefrontEntity> storefronts) {
		this.storefronts = storefronts;
	}

	public String getStorefrontsJSONString() {
		JSONArray jsons = new JSONArray();
		for (StorefrontEntity front : storefronts) {
			jsons.put(front.toJSONObject());
		}
		return jsons.toString();
	}

}

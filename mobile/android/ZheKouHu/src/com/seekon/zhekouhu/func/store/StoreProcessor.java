package com.seekon.zhekouhu.func.store;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_LAST_MODIFY_TIME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_LATITUDE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_LONGITUDE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_REGISTER_TIME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UPDATE_TIME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.func.FuncConst.KEY_BD_ADDR;
import static com.seekon.zhekouhu.func.FuncConst.KEY_BD_LAT;
import static com.seekon.zhekouhu.func.FuncConst.KEY_BD_LNG;
import static com.seekon.zhekouhu.func.FuncConst.KEY_BD_LOCATION;
import static com.seekon.zhekouhu.func.FuncConst.KEY_BD_NAME;
import static com.seekon.zhekouhu.func.FuncConst.KEY_BD_UID;
import static com.seekon.zhekouhu.func.FuncConst.KEY_BD_PHONE;
import static com.seekon.zhekouhu.func.FuncConst.NAME_DATA;
import static com.seekon.zhekouhu.func.FuncConst.NAME_IS_DELETED;
import static com.seekon.zhekouhu.func.FuncConst.NAME_STOREFRONTS;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.spi.IStoreProcessor;
import com.seekon.zhekouhu.func.sync.SyncData;
import com.seekon.zhekouhu.func.sync.SyncEntity;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;
import com.seekon.zhekouhu.utils.LocationUtils;

public class StoreProcessor implements IStoreProcessor {

	private StoreData storeData;
	private StorefrontData storefrontData;
	private CityData cityData;
	private SyncData syncData;

	public StoreProcessor() {
		super();
		storeData = new StoreData();
		storefrontData = new StorefrontData();
		cityData = new CityData();
		syncData = new SyncData();
	}

	@Override
	public ProcessResult<List<StoreEntity>> storesForOwner(final String owner) {
		final String tableName = DataConst.TABLE_STORE;
		final String itemId = owner;
		String updateTime = "-1";
		SyncEntity sync = new SyncData().syncEntity(tableName, itemId);
		if (sync != null) {
			updateTime = sync.getUpdateTime();
		}

		MethodExecuter<List<StoreEntity>> executer = new MethodExecuter<List<StoreEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<StoreEntity>> result) throws Exception {
				ArrayList<StoreEntity> stores = new ArrayList<StoreEntity>();

				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UPDATE_TIME)) {
					JSONArray data = resource.getJSONArray(NAME_DATA);
					for (int i = 0; i < data.length(); i++) {
						JSONObject jsonStore = data.getJSONObject(i);

						StoreEntity store = new StoreEntity(jsonStore);

						ArrayList<StorefrontEntity> storefronts = new ArrayList<StorefrontEntity>();
						JSONArray jsonFronts = jsonStore.getJSONArray(NAME_STOREFRONTS);
						for (int j = 0; j < jsonFronts.length(); j++) {
							JSONObject jsonFront = jsonFronts.getJSONObject(j);
							if ("1".equals(jsonFront.get(NAME_IS_DELETED))) {
								storefrontData.deleteStorefront(jsonFront
										.getString(COL_NAME_UUID));
								continue;
							}

							StorefrontEntity front = new StorefrontEntity(jsonFront);
							front.setStore(store);
							storefronts.add(front);
						}

						store.setStorefronts(storefronts);
						if ("1".equals(jsonStore.get(NAME_IS_DELETED))) {
							storeData.deleteStore(store);
							continue;
						}

						stores.add(store);
					}

					// 更新店铺记录
					storeData.save(stores.toArray(new StoreEntity[stores.size()]));

					// 记录同步更新记录
					syncData.save(tableName, itemId,
							resource.getString(COL_NAME_UPDATE_TIME));
				}

				result.setSuccessObj(storesForOwnerFromLocal(owner));
			}
		};
		return executer.execute(new GetStoresByOwnerMethod(owner, updateTime));
	}

	public List<StoreEntity> storesForOwnerFromLocal(String owner) {
		return storeData.storesForOwner(owner);
	}

	@Override
	public ProcessResult<Boolean> registerStore(final StoreEntity store) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					store.setUuid(resource.getString(COL_NAME_UUID));
					store.setRegisterTime(resource.getString(COL_NAME_REGISTER_TIME));

					JSONArray jsonFronts = resource.getJSONArray(NAME_STOREFRONTS);
					for (int i = 0; i < jsonFronts.length(); i++) {
						JSONObject jsonFront = jsonFronts.getJSONObject(i);
						StorefrontEntity front = store.getStorefronts().get(i);
						front.setUuid(jsonFront.getString(COL_NAME_UUID));
						front.setLatitude(jsonFront.getDouble(COL_NAME_LATITUDE));
						front.setLongitude(jsonFront.getDouble(COL_NAME_LONGITUDE));
						front.setStore(store);
					}

					storeData.save(new StoreEntity[] { store });
					syncData.save(DataConst.TABLE_STORE, RunEnv.getInstance().getUser()
							.getUuid(), resource.getString(COL_NAME_LAST_MODIFY_TIME));

					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new RegisterStoreMethod(store));
	}

	@Override
	public ProcessResult<Boolean> updateStore(final StoreEntity store) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					JSONArray jsonFronts = resource.getJSONArray(NAME_STOREFRONTS);
					for (int i = 0; i < jsonFronts.length(); i++) {
						JSONObject jsonFront = jsonFronts.getJSONObject(i);
						StorefrontEntity front = store.getStorefronts().get(i);
						front.setUuid(jsonFront.getString(COL_NAME_UUID));
						front.setLatitude(jsonFront.getDouble(COL_NAME_LATITUDE));
						front.setLongitude(jsonFront.getDouble(COL_NAME_LONGITUDE));
					}

					storeData.save(new StoreEntity[] { store });
					syncData.save(DataConst.TABLE_STORE, RunEnv.getInstance().getUser()
							.getUuid(), resource.getString(COL_NAME_LAST_MODIFY_TIME));

					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new UpdateStoreMethod(store));
	}

	@Override
	public ProcessResult<Boolean> deleteStore(final StoreEntity store) {
		MethodExecuter<Boolean> excecuter = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					storeData.deleteStore(store);
					syncData.save(DataConst.TABLE_STORE, RunEnv.getInstance().getUser()
							.getUuid(), resource.getString(COL_NAME_LAST_MODIFY_TIME));

					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return excecuter.execute(new DelStoreMethod(store.getUuid()));
	}

	@Override
	public ProcessResult<Boolean> addStorefront(final StorefrontEntity storefront) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					storefront.setLatitude(resource.getDouble(COL_NAME_LATITUDE));
					storefront.setLongitude(resource.getDouble(COL_NAME_LONGITUDE));
					storefront.setUuid(resource.getString(COL_NAME_UUID));

					storefrontData.save(new StorefrontEntity[] { storefront });
					syncData.save(DataConst.TABLE_STORE, RunEnv.getInstance().getUser()
							.getUuid(), resource.getString(COL_NAME_LAST_MODIFY_TIME));

					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new AddStorefrontMethod(storefront));
	}

	@Override
	public ProcessResult<Boolean> deleteStorefront(
			final StorefrontEntity storefront) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					storefrontData.deleteStorefront(storefront.getUuid());
					syncData.save(DataConst.TABLE_STORE, RunEnv.getInstance().getUser()
							.getUuid(), resource.getString(COL_NAME_LAST_MODIFY_TIME));

					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new DelStorefrontMethod(storefront.getUuid()));
	}

	@Override
	public ProcessResult<Boolean> updateStorefront(
			final StorefrontEntity storefront) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UUID)) {
					storefront.setLatitude(resource.getDouble(COL_NAME_LATITUDE));
					storefront.setLongitude(resource.getDouble(COL_NAME_LONGITUDE));

					storefrontData.save(new StorefrontEntity[] { storefront });
					syncData.save(DataConst.TABLE_STORE, RunEnv.getInstance().getUser()
							.getUuid(), resource.getString(COL_NAME_LAST_MODIFY_TIME));

					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new UpdateStorefrontMethod(storefront));
	}

	@Override
	public ProcessResult<List<CityEntity>> cities() {
		final String tableName = DataConst.TABLE_CITY;
		final String itemId = "*";
		String updateTime = "-1";
		SyncEntity sync = new SyncData().syncEntity(tableName, itemId);
		if (sync != null) {
			updateTime = sync.getUpdateTime();
		}

		MethodExecuter<List<CityEntity>> executer = new MethodExecuter<List<CityEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<CityEntity>> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_UPDATE_TIME)) {

					List<CityEntity> cities = new ArrayList<CityEntity>();
					if (resource.has(NAME_DATA)) {// 更新记录
						JSONArray jsons = resource.getJSONArray(NAME_DATA);
						for (int i = 0; i < jsons.length(); i++) {
							cities.add(new CityEntity(jsons.getJSONObject(i)));
						}
					}

					// 更新城市记录
					cityData.save(cities.toArray(new CityEntity[cities.size()]));

					// 记录同步更新记录
					new SyncData().save(tableName, itemId,
							resource.getString(COL_NAME_UPDATE_TIME));
				}
				result.setSuccessObj(citiesFromLocal());
			}

		};
		return executer.execute(new GetCitiesMethod(updateTime));
	}

	@Override
	public List<CityEntity> citiesFromLocal() {
		return cityData.cities();
	}

	@Override
	public ProcessResult<List<NearbyStoreEntity>> getNearbyStores(String query,
			int radius, int offset) {
		final LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return new ProcessResult<List<NearbyStoreEntity>>(
					new ArrayList<NearbyStoreEntity>());
		}

		MethodExecuter<List<NearbyStoreEntity>> executer = new MethodExecuter<List<NearbyStoreEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<NearbyStoreEntity>> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				String status = resource.getString("status");
				if ("0".equals(status)) {// 检索成功
					result.setSuccessObj(assembleStores_baidu(resource
							.getJSONArray("results")));
				} else {
					result.setSuccess(false);
					result.setError("1", "获取商家数据失败.");
				}
			}
		};
		return executer.execute(new GetNearbyStoresMethod(query, location, radius,
				offset));
	}

	private List<NearbyStoreEntity> assembleStores_baidu(JSONArray results) throws JSONException {
		if (results == null) {
			return null;
		}

		LocationEntity location = RunEnv.getInstance().getLocation();
		List<NearbyStoreEntity> storeList = new ArrayList<NearbyStoreEntity>();

		for (int i = 0; i < results.length(); i++) {
			JSONObject result = results.getJSONObject(i);
			NearbyStoreEntity store = new NearbyStoreEntity();
			store.setUuid(result.getString(KEY_BD_UID));
			store.setAddr(result.getString(KEY_BD_ADDR));
			store.setName(result.getString(KEY_BD_NAME));
			if (result.has(KEY_BD_PHONE)) {
				store.setPhone(result.getString(KEY_BD_PHONE));
			}
			JSONObject loc = result.getJSONObject(KEY_BD_LOCATION);
			store.setLat(loc.getDouble(KEY_BD_LAT));
			store.setLng(loc.getDouble(KEY_BD_LNG));
			store.setCityName(location.getCityName());
			
			store.setDistance(LocationUtils.latlngDist(store.getLat(),
					store.getLng(), location.getLat(), location.getLng()));

			storeList.add(store);
		}

		return storeList;
	}
}

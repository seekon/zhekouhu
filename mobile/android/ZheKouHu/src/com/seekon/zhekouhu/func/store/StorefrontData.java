package com.seekon.zhekouhu.func.store;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.utils.LocationUtils;

public class StorefrontData extends AbstractSQLiteData<StorefrontEntity>
		implements DataUpdater<StorefrontEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_STOREFRONT
			+ " (uuid, store_id, name, addr, phone, latitude, longitude, city, city_name,lat_sin_lng_cos,lat_sin_lng_sin,lat_cos ) "
			+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String QUERY_BASE_SQL = " select sf.uuid, sf.name, addr, phone, latitude, longitude"
			+ ", store_id, s.name as store_name "
			+ ", c.uuid, c.name, c.first_letter "
			+ " from z_storefront sf join z_store s on sf.store_id = s.uuid "
			+ " join z_city c on sf.city = c.uuid ";
	private static final String DELETE_BASE_SQL = " delete  from "
			+ DataConst.TABLE_STOREFRONT;

	@Override
	public int save(StorefrontEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (StorefrontEntity front : data) {
			save(front);
		}
		return data.length;
	}

	public int save(StorefrontEntity front){
		CityData cityData = new CityData();
		StoreData storeData = new StoreData();
		double lat = LocationUtils.translateLat(front.getLatitude());// 记录经纬度的正余玄信息，便于sql中距离计算
		double lng = LocationUtils.translateLng(front.getLongitude());
		executeUpdate(
				UPDATE_SQL,
				new Object[] { front.getUuid(), front.getStore().getUuid(),
						front.getName(), front.getAddr(), front.getPhone(),
						front.getLatitude(), front.getLongitude(),
						front.getCity().getUuid(), front.getCity().getName(),
						LocationUtils.latSinLngCos(lat, lng),
						LocationUtils.latSinLngSin(lat, lng), LocationUtils.latCos(lat) });
		if (front.getCity() != null) {
			cityData.save(front.getCity().getUuid(), front.getCity().getName());
		}
		if (front.getStore() != null) {
			storeData.save(front.getStore().getUuid(), front.getStore().getName());
		}
		return 1;
	}
	
	@Override
	protected StorefrontEntity processRow(Cursor cursor) {
		StorefrontEntity front = new StorefrontEntity();

		int i = 0;
		front.setUuid(cursor.getString(i++));
		front.setName(cursor.getString(i++));
		front.setAddr(cursor.getString(i++));
		front.setPhone(cursor.getString(i++));
		front.setLatitude(cursor.getDouble(i++));
		front.setLongitude(cursor.getDouble(i++));

		StoreEntity store = new StoreEntity();
		store.setUuid(cursor.getString(i++));
		store.setName(cursor.getString(i++));
		front.setStore(store);

		CityEntity city = new CityEntity();
		city.setUuid(cursor.getString(i++));
		city.setName(cursor.getString(i++));
		city.setFirstLetter(cursor.getString(i++));
		front.setCity(city);

		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location != null) {
			front.setDistance(LocationUtils.latlngDist(location.getLat(),
					location.getLng(), front.getLatitude(), front.getLongitude()));
		}

		return front;
	}

	public ArrayList<StorefrontEntity> storefrontsByStore(StoreEntity store) {
		String sql = QUERY_BASE_SQL + " where store_id = ? ";
		ArrayList<StorefrontEntity> fronts = query(sql,
				new String[] { store.getUuid() });
		for (StorefrontEntity storefront : fronts) {
			storefront.setStore(store);
		}
		return fronts;
	}

	public List<StorefrontEntity> storefrontsForDiscount(String discountId) {
		String sql = QUERY_BASE_SQL
				+ " where sf.uuid in (select storefront_id from z_discount_storefront where discount_id = ?) ";
		return query(sql, new String[] { discountId });
	}

	public void deleteStorefront(String uuid) {
		String sql = DELETE_BASE_SQL + " where uuid = ? ";
		executeUpdate(sql, new Object[] { uuid });
	}

	public StorefrontEntity storefrontById(String uuid) {
		String sql = QUERY_BASE_SQL + " where sf.uuid = ? ";
		return queryOne(sql, new String[] { uuid });
	}
}

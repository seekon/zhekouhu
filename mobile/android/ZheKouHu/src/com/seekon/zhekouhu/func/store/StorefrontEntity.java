package com.seekon.zhekouhu.func.store;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.utils.Logger;

public class StorefrontEntity extends Entity {

	private static final long serialVersionUID = -8393867179128085795L;

	private CityEntity city;
	private String name;
	private String addr;
	private String phone;
	private StoreEntity store;
	private double latitude;
	private double longitude;

	private double distance;

	public StorefrontEntity() {
		super();
	}

	public StorefrontEntity(JSONObject json) {
		this();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			name = json.getString(DataConst.COL_NAME_NAME);
			addr = json.getString(DataConst.COL_NAME_ADDR);
			phone = json.getString(DataConst.COL_NAME_PHONE);
			if ("null".equalsIgnoreCase(phone)) {
				phone = "";
			}
			latitude = json.getDouble(DataConst.COL_NAME_LATITUDE);
			longitude = json.getDouble(DataConst.COL_NAME_LONGITUDE);

			city = new CityEntity();
			city.setUuid(json.getString(DataConst.COL_NAME_CITY));
			city.setName(json.getString(DataConst.COL_NAME_CITY_NAME));
			if (json.has(DataConst.COL_NAME_FIRST_LETTER)) {
				city.setFirstLetter(json.getString(DataConst.COL_NAME_FIRST_LETTER));
			}
			if (store == null && json.has(DataConst.COL_NAME_STORE_ID)) {
				store = new StoreEntity();
				store.setUuid(json.getString(DataConst.COL_NAME_STORE_ID));
				if (json.has(DataConst.COL_NAME_STORE_NAME)) {
					store.setName(json.getString(DataConst.COL_NAME_STORE_NAME));
				}
			}
		} catch (Exception e) {
			Logger.error(StorefrontEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public CityEntity getCity() {
		return city;
	}

	public void setCity(CityEntity city) {
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public StoreEntity getStore() {
		return store;
	}

	public void setStore(StoreEntity store) {
		this.store = store;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public JSONObject toJSONObject() {
		JSONObject json = new JSONObject();
		try {
			String uuid = getUuid();
			if (uuid != null) {
				json.put(DataConst.COL_NAME_UUID, uuid);
			}
			if (store != null) {
				json.put(DataConst.COL_NAME_STORE_ID, store.getUuid());
			}
			if (phone != null && phone.length() > 0) {
				json.put(DataConst.COL_NAME_PHONE, phone);
			}
			json.put(DataConst.COL_NAME_CITY, city.getUuid());
			json.put(DataConst.COL_NAME_CITY_NAME, city.getName());
			json.put(DataConst.COL_NAME_NAME, name);
			json.put(DataConst.COL_NAME_ADDR, addr);
			// json.put(DataConst.COL_NAME_LATITUDE, latitude);
			// json.put(DataConst.COL_NAME_LONGITUDE, longitude);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return json;
	}
}

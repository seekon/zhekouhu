package com.seekon.zhekouhu.func.store;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.MultipartRequest;
import com.seekon.zhekouhu.rest.MultipartRestMethod;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;

public class UpdateStoreMethod extends MultipartRestMethod<JSONObjResource> {

	private static final URI UPDATE_STORE_URI = URI.create(Const.SERVER_APP_URL
			+ "/updateStore");

	private StoreEntity store;

	public UpdateStoreMethod(StoreEntity store) {
		super();
		this.store = store;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_UUID, store.getUuid());
		params.put(DataConst.COL_NAME_NAME, store.getName());
		params.put(DataConst.COL_NAME_LOGO, store.getLogo().getAliasName());
		params.put(DataConst.COL_NAME_OWNER, store.getOwner());
		params.put(FuncConst.NAME_STOREFRONTS, store.getStorefrontsJSONString());

		List<FileEntity> fileEntities = null;
		if (store.getLogo().getFileUri() != null) {
			fileEntities = new ArrayList<FileEntity>();
			fileEntities.add(store.getLogo());
		}

		return new MultipartRequest(Method.PUT, UPDATE_STORE_URI, null, params,
				fileEntities);
	}

	@Override
	protected JSONObjResource parseResponseBody(String responseBody)
			throws Exception {
		return new JSONObjResource(responseBody);
	}

}

package com.seekon.zhekouhu.func.store;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class UpdateStorefrontMethod extends JSONObjResourceMethod {

	private static final URI UPDATE_STOREFRONT_URI = URI
			.create(Const.SERVER_APP_URL + "/updateStorefront");

	private StorefrontEntity storefront;

	public UpdateStorefrontMethod(StorefrontEntity storefront) {
		super();
		this.storefront = storefront;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(FuncConst.NAME_DATA, storefront.toJSONObject().toString());

		return new BaseRequest(Method.PUT, UPDATE_STOREFRONT_URI, null, params);
	}

}

package com.seekon.zhekouhu.func.store.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;

public class CategoryChooseAdapter extends AbstractListAdapter<CategoryEntity> {

	private CategoryEntity selectedCategory;

	public CategoryChooseAdapter(Context context, List<CategoryEntity> itemList,
			CategoryEntity selectedCategory) {
		super(context, itemList);
		this.selectedCategory = selectedCategory;
	}

	public CategoryEntity getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(CategoryEntity selectedCategory) {
		this.selectedCategory = selectedCategory;
		this.notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.common_list_item_radio, null);

			holder = new ViewHolder();
			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);
			holder.checkedView = (ImageView) convertView
					.findViewById(R.id.img_checked);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CategoryEntity category = (CategoryEntity) getItem(position);
		holder.nameView.setText(category.getName());
		if (selectedCategory != null && category.equals(selectedCategory)) {
			holder.checkedView.setVisibility(View.VISIBLE);
		} else {
			holder.checkedView.setVisibility(View.GONE);
		}

		return convertView;
	}

	class ViewHolder {
		TextView nameView;
		ImageView checkedView;
	}
}

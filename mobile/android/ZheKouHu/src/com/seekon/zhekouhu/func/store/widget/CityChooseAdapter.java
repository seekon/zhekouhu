package com.seekon.zhekouhu.func.store.widget;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.store.CityEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;

public class CityChooseAdapter extends AbstractListAdapter<CityEntity>
		implements SectionIndexer {

	private CityEntity selectedCity;

	private Map<String, Integer> catalogMap = new HashMap<String, Integer>();

	public CityChooseAdapter(Context context, List<CityEntity> itemList,
			CityEntity selectedCity) {
		super(context, itemList);
		this.selectedCity = selectedCity;
		initCatalogList();
	}

	private void initCatalogList() {
		catalogMap.clear();
		int size = itemList.size();
		for (int i = 0; i < size; i++) {
			String firstLetter = itemList.get(i).getFirstLetter();
			Set<String> keys = catalogMap.keySet();
			if (!keys.contains(firstLetter)) {
				catalogMap.put(firstLetter, i);
			}
		}
	}

	public CityEntity getSelectedCity() {
		return selectedCity;
	}

	public void setSelectedCity(CityEntity selectedCity) {
		this.selectedCity = selectedCity;
		this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;

		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.city_choose_item, null);
			viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.t_name);
			viewHolder.categoryView = convertView.findViewById(R.id.row_catalog);
			viewHolder.tvLetter = (TextView) convertView
					.findViewById(R.id.city_catalog);
			viewHolder.imageView = (ImageView) convertView
					.findViewById(R.id.img_checked);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (catalogMap.isEmpty()) {
			initCatalogList();
		}
		CityEntity city = itemList.get(position);
		String firstLetter = city.getFirstLetter();
		int catalogIndex = catalogMap.get(firstLetter);
		if (position == catalogIndex) {
			viewHolder.categoryView.setVisibility(View.VISIBLE);
			viewHolder.tvLetter.setText(firstLetter);
		} else {
			viewHolder.categoryView.setVisibility(View.GONE);
		}

		viewHolder.tvTitle.setText(city.getName());
		if (selectedCity != null && selectedCity.equals(city)) {
			viewHolder.imageView.setVisibility(View.VISIBLE);
		} else {
			viewHolder.imageView.setVisibility(View.GONE);
		}
		return convertView;
	}

	public Set<String> getCatalogKeys() {
		return catalogMap.keySet();
	}

	@Override
	public int getPositionForSection(int section) {
		Set<String> keys = getCatalogKeys();
		for (String key : keys) {
			if (key.charAt(0) == section) {
				return catalogMap.get(key);
			}
		}
		return 0;
	}

	@Override
	public int getSectionForPosition(int position) {
		return itemList.get(position).getFirstLetter().charAt(0);
	}

	@Override
	public Object[] getSections() {
		return getCatalogKeys().toArray();
	}

	public static class ViewHolder {
		public View categoryView;
		public TextView tvLetter;
		public TextView tvTitle;
		public ImageView imageView;
	}
}

package com.seekon.zhekouhu.func.store.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.store.NearbyStoreEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;
import com.seekon.zhekouhu.utils.LocationUtils;

public class NearbyStoreListAdapter extends
		AbstractListAdapter<NearbyStoreEntity> {

	public NearbyStoreListAdapter(Context context,
			List<NearbyStoreEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.discount_storefront_item, null);

			holder = new ViewHolder();
			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);
			holder.distanceView = (TextView) convertView
					.findViewById(R.id.t_distance);
			holder.addrView = (TextView) convertView.findViewById(R.id.t_addr);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		NearbyStoreEntity store = (NearbyStoreEntity) getItem(position);
		holder.nameView.setText(store.getName());
		holder.addrView.setText(store.getAddr());
		holder.distanceView.setText(LocationUtils.formatDistance(store
				.getDistance() / 1000) + "km");

		return convertView;
	}

	class ViewHolder {
		TextView nameView;
		TextView distanceView;
		TextView addrView;
	}
}

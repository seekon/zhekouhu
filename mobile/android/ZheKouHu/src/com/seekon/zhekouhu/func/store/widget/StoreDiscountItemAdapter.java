package com.seekon.zhekouhu.func.store.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.file.ImageLoader;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;

public class StoreDiscountItemAdapter extends
		AbstractListAdapter<DiscountEntity> {

	public StoreDiscountItemAdapter(Context context, List<DiscountEntity> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.store_discount_item, null);

			holder = new ViewHolder();

			holder.logoView = (ImageView) convertView.findViewById(R.id.img_logo);

			holder.contentView = (TextView) convertView.findViewById(R.id.t_content);
			holder.visitView = (TextView) convertView
					.findViewById(R.id.t_visit_count);
			holder.parValueView = (TextView) convertView
					.findViewById(R.id.t_par_value);
			holder.statusView = (TextView) convertView
					.findViewById(R.id.t_audit_status);
			holder.visitLayout = convertView.findViewById(R.id.v_visit_count);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		DiscountEntity discount = (DiscountEntity) getItem(position);
		FileEntity imageFile = discount.getImages().get(0);
		ImageLoader.getInstance().displayImage(imageFile.getAliasName(),
				holder.logoView, true);

		String content = discount.getContent();
		if (content == null || content.trim().length() == 0) {
			holder.contentView.setText("无内容");
		} else {
			holder.contentView.setText(content);
		}
		String status = discount.getStatus();
		if (FuncConst.VAL_DISCOUNT_STATUS_VALID.equals(status)) {
			holder.statusView.setVisibility(View.GONE);
			holder.visitLayout.setVisibility(View.VISIBLE);
			holder.visitView.setText(discount.getVisitCount() + "次");
		} else {
			holder.statusView.setVisibility(View.VISIBLE);
			holder.visitLayout.setVisibility(View.GONE);
			if (FuncConst.VAL_DISCOUNT_STATUS_INVALID.equals(status)) {
				holder.statusView.setText(R.string.label_status_invalid);
				holder.statusView.setTextColor(context.getResources().getColor(
						android.R.color.holo_red_dark));
			} else if (FuncConst.VAL_DISCOUNT_STATUS_CHECKING.equals(status)) {
				holder.statusView.setText(R.string.label_status_checking);
				holder.statusView.setTextColor(context.getResources().getColor(
						R.color.rgb_ff6722));
			}
		}

		if (discount.getType().equals(FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY)) {
			holder.parValueView.setVisibility(View.GONE);
		} else {
			holder.parValueView.setVisibility(View.VISIBLE);
			holder.parValueView.setText("￥" + discount.getParValue());
		}

		return convertView;
	}

	class ViewHolder {
		ImageView logoView;
		TextView contentView;
		TextView visitView;
		TextView parValueView;
		TextView statusView;
		View visitLayout;
	}
}

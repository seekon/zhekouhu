package com.seekon.zhekouhu.func.store.widget;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_TYPE;

import java.util.List;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.seekon.zhekouhu.activity.store.DiscountEditActivity;
import com.seekon.zhekouhu.activity.user.UserEditDiscountActivity;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.DateIndexedEntity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.widget.DateIndexedListAdapter;

public class StoreDiscountListAdapter extends
		DateIndexedListAdapter<DiscountEntity> {

	private Fragment fragment;

	private int requestCode;

	public StoreDiscountListAdapter(Fragment fragment,
			List<DateIndexedEntity> dataList, int requestCode) {
		super(fragment.getActivity(), dataList);
		this.fragment = fragment;
		this.requestCode = requestCode;
	}

	@Override
	public void initSubItemListView(ListView subItemListView,
			final List<DiscountEntity> subItemDataList) {
		subItemListView.setAdapter(new StoreDiscountItemAdapter(context,
				subItemDataList));

		subItemListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				DiscountEntity discount = subItemDataList.get(position);
				Intent intent = null;
				String origin = discount.getOrigin();
				if (origin.equals(FuncConst.VAL_DISCOUNT_ORIGIN_STORER)) {
					intent = new Intent(context, DiscountEditActivity.class);
					intent.putExtra(FuncConst.NAME_DISCOUNT, discount);
					intent.putExtra(DataConst.COL_NAME_TYPE, discount.getType());
				}else{
					intent = new Intent(context, UserEditDiscountActivity.class);
					intent.putExtra(FuncConst.NAME_DISCOUNT, discount);
				}
				
				fragment.startActivityForResult(intent, requestCode);
			}
		});
	}

}

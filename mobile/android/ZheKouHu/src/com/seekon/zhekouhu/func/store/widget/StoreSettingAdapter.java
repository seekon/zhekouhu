package com.seekon.zhekouhu.func.store.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;

public class StoreSettingAdapter extends AbstractListAdapter<String> {

	public StoreSettingAdapter(Context context, List<String> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.common_list_item_arrow, null);

			holder = new ViewHolder();
			holder.textView = (TextView) convertView.findViewById(R.id.textViewItem);
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.imageViewItem);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		String item = (String) getItem(position);
		holder.textView.setText(item);

		if (position == 0) {
			holder.imageView.setVisibility(View.INVISIBLE);
		} else {
			holder.imageView.setVisibility(View.VISIBLE);
		}

		return convertView;
	}

	class ViewHolder {
		ImageView imageView;
		TextView textView;
	}
}

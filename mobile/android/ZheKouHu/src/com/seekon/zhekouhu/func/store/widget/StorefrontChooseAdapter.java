package com.seekon.zhekouhu.func.store.widget;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;

public class StorefrontChooseAdapter extends BaseAdapter implements
		StickyListHeadersAdapter {

	private List<StorefrontEntity> selectStorefronts;
	private List<StorefrontEntity> storefronts = new ArrayList<StorefrontEntity>();
	private Context context;

	public StorefrontChooseAdapter(List<StorefrontEntity> selectStorefronts,
			List<StoreEntity> stores, Context context) {
		super();

		this.selectStorefronts = selectStorefronts;
		this.context = context;
		if (stores != null) {
			for (StoreEntity store : stores) {
				storefronts.addAll(store.getStorefronts());
			}
		}
		if (this.selectStorefronts == null) {
			this.selectStorefronts = new ArrayList<StorefrontEntity>();
		}
	}

	public void updateData(List<StoreEntity> stores) {
		storefronts.clear();
		if (stores != null) {
			for (StoreEntity store : stores) {
				storefronts.addAll(store.getStorefronts());
			}
		}
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return storefronts == null ? 0 : storefronts.size();
	}

	@Override
	public Object getItem(int position) {
		return storefronts.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.common_list_item_checkbox, null);

			holder.nameView = (TextView) convertView.findViewById(R.id.textViewItem);
			holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBoxItem);
			holder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					StorefrontEntity storefront = storefronts.get(position);
					if (isChecked && !selectStorefronts.contains(storefront)) {
						selectStorefronts.add(storefront);
					} else if (!isChecked) {
						selectStorefronts.remove(storefront);
					}
				}
			});

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		StorefrontEntity storefront = (StorefrontEntity) getItem(position);
		holder.nameView.setText(storefront.getName());

		if (selectStorefronts.contains(storefront)) {
			holder.checkBox.setChecked(true);
		} else {
			holder.checkBox.setChecked(false);
		}

		return convertView;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		StoreEntity store = ((StorefrontEntity) getItem(position)).getStore();
		HeaderViewHolder holder;
		if (convertView == null) {
			holder = new HeaderViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.common_list_item_header, null);

			holder.nameView = (TextView) convertView.findViewById(R.id.item_header);

			convertView.setTag(holder);
		} else {
			holder = (HeaderViewHolder) convertView.getTag();
		}
		holder.nameView.setText(store.getName());

		return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		StorefrontEntity storefront = (StorefrontEntity) getItem(position);
		return storefront.getStore().hashCode();
	}

	public List<StorefrontEntity> getSelectStorefronts() {
		return selectStorefronts;
	}

	public void setSelectStorefronts(List<StorefrontEntity> selectStorefronts) {
		this.selectStorefronts = selectStorefronts;
		this.notifyDataSetChanged();
	}

	class ViewHolder {
		TextView nameView;
		CheckBox checkBox;
	}

	class HeaderViewHolder {
		TextView nameView;
	}
}

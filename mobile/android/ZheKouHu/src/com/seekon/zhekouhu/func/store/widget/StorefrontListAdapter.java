package com.seekon.zhekouhu.func.store.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.widget.AbstractListAdapter;

public class StorefrontListAdapter extends
		AbstractListAdapter<StorefrontEntity> {

	private boolean showCityName;
	private boolean showStoreName;

	public StorefrontListAdapter(Context context,
			List<StorefrontEntity> itemList, boolean showCityName,
			boolean showStoreName) {
		super(context, itemList);
		this.showCityName = showCityName;
		this.showStoreName = showStoreName;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.storefront_list_item, null);

			holder = new ViewHolder();
			holder.textView = (TextView) convertView.findViewById(R.id.t_name);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		StorefrontEntity front = (StorefrontEntity) getItem(position);
		String textName = front.getName();

		if (showCityName) {
			textName = front.getCity().getName() + "-" + textName;
		}
		if (showStoreName) {
			textName = front.getStore().getName() + "-" + textName;
		}
		holder.textView.setText(textName);

		return convertView;
	}

	class ViewHolder {
		TextView textView;
	}
}

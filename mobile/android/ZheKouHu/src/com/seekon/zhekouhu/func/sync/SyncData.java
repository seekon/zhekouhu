package com.seekon.zhekouhu.func.sync;

import static com.seekon.zhekouhu.db.DataConst.TABLE_SYNC;
import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataUpdater;

public class SyncData extends AbstractSQLiteData<SyncEntity> implements
		DataUpdater<SyncEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ TABLE_SYNC + " (uuid, table_name, item_id, update_time) "
			+ " values (?, ?, ?, ?) ";

	private static final String QUERY_SQL = " select uuid, table_name, item_id, update_time from "
			+ TABLE_SYNC + " where table_name = ? and  item_id = ? ";

	@Override
	public int save(SyncEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (SyncEntity sync : data) {
			save(sync.getUuid(), sync.getTableName(), sync.getItemId(),
					sync.getUpdateTime());
		}
		return data.length;
	}

	@Override
	protected SyncEntity processRow(Cursor cursor) {
		SyncEntity sync = new SyncEntity();

		int i = 0;
		sync.setUuid(cursor.getString(i++));
		sync.setTableName(cursor.getString(i++));
		sync.setItemId(cursor.getString(i++));
		sync.setUpdateTime(cursor.getString(i++));

		return sync;
	}

	public SyncEntity syncEntity(String tableName, String itemId) {
		return queryOne(QUERY_SQL, new String[] { tableName, itemId });
	}

	public void save(String tableName, String itemId, String updateTime) {
		String uuid = null;
		SyncEntity sync = syncEntity(tableName, itemId);
		if (sync == null) {
			uuid = System.currentTimeMillis() + "";
		} else {
			uuid = sync.getUuid();
		}
		save(uuid, tableName, itemId, updateTime);
	}

	private void save(String uuid, String tableName, String itemId,
			String updateTime) {
		executeUpdate(UPDATE_SQL, new Object[] { uuid, tableName, itemId,
				updateTime });
	}
}

package com.seekon.zhekouhu.func.sync;

import com.seekon.zhekouhu.func.Entity;

public class SyncEntity extends Entity {

	private static final long serialVersionUID = 6507944209536428344L;

	private String tableName;
	private String itemId;
	private String updateTime;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

}

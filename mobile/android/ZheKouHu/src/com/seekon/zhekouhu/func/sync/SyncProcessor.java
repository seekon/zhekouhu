package com.seekon.zhekouhu.func.sync;

import static com.seekon.zhekouhu.func.FuncConst.VAL_DISCOUNT_TYPE_ACTIVITY;
import static com.seekon.zhekouhu.func.FuncConst.VAL_DISCOUNT_TYPE_COUPON;

import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.spi.ISyncProcessor;
import com.seekon.zhekouhu.func.store.LocationEntity;

public class SyncProcessor implements ISyncProcessor {

	private SyncData syncData;

	public SyncProcessor() {
		this.syncData = new SyncData();
	}

	public static String getSyncTableNameWithOwner(String type) {
		String tableName = null;
		if (type.equals(VAL_DISCOUNT_TYPE_ACTIVITY)) {
			tableName = FuncConst.SYNC_TABLE_ACTIVITIES_BY_OWNER;
		} else if (type.equals(VAL_DISCOUNT_TYPE_COUPON)) {
			tableName = FuncConst.SYNC_TABLE_COUPONS_BY_OWNER;
		} else {
			tableName = FuncConst.SYNC_TABLE_DISCOUNTS_BY_OWNER;
		}
		return tableName;
	}

	@Override
	public SyncEntity discountSyncEntityByOwner(String owner, String type) {
		final String tableName = getSyncTableNameWithOwner(type);
		return syncData.syncEntity(tableName, owner);
	}

	@Override
	public SyncEntity couponMainSyncEntity() {
		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return null;
		}
		return syncData.syncEntity(FuncConst.SYNC_TABLE_COUPON_MAIN,
				location.getCityName());
	}

	@Override
	public SyncEntity storeCouponSyncEntity(String storeId) {
		LocationEntity location = RunEnv.getInstance().getLocation();
		if (location == null || location.getCityName() == null) {
			return null;
		}
		return syncData.syncEntity(FuncConst.SYNC_TABLE_STORE_COUPON, storeId);
	}
}

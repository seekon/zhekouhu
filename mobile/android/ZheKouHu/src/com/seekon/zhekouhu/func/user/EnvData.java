package com.seekon.zhekouhu.func.user;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.utils.Logger;

public class EnvData extends AbstractSQLiteData<String> {

	private static final String TAG = EnvData.class.getSimpleName();

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_ENV
			+ "(code, login_setting, last_modify_time) values (?, ?, ?) ";

	private static final String LAST_TIME_QUERY_SQL = "select login_setting from "
			+ DataConst.TABLE_ENV + " order by last_modify_time desc";

	private static final String DEL_SQL = " delete from " + DataConst.TABLE_ENV
			+ " where code = ? ";

	public void saveLoginEnv(UserEntity user) {
		executeUpdate(UPDATE_SQL, new Object[] { user.getCode(),
				parseToLoginSetting(user), System.currentTimeMillis() });
	}

	private String parseToLoginSetting(UserEntity user) {
		JSONObject json = new JSONObject();
		try {
			json.put(DataConst.COL_NAME_CODE, user.getCode());
			json.put(DataConst.COL_NAME_PWD, user.getPwd());
		} catch (Exception e) {
			Logger.warn(TAG, e.getMessage(), e);
		}
		return json.toString();
	}

	@Override
	protected String processRow(Cursor cursor) {
		return cursor.getString(0);
	}

	public Map<String, String> lastLoginEnv() {
		String loginSetting = this.queryOne(LAST_TIME_QUERY_SQL, null);
		if (loginSetting == null) {
			return null;
		}
		try {
			JSONObject json = new JSONObject(loginSetting);

			Map<String, String> result = new HashMap<String, String>();
			result.put(DataConst.COL_NAME_CODE,
					json.getString(DataConst.COL_NAME_CODE));
			result
					.put(DataConst.COL_NAME_PWD, json.getString(DataConst.COL_NAME_PWD));

			return result;
		} catch (Exception e) {
			Logger.warn(TAG, e.getMessage(), e);
		}
		return null;
	}

	public void deleteLoginEnv(String code) {
		executeUpdate(DEL_SQL, new Object[] { code });
	}
}

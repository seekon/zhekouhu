package com.seekon.zhekouhu.func.user;

import java.util.List;

import android.database.Cursor;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.discount.DiscountData;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.store.LocationEntity;

public class FavoritData extends AbstractSQLiteData<DiscountEntity> implements
		DataUpdater<FavoritEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_USER_FAVORIT
			+ " (uuid, user_id, discount_id) values (?, ?, ?)";
	private static final String QUERY_SQL = " select distinct discount_id from "
			+ DataConst.TABLE_USER_FAVORIT + " where user_id = ? ";
	private static final String DEL_SQL = " delete from "
			+ DataConst.TABLE_USER_FAVORIT
			+ " where user_id = ? and discount_id = ? ";
	private static final String IS_FAVORIT_SQL = " select count(1) from "
			+ DataConst.TABLE_USER_FAVORIT
			+ " where user_id = ? and discount_id = ? ";

	@Override
	public int save(FavoritEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}
		for (FavoritEntity userFavorit : data) {
			executeUpdate(UPDATE_SQL, new String[] { userFavorit.getUuid(),
					userFavorit.getUserId(), userFavorit.getDiscount().getUuid() });
		}
		return data.length;
	}

	@Override
	protected DiscountEntity processRow(Cursor cursor) {
		int i = 0;

		LocationEntity location = RunEnv.getInstance().getLocation();
		DiscountEntity discount = new DiscountData().discountById(
				cursor.getString(i++), location);

		return discount;
	}

	public List<DiscountEntity> userFovarits(String userId, int offset) {
		StringBuffer sql = new StringBuffer();
		sql.append(QUERY_SQL);
		sql.append(" limit " + Const.PAGE_SIZE + " offset " + offset);

		return query(sql.toString(), new String[] { userId });
	}

	public void deleteFavorit(String userId, String discountId) {
		executeUpdate(DEL_SQL, new String[] { userId, discountId });
	}

	public boolean isFavorited(String userId, String discountId) {
		return queryCount(IS_FAVORIT_SQL, new String[] { userId, discountId }) > 0;
	}
}

package com.seekon.zhekouhu.func.user;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.func.discount.DiscountEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.Logger;

public class FavoritEntity extends Entity {

	private static final long serialVersionUID = -3631063158893657L;

	private String userId;
	private DiscountEntity discount;

	public FavoritEntity() {
		super();
	}

	public FavoritEntity(JSONObject json) {
		super();
		try {
			uuid = json.getString(DataConst.COL_NAME_UUID);
			if (json.has(DataConst.COL_NAME_USER_ID)) {
				userId = json.getString(DataConst.COL_NAME_USER_ID);
			}
		} catch (Exception e) {
			Logger.error(StorefrontEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public DiscountEntity getDiscount() {
		return discount;
	}

	public void setDiscount(DiscountEntity discount) {
		this.discount = discount;
	}

}

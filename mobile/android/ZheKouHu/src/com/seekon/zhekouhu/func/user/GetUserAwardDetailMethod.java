package com.seekon.zhekouhu.func.user;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONArrayResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetUserAwardDetailMethod extends JSONArrayResourceMethod {

	private static String GET_USER_AWARD_DETAIL = Const.SERVER_APP_URL
			+ "/getUserAwardDetail/";

	private String userId;
	private int offset;

	public GetUserAwardDetailMethod(String userId, int offset) {
		super();
		this.userId = userId;
		this.offset = offset;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, GET_USER_AWARD_DETAIL + userId + "/"
				+ offset, null, null);
	}

}

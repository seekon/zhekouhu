package com.seekon.zhekouhu.func.user;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetUserAwardMethod extends JSONObjResourceMethod {

	private static String GET_USER_AWARD = Const.SERVER_APP_URL
			+ "/getUserAward/";

	private String userId;

	public GetUserAwardMethod(String userId) {
		super();
		this.userId = userId;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, GET_USER_AWARD + userId, null, null);
	}

}

package com.seekon.zhekouhu.func.user;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.BaseRestMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.rest.Response;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.RestStatus;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;
import com.seekon.zhekouhu.utils.ConnectionDetector;
import com.seekon.zhekouhu.utils.Logger;

/**
 * 登录rest访问
 * 
 * @author undyliu
 * 
 */
public class LoginMethod extends BaseRestMethod<JSONObjResource> {

	private final static String TAG = LoginMethod.class.getSimpleName();

	private String code;
	private String pwd;

	private static final URI LOGIN_URI = URI.create(Const.SERVER_APP_URL
			+ "/login");

	public LoginMethod(String code, String pwd) {
		super();
		this.code = code;
		this.pwd = pwd;
	}

	@Override
	public RestMethodResult<JSONObjResource> execute() {
		ConnectionDetector detector = ConnectionDetector.getInstance(getContext());
		if (!detector.isConnectingToInternet()) {// 检测网络是否开启，可用
			return new RestMethodResult<JSONObjResource>(
					RestStatus.NETWORK_NOT_OPENED, getContext().getString(
							R.string.network_not_open), null);
		}

		Request request = buildRequest();

		Response response = null;
		try {
			response = doRequest(request);
		} catch (Exception ex) {
			Logger.warn(TAG, ex.getMessage());
			int status = RestStatus.SERVER_NOT_AVAILABLE;
			String statusMsg = getContext().getString(R.string.server_not_available);
			return new RestMethodResult<JSONObjResource>(status, statusMsg, null);
		}

		List<String> cookieValues = response.getHeaders().get("Set-Cookie");
		if (cookieValues != null && cookieValues.size() > 0) {
			String cookieValue = cookieValues.get(0);
			if (cookieValue != null && cookieValue.length() > 0) {
				RunEnv.getInstance().setSessionId(
						cookieValue.substring(0, cookieValue.indexOf(";")));
			}
		}
		try {
			return buildResult(response);
		} catch (Exception e) {
			Logger.warn(TAG, e.getMessage());
			int status = RestStatus.RUNTIME_ERROR;
			String statusMsg = getContext().getString(R.string.runtime_error);
			return new RestMethodResult<JSONObjResource>(status, statusMsg, null);
		}
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_CODE, code);
		params.put(DataConst.COL_NAME_PWD, pwd);
		return new BaseRequest(Method.POST, LOGIN_URI, null, params);
	}

	@Override
	protected JSONObjResource parseResponseBody(String responseBody)
			throws Exception {
		return new JSONObjResource(responseBody);
	}

}

package com.seekon.zhekouhu.func.user;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.discount.GetActivitiesByDistanceMethod;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.utils.Logger;

public class RefreshFavoritsMethod extends JSONObjResourceMethod {

	private static final String GET_FAVORITS_BY_USER_URL = Const.SERVER_APP_URL
			+ "/getUserFavorits";

	private String userId;
	private String updateTime;
	private LocationEntity location;
	
	public RefreshFavoritsMethod(String userId, String updateTime,
			LocationEntity location) {
		super();
		this.userId = userId;
		this.updateTime = updateTime;
		this.location = location;
	}

	@Override
	protected Request buildRequest() {
		StringBuffer url = new StringBuffer();
		url.append(GET_FAVORITS_BY_USER_URL);
		url.append("/" + userId);
		url.append("/" + updateTime);
		try {
			url.append("/" + URLEncoder.encode(location.getCityName(), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			Logger.warn(GetActivitiesByDistanceMethod.class.getSimpleName(),
					e.getMessage(), e);
			throw new RuntimeException(e);
		}
		url.append("/" + location.getLat());
		url.append("/" + location.getLng());
		
		return new BaseRequest(Method.GET, url.toString(), null, null);
	}

}

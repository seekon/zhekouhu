package com.seekon.zhekouhu.func.user;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class RegisterMethod extends JSONObjResourceMethod {

	private static final URI REGISTER_URI = URI.create(Const.SERVER_APP_URL
			+ "/registerUser");

	private String code;
	private String pwd;

	public RegisterMethod(String code, String pwd) {
		super();
		this.code = code;
		this.pwd = pwd;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_CODE, code);
		params.put(DataConst.COL_NAME_PWD, pwd);
		return new BaseRequest(Method.POST, REGISTER_URI, null, params);
	}

}

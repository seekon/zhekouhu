package com.seekon.zhekouhu.func.user;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class ResetPwdMethod extends JSONObjResourceMethod {

	private static final String RESET_PWD_URL = Const.SERVER_APP_URL
			+ "/resetPwd/";

	private String userCode;

	public ResetPwdMethod(String userCode) {
		super();
		this.userCode = userCode;
	}

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, RESET_PWD_URL + userCode, null, null);
	}

}

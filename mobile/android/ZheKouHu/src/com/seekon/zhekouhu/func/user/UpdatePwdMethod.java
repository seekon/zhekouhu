package com.seekon.zhekouhu.func.user;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class UpdatePwdMethod extends JSONObjResourceMethod {

	private static final URI UPDATE_PWD_URI = URI.create(Const.SERVER_APP_URL
			+ "/updatePwd");

	private String uuid;
	private String pwd;
	private String oldPwd;

	public UpdatePwdMethod(String uuid, String pwd, String oldPwd) {
		super();
		this.uuid = uuid;
		this.pwd = pwd;
		this.oldPwd = oldPwd;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_UUID, uuid);
		params.put(DataConst.COL_NAME_PWD, pwd);
		params.put("old_pwd", oldPwd);
		return new BaseRequest(Method.PUT, UPDATE_PWD_URI, null, params);
	}
}

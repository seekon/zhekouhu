package com.seekon.zhekouhu.func.user;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.MultipartRequest;
import com.seekon.zhekouhu.rest.MultipartRestMethod;
import com.seekon.zhekouhu.rest.Request;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;

public class UpdateUserProfileMethod extends
		MultipartRestMethod<JSONObjResource> {

	private static final URI UPDATE_PROFILE_URI = URI.create(Const.SERVER_APP_URL
			+ "/updateUserProfile");

	private UserProfileEntity profile;

	public UpdateUserProfileMethod(UserProfileEntity profile) {
		super();
		this.profile = profile;
	}

	@Override
	protected Request buildRequest() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(DataConst.COL_NAME_UUID, profile.getUuid());
		params.put(DataConst.COL_NAME_NAME, profile.getName());
		params.put(DataConst.COL_NAME_PHONE, profile.getPhone());
		params.put(DataConst.COL_NAME_DELIVER_ADDR, profile.getDeliverAddr());
		params.put(DataConst.COL_NAME_PHOTO, profile.getPhoto().getAliasName());
		
		List<FileEntity> fileEntities = null;
		if (profile.getPhoto().getFileUri() != null) {
			fileEntities = new ArrayList<FileEntity>();
			fileEntities.add(profile.getPhoto());
		}

		return new MultipartRequest(Method.PUT, UPDATE_PROFILE_URI, null, params,
				fileEntities);
	}

	@Override
	protected JSONObjResource parseResponseBody(String responseBody)
			throws Exception {
		return new JSONObjResource(responseBody);
	}
}

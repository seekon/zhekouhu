package com.seekon.zhekouhu.func.user;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_AWARD_TIME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_COIN_VAL;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_COMMENT;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_DATA_ID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_USER_ID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;

import org.json.JSONObject;

import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.Logger;

public class UserAwardEntity extends Entity {

	private static final long serialVersionUID = 8896440966909872L;

	private String awardTime;
	private int coinVal;
	private String userId;
	private String comment;
	private String dataId;

	public UserAwardEntity() {
		super();
	}

	public UserAwardEntity(JSONObject json) {
		this();
		try {
			uuid = json.getString(COL_NAME_UUID);
			userId = json.getString(COL_NAME_USER_ID);
			awardTime = json.getString(COL_NAME_AWARD_TIME);
			dataId = json.getString(COL_NAME_DATA_ID);
			coinVal = json.getInt(COL_NAME_COIN_VAL);
			comment = json.getString(COL_NAME_COMMENT);
		} catch (Exception e) {
			Logger.error(StorefrontEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getAwardTime() {
		return awardTime;
	}

	public void setAwardTime(String awardTime) {
		this.awardTime = awardTime;
	}

	public int getCoinVal() {
		return coinVal;
	}

	public void setCoinVal(int coinVal) {
		this.coinVal = coinVal;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

}

package com.seekon.zhekouhu.func.user;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;

public class UserData extends AbstractSQLiteData<UserEntity> implements
		DataUpdater<UserEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_USER
			+ " (uuid, code, pwd, register_time) values (?, ?, ?, ?)";
	private static final String QUERY_SQL = " select uuid, code, pwd, register_time from "
			+ DataConst.TABLE_USER + " where code = ? ";
	private static final String DEL_SQL = " delete from " + DataConst.TABLE_USER
			+ " where uuid = ? ";
	private static final String UPDATE_PWD_SQL = " update "
			+ DataConst.TABLE_USER + " set pwd = ? where uuid = ? ";

	@Override
	public int save(UserEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}

		for (UserEntity user : data) {
			executeUpdate(UPDATE_SQL, new Object[] { user.getUuid(), user.getCode(),
					user.getPwd(), user.getRegisterTime() });
		}

		return data.length;
	}

	@Override
	protected UserEntity processRow(Cursor cursor) {
		UserEntity user = new UserEntity();

		int i = 0;
		user.setUuid(cursor.getString(i++));
		user.setCode(cursor.getString(i++));
		user.setPwd(cursor.getString(i++));
		user.setRegisterTime(cursor.getLong(i++));
		
		return user;
	}

	public UserEntity getUserByCode(String code) {
		return this.queryOne(QUERY_SQL, new String[] { code });
	}

	public void deleteUser(String uuid) {
		executeUpdate(DEL_SQL, new Object[] { uuid });
	}

	public void updatePwd(String uuid, String pwd) {
		executeUpdate(UPDATE_PWD_SQL, new Object[] { pwd, uuid });
	}
}

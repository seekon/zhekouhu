package com.seekon.zhekouhu.func.user;

import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.func.FuncConst;

/**
 * 用户对象
 * 
 * @author undyliu
 * 
 */
public class UserEntity extends Entity implements Cloneable {

	private static final long serialVersionUID = -4052088250779917423L;

	private String code;
	private String pwd;
	private long registerTime;
	private String type =  FuncConst.VAL_USER_TYPE_CUSTOMER;
	
	private UserProfileEntity profile;
	
	public UserEntity() {
		super();
	}

	public UserEntity(String uuid, String code, String pwd, long registerTime) {
		super(uuid);
		this.code = code;
		this.pwd = pwd;
		this.registerTime = registerTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public long getRegisterTime() {
		return registerTime;
	}

	public void setRegisterTime(long registerTime) {
		this.registerTime = registerTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public UserProfileEntity getProfile() {
		return profile;
	}

	public void setProfile(UserProfileEntity profile) {
		this.profile = profile;
	}

}

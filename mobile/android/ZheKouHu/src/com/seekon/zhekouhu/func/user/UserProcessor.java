package com.seekon.zhekouhu.func.user;

import static com.seekon.zhekouhu.func.FuncConst.VAL_USER_TYPE_CUSTOMER;
import static com.seekon.zhekouhu.func.FuncConst.VAL_USER_TYPE_STORER;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.setting.SettingEntity;
import com.seekon.zhekouhu.func.spi.IUserProcessor;
import com.seekon.zhekouhu.func.store.StoreData;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONArrayResource;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;
import com.seekon.zhekouhu.utils.Logger;

public class UserProcessor implements IUserProcessor {
	private final static String TAG = UserProcessor.class.getSimpleName();

	private UserData userData;
	private EnvData envData;
	private UserProfileData profileData;

	public UserProcessor() {
		super();
		userData = new UserData();
		envData = new EnvData();
		profileData = new UserProfileData();
	}

	@Override
	public ProcessResult<UserEntity> login(final String code, final String pwd) {
		UserEntity user = localLogin(code, pwd);
		if (user != null) {
			return new ProcessResult<UserEntity>(user);
		}
		return this.remoteLogin(code, pwd);
	}

	@Override
	public UserEntity localLogin() {
		Map<String, String> loginSetting = envData.lastLoginEnv();
		if (loginSetting == null) {
			return null;
		}

		String code = loginSetting.get(DataConst.COL_NAME_CODE);
		String pwd = loginSetting.get(DataConst.COL_NAME_PWD);

		return localLogin(code, pwd);
	}

	private UserEntity localLogin(String code, String pwd) {
		UserEntity user = userData.getUserByCode(code);
		if (user != null && user.getPwd().equals(pwd)) {
			boolean ownStores = new StoreData().ownStores(user.getUuid());
			user.setType(ownStores ? VAL_USER_TYPE_STORER : VAL_USER_TYPE_CUSTOMER);
			user.setProfile(profileData.getUserProfile(user.getUuid()));

			RunEnv.getInstance().setUser(user);
			return user;
		}
		return null;
	}

	@Override
	public ProcessResult<UserEntity> remoteLogin(final String code,
			final String pwd) {

		MethodExecuter<UserEntity> executer = new MethodExecuter<UserEntity>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<UserEntity> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				boolean authed = resource.getBoolean(FuncConst.NAME_AUTHED);
				result.setSuccess(authed);

				if (authed) {
					JSONObject jsonUser = resource.getJSONObject(FuncConst.NAME_USER);
					UserEntity user = new UserEntity(
							jsonUser.getString(DataConst.COL_NAME_UUID), code, pwd,
							jsonUser.getLong(DataConst.COL_NAME_REGISTER_TIME));
					user.setType(jsonUser.getString(DataConst.COL_NAME_TYPE));

					if (jsonUser.has(FuncConst.NAME_PROFILE)) {
						JSONObject jsonProfile = jsonUser
								.getJSONObject(FuncConst.NAME_PROFILE);
						if (jsonProfile.has(DataConst.COL_NAME_UUID)) {
							user.setProfile(new UserProfileEntity(jsonProfile));
						}
					}

					if (user.getProfile() != null) {
						profileData.save(new UserProfileEntity[] { user.getProfile() });
					}
					userData.save(new UserEntity[] { user });
					saveLoginEnv(user);
					recordStoreRegistered();

					result.setSuccessObj(user);

					RunEnv.getInstance().setUser(user);
				} else {
					String errorType = resource.getString(FuncConst.NAME_ERROR_TYPE);
					result.setError(errorType, null);
				}
			}
		};

		return executer.execute(new LoginMethod(code, pwd));
	}

	@Override
	public ProcessResult<UserEntity> autoLogin() {
		if (RunEnv.getInstance().getUser() != null) {
			return new ProcessResult<UserEntity>(RunEnv.getInstance().getUser());
		}

		Map<String, String> loginSetting = envData.lastLoginEnv();
		if (loginSetting == null) {
			return new ProcessResult<UserEntity>();
		}

		String code = loginSetting.get(DataConst.COL_NAME_CODE);
		String pwd = loginSetting.get(DataConst.COL_NAME_PWD);

		return this.login(code, pwd);
	}

	@Override
	public boolean logout() {
		try {
			UserEntity user = RunEnv.getInstance().getUser();
			profileData.deleteUserProfile(user.getUuid());
			userData.deleteUser(user.getUuid());
			envData.deleteLoginEnv(user.getCode());
			RunEnv.getInstance().setUser(null);
			RunEnv.getInstance().setSessionId(null);
		} catch (Throwable e) {
			Logger.error(TAG, e.getMessage(), e);
			return false;
		}
		return true;
	}

	@Override
	public ProcessResult<UserEntity> register(final String code, String pwd) {
		MethodExecuter<UserEntity> executer = new MethodExecuter<UserEntity>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<UserEntity> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(FuncConst.NAME_ERROR_TYPE)) {
					String errorType = resource.getString(FuncConst.NAME_ERROR_TYPE);
					if (errorType != null && errorType.trim().length() > 0) {
						result.setSuccess(false);
						result.setError(errorType, null);
						return;
					}
				}

				UserEntity user = new UserEntity();
				user.setCode(code);
				user.setPwd(resource.getString(DataConst.COL_NAME_PWD));
				user.setUuid(resource.getString(DataConst.COL_NAME_UUID));
				user.setRegisterTime(resource.getLong(DataConst.COL_NAME_REGISTER_TIME));
				user.setType(FuncConst.VAL_USER_TYPE_CUSTOMER);

				UserProfileEntity profile = new UserProfileEntity();
				profile.setUuid(user.getUuid());
				profile.setUserId(user.getUuid());
				user.setProfile(profile);

				profileData.save(new UserProfileEntity[] { profile });
				userData.save(new UserEntity[] { user });
				saveLoginEnv(user);
				recordStoreRegistered();

				RunEnv.getInstance().setUser(user);

				result.setSuccess(true);
				result.setSuccessObj(user);
			}
		};
		return executer.execute(new RegisterMethod(code, pwd));
	}

	@Override
	public ProcessResult<Boolean> changePwd(final String uuid, String pwd,
			String oldPwd) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(DataConst.COL_NAME_UUID)) {
					UserEntity user = RunEnv.getInstance().getUser();
					user.setPwd(resource.getString(DataConst.COL_NAME_PWD));

					userData.updatePwd(uuid, user.getPwd());
					saveLoginEnv(user);

					result.setSuccess(true);
					result.setSuccessObj(Boolean.TRUE);
				} else if (resource.has(FuncConst.NAME_ERROR_TYPE)) {
					result.setSuccess(false);
					result.setError(resource.getString(FuncConst.NAME_ERROR_TYPE),
							"原密码错误.");
				} else {
					result.setSuccess(false);
					result.setError(null, "修改密码失败.");
				}
			}
		};
		return executer.execute(new UpdatePwdMethod(uuid, pwd, oldPwd));
	}

	public ProcessResult<Boolean> resetPwd(String code) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(DataConst.COL_NAME_UUID)) {
					result.setSuccess(true);
					result.setSuccessObj(Boolean.TRUE);
				} else if (resource.has(FuncConst.NAME_ERROR_TYPE)) {
					result.setSuccess(false);
					result.setError(resource.getString(FuncConst.NAME_ERROR_TYPE),
							"此帐号还未注册.");
				} else {
					result.setSuccess(false);
					result.setError(null, "重置密码失败.");
				}
			}
		};
		return executer.execute(new ResetPwdMethod(code));
	}

	private void saveLoginEnv(UserEntity user) {
		envData.saveLoginEnv(user);
	}

	private void recordStoreRegistered() {
		SettingEntity setting = new SettingEntity(
				FuncConst.KEY_SETTING_STORE_REGISTERED, "1");
		ProcessorFactory.getSettingProcessor().saveSetting(setting);
	}

	@Override
	public ProcessResult<UserProfileEntity> getUserAward() {
		final UserEntity user = RunEnv.getInstance().getUser();
		if (user == null) {
			return new ProcessResult<UserProfileEntity>(new UserProfileEntity());
		}

		MethodExecuter<UserProfileEntity> executer = new MethodExecuter<UserProfileEntity>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<UserProfileEntity> result) throws Exception {
				UserProfileEntity profile = user.getProfile();
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(DataConst.COL_NAME_UUID)) {
					if (profile == null) {
						profile = new UserProfileEntity();
					}
					profile.setAwardSum(resource.getInt(DataConst.COL_NAME_AWARD_SUM));
					profile.setAwardUsed(resource.getInt(DataConst.COL_NAME_AWARD_USED));
					if (profile.getUuid() != null) {
						profileData.save(new UserProfileEntity[] { profile });
						user.setProfile(profile);
					}
				}
				result.setSuccessObj(profile);
			}
		};
		return executer.execute(new GetUserAwardMethod(user.getUuid()));
	}

	@Override
	public ProcessResult<Boolean> updateUserProfile(
			final UserProfileEntity profile) {
		MethodExecuter<Boolean> executer = new MethodExecuter<Boolean>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<Boolean> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(DataConst.COL_NAME_UUID)) {
					profileData.save(new UserProfileEntity[] { profile });
					UserEntity user = RunEnv.getInstance().getUser();
					if (user != null) {
						user.setProfile(profile);
					}
					result.setSuccessObj(Boolean.TRUE);
				} else {
					result.setSuccessObj(Boolean.FALSE);
				}
			}
		};
		return executer.execute(new UpdateUserProfileMethod(profile));
	}

	@Override
	public ProcessResult<List<UserAwardEntity>> getUserAwardDetail(int offset) {
		final UserEntity user = RunEnv.getInstance().getUser();
		if (user == null) {
			return new ProcessResult<List<UserAwardEntity>>(
					new ArrayList<UserAwardEntity>());
		}
		MethodExecuter<List<UserAwardEntity>> executer = new MethodExecuter<List<UserAwardEntity>>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<List<UserAwardEntity>> result) throws Exception {
				List<UserAwardEntity> data = new ArrayList<UserAwardEntity>();
				JSONArrayResource resource = (JSONArrayResource) mResult.getResource();
				for (int i = 0; i < resource.length(); i++) {
					JSONObject json = resource.getJSONObject(i);
					json.put(DataConst.COL_NAME_USER_ID, user.getUuid());
					data.add(new UserAwardEntity(json));
				}
				result.setSuccessObj(data);
			}
		};
		return executer.execute(new GetUserAwardDetailMethod(user.getUuid(), offset));
	}

}

package com.seekon.zhekouhu.func.user;

import android.database.Cursor;

import com.seekon.zhekouhu.db.AbstractSQLiteData;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.db.DataUpdater;
import com.seekon.zhekouhu.file.FileEntity;

public class UserProfileData extends AbstractSQLiteData<UserProfileEntity>
		implements DataUpdater<UserProfileEntity> {

	private static final String UPDATE_SQL = " insert or replace into "
			+ DataConst.TABLE_USER_PROFILE
			+ " (uuid, user_id, photo, sex, phone, deliver_addr, award_sum, award_used) values (?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String QUERY_SQL = " select uuid, user_id, photo, sex, phone, deliver_addr, award_sum, award_used from "
			+ DataConst.TABLE_USER_PROFILE + " where user_id = ? ";
	private static final String DEL_SQL = " delete from " + DataConst.TABLE_USER_PROFILE
			+ " where userId = ? ";
	
	@Override
	public int save(UserProfileEntity[] data) {
		if (data == null || data.length == 0) {
			return 0;
		}
		for (UserProfileEntity profile : data) {
			executeUpdate(
					UPDATE_SQL,
					new Object[] { profile.getUuid(), profile.getUserId(),
							profile.getPhoto().getAliasName(), profile.getPhone(), profile.getDeliverAddr(),
							profile.getAwardSum(), profile.getAwardUsed() });
		}
		return data.length;
	}

	@Override
	protected UserProfileEntity processRow(Cursor cursor) {
		UserProfileEntity profile = new UserProfileEntity();

		int i = 0;
		profile.setUuid(cursor.getString(i++));
		profile.setUserId(cursor.getString(i++));
		profile.setPhoto(new FileEntity(null, cursor.getString(i++)));
		profile.setSex(cursor.getString(i++));
		profile.setPhone(cursor.getString(i++));
		profile.setDeliverAddr(cursor.getString(i++));
		profile.setAwardSum(cursor.getInt(i++));
		profile.setAwardUsed(cursor.getInt(i++));

		return profile;
	}

	public UserProfileEntity getUserProfile(String userId) {
		return queryOne(QUERY_SQL, new String[] { userId });
	}
	
	public void deleteUserProfile(String userId){
		executeUpdate(DEL_SQL, new String[]{userId});
	}
}

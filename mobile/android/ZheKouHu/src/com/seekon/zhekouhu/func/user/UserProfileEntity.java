package com.seekon.zhekouhu.func.user;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_AWARD_SUM;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_AWARD_USED;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_PHOTO;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_NAME;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_PHONE;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_UUID;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_SEX;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_DELIVER_ADDR;
import static com.seekon.zhekouhu.db.DataConst.COL_NAME_USER_ID;
import org.json.JSONObject;

import com.seekon.zhekouhu.file.FileEntity;
import com.seekon.zhekouhu.func.Entity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.utils.Logger;

public class UserProfileEntity extends Entity {

	private static final long serialVersionUID = -2082369906423190902L;

	private String userId;
	private String name;
	private FileEntity photo;
	private String sex;
	private String phone;
	private String deliverAddr;
	private int awardSum;
	private int awardUsed;

	public UserProfileEntity() {
		super();
	}

	public UserProfileEntity(JSONObject json) {
		this();
		try {
			uuid = json.getString(COL_NAME_UUID);
			userId = json.getString(COL_NAME_USER_ID);
			name = json.getString(COL_NAME_NAME);
			photo = new FileEntity(null, json.getString(COL_NAME_PHOTO));
			sex = json.getString(COL_NAME_SEX);
			phone = json.getString(COL_NAME_PHONE);
			deliverAddr = json.getString(COL_NAME_DELIVER_ADDR);
			awardSum = json.getInt(COL_NAME_AWARD_SUM);
			awardUsed = json.getInt(COL_NAME_AWARD_USED);
		} catch (Exception e) {
			Logger.error(StorefrontEntity.class.getSimpleName(), e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FileEntity getPhoto() {
		return photo;
	}

	public void setPhoto(FileEntity photo) {
		this.photo = photo;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDeliverAddr() {
		return deliverAddr;
	}

	public void setDeliverAddr(String deliverAddr) {
		this.deliverAddr = deliverAddr;
	}

	public int getAwardSum() {
		return awardSum;
	}

	public void setAwardSum(int awardSum) {
		this.awardSum = awardSum;
	}

	public int getAwardUsed() {
		return awardUsed;
	}

	public void setAwardUsed(int awardUsed) {
		this.awardUsed = awardUsed;
	}

}

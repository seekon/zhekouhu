package com.seekon.zhekouhu.func.version;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.rest.BaseRequest;
import com.seekon.zhekouhu.rest.JSONObjResourceMethod;
import com.seekon.zhekouhu.rest.Method;
import com.seekon.zhekouhu.rest.Request;

public class GetNewestVersionMethod extends JSONObjResourceMethod {

	private static final String GET_NEWEST_VERSION_URL = Const.SERVER_APP_URL
			+ "/getNewestVersion";

	@Override
	protected Request buildRequest() {
		return new BaseRequest(Method.GET, GET_NEWEST_VERSION_URL, null, null);
	}

}

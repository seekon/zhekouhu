package com.seekon.zhekouhu.func.version;

import static com.seekon.zhekouhu.db.DataConst.COL_NAME_VALUE;

import com.seekon.zhekouhu.func.MethodExecuter;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.spi.IVersionProcessor;
import com.seekon.zhekouhu.rest.RestMethodResult;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;

public class VersionProcessor implements IVersionProcessor {

	@Override
	public ProcessResult<String> getNewestVersion() {
		MethodExecuter<String> executer = new MethodExecuter<String>() {

			@Override
			public void doSuccess(RestMethodResult<?> mResult,
					ProcessResult<String> result) throws Exception {
				JSONObjResource resource = (JSONObjResource) mResult.getResource();
				if (resource.has(COL_NAME_VALUE)) {
					result.setSuccessObj(resource.getString(COL_NAME_VALUE));
				}
			}
		};
		return executer.execute(new GetNewestVersionMethod());
	}
}

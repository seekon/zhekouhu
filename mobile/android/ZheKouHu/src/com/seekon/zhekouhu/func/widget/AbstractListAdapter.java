package com.seekon.zhekouhu.func.widget;

import java.util.List;

import android.content.Context;
import android.widget.BaseAdapter;

public abstract class AbstractListAdapter<T> extends BaseAdapter {

	protected Context context;
	protected List<T> itemList;

	public AbstractListAdapter(Context context, List<T> itemList) {
		super();
		this.context = context;
		this.itemList = itemList;
	}

	@Override
	public int getCount() {
		return itemList == null ? 0 : itemList.size();
	}

	@Override
	public Object getItem(int position) {
		return itemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void updateData(List<T> itemList) {
		this.itemList = itemList;
		this.notifyDataSetChanged();
	}
}

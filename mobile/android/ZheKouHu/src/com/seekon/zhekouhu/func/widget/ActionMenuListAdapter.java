package com.seekon.zhekouhu.func.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.discount.ActionMenu;

public class ActionMenuListAdapter extends AbstractListAdapter<ActionMenu> {

	public ActionMenuListAdapter(Context context, List<ActionMenu> itemList) {
		super(context, itemList);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(
					R.layout.action_menu_item, null);

			holder = new ViewHolder();
			holder.nameView = (TextView) convertView.findViewById(R.id.t_name);
			holder.imageView = (ImageView) convertView.findViewById(R.id.img_menu);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		ActionMenu menu = (ActionMenu) getItem(position);
		holder.nameView.setText(menu.getName());
		holder.imageView.setImageResource(menu.getIconResId());
		
		return convertView;
	}

	class ViewHolder {
		TextView nameView;
		ImageView imageView;
	}

}

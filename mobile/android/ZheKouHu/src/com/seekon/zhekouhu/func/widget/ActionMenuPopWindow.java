package com.seekon.zhekouhu.func.widget;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.discount.ActionMenu;

public class ActionMenuPopWindow extends PopupWindow {

	private Context context;
	private  View mMenuView;

	public ActionMenuPopWindow(Context context, List<ActionMenu> menuList,
			OnItemClickListener onItemClickListener) {
		super(context);
		
		this.context = context;
		initView(menuList, onItemClickListener);
	}

	private void initView(List<ActionMenu> menuList, OnItemClickListener listener) {
		mMenuView = LayoutInflater.from(context).inflate(
				R.layout.pop_action_menu, null);

		// 设置SelectPicPopupWindow的View
		this.setContentView(mMenuView);
		// 设置SelectPicPopupWindow弹出窗体的宽
		this.setWidth(LayoutParams.WRAP_CONTENT);
		// 设置SelectPicPopupWindow弹出窗体的高
		this.setHeight(LayoutParams.WRAP_CONTENT);
		// 设置SelectPicPopupWindow弹出窗体可点击
		this.setFocusable(true);
		// 设置SelectPicPopupWindow弹出窗体动画效果
		// this.setAnimationStyle(R.style.AnimBottom);
		// 实例化一个ColorDrawable颜色为半透明
		// ColorDrawable dw = new ColorDrawable(0xc0000000);
		// 设置SelectPicPopupWindow弹出窗体的背景
		// this.setBackgroundDrawable(dw);
		// mMenuView添加OnTouchListener监听判断获取触屏位置如果在选择框外面则销毁弹出框
		mMenuView.setOnTouchListener(new OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {

				int height = mMenuView.findViewById(R.id.pop_layout).getTop();
				int y = (int) event.getY();
				if (event.getAction() == MotionEvent.ACTION_UP) {
					if (y < height) {
						dismiss();
					}
				}
				return true;
			}
		});

		ListView listMenu = (ListView) mMenuView.findViewById(R.id.list_menu);
		listMenu.setAdapter(new ActionMenuListAdapter(context, menuList));
		listMenu.setOnItemClickListener(listener);
	}
	
	public void updateMenuList(List<ActionMenu> menuList){
		ListView listMenu = (ListView) mMenuView.findViewById(R.id.list_menu);
		ActionMenuListAdapter adapter = (ActionMenuListAdapter) listMenu.getAdapter();
		if (adapter == null) {
			listMenu.setAdapter(new ActionMenuListAdapter(context, menuList));
		}else{
			adapter.updateData(menuList);
			listMenu.setAdapter(new ActionMenuListAdapter(context, menuList));
		}
	}
}

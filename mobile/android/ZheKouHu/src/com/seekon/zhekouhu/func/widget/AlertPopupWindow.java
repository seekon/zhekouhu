package com.seekon.zhekouhu.func.widget;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.seekon.zhekouhu.R;

public class AlertPopupWindow extends AbstractPopupWindow {

	public AlertPopupWindow(Context context, String title, String message,
			OnClickListener itemsOnClick) {
		super(context, R.layout.pop_alert_view);

		if (!TextUtils.isEmpty(title)) {
			TextView titleView = (TextView) mMenuView.findViewById(R.id.t_title);
			titleView.setText(title);
		}

		if (!TextUtils.isEmpty(message)) {
			TextView messageView = (TextView) mMenuView.findViewById(R.id.t_message);
			messageView.setText(message);
		}

		View yesView = mMenuView.findViewById(R.id.t_yes);
		View noView = mMenuView.findViewById(R.id.t_no);

		// 设置按钮监听
		yesView.setOnClickListener(itemsOnClick);
		noView.setOnClickListener(itemsOnClick);
	}

}

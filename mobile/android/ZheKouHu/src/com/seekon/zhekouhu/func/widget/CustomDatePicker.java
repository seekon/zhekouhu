package com.seekon.zhekouhu.func.widget;

import java.lang.reflect.Field;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.NumberPicker;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.utils.Logger;

public class CustomDatePicker extends android.widget.DatePicker {
	private static final String TAG = CustomDatePicker.class.getSimpleName();

	public CustomDatePicker(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		changeSelectionDivider();
	}

	public CustomDatePicker(Context context) {
		super(context);
		changeSelectionDivider();
	}

	public CustomDatePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		changeSelectionDivider();
	}

	private void changeSelectionDivider() {
		Class<?> internalRID = null;
		try {
			internalRID = Class.forName("com.android.internal.R$id");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		Field month = null;
		try {
			month = internalRID.getField("month");
		} catch (NoSuchFieldException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		NumberPicker npMonth = null;
		try {
			npMonth = (NumberPicker) findViewById(month.getInt(null));
		} catch (IllegalArgumentException e) {
			Logger.debug(TAG, e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		Field day = null;
		try {
			day = internalRID.getField("day");
		} catch (NoSuchFieldException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		NumberPicker npDay = null;
		try {
			npDay = (NumberPicker) findViewById(day.getInt(null));
		} catch (IllegalArgumentException e) {
			Logger.debug(TAG, e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		Field year = null;
		try {
			year = internalRID.getField("year");
		} catch (NoSuchFieldException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		NumberPicker npYear = null;
		try {
			npYear = (NumberPicker) findViewById(year.getInt(null));
		} catch (IllegalArgumentException e) {
			Logger.debug(TAG, e.getMessage(), e);
		} catch (IllegalAccessException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		Class<?> numberPickerClass = null;
		try {
			numberPickerClass = Class.forName("android.widget.NumberPicker");
		} catch (ClassNotFoundException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		Field selectionDivider = null;
		try {
			selectionDivider = numberPickerClass
					.getDeclaredField("mSelectionDivider");
		} catch (NoSuchFieldException e) {
			Logger.debug(TAG, e.getMessage(), e);
		}

		Drawable d = getResources().getDrawable(R.drawable.np_selection_divider);
		try {
			selectionDivider.setAccessible(true);
			selectionDivider.set(npMonth, d);
			selectionDivider.set(npDay, d);
			selectionDivider.set(npYear, d);
		} catch (Exception e) {
			Logger.debug(TAG, e.getMessage(), e);
			e.printStackTrace();
		}
	}
}
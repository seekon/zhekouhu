package com.seekon.zhekouhu.func.widget;

import java.util.Date;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.DateIndexedEntity;
import com.seekon.zhekouhu.utils.DateUtils;

public abstract class DateIndexedListAdapter<T> extends
		AbstractListAdapter<DateIndexedEntity> {

	public DateIndexedListAdapter(Context context,
			List<DateIndexedEntity> dataList) {
		super(context, dataList);
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		ViewHolder holder = null;
		if (view == null) {
			holder = new ViewHolder();

			view = LayoutInflater.from(context).inflate(
					R.layout.date_indexed_list_item, null, false);
			holder.view = view;
			holder.subItemListView = (ListView) view.findViewById(R.id.sub_item_list);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		DateIndexedEntity dateIndexedEntity = (DateIndexedEntity) getItem(position);

		Date date = dateIndexedEntity.getDate();
		int day = DateUtils.getDayOfMoth(date);
		TextView dateView = (TextView) view.findViewById(R.id.item_day_field);
		if (day < 10) {
			dateView.setText( "0" + day + "");
		}else{
			dateView.setText( day + "");
		}
		dateView.getPaint().setFakeBoldText(true);// TODO:使用样式表来处理

		TextView monthView = (TextView) view.findViewById(R.id.item_month_field);
		int month = DateUtils.getMonth(date);
		if (month < 10) {
			monthView.setText("日0" + DateUtils.getMonth(date) + "月");
		}else{
			monthView.setText("日" + DateUtils.getMonth(date) + "月");
		}
		monthView.getPaint().setFakeBoldText(true);

		int itemCount = dateIndexedEntity.getItemCount();
		((TextView) view.findViewById(R.id.item_count_field)).setText(itemCount
				+ "笔");

		initSubItemListView(holder.subItemListView,
				dateIndexedEntity.getSubItemList());

		return view;
	}

	public abstract void initSubItemListView(ListView subItemListView,
			List<T> subItemDataList);

	class ViewHolder {
		View view;
		ListView subItemListView;
	}
}
package com.seekon.zhekouhu.func.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

public class HorizontalNestedGridView extends GridView {

	public HorizontalNestedGridView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}

	public HorizontalNestedGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public HorizontalNestedGridView(Context context) {
		super(context);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
				MeasureSpec.AT_MOST);
		super.onMeasure(expandSpec, heightMeasureSpec);
	}
}

package com.seekon.zhekouhu.func.widget;

import com.seekon.zhekouhu.file.FileEntity;

public interface ImageViewClickDelegate {

	public void doClick(FileEntity imageFile);

}

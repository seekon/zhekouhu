package com.seekon.zhekouhu.func.widget;

import android.content.Context;
import android.content.Intent;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.store.LocationEntity;
import com.seekon.zhekouhu.utils.LocationUtils;
import com.seekon.zhekouhu.utils.Logger;

public class MyLocationListener implements BDLocationListener {

	private static final String TAG = MyLocationListener.class.getSimpleName();

	private Context context;

	public MyLocationListener(Context context) {
		super();
		this.context = context;
	}

	@Override
	public void onReceiveLocation(BDLocation location) {
		if (location == null || location.getCity() == null
				|| location.getLatitude() < 0 || location.getLongitude() < 0) {
			return;
		}

		LocationEntity loc = new LocationEntity();
		loc.setLat(location.getLatitude());
		loc.setLng(location.getLongitude());
		loc.setCityName(location.getCity());
		RunEnv.getInstance().setLocation(loc);

		Intent intent = new Intent(Const.KEY_BROAD_LOCATION);
		intent.putExtra(Const.DATA_BROAD_LOCATION, loc);
		context.sendBroadcast(intent);

		Logger.debug(TAG, location.getCity());
		Logger.debug(TAG, location.getAddrStr());
		Logger.debug(TAG, "lat:" + location.getLatitude());
		Logger.debug(TAG, "lng:" + location.getLongitude());

		LocationUtils.stopLocate();// 定位成功后就停止定位
	}

	@Override
	public void onReceivePoi(BDLocation poiLocation) {

	}

}
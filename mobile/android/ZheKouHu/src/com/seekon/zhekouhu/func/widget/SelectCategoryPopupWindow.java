package com.seekon.zhekouhu.func.widget;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.category.CategoryEntity;
import com.seekon.zhekouhu.func.store.widget.CategoryChooseAdapter;

public class SelectCategoryPopupWindow extends AbstractPopupWindow {

	private CategoryEntity selectedCategory;

	private CategoryChooseAdapter adapter = null;

	public SelectCategoryPopupWindow(final Context context,
			CategoryEntity seleCategory, final List<CategoryEntity> categoryList,
			final OnItemClickListener itemClickListener) {
		super(context, R.layout.pop_select_category);

		final ListView listView = (ListView) mMenuView.findViewById(R.id.listView);

		this.selectedCategory = seleCategory;
		adapter = new CategoryChooseAdapter(context, categoryList, selectedCategory);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				selectedCategory = categoryList.get(position);
				adapter.setSelectedCategory(selectedCategory);
				itemClickListener.onItemClick(parent, listView, position, id);
			}
		});
	}
}

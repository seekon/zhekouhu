package com.seekon.zhekouhu.func.widget;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.store.CityEntity;
import com.seekon.zhekouhu.func.store.widget.CityChooseAdapter;

public class SelectCityPopupWindow extends AbstractPopupWindow {

	private CityEntity selectedCity;
	private CityChooseAdapter adapter;

	public SelectCityPopupWindow(Context context, CityEntity selectedCity,
			final List<CityEntity> cities, final OnItemClickListener itemClickListener) {
		super(context, R.layout.pop_select_city);

		final ListView listView = (ListView) mMenuView.findViewById(R.id.listView);

		this.selectedCity = selectedCity;
		adapter = new CityChooseAdapter(context, cities, selectedCity);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				SelectCityPopupWindow.this.selectedCity = cities.get(position);
				adapter.setSelectedCity(SelectCityPopupWindow.this.selectedCity);
				itemClickListener.onItemClick(parent, listView, position, id);
			}
		});

	}

}

package com.seekon.zhekouhu.func.widget;

import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.utils.DateUtils;

public class SelectDatePopupWindow extends AbstractPopupWindow {

	private Date mDate;
	private PopupWindowCallback callback;

	public SelectDatePopupWindow(Context context, Date date,
			final PopupWindowCallback callback) {
		super(context, R.layout.pop_select_date);

		this.mDate = date;
		this.callback = callback;

		int year = DateUtils.getYear(mDate);
		int month = DateUtils.getMonth(mDate) - 1;
		int day = DateUtils.getDayOfMoth(mDate);

		DatePicker datePicker = (DatePicker) mMenuView
				.findViewById(R.id.datePicker);
		datePicker.init(year, month, day, new OnDateChangedListener() {

			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				Calendar calendar = Calendar.getInstance();
				calendar.set(year, monthOfYear, dayOfMonth);
				mDate = calendar.getTime();
			}
		});
	}

	@Override
	public void dismiss() {
		if (callback != null) {
			callback.doCallback(mDate);
		}
		super.dismiss();
	}

	public interface PopupWindowCallback {
		public void doCallback(Date date);
	}
}

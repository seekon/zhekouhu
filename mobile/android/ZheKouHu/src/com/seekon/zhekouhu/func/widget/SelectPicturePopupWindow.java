package com.seekon.zhekouhu.func.widget;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;

import com.seekon.zhekouhu.R;

public class SelectPicturePopupWindow extends AbstractPopupWindow {

	private View cameraView;
	private View photoView;

	public SelectPicturePopupWindow(final Activity context,
			OnClickListener itemsOnClick) {
		super(context, R.layout.pop_select_pic);

		cameraView = mMenuView.findViewById(R.id.v_camera);
		photoView = mMenuView.findViewById(R.id.v_photo);

		// 设置按钮监听
		cameraView.setOnClickListener(itemsOnClick);
		photoView.setOnClickListener(itemsOnClick);
	}

}

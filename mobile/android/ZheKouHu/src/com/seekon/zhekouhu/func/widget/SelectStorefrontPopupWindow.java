package com.seekon.zhekouhu.func.widget;

import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;
import android.content.Context;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.store.StoreEntity;
import com.seekon.zhekouhu.func.store.StorefrontEntity;
import com.seekon.zhekouhu.func.store.widget.StorefrontChooseAdapter;

public class SelectStorefrontPopupWindow extends AbstractPopupWindow {

	private List<StorefrontEntity> selectedStorefronts;
	private StickyListHeadersListView stickyList;
	private StorefrontChooseAdapter adapter = null;
	private PopupWindowCallback callback;

	public SelectStorefrontPopupWindow(Context context,
			List<StorefrontEntity> selectedStorefrontList,
			List<StoreEntity> storeList, final PopupWindowCallback callback) {
		super(context, R.layout.pop_select_storefront);

		this.selectedStorefronts = selectedStorefrontList;
		this.callback = callback;

		stickyList = (StickyListHeadersListView) mMenuView
				.findViewById(R.id.header_listView);
		adapter = new StorefrontChooseAdapter(selectedStorefrontList, storeList,
				context);
		stickyList.setAdapter(adapter);
	}

	@Override
	public void dismiss() {
		selectedStorefronts = adapter.getSelectStorefronts();
		if (callback != null) {
			callback.doCallback(selectedStorefronts);
		}
		super.dismiss();
	}

	public interface PopupWindowCallback {
		public void doCallback(List<StorefrontEntity> selectedDataList);
	}
}

package com.seekon.zhekouhu.func.widget;

import android.content.Context;
import android.view.View.OnClickListener;

import com.seekon.zhekouhu.R;

public class SharePopupWindow extends AbstractPopupWindow {

	public SharePopupWindow(Context context, OnClickListener itemsOnClick) {
		super(context, R.layout.pop_share);

		mMenuView.findViewById(R.id.img_weixin).setOnClickListener(itemsOnClick);
		mMenuView.findViewById(R.id.img_friends).setOnClickListener(itemsOnClick);
		mMenuView.findViewById(R.id.img_sina).setOnClickListener(itemsOnClick);
	}

}

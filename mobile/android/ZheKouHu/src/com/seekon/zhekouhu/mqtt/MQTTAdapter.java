package com.seekon.zhekouhu.mqtt;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.android.service.MqttAndroidClient.Ack;
import org.eclipse.paho.android.service.MqttService;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import android.content.Context;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;

import com.seekon.zhekouhu.Const;
import com.seekon.zhekouhu.func.AsyncProcessorTask;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessResult;
import com.seekon.zhekouhu.func.RunEnv;
import com.seekon.zhekouhu.func.ProcessResult.Error;
import com.seekon.zhekouhu.func.user.UserEntity;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.ProcessorTaskCallback;
import com.seekon.zhekouhu.utils.ViewUtils;

public class MQTTAdapter {

	private static final String TOPIC_COMMON = "msg/common/#";// 订阅的公共主题

	private static final String TOPIC_DEVICE = "msg/%s/#";// 订阅的特定设备相关的主题格式

	public static void start(final Context context, MqttService service) {

		final String mDeviceId = String.format(ViewUtils.DEVICE_ID_FORMAT,
				Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));

		final String devId = PreferenceManager.getDefaultSharedPreferences(
				context.getApplicationContext()).getString(
				FuncConst.KEY_SHARE_PREFER_DEV_ID, null);
		if (devId == null) {// 若未注册，则注册设备，收到欢迎消息
			new Handler().post(new Runnable() {

				@Override
				public void run() {
					AsyncProcessorTask<Boolean> task = new AsyncProcessorTask<Boolean>(
							context, new ProcessorTaskCallback<Boolean>() {
								@Override
								public ProcessResult<Boolean> doInBackground() {
									return ProcessorFactory.getSettingProcessor().registerDevice(
											mDeviceId);
								}

								@Override
								public void onSuccess(Boolean result) {
									if (result) {
										PreferenceManager
												.getDefaultSharedPreferences(
														context.getApplicationContext())
												.edit()
												.putString(FuncConst.KEY_SHARE_PREFER_DEV_ID, mDeviceId)
												.commit();
									}
								}

								@Override
								public void onFailed(Error error) {
									System.err.println(error);
								}
							});
					task.execute();
				}
			});
		}

		MqttClientPersistence persistence = null;
		try {
			persistence = new MqttDefaultFilePersistence(context.getCacheDir()
					.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
			persistence = new MemoryPersistence();
		}

		MqttAndroidClient client = new MqttAndroidClient(context, Const.MQTT_URL,
				mDeviceId, persistence, Ack.AUTO_ACK);
		client.setCallback(new PushCallback(context));
		if (service != null) {
			client.setMqttService(service);
		} else {
			client.setServiceName(PushService.class.getName());
		}

		MqttConnectOptions opts = new MqttConnectOptions();
		opts.setCleanSession(false);
		opts.setUserName(Const.MQTT_USER_NAME);
		opts.setPassword(Const.MQTT_PASSWORD.toCharArray());
		opts.setKeepAliveInterval(50);
		try {
			client.connect(opts, null, new IMqttActionListener() {

				@Override
				public void onSuccess(IMqttToken token) {
					List<String> topics = new ArrayList<String>();
					topics.add(TOPIC_COMMON);
					topics.add(String.format(Locale.US, TOPIC_DEVICE, mDeviceId));
					int[] qos = null;
					UserEntity user = RunEnv.getInstance().getUser();
					if (user != null) {
						topics.add(String.format(Locale.US, TOPIC_DEVICE, user.getUuid()));
						qos = new int[] { 2, 2, 2 };
					}else{
						qos = new int[] { 2, 2 };
					}
					try {
						token.getClient().subscribe(
								topics.toArray(new String[topics.size()]), qos);
					} catch (MqttException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(IMqttToken token, Throwable e) {
					e.printStackTrace();
				}
			});

		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

	public static void stop() {

	}
}

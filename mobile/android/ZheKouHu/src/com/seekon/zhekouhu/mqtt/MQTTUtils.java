package com.seekon.zhekouhu.mqtt;

import android.app.ActivityManager;
import android.content.Context;

public class MQTTUtils {

	public static void startMqttService(Context context) {
		if (!serviceIsRunning(context.getApplicationContext())) {
			MQTTAdapter.start(context.getApplicationContext(), null);
		} else {
			// //MqttService.actionKeepalive(context.getApplicationContext());
		}
	}

	private static boolean serviceIsRunning(Context context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			// Log.d("MQTTUtils", service.service.getClassName());
			if (PushService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}

package com.seekon.zhekouhu.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.NotificationCompat;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.activity.SplashActivity;
import com.seekon.zhekouhu.activity.more.MessageDetailActivity;
import com.seekon.zhekouhu.activity.more.VersionActivity;
import com.seekon.zhekouhu.db.DataConst;
import com.seekon.zhekouhu.func.FuncConst;
import com.seekon.zhekouhu.func.ProcessorFactory;
import com.seekon.zhekouhu.func.message.MessageEntity;
import com.seekon.zhekouhu.func.setting.SettingEntity;

public class PushCallback implements MqttCallback {

	private Context context;

	public PushCallback(Context context) {
		this.context = context;
	}

	@Override
	public void connectionLost(Throwable cause) {
		// if(context instanceof PushService){
		// ((PushService)context).connectionLostCallback();
		// }
	}

	@Override
	public void messageArrived(String topic, MqttMessage mqttMsg)
			throws Exception {
		SettingEntity messageSetting = ProcessorFactory.getSettingProcessor()
				.getSetting(FuncConst.KEY_SETTING_MESSAGE_REMIND);
		if (messageSetting != null && "0".equals(messageSetting.getValue())) {
			return;
		}

		MessageEntity message = null;
		try {
			JSONObject jsonMsg = new JSONObject(new String(mqttMsg.getPayload()));
			message = new MessageEntity(jsonMsg);
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}

		if (message == null
				|| message.getUuid() == null
				|| ProcessorFactory.getMessageProcessor().messageExists(
						message.getUuid())) {
			return;
		}

		Resources r = context.getResources();
		Class<?> clazz = null;
		String title = null;
		if (message.getType().equals(FuncConst.MSG_TYPE_VERSION)) {
			title = r.getString(R.string.label_version_update);
			clazz = VersionActivity.class;
		} else {
			title = r.getString(R.string.title_sys_message);
			clazz = MessageDetailActivity.class;
		}

		if (title == null) {
			return;
		}

		try {
			ProcessorFactory.getMessageProcessor().saveMessage(message);
		} catch (Throwable e) {
			e.printStackTrace();
			return;
		}

		Intent intent = new Intent(context, SplashActivity.class);
		intent.putExtra(FuncConst.NAME_ACTIVITY_CLASS, clazz);
		intent.putExtra(DataConst.COL_NAME_UUID, message.getUuid());
		intent.putExtra(FuncConst.NAME_POP_TO_ROOT, true);

		PendingIntent pi = PendingIntent.getActivity(context, 0, intent, 0);

		Notification notification = new NotificationCompat.Builder(context)
				.setTicker(title).setSmallIcon(R.drawable.icon_28)
				.setContentTitle(title).setContentText(message.getContent())
				.setContentIntent(pi).setAutoCancel(true).build();
		notification.defaults = Notification.DEFAULT_ALL;

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(0, notification);
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {

	}

}

package com.seekon.zhekouhu.mqtt;

import org.eclipse.paho.android.service.MqttService;

import android.content.Intent;

public class PushService extends MqttService {

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		System.out.println("###开始启动服务:" + intent);
		if (intent == null && isOnline()) {// 重新启动服务，连接重连
			// we have an internet connection - have another try at
			// connecting
			MQTTAdapter.start(getApplicationContext(), this);
		}

		return super.onStartCommand(intent, flags, startId);
	}
}
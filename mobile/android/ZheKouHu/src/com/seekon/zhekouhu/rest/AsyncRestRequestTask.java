package com.seekon.zhekouhu.rest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.rest.resource.Resource;
import com.seekon.zhekouhu.utils.Logger;

public class AsyncRestRequestTask<T extends Resource> extends
		AsyncTask<Void, Void, RestMethodResult<T>> {

	private final static String TAG = AsyncRestRequestTask.class.getSimpleName();

	private Context context;

	private AbstractRestTaskCallback<T> callback;

	private ProgressDialog progressDialog;

	public AsyncRestRequestTask(AbstractRestTaskCallback<T> callback) {
		this(Application.getAppContext(), callback);
	}

	public AsyncRestRequestTask(Context context,
			AbstractRestTaskCallback<T> callback) {
		super();
		this.context = context;
		this.callback = callback;
		if (callback == null) {
			throw new RuntimeException("AsyncRestRequestTask构造函数必须传递callback参数.");
		}
	}

	@Override
	protected void onPreExecute() {
		progressDialog = ProgressDialog
				.show(context, "",
						context.getString(R.string.default_progress_status_message), true,
						true);
		super.onPreExecute();
	}

	@Override
	protected RestMethodResult<T> doInBackground(Void... params) {
		try {
			return callback.doInBackground();
		} catch (Throwable e) {
			Logger.warn(TAG, e.getMessage());
		}
		return new RestMethodResult<T>(RestStatus.RUNTIME_ERROR, Application
				.getAppContext().getString(R.string.runtime_error), null);
	}

	@Override
	protected void onPostExecute(RestMethodResult<T> result) {
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		int status = result.getStatusCode();
		if (status == RestStatus.SC_OK) {
			callback.onSuccess(result);
		} else {
			String message = result.getStatusMsg();
			if (message == null || message.trim().length() == 0) {
				message = Application.getAppContext().getString(R.string.runtime_error);
			}
			callback.onFailed(message);
		}
	}

	@Override
	protected void onCancelled() {
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		callback.onCancelled();
	}

}

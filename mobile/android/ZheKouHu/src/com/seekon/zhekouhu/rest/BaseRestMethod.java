package com.seekon.zhekouhu.rest;

import com.seekon.zhekouhu.rest.resource.Resource;

public abstract class BaseRestMethod<T extends Resource> extends
		AbstractRestMethod<T> {

	@Override
	protected Response doRequest(Request request) {
		RestClient client = new BaseRestClient();
		return client.execute(request);
	}

}

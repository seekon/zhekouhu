package com.seekon.zhekouhu.rest;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.rest.resource.JSONArrayResource;
import com.seekon.zhekouhu.utils.Logger;

public abstract class JSONArrayResourceMethod extends
		BaseRestMethod<JSONArrayResource> {
	private final static String TAG = JSONArrayResourceMethod.class
			.getSimpleName();

	public JSONArrayResourceMethod() {
		super();
	}

	@Override
	protected Context getContext() {
		return Application.getAppContext();
	}

	@Override
	protected RestMethodResult<JSONArrayResource> buildResult(Response response)
			throws Exception {
		return RestUtils.processResultErrorMessage(super.buildResult(response));
	}

	@Override
	protected JSONArrayResource parseResponseBody(String responseBody)
			throws Exception {
		JSONArrayResource resource = null;
		try {
			resource = new JSONArrayResource(responseBody);
		} catch (JSONException e) {
			Logger.warn(TAG, e.getMessage());
			try {
				JSONObject jsonObj = new JSONObject(responseBody);
				resource = new JSONArrayResource();
				resource.put(jsonObj);
			} catch (JSONException ee) {
				Logger.warn(TAG, ee.getMessage());
				throw e;
			}
		}

		return resource;
	}
}

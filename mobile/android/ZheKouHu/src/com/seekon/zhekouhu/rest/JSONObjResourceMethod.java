package com.seekon.zhekouhu.rest;

import android.content.Context;

import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.rest.resource.JSONObjResource;

public abstract class JSONObjResourceMethod extends
		BaseRestMethod<JSONObjResource> {

	public JSONObjResourceMethod() {
		super();
	}

	@Override
	protected Context getContext() {
		return Application.getAppContext();
	}

	@Override
	protected RestMethodResult<JSONObjResource> buildResult(Response response)
			throws Exception {
		return RestUtils.processResultErrorMessage(super.buildResult(response));
	}

	@Override
	protected JSONObjResource parseResponseBody(String responseBody)
			throws Exception {
		return new JSONObjResource(responseBody);
	}
}

package com.seekon.zhekouhu.rest;

import java.net.URI;
import java.util.List;
import java.util.Map;

import com.seekon.zhekouhu.file.FileEntity;

/**
 * 支持多附件的请求，主要用于post请求
 * 
 * @author undyliu
 * 
 */
public class MultipartRequest extends BaseRequest {

	private List<FileEntity> fileEntities = null;

	public MultipartRequest(Method method, URI requestUri,
			Map<String, List<String>> headers, Map<String, String> parameters,
			List<FileEntity> fileEntities) {
		super(method, requestUri, headers, parameters);
		this.fileEntities = fileEntities;
	}

	public MultipartRequest(URI requestUri, Map<String, List<String>> headers,
			Map<String, String> parameters, List<FileEntity> fileEntities) {
		this(Method.POST, requestUri, headers, parameters, fileEntities);
	}

	public List<FileEntity> getFileEntities() {
		return fileEntities;
	}

}

package com.seekon.zhekouhu.rest;

import android.content.Context;

import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.rest.resource.Resource;

public abstract class MultipartRestMethod<T extends Resource> extends
		AbstractRestMethod<T> {

	public MultipartRestMethod() {
		super();
	}

	@Override
	protected Context getContext() {
		return Application.getAppContext();
	}

	@Override
	protected Response doRequest(Request request) {
		RestClient client = new MultipartClient();
		return client.execute(request);
	}

	@Override
	protected RestMethodResult<T> buildResult(Response response) throws Exception {
		return RestUtils.processResultErrorMessage(super.buildResult(response));
	}
}

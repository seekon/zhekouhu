package com.seekon.zhekouhu.rest;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import com.sina.weibo.sdk.utils.LogUtil;

public abstract class RestClient {

	static final int MAX_CONNECTIONS = 3;

	public Response execute(Request request) {
		return execute(request, 0);
	}

	private Response execute(Request request, int failures) {
		HttpURLConnection conn = null;
		Response response = null;
		int status = -1;
		try {

			URL url = request.getRequestUri().toURL();
			conn = (HttpURLConnection) url.openConnection();

			// if (Build.VERSION.SDK != null && Build.VERSION.SDK_INT > 13) {
			// // conn.setRequestProperty("Connection", "close");
			// }
			if (failures > 0 && failures <= MAX_CONNECTIONS) {
				conn.setRequestProperty("Connection", "close");
			}

			processHttpConnection(conn, request);

			status = conn.getResponseCode();

			if (status == 200) {
				BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
				byte[] body = null;

				Map<String, List<String>> headers = conn.getHeaderFields();
				if (headers.containsKey("Content-Encoding")) {
					List<String> values = headers.get("Content-Encoding");
					if (values != null && values.size() > 0) {
						String contentEncoding = values.get(0);
						if (contentEncoding != null
								&& contentEncoding.equalsIgnoreCase("gzip")) {
							GZIPInputStream gzipIn = new GZIPInputStream(in);
							body = RestUtils.readStream(gzipIn);
						}
					}
				}
				if (body == null) {
					body = RestUtils.readStream(in);
				}
				response = new Response(conn.getResponseCode(), conn.getHeaderFields(),
						body);
			} else {
				response = new Response(status, conn.getHeaderFields(), new byte[] {});
			}

		} catch (EOFException e) {
			LogUtil.d(RestClient.class.getName(), "failures number :" + failures);
			LogUtil.e(RestClient.class.getName(), e.getLocalizedMessage());
			if (failures <= MAX_CONNECTIONS) {
				if (conn != null) {
					conn.disconnect();
				}
				conn = null;
				return execute(request, failures + 1);
			}
		} catch (Exception e) {
			LogUtil.e(RestClient.class.getName(), e.getLocalizedMessage());
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				conn.disconnect();
				conn = null;
			}
		}

		if (response == null) {
			response = new Response(status, new HashMap<String, List<String>>(),
					new byte[] {});
		}

		return response;
	}

	protected abstract void processHttpConnection(HttpURLConnection conn,
			Request request) throws Exception;
}

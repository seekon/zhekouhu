package com.seekon.zhekouhu.rest;

import com.seekon.zhekouhu.rest.resource.Resource;

public interface RestMethod<T extends Resource> {

	public RestMethodResult<T> execute();
}

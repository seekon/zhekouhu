package com.seekon.zhekouhu.rest.sercurity;

import com.seekon.zhekouhu.utils.Logger;

public class AuthorizationManager {

	private static AuthorizationManager mInstance = null;

	private static final String DEFAULT_SIGNER_CLASS = "com.seekon.zhekouhu.func.DefaultRequestSigner";

	private RequestSigner signer;

	public static AuthorizationManager getInstance() {
		if (mInstance == null) {
			mInstance = new AuthorizationManager();
		}
		return mInstance;
	}

	public RequestSigner getDefaultRequestSigner() {
		if (signer == null) {
			try {
				signer = (RequestSigner) Class.forName(DEFAULT_SIGNER_CLASS)
						.newInstance();
			} catch (Exception e) {
				Logger.error(AuthorizationManager.class.getSimpleName(),
						e.getMessage(), e);
				throw new RuntimeException(e);
			}
		}
		return signer;
	}
}

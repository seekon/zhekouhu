package com.seekon.zhekouhu.rest.sercurity;

import com.seekon.zhekouhu.rest.Request;

public interface RequestSigner {

	/**
	 * Adds the required OAuth information to a Request
	 * 
	 * @param conn
	 */
	public boolean authorize(Request request);

}

package com.seekon.zhekouhu.utils;

import java.text.DecimalFormat;

import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.func.widget.MyLocationListener;

public class LocationUtils {

	private static LocationClient mLocationClient = null;

	private LocationUtils() {

	}

	public static final double EARTH_RADIUS = 6378137;

	public synchronized static void startLocate() {
		if (mLocationClient == null) {
			mLocationClient = new LocationClient(Application.getAppContext());
			mLocationClient.registerLocationListener(new MyLocationListener(
					Application.getAppContext()));
			mLocationClient.setLocOption(LocationUtils.getDefaultLocationOption());
		}
		if (!mLocationClient.isStarted()) {
			mLocationClient.start();
		}
	}

	public synchronized static void stopLocate() {
		if (mLocationClient != null && mLocationClient.isStarted()) {
			mLocationClient.stop();
			mLocationClient = null;
		}
	}

	public static double latlngDist(double lat1, double lon1, double lat2,
			double lon2) {
		// double radLat1 = deg2rad(lat1);
		// double radLat2 = deg2rad(lat2);
		//
		// double radLon1 = deg2rad(lon1);
		// double radLon2 = deg2rad(lon2);
		//
		// if (radLat1 < 0)
		// radLat1 = Math.PI / 2 + Math.abs(radLat1);// south
		// if (radLat1 > 0)
		// radLat1 = Math.PI / 2 - Math.abs(radLat1);// north
		// if (radLon1 < 0)
		// radLon1 = Math.PI * 2 - Math.abs(radLon1);// west
		// if (radLat2 < 0)
		// radLat2 = Math.PI / 2 + Math.abs(radLat2);// south
		// if (radLat2 > 0)
		// radLat2 = Math.PI / 2 - Math.abs(radLat2);// north
		// if (radLon2 < 0)
		// radLon2 = Math.PI * 2 - Math.abs(radLon2);// west
		double radLat1 = translateLat(lat1);
		double radLon1 = translateLng(lon1);
		double radLat2 = translateLat(lat2);
		double radLon2 = translateLng(lon2);

		double x1 = latSinLngCos(radLat1, radLon1);
		double y1 = latSinLngSin(radLat1, radLon1);
		double z1 = latCos(radLat1);

		double x2 = latSinLngCos(radLat2, radLon2);
		double y2 = latSinLngSin(radLat2, radLon2);
		double z2 = latCos(radLat2);

		// double d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
		// + (z1 - z2) * (z1 - z2));
		// // 余弦定理求夹角
		// double theta = Math.acos((EARTH_RADIUS * EARTH_RADIUS + EARTH_RADIUS
		// * EARTH_RADIUS - d * d)
		// / (2 * EARTH_RADIUS * EARTH_RADIUS));
		double d = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2)
				* (z1 - z2);
		double theta = Math.acos((EARTH_RADIUS * EARTH_RADIUS + EARTH_RADIUS
				* EARTH_RADIUS - d)
				/ (2 * EARTH_RADIUS * EARTH_RADIUS));
		double dist = theta * EARTH_RADIUS;
		return dist;
	}

	public static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	public static double latSinLngCos(double lat, double lng) {
		return EARTH_RADIUS * Math.sin(lat) * Math.cos(lng);
	}

	public static double latSinLngSin(double lat, double lng) {
		return EARTH_RADIUS * Math.sin(lat) * Math.sin(lng);
	}

	public static double latCos(double lat) {
		return EARTH_RADIUS * Math.cos(lat);
	}

	public static double translateLat(double lat) {
		double radLat = deg2rad(lat);
		if (radLat < 0)
			radLat = Math.PI / 2 + Math.abs(radLat);// south
		if (radLat > 0)
			radLat = Math.PI / 2 - Math.abs(radLat);// north
		return radLat;
	}

	public static double translateLng(double lng) {
		double radLng = deg2rad(lng);
		if (radLng < 0)
			radLng = Math.PI * 2 - Math.abs(radLng);// west
		return radLng;
	}

	public static String formatDistance(double distance) {
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format(distance);
	}

	public static LocationClientOption getDefaultLocationOption() {
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);
		option.setIsNeedAddress(true);// 返回的定位结果包含地址信息
		option.setAddrType("all");// 返回的定位结果包含地址信息
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度,默认值gcj02
		option.setScanSpan(60 * 1000);// 设置发起定位请求的间隔时间为5000ms
		// option.disableCache(true);// 禁止启用缓存定位
		option.setPoiNumber(5); // 最多返回POI个数
		option.setPoiDistance(1000); // poi查询距离
		option.setPoiExtraInfo(true); // 是否需要POI的电话和地址等详细信息
		return option;
	}
}

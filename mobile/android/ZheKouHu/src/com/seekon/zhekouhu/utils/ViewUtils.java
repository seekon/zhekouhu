package com.seekon.zhekouhu.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.seekon.zhekouhu.Application;
import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.FuncConst;

public class ViewUtils {

	public static final String DEVICE_ID_FORMAT = "andr_%s"; // Device ID Format

	public static void showToast(String message) {
		showToast(message, 100);
	}

	public static void showToast(String message, int yOffset) {
		Toast toast = Toast.makeText(Application.getAppContext(), message,
				Toast.LENGTH_LONG);
		toast.setGravity(Gravity.BOTTOM, 0, yOffset);
		toast.show();
	}

	/**
	 * 弹出输入法窗口
	 */
	public static void popupInputMethodWindow(final Activity activity) {

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				InputMethodManager imm = (InputMethodManager) activity
						.getSystemService(Service.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}, 200);
	}

	/**
	 * 隐藏输入法键盘
	 * 
	 * @param activity
	 */
	public static void hideInputMethodWindow(Activity activity) {
		if (activity == null) {
			return;
		}
		InputMethodManager imm = ((InputMethodManager) activity
				.getSystemService(Service.INPUT_METHOD_SERVICE));
		if (imm != null && activity.getCurrentFocus() != null
				&& activity.getCurrentFocus().getWindowToken() != null) {
			imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	/**
	 * 设置textview为只读
	 * 
	 * @param view
	 */
	public static void setEditTextReadOnly(TextView view) {
		// view.setTextColor(android.R.color.background_light); // 设置只读时的文字颜色
		if (view instanceof android.widget.EditText) {
			view.setCursorVisible(false); // 设置输入框中的光标不可见
			view.setFocusable(false); // 无焦点
			view.setFocusableInTouchMode(false); // 触摸时也得不到焦点
		}
	}

	public static void cleanImageView(ImageView imageview) {
		if (!(imageview.getDrawable() instanceof BitmapDrawable)) {
			return;
		}
		// BitmapDrawable b = (BitmapDrawable) imageview.getDrawable();
		// b.getBitmap().recycle();
		imageview.setImageDrawable(null);
	}

	public static Dialog createProgreddDialog(Context context, String msg) {

		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.dialog_progress, null);// 得到加载view
		// main.xml中的ImageView
		// ImageView spaceshipImage = (ImageView) v.findViewById(R.id.img);
		TextView tipTextView = (TextView) v.findViewById(R.id.t_hit);// 提示文字
		// 加载动画
		// Animation hyperspaceJumpAnimation = AnimationUtils.loadAnimation(
		// context, R.anim.load_animation);
		// // 使用ImageView显示动画
		// spaceshipImage.startAnimation(hyperspaceJumpAnimation);
		tipTextView.setText(msg);// 设置加载信息

		Dialog loadingDialog = new Dialog(context, R.style.progress_dialog);// 创建自定义样式dialog
		loadingDialog.setContentView(v);
		// loadingDialog.setCancelable(false);// 不可以用“返回键”取消
		// loadingDialog.setContentView(v, new LinearLayout.LayoutParams(
		// LinearLayout.LayoutParams.FILL_PARENT,
		// LinearLayout.LayoutParams.FILL_PARENT));// 设置布局
		return loadingDialog;

	}

	public static String getDeviceId() {
		Context context = Application.getAppContext();
		String devId = PreferenceManager.getDefaultSharedPreferences(context)
				.getString(FuncConst.KEY_SHARE_PREFER_DEV_ID, null);
		if (devId == null) {
			devId = String.format(DEVICE_ID_FORMAT,
					Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
		}
		return devId;
	}
	
	/**
	 * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
	 * 
	 * @param v
	 * @param event
	 * @return
	 */
	public static boolean isShouldHideInput(View v, MotionEvent event) {
		if (v != null && (v instanceof EditText)) {
			int[] l = { 0, 0 };
			v.getLocationInWindow(l);
			int left = l[0], top = l[1], bottom = top + v.getHeight(), right = left
					+ v.getWidth();
			if (event.getX() > left && event.getX() < right && event.getY() > top
					&& event.getY() < bottom) {
				// 点击EditText的事件，忽略它。
				return false;
			} else {
				return true;
			}
		}
		// 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
		return false;
	}
}

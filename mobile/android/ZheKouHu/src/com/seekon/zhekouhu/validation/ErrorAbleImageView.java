package com.seekon.zhekouhu.validation;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.seekon.zhekouhu.R;

public class ErrorAbleImageView extends ImageView implements ErrorAble {

	private Context mContext;
	private ErrorHandler mSetErrorHandler;

	public ErrorAbleImageView(Context context) {
		super(context);
		mContext = context;
		mSetErrorHandler = new ErrorHandler(context, this);
	}

	// necessary for XML inflation
	public ErrorAbleImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		mSetErrorHandler = new ErrorHandler(context, this);
	}

	public ErrorAbleImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		mSetErrorHandler = new ErrorHandler(context, this);
	}

	@Override
	public void setErrorPopupPadding(int left, int top, int right, int bottom) {
		// TODO Auto-generated method stub

	}

	// for Views that didnt already have a setError method, simply proxy these
	// methods to setError,
	// otherwise proxy to SetErrorHandler.setError.
	@Override
	public void betterSetError(CharSequence error) {
		Drawable icon = mContext.getResources().getDrawable(
				R.drawable.indicator_input_error);
		mSetErrorHandler.setError(error, icon);
	}

	@Override
	public void betterSetError(CharSequence error, Drawable icon) {
		mSetErrorHandler.setError(error, icon);
	}

	@Override
	public void betterSetError(CharSequence error, boolean showError) {
		Drawable icon = mContext.getResources().getDrawable(
				R.drawable.indicator_input_error);
		mSetErrorHandler.setError(error, icon, showError);
	}

	@Override
	public void betterSetError(CharSequence error, Drawable icon,
			boolean showError) {
		mSetErrorHandler.setError(error, icon, showError);
	}

	@Override
	public void betterSetError(CharSequence error, Drawable icon,
			boolean showError, boolean showCompoundDrawableOnRight) {
		mSetErrorHandler.setError(error, icon, showError,
				showCompoundDrawableOnRight);
	}

	@Override
	public void betterSetError(CharSequence error, boolean showError,
			boolean showCompoundDrawableOnRight) {
		Drawable icon = mContext.getResources().getDrawable(
				R.drawable.indicator_input_error);
		mSetErrorHandler.setError(error, icon, showError,
				showCompoundDrawableOnRight);
	}

	// mimic the behaviour of native TextView in showing and hiding its ErrorPopup
	// onFocusChanged.
	// note: you must set the View's isFocusable and isFocusableInTouchMode to
	// true for this to work.
	@Override
	protected void onFocusChanged(boolean focused, int direction,
			Rect previouslyFocusedRect) {
		if (focused) {
			if (mSetErrorHandler.mError != null) {
				mSetErrorHandler.showError();
			}
		} else {
			if (mSetErrorHandler.mError != null) {
				mSetErrorHandler.hideError();
			}
		}
		super.onFocusChanged(focused, direction, previouslyFocusedRect);
	}

	@Override
	protected void onDetachedFromWindow() {
		if (mSetErrorHandler.mError != null) {
			mSetErrorHandler.hideError();
		}
		super.onDetachedFromWindow();
	}

	@Override
	public void setError(CharSequence error) {
		this.betterSetError(error);
	}

	@Override
	public void setError(CharSequence error, Drawable icon) {
		this.betterSetError(error, icon);
	}

	public void hideError() {
		if (mSetErrorHandler.mError != null) {
			mSetErrorHandler.hideError();
		}
	}
}

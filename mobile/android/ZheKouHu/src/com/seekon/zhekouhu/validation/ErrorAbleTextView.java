package com.seekon.zhekouhu.validation;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.seekon.zhekouhu.R;

public class ErrorAbleTextView extends TextView implements ErrorAble {

	private Context mContext;
	private ErrorHandler mSetErrorHandler;

	public ErrorAbleTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		mSetErrorHandler = new ErrorHandler(context, this);
		setupListeners();
	}

	public ErrorAbleTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		mSetErrorHandler = new ErrorHandler(context, this);
		setupListeners();
	}

	public ErrorAbleTextView(Context context) {
		super(context);
		mContext = context;
		mSetErrorHandler = new ErrorHandler(context, this);
		setupListeners();
	}

	private void setupListeners() {
		this.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				if (mSetErrorHandler.mError != null) {
					mSetErrorHandler.setError(null, null);
					// mSetErrorHandler.hideError();
				}
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
		});
		this.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (mSetErrorHandler.mError != null) {
					mSetErrorHandler.setError(null, null);
					// mSetErrorHandler.hideError();
				}
				return false;
			}
		});
	}

	@Override
	public void setErrorPopupPadding(int left, int top, int right, int bottom) {
		// TODO Auto-generated method stub

	}

	// for Views that didnt already have a setError method, simply proxy these
	// methods to setError,
	// otherwise proxy to SetErrorHandler.setError.
	@Override
	public void betterSetError(CharSequence error) {
		Drawable icon = mContext.getResources().getDrawable(
				R.drawable.indicator_input_error);
		mSetErrorHandler.setError(error, icon);
	}

	@Override
	public void betterSetError(CharSequence error, Drawable icon) {
		mSetErrorHandler.setError(error, icon);
	}

	@Override
	public void betterSetError(CharSequence error, boolean showError) {
		Drawable icon = mContext.getResources().getDrawable(
				R.drawable.indicator_input_error);
		mSetErrorHandler.setError(error, icon, showError);
	}

	@Override
	public void betterSetError(CharSequence error, Drawable icon,
			boolean showError) {
		mSetErrorHandler.setError(error, icon, showError);
	}

	@Override
	public void betterSetError(CharSequence error, Drawable icon,
			boolean showError, boolean showCompoundDrawableOnRight) {
		mSetErrorHandler.setError(error, icon, showError,
				showCompoundDrawableOnRight);
	}

	@Override
	public void betterSetError(CharSequence error, boolean showError,
			boolean showCompoundDrawableOnRight) {
		Drawable icon = mContext.getResources().getDrawable(
				R.drawable.indicator_input_error);
		mSetErrorHandler.setError(error, icon, showError,
				showCompoundDrawableOnRight);
	}

	// mimic the behaviour of native TextView in showing and hiding its ErrorPopup
	// onFocusChanged.
	@Override
	protected void onFocusChanged(boolean focused, int direction,
			Rect previouslyFocusedRect) {
		if (focused) {
			if (mSetErrorHandler.mError != null) {
				mSetErrorHandler.showError();
			}
		} else {
			if (mSetErrorHandler.mError != null) {
				mSetErrorHandler.hideError();
			}
		}
		super.onFocusChanged(focused, direction, previouslyFocusedRect);
	}

	@Override
	protected void onDetachedFromWindow() {
		if (mSetErrorHandler.mError != null) {
			mSetErrorHandler.hideError();
		}
		super.onDetachedFromWindow();
	}

	public void hideError() {
		if (mSetErrorHandler.mError != null) {
			mSetErrorHandler.hideError();
		}
		this.setCompoundDrawables(null, null, null, null);
	}
}

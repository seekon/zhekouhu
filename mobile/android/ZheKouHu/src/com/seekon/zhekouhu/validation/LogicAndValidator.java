package com.seekon.zhekouhu.validation;

public class LogicAndValidator extends AbstractValueValidator {

	private Object mSource;

	private AbstractValueValidator[] validators;

	public LogicAndValidator(Object mSource, AbstractValueValidator[] validators) {
		super();
		this.mSource = mSource;
		this.validators = validators;
	}

	@Override
	public ValueValidationResult validateValue() {
		for (AbstractValueValidator validator : validators) {
			ValueValidationResult result = validator.validateValue();
			if (!result.isValid()) {
				return result;
			}
		}
		return new ValueValidationResult(mSource, true);
	}

	@Override
	public String getExpression() {
		return "login and validator";
	}

	@Override
	public void setSource(Object source) {
		mSource = source;
	}

	@Override
	public Object getSource() {
		return mSource;
	}

}

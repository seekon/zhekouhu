package com.seekon.zhekouhu.validation;

public class LogicOrValidator extends AbstractValueValidator {

	private Object mSource;

	private AbstractValueValidator[] validators;

	private String mFaultExpressionMessage;

	public LogicOrValidator(Object mSource, AbstractValueValidator[] validators,
			String mFaultExpressionMessage) {
		super();
		this.mSource = mSource;
		this.validators = validators;
		this.mFaultExpressionMessage = mFaultExpressionMessage;
	}

	@Override
	public ValueValidationResult validateValue() {
		for (AbstractValueValidator validator : validators) {
			ValueValidationResult result = validator.validateValue();
			if (result.isValid()) {
				return result;
			}
		}
		return new ValueValidationResult(mSource, false, mFaultExpressionMessage);
	}

	@Override
	public String getExpression() {
		return "login or validator";
	}

	@Override
	public void setSource(Object source) {
		mSource = source;
	}

	@Override
	public Object getSource() {
		return mSource;
	}

}

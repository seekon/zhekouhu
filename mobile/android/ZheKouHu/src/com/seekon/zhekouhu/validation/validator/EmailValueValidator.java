package com.seekon.zhekouhu.validation.validator;

import android.widget.EditText;

public class EmailValueValidator extends RegExpressionValueValidator {

	private static final String expression = ".+@.+\\.[a-z]+";

	public EmailValueValidator(EditText source, String faultMessage) {
		super(source, expression, faultMessage);
	}

}

package com.seekon.zhekouhu.validation.validator;

import android.widget.TextView;

import com.seekon.zhekouhu.validation.AbstractValueValidator;
import com.seekon.zhekouhu.validation.ValueValidationResult;

public class LengthValidator extends AbstractValueValidator {

	private TextView mSource;
	private String mFaultMessage;
	private int minLenth;
	private int maxLenth;

	public LengthValidator(TextView mSource, String mFaultMessage, int minLenth,
			int maxLenth) {
		super();
		this.mSource = mSource;
		this.mFaultMessage = mFaultMessage;
		this.minLenth = minLenth;
		this.maxLenth = maxLenth;
	}

	@Override
	public ValueValidationResult validateValue() {
		String text = mSource.getText().toString();
		if (text.length() >= minLenth && text.length() <= maxLenth) {
			return new ValueValidationResult(mSource, true);
		}
		return new ValueValidationResult(mSource, false, mFaultMessage);
	}

	@Override
	public String getExpression() {
		return null;
	}

	@Override
	public void setSource(Object source) {
		mSource = (TextView) source;
	}

	@Override
	public Object getSource() {
		return mSource;
	}
}

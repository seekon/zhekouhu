package com.seekon.zhekouhu.validation.validator;

import android.text.TextUtils;
import android.widget.EditText;

import com.seekon.zhekouhu.validation.AbstractValueValidator;
import com.seekon.zhekouhu.validation.ValueValidationResult;

public class PhoneValueValidator extends AbstractValueValidator {

	private EditText mSource;

	private String faultMessage;

	public PhoneValueValidator(EditText mSource, String faultMessage) {
		super();
		this.mSource = mSource;
		this.faultMessage = faultMessage;
	}

	@Override
	public ValueValidationResult validateValue() {
		String phone = mSource.getText().toString();
		if (TextUtils.isEmpty(phone)) {
			return new ValueValidationResult(mSource, false, faultMessage);
		}
		boolean validate = TextUtils.isDigitsOnly(phone) && phone.length() == 11;

		return new ValueValidationResult(mSource, validate, faultMessage);
	}

	@Override
	public String getExpression() {
		return null;
	}

	@Override
	public void setSource(Object source) {
		mSource = (EditText) source;
	}

	@Override
	public Object getSource() {
		return mSource;
	}

}

package com.seekon.zhekouhu.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.seekon.zhekouhu.R;
import com.seekon.zhekouhu.func.share.weixin.WXConst;
import com.seekon.zhekouhu.utils.ViewUtils;
import com.tencent.mm.sdk.openapi.BaseReq;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

	private IWXAPI api;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		api = WXAPIFactory.createWXAPI(this, WXConst.APP_ID, false);
		api.handleIntent(getIntent(), this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		setIntent(intent);
		api.handleIntent(intent, this);
	}

	// 微信发送请求到第三方应用时，会回调到该方法
	@Override
	public void onReq(BaseReq req) {
		switch (req.getType()) {
		// case ConstantsAPI.COMMAND_GETMESSAGE_FROM_WX:
		// goToGetMsg();
		// break;
		// case ConstantsAPI.COMMAND_SHOWMESSAGE_FROM_WX:
		// goToShowMsg((ShowMessageFromWX.Req) req);
		// break;
		default:
			break;
		}
	}

	// 第三方应用发送到微信的请求处理后的响应结果，会回调到该方法
	@Override
	public void onResp(BaseResp resp) {
		int result = 0;

		switch (resp.errCode) {
		case BaseResp.ErrCode.ERR_OK:
			result = R.string.share_errcode_success;
			break;
		case BaseResp.ErrCode.ERR_USER_CANCEL:
			// result = R.string.share_errcode_cancel;
			break;
		case BaseResp.ErrCode.ERR_AUTH_DENIED:
			result = R.string.share_errcode_deny;
			break;
		default:
			result = R.string.share_errcode_unknown;
			break;
		}

		if (result != 0) {
			ViewUtils.showToast(getString(result));
		}
		finish();
	}

}

//
//  NSDate+Utils.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-21.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)
+ (NSString *) currentTimeString
{
    return [NSDate milliSeconds:[NSDate date]];
}

+ (NSString *)milliSeconds:(NSDate *)datetime
{
     NSTimeInterval interval = [datetime timeIntervalSince1970];
    long long totalMilliseconds = interval*1000 ;
    return [NSString stringWithFormat:@"%llu", totalMilliseconds];
}

+ (id)dateWithMilliSeconds:(long long)milliSeconds
{
    return [NSDate dateWithTimeIntervalSince1970:milliSeconds/1000.0];
}

- (NSString *)toyyyyMMddString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:self];
}

- (NSString *)toddMMString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"d日M月"];
    return [dateFormatter stringFromDate:self];
}

+ (id)initWithyyyyMMddString:(NSString *)str
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    return [df dateFromString:str];
}

#define D_DAY    86400
- (NSDate *) dateByAddingDays: (NSInteger) dDays
{
    NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + D_DAY * dDays;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

@end

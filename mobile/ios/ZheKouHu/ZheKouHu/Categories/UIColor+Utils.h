//
//  UIColor+Utils.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-1.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Utils)

+ (UIColor *) colorWithRGB:(int)red green:(int)green blue:(int)blue alpha:(float) alpha;

+ (UIColor *)color_f5f5f5;
+ (UIColor *)color_ff6722;
+ (UIColor *)color_2d2d2d;
+ (UIColor *)color_e3e3e3;
+ (UIColor *)color_7f7f7f;
+ (UIColor *)color_bbbbbb;
+ (UIColor *)color_f8f8f8;
+ (UIColor *)color_e45717;
+ (UIColor *)color_d5d5d5;

@end

//
//  UIColor+Utils.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-1.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "UIColor+Utils.h"

@implementation UIColor (Utils)

+ (UIColor *) colorWithRGB:(int)red green:(int)green blue:(int)blue alpha:(float) alpha
{
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha];
}

+ (UIColor *)color_f5f5f5
{
    return [UIColor colorWithRGB:245 green:245 blue:245 alpha:1];
}

+ (UIColor *)color_ff6722
{
    return [UIColor colorWithRGB:255 green:103 blue:34 alpha:1];
}

+ (UIColor *)color_2d2d2d
{
    return [UIColor colorWithRGB:45 green:45 blue:45 alpha:1];
}

+ (UIColor *)color_e3e3e3
{
    return [UIColor colorWithRGB:227 green:227 blue:227 alpha:1];
}

+ (UIColor *)color_7f7f7f
{
    return [UIColor colorWithRGB:127 green:127 blue:127 alpha:1];
}

+ (UIColor *)color_bbbbbb
{
    return [UIColor colorWithRGB:187 green:187 blue:187 alpha:1];
}

+ (UIColor *)color_f8f8f8
{
    return [UIColor colorWithRGB:248 green:248 blue:248 alpha:1];
}

+ (UIColor *)color_e45717
{
    return [UIColor colorWithRGB:228 green:87 blue:23 alpha:1];
}

+ (UIColor *)color_d5d5d5
{
    return [UIColor colorWithRGB:213 green:213 blue:213 alpha:1];
}
@end

//
//  UIImage+Utils.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-1.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "UIImage+Utils.h"

@implementation UIImage (Utils)

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size

{
        CGRect rect = CGRectMake(0, 0, size.width, size.height);
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColorWithColor(context,color.CGColor);
        CGContextFillRect(context, rect);
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return img;
}

+ (UIImage *)imageNamed:(NSString *)name scale:(float)scale
{
    return [UIImage imageWithData:UIImagePNGRepresentation([UIImage imageNamed:name] ) scale:scale];
}
@end

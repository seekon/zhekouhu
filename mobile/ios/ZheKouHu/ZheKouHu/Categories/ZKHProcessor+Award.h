//
//  ZKHProcessor+Award.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-21.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"

@interface ZKHProcessor (Award)

typedef void (^GoodsListResponseBlock)(NSMutableArray *goodsList);
- (void) goodsList:(GoodsListResponseBlock) goodsListBlock errorHandler:(RestResponseErrorBlock) errorBlock;

@end

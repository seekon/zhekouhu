//
//  ZKHProcessor+Award.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-21.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+Award.h"
#import "ZKHConst.h"

@implementation ZKHProcessor (Award)

#define GET_GOODS @"/getGoods"
- (void)goodsList:(GoodsListResponseBlock)goodsListBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = GET_GOODS;
    request.method = METHOD_GET;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray * goodsList = [[NSMutableArray alloc] init];
        for (id json in jsonObject) {
            [goodsList addObject:[[ZKHGoodsEntity alloc] initWithJSONObject:json]];
        }
        goodsListBlock(goodsList);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

@end

//
//  ZKHProcessor+Category.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"

@interface ZKHProcessor (Category)

typedef void (^CategoriesResponseBlock)(NSMutableArray *categories);
- (void) categories:(CategoriesResponseBlock) categoriesBlock errorHandler:(RestResponseErrorBlock) errorBlock;

@end

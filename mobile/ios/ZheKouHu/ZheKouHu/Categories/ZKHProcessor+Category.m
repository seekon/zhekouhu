//
//  ZKHProcessor+Category.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+Category.h"
#import "ZKHData.h"
#import "ZKHConst.h"

@implementation ZKHProcessor (Category)

//获取分类列表
#define GET_CATEGORIES_URL(__UPDATE_TIME__) [NSString stringWithFormat:@"/getCategories/%@", __UPDATE_TIME__]
- (void)categories:(CategoriesResponseBlock)categoriesBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHCategoryData *categoryData = [[ZKHCategoryData alloc] init];
    NSMutableArray *categories = [categoryData categories];
    if ([categories count] > 0) {
        categoriesBlock(categories);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = GET_CATEGORIES_URL(@"-1");
    request.method = METHOD_GET;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray *categories = [[NSMutableArray alloc] init];
        for (id jsonCategory in jsonObject) {
            ZKHCategoryEntity *category = [[ZKHCategoryEntity alloc] init];
            category.uuid = jsonCategory[KEY_UUID];
            category.name = jsonCategory[KEY_NAME];
            category.icon = jsonCategory[KEY_ICON];
            category.ordIndex = [jsonCategory[KEY_ORD_INDEX] intValue];
            if (jsonCategory[KEY_PARENT_ID]) {
                category.parentId = jsonCategory[KEY_PARENT_ID];
            }else{
                category.parentId = @"";
            }
            [categories addObject:category];
        }
        [categoryData save:categories];
        categoriesBlock(categories);
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

@end

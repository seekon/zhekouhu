//
//  ZKHProcessor+City.h
//  ZheKouHu
//
//  Created by undyliu on 14-7-14.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"

@interface ZKHProcessor (City)

typedef void (^CitiesResponseBlock)(NSMutableArray *cities);

- (void) cities:(CitiesResponseBlock) citiesBlock errorHandler:(RestResponseErrorBlock) errorBlock;
- (NSMutableArray *) citiesFromLocal;

@end

//
//  ZKHProcessor+City.m
//  ZheKouHu
//
//  Created by undyliu on 14-7-14.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+City.h"
#import "NSString+Utils.h"
#import "ZKHData.h"
#import "ZKHEntity.h"
#import "ZKHConst.h"

@implementation ZKHProcessor (City)

//远程获取城市
#define GET_CITIES_URL(_UPDATE_TIME_) [NSString stringWithFormat:@"/getCities/%@", _UPDATE_TIME_]
- (void)cities:(CitiesResponseBlock)citiesBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSString *updateTime;
    ZKHSyncEntity *sync = [[[ZKHSyncData alloc] init] syncEntity:CITY_TABLE itemId:@"*"];
    if (sync) {
        updateTime = sync.updateTime;
    }
    if (!updateTime) {
        updateTime = @"-1";
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = GET_CITIES_URL(updateTime);
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray *cityList = [[NSMutableArray alloc] init];
        NSString *updateTime = jsonObject[KEY_UPDATE_TIME];
        if (![NSString isNull:updateTime]) {
            id data = jsonObject[KEY_DATA];
            for (id jsonCity in data) {
                [cityList addObject:[[ZKHCityEntity alloc] initWithJSONObject:jsonCity]];
            }
            [[[ZKHCityData alloc] init] save:cityList];
            [[[ZKHSyncData alloc] init] save:CITY_TABLE itemId:@"*" updateTime:updateTime];
        }
        citiesBlock([self citiesFromLocal]);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    } ];
}

- (NSMutableArray *)citiesFromLocal
{
    return [[ZKHCityData alloc] cities];
}
@end

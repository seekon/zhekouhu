//
//  ZKHProcessor+Discount.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"
#import "ZKHEntity.h"
@interface ZKHProcessor (Discount)

/**
 * 发布活动
 */
- (void) publishDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock) publishDiscountBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//更新活动
- (void) updateDiscount:(ZKHDiscountEntity *)discount addImages:(NSMutableArray *)addImages deletedImages:(NSMutableArray *)deletedImages deletedStorfronts:(NSMutableArray *)deletedStorfronts completionHandler:(BooleanResultResponseBlock) updateDiscountBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//删除活动
- (void) deleteDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock) deleteDiscountBlock errorHandler:(RestResponseErrorBlock) errorBlock;


//获取店主所发布的折扣活动数目
- (int) activityCountForOwner:(NSString *)owner;

//获取店主所发布的优惠券数目
- (int) couponCountForOwner:(NSString *)owner;

//获取个人所发布的活动数目
- (int) discountCountForOwner:(NSString *)owner;

typedef void (^DiscountsResponseBlock)(NSMutableArray *discounts);

//远程更新本人发布的活动列表
- (void)discountsRemoteRefresh:(NSString *)owner type:(NSString *)type completionHandler:(BooleanResultResponseBlock)discountsBlock errorHandler:(RestResponseErrorBlock)errorBlock;

//浏览活动记录
- (void)visitDiscount:(ZKHDiscountEntity *) discount completionHandler:(BooleanResultResponseBlock)visitDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock;

//按时间分组从本地获取本人所发布的折扣活动列表
- (NSMutableArray *)activitiesGroupByPublishDate:(NSString *)owner offset:(long)offset;

//按时间分组从本地获取本人所发布的优惠券列表
- (NSMutableArray *)couponsGroupByPublishDate:(NSString *)owner offset:(long)offset;

//按时间分组获取本人所发布的活动列表，每次调用都从远程刷新
- (void)discountsGroupByPublishDate:(NSString *)owner offset:(long)offset completionHandler:(DiscountsResponseBlock) discountsBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//远程获取最新的活动记录
- (void) newestActivities:(long)offset completionHandler:(DiscountsResponseBlock) activitiesBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//按分类获取折扣活动数据
- (void)activitiesForCategory:(NSString *)categoryId offset:(long)offset completionHandler:(DiscountsResponseBlock) activitiesBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//远程刷新店铺的优惠券列表
- (void)couponsForStoreRemoteRefresh:(NSString *)storeId cateId:(NSString *)cateId offset:(long)offset completionHandler:(DiscountsResponseBlock) couponsBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//根据storeId从本地获取优惠券列表
//- (NSMutableArray *)couponsForStore:(NSString *)storeId cateId:(NSString *)cateId offset:(long)offset;

typedef void (^CouponStoresResponseBlock)(NSMutableDictionary *couponStores);

//远程更新有优惠券的店铺列表
- (void)storesHasCouponRemoteRefresh:(CouponStoresResponseBlock) couponStoresBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//从本地获取存在优惠券的店铺列表
- (NSMutableDictionary *)storesHasCouponFromLocal;

//搜索活动数据
- (void)discountsByKeyword:(NSString *)searchWord offset:(long)offset completionHandler:(DiscountsResponseBlock) couponsBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//根据id获取活动
typedef void (^DiscountResponseBlock)(ZKHDiscountEntity *discount);
- (void)discountsById:(NSString *)id completionHandler:(DiscountResponseBlock) discountBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//获取活动的门店列表
typedef void (^StorefrontsResponseBlock)(NSMutableArray *storefronts);
- (void) storeFrontsByDiscount:(ZKHDiscountEntity *)discount limit:(int)limit offset:(long)offset
completionHandler:(StorefrontsResponseBlock) storefrontsBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//获取活动的评论列表
typedef void (^CommentsResponseBlock)(NSMutableArray *comments);
- (void) commentsByDiscount:(ZKHDiscountEntity *)discount limit:(int)limit offset:(long)offset
                completionHandler:(CommentsResponseBlock) commentsBlock errorHandler:(RestResponseErrorBlock) errorBlock;
//发布评论
typedef void (^CommentResponseBlock)(ZKHCommentEntity *comment);
- (void) publishComment:(ZKHCommentEntity *)comment
          completionHandler:(CommentResponseBlock) commentBlock errorHandler:(RestResponseErrorBlock) errorBlock;
//收藏活动
- (void) favoritDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock) favoritDiscountBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//取消收藏活动
- (void) unfavoritDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock) unfavoritDiscountBlock errorHandler:(RestResponseErrorBlock) errorBlock;
//检查是否已收藏
-(Boolean) isUserFavorited:(NSString *)userId discountId:(NSString *)discountId;

//获取收藏活动列表
- (void) userFavorits:(long)offset completionHandler:(DiscountsResponseBlock) discountsBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//刷新收藏活动列表
- (void) refreshUserFavorits:(NSString *)userId completionHandler:(BooleanResultResponseBlock) refreshBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//获取活动的额外信息，因收藏并没保存完整的数据
- (void)discountExtWithFavorit:(ZKHDiscountEntity *) discount completionHandler:(DiscountResponseBlock) discountBlock errorHandler:(RestResponseErrorBlock) errorBlock;

@end

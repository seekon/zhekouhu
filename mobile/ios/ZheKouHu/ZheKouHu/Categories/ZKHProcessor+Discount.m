//
//  ZKHProcessor+Discount.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+Discount.h"
#import "ZKHConst.h"
#import "NSString+Utils.h"
#import "NSString+MKNetworkKitAdditions.h"
#import "NSDate+Utils.h"
#import "ZKHData.h"
#import "ZKHContext.h"
#import "ZKHLocationUtils.h"
#import "ZKHViewUtils.h"

@implementation ZKHProcessor (Discount)

- (NSString *) discountStorefrontsJSONString:(NSMutableArray *)storefronts
{
    NSMutableString *fronts = [[NSMutableString alloc] init];
    [fronts appendString:@"["];
    for (ZKHStorefrontEntity *front in storefronts) {
        [fronts appendString:@"{"];
        [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_UUID, front.uuid];
        [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_STORE_ID, front.storeId];
        [fronts appendString:@"}"];
    }
    [fronts appendString:@"]"];
    
    return fronts;
}

//处理请求返回的折扣数据
- (int) saveReponseDiscountsData:(id) data
{
    int result = 0;
    //活动列表
    NSMutableArray *discounts = [[NSMutableArray alloc] init];
    id jsonDiscounts = data[KEY_DISCOUNTS];
    for (id jsonDiscount in jsonDiscounts) {
        
        NSString *status = jsonDiscount[KEY_STATUS];
        if ([status isEqualToString:@"9"]) {
            [[[ZKHDiscountData alloc] init] deleteDiscount:jsonDiscount[KEY_UUID]];
            continue;
        }
        
        ZKHDiscountEntity *discount = [[ZKHDiscountEntity alloc] initWithJSONObject:jsonDiscount];
        
        discount.category = [[ZKHCategoryEntity alloc] init];
        discount.category.uuid = jsonDiscount[KEY_CATEGORY_ID];
        
        [discounts addObject:discount];
    }
    
    ZKHDiscountStorefrontData *discountStorefrontData = [[ZKHDiscountStorefrontData alloc] init];
    ZKHDiscountImageData *imageData = [[ZKHDiscountImageData alloc] init];
    
    //图片列表
    NSMutableArray *discountImages = [[NSMutableArray alloc] init];
    id jsonImages = data[KEY_IMAGES];
    for (id jsonImage in jsonImages) {
        NSString *isDeleted = jsonImage[KEY_IS_DELETED];
        if ([@"1" isEqualToString:isDeleted]) {
            [imageData deleteById:jsonImage[KEY_UUID]];
            continue;
        }
        [discountImages addObject:[[ZKHDiscountImageEntity alloc] initWithJSONObject:jsonImage]];
    }
    
    //活动门店对应列表
    NSMutableArray *discountFronts = [[NSMutableArray alloc] init];
    id jsonDiscountFronts = data[KEY_DISCOUNT_STOREFRONTS];
    for (id jsonDiscountFront in jsonDiscountFronts) {
        NSString *isDeleted = jsonDiscountFront[KEY_IS_DELETED];
        if ([@"1" isEqualToString:isDeleted]) {
            [discountStorefrontData deleteById:jsonDiscountFront[KEY_UUID]];
            continue;
        }
        [discountFronts addObject:[[ZKHDiscountStorefrontEntity alloc] initWithJSONObject:jsonDiscountFront]];
    }
    
    //门店列表
    NSMutableArray *storefronts = [[NSMutableArray alloc] init];
    id jsonStorefronts = data[KEY_STOREFRONTS];
    for (id jsonFront in jsonStorefronts) {
        [storefronts addObject:[[ZKHStorefrontEntity alloc] initWithJSONObject:jsonFront]];
    }
    
    //店铺列表
    NSMutableArray *stores = [[NSMutableArray alloc] init];
    id jsonStores = data[KEY_STORES];
    for (id jsonStore in jsonStores) {
        [stores addObject:[[ZKHStoreEntity alloc] initWithJSONObject:jsonStore]];
    }
    
    [[[ZKHStorefrontData alloc] init] save:storefronts];
    [[[ZKHStoreData alloc] init] save:stores];
    
    [imageData save:discountImages];
    [discountStorefrontData save:discountFronts];
    [[[ZKHDiscountData alloc] init] save:discounts];
    
    result = [storefronts count] + [stores count] + [discountImages count] + [discountFronts count] + [discounts count];
    return result;
}

- (ZKHDiscountEntity *)assembleDiscount:(id)json
{
    ZKHDiscountEntity *discount = [[ZKHDiscountEntity alloc] initWithJSONObject:json];
    //图片
    if (json[KEY_IMAGES]) {
        NSMutableArray *images = [[NSMutableArray alloc] init];
        id jsonImages = json[KEY_IMAGES];
        for (id jsonImg in jsonImages) {
            ZKHDiscountImageEntity *image = [[ZKHDiscountImageEntity alloc] initWithJSONObject:jsonImg];
            image.discountId = discount.uuid;
            [images addObject:image];
        }
        discount.images = images;
    }
    //门店数
    if (json[KEY_FRONT_COUNT]) {
        discount.frontCount = [json[KEY_FRONT_COUNT] doubleValue];
    }
    //评论数
    if (json[KEY_COMMENT_COUNT]) {
        discount.commentCount = [json[KEY_COMMENT_COUNT] doubleValue];
    }
    
    //最新的门店
    if(json[KEY_STOREFRONT] && discount.frontCount > 0){
        NSMutableArray *storefronts = [[NSMutableArray alloc] init];
        id jsonFront = json[KEY_STOREFRONT];
        
        ZKHStorefrontEntity *front = [[ZKHStorefrontEntity alloc] init];
        front.uuid = jsonFront[KEY_UUID];
        front.name = jsonFront[KEY_NAME];
        if (json[KEY_DISTANCE]) {
            front.distance = [json[KEY_DISTANCE] integerValue];
        }
        front.storeId = jsonFront[KEY_STORE_ID];
        front.storeName = jsonFront[KEY_STORE_NAME];
        
        [storefronts addObject:front];
        discount.storefronts = storefronts;
    }
    
    if (discount.frontCount == 0) {
        return nil;
    }
    return discount;
}

- (NSMutableArray *)assembleDiscounts:(id) data
{
    NSMutableArray *discountList = [[NSMutableArray alloc] init];
    for (id json in data) {
        ZKHDiscountEntity *discount = [self assembleDiscount:json];
        if (discount) {
            [discountList addObject:discount];
        }
    }
    return discountList;
}

//将json类型的数据组装成java对象数据
- (NSMutableArray *)_assembleDiscounts:(id) data
{
    NSMutableArray *discountList = [[NSMutableArray alloc] init];
    if (data[KEY_DISCOUNTS]) {
        //图片
        NSMutableDictionary *discountImageMap = [[NSMutableDictionary alloc] init];
        ZKHDiscountImageData *imageData = [[ZKHDiscountImageData alloc] init];
        id jsonImages = data[KEY_IMAGES];
        for (id jsonImage in jsonImages) {
            NSString *isDeleted = jsonImage[KEY_IS_DELETED];
            if ([@"1" isEqualToString:isDeleted]) {
                [imageData deleteById:jsonImage[KEY_UUID]];
                continue;
            }
            NSString *discountId = jsonImage[KEY_DISCOUNT_ID];
            NSMutableArray *images = discountImageMap[discountId];
            if (!images) {
                images = [[NSMutableArray alloc] init];
            }
            ZKHFileEntity *imageFile = [[ZKHFileEntity alloc] init];
            imageFile.aliasName = jsonImage[KEY_IMG];
            
            [images addObject:imageFile];
            [discountImageMap setObject:images forKey:discountId];
        }
        
        //分类
        NSMutableDictionary *cateMap = [[NSMutableDictionary alloc] init];
        NSMutableArray *categories = [[[ZKHCategoryData alloc] init] categories];
        for (ZKHCategoryEntity *cate in categories) {
            [cateMap setObject:cate forKey:cate.uuid];
        }
        
        //店铺
        NSMutableDictionary *storeMap = [[NSMutableDictionary alloc] init];
        id jsonStores = data[KEY_STORES];
        for (id jsonStore in jsonStores) {
            ZKHStoreEntity *store = [[ZKHStoreEntity alloc] initWithJSONObject:jsonStore];
            [storeMap setObject:store forKey:store.uuid];
        }
        
        //门店
        NSMutableDictionary *storefrontMap = [[NSMutableDictionary alloc] init];
        id jsonStorefronts = data[KEY_STOREFRONTS];
        for (id jsonFront in jsonStorefronts) {
            ZKHStorefrontEntity *front = [[ZKHStorefrontEntity alloc] initWithJSONObject:jsonFront];
            front.storeName = ((ZKHStoreEntity *)storeMap[front.storeId]).name;
            
            ZKHLocation *location = [ZKHContext getInstance].location;
            if (location) {
                double distance = [ZKHLocationUtils latlngDist:location.lat lng1:location.lng lat2:[front.latitude doubleValue] lng2:[front.longitude doubleValue]];
                front.distance = distance;
            }
            [storefrontMap setObject:front forKey:front.uuid];
        }
        
        //门店活动对应
        ZKHDiscountStorefrontData *discountStorefrontData = [[ZKHDiscountStorefrontData alloc] init];
        NSMutableDictionary *discountSFMap = [[NSMutableDictionary alloc] init];
        
        id jsonDiscountFronts = data[KEY_DISCOUNT_STOREFRONTS];
        for (id jsonDiscountFront in jsonDiscountFronts) {
            NSString *isDeleted = jsonDiscountFront[KEY_IS_DELETED];
            if ([@"1" isEqualToString:isDeleted]) {
                [discountStorefrontData deleteById:jsonDiscountFront[KEY_UUID]];
                continue;
            }
            NSString *discountId = jsonDiscountFront[KEY_DISCOUNT_ID];
            NSMutableArray *frontList = discountSFMap[discountId];
            if (!frontList) {
                frontList = [[NSMutableArray alloc] init];
            }
            id storefront = storefrontMap[jsonDiscountFront[KEY_STOREFRONT_ID]];
            if (storefront) {
                [frontList addObject:storefront];
            }
            [discountSFMap setObject:frontList forKey:discountId];
        }
        
        //活动列表
        id jsonDiscounts = data[KEY_DISCOUNTS];
        for (id jsonDiscount in jsonDiscounts) {
            NSString *status = jsonDiscount[KEY_STATUS];
            if ([status isEqualToString:@"9"]) {
                [[[ZKHDiscountData alloc] init] deleteDiscount:jsonDiscount[KEY_UUID]];
                continue;
            }
            
            ZKHDiscountEntity *discount = [[ZKHDiscountEntity alloc] initWithJSONObject:jsonDiscount];
            
            
            discount.category = cateMap[jsonDiscount[KEY_CATEGORY_ID]];
            discount.images = discountImageMap[discount.uuid];
            discount.storefronts = [[discountSFMap[discount.uuid] sortedArrayUsingComparator:^NSComparisonResult(ZKHStorefrontEntity *obj1, ZKHStorefrontEntity *obj2) {
                
                NSComparisonResult result = obj1.distance > obj2.distance;
                
                return result == NSOrderedDescending; // 升序
            }] mutableCopy] ;
            
            [discountList addObject:discount];
        }
    }
    return discountList;
}

#define PUBLISH_DISCOUNT_URL @"/publishDiscount"
- (void)publishDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock)publishDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    if ([ZKHContext getInstance].user) {
        request.urlString = PUBLISH_DISCOUNT_URL;
    }else{
        request.urlString = @"/anonymPublishDiscount";
    }
    request.method = METHOD_POST;
    request.params = @{KEY_CONTENT: discount.content, KEY_PAR_VALUE: discount.parValue,KEY_START_DATE: discount.startDate, KEY_END_DATE: discount.endDate, KEY_TYPE:discount.type, KEY_PUBLISHER: discount.publisher, KEY_CATEGORY_ID: discount.category.uuid,KEY_STOREFRONTS:[self discountStorefrontsJSONString:discount.storefronts], KEY_ORIGIN :discount.origin, KEY_LOCATION :discount.location};
    request.files = discount.images;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            publishDiscountBlock(false);
        }else{
            discount.uuid = uuid;
            discount.publishDate = jsonObject[KEY_PUBLISH_DATE];
            discount.publishTime = jsonObject[KEY_PUBLISH_TIME];
            discount.visitCount = 0;
            discount.status = jsonObject[KEY_STATUS];
            
            NSMutableArray *discountImages = [[NSMutableArray alloc] init];
            id jsonImages = jsonObject[KEY_IMAGES];
            for (id jsonImage in jsonImages) {
                ZKHDiscountImageEntity *image = [[ZKHDiscountImageEntity alloc] initWithJSONObject:jsonImage];
                image.discountId = discount.uuid;
                
                [discountImages addObject:image];
            }
            
            NSMutableArray *discountStorefronts = [[NSMutableArray alloc] init];
            id jsonFronts = jsonObject[KEY_STOREFRONTS];
            for (id jsonFront in jsonFronts) {
                ZKHDiscountStorefrontEntity *front = [[ZKHDiscountStorefrontEntity alloc] initWithJSONObject:jsonFront];
                front.discountId = discount.uuid;
                
                [discountStorefronts addObject:front];
            }
            
            [[[ZKHDiscountStorefrontData alloc] init] save:discountStorefronts];
            [[[ZKHDiscountImageData alloc] init] save:discountImages];
            [[[ZKHDiscountData alloc] init] save:@[discount]];
            
            publishDiscountBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define UPDATE_DISCOUNT_URL @"/updateDiscount"
- (void)updateDiscount:(ZKHDiscountEntity *)discount addImages:(NSMutableArray *)addImages deletedImages:(NSMutableArray *)deletedImages deletedStorfronts:(NSMutableArray *)deletedStorfronts completionHandler:(BooleanResultResponseBlock)updateDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = UPDATE_DISCOUNT_URL;
    request.method = METHOD_PUT;
    
    request.params = @{KEY_UUID: discount.uuid, KEY_CONTENT: discount.content, KEY_PAR_VALUE: discount.parValue, KEY_START_DATE: discount.startDate, KEY_END_DATE: discount.endDate, KEY_TYPE:discount.type, KEY_PUBLISHER: discount.publisher, KEY_CATEGORY_ID: discount.category.uuid, KEY_STOREFRONTS:[self discountAddedStorefrontString:discount.storefronts discountId:discount.uuid], KEY_DEL_IMAGES: [self discountDeletedImageString:deletedImages], KEY_DEL_STOREFRONTS: [self discountDeletedStorefrontString:deletedStorfronts], KEY_ORIGIN:discount.origin, KEY_LOCATION :discount.location};
    
    request.files = addImages;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            updateDiscountBlock(false);
        }else{
            ZKHDiscountStorefrontData *storefrontData = [[ZKHDiscountStorefrontData alloc] init];
            ZKHDiscountImageData *imageData = [[ZKHDiscountImageData alloc] init];
            
            //删除图片
            for (ZKHFileEntity *file in deletedImages) {
                [imageData deleteByDiscountImage:discount.uuid image:file.aliasName];
            }
            
            //删除门店
            for (ZKHStorefrontEntity *front in deletedStorfronts) {
                [storefrontData deleteByDiscountStorefront:discount.uuid storefrontId:front.uuid];
            }
            
            //插入增加的图片
            NSMutableArray *discountImages = [[NSMutableArray alloc] init];
            id jsonImages = jsonObject[KEY_IMAGES];
            for (id jsonImage in jsonImages) {
                ZKHDiscountImageEntity *image = [[ZKHDiscountImageEntity alloc] initWithJSONObject:jsonImage];
                image.discountId = discount.uuid;
                
                [discountImages addObject:image];
            }
            
            //插入增加的门店
            NSMutableArray *discountStorefronts = [[NSMutableArray alloc] init];
            id jsonFronts = jsonObject[KEY_STOREFRONTS];
            for (id jsonFront in jsonFronts) {
                ZKHDiscountStorefrontEntity *front = [[ZKHDiscountStorefrontEntity alloc] initWithJSONObject:jsonFront];
                front.discountId = discount.uuid;
                
                [discountStorefronts addObject:front];
            }
            
            [storefrontData save:discountStorefronts];
            [imageData save:discountImages];
            [[[ZKHDiscountData alloc] init] save:@[discount]];
            
            updateDiscountBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (NSString *) discountDeletedImageString:(NSMutableArray *)images
{
    NSMutableString *result = [[NSMutableString alloc] init];
    [result appendString:@"["];
    for (ZKHFileEntity *image in images) {
        [result appendString:@"{"];
        [result appendFormat:@" \"%@\":\"%@\" ", KEY_IMG, image.aliasName];
        [result appendString:@"}"];
    }
    [result appendString:@"]"];
    return result;
}

- (NSString *) discountDeletedStorefrontString:(NSMutableArray *)storefronts
{
    NSMutableString *result = [[NSMutableString alloc] init];
    [result appendString:@"["];
    for (ZKHStorefrontEntity *storefront in storefronts) {
        [result appendString:@"{"];
        [result appendFormat:@" \"%@\":\"%@\" ", KEY_UUID, storefront.uuid];
        [result appendString:@"}"];
    }
    [result appendString:@"]"];
    return result;
}

- (NSString *) discountAddedStorefrontString:(NSMutableArray *)storefronts discountId:(NSString *)discountId
{
    NSMutableArray *oldFronts = [[[ZKHStorefrontData alloc] init] storefrontsForDiscount:discountId];
    NSMutableArray *addedFronts = [[NSMutableArray alloc] init];
    for (id front in storefronts) {
        if (![oldFronts containsObject:front]) {
            [addedFronts addObject:front];
        }
    }
    return [self discountStorefrontsJSONString:addedFronts];
}

//删除折扣数据
#define DEL_DISCOUNT_URL(__UUID__) [NSString stringWithFormat:@"/deleteDiscount/%@", __UUID__]
- (void)deleteDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock)deleteDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_DELETE;
    request.urlString = DEL_DISCOUNT_URL(discount.uuid);
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            deleteDiscountBlock(false);
        }else{
            [[[ZKHDiscountData alloc] init] deleteDiscount:discount.uuid];
            deleteDiscountBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (int)activityCountForOwner:(NSString *)owner
{
    return [[[ZKHDiscountData alloc] init] activityCountForOwner:owner];
}

- (int)couponCountForOwner:(NSString *)owner
{
    return [[[ZKHDiscountData alloc] init] couponCountForOwner:owner];
}

- (int)discountCountForOwner:(NSString *)owner
{
    return [[[ZKHDiscountData alloc] init] discountCountForOwner:owner];
}

#define GET_ACTIVITIES_BY_OWNER_URL(_OWNER_, _UPDATE_TIME_) [NSString stringWithFormat:@"/getActivitiesByOwner/%@/%@", _OWNER_, _UPDATE_TIME_]
#define GET_COUPONS_BY_OWNER_URL(_OWNER_, _UPDATE_TIME_) [NSString stringWithFormat:@"/getCouponsByOwner/%@/%@", _OWNER_, _UPDATE_TIME_]
#define GET_DISCOUNTS_BY_OWNER_URL(_OWNER_, _UPDATE_TIME_) [NSString stringWithFormat:@"/getDiscountsByOwner/%@/%@", _OWNER_, _UPDATE_TIME_]

- (void)discountsRemoteRefresh:(NSString *)owner type:(NSString *)type completionHandler:(BooleanResultResponseBlock)discountsBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    static NSString *action = nil;
    if ([VAL_DISCOUNT_TYPE_ACTIVITY isEqualToString:type]) {
        action = SYNC_TABLE_ACTIVITIES_BY_OWNER;
    }else if ([VAL_DISCOUNT_TYPE_COUPON isEqualToString:type]){
        action = SYNC_TABLE_COUPONS_BY_OWNER;
    }else{
        action = SYNC_TABLE_DISCOUNTS_BY_OWNER;
    }
    
    ZKHSyncEntity *syncEntity = [[[ZKHSyncData alloc] init] syncEntity:action itemId:owner];
    NSString *updateTime = @"-1";
    if (syncEntity) {
        updateTime = syncEntity.updateTime;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    
    if ([VAL_DISCOUNT_TYPE_ACTIVITY isEqualToString:type]) {
        request.urlString = GET_ACTIVITIES_BY_OWNER_URL(owner, updateTime);
    }else if ([VAL_DISCOUNT_TYPE_COUPON isEqualToString:type]){
        request.urlString = GET_COUPONS_BY_OWNER_URL(owner, updateTime);
    }else{
        request.urlString = GET_DISCOUNTS_BY_OWNER_URL(owner, updateTime);
    }
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *updateTime = jsonObject[KEY_UPDATE_TIME];
        if (updateTime) {
            int count = 0;
            id data = jsonObject[KEY_DATA];
            if (data) {
                count = [self saveReponseDiscountsData:data];
            }
            
            //记录更新时间
            ZKHSyncEntity *syncEntity = [[[ZKHSyncData alloc] init] syncEntity:action itemId:owner];
            if (!syncEntity) {
                syncEntity = [[ZKHSyncEntity alloc] init];
                syncEntity.tableName = action;
                syncEntity.itemId = owner;
                syncEntity.uuid = [[NSDate currentTimeString] copy];
            }
            syncEntity.updateTime = updateTime;
            [[[ZKHSyncData alloc] init] save:@[syncEntity]];
            if (count) {
                discountsBlock(true);
            }else{
                discountsBlock(false);
            }
        }else{
            discountsBlock(false);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];

}

- (NSMutableArray *)activitiesGroupByPublishDate:(NSString *)owner offset:(long)offset
{
    return [[ZKHDiscountData alloc] activitiesGroupByPublishDate:owner offset:offset];
}

- (NSMutableArray *)couponsGroupByPublishDate:(NSString *)owner offset:(long)offset 
{
   return [[[ZKHDiscountData alloc] init] couponsGroupByPublishDate:owner offset:offset];
}

- (void)discountsGroupByPublishDate:(NSString *)owner offset:(long)offset completionHandler:(DiscountsResponseBlock) discountsBlock errorHandler:(RestResponseErrorBlock) errorBlock
{
    if (offset > 0) {
        discountsBlock([[[ZKHDiscountData alloc] init] discountsGroupByPublishDate:owner offset:offset]);
        return;
    }
    
    [self discountsRemoteRefresh:owner type:VAL_DISCOUNT_TYPE_ALL completionHandler:^(Boolean result) {
        
        discountsBlock([[[ZKHDiscountData alloc] init] discountsGroupByPublishDate:owner offset:offset]);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define VISIT_DISCOUNT_URI @"/visitDiscount"
- (void)visitDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock)visitDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_POST;
    request.urlString = VISIT_DISCOUNT_URI;
    request.params = @{KEY_DISCOUNT_ID: discount.uuid};
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            visitDiscountBlock(false);
        }else{
            [[[ZKHDiscountData alloc] init] increaseVisitCount:discount];
            visitDiscountBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define GET_NEWEST_ACTIVITIES_URL @"/getNewestActivities"
- (void)newestActivities:(long)offset completionHandler:(DiscountsResponseBlock)activitiesBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        [self showLocationAlertView];
        activitiesBlock([[NSMutableArray alloc] init]);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%f/%f/%ld", GET_NEWEST_ACTIVITIES_URL, [location.cityName mk_urlEncodedString], location.lat, location.lng, offset];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray *discounts = [[NSMutableArray alloc] init];
        id data = jsonObject[KEY_DATA];
        if (data) {
            discounts = [self assembleDiscounts:data];
        }
                
        activitiesBlock(discounts);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}


#define GET_ACTIVITIES_BY_DISTANCE_URL @"/getActivitiesByDistance"
- (void)activitiesForCategory:(NSString *)categoryId offset:(long)offset completionHandler:(DiscountsResponseBlock)activitiesBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        [self showLocationAlertView];
        activitiesBlock(nil);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%ld/%@/%f/%f/%d",GET_ACTIVITIES_BY_DISTANCE_URL, categoryId, offset, [location.cityName mk_urlEncodedString], location.lat, location.lng, KEY_VERSION];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        id data = jsonObject[KEY_DATA];
        NSMutableArray *discounts = [self assembleDiscounts:data];
        activitiesBlock(discounts);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define GET_COUPONS_BY_STORE_URL @"/getCouponsByStore"
- (void)couponsForStoreRemoteRefresh:(NSString *)storeId cateId:(NSString *)cateId offset:(long)offset completionHandler:(DiscountsResponseBlock)couponsBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        [self showLocationAlertView];
        couponsBlock(nil);
        return;
    }
    
//    NSString *updateTime;
//    ZKHSyncEntity *sync = [[[ZKHSyncData alloc] init] syncEntity:SYNC_TABLE_STORE_COUPON itemId:storeId];
//    if (sync) {
//        updateTime = sync.updateTime;
//    }else{
//        updateTime = @"-1";
//    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%f/%f/%ld", GET_COUPONS_BY_STORE_URL, storeId, cateId, [location.cityName mk_urlEncodedString], location.lat, location.lng, offset];
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        //NSString *updateTime = jsonObject[KEY_UPDATE_TIME];
        id data = jsonObject[KEY_DATA];
        couponsBlock([self assembleDiscounts:data]);
        //[self saveReponseDiscountsData:data];
        //[[[ZKHSyncData alloc] init] save:SYNC_TABLE_STORE_COUPON itemId:storeId updateTime:updateTime];
       // NSArray *discounts = data[KEY_DISCOUNTS];
//        if (discounts && [discounts count] > 0) {
//            couponsBlock([self couponsForStore:storeId cateId:cateId offset:0]);
//        }else{
//            couponsBlock(nil);
//        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

//- (NSMutableArray *)couponsForStore:(NSString *)storeId cateId:(NSString *)cateId offset:(long)offset
//{
//    ZKHLocation *location = [ZKHContext getInstance].location;
//    if (!location || !location.cityName) {
//        [self showLocationAlertView];
//        return nil;
//    }
//    return [[[ZKHDiscountData alloc] init] couponsForStore:storeId cateId:cateId offset:offset location:location];
//}

#define GET_STORES_HAS_COUPON_URL @"/getStoresHasCoupon"
- (void)storesHasCouponRemoteRefresh:(CouponStoresResponseBlock)couponStoresBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        [self showLocationAlertView];
        couponStoresBlock(nil);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@", GET_STORES_HAS_COUPON_URL, [location.cityName mk_urlEncodedString]];
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        id data = jsonObject[KEY_DATA];
        if (data) {
            NSMutableArray *stores = [[NSMutableArray alloc] init];
            for (id jsonStore in data) {
                ZKHCouponStoreEntity *store = [[ZKHCouponStoreEntity alloc] initWithJSONObject:jsonStore];
                store.cityName = location.cityName;
                [stores addObject:store];
            }
            if ([stores count] > 0) {
                ZKHCouponStoreData *data = [[ZKHCouponStoreData alloc] init];
                [data deleteCouponStoresByCity:location.cityName];
                [data save:stores];
                couponStoresBlock([self storesHasCouponFromLocal]);
                return;
            }
        }
        couponStoresBlock(nil);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (NSMutableDictionary *)storesHasCouponFromLocal
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        [self showLocationAlertView];
        return nil;
    }
    
    NSMutableDictionary *coupnStores = [[NSMutableDictionary alloc] init];
    ZKHCouponStoreData *storeData = [[ZKHCouponStoreData alloc] init];
    
    NSMutableArray *categories = [[[ZKHCategoryData alloc] init] categoriesHasCoupons:location];
    for (ZKHCategoryEntity *category in categories) {
        [coupnStores setObject:[storeData couponStoresByLocation:category.uuid location:location] forKey:category.name];
    }
    return coupnStores;
}

#define GET_DISCOUNTS_BY_SEARCHWORD_URL @"/getDiscountsByKeyword"
- (void)discountsByKeyword:(NSString *)searchWord offset:(long)offset completionHandler:(DiscountsResponseBlock)couponsBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        [self showLocationAlertView];
        couponsBlock(nil);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%f/%f/%@/%ld", GET_DISCOUNTS_BY_SEARCHWORD_URL,[location.cityName mk_urlEncodedString], location.lat, location.lng, [searchWord mk_urlEncodedString], offset];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        id data = jsonObject[KEY_DATA];
        NSMutableArray * discounts = [self assembleDiscounts:data];
        couponsBlock(discounts);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define GET_DISCOUNT_BY_ID_URL @"/getDiscountById"
- (void)discountsById:(NSString *)discountId completionHandler:(DiscountResponseBlock)discountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    ZKHDiscountEntity *discount = [[[ZKHDiscountData alloc] init] discountById:discountId location:location];
    if (discount) {
        discountBlock(discount);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@", GET_DISCOUNT_BY_ID_URL,discountId];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        id data = jsonObject[KEY_DATA];
        [self saveReponseDiscountsData:data];
        
        ZKHDiscountEntity *discount = [[[ZKHDiscountData alloc] init] discountById:discountId location:location];
        discountBlock(discount);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define GET_STOREFRONTS_BY_DISCOUNT_URL @"/getStorefrontsByDiscount"
- (void)storeFrontsByDiscount:(ZKHDiscountEntity *)discount limit:(int)limit offset:(long)offset completionHandler:(StorefrontsResponseBlock)storefrontsBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        [self showLocationAlertView];
        storefrontsBlock(nil);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%@/%f/%f/%d/%ld", GET_STOREFRONTS_BY_DISCOUNT_URL,discount.uuid,[location.cityName mk_urlEncodedString], location.lat, location.lng,limit,offset];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray *storefronts = [[NSMutableArray alloc] init];
        for (id json in jsonObject) {
            ZKHDiscountStorefrontEntity * front = [[ZKHDiscountStorefrontEntity alloc] initWithJSONObject:json initSuper:TRUE];
            front.discountId = discount.uuid;
            front.distance = [json[KEY_DISTANCE] doubleValue];
            [storefronts addObject:front];
        }
        storefrontsBlock(storefronts);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define GET_COMMENTS_BY_DISCOUNT_URL @"/getCommentsByDiscount"
- (void)commentsByDiscount:(ZKHDiscountEntity *)discount limit:(int)limit offset:(long)offset completionHandler:(CommentsResponseBlock)commentsBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%d/%ld", GET_COMMENTS_BY_DISCOUNT_URL,discount.uuid,limit,offset];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray *comments = [[NSMutableArray alloc] init];
        for (id json in jsonObject) {
            ZKHCommentEntity *comment = [[ZKHCommentEntity alloc] initWithJSONObject:json];
            [comments addObject:comment];
        }
        commentsBlock(comments);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
}

#define PUBLISH_COMMENT_URL @"/publishComment"
- (void)publishComment:(ZKHCommentEntity *) comment completionHandler:(CommentResponseBlock)commentBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_POST;
    request.urlString = PUBLISH_COMMENT_URL;
    request.params = @{KEY_DISCOUNT_ID: comment.discountId, KEY_CONTENT : comment.content
                       ,KEY_PUBLISHER: comment.publisher, KEY_TYPE: comment.type};
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            commentBlock(nil);
        }else{
            comment.uuid = uuid;
            comment.publishTime = jsonObject[KEY_PUBLISH_TIME];
            commentBlock(comment);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (void)favoritDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock)favoritDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    [self favoritDiscount:discount action:KEY_ACTION_FAVORIT completionHandler:^(Boolean result) {
        favoritDiscountBlock(result);
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

- (void)unfavoritDiscount:(ZKHDiscountEntity *)discount completionHandler:(BooleanResultResponseBlock)unfavoritDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    [self favoritDiscount:discount action:KEY_ACTION_UNFAVORIT completionHandler:^(Boolean result) {
        unfavoritDiscountBlock(result);
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

#define FAVORIT_DISCOUNT_URL @"/favoritDiscount"
- (void)favoritDiscount:(ZKHDiscountEntity *)discount action:(NSString *)action completionHandler:(BooleanResultResponseBlock)favoritDiscountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSString *userId = nil;
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        userId = user.uuid;
    }else{
        userId = [ZKHViewUtils deviceId];
    }
    if (!userId) {
        favoritDiscountBlock(FALSE);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_POST;
    request.urlString = FAVORIT_DISCOUNT_URL;
    request.params = @{KEY_DISCOUNT_ID: discount.uuid, KEY_USER_ID:userId, KEY_TYPE: action};
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *updateTime = jsonObject[KEY_UPDATE_TIME];
        if ([NSString isNull:updateTime]) {
            favoritDiscountBlock(FALSE);
        }else{
            if ([action isEqualToString:KEY_ACTION_UNFAVORIT]) {
                [[[ZKHFavoritData alloc] init] deleteFavorit:userId discountId:discount.uuid];
            }else{
                ZKHFavoritEntity *favorit = [[ZKHFavoritEntity alloc] init];
                favorit.userId = userId;
                favorit.discount = discount;
                favorit.uuid = jsonObject[KEY_DATA][KEY_UUID];
                [self saveFavoritLocal:favorit];
                
                //记录更新时间
                ZKHSyncEntity *syncEntity = [[[ZKHSyncData alloc] init] syncEntity:SYNC_TABLE_FAVORIT_DISCOUNT itemId:userId];
                if (!syncEntity) {
                    syncEntity = [[ZKHSyncEntity alloc] init];
                    syncEntity.tableName = SYNC_TABLE_FAVORIT_DISCOUNT;
                    syncEntity.itemId = userId;
                    syncEntity.uuid = [[NSDate currentTimeString] copy];
                }
                syncEntity.updateTime = updateTime;
                [[[ZKHSyncData alloc] init] save:@[syncEntity]];
            }
            favoritDiscountBlock(TRUE);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (Boolean) saveFavoritLocal:(ZKHFavoritEntity *) favorit
{
    [[[ZKHFavoritData alloc] init] save:@[favorit]];
    ZKHDiscountEntity *discount = favorit.discount;
    if (discount) {
        //保存活动
        [[[ZKHDiscountData alloc] init] save:@[discount]];
        //保存图片
        [[[ZKHDiscountImageData alloc] init] save:discount.images];
        //保存门店
        [[[ZKHDiscountStorefrontData alloc] init] save:discount.storefronts];
    }
    return TRUE;
}

- (Boolean)isUserFavorited:(NSString *)userId discountId:(NSString *)discountId
{
    return [[[ZKHFavoritData alloc] init] isFavorited:userId discountId:discountId];
}

- (void)userFavorits:(long)offset completionHandler:(DiscountsResponseBlock)discountsBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSString *userId = nil;
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        userId = user.uuid;
    }else{
        userId = [ZKHViewUtils deviceId];
    }
    if (!userId) {
        discountsBlock(nil);
        return;
    }
    
    if (offset == 0) {
        [self refreshUserFavorits:userId completionHandler:^(Boolean result) {
            discountsBlock([[[ZKHFavoritData alloc] init] userFavorits:userId offset:offset]);
            
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }else{
        discountsBlock([[[ZKHFavoritData alloc] init] userFavorits:userId offset:offset]);
    }
}

#define GET_FAVORITS_BY_USER_URL @"/getUserFavorits"
- (void)refreshUserFavorits:(NSString *)userId completionHandler:(BooleanResultResponseBlock)refreshBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        refreshBlock(false);
        return;
    }
    
    ZKHSyncEntity *syncEntity = [[[ZKHSyncData alloc] init] syncEntity:SYNC_TABLE_FAVORIT_DISCOUNT itemId:userId];
    NSString *updateTime = @"-1";
    if (syncEntity) {
        updateTime = syncEntity.updateTime;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%f/%f", GET_FAVORITS_BY_USER_URL,userId, updateTime,[location.cityName mk_urlEncodedString], location.lat, location.lng];
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *updateTime = jsonObject[KEY_UPDATE_TIME];
        if (updateTime) {
            id data = jsonObject[KEY_DATA];
            for (id row in data) {
                id jsonFavorit = row[KEY_FAVORIT];
                ZKHFavoritEntity *favorit = [[ZKHFavoritEntity alloc] init];
                favorit.uuid = jsonFavorit[KEY_UUID];
                favorit.userId = userId;
                
                id jsonDiscount = row[KEY_DISCOUNT];
                ZKHDiscountEntity *discount = [self assembleDiscount:jsonDiscount];
                if (jsonDiscount[KEY_STOREFRONT]) {
                    NSMutableArray *storefronts = [[NSMutableArray alloc] init];
                    ZKHDiscountStorefrontEntity *dsf = [[ZKHDiscountStorefrontEntity alloc] initWithJSONObject:jsonDiscount[KEY_STOREFRONT] initSuper:true];
                    dsf.discountId = discount.uuid;
                    [storefronts addObject:dsf];
                    discount.storefronts = storefronts;
                }
                favorit.discount = discount;
                [self saveFavoritLocal:favorit];
            }
            
            //记录更新时间
            ZKHSyncEntity *syncEntity = [[[ZKHSyncData alloc] init] syncEntity:SYNC_TABLE_FAVORIT_DISCOUNT itemId:userId];
            if (!syncEntity) {
                syncEntity = [[ZKHSyncEntity alloc] init];
                syncEntity.tableName = SYNC_TABLE_FAVORIT_DISCOUNT;
                syncEntity.itemId = userId;
                syncEntity.uuid = [[NSDate currentTimeString] copy];
            }
            syncEntity.updateTime = updateTime;
            [[[ZKHSyncData alloc] init] save:@[syncEntity]];
            
            refreshBlock(true);
        }else{
            refreshBlock(false);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define GET_DISCOUNT_EXT_WITH_FAVORIT_URL @"/getDiscountExtWithFavorit"
- (void)discountExtWithFavorit:(ZKHDiscountEntity *)discount completionHandler:(DiscountResponseBlock)discountBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        discountBlock(nil);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@/%@", GET_DISCOUNT_EXT_WITH_FAVORIT_URL,discount.uuid,[location.cityName mk_urlEncodedString]];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        if (jsonObject[KEY_FRONT_COUNT]) {
            discount.frontCount = [jsonObject[KEY_FRONT_COUNT] integerValue];
        }
        if (jsonObject[KEY_COMMENT_COUNT]) {
            discount.commentCount = [jsonObject[KEY_COMMENT_COUNT] integerValue];
        }
        if (jsonObject[KEY_COMMENTS]) {
            NSMutableArray *comments = [[NSMutableArray alloc] init];
            id jsonComments = jsonObject[KEY_COMMENTS];
            for (id jsonComment in jsonComments) {
                ZKHCommentEntity *comment = [[ZKHCommentEntity alloc] initWithJSONObject:jsonComment];
                [comments addObject:comment];
            }
            discount.comments = comments;
        }
        discountBlock(discount);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (void) showLocationAlertView
{
    [ZKHLocationUtils showLocationAlertView];
}
@end

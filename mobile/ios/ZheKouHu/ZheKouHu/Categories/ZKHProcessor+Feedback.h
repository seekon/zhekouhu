//
//  ZKHProcessor+Feedback.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"

@interface ZKHProcessor (Feedback)

- (void) submit:(NSString *)content contact:(NSString *)contact completionHandler:(BooleanResultResponseBlock) submitBlock errorHandler:(RestResponseErrorBlock) errorBlock;
@end

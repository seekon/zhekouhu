//
//  ZKHProcessor+Feedback.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+Feedback.h"
#import "ZKHConst.h"
#import "NSString+Utils.h"

@implementation ZKHProcessor (Feedback)

#define SUBMIT_FEEDBACK_URL @"/submitFeedback"
- (void)submit:(NSString *)content contact:(NSString *)contact completionHandler:(BooleanResultResponseBlock)submitBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_POST;
    request.urlString = SUBMIT_FEEDBACK_URL;
    request.params = @{KEY_CONTENT: content, KEY_CONTACT: contact};
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            submitBlock(false);
        }else{
            submitBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}
@end

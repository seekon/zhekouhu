//
//  ZKHProcessor+Message.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"

@interface ZKHProcessor (Message)

- (void)saveMessges:(NSMutableArray *)messages;

- (NSMutableArray *)messages:(int) offset;

- (void)deleteMessages;

- (void)deleteMessageById:(NSString *)msgId;

@end

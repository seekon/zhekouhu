//
//  ZKHProcessor+Message.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+Message.h"
#import "ZKHData.h"

@implementation ZKHProcessor (Message)

- (void)saveMessges:(NSMutableArray *)messages
{
    [[[ZKHMessageData alloc] init] save:messages];
}

- (NSMutableArray *)messages:(int) offset
{
    return [[[ZKHMessageData alloc] init] messages:offset];
}

- (void)deleteMessages
{
    [[[ZKHMessageData alloc] init] deleteMessages];
}

-(void)deleteMessageById:(NSString *)msgId
{
    [[[ZKHMessageData alloc] init] deleteMessageById:msgId];
}

@end

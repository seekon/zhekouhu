//
//  ZKHProcessor+Setting.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"
#import "ZKHEntity.h"

@interface ZKHProcessor (Setting)

- (ZKHSettingEntity *) settingByCode:(NSString *)code;

- (void) save:(NSArray *)settings;


typedef void (^StringResultResponseBlock)(NSString *result);
//检测版本
- (void) newestVersion:(StringResultResponseBlock) newestVersionBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//注册设备
- (void) registerDevice:(NSString *)deviceId  completionHandler:(BooleanResultResponseBlock) registerDeviceBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//设置设备不提醒消息
- (void) setNotifyState:(NSString *)deviceId state:(NSString *)state completionHandler:(BooleanResultResponseBlock) setUnNotifyBlock errorHandler:(RestResponseErrorBlock) errorBlock;

@end

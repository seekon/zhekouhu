//
//  ZKHProcessor+Setting.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+Setting.h"
#import "ZKHProcessor+Message.h"
#import "ZKHData.h"
#import "ZKHConst.h"
#import "NSString+Utils.h"

@implementation ZKHProcessor (Setting)

- (ZKHSettingEntity *) settingByCode:(NSString *)code
{
    return [[[ZKHSettingData alloc] init] settingByCode:code];
}

- (void)save:(NSArray *)settings
{
    [[[ZKHSettingData alloc] init] save:settings];
}

#define GET_NEWEST_VERSION_URL @"/getNewestVersion"
- (void)newestVersion:(StringResultResponseBlock)newestVersionBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = GET_NEWEST_VERSION_URL;
    request.method = METHOD_GET;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *version = [jsonObject valueForKey:KEY_VALUE];
        if (version) {
            newestVersionBlock(version);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

#define REGISTER_DEVICE_URL @"/registerDevice"
- (void)registerDevice:(NSString *)deviceId completionHandler:(BooleanResultResponseBlock)registerDeviceBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHSettingEntity *messageSetting = [self settingByCode:KEY_SETTING_MESSAGE_REMIND];
    if (messageSetting && [messageSetting.value isTrue]) {
        registerDeviceBlock(true);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = REGISTER_DEVICE_URL;
    request.method = METHOD_POST;
    request.params = @{KEY_DEV_ID: deviceId, KEY_TYPE : @"1"};
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        id devId = jsonObject[KEY_DEV_ID];
        if (devId) {
//            if (jsonObject[KEY_UUID]) {
//                ZKHMessageEntity *msg = [[ZKHMessageEntity alloc] init];
//                msg.uuid = jsonObject[KEY_UUID];
//                msg.sendTime = jsonObject[KEY_SEND_TIME];
//                msg.type = jsonObject[KEY_TYPE];
//                msg.msgId = jsonObject[KEY_MSG_ID];
//                msg.content = jsonObject[KEY_CONTENT];
//                
//                [[ZKHProcessor getInstance] saveMessges:[@[msg] mutableCopy]];
//            }
            registerDeviceBlock(true);
        }else{
            registerDeviceBlock(false);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define SET_NOTIFY_STATE_URL @"/setNotifyState"
- (void) setNotifyState:(NSString *)deviceId state:(NSString *)state completionHandler:(BooleanResultResponseBlock) setUnNotifyBlock errorHandler:(RestResponseErrorBlock) errorBlock;

{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = SET_NOTIFY_STATE_URL;
    request.method = METHOD_PUT;
    request.params = @{KEY_DEV_ID: deviceId, KEY_STATUS: state};
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        id devId = jsonObject[KEY_DEV_ID];
        if (devId) {
            setUnNotifyBlock(true);
        }else{
            setUnNotifyBlock(false);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

@end

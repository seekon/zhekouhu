//
//  ZKHProcessor+Store.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-23.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"
#import "ZKHEntity.h"

@interface ZKHProcessor (Store)

typedef void (^StoresResponseBlock)(NSMutableArray *stores);
typedef void (^StoreResponseBlock)(ZKHStoreEntity *store);
typedef void (^StorefrontResponseBlock)(ZKHStorefrontEntity *storefront);

- (void) storesForOwner:(NSString *)owner completionHandler:(StoresResponseBlock) storesBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) registerStore:(ZKHStoreEntity *)store completionHandler:(BooleanResultResponseBlock) resgisterStoreBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) deleteStore:(ZKHStoreEntity *)store completionHandler:(BooleanResultResponseBlock) deleteStoreBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) updateStore:(ZKHStoreEntity *)store completionHandler:(BooleanResultResponseBlock) updateStoreBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) deleteStorefront:(ZKHStorefrontEntity *)storefront completionHandler:(BooleanResultResponseBlock) deleteStorefrontBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) updateStorefront:(ZKHStorefrontEntity *)storefront completionHandler:(BooleanResultResponseBlock) updateStorefrontBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) addStorefront:(ZKHStorefrontEntity *)storefront completionHandler:(StorefrontResponseBlock) addStorefrontBlock errorHandler:(RestResponseErrorBlock) errorBlock;

//获取附件商家列表
- (void) nearbyStores:(NSString *)query radius:(int) radius offset:(int)offset completionHandler:(StoresResponseBlock) storesBlock errorHandler:(RestResponseErrorBlock) errorBlock;
@end

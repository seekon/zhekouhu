//
//  ZKHProcessor+Store.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-23.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+Store.h"
#import "ZKHConst.h"
#import "NSString+Utils.h"
#import "ZKHData.h"
#import "ZKHContext.h"
#import "ZKHLocationUtils.h"

@implementation ZKHProcessor (Store)

//获取店铺列表
#define GET_STORES_URL(_OWNER_, _UPDATE_TIME_) [NSString stringWithFormat:@"/getStores/%@/%@", _OWNER_,_UPDATE_TIME_]
- (void)storesForOwner:(NSString *)owner completionHandler:(StoresResponseBlock)storesBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSString *updateTime ;
    ZKHSyncEntity *sync = [[[ZKHSyncData alloc] init] syncEntity:STORE_TABLE itemId:owner];
    if (sync) {
        updateTime = sync.updateTime;
    }
    if (!updateTime) {
        updateTime = @"-1";
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = GET_STORES_URL(owner, updateTime);
    request.method = METHOD_GET;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray *stores = [[NSMutableArray alloc] init];
        NSString *updateTime = jsonObject[KEY_UPDATE_TIME];
        if (![NSString isNull:updateTime]){
            NSArray *data = jsonObject[KEY_DATA];
            if ([data count] > 0) {
                for (id jsonStore in data) {
                    ZKHStoreEntity *store = [[ZKHStoreEntity alloc] initWithJSONObject:jsonStore];
                    NSString *isDel = jsonStore[KEY_IS_DELETED];
                    if ([@"1" isEqualToString:isDel]) {
                        [[[ZKHStoreData alloc] init] deleteStore:store];
                        continue;
                    }
                    
                    id jsonFronts = jsonStore[KEY_STOREFRONTS];
                    for (id jsonFront in jsonFronts) {
                        isDel = jsonFront[KEY_IS_DELETED];
                        if ([@"1" isEqualToString:isDel]) {
                            [[[ZKHStorefrontData alloc] init] deleteById:jsonFront[KEY_UUID]];
                            continue;
                        }
                        
                        ZKHStorefrontEntity *storefront = [[ZKHStorefrontEntity alloc] initWithJSONObject:jsonFront];
                        [store.storefronts addObject:storefront];
                    }
                
                    [stores addObject:store];
                }
                [[[ZKHStoreData alloc] init] save:stores];
                [[[ZKHSyncData alloc] init] save:STORE_TABLE itemId:owner updateTime:updateTime];
            }
        }
        storesBlock([[[ZKHStoreData alloc] init] storesForOwner:owner]);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

//店铺注册
#define REGISTER_STORE_URL @"/registerStore"
- (void)registerStore:(ZKHStoreEntity *)store completionHandler:(BooleanResultResponseBlock)resgisterStoreBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSString *fronts = [self storefrontsJSONString:store.storefronts];
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = REGISTER_STORE_URL;
    request.method = METHOD_POST;
    request.params = @{KEY_NAME: store.name, KEY_LOGO: store.logo.aliasName, KEY_OWNER:store.owner, KEY_STOREFRONTS: fronts};
    request.files = @[store.logo];
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            resgisterStoreBlock(false);
        }else{
            store.uuid = uuid;
            store.registerTime = jsonObject[KEY_REGISTER_TIME];
            
            NSArray *fronts = jsonObject[KEY_STOREFRONTS];
            for (int i = 0; i < [fronts count]; i++) {
                ZKHStorefrontEntity *front = store.storefronts[i];
                front.uuid = [fronts[i] valueForKey:KEY_UUID];
                front.storeId = store.uuid;
            }
            
            [[[ZKHStoreData alloc] init] save:@[store]];
            [[[ZKHSyncData alloc] init] save:STORE_TABLE itemId:[ZKHContext getInstance].user.uuid updateTime:jsonObject[KEY_LAST_MODIFY_TIME]];
            
            resgisterStoreBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

//删除店铺
#define DEL_STORE_URL(_UUID_) [NSString stringWithFormat:@"/deleteStore/%@", _UUID_]
- (void)deleteStore:(ZKHStoreEntity *)store completionHandler:(BooleanResultResponseBlock)deleteStoreBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_DELETE;
    request.urlString = DEL_STORE_URL(store.uuid);
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            deleteStoreBlock(false);
        }else{
            [[[ZKHStoreData alloc] init] deleteStore:store];
            [[[ZKHSyncData alloc] init] save:STORE_TABLE itemId:[ZKHContext getInstance].user.uuid updateTime:jsonObject[KEY_LAST_MODIFY_TIME]];
            deleteStoreBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

//编辑保存店铺
#define UPDATE_STORE_URL @"/updateStore"
- (void)updateStore:(ZKHStoreEntity *)store completionHandler:(BooleanResultResponseBlock)updateStoreBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSString *fronts = [self storefrontsJSONString:store.storefronts];
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = UPDATE_STORE_URL;
    request.method = METHOD_PUT;
    request.params = @{KEY_UUID:store.uuid, KEY_NAME: store.name, KEY_LOGO: store.logo.aliasName, KEY_OWNER:store.owner, KEY_STOREFRONTS: fronts};
    request.files = @[store.logo];
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            updateStoreBlock(false);
        }else{
            NSArray *fronts = jsonObject[KEY_STOREFRONTS];
            if (fronts) {
                for (int i = 0; i < [fronts count]; i++) {
                    id jsonFront = fronts[i];
                    NSString *isDeleted = jsonFront[KEY_IS_DELETED];
                    if (isDeleted && [@"1" isEqualToString:isDeleted]) {
                        [[[ZKHStorefrontData alloc] init] deleteById:jsonFront[KEY_UUID]];
                        continue;
                    }
                    ZKHStorefrontEntity *front = store.storefronts[i];
                    front.uuid = jsonFront[KEY_UUID];
                }
            }
            [[[ZKHStoreData alloc] init] save:@[store]];
            [[[ZKHSyncData alloc] init] save:STORE_TABLE itemId:[ZKHContext getInstance].user.uuid updateTime:jsonObject[KEY_LAST_MODIFY_TIME]];
            
            updateStoreBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

//删除门店
#define DEL_STOREFRONT_URL(__UUID__) [NSString stringWithFormat:@"/deleteStorefront/%@", __UUID__]
- (void)deleteStorefront:(ZKHStorefrontEntity *)storefront completionHandler:(BooleanResultResponseBlock)deleteStorefrontBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_DELETE;
    request.urlString = DEL_STOREFRONT_URL(storefront.uuid);
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            deleteStorefrontBlock(false);
        }else{
            [[[ZKHStorefrontData alloc] init]deleteById:storefront.uuid];
            [[[ZKHSyncData alloc] init] save:STORE_TABLE itemId:[ZKHContext getInstance].user.uuid updateTime:jsonObject[KEY_LAST_MODIFY_TIME]];
            
            deleteStorefrontBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

//更新门店
#define UPDATE_STOREFRONT_URL @"/updateStorefront"
- (void)updateStorefront:(ZKHStorefrontEntity *)storefront completionHandler:(BooleanResultResponseBlock)updateStorefrontBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_PUT;
    request.urlString = UPDATE_STOREFRONT_URL;
    request.params = @{KEY_DATA: [self storefrontJSONString:storefront]};
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            updateStorefrontBlock(false);
        }else{
            [[[ZKHStorefrontData alloc] init] save:@[storefront]];
            [[[ZKHSyncData alloc] init] save:STORE_TABLE itemId:[ZKHContext getInstance].user.uuid updateTime:jsonObject[KEY_LAST_MODIFY_TIME]];
            
            updateStorefrontBlock(true);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

//添加门店
#define ADD_STOREFRONT_URL @"/addStorefront"
- (void)addStorefront:(ZKHStorefrontEntity *)storefront completionHandler:(StorefrontResponseBlock)addStorefrontBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_POST;
    request.urlString = ADD_STOREFRONT_URL;
    request.params = @{KEY_DATA: [self storefrontJSONString:storefront]};
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            addStorefrontBlock(nil);
        }else{
            storefront.uuid = uuid;
            [[[ZKHStorefrontData alloc] init] save:@[storefront]];
            [[[ZKHSyncData alloc] init] save:STORE_TABLE itemId:[ZKHContext getInstance].user.uuid updateTime:jsonObject[KEY_LAST_MODIFY_TIME]];
            
            addStorefrontBlock(storefront);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

- (NSString *) storefrontsJSONString:(NSMutableArray *)storefronts
{
    NSMutableString *fronts = [[NSMutableString alloc] init];
    [fronts appendString:@"["];
    for (ZKHStorefrontEntity *front in storefronts) {
        [fronts appendString:[self storefrontJSONString:front]];
    }
    [fronts appendString:@"]"];
    
    return fronts;
}

- (NSString *)storefrontJSONString:(ZKHStorefrontEntity *) front
{
    NSMutableString *fronts = [[NSMutableString alloc] init];
    [fronts appendString:@"{"];
    [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_NAME, front.name];
    [fronts appendFormat:@", \"%@\":\"%@\" ", KEY_ADDR, front.addr];
    if ([front.uuid length] > 0) {
        [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_UUID, front.uuid];
    }
    if ([front.storeId length] > 0) {
        [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_STORE_ID, front.storeId];
    }
    if ([front.phone length] > 0) {
        [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_PHONE, front.phone];
    }
    if ([front.latitude length] > 0) {
        [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_LATITUDE, front.latitude];
    }
    if ([front.longitude length] > 0) {
        [fronts appendFormat:@" \"%@\":\"%@\" ", KEY_LONGITUDE, front.longitude];
    }
    [fronts appendFormat:@", \"%@\":\"%@\" ", KEY_CITY, front.city.uuid];
    [fronts appendFormat:@", \"%@\":\"%@\" ", KEY_CITY_NAME, front.city.name];
    [fronts appendString:@"}"];
    return fronts;
}

#define KEY_BAIDU_AK @"Tg44W5KKibdXbVDCdjA7xWL1"
#define GET_NEARBY_STORES @"http://api.map.baidu.com/place/v2/search?output=json&scope=1"
- (void)nearbyStores:(NSString *)query radius:(int)radius offset:(int)offset completionHandler:(StoresResponseBlock)storesBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (!location || !location.cityName) {
        //[self showLocationAlertView];
        storesBlock([[NSMutableArray alloc] init]);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@&ak=%@&page_size=%d&page_num=%d&location=%f,%f&radius=%d", GET_NEARBY_STORES, KEY_BAIDU_AK, DEFAULT_PAGE_SIZE, (offset/DEFAULT_PAGE_SIZE), location.lat, location.lng, radius];
    if ([NSString isNull:query]) {
        request.urlString = [NSString stringWithFormat:@"%@&query=%@", request.urlString, [@"餐馆$宾馆$娱乐" mk_urlEncodedString]];
    }else{
        request.urlString = [NSString stringWithFormat:@"%@&query=%@", request.urlString, [query mk_urlEncodedString]];
    }
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        long status = [jsonObject[KEY_STATUS] integerValue];
        if (status == 0) {
            storesBlock([self assembleStores_baidu:jsonObject[@"results"]]);
        }else{
            storesBlock(nil);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (NSMutableArray *)assembleStores_baidu:(id)data
{
    if (!data) {
        return nil;
    }
    ZKHLocation *location = [ZKHContext getInstance].location;
    NSMutableArray *storeList = [[NSMutableArray alloc] init];
    for (id json in data) {
        ZKHNearbyStoreEntity *store = [[ZKHNearbyStoreEntity alloc] init];
        store.uuid = json[@"uid"];
        store.name = json[@"name"];
        store.addr = json[@"address"];
        store.phone = json[@"telephone"];
        store.lat = [json[@"location"][@"lat"] doubleValue];
        store.lng = [json[@"location"][@"lng"] doubleValue];
        store.cityName = location.cityName;
        store.distance = [ZKHLocationUtils latlngDist:store.lat lng1:store.lng lat2:location.lat lng2:location.lng];
        
        [storeList addObject:store];
    }
    return storeList;
}

@end

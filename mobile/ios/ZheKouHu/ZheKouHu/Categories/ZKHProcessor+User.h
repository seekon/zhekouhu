//
//  ZKHProcessor+User.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"
#import "ZKHEntity.h"

@interface ZKHProcessor (User)

typedef void (^LoginResponseBlock)(NSMutableDictionary *authObj);

- (void) autoLogin:(BooleanResultResponseBlock) loginBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) login: (NSString *) code pwd:(NSString *)pwd completionHandler:(LoginResponseBlock) loginBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) remoteLogin: (NSString *) code pwd:(NSString *)pwd completionHandler:(LoginResponseBlock) loginBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (ZKHUserEntity *) localLogin;

- (ZKHUserEntity *) localLogin: (NSString *) code pwd:(NSString *)pwd;

- (void) registerUser:(ZKHUserEntity *)user completionHandler:(BooleanResultResponseBlock) resgisterUserBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) updatePwd:(NSString *)uuid pwd:(NSString *)pwd oldPwd:(NSString *)oldPwd completionHandler:(LoginResponseBlock) updatePwdBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (void) resetPwd:(NSString *) code  completionHandler:(LoginResponseBlock) resetPwdBlock errorHandler:(RestResponseErrorBlock) errorBlock;

- (NSMutableDictionary *)getLastLoginEnv;

- (Boolean) logout;

- (Boolean) isStoreRegistered;

- (void) saveUserProfile:(ZKHUserProfileEntity *)profile completionHandler:(BooleanResultResponseBlock) saveUserProfileBlock errorHandler:(RestResponseErrorBlock) errorBlock;

typedef void (^ProfileResponseBlock)(ZKHUserProfileEntity *profile);
- (void) userAward:(ProfileResponseBlock) profileBlock errorHandler:(RestResponseErrorBlock) errorBlock;

typedef void (^UserAwardsResponseBlock)(NSMutableArray *userAwards);
- (void) userAwardDetails:(int) offset completionHandler: (UserAwardsResponseBlock) userAwardsBlock errorHandler:(RestResponseErrorBlock) errorBlock;


@end

//
//  ZKHProcessor+User.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor+User.h"
#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHContext.h"
#import "NSString+Utils.h"

@implementation ZKHProcessor (User)

- (void)autoLogin:(BooleanResultResponseBlock)loginBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    if ([ZKHContext getInstance].user) {
        loginBlock(true);
        return;
    }
    
    NSMutableDictionary *loginEnv = [self getLastLoginEnv];
    if (loginEnv != nil && [loginEnv count] > 0) {
        NSString *code = [loginEnv valueForKey:KEY_CODE];
        NSString *pwd = [loginEnv valueForKey:KEY_PWD];
        
        [self login:code pwd:pwd completionHandler:^(NSMutableDictionary *authObj) {
            Boolean authed = [[authObj objectForKey:KEY_AUTHED] boolValue];
            loginBlock(authed);
        } errorHandler:^(ZKHErrorEntity *error) {
            errorBlock(error);
        }];
    }else{
        loginBlock(false);
    }
}

- (void)login:(NSString *)code pwd:(NSString *)pwd completionHandler:(LoginResponseBlock)loginBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHUserEntity *user = [self localLogin:code pwd:pwd];
    if (user) {
        loginBlock([[NSMutableDictionary alloc] initWithDictionary: @{KEY_USER: user, KEY_AUTHED: @"true"}]);
        return;
    }
    
    [self remoteLogin:code pwd:pwd completionHandler:^(NSMutableDictionary *authObj) {
        [self recordStoreRegistered];
        loginBlock(authObj);
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

- (ZKHUserEntity *) localLogin
{
    NSMutableDictionary *loginEnv = [self getLastLoginEnv];
    if (loginEnv != nil && [loginEnv count] > 0) {
        NSString *code = [loginEnv valueForKey:KEY_CODE];
        NSString *pwd = [loginEnv valueForKey:KEY_PWD];
        return [self localLogin:code pwd:pwd];
    }
    return nil;
}

- (ZKHUserEntity *)localLogin:(NSString *)code pwd:(NSString *)pwd
{
    ZKHUserData *data = [[ZKHUserData alloc] init];
    ZKHUserEntity *user = [data user:code];
    if ([pwd isEqualToString:user.pwd]) {
        Boolean ownStores = [[[ZKHStoreData alloc] init] ownStores:user.uuid];
        user.type = ownStores ? VAL_USER_TYPE_STORER : VAL_USER_TYPE_CUSTOMER;
        user.profile = [[[ZKHUserProfileData alloc] init] profileByUserId:user.uuid];
        
        [ZKHContext getInstance].user = user;
        return user;
    }
    return nil;
}

#define LOGIN_URL @"/login"
- (void)remoteLogin:(NSString *)code pwd:(NSString *)pwd completionHandler:(LoginResponseBlock)loginBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSDictionary *params = @{KEY_CODE : code, KEY_PWD : pwd};
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = LOGIN_URL;
    request.params = params;
    request.method = METHOD_POST;
    
    [restClient execute:request completionHandler:^(NSHTTPURLResponse *response, id jsonObject) {
        Boolean authed = [[jsonObject valueForKey:KEY_AUTHED] boolValue];
        switch (authed) {
            case 1:
            {
                NSDictionary *userJson = [jsonObject valueForKey:KEY_USER];
                ZKHUserEntity *user = [[ZKHUserEntity alloc] initWithJsonObject:userJson noPwd:false];
                user.type = userJson[KEY_TYPE];
                
                id jsonProfile = userJson[KEY_PROFILE];
                if (jsonProfile) {
                    user.profile = [[ZKHUserProfileEntity alloc] initWithJsonObject:jsonProfile];
                }
                
                if (user.profile) {
                    [[[ZKHUserProfileData alloc] init] save:@[user.profile]];
                }
                [[[ZKHUserData alloc] init] save:@[user]];
                [self saveLoginEnv:user];
                
                [ZKHContext getInstance].user = user;
                [self setCookie:response];
            }
                break;
                
            default:
                break;
        }
        
        loginBlock(jsonObject);
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];

}

//会员注册
#define REGISTER_USER_URL @"/registerUser"
- (void)registerUser:(ZKHUserEntity *)user completionHandler:(BooleanResultResponseBlock)resgisterUserBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_POST;
    request.urlString = REGISTER_USER_URL;
    request.params = @{KEY_CODE: user.code, KEY_PWD: user.pwd};
    
    [restClient execute:request completionHandler:^(NSHTTPURLResponse *response, id jsonObject) {
        NSString *uuid = [jsonObject valueForKey:KEY_UUID];
        if (uuid != nil) {
            user.uuid = uuid;
            user.registerTime = [jsonObject valueForKey:KEY_REGISTER_TIME];
            user.pwd = [jsonObject valueForKey:KEY_PWD];
            
            [[[ZKHUserData alloc] init] save:@[user]];
            [self saveLoginEnv:user];
            [self recordStoreRegistered];
            
            [ZKHContext getInstance].user = user;
            [self setCookie:response];
            
            resgisterUserBlock(true);
        }else{
            resgisterUserBlock(false);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

//修改密码
#define UPDATE_PWD_URL @"/updatePwd"
- (void)updatePwd:(NSString *)uuid pwd:(NSString *)pwd oldPwd:(NSString *)oldPwd completionHandler:(LoginResponseBlock)updatePwdBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_PUT;
    request.urlString = UPDATE_PWD_URL;
    request.params = @{KEY_UUID: uuid, KEY_PWD :pwd, KEY_OLD_PWD: oldPwd};
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if ([NSString isNull:uuid]) {
            updatePwdBlock(jsonObject);
        }else{
            ZKHUserEntity *user = [ZKHContext getInstance].user;
            user.pwd = jsonObject[KEY_PWD];
            
            [[[ZKHUserData alloc] init] updateUserPwd:uuid pwd:user.pwd];
            [self saveLoginEnv:user];
            
            updatePwdBlock(jsonObject);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define RESET_PWD_URL @"/resetPwd"
-(void)resetPwd:(NSString *)code completionHandler:(LoginResponseBlock)resetPwdBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.method = METHOD_GET;
    request.urlString = [NSString stringWithFormat:@"%@/%@", RESET_PWD_URL, [code mk_urlEncodedString]];
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        resetPwdBlock(jsonObject);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (void) setCookie:(NSHTTPURLResponse *)response
{
    NSString *cookieValue = [[response allHeaderFields] valueForKey:KET_SET_COOKIE];
    if (cookieValue != nil) {
        [ZKHContext getInstance].sessionId = cookieValue;
    }
}

- (void)saveLoginEnv:(ZKHUserEntity *)user
{
    NSString *code = user.code;
    NSMutableDictionary *loginEnv = [[NSMutableDictionary alloc] init];
    [loginEnv setObject:code forKey:KEY_CODE];
    [loginEnv setObject:user.pwd forKey:KEY_PWD];
    
    [[[ZKHEnvData alloc] init] saveLoginEnv:code value:loginEnv];
}

- (NSMutableDictionary *)getLastLoginEnv
{
    return [[[ZKHEnvData alloc] init] lastLoginEnv];
}

- (void) recordStoreRegistered
{
    ZKHSettingEntity *store = [[ZKHSettingEntity alloc] init];
    store.uuid = KEY_SETTING_STORE_REGISTERED;
    store.value = @"1";
    [[[ZKHSettingData alloc] init] save:@[store]];
}

- (Boolean)logout
{
    @try {
        ZKHUserEntity *user = [ZKHContext getInstance].user;
        
        [[[ZKHEnvData alloc] init] deleteLoginEnv:user.code];
        [[[ZKHUserData alloc] init] deleteUser:user];
        
        [ZKHContext getInstance].user = nil;
        [ZKHContext getInstance].sessionId = nil;
        
        return true;
    }
    @catch (NSException *exception) {
        return false;
    }
    @finally {
        
    }
}

- (Boolean)isStoreRegistered
{
    ZKHSettingEntity *setting = [[[ZKHSettingData alloc] init] settingByCode:KEY_SETTING_STORE_REGISTERED];
    
    return setting && setting.value && [setting.value isTrue];
}

#define UPDATE_PROFILE_URI @"/updateUserProfile"
- (void)saveUserProfile:(ZKHUserProfileEntity *)profile completionHandler:(BooleanResultResponseBlock)saveUserProfileBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = UPDATE_PROFILE_URI;
    request.method = METHOD_PUT;
    request.params = [@{KEY_UUID:profile.uuid, KEY_NAME: profile.name, KEY_PHONE: profile.phone, KEY_DELIVER_ADDR: profile.devilerAddr} mutableCopy];
    if (profile.photo && profile.photo.aliasName) {
        [request.params setValue:profile.photo.aliasName forKey:KEY_PHOTO];
        request.files = @[profile.photo];
    }
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSString *uuid = jsonObject[KEY_UUID];
        if (uuid) {
            [[[ZKHUserProfileData alloc] init] save:@[profile]];
            saveUserProfileBlock(true);
        }else{
            saveUserProfileBlock(false);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

#define GET_USER_AWARD @"/getUserAward"
- (void)userAward:(ProfileResponseBlock)profileBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (!user) {
        profileBlock(nil);
        return;
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = [NSString stringWithFormat:@"%@/%@", GET_USER_AWARD, user.uuid];
    request.method = METHOD_GET;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        if (jsonObject[KEY_UUID]) {
            ZKHUserProfileEntity *profile = user.profile;
            if (!profile) {
                profile = [[ZKHUserProfileEntity alloc] init];
            }
            profile.awardSum = jsonObject[KEY_AWARD_SUM];
            profile.awardUsed = jsonObject[KEY_AWARD_USED];
            if (profile.uuid) {
                [[[ZKHUserProfileData alloc] init] save:@[profile]];
                user.profile = profile;
            }
            profileBlock(profile);
        }else{
            profileBlock(nil);
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#define GET_USER_AWARD_DETAIL @"/getUserAwardDetail"
- (void)userAwardDetails:(int)offset completionHandler:(UserAwardsResponseBlock)userAwardsBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (!user) {
        userAwardsBlock(nil);
    }
    
    ZKHRestRequest *request = [[ZKHRestRequest alloc] init];
    request.urlString = [NSString stringWithFormat:@"%@/%@/%d", GET_USER_AWARD_DETAIL, user.uuid, offset];
    request.method = METHOD_GET;
    
    [restClient executeWithJsonResponse:request completionHandler:^(id jsonObject) {
        NSMutableArray *data = [[NSMutableArray alloc] init];
        for (id json in jsonObject) {
            [data addObject:[[ZKHUserAwardEntity alloc] initWithJSONObject:json]];
        }
        userAwardsBlock(data);
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

@end

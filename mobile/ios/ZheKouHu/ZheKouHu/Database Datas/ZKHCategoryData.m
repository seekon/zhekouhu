
#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"

#define CATEGORY_TABLE @"z_category"
#define CATEGORY_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ integer) ", CATEGORY_TABLE, KEY_UUID, KEY_NAME, KEY_PARENT_ID, KEY_ICON, KEY_ORD_INDEX]
#define CATEGORY_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@,%@, %@,  %@) values (?, ?, ?, ?, ?)", CATEGORY_TABLE, KEY_UUID, KEY_NAME, KEY_PARENT_ID, KEY_ICON,KEY_ORD_INDEX]
#define CATEGORY_QUERY_BASE_SQL [NSString stringWithFormat:@" select %@, %@, %@, %@ from %@ c ", KEY_UUID, KEY_NAME, KEY_ICON, KEY_ORD_INDEX, CATEGORY_TABLE]

@implementation ZKHCategoryData

- (id)init
{
    if (self = [super init]) {
        [self createTable:CATEGORY_CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    for (ZKHCategoryEntity *category in data) {
        [self executeUpdate:CATEGORY_UPDATE_SQL params:@[category.uuid, category.name, category.parentId, category.icon, [NSString stringWithFormat:@"%d", category.ordIndex]]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHCategoryEntity *category = [[ZKHCategoryEntity alloc] init];
    
    int i = 0;
    
    category.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    category.name = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    category.icon = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    category.ordIndex = sqlite3_column_int(stmt, i++);
    
    return category;
}

- (NSMutableArray *)categories
{
    NSString *sql = [NSString stringWithFormat:@"%@ order by %@", CATEGORY_QUERY_BASE_SQL, KEY_ORD_INDEX];
    return [self query:sql params:nil];
}

- (ZKHCategoryEntity *)categoryById:(NSString *)uuid
{
    NSString *sql = [NSString stringWithFormat:@"%@ where %@ = ? ", CATEGORY_QUERY_BASE_SQL, KEY_UUID];
    return [self queryOne:sql params:@[uuid]];
}

- (NSMutableArray *)categoriesHasCoupons:(ZKHLocation *) location
{
    NSMutableString *sql = [NSMutableString stringWithString:CATEGORY_QUERY_BASE_SQL];
    [sql appendString:@" WHERE EXISTS (select 1 from z_coupon_store cs where cs.category_id = c.uuid and cs.city_name = ?) order by c.ord_index "];
    return [self query:sql params:@[location.cityName]];
}
@end

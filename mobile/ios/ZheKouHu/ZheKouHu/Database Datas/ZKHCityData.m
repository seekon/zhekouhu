
#import "ZKHData.h"
#import "ZKHConst.h"

#define CITY_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text) ", CITY_TABLE, KEY_UUID, KEY_NAME, KEY_FIRST_LETTER]
#define CITY_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@) values (?, ?, ?)", CITY_TABLE, KEY_UUID, KEY_NAME, KEY_FIRST_LETTER]
#define CITY_QUERY_BASE_SQL [NSString stringWithFormat:@" select %@, %@, %@ from %@  ", KEY_UUID, KEY_NAME, KEY_FIRST_LETTER, CITY_TABLE]

@implementation ZKHCityData

- (id)init
{
    if (self = [super init]) {
        [self createTable:CITY_CREATE_SQL];
    }
    return self;
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHCityEntity *city = [[ZKHCityEntity alloc] init];
    
    int i = 0;
    city.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    city.name = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    city.firstLetter = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    return city;
}

- (void)save:(NSArray *)data
{
    for (ZKHCityEntity *city in data) {
        [self executeUpdate:CITY_UPDATE_SQL params:@[city.uuid, city.name, city.firstLetter]];
    }
}
- (void)save:(NSString *)uuid name:(NSString *)name
{
    NSString *sql = [NSString stringWithFormat:@"%@ where uuid = ?", CITY_QUERY_BASE_SQL];
    ZKHCityEntity *city = [self queryOne:sql params:@[uuid]];
    if (city) {
        city.name = name;
    }else{
        city = [[ZKHCityEntity alloc] init];
        city.uuid = uuid;
        city.name = name;
        city.firstLetter = @"";
    }
    [self save:@[city]];
}

- (NSMutableArray *)cities
{
    NSString *sql =[NSString stringWithFormat:@"%@ order by %@ ", CITY_QUERY_BASE_SQL, KEY_FIRST_LETTER];
    return [self query:sql params:nil];
}
@end

#import "ZKHData.h"
#import "ZKHConst.h"

#define COUPON_STORE_TABLE @"z_coupon_store"
#define COUPON_STORE_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ text, %@ text) ", COUPON_STORE_TABLE, KEY_UUID, KEY_STORE_ID, KEY_NAME, KEY_LOGO, KEY_CITY_NAME, KEY_CATEGORY_ID]
#define COUPON_STORE_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@,%@, %@, %@) values (?, ?, ?,?, ?, ?)", COUPON_STORE_TABLE, KEY_UUID, KEY_STORE_ID, KEY_NAME, KEY_LOGO, KEY_CITY_NAME, KEY_CATEGORY_ID]
#define COUPON_STORE_QUERY_SQL [NSString stringWithFormat:@" select %@, %@, %@,%@, %@, %@ from %@ ", KEY_UUID, KEY_STORE_ID, KEY_NAME, KEY_LOGO, KEY_CITY_NAME, KEY_CATEGORY_ID, COUPON_STORE_TABLE]

@implementation ZKHCouponStoreData

- (id)init
{
    if (self = [super init]) {
        [self createTable:COUPON_STORE_CREATE_SQL];
    }
    return self;
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHCouponStoreEntity *store = [[ZKHCouponStoreEntity alloc] init];
    
    int i = 0;
    store.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    store.storeId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    store.name = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    store.logo = [[ZKHFileEntity alloc] init];
    store.logo.aliasName = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    store.cityName = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    store.categoryId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];

    return store;
}

- (void)save:(NSArray *)data
{
    for (ZKHCouponStoreEntity *store in data) {
        [self executeUpdate:COUPON_STORE_UPDATE_SQL params:@[store.uuid, store.storeId, store.name, store.logo.aliasName, store.cityName, store.categoryId]];
    }
}

- (NSMutableArray *)couponStoresByLocation:(NSString *)categoryId location:(ZKHLocation *)location
{
    NSString *sql = [NSString stringWithFormat:@"%@ where category_id = ? and city_name = ?", COUPON_STORE_QUERY_SQL];
    return [self query:sql params:@[categoryId, location.cityName]];
}

- (void)deleteCouponStoresByCity:(NSString *)city
{
    [self executeUpdate:@" delete from z_coupon_store where city_name = ? " params:@[city]];
}

@end
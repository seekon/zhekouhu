//
//  ZKHData.h
//  ZheKouHu
//
//  Created by undyliu on 14-5-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ZKHEntity.h"

@protocol ZKHEntityUpdater <NSObject>

@optional
- (void) save:(NSArray *) data;

@end

@interface ZKHData : NSObject

- (sqlite3 *)openDatabase;
- (void) closeDatabase: (sqlite3 *) database;
- (sqlite3_stmt *) prepareStatement:(NSString *) sql params:(NSArray *) params database:(sqlite3 *) db;

- (void) createTable:(NSString *) sql;
- (void) executeUpdate: (NSString *) sql params:(NSArray *) params;
- (NSMutableArray *) query: (NSString *) sql params:(NSArray *) params;
- (id) queryOne: (NSString *) sql params:(NSArray *) params;
- (id) processRow:(sqlite3_stmt *)stmt;
- (int)queryCount:(NSString *)sql params:(NSArray *)params;
- (Boolean) checkColumnExist:(NSString *)tableName columnName:(NSString *)columnName;
@end

@interface ZKHSettingData : ZKHData<ZKHEntityUpdater>
- (ZKHSettingEntity *) settingByCode:(NSString *)code;
@end

@interface ZKHCategoryData : ZKHData <ZKHEntityUpdater>
- (NSMutableArray *) categories;
- (ZKHCategoryEntity *) categoryById:(NSString *)uuid;
- (NSMutableArray *) categoriesHasCoupons:(ZKHLocation *) location;
@end

#define CITY_TABLE @"z_city"
@interface ZKHCityData : ZKHData <ZKHEntityUpdater>
- (NSMutableArray *) cities;
- (void) save:(NSString *)uuid name:(NSString *)name;
@end

@interface ZKHUserData : ZKHData <ZKHEntityUpdater>
- (ZKHUserEntity *) user: (NSString *)code;
- (void) updateUserPwd:(NSString *) uuid pwd:(NSString *)pwd;
- (void) deleteUser:(ZKHUserEntity *)user;
@end

@interface ZKHEnvData : ZKHData
- (void) saveLoginEnv:(NSString *)code value:(NSMutableDictionary *) value;
- (NSMutableDictionary *) loginEnv:(NSString *)code;
- (NSMutableDictionary *) lastLoginEnv;
- (void) deleteLoginEnv:(NSString *)code;

@end

@interface ZKHSyncData : ZKHData <ZKHEntityUpdater>
- (ZKHSyncEntity *) syncEntity:(NSString *)tableName itemId:(NSString *)itemId;
- (void) save:(NSString *)tableName itemId:(NSString *) itemId updateTime:(NSString *) updateTime;
@end

#define STORE_TABLE @"z_store"
@interface ZKHStoreData : ZKHData <ZKHEntityUpdater>
- (NSMutableArray *)storesForOwner:(NSString *)owner;
- (NSMutableArray *)storesHasCouponsForCategory:(NSString *)categoryId;
- (void) deleteStore:(ZKHStoreEntity *)store;
- (void) save:(NSString *)uuid name:(NSString *)name;
- (Boolean) ownStores:(NSString *)owner;
@end

@interface ZKHStorefrontData : ZKHData <ZKHEntityUpdater>
- (NSMutableArray *)storefrontsForStore:(NSString *)storeId;
- (NSMutableArray *)storefrontsForDiscount:(NSString *)discountId;
- (ZKHStorefrontEntity *) storefrontById:(NSString *)uuid;
- (void)deleteById:(NSString *)uuid;
- (void)deleteByStore:(ZKHStoreEntity *)store;
@end

@interface ZKHDiscountData : ZKHData <ZKHEntityUpdater>
- (int) activityCountForOwner:(NSString *)owner;
- (int) couponCountForOwner:(NSString *)owner;
- (int) discountCountForOwner:(NSString *)owner;

- (NSMutableArray *)activitiesGroupByPublishDate:(NSString *)owner offset:(int)offset;
- (NSMutableArray *)couponsGroupByPublishDate:(NSString *)owner offset:(int)offset;
- (NSMutableArray *)discountsGroupByPublishDate:(NSString *)owner offset:(int)offset;

- (void) deleteDiscount:(NSString *) uuid;

- (NSMutableArray *)couponsForStore:(NSString *)storeId cateId:(NSString *)cateId offset:(int)offset location:(ZKHLocation *)location;

- (ZKHDiscountEntity *)discountById:(NSString *)uuid location:(ZKHLocation *)location;

- (void)increaseVisitCount:(ZKHDiscountEntity *) discount;
@end

@interface ZKHDiscountImageData : ZKHData <ZKHEntityUpdater>
- (NSMutableArray *) imagesByDiscount:(NSString *)discountId;
- (void) deleteByDiscount:(NSString *)discountId;
- (void) deleteByDiscountImage:(NSString *)discountId image:(NSString *)image;
- (void) deleteById:(NSString *)uuid;
@end

@interface ZKHDiscountStorefrontData : ZKHStorefrontData
- (NSMutableArray *) storefrontsByDiscount:(NSString *) discountId location:(ZKHLocation *)location;
- (void) deleteByDiscount:(NSString *)discountId;
- (void) deleteByDiscountStorefront:(NSString *)discountId storefrontId:(NSString *)storefrontId;
- (void) deleteById:(NSString *)uuid;
@end

@interface ZKHCouponStoreData : ZKHData<ZKHEntityUpdater>
- (NSMutableArray *) couponStoresByLocation:(NSString *)categoryId location:(ZKHLocation *)location;
- (void) deleteCouponStoresByCity:(NSString *)city;
@end

@interface ZKHMessageData : ZKHData<ZKHEntityUpdater>
- (NSMutableArray *)messages:(int) offset;
- (void)deleteMessages;
- (void)deleteMessageById:(NSString *)id;
@end

@interface ZKHFavoritData : ZKHData<ZKHEntityUpdater>
- (NSMutableArray *) userFavorits:(NSString *)userId offset:(int)offset;
- (void) deleteFavorit:(NSString *)userId discountId:(NSString *)discountId;
- (Boolean) isFavorited:(NSString *)userId discountId:(NSString *)discountId;
@end

@interface ZKHUserProfileData : ZKHData<ZKHEntityUpdater>
- (ZKHUserProfileEntity *)profileByUserId:(NSString *)userId;
- (void)deleteById:(NSString *)uuid;
@end
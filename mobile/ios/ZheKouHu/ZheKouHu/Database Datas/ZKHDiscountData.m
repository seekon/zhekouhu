
#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"
#import "NSDate+Utils.h"

#define DISCOUNT_TABLE @"z_discount"
#define DISCOUNT_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text) ", DISCOUNT_TABLE, KEY_UUID, KEY_CONTENT, KEY_START_DATE, KEY_END_DATE, KEY_TYPE, KEY_CATEGORY_ID, KEY_PUBLISHER, KEY_PUBLISH_DATE, KEY_PUBLISH_TIME,KEY_VISIT_COUNT,KEY_PAR_VALUE, KEY_ORIGIN, KEY_STATUS, KEY_LOCATION, KEY_AUDIT_IDEA]
#define DISCOUNT_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", DISCOUNT_TABLE, KEY_UUID, KEY_CONTENT, KEY_START_DATE, KEY_END_DATE, KEY_TYPE, KEY_CATEGORY_ID, KEY_PUBLISHER, KEY_PUBLISH_DATE, KEY_PUBLISH_TIME, KEY_VISIT_COUNT, KEY_PAR_VALUE, KEY_ORIGIN, KEY_STATUS, KEY_LOCATION, KEY_AUDIT_IDEA]
#define DISCOUNT_DEL_BY_UUID_SQL [NSString stringWithFormat:@"delete from %@ where %@ = ?", DISCOUNT_TABLE, KEY_UUID]
#define DISCOUNT_COUNT_BASE_SQL [NSString stringWithFormat:@"select count(1) from %@ ", DISCOUNT_TABLE]
#define DISCOUNT_BASE_QUERY_SQL [NSString stringWithFormat:@"select %@, %@, %@, %@,%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ d ", KEY_UUID, KEY_CONTENT, KEY_START_DATE, KEY_END_DATE, KEY_TYPE, KEY_CATEGORY_ID, KEY_PUBLISHER, KEY_PUBLISH_DATE, KEY_PUBLISH_TIME, KEY_VISIT_COUNT, KEY_PAR_VALUE, KEY_ORIGIN, KEY_STATUS, KEY_LOCATION, KEY_AUDIT_IDEA, DISCOUNT_TABLE]

@implementation ZKHDiscountData

- (id)init
{
    if (self = [super init]) {
        [self createTable:DISCOUNT_CREATE_SQL];
        [self upgrade];
    }
    return self;
}

//执行数据库升级
- (void) upgrade
{
    if (![super checkColumnExist:DISCOUNT_TABLE columnName:KEY_ORIGIN]) {
        [self createTable:@" alter table z_discount add origin text default '0' "];
    }
    if (![super checkColumnExist:DISCOUNT_TABLE columnName:KEY_STATUS]) {
        [self createTable:@" alter table z_discount add status text "];
        [self executeUpdate:@" update z_discount set status = '1' where status is null or status = '' " params:@[]];
    }
    if (![super checkColumnExist:DISCOUNT_TABLE columnName:KEY_LOCATION]) {
        [self createTable:@" alter table z_discount add location text "];
    }
    if (![super checkColumnExist:DISCOUNT_TABLE columnName:KEY_AUDIT_IDEA]) {
        [self createTable:@" alter table z_discount add audit_idea text "];
    }
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHDiscountEntity *discount in data) {
        NSString *visitCount = [NSString stringWithFormat:@"%d", discount.visitCount];
        [self executeUpdate:DISCOUNT_UPDATE_SQL params:@[discount.uuid, discount.content, discount.startDate, discount.endDate, discount.type, discount.category.uuid, discount.publisher, discount.publishDate, discount.publishTime, visitCount, discount.parValue, discount.origin, discount.status, discount.location, discount.auditIdea]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHDiscountEntity *discount = [[ZKHDiscountEntity alloc] init];
    
    int i = 0;
    discount.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.content = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.startDate = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.endDate = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.type = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    NSString *categoryId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];

    discount.publisher = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.publishDate = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.publishTime = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.visitCount = sqlite3_column_int(stmt, i++);
    discount.parValue = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.origin = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.status = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.location = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    discount.auditIdea = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    discount.category = [[[ZKHCategoryData alloc] init] categoryById:categoryId];
    discount.images = [[[ZKHDiscountImageData alloc] init] imagesByDiscount:discount.uuid];
    //discount.storefronts = [[[ZKHDiscountStorefrontData alloc] init] storefrontsByDiscount:discount.uuid];
    
    return discount;
}

- (int)discountCountForOwner:(NSString *)owner
{
    return [self discountCountForOwner:owner type:nil];
}

- (int)discountCountForOwner:(NSString *)owner type:(NSString *)type
{
    NSString *sql = [NSString stringWithFormat:@"%@ where %@ = ? ", DISCOUNT_COUNT_BASE_SQL, KEY_PUBLISHER];
    NSMutableArray *params = [@[owner] mutableCopy];
    if (type) {
        sql = [NSString stringWithFormat:@"%@ and %@", sql, KEY_TYPE];
        [params addObject:type];
    }
    return [self queryCount:sql params:params];
}

- (int)activityCountForOwner:(NSString *)owner
{
    return [self discountCountForOwner:owner type:VAL_DISCOUNT_TYPE_ACTIVITY];
}

- (int)couponCountForOwner:(NSString *)owner
{
    return [self discountCountForOwner:owner type:VAL_DISCOUNT_TYPE_COUPON];
}

- (NSMutableArray *)activitiesGroupByPublishDate:(NSString *)owner offset:(int)offset
{
    return [self discountsGroupByPublishDate:VAL_DISCOUNT_TYPE_ACTIVITY owner:owner offset:offset];
}

- (NSMutableArray *)couponsGroupByPublishDate:(NSString *)owner offset:(int)offset
{
    return [self discountsGroupByPublishDate:VAL_DISCOUNT_TYPE_COUPON owner:owner offset:offset];
}

- (NSMutableArray *)discountsGroupByPublishDate:(NSString *)owner offset:(int)offset
{
    return [self discountsGroupByPublishDate:nil owner:owner offset:offset];
}

- (NSMutableArray *)discountsGroupByPublishDate:(NSString *)type owner:(NSString *)owner offset:(int)offset
{
    NSMutableArray *params = [@[owner] mutableCopy];
    NSMutableString *sql = [NSMutableString stringWithString:@" select publish_date, count(1) from z_discount "];
    [sql appendString:@" where publisher = ? "];
    if (type) {
        [sql appendString:@" and type = ? "];
        [params addObject:type];
    }
    [sql appendString:@" group by publish_date order by publish_date desc "];
    [sql appendFormat:@" limit %d offset %d ", DEFAULT_PAGE_SIZE, offset];
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    //NSLog(@"sql : %@", sql);
    sqlite3 *database = nil;
    @try {
        database = [self openDatabase];
        sqlite3_stmt *stmt;
        
        @try {
            stmt = [self prepareStatement:sql params:params database:database];
            while (sqlite3_step(stmt) == SQLITE_ROW) {
                ZKHDateIndexedEntity *entity = [[ZKHDateIndexedEntity alloc] init];
                
                int i = 0;
                NSString *publishDate = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
                entity.date = [NSDate initWithyyyyMMddString:publishDate];
                entity.count = sqlite3_column_int(stmt, i++);
                
                entity.items = [self discountsByPublishdate:type owner:owner publishDate:publishDate];
                
                [result addObject:entity];
            }
        }
        @catch (NSException *exception) {
            @throw exception;
        }
        @finally {
            sqlite3_finalize(stmt);
        }
    }
    @catch (NSException *exception) {
        @throw exception;
    }
    @finally {
        [self closeDatabase:database];
    }
    return result;
    
}

- (NSMutableArray *) discountsByPublishdate:(NSString *)type owner:(NSString *)owner publishDate:(NSString *)publishDate
{
    NSMutableArray *params = [@[publishDate, owner] mutableCopy];
    NSMutableString *sql = [NSMutableString stringWithString:DISCOUNT_BASE_QUERY_SQL];
    [sql appendFormat:@" where publish_date = ? and publisher = ? "];
    if (type) {
        [sql appendFormat:@" and type = ? "];
        [params addObject:type];
    }
    [sql appendString:@" order by publish_time desc "];
    
     NSMutableArray *discounts = [self query:sql params:params];
    [self setStorefronts:discounts location:nil];
    return discounts;
}

- (void)deleteDiscount:(NSString *)uuid
{
    [self executeUpdate:DISCOUNT_DEL_BY_UUID_SQL params:@[uuid]];
    [[[ZKHDiscountImageData alloc] init] deleteByDiscount:uuid];
    [[[ZKHDiscountStorefrontData alloc] init] deleteByDiscount:uuid];
}

- (NSMutableArray *)couponsForStore:(NSString *)storeId cateId:(NSString *)cateId offset:(int)offset location:(ZKHLocation *)location
{
    NSString *currentTime = [NSDate currentTimeString];
    NSMutableString *sql = [NSMutableString stringWithString:DISCOUNT_BASE_QUERY_SQL];
    [sql appendFormat:@" where type = %@ and category_id = %@", VAL_DISCOUNT_TYPE_COUPON, cateId];
    [sql appendFormat:@" and EXISTS (select 1 from z_discount_storefront ds, z_storefront f where ds.store_id = ? and ds.discount_id = d.uuid and ds.storefront_id = f.uuid and f.city_name = ?)"];
    [sql appendFormat:@" and start_date <= ? and end_date >= ? "];
    [sql appendString:@" order by publish_time"];
    [sql appendFormat:@" limit %d offset %d ", DEFAULT_PAGE_SIZE, offset];
    
    NSMutableArray *discounts = [self query:sql params:@[storeId, location.cityName, currentTime, currentTime]];
    [self setStorefronts:discounts location:location];
    return discounts;
}

- (ZKHDiscountEntity *)discountById:(NSString *)uuid location:(ZKHLocation *)location
{
    NSString *sql = [NSString stringWithFormat:@"%@ where uuid = ?", DISCOUNT_BASE_QUERY_SQL];
    ZKHDiscountEntity *discount = [self queryOne:sql params:@[uuid]];
    [self setDiscountStorefronts:discount location:location];
    return discount;
}

- (void)setStorefronts:(NSMutableArray *)discounts location:(ZKHLocation *)location
{
    for (ZKHDiscountEntity *discount in discounts) {
        [self setDiscountStorefronts:discount location:location];
    }
}

- (void)setDiscountStorefronts:(ZKHDiscountEntity *)discount location:(ZKHLocation *)location
{
    if (!discount) {
        return;
    }
    NSMutableArray *fronts = [[[ZKHDiscountStorefrontData alloc] init] storefrontsByDiscount:discount.uuid location:location];
    discount.storefronts = [[fronts sortedArrayUsingComparator:^NSComparisonResult(ZKHStorefrontEntity *obj1, ZKHStorefrontEntity *obj2) {
        
        NSComparisonResult result = obj1.distance > obj2.distance;
        
        return result == NSOrderedDescending; // 升序
    }] mutableCopy] ;
}

- (void)increaseVisitCount:(ZKHDiscountEntity *)discount
{
    NSString *sql = @" update z_discount set visit_count = visit_count + 1 where uuid = ? ";
    [self executeUpdate:sql params:@[discount.uuid]];
}
@end

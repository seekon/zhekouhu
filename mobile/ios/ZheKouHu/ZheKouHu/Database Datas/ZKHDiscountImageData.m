

#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"

#define DISCOUNT_IMG_TABLE @"z_discount_img"
#define DISCOUNT_IMG_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text) ", DISCOUNT_IMG_TABLE, KEY_UUID, KEY_DISCOUNT_ID, KEY_IMG, KEY_ORD_INDEX]
#define DISCOUNT_IMG_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@) values (?, ?, ?, ?)", DISCOUNT_IMG_TABLE, KEY_UUID, KEY_DISCOUNT_ID, KEY_IMG, KEY_ORD_INDEX]
#define DISCOUNT_IMG_QUERY_SQL [NSString stringWithFormat:@" select %@,%@,%@ from %@ where %@ = ? order by %@ ", KEY_UUID, KEY_DISCOUNT_ID, KEY_IMG, DISCOUNT_IMG_TABLE, KEY_DISCOUNT_ID, KEY_ORD_INDEX]
#define DISCOUNT_IMG_DEL_BY_DISCOUNT_SQL [NSString stringWithFormat:@"delete from %@ where %@ = ?", DISCOUNT_IMG_TABLE, KEY_DISCOUNT_ID]
#define DISCOUNT_IMG_DEL_BASE_SQL [NSString stringWithFormat:@"delete from %@ ", DISCOUNT_IMG_TABLE]

@implementation ZKHDiscountImageData

- (id)init
{
    if (self = [super init]) {
        [self createTable:DISCOUNT_IMG_CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHDiscountImageEntity *discountImg in data) {
        [self executeUpdate:DISCOUNT_IMG_UPDATE_SQL params:@[discountImg.uuid, discountImg.discountId, discountImg.aliasName, discountImg.ordIndex? discountImg.ordIndex : @"0"]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHDiscountImageEntity *image = [[ZKHDiscountImageEntity alloc] init];
    
    int i = 0;
    image.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    image.discountId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    image.aliasName = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];

    return image;
}

- (NSMutableArray *)imagesByDiscount:(NSString *)discountId
{
    return [self query:DISCOUNT_IMG_QUERY_SQL params:@[discountId]];
}

- (void) deleteByDiscount:(NSString *)discountId
{
    [self executeUpdate:DISCOUNT_IMG_DEL_BY_DISCOUNT_SQL params:@[discountId]];
    //TODO删除本地图片文件
}

- (void) deleteByDiscountImage:(NSString *)discountId image:(NSString *)image
{
    NSString *sql = [NSString stringWithFormat:@"%@ and %@ = ? ", DISCOUNT_IMG_DEL_BY_DISCOUNT_SQL, KEY_IMG];
    [self executeUpdate:sql params:@[discountId, image]];
}
- (void) deleteById:(NSString *)uuid
{
    NSString *sql = [NSString stringWithFormat:@"%@ where %@ = ?", DISCOUNT_IMG_DEL_BASE_SQL, KEY_UUID];
    [self executeUpdate:sql params:@[uuid]];
}
@end
#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"

#define DISCOUNT_STOREFRONT_TABLE @"z_discount_storefront"
#define DISCOUNT_STOREFRONT_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ text) ", DISCOUNT_STOREFRONT_TABLE, KEY_UUID, KEY_DISCOUNT_ID, KEY_STOREFRONT_ID, KEY_STORE_ID, KEY_ORD_INDEX]
#define DISCOUNT_STOREFRONT_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@, %@) values (?, ?, ?, ?, ?)", DISCOUNT_STOREFRONT_TABLE, KEY_UUID, KEY_DISCOUNT_ID, KEY_STOREFRONT_ID, KEY_STORE_ID, KEY_ORD_INDEX]
#define DISCOUNT_STOREFRONT_QUERY_SQL [NSString stringWithFormat:@" select %@ from %@ ds where %@ = ?  ", KEY_STOREFRONT_ID, DISCOUNT_STOREFRONT_TABLE, KEY_DISCOUNT_ID]
#define DISCOUNT_STOREFRONT_DEL_BY_DISCOUNT_SQL [NSString stringWithFormat:@"delete from %@ where %@ = ?", DISCOUNT_STOREFRONT_TABLE, KEY_DISCOUNT_ID]
#define DISCOUNT_STOREFRONT_DEL_BASE_SQL [NSString stringWithFormat:@"delete from %@ ", DISCOUNT_STOREFRONT_TABLE]

@implementation ZKHDiscountStorefrontData

- (id)init
{
    if (self = [super init]) {
        [self createTable:DISCOUNT_STOREFRONT_CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHDiscountStorefrontEntity *front in data) {
        if (front.name && front.city && front.storeId) {
            [super save:@[front]];
        }
        [self executeUpdate:DISCOUNT_STOREFRONT_UPDATE_SQL params:@[front.keyId, front.discountId, front.uuid, front.storeId, front.ordIndex]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    int i = 0;
    NSString *storefrontId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    return [[[ZKHStorefrontData alloc] init] storefrontById:storefrontId];
}

- (NSMutableArray *) storefrontsByDiscount:(NSString *) discountId location:(ZKHLocation *)location
{
    if (location) {
        NSString *sql = [NSString stringWithFormat:@"%@ and EXISTS (select 1 from z_storefront f where ds.storefront_id = f.uuid and f.city_name = ?) ", DISCOUNT_STOREFRONT_QUERY_SQL];
        return [self query:sql params:@[discountId, location.cityName]];
    }else{
        return [self query:DISCOUNT_STOREFRONT_QUERY_SQL params:@[discountId]];
    }
    
}

- (void)deleteByDiscount:(NSString *)discountId
{
    [self executeUpdate:DISCOUNT_STOREFRONT_DEL_BY_DISCOUNT_SQL params:@[discountId]];
}

- (void)deleteByDiscountStorefront:(NSString *)discountId storefrontId:(NSString *)storefrontId
{
    NSString *sql = [NSString stringWithFormat:@"%@ and %@ = ? ", DISCOUNT_STOREFRONT_DEL_BY_DISCOUNT_SQL, KEY_STOREFRONT_ID];
    [self executeUpdate:sql params:@[discountId, storefrontId]];
}

- (void)deleteById:(NSString *)uuid
{
    NSString *sql = [NSString stringWithFormat:@"%@ where %@ = ? ", DISCOUNT_STOREFRONT_DEL_BASE_SQL, KEY_UUID];
    [self executeUpdate:sql params:@[uuid]];
}
@end;


#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"
#import "ZKHContext.h"

#define USER_FAVORIT_TABLE  @"z_user_favorit"
#define CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text) ", USER_FAVORIT_TABLE, KEY_UUID, KEY_USER_ID, KEY_DISCOUNT_ID]
#define UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@) values (?, ?, ?)", USER_FAVORIT_TABLE, KEY_UUID, KEY_USER_ID, KEY_DISCOUNT_ID]
#define DEL_BASE_SQL [NSString stringWithFormat:@"delete from %@ ", USER_FAVORIT_TABLE]
#define QUERY_BASE_SQL [NSString stringWithFormat:@"select distinct %@ from %@ ", KEY_DISCOUNT_ID, USER_FAVORIT_TABLE]
#define COUNT_BASE_SQL [NSString stringWithFormat:@"select count(1) from %@ ", USER_FAVORIT_TABLE]

@implementation ZKHFavoritData

- (id)init
{
    if (self = [super init]) {
        [self createTable:CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHFavoritEntity *favorit in data) {
        [self executeUpdate:UPDATE_SQL params:@[favorit.uuid, favorit.userId, favorit.discount.uuid]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    int i = 0;
    NSString *discountId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    ZKHLocation *location = [ZKHContext getInstance].location;
    return [[[ZKHDiscountData alloc] init] discountById:discountId location:location];
}

- (NSMutableArray *)userFavorits:(NSString *)userId offset:(int)offset
{
    NSString *sql = [NSString stringWithFormat:@"%@ where user_id = ? limit %d offset %d ", QUERY_BASE_SQL, DEFAULT_PAGE_SIZE, offset];
    return [self query:sql params:@[userId]];
}

- (void)deleteFavorit:(NSString *)userId discountId:(NSString *)discountId
{
    NSString *sql = [NSString stringWithFormat:@"%@ where user_id = ? and discount_id = ? ", DEL_BASE_SQL];
    return [self executeUpdate:sql params:@[userId, discountId]];
}

- (Boolean)isFavorited:(NSString *)userId discountId:(NSString *)discountId
{
    NSString *sql = [NSString stringWithFormat:@"%@ where user_id = ? and discount_id = ? ", COUNT_BASE_SQL];
    return [self queryCount:sql params:@[userId, discountId]] > 0;
}
@end
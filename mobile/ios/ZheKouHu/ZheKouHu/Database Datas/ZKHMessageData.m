
#import "ZKHData.h"
#import "ZKHConst.h"

#define MSG_TABLE @"z_message"
#define MSG_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ text, %@ text default '0') ", MSG_TABLE, KEY_UUID, KEY_MSG_ID, KEY_CONTENT, KEY_TYPE, KEY_SEND_TIME, KEY_IS_DELETED]
#define MSG_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@, %@) values (?, ?, ?, ?, ?)", MSG_TABLE, KEY_UUID, KEY_MSG_ID, KEY_CONTENT, KEY_TYPE, KEY_SEND_TIME]
#define MSG_QUERY_SQL [NSString stringWithFormat:@" select %@, %@, %@, %@, %@ from %@ where is_deleted = '0' order by %@ desc", KEY_UUID, KEY_MSG_ID, KEY_CONTENT, KEY_TYPE, KEY_SEND_TIME, MSG_TABLE, KEY_SEND_TIME]
#define MSG_DEL_BASE_SQL [NSString stringWithFormat:@" update %@ set is_deleted = '1' ",  MSG_TABLE]

@implementation ZKHMessageData

- (id)init
{
    if (self = [super init]) {
        [self createTable:MSG_CREATE_SQL];
    }
    return self;
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHMessageEntity *message = [[ZKHMessageEntity alloc] init];
    
    int i = 0;
    message.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    message.msgId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    message.content = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    message.type = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    message.sendTime = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    return message;
}

- (void)save:(NSArray *)data
{
    for (ZKHMessageEntity *message in data) {
        [self executeUpdate:MSG_UPDATE_SQL params:@[message.uuid, message.msgId, message.content, message.type, message.sendTime]];
    }
}

- (NSMutableArray *)messages:(int) offset
{
    NSString *sql = [NSString stringWithFormat:@"%@ limit %d offset %d", MSG_QUERY_SQL, DEFAULT_PAGE_SIZE, offset];
    return [self query:sql params:nil];
}

-(void)deleteMessages
{
    [self executeUpdate:MSG_DEL_BASE_SQL params:nil];
}

- (void)deleteMessageById:(NSString *)msgId
{
    NSString *sql = [NSString stringWithFormat:@"%@ where uuid = ?", MSG_DEL_BASE_SQL];
    [self executeUpdate:sql params:@[msgId]];
}

@end
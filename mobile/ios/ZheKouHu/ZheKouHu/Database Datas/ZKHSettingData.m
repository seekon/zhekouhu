
#import "ZKHData.h"
#import "ZKHConst.h"

#define SETTING_TABLE @"z_setting"
#define SETTING_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text) ", SETTING_TABLE, KEY_CODE, KEY_VALUE]
#define SETTING_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@) values (?, ?)", SETTING_TABLE, KEY_CODE, KEY_VALUE]
#define SETTING_QUERY_SQL [NSString stringWithFormat:@" select %@, %@ from %@ where %@ = ?", KEY_CODE, KEY_VALUE, SETTING_TABLE, KEY_CODE]

@implementation ZKHSettingData

- (id)init
{
    if (self = [super init]) {
        [self createTable:SETTING_CREATE_SQL];
        //[self initVersion];
    }
    return self;
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHSettingEntity *setting = [[ZKHSettingEntity alloc] init];
    
    int i = 0;
    setting.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    setting.value = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    return setting;
}

- (void)save:(NSArray *)data
{
    for (ZKHSettingEntity *setting in data) {
        [self executeUpdate:SETTING_UPDATE_SQL params:@[setting.uuid, setting.value]];
    }
}

- (ZKHSettingEntity *)settingByCode:(NSString *)code
{
    return [self queryOne:SETTING_QUERY_SQL params:@[code]];
}

//- (void) initVersion
//{
//    [self executeUpdate:SETTING_UPDATE_SQL params:@[KEY_SETTING_VERSION, @"1.0.0"]];
//}
@end
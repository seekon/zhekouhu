
#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"

#define STORE_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ text) ", STORE_TABLE, KEY_UUID, KEY_NAME, KEY_LOGO, KEY_OWNER, KEY_REGISTER_TIME]
#define STORE_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@, %@ ) values (?, ?, ?, ?, ?)", STORE_TABLE, KEY_UUID, KEY_NAME, KEY_LOGO, KEY_OWNER, KEY_REGISTER_TIME]
#define STORE_QUERY_BASE_SQL [NSString stringWithFormat:@" select %@, %@, %@, %@ , %@ from %@ s ", KEY_UUID, KEY_NAME, KEY_LOGO, KEY_OWNER, KEY_REGISTER_TIME, STORE_TABLE]
#define STORE_DEL_BY_UUID_SQL [NSString stringWithFormat:@"delete from %@ where %@ = ?", STORE_TABLE, KEY_UUID]
#define QUERY_CHECK_OWNER_SQL [NSString stringWithFormat:@"select count(1) from %@ where %@ = ?", STORE_TABLE, KEY_OWNER]

@implementation ZKHStoreData

- (id)init
{
    if (self = [super init]) {
        [self createTable:STORE_CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHStoreEntity *store in data) {
        ZKHFileEntity *logo = store.logo;
        [self executeUpdate:STORE_UPDATE_SQL params:@[store.uuid, store.name, logo.aliasName, store.owner, store.registerTime]];
        
        NSMutableArray *fronts = store.storefronts;
        [[[ZKHStorefrontData alloc] init] save:fronts];
    }
}
- (void) save:(NSString *)uuid name:(NSString *)name
{
    int count = [self queryCount:@" select count(1) from z_store where uuid = ? " params:@[uuid]];
    if (count > 0) {
        [self executeUpdate:@" update z_store set name = ? where uuid = ?" params:@[name, uuid]];
    }else{
        ZKHStoreEntity *store = [[ZKHStoreEntity alloc] init];
        store.uuid = uuid;
        store.name = name;
        store.logo = [[ZKHFileEntity alloc] init];
        store.logo.aliasName = @"";
        store.owner = @"";
        store.registerTime = @"";
        [self save:@[store]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHStoreEntity *store = [[ZKHStoreEntity alloc] init];
    
    int i = 0;
    store.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    store.name = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    store.logo = [[ZKHFileEntity alloc] init];
    store.logo.aliasName = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    store.owner = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    store.registerTime = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    store.storefronts = [[[ZKHStorefrontData alloc] init] storefrontsForStore:store.uuid];
    
    return store;
}

- (NSMutableArray *)storesForOwner:(NSString *)owner
{
    NSString *sql = [NSString stringWithFormat:@"%@ where %@ = ? order by %@", STORE_QUERY_BASE_SQL, KEY_OWNER, KEY_REGISTER_TIME];
    return [self query:sql params:@[owner]];
}

- (NSMutableArray *)storesHasCouponsForCategory:(NSString *)categoryId
{
    NSMutableString *sql = [NSMutableString stringWithString:STORE_QUERY_BASE_SQL];
    [sql appendString:@" where EXISTS (select 1 from z_storefront f, z_discount d, z_discount_storefront ds where d.type='1' and d.uuid = ds.discount_id and ds.storefront_id = f.uuid and f.store_id = s.uuid and d.category_id = ?) "];
    return [self query:sql params:@[categoryId]];
}

- (void)deleteStore:(ZKHStoreEntity *)store
{
    [self executeUpdate:STORE_DEL_BY_UUID_SQL params:@[store.uuid]];
    [[[ZKHStorefrontData alloc] init] deleteByStore:store];
}

- (Boolean)ownStores:(NSString *)owner
{
    return [self queryCount:QUERY_CHECK_OWNER_SQL params:@[owner]] > 0;
}

@end
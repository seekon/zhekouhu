
#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"
#import "ZKHContext.h"
#import "ZKHLocationUtils.h"

#define STOREFRONT_TABLE @"z_storefront"
#define STOREFRONT_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text) ", STOREFRONT_TABLE, KEY_UUID, KEY_STORE_ID, KEY_NAME, KEY_ADDR, KEY_PHONE, KEY_LATITUDE, KEY_LONGITUDE, KEY_CITY, KEY_CITY_NAME]
#define STOREFRONT_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@ ) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", STOREFRONT_TABLE, KEY_UUID, KEY_STORE_ID, KEY_NAME, KEY_ADDR, KEY_PHONE, KEY_LATITUDE, KEY_LONGITUDE, KEY_CITY, KEY_CITY_NAME]
#define STOREFRONT_QUERY_BASE_SQL [NSString stringWithFormat:@" select sf.%@, %@, sf.%@, %@, %@, %@, %@ , s.name as store_name, sf.city, c.name as city_name, c.first_letter from %@ sf, z_store s, z_city c where sf.store_id = s.uuid and sf.city = c.uuid ", KEY_UUID, KEY_STORE_ID, KEY_NAME, KEY_ADDR, KEY_PHONE, KEY_LATITUDE, KEY_LONGITUDE, STOREFRONT_TABLE]
#define STOREFRONT_DEL_BASE_SQL [NSString stringWithFormat:@"delete from %@ ", STOREFRONT_TABLE]

@implementation ZKHStorefrontData

- (id)init
{
    if (self = [super init]) {
        [self createTable:STOREFRONT_CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHStorefrontEntity *front in data) {
        if (!front.uuid) {
            continue;
        }
        NSString *latitude = front.latitude ? front.latitude : @"0";
        NSString *longitude  = front.longitude ? front.longitude :@"0";
        [self executeUpdate:STOREFRONT_UPDATE_SQL params:@[front.uuid, front.storeId, front.name, front.addr, front.phone, latitude, longitude, front.city.uuid, front.city.name]];
        if (front.city) {
            [[[ZKHCityData alloc] init] save:front.city.uuid name:front.city.name];
        }
        if (front.storeId && front.storeName) {
            [[[ZKHStoreData alloc] init] save:front.storeId name:front.storeName];
        }
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHStorefrontEntity *front = [[ZKHStorefrontEntity alloc] init];
    
    int i = 0;
    front.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.storeId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.name = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.addr = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.phone = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.latitude = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.longitude = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.storeName = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.city = [[ZKHCityEntity alloc] init];
    front.city.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.city.name = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    front.city.firstLetter = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    ZKHLocation *location = [ZKHContext getInstance].location;
    if (location) {
        front.distance = [ZKHLocationUtils latlngDist:location.lat lng1:location.lng lat2:[front.latitude doubleValue] lng2:[front.longitude doubleValue]];
    }else{
        front.distance = DBL_MAX;
    }
    return front;
}

- (NSMutableArray *)storefrontsForStore:(NSString *)storeId
{
    NSString *sql = [NSString stringWithFormat:@"%@ and sf.%@ = ?", STOREFRONT_QUERY_BASE_SQL, KEY_STORE_ID];
    return [self query:sql params:@[storeId]];
}

- (NSMutableArray *)storefrontsForDiscount:(NSString *)discountId
{
    NSString *sql = [NSString stringWithFormat:@"%@ and sf.uuid in (select storefront_id from z_discount_storefront where discount_id = ?) ", STOREFRONT_QUERY_BASE_SQL];
    return [self query:sql params:@[discountId]];
}

- (ZKHStorefrontEntity *)storefrontById:(NSString *)uuid
{
    NSString *sql = [NSString stringWithFormat:@"%@ and sf.%@ = ?", STOREFRONT_QUERY_BASE_SQL, KEY_UUID];
    return [self queryOne:sql params:@[uuid]];
}

- (void)deleteById:(NSString *)uuid
{
    NSString *sql = [NSString stringWithFormat:@"%@ where %@ = ? ", STOREFRONT_DEL_BASE_SQL, KEY_UUID];
    [self executeUpdate:sql params:@[uuid]];
}

- (void)deleteByStore:(ZKHStoreEntity *)store
{
    NSString *sql = [NSString stringWithFormat:@"%@ where %@ = ? ", STOREFRONT_DEL_BASE_SQL, KEY_STORE_ID];
    [self executeUpdate:sql params:@[store.uuid]];
}
@end

#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"

#define USER_TABLE @"z_user"
#define USER_CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text) ", USER_TABLE, KEY_UUID, KEY_CODE, KEY_PWD, KEY_REGISTER_TIME]
#define USER_UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@) values (?, ?, ?, ?)", USER_TABLE, KEY_UUID, KEY_CODE, KEY_PWD, KEY_REGISTER_TIME]
#define USER_BY_CODE_QUERY_SQL [NSString stringWithFormat:@" select %@, %@, %@, %@ from %@ where %@ = ? ", KEY_UUID, KEY_CODE, KEY_PWD, KEY_REGISTER_TIME, USER_TABLE, KEY_CODE]
#define USER_DEL_BY_UUID_SQL [NSString stringWithFormat:@"delete from %@ where %@ = ?", USER_TABLE, KEY_UUID]

@implementation ZKHUserData

- (id)init
{
    if (self = [super init]) {
        [self createTable:USER_CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHUserEntity *user in data) {
        [self executeUpdate:USER_UPDATE_SQL params:@[user.uuid, user.code, user.pwd, user.registerTime]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHUserEntity *user = [[ZKHUserEntity alloc] init];
    
    int i = 0;
    user.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    user.code = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    char * pwd = (char*)sqlite3_column_text(stmt, i++);
    if (pwd != NULL) {
        user.pwd = [[NSString alloc] initWithUTF8String:pwd];
    }
    
    user.registerTime = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    return user;
}

- (ZKHUserEntity *)user:(NSString *)code
{
    return (ZKHUserEntity *)[self queryOne:USER_BY_CODE_QUERY_SQL params:@[code]];
}

#define USER_FIELD_UPDATE_SQL(__FIELD_NAME__) [NSString stringWithFormat:@"update %@ set %@=? where %@=?", USER_TABLE, __FIELD_NAME__, KEY_UUID]

- (void)updateUserPwd:(NSString *)uuid pwd:(NSString *)pwd
{
    [self executeUpdate:USER_FIELD_UPDATE_SQL(KEY_PWD) params:@[pwd, uuid]];
}

- (void)deleteUser:(ZKHUserEntity *)user
{
    [self executeUpdate:USER_DEL_BY_UUID_SQL params:@[user.uuid]];
}
@end
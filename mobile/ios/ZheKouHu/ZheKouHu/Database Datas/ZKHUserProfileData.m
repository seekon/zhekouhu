
#import "ZKHData.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"

#define USER_PROFILE_TABLE @"z_user_profile"
#define CREATE_SQL [NSString stringWithFormat:@" create table if not exists %@ (%@ text primary key, %@ text, %@ text, %@ text, %@ text, %@ text, %@ text, %@ integer, %@ integer) ", USER_PROFILE_TABLE, KEY_UUID, KEY_USER_ID, KEY_NAME, KEY_PHOTO, KEY_SEX, KEY_PHONE, KEY_DELIVER_ADDR, KEY_AWARD_SUM, KEY_AWARD_USED]
#define UPDATE_SQL [NSString stringWithFormat:@" insert or replace into %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@) values (?, ?, ?, ?, ?, ?, ?, ?, ?)", USER_PROFILE_TABLE, KEY_UUID, KEY_USER_ID, KEY_NAME, KEY_PHOTO, KEY_SEX, KEY_PHONE, KEY_DELIVER_ADDR, KEY_AWARD_SUM, KEY_AWARD_USED]
#define PROFILE_QUERY_SQL [NSString stringWithFormat:@" select %@, %@, %@, %@, %@, %@, %@, %@, %@ from %@ where %@ = ? ", KEY_UUID, KEY_USER_ID, KEY_NAME, KEY_PHOTO, KEY_SEX, KEY_PHONE, KEY_DELIVER_ADDR, KEY_AWARD_SUM, KEY_AWARD_USED, USER_PROFILE_TABLE, KEY_USER_ID]
#define PROFILE_DEL_BY_UUID_SQL [NSString stringWithFormat:@"delete from %@ where %@ = ?", USER_PROFILE_TABLE, KEY_UUID]

@implementation ZKHUserProfileData

- (id)init
{
    if (self = [super init]) {
        [self createTable:CREATE_SQL];
    }
    return self;
}

- (void)save:(NSArray *)data
{
    if (data == nil || [data count] == 0) {
        return;
    }
    
    for (ZKHUserProfileEntity *profile in data) {
        NSString *photo = nil;
        if (profile.photo) {
            photo = profile.photo.aliasName;
        }
        if (!photo) {
            photo = @"";
        }
        [self executeUpdate:UPDATE_SQL params:@[profile.uuid, profile.userId, profile.name, photo, profile.sex, profile.phone, profile.devilerAddr, profile.awardSum, profile.awardUsed]];
    }
}

- (id)processRow:(sqlite3_stmt *)stmt
{
    ZKHUserProfileEntity *profile = [[ZKHUserProfileEntity alloc] init];
    
    int i = 0;
    profile.uuid = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.userId = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.name = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.photo = [[ZKHFileEntity alloc] init];
    profile.photo.aliasName = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.sex = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.phone = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.devilerAddr = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.awardSum = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    profile.awardUsed = [[NSString alloc] initWithUTF8String:(char*)sqlite3_column_text(stmt, i++)];
    
    return profile;
}

- (ZKHUserProfileEntity *)profileByUserId:(NSString *)userId
{
    return [self queryOne:PROFILE_QUERY_SQL params:@[userId]];
}

- (void)deleteById:(NSString *)uuid
{
    [self executeUpdate:PROFILE_DEL_BY_UUID_SQL params:@[uuid]];
}

@end
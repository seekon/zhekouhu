//
//  ZKHRequestSigner.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHRequestSigner.h"
#import "ZKHContext.h"
#import "ZKHProcessor+User.h"
#import "ZKHConst.h"
#import "ZKHEntity.h"
#import "NSString+Utils.h"

@implementation ZKHRequestSigner

- (void)authorize:(ZKHRestRequest *)request completionHandler:(authResponseBlock)authorizeBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    NSString *method = request.method;
    NSString *urlString = request.urlString;
    if (([method isEqualToString:METHOD_GET])
         || [urlString hasSuffix:@"/login"]
         || [urlString hasSuffix:@"/registerUser"]
         || [urlString hasSuffix:@"/submitFeedback"]
         || [urlString hasSuffix:@"/visitDiscount"]
         || [urlString hasSuffix:@"/registerDevice"]
         || [urlString hasSuffix:@"/setNotifyState"]
         || [urlString hasSuffix:@"/publishComment"]
         || [urlString hasSuffix:@"/anonymPublishDiscount"]
         || [urlString hasSuffix:@"/favoritDiscount"]
        ) {
        authorizeBlock(request);
        return;
    }
    
    ZKHContext *context = [ZKHContext getInstance];
    NSString *sessionId = context.sessionId;
    if (sessionId == nil) {
        ZKHUserEntity *user = context.user;
        [[ZKHProcessor getInstance] remoteLogin:user.code pwd:user.pwd completionHandler:^(NSMutableDictionary *authObj) {
            NSString *sessionId = context.sessionId;
            if (sessionId != nil) {
                [request addHeader:@"cookie" value:sessionId];
                authorizeBlock(request);
            }else{
                //TODO:
                errorBlock(nil);
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            errorBlock(error);
        }];
    }else{
        [request addHeader:@"cookie" value:sessionId];
        authorizeBlock(request);
    }
}

@end

//
//  UITableViewController+CWPopup.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-5.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHTouchesDelegate.h"
#import "UIViewController+CWPopup.h"

@interface UITableViewController (CWPopup) <ZKHTouchesDelegate>

@end

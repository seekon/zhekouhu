//
//  UIViewController+CWPopup.h
//  CWPopupDemo
//
//  Created by Cezary Wojcik on 8/21/13.
//  Copyright (c) 2013 Cezary Wojcik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHTouchesDelegate.h"

@interface UIViewController (CWPopup) <ZKHTouchesDelegate>

@property (nonatomic, readwrite) UIViewController *popupViewController;
@property (nonatomic, readwrite) BOOL useBlurForPopup;
@property (nonatomic, readwrite) CGPoint popupViewOffset;

- (void)cm_presentPopupViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag;
- (void)cm_presentPopupViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion;
- (void)cm_dismissPopupViewControllerAnimated:(BOOL)flag;
- (void)cm_dismissPopupViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion;
- (void)setUseBlurForPopup:(BOOL)useBlurForPopup;
- (BOOL)useBlurForPopup;

@end

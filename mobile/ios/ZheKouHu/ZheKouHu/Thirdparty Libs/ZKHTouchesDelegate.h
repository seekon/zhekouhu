//
//  CMTouchesDelegate.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-4.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ZKHTouchesDelegate <NSObject>

@optional
- (void)view:(UIView*)view touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event;

@end

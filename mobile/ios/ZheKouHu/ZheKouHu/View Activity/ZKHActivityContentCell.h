//
//  ZKHActivityContentCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"

@interface ZKHActivityContentCell : UITableViewCell
{
    UILabel *contentLabel;
}

@property (nonatomic) ZKHDiscountEntity *discount;

+ (CGFloat) cellHeight:(ZKHDiscountEntity *)discount;

@end

//
//  ZKHActivityContentCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHActivityContentCell.h"
#import "UIColor+Utils.h"

#define kGapWidth 16

@implementation ZKHActivityContentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat) cellContentWidth
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
        return screenBounds.size.width - 2 * kGapWidth;
    }else{
        return screenBounds.size.height - 2 * kGapWidth;
    }
}

+ (CGFloat) contentHeight:(NSString *)content
{
    int fontSize = 13;
    CGFloat cellRightWidth = [ZKHActivityContentCell cellContentWidth];
    int mod = (fontSize * content.length) % [[[NSDecimalNumber alloc]initWithFloat:cellRightWidth] intValue];
    int rows = fontSize * content.length / cellRightWidth;
    if (rows == 0) {
        rows = 1;
    }else{
        if (mod > 0) {
            rows++;
        }
    }
    return rows * 20;
}

+ (CGFloat) cellHeightByContent:(NSString *)content
{
    NSRange range = [content rangeOfString:@"\r\n"];
    if (range.length > 0 && range.location > 0) {
        return [ZKHActivityContentCell contentHeight:[content substringToIndex:range.location]]
        + [ZKHActivityContentCell cellHeightByContent:[content substringFromIndex:(range.location + range.length)]];
    }else{
        return [ZKHActivityContentCell contentHeight:content];
    }
}

+ (CGFloat) cellHeight:(ZKHDiscountEntity *)discount
{
    return 2 * 16 + [ZKHActivityContentCell cellHeightByContent:discount.content];
}

- (void)setDiscount:(ZKHDiscountEntity *)discount
{
    if (!contentLabel) {
        contentLabel = [[UILabel alloc] init];
        contentLabel.font = [UIFont systemFontOfSize:13];
        contentLabel.textColor = [UIColor color_7f7f7f];
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.numberOfLines = 0;
        
        [self addSubview:contentLabel];
    }
    CGRect frame = CGRectMake(kGapWidth, kGapWidth, [ZKHActivityContentCell cellContentWidth], [ZKHActivityContentCell cellHeight:discount]);
    contentLabel.frame = frame;
    //contentLabel.text = discount.content;
    _discount = discount;
    
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:discount.content];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:6];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [discount.content length])];
    [contentLabel setAttributedText:attributedString1];
    [contentLabel sizeToFit];
}

@end

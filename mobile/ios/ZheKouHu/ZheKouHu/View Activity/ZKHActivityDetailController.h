//
//  ZKHActivityDetailController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"
#import "ZKHBackTableViewController.h"
#import "LXActivity.h"
#import "REMenu.h"
#import "REMenuItem.h"

@interface ZKHActivityDetailController : ZKHBackTableViewController<LXActivityDelegate>
{
    Boolean frontLoaded;
    Boolean commentLoaded;
    UIImageView *hintImageView;
    UILabel *hintLabel;
    UITextField *inputField;
    
    REMenu *menu;
    REMenuItem *favoritItem;
    REMenuItem *unfavoritItem;
    REMenuItem *faultItem;
    REMenuItem *shareItem;
}
@property (strong, nonatomic) ZKHDiscountEntity *discount;
@property (nonatomic) int openOrigin;
@end

@interface ZKHActivityTermCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *termLabel;
@property (weak, nonatomic) IBOutlet UILabel *parLabel;
@end

@interface ZKHActivityFrontTitleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UIImageView *titleImageView;
@end

//
//  ZKHActivityDetailController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "ZKHActivityDetailController.h"
#import "ZKHViewUtils.h"
#import "ZKHActivityStorefrontCell.h"
#import "ZKHActivityImageCell.h"
#import "ZKHActivityContentCell.h"
#import "ZKHConst.h"
#import "NSDate+Utils.h"
#import "NSString+Utils.h"
#import "ZKHProcessor+Discount.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "NSDate+Utils.h"
#import "ZKHShareUtils.h"
#import "ZKHShowMoreCell.h"
#import "ZKHActivityCommentCell.h"
#import "ZKHDiscountStorefrontController.h"
#import "ZKHDiscountCommentController.h"
#import "ZKHContext.h"
#import "UIView+Toast.h"
#import "SCLAlertView.h"
#import "SCLAlertViewStyleKit.h"

#define kDefaultCellNumber 7
#define kImageCellHeight 153
#define kShowMoreCellHeight 35
#define kTitleCellHeight 29

static NSString *ActivityImageCellIdentifier = @"ActivityImageCell";
static NSString *ActivityContentCellIdentifier = @"ActivityContentCell";
static NSString *ActivityTermCellIdentifier = @"ActivityTermCell";
static NSString *ActivityBlankCellIdentifier = @"ActivityBlankCell";
static NSString *ActivityStorefrontTitleCellIdentifier = @"ActivityStorefrontTitleCell";
static NSString *ActivityStorefrontCellIdentifier = @"ActivityStorefrontCell";
static NSString *ActivityCommentTitleCellIdentifier = @"ActivityCommentTitleCell";
static NSString *ActivityCommentCellIdentifier = @"ActivityCommentCell";
static NSString *ShowMoreCellIdentifier = @"ShowMoreCell";

@implementation ZKHActivityDetailController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    frontLoaded = false;
    commentLoaded = false;
    [self menuInit];
    
    UIButton *shareButton = [[UIButton alloc] init];
    [shareButton setImage:[UIImage imageNamed:@"action_more" scale:2] forState:UIControlStateNormal];
    shareButton.frame = CGRectMake(0, 0, 40, 40);
    [shareButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:shareButton.frame.size] forState:UIControlStateHighlighted];
    [shareButton addTarget:self action:@selector(openMenuView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
    
    if ([self.discount.type isEqualToString:VAL_DISCOUNT_TYPE_ACTIVITY]) {
        self.title = @"活动详情";
    }else{
        self.title = @"优惠券详情";
    }
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    [[ZKHProcessor getInstance] visitDiscount:self.discount completionHandler:^(Boolean result) {
        
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    [self.tableView setBackgroundColor:[UIColor color_f5f5f5]];
    
    UINib *showMoreNib = [UINib nibWithNibName:@"ZKHShowMoreCell" bundle:nil];
    [self.tableView registerNib:showMoreNib forCellReuseIdentifier:ShowMoreCellIdentifier];
    
    [[ZKHProcessor getInstance] storeFrontsByDiscount:self.discount limit:DEFAULT_DISCOUNT_FRONTS_NUM offset:0 completionHandler:^(NSMutableArray *storefronts) {
        self.discount.storefronts = storefronts;
        frontLoaded = true;
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    [[ZKHProcessor getInstance] commentsByDiscount:self.discount limit:DEFAULT_DISCOUNT_COMMENTS_NUM offset:0 completionHandler:^(NSMutableArray *comments) {
        self.discount.comments = comments;
        commentLoaded = true;
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    if (self.openOrigin == DISCOUNT_ORIGIN_FAVORIT) {//从收藏打开的，需获取额外的信息
        [[ZKHProcessor getInstance] discountExtWithFavorit:self.discount completionHandler:^(ZKHDiscountEntity *discount) {
            if (discount) {
                self.discount = discount;
                [self.tableView reloadData];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }
}

- (void) menuInit
{
    favoritItem = [[REMenuItem alloc] initWithTitle:@"收藏" image:[UIImage imageNamed:@"menu_favorit" scale:2] highlightedImage:nil action:^(REMenuItem *item) {
        [menu close];
        [self doFavoritDiscount:true];
    }];
    unfavoritItem = [[REMenuItem alloc] initWithTitle:@"取消收藏" image:[UIImage imageNamed:@"menu_unfavorit" scale:2] highlightedImage:nil action:^(REMenuItem *item) {
        [menu close];
        [self doFavoritDiscount:false];
    }];
    faultItem = [[REMenuItem alloc] initWithTitle:@"纠错" image:[UIImage imageNamed:@"menu_fault" scale:2] highlightedImage:nil action:^(REMenuItem *item) {
        [menu close];
        [self faultDiscountClick];
    }];
    shareItem = [[REMenuItem alloc] initWithTitle:@"分享" image:[UIImage imageNamed:@"menu_share" scale:2] highlightedImage:nil action:^(REMenuItem *item) {
        [menu close];
        [self shareDiscountClick];
    }];
    
    NSString *userId = nil;
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        userId = user.uuid;
    }else{
        userId = [ZKHViewUtils deviceId];
    }
    
    Boolean isFavorited = false;
    if (userId) {
        isFavorited = [[ZKHProcessor getInstance] isUserFavorited:userId discountId:self.discount.uuid];
    }
    if (isFavorited) {
        menu = [[REMenu alloc] initWithItems:@[unfavoritItem, shareItem, faultItem]];
    }else{
        menu = [[REMenu alloc] initWithItems:@[favoritItem, shareItem, faultItem]];
    }
    menu.backgroundColor = [UIColor color_f5f5f5];
    menu.font = [UIFont systemFontOfSize:13.0];
    menu.textColor = [UIColor color_2d2d2d];
    menu.separatorColor = [UIColor color_e3e3e3];
    menu.separatorHeight = 0.5;
    menu.borderColor = [UIColor color_e3e3e3];
    menu.borderWidth = 0.5;
    menu.highlightedBackgroundColor = [UIColor color_7f7f7f];
    menu.itemHeight = 40.0f;
}

- (void) openMenuView:(id)sender
{
    if (menu.isOpen) {
        [menu close];
    }else{
        if (menu.isAnimating) {
            return;
        }
        
        [menu showFromRect:CGRectMake(0, 64, self.navigationController.navigationBar.frame.size.width, self.navigationController.view.frame.size.height) inView:self.navigationController.view];
    }
}

- (void)shareDiscountClick
{
    NSArray *shareButtonTitleArray = @[@"微信",@"朋友圈",@"新浪微博"];
    NSArray *shareButtonImageNameArray = @[@"share_weixin",@"share_friends",@"share_sina"];
    
    LXActivity *lxActivity = [[LXActivity alloc] initWithTitle:nil delegate:self cancelButtonTitle:nil ShareButtonTitles:shareButtonTitleArray withShareButtonImagesName:shareButtonImageNameArray];
    [lxActivity showInView:self.view];
}

- (void) doFavoritDiscount:(Boolean) favorit
{
    if (favorit) {
        [[ZKHProcessor getInstance] favoritDiscount:self.discount completionHandler:^(Boolean result) {
            if (result) {
                menu.items = @[unfavoritItem, shareItem, faultItem];
                [self.view makeToast:@"收藏成功."];
            }else{
                [self.view makeToast:@"收藏失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }else{
        [[ZKHProcessor getInstance] unfavoritDiscount:self.discount completionHandler:^(Boolean result) {
            if (result) {
                menu.items = @[favoritItem, shareItem, faultItem];
                [self.view makeToast:@"取消收藏成功."];
            }else{
                [self.view makeToast:@"取消收藏失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }
}

- (void) faultDiscountClick
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    UITextField *textField = [alert addTextField:@""];
    if (IOS7) {
        textField.tintColor = [UIColor color_ff6722];
    }else{
        [[textField valueForKey:@"textInputTraits"] setValue:[UIColor color_ff6722] forKey:@"insertionPointColor"];
    }
    
    [alert addButton:@"确定" actionBlock:^(void) {
        NSString *content = textField.text;
        if ([NSString isNull:content]) {
            return;
        }
        
        ZKHCommentEntity *comment = [[ZKHCommentEntity alloc] init];
        ZKHUserEntity *user = [ZKHContext getInstance].user;
        if (user) {
            comment.publisher = user.code;
        }else{
            comment.publisher = [ZKHViewUtils deviceId];
        }
        if (!comment.publisher) {
            comment.publisher = [NSDate currentTimeString];
        }
        comment.discountId = self.discount.uuid;
        comment.content = content;
        comment.type = KEY_TYPE_FAULT;
        
        //发布评论
        [[ZKHProcessor getInstance] publishComment:comment completionHandler:^(ZKHCommentEntity *comment) {
            if (comment) {
                [self.view makeToast:@"发送成功，感谢您对折扣虎的支持."];
            }else{
                [self.view makeToast:@"发送失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }];
    
    [alert showCustom:self image:SCLAlertViewStyleKit.imageOfEdit color:[UIColor color_ff6722] title:@"提示" subTitle:@"请输入纠错原因" closeButtonTitle:@"取消" duration:0.0f];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [menu close];
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - LXActivityDelegate

- (void)didClickOnImageIndex:(int)imageIndex
{
    switch (imageIndex) {
        case 0:
            [ZKHShareUtils shareDiscountToWeixin:self.discount];
            break;
        case 1:
            [ZKHShareUtils shareDiscountToFriends:self.discount];
            break;
        case 2:
            [ZKHShareUtils shareDiscountToSina:self.discount];
            break;
        default:
            break;
    }
}

- (void)didClickOnCancelButton
{
    NSLog(@"didClickOnCancelButton");
}

- (NSInteger) numberOfRowsInSection:(NSInteger)section
{
    return kDefaultCellNumber + [self frontDisplayCount] + [self commentDisplayCount];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = nil;
    
    long lastRowIndex = [self numberOfRowsInSection:indexPath.section] - 1;
    long rowIndex = indexPath.row;
    switch (rowIndex) {
        case 0:
        {
            CellIdentifier = ActivityImageCellIdentifier;
            ZKHActivityImageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            [cell updateViews:self images:self.discount.images scaleSize:CGSizeMake([ZKHViewUtils screenWidth], kImageCellHeight)];
            return cell;
        }
        case 1:
        {
            CellIdentifier = ActivityTermCellIdentifier;
            ZKHActivityTermCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

            cell.termLabel.text = [NSString stringWithFormat:@"有效期：%@ 到 %@",[[NSDate dateWithMilliSeconds:[self.discount.startDate longLongValue]] toyyyyMMddString] , [[NSDate dateWithMilliSeconds:[self.discount.endDate longLongValue]] toyyyyMMddString]] ;
            if ([self.discount.type isEqualToString:VAL_DISCOUNT_TYPE_COUPON]) {
                cell.parLabel.text = [NSString stringWithFormat:@"￥%@", self.discount.parValue];
            }else{
                cell.parLabel.text = @"";
            }
            [ZKHViewUtils drawSeparaterAtHeader:cell offsetX:0];
            return cell;
        }
        case 2:
        {
            CellIdentifier = ActivityContentCellIdentifier;
            ZKHActivityContentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            cell.discount = self.discount;
            [ZKHViewUtils drawSeparaterAtHeader:cell offsetX:0];
            [ZKHViewUtils drawSeparaterAtFooter:cell offsetX:0];
            return cell;
        }
        case 3:
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ActivityBlankCellIdentifier forIndexPath:indexPath];
            
            return cell;
        }
        case 4:
        {
            ZKHActivityFrontTitleCell *cell = [self getActivityFrontTitleCell:tableView cellForRowAtIndexPath:indexPath isFront:true];
            cell.titleLabel.text = [NSString stringWithFormat:@"%d家门店", self.discount.frontCount];
            return cell;
        }
        default:
        {
            //最后增加一空行
            if (rowIndex == lastRowIndex) {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ActivityBlankCellIdentifier forIndexPath:indexPath];
                return cell;
            }
            
            long frontDisplayCount = [self frontDisplayCount];
            //处理门店列表
            long frontIndex = rowIndex - 5;
            if (frontIndex < frontDisplayCount) {//前5项
                if (!frontLoaded) {
                    return [self getShowMoreCell:tableView cellForRowAtIndexPath:indexPath loading:true isFront:true];
                }else{
                    if ((frontIndex - DEFAULT_DISCOUNT_FRONTS_NUM < 0)
                        && (frontIndex < [self.discount.storefronts count])) {
                        return [self getActivityStorefrontCell:tableView cellForRowAtIndexPath:indexPath frontIndex:frontIndex];
                    }else{
                        return [self getShowMoreCell:tableView cellForRowAtIndexPath:indexPath loading:false isFront:true];
                    }
                }
            }
            //处理评论列表
            //int commentDisplayCount = [self commentDisplayCount];
            if (frontIndex - frontDisplayCount == 0) {//显示titile
                ZKHActivityFrontTitleCell *cell = [self getActivityFrontTitleCell:tableView cellForRowAtIndexPath:indexPath isFront:false];
                cell.titleLabel.text = [NSString stringWithFormat:@"%d条评论", self.discount.commentCount];
                return cell;
            }
            if (!commentLoaded) {
                return [self getShowMoreCell:tableView cellForRowAtIndexPath:indexPath loading:true isFront:false];
            }else{
                int commentIndex = frontIndex - frontDisplayCount - 1;
                if ((commentIndex - DEFAULT_DISCOUNT_COMMENTS_NUM < 0)
                    && (commentIndex < [self.discount.comments count])) {
                    return [self getActivityCommentCell:tableView cellForRowAtIndexPath:indexPath frontIndex:commentIndex];
                }else{
                    return [self getShowMoreCell:tableView cellForRowAtIndexPath:indexPath loading:false isFront:false];
                }
                
            }
            
        }
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    switch (indexPath.row) {
        case 0:
            return  kImageCellHeight;
            break;
        case 1:
            return  40;
            break;
        case 2:
            return  [ZKHActivityContentCell cellHeight:self.discount];
            break;
        case 3:
            return  10;
            break;
        case 4:
            return kTitleCellHeight;
            break;
        default:
            height = 40;
            break;
    }
    
    int rowIndex = indexPath.row;
    if (rowIndex == ([self numberOfRowsInSection:indexPath.section] - 1)) {
        CGFloat screenHeight = [ZKHViewUtils screenHeight];
        height = screenHeight - k_navHeight - kImageCellHeight - 40 - ([ZKHActivityContentCell cellHeight:self.discount])- 10 - kTitleCellHeight*2 - [self frontDisplayCount] * 58 - [self commentDisplayCount] * 56 - 40 ;
        if (height <= 0) {
            height = 20;
        }
        return height;
    }
    
    int frontDisplayCount = [self frontDisplayCount];
    //处理门店列表
    int frontIndex = rowIndex - 5;
    if (frontIndex < frontDisplayCount) {//前5项
        if (!frontLoaded) {
            return kShowMoreCellHeight;
        }else{
            if (frontIndex - DEFAULT_DISCOUNT_FRONTS_NUM < 0){
                return 58;
            }else{
                return kShowMoreCellHeight;
            }
        }
    }
    //处理评论列表
    if (frontIndex - frontDisplayCount == 0) {//显示titile
        return kTitleCellHeight;
    }
    if (!commentLoaded) {
        return kShowMoreCellHeight;
    }else{
        int commentIndex = frontIndex - frontDisplayCount - 1;
        if (commentIndex - DEFAULT_DISCOUNT_COMMENTS_NUM < 0) {
            return 56;
        }else{
            return kShowMoreCellHeight;
        }
    }
    return height;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 3) {
        cell.backgroundColor = [UIColor whiteColor];
    }else{
        cell.backgroundColor = [UIColor color_f5f5f5];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 320, 50);
    view.backgroundColor = [UIColor color_f5f5f5];
    view.layer.borderWidth = 0.5;
    view.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    //[ZKHViewUtils drawSeparaterAtHeader:view offsetX:0];
    
    inputField = [[UITextField alloc] init];
    inputField.frame = CGRectMake(10, 6, 242, 28);
    inputField.background = [UIImage imageNamed:@"bg_textfield_sel"];
    inputField.font = [UIFont systemFontOfSize:13];
    if (IOS7) {
        inputField.tintColor = [UIColor color_ff6722];
    }else{
        [[inputField valueForKey:@"textInputTraits"] setValue:[UIColor color_ff6722] forKey:@"insertionPointColor"];
    }

    [view addSubview:inputField];
    [inputField addTarget:self action:@selector(inputFieldEditingDidBegin:) forControlEvents:UIControlEventEditingDidBegin];
    
    hintImageView= [[UIImageView alloc] init];
    hintImageView.frame = CGRectMake(10, 12, 16, 16);
    hintImageView.image = [UIImage imageNamed:@"edit" scale:2];
    [view addSubview:hintImageView];
    
    hintLabel= [[UILabel alloc] init];
    hintLabel.frame = CGRectMake(30, 5, 50, 30);
    hintLabel.font = [UIFont systemFontOfSize:13];
    hintLabel.text = @"写点评";
    hintLabel.textColor = [UIColor color_7f7f7f];
    [view addSubview:hintLabel];
    
    UIButton *sendButton = [[UIButton alloc] init];
    sendButton.frame = CGRectMake(256, 7, 55, 26);
    sendButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [sendButton setTitle:@"发送" forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setBackgroundImage:[UIImage imageNamed:@"bg_send_nor" scale:2] forState:UIControlStateNormal];
    [sendButton setBackgroundImage:[UIImage imageNamed:@"bg_send_sel" scale:2] forState:UIControlStateHighlighted];
    [sendButton addTarget:self action:@selector(publishCommentClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:sendButton];
    
    return view;
}

- (void)inputFieldEditingDidBegin:(id)sender
{
    hintImageView.hidden = true;
    hintLabel.hidden = true;
}

- (void)publishCommentClick:(id)sender
{
    NSString *content = inputField.text;
    if ([NSString isNull:content]) {
        return;
    }
    
    [inputField resignFirstResponder];
    
    ZKHCommentEntity *comment = [[ZKHCommentEntity alloc] init];
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        comment.publisher = user.code;
    }else{
        comment.publisher = [ZKHViewUtils deviceId];
    }
    if (!comment.publisher) {
        comment.publisher = [NSDate currentTimeString];
    }
    comment.discountId = self.discount.uuid;
    comment.content = content;
    comment.type = KEY_TYPE_COMMENT;
    
    //发布评论
    [[ZKHProcessor getInstance] publishComment:comment completionHandler:^(ZKHCommentEntity *comment) {
        if (comment) {
            if (self.discount.comments) {
                [self.discount.comments insertObject:comment atIndex:0];
            }else{
                NSMutableArray *comments = [[NSMutableArray alloc] init];
                [comments addObject:comment];
                self.discount.comments = comments;
            }
            self.discount.commentCount += 1;
            [self.tableView reloadData];
            [self.view makeToast:@"发送评论成功."];
        }else{
            [self.view makeToast:@"发送评论失败."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (ZKHActivityFrontTitleCell *) getActivityFrontTitleCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isFront:(Boolean) isFront
{
    NSString *CellIdentifier = ActivityStorefrontTitleCellIdentifier;
    ZKHActivityFrontTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.cellView.layer.borderWidth = 0.5;
    cell.cellView.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    if (isFront) {
        cell.titleImageView.image = [UIImage imageNamed:@"store_icon" scale:2];
    }else{
        cell.titleImageView.image = [UIImage imageNamed:@"comment_icon" scale:2];
    }
    return cell;
}

- (ZKHActivityStorefrontCell *) getActivityStorefrontCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath frontIndex:(long) frontIndex
{
    NSString *CellIdentifier = ActivityStorefrontCellIdentifier;
    ZKHActivityStorefrontCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    ZKHStorefrontEntity *front = self.discount.storefronts[frontIndex];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@-%@", front.storeName, front.name];
    cell.addrLabel.text = [NSString stringWithFormat:@"%@", front.addr];
    cell.addrLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.addrLabel.numberOfLines = 0;
    
    cell.nameLabel.hidden = false;
    cell.positionImageView.hidden = false;
    cell.distanceLabel.hidden = false;
    
    if (front.distance == DBL_MAX) {
        cell.distanceLabel.hidden = true;
    }else{
        cell.distanceLabel.hidden = false;
        cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", front.distance/1000];
    }
    
    CGFloat lineHeight = 0.5f;
    cell.cellView.layer.borderWidth = lineHeight;
    cell.cellView.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    
    [ZKHViewUtils drawStraightLine:cell startPoint:CGPointMake(5 + lineHeight, 0) color:[UIColor whiteColor] lineSize:CGSizeMake(320 - 10 - lineHeight * 2, lineHeight * 2)];
    
    [cell updateViews:front];
    return cell;
}

- (ZKHActivityCommentCell *) getActivityCommentCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath frontIndex:(long) commentIndex
{
    NSString *CellIdentifier = ActivityCommentCellIdentifier;
    ZKHActivityCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZKHCommentEntity *comment = self.discount.comments[commentIndex];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@...%@", [comment.publisher substringToIndex:2], [comment.publisher substringFromIndex:comment.publisher.length - 2]] ;
    cell.timeLabel.text = [[NSDate dateWithMilliSeconds:[comment.publishTime longLongValue]] toyyyyMMddString];
    cell.contentLabel.text = comment.content;
    
    CGFloat lineHeight = 0.5f;
    cell.cellView.layer.borderWidth = lineHeight;
    cell.cellView.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    
    [ZKHViewUtils drawStraightLine:cell startPoint:CGPointMake(5 + lineHeight, 0) color:[UIColor whiteColor] lineSize:CGSizeMake(320 - 10 - lineHeight * 2, lineHeight * 2)];
    return cell;
}

- (ZKHShowMoreCell *) getShowMoreCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath loading:(Boolean)loading isFront:(Boolean) isFront
{
    NSString *CellIdentifier = ShowMoreCellIdentifier;
    ZKHShowMoreCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (loading) {
        cell.loadingLabel.hidden = false;
        cell.showAllImage.hidden = true;
        cell.showAllLabel.hidden = true;
    }else{
        cell.loadingLabel.hidden = true;
        cell.showAllImage.hidden = false;
        cell.showAllLabel.hidden = false;
        
        cell.showAllLabel.userInteractionEnabled = TRUE;
        if (isFront) {
           [cell.showAllLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showMoreFronts:)]];
        }else{
            [cell.showAllLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showMoreComments:)]];
        }
        
    }
    
    CGFloat lineHeight = 0.5f;
    cell.cellView.layer.borderWidth = lineHeight;
    cell.cellView.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    
    [ZKHViewUtils drawStraightLine:cell startPoint:CGPointMake(5 + lineHeight, 0) color:[UIColor whiteColor] lineSize:CGSizeMake(320 - 10 - lineHeight * 2, lineHeight * 2)];
    
    return cell;
}

- (long) frontDisplayCount
{
    int frontDisplayCount = 0;
    if (frontLoaded) {
        int storefrontCount = self.discount.frontCount;
        if (storefrontCount > DEFAULT_DISCOUNT_FRONTS_NUM) {
            frontDisplayCount = DEFAULT_DISCOUNT_FRONTS_NUM + 1;//需显示“更多”
        }else{
            frontDisplayCount = storefrontCount;
        }
    }else{
        frontDisplayCount = 1;//默认显示加载中
    }
    return frontDisplayCount;
}

- (long) commentDisplayCount
{
    int commentDisplayCount = 0;
    if (commentLoaded) {
        int commentCount = self.discount.commentCount;
        if (commentCount > DEFAULT_DISCOUNT_COMMENTS_NUM) {
            commentDisplayCount = DEFAULT_DISCOUNT_COMMENTS_NUM + 1;//需显示“更多”
        }else{
            commentDisplayCount = commentCount;
        }
    }else{
        commentDisplayCount = 1;//默认显示加载中
    }
    return commentDisplayCount;
}

- (void) showMoreFronts:(id)sender
{
   ZKHDiscountStorefrontController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"DiscountStorefrontController"];
    controller.discount = self.discount;
    [self.navigationController pushViewController:controller animated:true];
}

- (void) showMoreComments:(id)sender
{
    ZKHDiscountCommentController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"DiscountCommentController"];
    controller.discount = self.discount;
    [self.navigationController pushViewController:controller animated:true];
}

@end

@implementation ZKHActivityTermCell

@end

@implementation ZKHActivityFrontTitleCell

@end

//
//  ZKHActivityImageCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHImageActionDelegate.h"
#import "ZKHPageControl.h"

@interface ZKHActivityImageCell : UITableViewCell<UIScrollViewDelegate, ZKHImageActionDelegate>

{
    NSMutableArray *images;
    UIViewController *parentViewController;
    CGSize scaleSize;
    NSMutableArray *imageViews;
    BOOL pageControlUsed;
    ZKHPageControl *pageControl;
    
    UIMenuController *menuController;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (void) updateViews:(UIViewController *) viewController images:(NSMutableArray *)images scaleSize:(CGSize)size;

@end

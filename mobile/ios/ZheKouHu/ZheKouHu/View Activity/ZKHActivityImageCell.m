//
//  ZKHActivityImageCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHActivityImageCell.h"
#import "ZKHImagePreviewController.h"
#import "ZKHImageListPreviewController.h"
#import "ZKHImageLoader.h"

@implementation ZKHActivityImageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateViews:(UIViewController *)viewController images:(NSMutableArray *)_images scaleSize:(CGSize)size
{
    scaleSize = size;
    images = _images;
    parentViewController = viewController;
    
    long kNumberOfPages = [images count];
    
    NSMutableArray *views = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [views addObject:[NSNull null]];
    }
    imageViews = views;
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * kNumberOfPages, self.scrollView.frame.size.height);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    int pageControlHeight = 10;
    if (!pageControl) {
        pageControl = [[ZKHPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - pageControlHeight, self.frame.size.width, pageControlHeight)];
        [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:pageControl];
        
        pageControl.numberOfPages = kNumberOfPages;
        pageControl.currentPage = 0;
        pageControl.backgroundColor = [UIColor clearColor];
    }
    
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    
    self.scrollView.userInteractionEnabled = true;
    [self.scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doImageViewClicked:)]];
    
//    UILongPressGestureRecognizer * longPressGr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressImageView:)];
//    longPressGr.minimumPressDuration = 2.0;
//    [self.scrollView addGestureRecognizer:longPressGr];
}

- (void)loadScrollViewWithPage:(long)page {
	long kNumberOfPages = [images count];
	
    if (page < 0) return;
    if (page >= kNumberOfPages) return;
	
    // replace the placeholder if necessary
    UIImageView *view = [imageViews objectAtIndex:page];
    if ((NSNull *)view == [NSNull null]) {
        ZKHFileEntity *imageFile = images[page];
        view = [[UIImageView alloc] init];
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        view.frame = frame;
        [self.scrollView addSubview:view];
       
        [ZKHImageLoader showImageForName:imageFile.aliasName imageView:view toScale:1.5  ];
        
        [imageViews replaceObjectAtIndex:page withObject:view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (pageControlUsed) {
        return;
    }
	
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pageControl.currentPage = page;
	
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
	
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    pageControlUsed = NO;
}

- (IBAction)changePage:(id)sender {
    long page = pageControl.currentPage;

    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:YES];
    
    pageControlUsed = YES;
    
    [pageControl updateDots];
}

- (void)doImageViewClicked:(id)sender
{
    ZKHImageListPreviewController *controller = [[ZKHImageListPreviewController alloc] init];
    
    controller.currentIndex = pageControl.currentPage;
    controller.imageFiles = images;
    [parentViewController.navigationController pushViewController:controller animated:YES];
}

//- (void)handleLongPressImageView:(UILongPressGestureRecognizer *)sender
//{
//    if (!menuController) {
//        menuController = [UIMenuController sharedMenuController];
//        UIMenuItem *saveItem = [[UIMenuItem alloc] initWithTitle:@"保存到相册" action:@selector(saveToPhotoDir:)];
//        menuController.menuItems = @[saveItem];
//        [menuController setTargetRect:CGRectMake(0, 0, 50, 50) inView:self];
//    }
//    [menuController setMenuVisible:YES animated:YES];
//}
//- (void) saveToPhotoDir:(id)sender
//{
//    
//}

@end

//
//  ZKHDiscountMainController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullTableView.h"
#import "ZKHEntity.h"

@interface ZKHActivityMainController : UITableViewController<PullTableViewDelegate>
{
    UIBarButtonItem *negativeSpacer;
    UIBarButtonItem *logoItem;
    UIBarButtonItem *searchItem;
    
    int offset;
    NSMutableArray *categories;
    NSMutableArray *activities;
}

@end

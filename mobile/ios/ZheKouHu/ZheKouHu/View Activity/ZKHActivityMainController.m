//
//  ZKHDiscountMainController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHActivityMainController.h"
#import "ZKHProcessor+Category.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHMainActivityCell.h"
#import "ZKHMainCategoryCell.h"
#import "ZKHImageLoader.h"
#import "ZKHViewUtils.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"
#import "ZKHContext.h"
#import "ZKHConst.h"

#define kAddedCellNumber 3

static NSString *CategoryCellIdentifier = @"MainCategoryCell";
static NSString *ActivityTitleCellIdentifier = @"MainActivityTitleCell";
static NSString *ActivityCellIdentifier = @"MainActivityCell";
static NSString *BlankCellIdentifier = @"BlankCell";

@implementation ZKHActivityMainController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(31, 0, 56, 25)];
    logoView.image = [UIImage imageNamed:@"logo_icon.png" scale:2];
    logoItem = [[UIBarButtonItem alloc] initWithCustomView:logoView];
    
    UIButton *searchButton = [[UIButton alloc] init];
    [searchButton setImage:[UIImage imageNamed:@"search.png"scale:2] forState:UIControlStateNormal];
    searchButton.frame = CGRectMake(0, 0, 44, 44);
    [searchButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:searchButton.frame.size] forState:UIControlStateHighlighted];
    [searchButton addTarget:self action:@selector(doSearch:) forControlEvents:UIControlEventTouchUpInside];
    
    searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    
    negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = 10;
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    
    [[ZKHProcessor getInstance] categories:^(NSMutableArray *result) {
        categories = result;
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ZKHContext *context = [ZKHContext getInstance];
    if (context.location) {
        [self refreshTable];
    }
    
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNavigation];
    
    ZKHContext *context = [ZKHContext getInstance];
    //if (!context.location) {
        [context addObserver:self forKeyPath:@"location" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    //}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[ZKHContext class]] && [keyPath isEqualToString:@"location"]) {
        [self refreshTable];
    }
}

- (void) updateNavigation
{
    UINavigationItem *nav  = nil;
    if ([self.parentViewController isKindOfClass:[UITabBarController class]]) {
        nav = self.parentViewController.navigationItem;
    }else{
        nav = self.navigationItem;
    }
    nav.title = nil;
    nav.rightBarButtonItem = searchItem;
    nav.leftBarButtonItems = [NSArray
                             arrayWithObjects:negativeSpacer, logoItem, nil];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    ZKHContext *context = [ZKHContext getInstance];
    [context removeObserver:self forKeyPath:@"location"];
    
    [super viewWillDisappear:animated];
}

- (void) doSearch:(id)sender
{
    UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHSearchDiscountsController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [[ZKHProcessor getInstance] newestActivities:0 completionHandler:^(NSMutableArray *discounts) {
        if ([discounts count] > 0) {
            offset = [discounts count];
            activities = discounts;
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [[ZKHProcessor getInstance] newestActivities:offset completionHandler:^(NSMutableArray *discounts) {
        if ([discounts count] > 0) {
            offset += [discounts count];
            [activities addObjectsFromArray:discounts];
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [activities count] + kAddedCellNumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = nil;
    switch (indexPath.row) {
        case 0:
        {
            CellIdentifier = CategoryCellIdentifier;
            ZKHMainCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            [cell updateViews:self categories:categories];
            return cell;
        }
        case 1:
        {
            CellIdentifier = BlankCellIdentifier;
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            cell.backgroundColor = [UIColor color_f5f5f5];
            return cell;
        }
        case 2:
            CellIdentifier = ActivityTitleCellIdentifier;
            break;
        default:
            CellIdentifier = ActivityCellIdentifier;
            ZKHMainActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
            
            ZKHDiscountEntity *discount = activities[indexPath.row - kAddedCellNumber];
            ZKHFileEntity *file = discount.images[0];
            [ZKHImageLoader showImageForName:file.aliasName imageView:cell.photoView toScale:3];
            //cell.contentMode = UIViewContentModeScaleAspectFill;
            //[ZKHImageLoader showImageForName:file.aliasName imageView:cell.photoView];
            
            cell.contentLabel.text = discount.content;
            if ([discount.origin isEqualToString:VAL_DISCOUNT_ORIGIN_STORER]) {
                cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_STORER;
            }else{
                cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_CUSTOMER;
            }
            
            NSMutableArray *fronts = discount.storefronts;
            if ([fronts count] > 0) {
                ZKHStorefrontEntity *front = fronts[0];
                
                cell.storeLabel.hidden = false;
                cell.storeLabel.text = [NSString stringWithFormat:@"%@-%@", front.storeName, front.name];
                if (front.distance == DBL_MAX) {
                    cell.distanceLabel.hidden = true;
                }else{
                    cell.distanceLabel.hidden = false;
                    cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", front.distance/1000];
                }
                [cell updateViews:front];
            }else{
                cell.storeLabel.hidden = true;
                cell.distanceLabel.hidden = true;
            }
            cell.tag = indexPath.row;
            
            return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    switch (indexPath.row) {
        case 0:
            height = [ZKHMainCategoryCell cellHeight];
            break;
        case 1:
            height = 4;
            break;
        case 2:
            height = 23;
            break;
        default:
            height = 86;
            break;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        cell.backgroundColor = [UIColor color_f5f5f5];
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([sender isKindOfClass:[ZKHMainActivityCell class]]) {
        long index = ((ZKHMainActivityCell *)sender).tag;
        ZKHDiscountEntity *discount = activities[index - kAddedCellNumber];
        if ([dest respondsToSelector:@selector(setDiscount:)]) {
            [dest setValue:discount forKey:@"discount"];
        }
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}
@end

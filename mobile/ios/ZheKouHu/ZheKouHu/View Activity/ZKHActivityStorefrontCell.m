//
//  ZKHActivityStorefrontCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHActivityStorefrontCell.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"

@implementation ZKHActivityStorefrontCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.positionImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"position" scale:2]];
    [self addSubview:self.positionImageView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void) updateViews:(ZKHStorefrontEntity *)front
{
    if (front.distance == DBL_MAX) {
        self.positionImageView.hidden = true;
        return;
    }
    self.positionImageView.hidden = false;
    NSString *distanceText = self.distanceLabel.text;
    CGFloat countX = 320 - 16 - distanceText.length * 5;
    
    self.positionImageView.frame = CGRectMake(countX - 13, 58 - 14 - 8, 10, 10);
    
    CGFloat addrWidth = self.positionImageView.frame.origin.x - self.addrLabel.frame.origin.x;
    int num = addrWidth / 12.0;
    if ([front.addr length] > num) {
        self.addrLabel.text = [NSString stringWithFormat:@"%@...", [front.addr substringToIndex:num]];
    }
}
@end

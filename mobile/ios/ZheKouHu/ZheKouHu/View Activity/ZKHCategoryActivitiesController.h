//
//  ZKHCategoryActivitiesController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullTableView.h"
#import "ZKHEntity.h"
#import "ZKHBackTableViewController.h"

@interface ZKHCategoryActivitiesController : ZKHBackTableViewController<PullTableViewDelegate>
{
    NSMutableArray *activities;
    int offset;
}

@property (strong, nonatomic) ZKHCategoryEntity *category;

@end

//
//  ZKHCategoryActivitiesController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHCategoryActivitiesController.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHMainActivityCell.h"
#import "ZKHImageLoader.h"
#import "ZKHViewUtils.h"
#import "ZKHContext.h"
#import "ZKHConst.h"

static NSString *CellIdentifier = @"CategoryActivityCell";

@implementation ZKHCategoryActivitiesController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
      
    self.title = self.category.name;
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    ZKHContext *context = [ZKHContext getInstance];
    if (context.location) {
        [self refreshTable];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[ZKHContext class]] && [keyPath isEqualToString:@"location"]) {
        [self refreshTable];
    }
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    ZKHContext *context = [ZKHContext getInstance];
    //if (!context.location) {
        self.dataLoaded = true;
        [context addObserver:self forKeyPath:@"location" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    //}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated
{
    ZKHContext *context = [ZKHContext getInstance];
    [context removeObserver:self forKeyPath:@"location"];
    
    [super viewWillDisappear:animated];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [[ZKHProcessor getInstance] activitiesForCategory:self.category.uuid offset:0 completionHandler:^(NSMutableArray *discounts) {
        if ([discounts count] > 0) {
            offset = [discounts count];
            activities = discounts;
        }
        self.dataLoaded = true;
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [[ZKHProcessor getInstance] activitiesForCategory:self.category.uuid offset:offset completionHandler:^(NSMutableArray *discounts) {
        if ([discounts count] > 0) {
            offset += [discounts count];
            [activities addObjectsFromArray:discounts];
            [self.tableView reloadData];
        }
        
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long rows = [activities count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHMainActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.tag = indexPath.row;
    
    ZKHDiscountEntity *discount = activities[indexPath.row];
    
    ZKHFileEntity *file = discount.images[0];
    [ZKHImageLoader showImageForName:file.aliasName imageView:cell.photoView toScale:3];
    
    cell.contentLabel.text = discount.content;
    
    if ([discount.origin isEqualToString:VAL_DISCOUNT_ORIGIN_STORER]) {
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_STORER;
    }else{
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_CUSTOMER;
    }
    
    NSMutableArray *fronts = discount.storefronts;
    if ([fronts count] > 0) {
        ZKHStorefrontEntity *front = fronts[0];
        
        cell.storeLabel.hidden = false;
        cell.storeLabel.text = [NSString stringWithFormat:@"%@-%@", front.storeName, front.name];
        if (front.distance == DBL_MAX) {
            cell.distanceLabel.hidden = true;
        }else{
            cell.distanceLabel.hidden = false;
            cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", front.distance/1000];
        }
        [cell updateViews:front];
    }else{
        cell.storeLabel.hidden = true;
        cell.distanceLabel.hidden = true;
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([sender isKindOfClass:[ZKHMainActivityCell class]]) {
        long index = ((ZKHMainActivityCell *)sender).tag;
        ZKHDiscountEntity *discount = activities[index];
        if ([dest respondsToSelector:@selector(setDiscount:)]) {
            [dest setValue:discount forKey:@"discount"];
        }
    }
}

@end

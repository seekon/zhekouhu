//
//  ZKHCommentsController.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullTableView.h"
#import "ZKHBackTableViewController.h"
#import "ZKHEntity.h"

@interface ZKHDiscountCommentController : ZKHBackTableViewController<PullTableViewDelegate>
{
    long offset;
    UIImageView *hintImageView;
    UILabel *hintLabel;
    UITextField *inputField;
}

@property (strong, nonatomic) ZKHDiscountEntity *discount;

@end

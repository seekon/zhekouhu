//
//  ZKHCommentsController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHDiscountCommentController.h"
#import "ZKHViewUtils.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHConst.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"
#import "NSDate+Utils.h"
#import "ZKHActivityCommentCell.h"
#import "UIView+Toast.h"
#import "ZKHContext.h"
#import "NSString+Utils.h"

static NSString *CellIdentifier = @"DiscountCommentCell";


@interface ZKHDiscountCommentController ()

@end

@implementation ZKHDiscountCommentController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    [self refreshTable];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [[ZKHProcessor getInstance] commentsByDiscount:self.discount limit:DEFAULT_PAGE_SIZE offset:0 completionHandler:^(NSMutableArray *comments) {
        self.dataLoaded = true;
        if ([comments count] > 0) {
            self.discount.comments = comments;
            offset = [comments count];
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [[ZKHProcessor getInstance] commentsByDiscount:self.discount limit:DEFAULT_PAGE_SIZE offset:offset completionHandler:^(NSMutableArray *comments) {
        if ([comments count] > 0) {
            [self.discount.storefronts addObjectsFromArray:comments];
            offset += [comments count];
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.dataLoaded) {
        [super backgroundViewHidden:false];
        return 0;
    }
    
    [super backgroundViewHidden:true];
    return [self.discount.comments count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHActivityCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (indexPath.row == [self.discount.comments count]) {
        cell.nameLabel.hidden = true;
        cell.timeLabel.hidden = true;
        cell.contentLabel.hidden = true;
        return cell;
    }
    
    cell.nameLabel.hidden = false;
    cell.timeLabel.hidden = false;
    cell.contentLabel.hidden = false;
    
    ZKHCommentEntity *comment = self.discount.comments[indexPath.row];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@...%@", [comment.publisher substringToIndex:2], [comment.publisher substringFromIndex:comment.publisher.length - 2]] ;
    cell.timeLabel.text = [[NSDate dateWithMilliSeconds:[comment.publishTime longLongValue]] toyyyyMMddString];
    cell.contentLabel.text = comment.content;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 56;
    if (indexPath.row == [self.discount.comments count])
    {
        CGFloat screenHeight = [ZKHViewUtils screenHeight];
        height = screenHeight - k_navHeight - height * [self.discount.comments count] - 40 - 30;
        if (height <= 0) {
            height = 20;
        }
        return height;
    }
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title = [NSString stringWithFormat:@"共%d条评论", self.discount.commentCount];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(32, 8, 100, 14);
    titleLabel.text = title;
    titleLabel.font = [UIFont systemFontOfSize:13.0];
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 320, 29);
    view.backgroundColor = [UIColor color_f5f5f5];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(11, 8, 14, 14);
    imageView.image = [UIImage imageNamed:@"comment_icon" scale:2];
    
    [view addSubview:imageView];
    [view addSubview:titleLabel];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 320, 50);
    view.backgroundColor = [UIColor color_f5f5f5];
    view.layer.borderWidth = 0.5;
    view.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    //[ZKHViewUtils drawSeparaterAtHeader:view offsetX:0];
    
    inputField = [[UITextField alloc] init];
    inputField.frame = CGRectMake(10, 6, 242, 28);
    inputField.background = [UIImage imageNamed:@"bg_textfield_sel"];
    inputField.font = [UIFont systemFontOfSize:13];
    if (IOS7) {
        inputField.tintColor = [UIColor color_ff6722];
    }else{
        [[inputField valueForKey:@"textInputTraits"] setValue:[UIColor color_ff6722] forKey:@"insertionPointColor"];
    }
    
    [view addSubview:inputField];
    [inputField addTarget:self action:@selector(inputFieldEditingDidBegin:) forControlEvents:UIControlEventEditingDidBegin];
    
    hintImageView= [[UIImageView alloc] init];
    hintImageView.frame = CGRectMake(10, 12, 16, 16);
    hintImageView.image = [UIImage imageNamed:@"edit" scale:2];
    [view addSubview:hintImageView];
    
    hintLabel= [[UILabel alloc] init];
    hintLabel.frame = CGRectMake(30, 5, 50, 30);
    hintLabel.font = [UIFont systemFontOfSize:13];
    hintLabel.text = @"写点评";
    hintLabel.textColor = [UIColor color_7f7f7f];
    [view addSubview:hintLabel];
    
    UIButton *sendButton = [[UIButton alloc] init];
    sendButton.frame = CGRectMake(256, 7, 55, 26);
    sendButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [sendButton setTitle:@"发送" forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton setBackgroundImage:[UIImage imageNamed:@"bg_send_nor" scale:2] forState:UIControlStateNormal];
    [sendButton setBackgroundImage:[UIImage imageNamed:@"bg_send_sel" scale:2] forState:UIControlStateHighlighted];
    [sendButton addTarget:self action:@selector(publishCommentClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:sendButton];
    
    return view;
}

- (void)inputFieldEditingDidBegin:(id)sender
{
    hintImageView.hidden = true;
    hintLabel.hidden = true;
}

- (void)publishCommentClick:(id)sender
{
    NSString *content = inputField.text;
    if ([NSString isNull:content]) {
        return;
    }
    
    [inputField resignFirstResponder];
    
    ZKHCommentEntity *comment = [[ZKHCommentEntity alloc] init];
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        comment.publisher = user.code;
    }else{
        comment.publisher = [ZKHViewUtils deviceId];
    }
    if (!comment.publisher) {
        comment.publisher = [NSDate currentTimeString];
    }
    comment.discountId = self.discount.uuid;
    comment.content = content;
    comment.type = KEY_TYPE_COMMENT;
    
    //发布评论
    [[ZKHProcessor getInstance] publishComment:comment completionHandler:^(ZKHCommentEntity *comment) {
        if (comment) {
            if (self.discount.comments) {
                [self.discount.comments insertObject:comment atIndex:0];
            }else{
                NSMutableArray *comments = [[NSMutableArray alloc] init];
                [comments addObject:comment];
                self.discount.comments = comments;
            }
            self.discount.commentCount += 1;
            [self.tableView reloadData];
            [self.view makeToast:@"发送评论成功."];
        }else{
            [self.view makeToast:@"发送评论失败."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}
@end

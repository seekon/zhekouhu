//
//  ZKHDiscountStorefrontController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHDiscountStorefrontController.h"
#import "ZKHViewUtils.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHConst.h"
#import "ZKHActivityStorefrontCell.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"

static NSString *CellIdentifier = @"DiscountStorefrontCell";

@interface ZKHDiscountStorefrontController ()

@end

@implementation ZKHDiscountStorefrontController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    [self refreshTable];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [[ZKHProcessor getInstance] storeFrontsByDiscount:self.discount limit:DEFAULT_PAGE_SIZE offset:0 completionHandler:^(NSMutableArray *storefronts) {
        self.dataLoaded = true;
        if ([storefronts count] > 0) {
            self.discount.storefronts = storefronts;
            offset = [storefronts count];
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [[ZKHProcessor getInstance] storeFrontsByDiscount:self.discount limit:DEFAULT_PAGE_SIZE offset:offset completionHandler:^(NSMutableArray *storefronts) {
        if ([storefronts count] > 0) {
            [self.discount.storefronts addObjectsFromArray:storefronts];
            offset += [storefronts count];
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!self.dataLoaded) {
        [super backgroundViewHidden:false];
        return 0;
    }
    
    [super backgroundViewHidden:true];
    return [self.discount.storefronts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHActivityStorefrontCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZKHStorefrontEntity *front = self.discount.storefronts[indexPath.row];
    cell.nameLabel.text = [NSString stringWithFormat:@"%@-%@", front.storeName, front.name];
    cell.addrLabel.text = [NSString stringWithFormat:@"%@", front.addr];
    cell.addrLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.addrLabel.numberOfLines = 0;
    
    cell.nameLabel.hidden = false;
    cell.positionImageView.hidden = false;
    cell.distanceLabel.hidden = false;
    
    if (front.distance == DBL_MAX) {
        cell.distanceLabel.hidden = true;
    }else{
        cell.distanceLabel.hidden = false;
        cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", front.distance/1000];
    }
    
    [cell updateViews:front];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *title = [NSString stringWithFormat:@"共%d家门店", self.discount.frontCount];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(28, 8, 100, 14);
    titleLabel.text = title;
    titleLabel.font = [UIFont systemFontOfSize:13.0];
    
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 320, 29);
    view.backgroundColor = [UIColor color_f5f5f5];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.frame = CGRectMake(11, 9, 12, 12);
    imageView.image = [UIImage imageNamed:@"store_icon" scale:2];
    
    [view addSubview:imageView];
    [view addSubview:titleLabel];
    return view;
}

@end

//
//  ZKHMainActivityCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHMainActivityCell.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"

@implementation ZKHMainActivityCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.discountImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"position" scale:2]];
    [self addSubview:self.discountImageView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    self.backgroundColor = [UIColor color_f8f8f8];
}

- (void)updateViews:(ZKHStorefrontEntity *)front
{
    if (front.distance == DBL_MAX) {
        self.discountImageView.hidden = true;
        return;
    }
    
    self.discountImageView.hidden = false;
    
    NSString *distanceText = self.distanceLabel.text;
    CGFloat countX = 320 - 10 - distanceText.length * 5;
    
    self.discountImageView.frame = CGRectMake(countX - 15, 86 - 11 - 10, 10, 10);
}

@end

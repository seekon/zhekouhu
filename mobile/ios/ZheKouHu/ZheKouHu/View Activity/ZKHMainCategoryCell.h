//
//  ZKHMainCategoryCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHMainCategoryCell : UITableViewCell<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    UINib *categorySubItemNib;
    UICollectionView *imageContainer;
    NSMutableArray *categories;
    UIViewController *parentController;
}

+ (CGFloat) cellHeight;

- (void) updateViews:(UIViewController *) viewController categories:(NSMutableArray *)categories;

@end

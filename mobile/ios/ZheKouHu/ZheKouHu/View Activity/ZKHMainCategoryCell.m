//
//  ZKHMainCategoryCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHMainCategoryCell.h"
#import "ZKHCategoryCell.h"
#import "ZKHEntity.h"
#import "ZKHCategoryActivitiesController.h"
#import "ZKHViewUtils.h"

#define kPaddingWidth 10
#define kPaddingWidth_h 10

#define kPaddingHeight 10
#define kPaddingHeight_h 10

static NSString *CellIdentifier = @"CategoryCell";

@implementation ZKHMainCategoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

+ (CGFloat) cellContentWidth
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
        return screenBounds.size.width - 2 * kPaddingWidth;
    }else{
        return screenBounds.size.height - 2 * kPaddingWidth_h;
    }
}

+ (CGFloat) cellHeight
{
    CGFloat height = 168;
    return height;
}

//每个集合视图中单元格的高度
+ (CGFloat) collectionCellWidth
{
    CGFloat width = 0;
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
        width = [self cellContentWidth] /3;
    }else{
        width = [self cellContentWidth]/6;
    }
    return width - 10;
}

+ (CGFloat) collectionCellHeight
{
    CGFloat height = 68;
//    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
//    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
//        height = [self cellContentWidth] /3 - 10;
//    }else{
//        height = [self cellContentWidth]/6 + 10;
//    }
    return height ;
}

+ (CGFloat) collectionCellX
{
    CGFloat x = 0;
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
        x=  kPaddingWidth;
    }else{
        x=  kPaddingWidth_h;
    }
    return x;
}

+ (CGFloat) collectionCellY
{
    CGFloat y = 0;
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
        y = kPaddingHeight;
    }else{
        y = kPaddingHeight_h;
    }
    return y;
}

- (void)updateViews:(UIViewController *)viewController categories:(NSMutableArray *)_categories
{
    categories = _categories;
    parentController = viewController;
    
    CGFloat x = [ZKHMainCategoryCell collectionCellX];
    CGFloat y = [ZKHMainCategoryCell collectionCellY];
    CGFloat width = [ZKHMainCategoryCell cellContentWidth];
    CGFloat height = [ZKHMainCategoryCell cellHeight];
    CGRect frame = CGRectMake(x, y, width, height);
    
    if (!imageContainer) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        imageContainer = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
        imageContainer.backgroundColor = [UIColor clearColor];
        
        imageContainer.dataSource = self;
        imageContainer.delegate = self;
        
        if (!categorySubItemNib) {
            categorySubItemNib = [UINib nibWithNibName:@"ZKHCategoryCell" bundle:nil];
        }
        [imageContainer registerNib:categorySubItemNib forCellWithReuseIdentifier:CellIdentifier];
        
        [self addSubview:imageContainer];
    }else{
        imageContainer.frame = frame;
    }
    
    [imageContainer reloadData];
}

#pragma mark - collectionView datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHCategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZKHCategoryEntity *category = categories[indexPath.row];
    cell.nameLabel.text = category.name;
    cell.iconView.image = [UIImage imageNamed:category.icon];
    cell.tag = indexPath.row;
    
    return cell;
}

#pragma mark --UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellWidth = [ZKHMainCategoryCell collectionCellWidth];
    CGFloat cellHeight = [ZKHMainCategoryCell collectionCellHeight];
    return CGSizeMake(cellWidth, cellHeight);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - collectionView delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
//    UIView *view = [collectionView viewWithTag:indexPath.row];
//    view.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
//    [UIView beginAnimations:nil context:UIGraphicsGetCurrentContext()];
//    [UIView setAnimationDuration:2.0f];//动画时间
//    view.transform = CGAffineTransformMakeScale(0.01f, 0.01f);
//    [UIView commitAnimations]; //启动动画
    
    ZKHCategoryActivitiesController *controller = [[ZKHViewUtils mainStoryboard]instantiateViewControllerWithIdentifier:@"ZKHCategoryActivitiesController"];
    controller.category = categories[indexPath.row];
    [parentController.navigationController pushViewController:controller animated:YES];
}

@end

//
//  ZKHSearchDiscountsController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-8.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHBackTableViewController.h"
#import "PullTableView.h"

@interface ZKHSearchDiscountsController : ZKHBackTableViewController<UISearchBarDelegate, PullTableViewDelegate>
{
    int offset;
    NSMutableArray *discounts;
    NSString *searchWord;
}
@end

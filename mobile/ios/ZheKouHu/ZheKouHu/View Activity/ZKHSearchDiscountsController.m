//
//  ZKHSearchDiscountsController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-8.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHSearchDiscountsController.h"
#import "ZKHViewUtils.h"
#import "ZKHSearchBar.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHMainActivityCell.h"
#import "ZKHImageLoader.h"
#import "ZKHEntity.h"
#import "ZKHConst.h"

static NSString *CellIdentifier = @"SearchDiscountCell";

@implementation ZKHSearchDiscountsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.dataLoaded = true;
    
    ZKHSearchBar *searchBar = [[ZKHSearchBar alloc] initWithFrame:CGRectMake(0, 0, 256, 44)];
    searchBar.delegate = self;
    searchBar.placeholder = @"请输入店铺、活动";
    [searchBar setBackgroundImage:[UIImage imageNamed:@"bg_nav"]];
    
    [searchBar becomeFirstResponder];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 256, 44)];
    [titleView addSubview:searchBar];
    [searchBar setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[searchBar]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(titleView, searchBar)]];
    
    
    [titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searchBar]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(titleView, searchBar)]];
    self.navigationItem.titleView = titleView;
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    searchWord = searchBar.text;
    
    [[ZKHProcessor getInstance] discountsByKeyword:searchWord offset:0 completionHandler:^(NSMutableArray *result) {
        offset = [result count];
        discounts = result;
        [self.tableView reloadData];
        
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];

}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [[ZKHProcessor getInstance] discountsByKeyword:searchWord offset:0 completionHandler:^(NSMutableArray *result) {
        if ([result count] > 0) {
            offset = [result count];
            discounts = result;
            [self.tableView reloadData];
        }
        
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [[ZKHProcessor getInstance] discountsByKeyword:searchWord offset:offset completionHandler:^(NSMutableArray *result) {
        if ([result count] > 0) {
            offset += [result count];
            [discounts addObjectsFromArray:result];
            [self.tableView reloadData];
        }
        
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rows = [discounts count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHMainActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.tag = indexPath.row;
    
    ZKHDiscountEntity *discount = discounts[indexPath.row];
    
    ZKHFileEntity *file = discount.images[0];
    [ZKHImageLoader showImageForName:file.aliasName imageView:cell.photoView toScale:3];
    
    Boolean searchFront = false;
    cell.contentLabel.text = discount.content;
    NSRange range = [discount.content rangeOfString:searchWord];
    if (range.location == NSNotFound) {
        searchFront = true;
    }
    
    if ([discount.origin isEqualToString:VAL_DISCOUNT_ORIGIN_STORER]) {
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_STORER;
    }else{
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_CUSTOMER;
    }
    
    NSMutableArray *fronts = discount.storefronts;
    if ([fronts count] > 0) {
        ZKHStorefrontEntity *front = nil;
        if (searchFront) {
            for (ZKHStorefrontEntity *sFront in fronts) {
                NSString *fullName = [NSString stringWithFormat:@"%@-%@", sFront.storeName, sFront.name];
                range = [fullName rangeOfString:searchWord];
                if (range.location != NSNotFound) {
                    front = sFront;
                    break;
                }
            }
        }
        
        if (!front || !searchFront) {
            front = fronts[0];
        }
        
        cell.storeLabel.hidden = false;
        cell.storeLabel.text = [NSString stringWithFormat:@"%@-%@", front.storeName, front.name];
        if (front.distance == DBL_MAX) {
            cell.distanceLabel.hidden = true;
        }else{
            cell.distanceLabel.hidden = false;
            cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", front.distance/1000];
        }
        [cell updateViews:front];
    }else{
        cell.storeLabel.hidden = true;
        cell.distanceLabel.hidden = true;
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([sender isKindOfClass:[ZKHMainActivityCell class]]) {
        int index = ((ZKHMainActivityCell *)sender).tag;
        ZKHDiscountEntity *discount = discounts[index];
        if ([dest respondsToSelector:@selector(setDiscount:)]) {
            [dest setValue:discount forKey:@"discount"];
        }
    }
}
@end

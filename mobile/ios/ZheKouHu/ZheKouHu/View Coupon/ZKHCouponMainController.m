//
//  ZKHCouponMainController2.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHCouponMainController.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHImageLoader.h"
#import "ZKHMainCouponCellHeader.h"
#import "ZKHMainCouponCell.h"
#import "ZKHContext.h"
#import "ZKHViewUtils.h"

#define kAddedSectionNumber 0

static NSString *CellIdentifier = @"MainCouponCell";
static NSString *HeaderIdentifier = @"MainCouponCellHeader";
static NSString *FooterIdentifier = @"MainCouponCellFooter";

@implementation ZKHCouponMainController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    backgroundView = [[[NSBundle mainBundle] loadNibNamed:@"ZKHEmptyTableBGView" owner:self options:nil] lastObject];
    
    backgroundView.hidden = true;
    [self.collectionView setBackgroundView:backgroundView];
    backgroundView.hintLabel.text = @"正在加载数据";
    
    ZKHContext *context = [ZKHContext getInstance];
    if (context.location) {
        [self refreshData];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateNavigation];
    
    ZKHContext *context = [ZKHContext getInstance];
    //if (!context.location) {
        backgroundView.hintLabel.text = @"暂时没有数据";
        [context addObserver:self forKeyPath:@"location" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    //}

}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[ZKHContext class]] && [keyPath isEqualToString:@"location"]) {
        [self refreshData];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    ZKHContext *context = [ZKHContext getInstance];
    [context removeObserver:self forKeyPath:@"location"];
    
    [super viewWillDisappear:animated];
}

- (void)refreshData
{
    [[ZKHProcessor getInstance] storesHasCouponRemoteRefresh:^(NSMutableDictionary *result) {
        backgroundView.hintLabel.text = @"暂时没有数据";
        if ([result count] > 0) {
            coupnStores = result;
            [self.collectionView reloadData];
        }else{
            coupnStores = [[ZKHProcessor getInstance] storesHasCouponFromLocal];
            [self.collectionView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        coupnStores = [[ZKHProcessor getInstance] storesHasCouponFromLocal];
        [self.collectionView reloadData];
    }];
}

- (void) updateNavigation
{
    UINavigationItem *nav  = nil;
    if ([self.parentViewController isKindOfClass:[UITabBarController class]]) {
        nav = self.parentViewController.navigationItem;
    }else{
        nav = self.navigationItem;
    }
    nav.title = self.tabBarItem.title;
    nav.rightBarButtonItem = nil;
    nav.leftBarButtonItems = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - collectionView datasource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    long result = [[coupnStores allKeys] count] + kAddedSectionNumber;
    if (result > 0) {
        backgroundView.hidden = true;
    }else{
        backgroundView.hidden = false;
    }
    return result;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    if (section == 0) {
//        return 1;
//    }
    
    id key = [coupnStores allKeys][section - kAddedSectionNumber];
    NSArray *stores = [coupnStores objectForKey:key];
    long result = [stores count];
    if(result == 0){
        //NSLog(@"%d,result:%ld, key: %@", section, result, key);
    }
    
    return result;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHMainCouponCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.indexPath = indexPath;
    
    ZKHStoreEntity *store = [self store:indexPath];
    cell.nameLabel.text = store.name;
    @try {
        [ZKHImageLoader showImageForName:store.logo.aliasName imageView:cell.imageView toScale:2];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
         return cell;
    }
   
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        ZKHMainCouponCellHeader *header = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:HeaderIdentifier forIndexPath:indexPath];
        
        header.nameLabel.text = [coupnStores allKeys][indexPath.section - kAddedSectionNumber];
        
        return header;
    } else if([kind isEqualToString:UICollectionElementKindSectionFooter]){
        UICollectionReusableView *footer = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:FooterIdentifier forIndexPath:indexPath];
        [ZKHViewUtils drawSeparaterAtHeader:footer offsetX:0];
        [ZKHViewUtils drawSeparaterAtFooter:footer offsetX:0];
        return footer;
    }
    
    return nil;
}

#pragma mark - collectionView delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (ZKHStoreEntity *) store:(NSIndexPath *)indexPath
{
    id key = [coupnStores allKeys][indexPath.section - kAddedSectionNumber];
    return [coupnStores objectForKey:key][indexPath.row];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([sender isKindOfClass:[ZKHMainCouponCell class]]) {
        NSIndexPath *indexPath = ((ZKHMainCouponCell *)sender).indexPath;
//        if (indexPath.section == 0) {
//            return;
//        }
        ZKHStoreEntity *store = [self store:indexPath];
        if ([dest respondsToSelector:@selector(setStore:)]) {
            [dest setValue:store forKey:@"store"];
        }
    }
}
@end

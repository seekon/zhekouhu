//
//  ZKHMainCouponCellHeader.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHMainCouponCellHeader : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end

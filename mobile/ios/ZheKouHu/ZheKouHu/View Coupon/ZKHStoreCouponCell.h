//
//  ZKHStoreCouponCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"

@interface ZKHStoreCouponCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UILabel *storeLabel;
@property (weak, nonatomic) IBOutlet UILabel *parLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *originLabel;

@property (strong, nonatomic) UIImageView *positionImageView;

- (void)updateViews:(ZKHStorefrontEntity *)front;
@end

//
//  ZKHStoreCouponCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStoreCouponCell.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"

@implementation ZKHStoreCouponCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.positionImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"position" scale:2]];
    [self addSubview:self.positionImageView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.backgroundColor = [UIColor color_f8f8f8];
}

- (void)updateViews:(ZKHStorefrontEntity *)front
{
    if (front.distance == DBL_MAX) {
        self.positionImageView.hidden = true;
        return;
    }
    
    self.positionImageView.hidden = false;
    NSString *distanceText = self.distanceLabel.text;
    CGFloat countX = 320 - 10 - distanceText.length * 5;
    
    self.positionImageView.frame = CGRectMake(countX - 15, 86 - 11 - 10, 10, 10);
}

@end

//
//  ZKHStoreCouponsController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"
#import "PullTableView.h"
#import "ZKHBackTableViewController.h"

@interface ZKHStoreCouponsController : ZKHBackTableViewController<PullTableViewDelegate>
{
    NSMutableArray *coupons;
    int offset;
}

@property (strong, nonatomic) ZKHCouponStoreEntity *store;

@end

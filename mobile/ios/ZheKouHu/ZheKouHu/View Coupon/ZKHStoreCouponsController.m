//
//  ZKHStoreCouponsController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStoreCouponsController.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHViewUtils.h"
#import "ZKHStoreCouponCell.h"
#import "ZKHImageLoader.h"
#import "ZKHConst.h"

static NSString *CellIdentifier = @"StoreCouponCell";

@implementation ZKHStoreCouponsController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title =  self.store.name;
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    if (self.store) {
        [[ZKHProcessor getInstance] couponsForStoreRemoteRefresh:self.store.storeId cateId:self.store.categoryId offset:0 completionHandler:^(NSMutableArray *discounts) {
                self.dataLoaded = true;
                if ([discounts count] > 0) {
                    offset = [discounts count];
                    coupons = discounts;
                    [self.tableView reloadData];
                }else{
                    [super backgroundViewHidden:false];
                }
            } errorHandler:^(ZKHErrorEntity *error) {
                
            }];
    }
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
//    [[ZKHProcessor getInstance] couponsForStoreRemoteRefresh:self.store.storeId cateId:self.store.categoryId completionHandler:^(NSMutableArray *discounts) {
//        self.dataLoaded = true;
//        if ([discounts count] > 0) {
//            offset = [discounts count];
//            coupons = discounts;
//            [self.tableView reloadData];
//        }else{
//            [super backgroundViewHidden:false];
//        }
//    } errorHandler:^(ZKHErrorEntity *error) {
//        
//    }];
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    if (self.store.uuid) {
        [[ZKHProcessor getInstance] couponsForStoreRemoteRefresh:self.store.storeId cateId:self.store.categoryId offset:offset completionHandler:^(NSMutableArray *discounts) {
            if ([discounts count] > 0) {
                offset += [discounts count];
                [coupons addObjectsFromArray:discounts];
                [self.tableView reloadData];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long rows = [coupons count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHStoreCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.tag = indexPath.row;
    
    ZKHDiscountEntity *coupon = coupons[indexPath.row];
    
    ZKHFileEntity *file = coupon.images[0];
    [ZKHImageLoader showImageForName:file.aliasName imageView:cell.photoView toScale:3];
    
    cell.contentLabel.text = coupon.content;
    
    if ([coupon.origin isEqualToString:VAL_DISCOUNT_ORIGIN_STORER]) {
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_STORER;
    }else{
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_CUSTOMER;
    }
    
    NSMutableArray *fronts = coupon.storefronts;
    if ([fronts count] > 0) {
        ZKHStorefrontEntity *front;
        for (ZKHStorefrontEntity *sFront in fronts) {
            if ([sFront.storeName isEqualToString:self.store.name]) {
                front = sFront;
            }
        }
        if (!front) {
            front = fronts[0];
        }
        
        cell.storeLabel.hidden = false;
        cell.storeLabel.text = front.name;
        if (front.distance == DBL_MAX) {
            cell.distanceLabel.hidden = true;
        }else{
            cell.distanceLabel.hidden = false;
            cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", front.distance/1000];
        }
        [cell updateViews:front];
    }else{
        cell.storeLabel.hidden = true;
        cell.distanceLabel.hidden = true;
    }
    cell.parLabel.text = [NSString stringWithFormat:@"￥%@", coupon.parValue];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([sender isKindOfClass:[ZKHStoreCouponCell class]]) {
        long index = ((ZKHStoreCouponCell *)sender).tag;
        ZKHDiscountEntity *coupon = coupons[index];
        if ([dest respondsToSelector:@selector(setDiscount:)]) {
            [dest setValue:coupon forKey:@"discount"];
        }
    }
}
@end

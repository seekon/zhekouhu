//
//  ZKHFeedbackController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHTextView.h"
#import "ZKHBackViewController.h"

@interface ZKHFeedbackController : ZKHBackViewController

@property (weak, nonatomic) IBOutlet ZKHTextView *contentField;
@property (weak, nonatomic) IBOutlet UITextField *contactField;

- (IBAction)submit:(id)sender;
- (IBAction)tapTouchDown:(id)sender;

@end

//
//  ZKHFeedbackController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-27.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHFeedbackController.h"
#import "ZKHProcessor+Feedback.h"
#import "NSString+Utils.h"
#import "ZKHContext.h"
#import "ZKHEntity.h"
#import "ZKHViewUtils.h"
#import "UIView+Toast.h"

@implementation ZKHFeedbackController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.contentField.backgroundColor = [UIColor clearColor];
    self.contentField.placeholder = @"请写下您的宝贵意见或建议";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

- (IBAction)submit:(id)sender {
    [self.contentField endEditing:YES];
    [self.contactField endEditing:YES];
    
    NSString *content = self.contentField.text;
    NSString *contact = self.contactField.text;
    
    if ([NSString isNull: content] || content.length > 500) {
        [self.contentField showTipView:@"意见或建议内容必须在500字以内."];
        return;
    }
    if ([NSString isNull:contact]) {
        ZKHUserEntity *user = [ZKHContext getInstance].user;
        if (user) {
            contact = [ZKHContext getInstance].user.code;
        }
    }
    
    [[ZKHProcessor getInstance] submit:content contact:contact completionHandler:^(Boolean result) {
        if (result) {
            [self.view makeToast:@"感谢您提出的宝贵意见和建议."];
            self.contentField.text = @"";
            self.contactField.text = @"";
        }else{
            [self.view makeToast:@"提交失败."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (IBAction)tapTouchDown:(id)sender {
    [self.contentField resignFirstResponder];
    [self.contactField resignFirstResponder];
}


@end

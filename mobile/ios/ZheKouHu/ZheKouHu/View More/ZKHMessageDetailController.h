//
//  ZKHMessageDetailController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHBackViewController.h"
#import "ZKHEntity.h"

@interface ZKHMessageDetailController : ZKHBackViewController
{
    UILabel *contentLabel;
    UILabel *discountLabel;
}

@property (strong, nonatomic) ZKHMessageEntity *message;
@property (nonatomic) Boolean popToRoot;

@end

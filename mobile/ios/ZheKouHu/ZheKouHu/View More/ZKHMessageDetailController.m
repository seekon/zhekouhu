//
//  ZKHMessageDetailController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHMessageDetailController.h"
#import "ZKHConst.h"
#import "ZKHViewUtils.h"
#import "UIColor+Utils.h"
#import "ZKHActivityDetailController.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHProcessor+Message.h"

@implementation ZKHMessageDetailController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    contentLabel = [[UILabel alloc] init];
    CGRect contentFrame = CGRectMake(20, 20, [ZKHViewUtils screenWidth] - 40, [self contentHeight]);
    contentLabel.frame = contentFrame;
    
    contentLabel.numberOfLines = 0;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    contentLabel.font = [UIFont systemFontOfSize:14];
    
    NSMutableAttributedString * attributedString1 = [[NSMutableAttributedString alloc] initWithString:self.message.content];
    NSMutableParagraphStyle * paragraphStyle1 = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle1 setLineSpacing:6];
    [attributedString1 addAttribute:NSParagraphStyleAttributeName value:paragraphStyle1 range:NSMakeRange(0, [self.message.content length])];
    [contentLabel setAttributedText:attributedString1];
    [contentLabel sizeToFit];
    
    [self.view addSubview:contentLabel];
    
    if ([self.message.type isEqualToString:MSG_TYPE_DISCOUNT]) {
        discountLabel = [[UILabel alloc] init];
        discountLabel.textAlignment = NSTextAlignmentRight;
        discountLabel.frame = CGRectMake(contentFrame.origin.x, contentFrame.origin.y + contentFrame.size.height + 20, contentFrame.size.width, 20);
        
        discountLabel.font = [UIFont systemFontOfSize:12];
        NSMutableAttributedString *discount = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"查看活动"]];
        NSRange contentRange = {0,[discount length]};
        [discount addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
        [discount addAttribute:NSForegroundColorAttributeName value:[UIColor color_7f7f7f] range:contentRange];
        discountLabel.attributedText = discount;
        
        discountLabel.userInteractionEnabled = TRUE;
        [discountLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showDiscount:)]];
        
        [self.view addSubview:discountLabel];
    }
    
    UIBarButtonItem *deleteBar = [ZKHViewUtils deleteBarButtonItem];
    UIButton *delButton = (UIButton *)deleteBar.customView;
    [delButton addTarget:self action:@selector(deleteMessage:) forControlEvents:UIControlEventTouchUpInside];
    //self.navigationItem.rightBarButtonItem = deleteBar;
}

- (void)showDiscount:(id) sender
{
    [[ZKHProcessor getInstance] discountsById:self.message.msgId completionHandler:^(ZKHDiscountEntity *discount) {
        if (discount) {
            ZKHActivityDetailController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHActivityDetailController"];
            controller.discount = discount;
            [self.navigationController pushViewController:controller animated:YES];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
    
}

- (void)deleteMessage:(id) sender
{
    [[ZKHProcessor getInstance] deleteMessageById:self.message.uuid];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (CGFloat) contentHeight
{
    int fontSize = 14;
    CGFloat cellRightWidth = [ZKHViewUtils screenWidth] - 40;
    int mod = (fontSize * self.message.content.length) % [[[NSDecimalNumber alloc]initWithFloat:cellRightWidth] intValue];
    int rows = fontSize * self.message.content.length / cellRightWidth;
    if (rows == 0) {
        rows = 1;
    }else{
        if (mod > 0) {
            rows++;
        }
    }
    
    return  rows * 19;
}

- (void)back:(id)sender
{
    if (self.popToRoot) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [super back:sender];
    }
}
@end

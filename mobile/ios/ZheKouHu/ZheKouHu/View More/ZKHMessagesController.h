//
//  ZKHMessagesController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHBackTableViewController.h"
#import "PullTableView.h"

@interface ZKHMessagesController : ZKHBackTableViewController<PullTableViewDelegate>
{
    NSMutableArray *messages;
    int offset;
    
    UIBarButtonItem *cleanBarItem;
}
@end

@interface ZKHMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *contentLable;

@property (weak, nonatomic) IBOutlet UILabel *timeLable;
@end
//
//  ZKHMessagesController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHMessagesController.h"
#import "ZKHViewUtils.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHEntity.h"
#import "ZKHProcessor+Message.h"
#import "NSDate+Utils.h"
#import "UIViewController+CWPopup.h"
#import "SCLAlertView.h"
#import "SCLAlertViewStyleKit.h"

static NSString *CellIdentifier = @"MessageCell";

@implementation ZKHMessagesController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    messages = [[NSMutableArray alloc] init];
    
    UIButton *deleteButton = [[UIButton alloc] init];
    [deleteButton setImage:[UIImage imageNamed:@"clean.png" scale:2] forState:UIControlStateNormal];
    deleteButton.frame = CGRectMake(0, 2, 40, 40);
    [deleteButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:deleteButton.frame.size] forState:UIControlStateHighlighted];
    [deleteButton addTarget:self action:@selector(clean:) forControlEvents:UIControlEventTouchUpInside];
    cleanBarItem = [[UIBarButtonItem alloc] initWithCustomView:deleteButton];
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    [self loadMoreDataToTable];
    
    backgroundView.hintLabel.text = @"暂时没有收到消息";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clean:(id)sender
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"确定" actionBlock:^(void) {
        [[ZKHProcessor getInstance] deleteMessages];
        self.navigationItem.rightBarButtonItem = nil;
        
        offset = 0;
        [messages removeAllObjects];
        [self.tableView reloadData];
    }];
    [alert showCustom:self.navigationController image:SCLAlertViewStyleKit.imageOfNotice color:[UIColor color_ff6722] title:@"提示" subTitle:@"是否清空所有消息?" closeButtonTitle:@"取消" duration:0.0f];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    self.dataLoaded = true;
    NSMutableArray *result = [[ZKHProcessor getInstance] messages:offset];
    if ([result count] > 0) {
        offset += [result count];
        [messages addObjectsFromArray:result];
        
        if ([messages count] > 0) {
            self.navigationItem.rightBarButtonItem = cleanBarItem;
        }else{
            self.navigationItem.rightBarButtonItem = nil;
        }
        [self.tableView reloadData];
    }
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:1.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long rows = [messages count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.tag = indexPath.row;
    
    ZKHMessageEntity *message = messages[indexPath.row];
    cell.contentLable.text = message.content;
    cell.timeLable.text = [[NSDate dateWithMilliSeconds:[message.sendTime longLongValue]] toyyyyMMddString];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([sender isKindOfClass:[ZKHMessageCell class]]) {
        long index = ((ZKHMessageCell *)sender).tag;
        ZKHMessageEntity *message = messages[index];
        if ([dest respondsToSelector:@selector(setMessage:)]) {
            [dest setValue:message forKey:@"message"];
        }
    }
}
@end

@implementation ZKHMessageCell

@end

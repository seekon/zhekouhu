//
//  ZKHMoreMainController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"
#import "MBSwitch.h"
#import "ZKHBackTableViewController.h"

@interface ZKHMoreMainController : ZKHBackTableViewController
{
    ZKHSettingEntity *messageSetting;
    MBSwitch *messageSwitch;
    NSString *appVersion;
    int logoutClickCount;
}

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIView *switchView;

- (void)messageSwitchValueChanged:(id)sender;

@end

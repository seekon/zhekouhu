//
//  ZKHMoreMainController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHMoreMainController.h"
#import "ZKHContext.h"
#import "ZKHProcessor+Setting.h"
#import "ZKHConst.h"
#import "NSString+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHViewUtils.h"
#import "UIView+Toast.h"
#import "ZKHContext.h"
#import "ZKHProcessor+User.h"

@implementation ZKHMoreMainController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{    
    [super viewDidLoad];
    
    messageSwitch = [[MBSwitch alloc] initWithFrame:CGRectMake(0, 0, 48, 33)];
    [messageSwitch setOnTintColor:[UIColor color_ff6722]];
    [messageSwitch addTarget:self action:@selector(messageSwitchValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.switchView addSubview:messageSwitch];
    
    messageSetting = [[ZKHProcessor getInstance] settingByCode:KEY_SETTING_MESSAGE_REMIND];
    if (!messageSetting) {
        messageSetting = [[ZKHSettingEntity alloc] init];
        messageSetting.uuid = KEY_SETTING_MESSAGE_REMIND;
        messageSetting.value = @"1";
    }
    [messageSwitch setOn:[messageSetting.value isTrue]];
    
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    CFShow((__bridge CFTypeRef)(infoDic));
    appVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];
    
    self.versionLabel.text = [NSString stringWithFormat:@"当前版本V%@", appVersion];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    long sections = [super numberOfSectionsInTableView:tableView];
    return [ZKHContext getInstance].user ? sections : sections - 1;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([dest respondsToSelector:@selector(setUrlString:)]) {
        [dest setValue:@"/user_help.html" forKey:@"urlString"];
    }
}
- (void)messageSwitchValueChanged:(id)sender {
    NSString *devId = [ZKHViewUtils deviceId];
    if (devId) {
        messageSetting.value = messageSwitch.isOn ? @"1" : @"0";
        if ([messageSetting.value isTrue]) {
            [[ZKHProcessor getInstance] setNotifyState:devId state:@"1" completionHandler:^(Boolean result) {
                if (result) {
                   [[ZKHProcessor getInstance] save:@[messageSetting]];
                }
            } errorHandler:^(ZKHErrorEntity *error) {
                
            }];
            
        }else{
            [[ZKHProcessor getInstance] setNotifyState:devId state:@"0" completionHandler:^(Boolean result) {
                if (result) {
                    [[ZKHProcessor getInstance] save:@[messageSetting]];
                }
            } errorHandler:^(ZKHErrorEntity *error) {
                
            }];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return section == 0 ? 6 : 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor color_f5f5f5];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, section == 0 ? 6 : 5, 320, 0.5)];
    lineView.backgroundColor = [UIColor color_e3e3e3];
    [view addSubview:lineView];
    
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor color_f5f5f5];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.5)];
    lineView.backgroundColor = [UIColor color_e3e3e3];
    [view addSubview:lineView];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    long section = indexPath.section;
    long row = indexPath.row;
    
    if (section == 2 && row == 2){//注销帐户
        [self logout];
    }else{
        logoutClickCount = 0;
    }
    
    if (section == 1 && row == 0) {//检查版本
        [self checkVersion];
        return;
    }
    
}

- (void) logout
{
    logoutClickCount++;
    if (logoutClickCount < 2) {
        [self.view makeToast:@"再次点击将退出店铺."];
        return;
    }
    Boolean result = [[ZKHProcessor getInstance] logout];
    if (result) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)checkVersion
{
    [[ZKHProcessor getInstance] newestVersion:^(NSString *result) {
        if ([appVersion isEqualToString:result]) {
            [self.view makeToast:@"已经是最新版，不需要更新." duration:3 position:@"bottom"];
        }else{
            UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHVersionController"];
            [self.navigationController pushViewController:controller animated:YES];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

@end

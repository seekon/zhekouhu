//
//  ZKHVersionController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-8.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackViewController.h"

@interface ZKHVersionController : ZKHBackViewController

- (IBAction)updateVersion:(id)sender;

@end

//
//  ZKHVersionController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-8.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHVersionController.h"
#import "ZKHConst.h"

@interface ZKHVersionController ()

@end

@implementation ZKHVersionController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)updateVersion:(id)sender {
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:[NSURL URLWithString:KEY_DOWNLOAD_URL]];
}

@end

//
//  ZKHCategoryListController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"

@protocol ZKHCategoryChooseDelegate;

@interface ZKHCategoryChooseController : UITableViewController
{
    NSMutableArray *categories;
}

@property(copy, nonatomic) ZKHCategoryEntity *checkedCategory;
@property(strong, nonatomic) id<ZKHCategoryChooseDelegate> delegate;

@end

@protocol ZKHCategoryChooseDelegate <NSObject>

-(void) choosedCategoryChanged:(ZKHCategoryEntity *) choosedCategory;

@end

@interface ZKHCategoryChooseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tickImgView;
@end
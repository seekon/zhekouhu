//
//  ZKHCategoryListController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHCategoryChooseController.h"
#import "ZKHProcessor+Category.h"
#import "ZKHViewUtils.h"
#import "UITableViewController+CWPopup.h"
#import "UIColor+Utils.h"

static NSString *CellIdentifier = @"CategoryChooseCell";

@implementation ZKHCategoryChooseController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    [[ZKHProcessor getInstance] categories:^(NSMutableArray *result) {
        categories = result;
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHCategoryChooseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZKHCategoryEntity *category = categories[indexPath.row];
    cell.nameLabel.text = category.name;
    
    if ([category isEqual:self.checkedCategory]) {
        cell.tickImgView.hidden = false;
    }else{
        cell.tickImgView.hidden = true;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    self.checkedCategory = categories[row];
    if (self.delegate) {
        [self.delegate choosedCategoryChanged:self.checkedCategory];
    }
    UIViewController *controller = [self parentViewController];
    if (controller) {
        [controller cm_dismissPopupViewControllerAnimated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat sHeight = [ZKHViewUtils screenHeight];
    CGFloat height = sHeight - k_navHeight - 44 * [categories count];
    return height < k_minPopTopHeight ? k_minPopTopHeight : height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *myView = [[UIView alloc] init];
    //myView.backgroundColor = [UIColor colorWithRGB:0 green:0 blue:0 alpha:0.6f];
    myView.backgroundColor = [UIColor clearColor];
    myView.userInteractionEnabled = TRUE;
    [myView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topViewClick:)]];
    
    return myView;
}

- (void)topViewClick:(id)sender
{
    UIViewController *controller = [self parentViewController];
    if (controller) {
        [controller cm_dismissPopupViewControllerAnimated:YES];
    }
}

@end

@implementation ZKHCategoryChooseCell

@end

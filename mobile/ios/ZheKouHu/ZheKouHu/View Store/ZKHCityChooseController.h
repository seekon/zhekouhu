//
//  ZKHCityChooseController.h
//  ZheKouHu
//
//  Created by undyliu on 14-7-14.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"

@protocol ZKHCitySelectedDelegate <NSObject>
- (void) doSelectCity:(ZKHCityEntity *)city;
@end

@interface ZKHCityChooseController : UITableViewController
{
    NSMutableDictionary *cityMap;
    NSArray *keys;
}

@property (strong, nonatomic) ZKHCityEntity *selectedCity;
@property (strong, nonatomic) id<ZKHCitySelectedDelegate> deleage;

@end

@interface ZKHCityChooseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@end

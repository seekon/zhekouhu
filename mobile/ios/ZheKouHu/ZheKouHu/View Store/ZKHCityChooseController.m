//
//  ZKHCityChooseController.m
//  ZheKouHu
//
//  Created by undyliu on 14-7-14.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHCityChooseController.h"
#import "ZKHProcessor+City.h"
#import "UIViewController+CWPopup.h"
#import "UIColor+Utils.h"
#import "ZKHViewUtils.h"

static NSString *CellIdentifier = @"CityChooseCell";

@implementation ZKHCityChooseController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    cityMap = [[NSMutableDictionary alloc] init];
    
    [[ZKHProcessor getInstance] cities:^(NSMutableArray *cities) {
        [self updateData:cities];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void) updateData:(NSMutableArray *)cities
{
    [cityMap removeAllObjects];
    for (ZKHCityEntity *city in cities) {
        NSMutableArray *value;
        NSString *firstLetter = [city firstLetter];
        if ([[cityMap allKeys] containsObject:firstLetter]) {
            value = cityMap[firstLetter];
        }else{
            value = [[NSMutableArray alloc] init];
        }
        [value addObject:city];
        [cityMap setObject:value forKey:firstLetter];
    }
    
    keys = [[cityMap allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        NSComparisonResult result = [obj1 compare:obj2];
        
        return result == NSOrderedDescending; // 升序
        //return result == NSOrderedAscending;  // 降序
    }];
    
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[cityMap allKeys] count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    id key = keys[section - 1];
    NSArray *cities = cityMap[key];
    return [cities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHCityChooseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    id key = keys[indexPath.section - 1];
    ZKHCityEntity *city = cityMap[key][indexPath.row];
    
    cell.nameLabel.text = city.name;
    if (self.selectedCity && [city isEqual:self.selectedCity]) {
        cell.iconView.hidden = false;
    }else{
        cell.iconView.hidden = true;
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* myView = [[UIView alloc] init];
    if (section == 0) {
        myView.backgroundColor = [UIColor clearColor];
        myView.userInteractionEnabled = TRUE;
        [myView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topViewClick:)]];
    }else{
        myView.backgroundColor = [UIColor color_f5f5f5];
    
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 10, 90, 22)];
        titleLabel.textColor = [UIColor color_2d2d2d];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.text = keys[section - 1 ];
        titleLabel.font = [UIFont systemFontOfSize:14];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.5)];
        lineView.backgroundColor = [UIColor color_e3e3e3];
    
        [myView addSubview:titleLabel];
        [myView addSubview:lineView];
    }
    
    return myView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 40;
    if (section == 0) {
        CGFloat sHeight = [ZKHViewUtils screenHeight];
        CGFloat tableViewHeight = [cityMap.allKeys count] * 40;
        for (NSArray *value in [cityMap allValues]) {
            tableViewHeight += [value count] * 44;
        }
        height = sHeight - k_navHeight - tableViewHeight;
        height = height < k_minPopTopHeight ? k_minPopTopHeight : height;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    id key = keys[indexPath.section -1 ];
    self.selectedCity = cityMap[key][indexPath.row];
    if (self.deleage) {
        [self.deleage doSelectCity:self.selectedCity];
    }
    if (self.parentViewController) {
        [self.parentViewController cm_dismissPopupViewControllerAnimated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)topViewClick:(id)sender
{
    UIViewController *controller = [self parentViewController];
    if (controller) {
        [controller cm_dismissPopupViewControllerAnimated:YES];
    }
}
@end

@implementation ZKHCityChooseCell
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
@end

//
//  ZKHNearbyStoresController.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-22.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHBackTableViewController.h"
#import "PullTableView.h"
#import "ZKHEntity.h"

@protocol ZKHNearbyStoreSelectedDelegate <NSObject>
- (void) doSelectNearbyStore:(ZKHNearbyStoreEntity *)nearbyStore;
@end

@interface ZKHNearbyStoresController : ZKHBackTableViewController<UISearchBarDelegate, PullTableViewDelegate>
{
    int offset;
    NSMutableArray *storeList;
    NSString *searchWord;
}
@property (strong, nonatomic) id<ZKHNearbyStoreSelectedDelegate> nearbyStoreSelectedDelegate;
@end

@interface ZKHNearbyStoreCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addrLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@end
//
//  ZKHNearbyStoresController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-22.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHNearbyStoresController.h"
#import "ZKHViewUtils.h"
#import "ZKHSearchBar.h"
#import "ZKHMainActivityCell.h"
#import "ZKHImageLoader.h"
#import "ZKHProcessor+Store.h"
#import "UIColor+Utils.h"

static NSString *NearbyCellIdentifier = @"NearbyStoreCell";

@interface ZKHNearbyStoresController ()

@end

@implementation ZKHNearbyStoresController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataLoaded = true;
    storeList = [[NSMutableArray alloc] init];
    
    ZKHSearchBar *searchBar = [[ZKHSearchBar alloc] initWithFrame:CGRectMake(0, 0, 256, 44)];
    searchBar.delegate = self;
    searchBar.placeholder = @"请输入商家、地址";
    [searchBar setBackgroundImage:[UIImage imageNamed:@"bg_nav"]];
    
    //[searchBar becomeFirstResponder];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 256, 44)];
    [titleView addSubview:searchBar];
    [searchBar setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[searchBar]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(titleView, searchBar)]];
    
    
    [titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[searchBar]|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(titleView, searchBar)]];
    self.navigationItem.titleView = titleView;
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    [self loadData:0];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    searchWord = searchBar.text;
    [self loadData:0];
}

- (void) loadData:(int)offsetVal
{
    [[ZKHProcessor getInstance] nearbyStores:searchWord radius:2000 offset:offsetVal completionHandler:^(NSMutableArray *stores) {
        self.dataLoaded = true;
        if (offsetVal == 0) {
            offset = 0;
            [storeList removeAllObjects];
        }
        if ([stores count] > 0) {
            offset += [stores count];
            [storeList addObjectsFromArray:stores];
        }
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [self loadData:0];
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [self loadData:offset];
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long rows = [storeList count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHNearbyStoreCell *cell = [tableView dequeueReusableCellWithIdentifier:NearbyCellIdentifier forIndexPath:indexPath];
    ZKHNearbyStoreEntity *store = storeList[indexPath.row];
    cell.nameLabel.text = store.name;
    cell.addrLabel.text = store.addr;
    cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", store.distance/1000];
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.nearbyStoreSelectedDelegate) {
        [self.nearbyStoreSelectedDelegate doSelectNearbyStore:storeList[indexPath.row]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

@end

@implementation ZKHNearbyStoreCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.backgroundColor = [UIColor color_f8f8f8];
}

@end
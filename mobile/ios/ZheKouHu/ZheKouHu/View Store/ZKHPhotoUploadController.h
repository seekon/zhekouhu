//
//  ZKHPhotoUploadController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-5.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZKHPhotoChooseDelegate <NSObject>

@optional
- (void) doImageChoosed:(UIImage *)selectedImage;
- (void) doImageCanceled;
@end

@interface ZKHPhotoUploadController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property(nonatomic,strong) id<ZKHPhotoChooseDelegate> delegate;

- (void) openCamera:(id)sender;
- (void) openPhotos:(id)sender;

@end

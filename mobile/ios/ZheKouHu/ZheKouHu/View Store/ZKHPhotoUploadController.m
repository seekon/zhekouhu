//
//  ZKHPhotoUploadController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-5.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ZKHPhotoUploadController.h"
#import "UIViewController+CWPopup.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"
#import "ZKHConst.h"
@implementation ZKHPhotoUploadController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UIView *funcView = [[UIView alloc] initWithFrame:CGRectMake(16, 46, 288, 41)];
    funcView.layer.cornerRadius = 2;
    funcView.layer.masksToBounds = YES;
    funcView.layer.borderWidth = 0.5;
    funcView.layer.borderColor = [[UIColor color_d5d5d5] CGColor];
    funcView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:funcView];
    
    UIView *hView = [[UIView alloc] initWithFrame:CGRectMake(160, 52, 0.5, 31)];
    hView.backgroundColor = [UIColor color_d5d5d5];
    [self.view addSubview:hView];
    
    UIImageView *cameraImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"camera" scale:2]];
    cameraImgView.frame = CGRectMake(56, 57, 16, 16);
    cameraImgView.userInteractionEnabled = TRUE;
    [cameraImgView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openCamera:)]];
    [self.view addSubview:cameraImgView];
    
    UILabel *cameraLabel = [[UILabel alloc] init];
    cameraLabel.text = @"拍照";
    cameraLabel.font = [UIFont systemFontOfSize:12.0];
    cameraLabel.textColor = [UIColor color_7f7f7f];
    cameraLabel.frame = CGRectMake(83, 57, 28, 18);
    cameraLabel.userInteractionEnabled = TRUE;
    [cameraLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openCamera:)]];
    [self.view addSubview:cameraLabel];
    
    UIImageView *photoImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photos" scale:2]];
    photoImgView.frame = CGRectMake(216, 57, 16, 16);
    photoImgView.userInteractionEnabled = TRUE;
    [photoImgView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openPhotos:)]];
    [self.view addSubview:photoImgView];
    
    UILabel *photoLabel = [[UILabel alloc] init];
    photoLabel.text = @"相册 ";
    photoLabel.font = [UIFont systemFontOfSize:12.0];
    photoLabel.textColor = [UIColor color_7f7f7f];
    photoLabel.frame = CGRectMake(243, 57, 28, 18);
    photoLabel.userInteractionEnabled = TRUE;
    [photoLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openPhotos:)]];
    [self.view addSubview:photoLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)topViewTouchDown:(id)sender {
    UIViewController *parent = self.parentViewController;
    if (parent) {
        [parent cm_dismissPopupViewControllerAnimated:YES completion:^{
            
        }];
    }
}

- (void)openCamera:(id)sender
{
    [self doPhotoChoose:UIImagePickerControllerSourceTypeCamera];
}

- (void)openPhotos:(id)sender
{
    [self doPhotoChoose:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
}

- (void) doPhotoChoose:(NSInteger )sourceType
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = sourceType;
    [self presentViewController:picker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(doImageChoosed:)]) {
            [self.delegate doImageChoosed:selectedImage];
        }
    }
    
    if (IOS7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self topViewTouchDown:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    if (IOS7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(doImageCanceled)]) {
            [self.delegate doImageCanceled];
        }
    }
}

@end

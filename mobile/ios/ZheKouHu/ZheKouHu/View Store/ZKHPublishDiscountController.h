//
//  ZKHPublishDiscountController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHTextView.h"
#import "ZKHTextField.h"
#import "ZKHEntity.h"
#import "ZKHCategoryChooseController.h"
#import "ZKHStorefrontChooseController.h"
#import "ZKHImageActionDelegate.h"
#import "ZKHBackTableViewController.h"
#import "ZKHPhotoUploadController.h"
#import "FlatDatePicker.h"

@interface ZKHPublishDiscountController : ZKHBackTableViewController<UICollectionViewDataSource, UICollectionViewDelegate,ZKHCategoryChooseDelegate, ZKHStorefrontChooseDelegate, ZKHImageActionDelegate, ZKHPhotoChooseDelegate, FlatDatePickerDelegate>

{
    NSMutableArray *selectedImages;
    NSMutableArray *deletedImageFiles;
    NSMutableArray *deletedStorefronts;
    
    UICollectionView *imageContainer;
    ZKHTextView *contentTextView;
    ZKHTextField *parValueField;
    
    UILabel *startDateField;
    UILabel *endDateField;
    UILabel *categoryValueLabel;
    UILabel *storefrontLabel;
    
    ZKHCategoryChooseController *cateChooseController;
    ZKHStorefrontChooseController *frontChooseController;
    ZKHPhotoUploadController *photoController;
    
    Boolean showParCell;
    
    FlatDatePicker *flatDatePicker;
    UILabel *currentDateLabel;
    
    UIBarButtonItem *deleteBarItem;
}

@property (strong, nonatomic) UIViewController *controllerOpener;
@property (strong, nonatomic) ZKHDiscountEntity *discount;
@property (copy, nonatomic) NSString *type;

- (void)publish:(id)sender;
- (void)deleteDiscount:(id)sender;

@end


@interface ZKHDiscountContentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ZKHTextView *contentTextView;

@end

@interface ZKHDiscountImagesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *imageContainer;
@property (weak, nonatomic) IBOutlet UIView *discountImgView;

@end

@interface ZKHDiscountParCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ZKHTextField *parField;

@end

@interface ZKHDiscountTermCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *startDateField;

@property (weak, nonatomic) IBOutlet UILabel *endDateField;

@end

@interface ZKHDiscountCateCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cateLabel;

@end

@interface ZKHDiscountFrontTitleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *frontTitleLabel;

@end

@interface ZKHDiscountPubCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *publishBtn;

@end
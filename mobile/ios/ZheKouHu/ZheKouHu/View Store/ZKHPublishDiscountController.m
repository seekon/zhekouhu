//
//  ZKHPublishDiscountController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHPublishDiscountController.h"
#import "ZKHConst.h"
#import "ZKHPictureCell.h"
#import "ZKHViewUtils.h"
#import "ZKHImageListPreviewController.h"
#import "NSString+Utils.h"
#import "NSDate+Utils.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHContext.h"
#import "ZKHImageLoader.h"
#import "UITableViewController+CWPopup.h"
#import "UIImage+Utils.h"
#import "UIView+Toast.h"
#import "UIImage+ImageCompress.h"
#import "UIColor+Utils.h"
#import "SCLAlertView.h"
#import "SCLAlertViewStyleKit.h"

#define kImageWidth 120
#define kImageHeight 120

static NSString *ContentCellIdentifier = @"DiscountContentCell";
static NSString *ImagesCellIdentifier = @"DiscountImagesCell";
static NSString *ParCellIdentifier = @"DiscountParCell";
static NSString *TermCellIdentifier = @"DiscountTermCell";
static NSString *FrontTitleCellIdentifier = @"DiscountFrontTitleCell";
static NSString *CateCellIdentifier = @"DiscountCateCell";
static NSString *PubCellIdentifier = @"DiscountPubCell";

static NSString *CellIdentifier = @"DefaultPictureCell";

@implementation ZKHPublishDiscountController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.title = @"发优惠";
    
    deleteBarItem = [ZKHViewUtils deleteBarButtonItem];
    UIButton *deleteButton = (UIButton *)deleteBarItem.customView;
    [deleteButton addTarget:self action:@selector(deleteDiscount:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *action = nil;
    selectedImages = [[NSMutableArray alloc] init];
    if (!self.discount) {
        self.discount = [[ZKHDiscountEntity alloc] init];
        self.discount.storefronts = [[NSMutableArray alloc] init];
        action = @"发布";
        self.navigationItem.rightBarButtonItem = nil;
    }else{
        self.type = self.discount.type;
        action = @"编辑";
        deletedImageFiles = [[NSMutableArray alloc] init];
        deletedStorefronts = [[NSMutableArray alloc] init];
        
        self.navigationItem.rightBarButtonItem = deleteBarItem;
    }
    
    if (!self.type) {
        self.type = VAL_DISCOUNT_TYPE_COUPON;
    }

    if ([self.type isEqualToString:VAL_DISCOUNT_TYPE_ACTIVITY]) {
        //self.title = [NSString stringWithFormat:@"%@折扣活动", action];
    }else{
        //self.title = [NSString stringWithFormat:@"%@优惠券", action];
        showParCell = true;
    }
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    if (self.controllerOpener) {
        flatDatePicker = [[FlatDatePicker alloc] initWithParentViewController:self];
    }else{
        flatDatePicker = [[FlatDatePicker alloc] initWithParentViewController:self.navigationController];
    }
    
    flatDatePicker.delegate = self;
    //flatDatePicker.title = @"";
    flatDatePicker.datePickerMode = FlatDatePickerModeDate;
    
    [flatDatePicker setMaximumDate:[[NSDate date] dateByAddingDays:365]];
    [flatDatePicker setMinimumDate:[[NSDate date] dateByAddingDays:(-365)]];
    [flatDatePicker setDate:[NSDate date] animated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.controllerOpener) {
        [self updateNavigation];
    }
    [imageContainer reloadData];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateNavigation
{
    UINavigationItem *nav  = nil;
    if ([self.parentViewController isKindOfClass:[UITabBarController class]]) {
        nav = self.parentViewController.navigationItem;
    }else{
        nav = self.navigationItem;
    }
    nav.title = self.tabBarItem.title;
    nav.rightBarButtonItem = nil;
    nav.leftBarButtonItems = nil;
}

#pragma mark - FlatDatePicker Delegate

- (void)flatDatePicker:(FlatDatePicker*)datePicker dateDidChange:(NSDate*)date {
    currentDateLabel.text = [date toyyyyMMddString];
    if ([currentDateLabel isEqual:startDateField]) {
        self.discount.startDate = [NSDate milliSeconds:date];
    }else if([currentDateLabel isEqual:endDateField]){
        self.discount.endDate = [NSDate milliSeconds:date];
    }
}

- (void)startDateFieldClick:(id)sender
{
    [self hideKeyboard];
    currentDateLabel = startDateField;
    flatDatePicker.title = @"选择开始时间";
    if (startDateField.text) {
        [flatDatePicker setDate:[NSDate initWithyyyyMMddString:startDateField.text] animated:NO];
    }
    [flatDatePicker show];
}

- (void)endDateFieldClick:(id)sender
{
    [self hideKeyboard];
    currentDateLabel = endDateField;
    flatDatePicker.title = @"选择结束时间";
    if (endDateField.text) {
        [flatDatePicker setDate:[NSDate initWithyyyyMMddString:endDateField.text] animated:NO];
    }
    [flatDatePicker show];
}

#pragma mark - collectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [selectedImages count] + [self.discount.images count] + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHPictureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    long row = indexPath.row;
    
    long discountImgCount = [self.discount.images count];
    long selectedImgCount = [selectedImages count];
    
    if (row == (discountImgCount + selectedImgCount)) {
        cell.imageView.image = [UIImage imageNamed:@"add_image.png" scale:2];
        cell.imageView.userInteractionEnabled = TRUE;
        [cell.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addPicViewClick:)]];
    }else{
        if (row < discountImgCount) {
            ZKHFileEntity *file = self.discount.images[row];
            [ZKHImageLoader showImageForName:file.aliasName imageView:cell.imageView toScale:2];
        }else{
            long index = row - discountImgCount;
            cell.imageView.image = [ZKHImageLoader scaleImage:selectedImages[index] toScale:CGSizeMake(kImageWidth, kImageHeight)];
        }
        
        cell.imageView.tag = row;
        cell.imageView.userInteractionEnabled = TRUE;
        [cell.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(discountImageClick:)]];
    }
    
    return cell;
}

- (void) discountImageClick:(UITapGestureRecognizer *)sender
{
    long row = sender.view.tag;
    
    long imagesCount = [self.discount.images count] + [selectedImages count];
    if (row < imagesCount) {
        ZKHImageListPreviewController *controller = [[ZKHImageListPreviewController alloc] init];
        
        NSMutableArray *imageFiles = [[NSMutableArray alloc] init];
        if (self.discount.images) {
            [imageFiles addObjectsFromArray:self.discount.images];
        }
        if (selectedImages) {
            [imageFiles addObjectsFromArray:selectedImages];
        }
        
        controller.currentIndex = row;
        controller.imageFiles = imageFiles;
        controller.readonly = false;
        controller.imageActionDelegate = self;
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }

}

- (void)imageContainerClick:(id)sender
{
    [self hideKeyboard];
}

#pragma mark - ZKHImageAction delegate
- (void) doDeleteImage:(id)image index:(int)index
{
    long discountImgCount = [self.discount.images count];
    if (index < discountImgCount) {
        [self.discount.images removeObject:image];
        [deletedImageFiles addObject:image];
    }else{
        [selectedImages removeObject:image];
    }
    
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
}

- (void)doImageCanceled
{
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
}

- (void)addPicViewClick:(UITapGestureRecognizer *)sender
{
    [self hideKeyboard];
    photoController = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHPhotoUploadController"];
    photoController.view.frame = CGRectMake(0, 0, 320, 113);
    photoController.delegate = self;
    [self.navigationController cm_presentPopupViewController:photoController animated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return showParCell ? 7 : 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    if (row > 1 && !showParCell) {
        row += 1;
    }
    switch (row) {
        case 1:
        {
            ZKHDiscountContentCell *cell = [tableView dequeueReusableCellWithIdentifier:ContentCellIdentifier forIndexPath:indexPath];
            contentTextView = cell.contentTextView;
            contentTextView.placeholder = @"请填写活动内容";
            contentTextView.text = self.discount.content;
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contentDidEndEditing:) name:UITextViewTextDidEndEditingNotification object:nil];
            
            return cell;
        }
        case 0:
        {
            ZKHDiscountImagesCell *cell = [tableView dequeueReusableCellWithIdentifier:ImagesCellIdentifier forIndexPath:indexPath];
            imageContainer = cell.imageContainer;
            imageContainer.dataSource = self;
            imageContainer.delegate = self;
            
            imageContainer.userInteractionEnabled = TRUE;
            UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageContainerClick:)];
            
            [imageContainer addGestureRecognizer:gr];
            
            cell.discountImgView.layer.borderWidth = 0.5;
            cell.discountImgView.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
            
            return cell;
        }
        case 2:
        {
            ZKHDiscountParCell *cell = [tableView dequeueReusableCellWithIdentifier:ParCellIdentifier forIndexPath:indexPath];
            parValueField = cell.parField;
            cell.parField.text = self.discount.parValue;
            
            [cell.parField addTarget:self action:@selector(parValueDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
            return cell;
        }
        case 3:
        {
            ZKHDiscountTermCell *cell = [tableView dequeueReusableCellWithIdentifier:TermCellIdentifier forIndexPath:indexPath];
            startDateField = cell.startDateField;
            if (self.discount.startDate) {
                startDateField.text = [[NSDate dateWithMilliSeconds:[self.discount.startDate longLongValue]] toyyyyMMddString];
            }else{
                startDateField.text = @"";
            }
            startDateField.userInteractionEnabled = TRUE;
            [startDateField addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(startDateFieldClick:)]];
            
            endDateField = cell.endDateField;
            if (self.discount.endDate) {
                endDateField.text = [[NSDate dateWithMilliSeconds:[self.discount.endDate longLongValue]] toyyyyMMddString];
            }else{
                endDateField.text = @"";
            }
            endDateField.userInteractionEnabled = TRUE;
            [endDateField addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endDateFieldClick:)]];
            
            [ZKHViewUtils drawSeparaterAtFooter:cell offsetX:18];
            
            return cell;
        }
        case 4:
        {
            ZKHDiscountCateCell *cell = [tableView dequeueReusableCellWithIdentifier:CateCellIdentifier forIndexPath:indexPath];
            categoryValueLabel = cell.cateLabel;
            categoryValueLabel.text = self.discount.category.name;
            
            [ZKHViewUtils drawSeparaterAtFooter:cell offsetX:18];
            return cell;
        }
        case 5:
        {
            ZKHDiscountFrontTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:FrontTitleCellIdentifier forIndexPath:indexPath];
            storefrontLabel = cell.frontTitleLabel;
            if ([self.discount.storefronts count] > 0) {
                storefrontLabel.text = [NSString stringWithFormat:@"使用门店(%d家)", [self.discount.storefronts count]];
            }else{
                storefrontLabel.text = @"添加门店";
            }
            [ZKHViewUtils drawSeparaterAtFooter:cell offsetX:18];
            
            return cell;
        }
        case 6:
        {
            ZKHDiscountPubCell *cell = [tableView dequeueReusableCellWithIdentifier:PubCellIdentifier forIndexPath:indexPath];
            [cell.publishBtn addTarget:self action:@selector(publish:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        default:
            break;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 45;
    long row = indexPath.row;
    if (showParCell) {
        switch (row) {
            case 0:
                height = 129;
                break;
            case 1:
                height = 76;
                break;
            case 2:
                height = 40;
                break;
            case 6:
                height = 87;
                break;
            default:
                break;
        }
    }else{
        switch (row) {
            case 0:
                height = 129;
                break;
            case 1:
                height = 76;
                break;
            case 5:
                height = 87;
                break;
            default:
                break;
        }
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideKeyboard];
    long row = indexPath.row;
    if (!showParCell) {
        row += 1;
    }
    if (row == 4) {
        cateChooseController = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHCategoryChooseController"];
        cateChooseController.checkedCategory = self.discount.category;
        cateChooseController.delegate = self;
        [self.navigationController cm_presentPopupViewController:cateChooseController animated:YES];
    }else if(row == 5){
        frontChooseController = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHStorefrontChooseController"];
        frontChooseController.selectedStorefronts = self.discount.storefronts;
        frontChooseController.delegate = self;
        [self.navigationController cm_presentPopupViewController:frontChooseController animated:YES];
    }
}

#pragma mark - choosedCategoryChanged
- (void)choosedCategoryChanged:(ZKHCategoryEntity *)choosedCategory
{
    self.discount.category = choosedCategory;
    [self.tableView reloadData];
}

#pragma mark - choosedStorefrontsChanged
- (void)choosedStorefrontsChanged:(id)storefront choosed:(Boolean)choosed
{
    if (choosed) {
        [self.discount.storefronts addObject:storefront];
        [deletedStorefronts removeObject:storefront];
    }else{
        [self.discount.storefronts removeObject:storefront];
        [deletedStorefronts addObject:storefront];
    }
    
[self.tableView reloadData];
}

#pragma mark - photoChoose
- (void) doImageChoosed:(UIImage *)_selectedImage
{
    if (IOS7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    if (!selectedImages) {
        selectedImages = [[NSMutableArray alloc] init];
    }
    [selectedImages addObject:_selectedImage];
    [imageContainer reloadData];
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([dest isKindOfClass:[ZKHCategoryChooseController class]]) {
        ZKHCategoryChooseController *controller = (ZKHCategoryChooseController *)dest;
        controller.delegate = self;
        controller.checkedCategory = self.discount.category;
    }else if ([dest isKindOfClass:[ZKHStorefrontChooseController class]]){
        ZKHStorefrontChooseController *controller = (ZKHStorefrontChooseController *)dest;
        controller.delegate = self;
        controller.selectedStorefronts = self.discount.storefronts;
    }
}

- (void) hideKeyboard
{
    [contentTextView resignFirstResponder];
    [parValueField resignFirstResponder];
}

- (void)deleteDiscount:(id)sender
{
    NSString *subTitle = nil;
    if ([self.type isEqualToString:VAL_DISCOUNT_TYPE_ACTIVITY]) {
        subTitle = @"是否删除活动?";
    }else{
        subTitle = @"是否删除优惠券?";
    }

    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"确定" actionBlock:^(void) {
        if (!self.discount.uuid) {
            return;
        }
        
        [[ZKHProcessor getInstance] deleteDiscount:self.discount completionHandler:^(Boolean result) {
            if (result) {
                [self.view makeToast:@"删除成功."];
                if (self.controllerOpener && [self.controllerOpener isKindOfClass:[UIViewController class]]) {
                    [self.navigationController popToViewController:self.controllerOpener animated:YES];
                }
            }else{
                [self.view makeToast:@"删除失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }];
    [alert showCustom:self.navigationController image:SCLAlertViewStyleKit.imageOfNotice color:[UIColor color_ff6722] title:@"提示" subTitle:subTitle closeButtonTitle:@"取消" duration:0.0f];
}

- (void)publish:(id)sender {
    [contentTextView endEditing:true];
    
    //Boolean cancel = false;
    
    NSString *content = contentTextView.text;
    if ([NSString isNull:content] || content.length > 140) {
        [contentTextView showTipView:@"活动内容必须140字以内."];
        return;
        //cancel = true;
    }
    
    long imagesCount = [self.discount.images count] + [selectedImages count];
    if (imagesCount == 0) {
        [ZKHViewUtils showTipView:imageContainer message:@"请上传活动图片" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
        //cancel = true;
    }
//    if ([self.type isEqualToString: VAL_DISCOUNT_TYPE_COUPON]
//        && [NSString isNull:parValueField.text]) {
//        [parValueField showErrorMessage:@"面值不能为空."];
//        return;
//    }
    
    NSString *startDate = startDateField.text;
    NSString *endDate = endDateField.text;
    if ([NSString isNull:startDate]) {
        [ZKHViewUtils showTipView:startDateField inView:self.tableView message:@"请选择开始时间" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
        //cancel = true;
    }
    if ([NSString isNull:endDate]) {
        [ZKHViewUtils showTipView:endDateField inView:self.tableView message:@"请选择结束时间" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
        //cancel = true;
    }
    NSComparisonResult dateCompareResult = [[NSDate initWithyyyyMMddString:startDate] compare:[NSDate initWithyyyyMMddString:endDate]];
    if (dateCompareResult == NSOrderedDescending) {
        [ZKHViewUtils showTipView:endDateField inView:self.tableView message:@"结束时间应该在开始时间之后" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
    }
    
    if (!self.discount.category) {
        [ZKHViewUtils showTipView:categoryValueLabel inView:self.tableView message:@"请选择分类" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
        //cancel = true;
    }
    
    if (!self.discount.storefronts || [self.discount.storefronts count] == 0) {
        [ZKHViewUtils showTipView:storefrontLabel inView:self.tableView message:@"至少添加一家门店" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
        //cancel = true;
    }
    
    self.discount.content = content;
//    if ([self.type isEqualToString: VAL_DISCOUNT_TYPE_COUPON]){
//        self.discount.parValue = parValueField.text;
//    }
//    if (!self.discount.parValue) {
//        self.discount.parValue = @"0";
//    }
    if ([NSString isNull:parValueField.text]) {
        self.discount.parValue = @"0";
        self.discount.type = VAL_DISCOUNT_TYPE_ACTIVITY;
    }else{
        self.discount.parValue = parValueField.text;
        self.discount.type = VAL_DISCOUNT_TYPE_COUPON;
    }
    
    self.discount.startDate = [NSDate milliSeconds:[NSDate initWithyyyyMMddString:startDate]];
    self.discount.endDate = [NSDate milliSeconds:[NSDate initWithyyyyMMddString:endDate]];
    self.discount.publisher = [ZKHContext getInstance].user.uuid;
    
    if (self.discount.uuid) {//编辑发布
        NSMutableArray *addedImages = [self getImageFiles];
        
        [[ZKHProcessor getInstance] updateDiscount:self.discount addImages:addedImages deletedImages:deletedImageFiles deletedStorfronts:deletedStorefronts completionHandler:^(Boolean result) {
            if (result) {
                [self.view makeToast:@"发布成功."];
                if ([addedImages count] > 0) {
                    [self.discount.images addObjectsFromArray:addedImages];
                }
                //[self.navigationController popViewControllerAnimated:YES];
            }else{
                [self.view makeToast:@"发布失败."];
                [self deleteImageFiles:addedImages];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];

    }else{//新增发布
        self.discount.images = [self getImageFiles];
        self.discount.origin = VAL_DISCOUNT_ORIGIN_STORER;
        self.discount.location = @"";
        self.discount.auditIdea = @"";
        [[ZKHProcessor getInstance] publishDiscount:self.discount completionHandler:^(Boolean result) {
            if (result) {
                [deletedImageFiles removeAllObjects];
                [deletedStorefronts removeAllObjects];
                
                [self.view makeToast:@"发布成功."];
                if (!self.controllerOpener) {//清空数据
                    contentTextView.text = @"";
                    parValueField.text = @"";
                    startDateField.text = @"";
                    endDateField.text = @"";
                    categoryValueLabel.text = @"";
                    selectedImages = [[NSMutableArray alloc]init];
                    self.discount = [[ZKHDiscountEntity alloc] init];
                    [imageContainer reloadData];
                    [self.tableView reloadData];
                }
            }else{
                [self.view makeToast:@"发布失败."];
            
                [self deleteImageFiles:self.discount.images];
                self.discount.images = nil;
            }
        } errorHandler:^(ZKHErrorEntity *error) {
        
        }];
    }
}

- (NSMutableArray *) getImageFiles
{
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    NSMutableArray *files = [[NSMutableArray alloc] init];
    
    for (UIImage *image in selectedImages) {
        NSString *aliasName = [NSString stringWithFormat:@"%@_discount_%@.png", user.code, [NSDate currentTimeString]];
        NSString *filePath = [ZKHImageLoader saveImageData:[UIImage compressImageData:image compressRatio:0.9f] fileName:aliasName];
        
        ZKHFileEntity *file = [[ZKHFileEntity alloc] init];
        file.aliasName = aliasName;
        file.fileUrl = filePath;
        
        [files addObject:file];
    }
    return files;
}

- (void) deleteImageFiles:(NSMutableArray *)files
{
    for (ZKHFileEntity *file in files) {
        [ZKHImageLoader removeImageWithName:file.aliasName];
    }
}

- (void)back:(id)sender
{
    NSArray *childControllers = self.childViewControllers;
    if ([childControllers containsObject:photoController]
        || [childControllers containsObject:cateChooseController]
        || [childControllers containsObject:frontChooseController]
        ) {
        [self cm_dismissPopupViewControllerAnimated:YES];
    }else if(flatDatePicker.isOpen){
        [flatDatePicker dismiss];
    }else{
        [self.discount.images addObjectsFromArray:deletedImageFiles];
        [self.discount.storefronts addObjectsFromArray:deletedStorefronts];
        [super back:sender];
    }
}

- (void)contentDidEndEditing:(NSNotification *)notification
{
    self.discount.content = contentTextView.text;
}

- (void)parValueDidEndEditing:(id)sender
{
    self.discount.parValue = parValueField.text;
}

@end

@implementation ZKHDiscountContentCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect frame = self.contentView.frame;
    frame.origin.x = 17;
    frame.origin.y = 19;
    frame.size.width = 286;
    frame.size.height = 61;
    self.contentView.frame = frame;
}

@end

@implementation ZKHDiscountImagesCell

@end

@implementation ZKHDiscountParCell

@end

@implementation ZKHDiscountTermCell

@end

@implementation ZKHDiscountCateCell

@end

@implementation ZKHDiscountFrontTitleCell

@end

@implementation ZKHDiscountPubCell

@end
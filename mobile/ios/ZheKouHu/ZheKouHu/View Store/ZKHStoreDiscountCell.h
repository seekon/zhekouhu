//
//  ZKHStoreDiscountCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-25.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"

@interface ZKHStoreDiscountCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *parValLabel;

@property (weak, nonatomic) IBOutlet UIImageView *saleImageView;
@property (strong, nonatomic) IBOutlet UILabel *contentLabel;
@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) UIImageView *countImageView;

- (void) updateViews:(ZKHDiscountEntity *)discount;

@end

//
//  ZKHStoreDiscountCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-25.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStoreDiscountCell.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"
#import "ZKHConst.h"

@implementation ZKHStoreDiscountCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.countImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"man_time" scale:2]];
    [self addSubview:self.countImageView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    self.backgroundColor = [UIColor color_f8f8f8];
}

- (void)updateViews:(ZKHDiscountEntity *)discount
{
    NSString *countText = self.countLabel.text;
    CGRect countFrame = self.countLabel.frame;
    CGFloat countX = countFrame.origin.x + countFrame.size.width - (countText.length + 1) *5;
    NSString *status = discount.status;
    if ([status isEqualToString:VAL_DISCOUNT_STATUS_VALID]) {
        self.countImageView.frame = CGRectMake(countX - 14, countFrame.origin.y, 12, 12);
    }else{
        self.countImageView.hidden = true;
    }
}
@end

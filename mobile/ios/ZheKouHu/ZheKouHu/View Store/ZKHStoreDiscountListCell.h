//
//  ZKHStoreDiscountCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-25.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHStoreDiscountListCell : UITableViewCell<UITableViewDataSource, UITableViewDelegate>
{
    UINib *discountSubItemNib;
    UITableView *itemTableView;
}

@property (weak, nonatomic) IBOutlet UILabel *publishDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (strong, nonatomic) NSMutableArray *discountList;
@property (strong, nonatomic) UIViewController *parentController;

+(CGFloat) cellHeight:(NSMutableArray *)discountList;

@end

//
//  ZKHStoreDiscountCell.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-25.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStoreDiscountListCell.h"
#import "ZKHEntity.h"
#import "ZKHStoreDiscountCell.h"
#import "ZKHImageLoader.h"
#import "ZKHPublishDiscountController.h"
#import "ZKHViewUtils.h"
#import "UIColor+Utils.h"
#import "ZKHConst.h"
#import "ZKHUserPubDiscountController.h"
#import "NSString+Utils.h"

#define kTableViewOffsetX 85

static NSString *SubCellIdentifier = @"StoreDiscountCell";

@implementation ZKHStoreDiscountListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect frame = self.publishDateLabel.frame;
    frame.origin.x = 10;
    frame.origin.y = 5;
    self.publishDateLabel.frame = frame;
    
    CGRect countFrame = self.countLabel.frame;
    countFrame.origin.y = frame.origin.y + frame.size.height ;
    countFrame.origin.x = frame.origin.x;
    countFrame.size.width = frame.size.width;
    self.countLabel.frame = countFrame;
}

+ (CGFloat)cellHeight:(NSMutableArray *)discountList
{
    CGFloat cellHeight = 85 * [discountList count];
    return cellHeight;
}

+ (CGFloat) cellRightWidth
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
        return screenBounds.size.width - kTableViewOffsetX;
    }else{
        return screenBounds.size.height - kTableViewOffsetX;
    }
}

- (void)setDiscountList:(NSMutableArray *)discountList
{
    if (!discountSubItemNib) {
        discountSubItemNib = [UINib nibWithNibName:@"ZKHStoreDiscountCell" bundle:nil];
    }
    
    _discountList = discountList;
    
    CGFloat width = [ZKHStoreDiscountListCell cellRightWidth];
    CGRect frame  = CGRectMake(kTableViewOffsetX, 0, width, [ZKHStoreDiscountListCell cellHeight:discountList]);
    if (itemTableView) {
        itemTableView.frame = frame;
    }else{
        itemTableView = [[UITableView alloc] init];
        [itemTableView registerNib:discountSubItemNib forCellReuseIdentifier:SubCellIdentifier];
        
        [self addSubview:itemTableView];
        itemTableView.delegate = self;
        itemTableView.dataSource = self;
        itemTableView.scrollEnabled = NO;
        
        if ([itemTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [itemTableView setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    itemTableView.frame = frame;
    [itemTableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.discountList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHStoreDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:SubCellIdentifier forIndexPath:indexPath];
    
    ZKHDiscountEntity *discount = self.discountList[indexPath.row];
    ZKHFileEntity *imageFile = discount.images[0];
    [ZKHImageLoader showImageForName:imageFile.aliasName imageView:cell.saleImageView toScale:3];
    
    if ([NSString isNull:discount.content]) {
        cell.contentLabel.text = @"无内容";
    }else{
        cell.contentLabel.text = discount.content;
    }
    NSString *status = discount.status;
    if ([status isEqualToString:VAL_DISCOUNT_STATUS_VALID]) {
        cell.countLabel.text = [NSString stringWithFormat:@"%d次", discount.visitCount];
    }else if([status isEqualToString:VAL_DISCOUNT_STATUS_INVALID]){
        cell.countLabel.text = @"无效";
    }else if ([status isEqualToString:VAL_DISCOUNT_STATUS_CHECHING]){
        cell.countLabel.text = @"核对中";
    }else{
        cell.countLabel.text = @"";
    }
    
    if ([discount.type isEqualToString:VAL_DISCOUNT_TYPE_COUPON]) {
        cell.parValLabel.text = [NSString stringWithFormat:@"￥%@", discount.parValue];
    }else{
        cell.parValLabel.text = @"";
    }
    
    [cell updateViews:discount];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHDiscountEntity *discount = self.discountList[indexPath.row];
    if ([discount.origin isEqualToString:VAL_DISCOUNT_ORIGIN_CUSTOMER]) {
        ZKHUserPubDiscountController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHUserPubDiscountController"];
        controller.controllerOpener = self.parentController;
        controller.discount = discount;
        [self.parentController.navigationController pushViewController:controller animated:YES];
    }else{
        ZKHPublishDiscountController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHPublishDiscountController"];
        controller.controllerOpener = self.parentController;
        controller.discount = discount;
        [self.parentController.navigationController pushViewController:controller animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

@end

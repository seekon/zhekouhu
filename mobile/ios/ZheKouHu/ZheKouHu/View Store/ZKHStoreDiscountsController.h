//
//  ZKHStoreDiscountsController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullTableView.h"
#import "ZKHBackTableViewController.h"

@interface ZKHStoreDiscountsController : ZKHBackTableViewController<PullTableViewDelegate>
{
    NSMutableArray *discountCountList;
    int offset;
}

@property(copy, nonatomic) NSString *type;

-(void) publishDiscount:(id)sender;

@end

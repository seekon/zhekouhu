//
//  ZKHStoreDiscountsController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStoreDiscountsController.h"
#import "ZKHConst.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHContext.h"
#import "ZKHStoreDiscountListCell.h"
#import "NSDate+Utils.h"
#import "ZKHViewUtils.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHPublishDiscountController.h"
#import "ZKHUserPubDiscountController.h"
#import "UIView+Toast.h"

static NSString *CellIdentifier = @"StoreDiscountListCell";

@implementation ZKHStoreDiscountsController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIButton *publishButton = [[UIButton alloc] init];
    [publishButton setImage:[UIImage imageNamed:@"publish" scale:2] forState:UIControlStateNormal];
    publishButton.frame = CGRectMake(0, 0, 40, 40);
    [publishButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:publishButton.frame.size] forState:UIControlStateHighlighted];
    [publishButton addTarget:self action:@selector(publishDiscount:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:publishButton];
    
    offset = 0;
    discountCountList = [[NSMutableArray alloc] init];
    
    if (!self.type) {
        self.type = VAL_DISCOUNT_TYPE_ACTIVITY;
    }
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    [self.tableView setBackgroundColor:[UIColor color_f5f5f5]];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadData:0];
}

- (void)publishDiscount:(id)sender
{
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (!user || [user.type isEqualToString:VAL_USER_TYPE_CUSTOMER]) {
        ZKHUserPubDiscountController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHUserPubDiscountController"];
        controller.controllerOpener = self;
        [self.navigationController pushViewController:controller animated:YES];
    }else{
        ZKHPublishDiscountController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHPublishDiscountController"];
        controller.controllerOpener = self;
        controller.type = self.type;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [self loadData:offset];
    
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

- (void) loadData:(int) offsetVal
{
    NSString *owner = nil;
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        owner = user.uuid;
    }else{
        owner = [ZKHViewUtils deviceId];
    }
    if (!owner) {
        self.dataLoaded = true;
        [self.tableView reloadData];
        return;
    }
    
    [[ZKHProcessor getInstance] discountsGroupByPublishDate:owner offset:offsetVal completionHandler:^(NSMutableArray *discounts) {
        self.dataLoaded = true;
        if (offsetVal == 0) {
            offset = 0;
            [discountCountList removeAllObjects];
        }
        if (discounts) {
            [discountCountList addObjectsFromArray:discounts];
            offset += [discounts count];
            [self.tableView reloadData];
        }else{
            [self.view makeToast:@"加载优惠数据失败."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long rows = [discountCountList count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHStoreDiscountListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZKHDateIndexedEntity *entity = discountCountList[indexPath.row];
    
    NSString *dateStr = [entity.date toddMMString];
    NSRange dayRange = [dateStr rangeOfString:@"日"];
    dayRange = NSMakeRange(0, dayRange.location);
    NSMutableAttributedString *richDateStr = [[NSMutableAttributedString alloc] initWithString:dateStr];
    NSDictionary *dayAttr = @{NSFontAttributeName: [UIFont systemFontOfSize:26]};
    [richDateStr setAttributes:dayAttr range:dayRange];
    
    cell.publishDateLabel.attributedText = richDateStr;
    
    cell.countLabel.text = [NSString stringWithFormat:@"%d笔", entity.count];
    
    cell.parentController = self;
    cell.discountList = entity.items;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZKHDateIndexedEntity *entity = discountCountList[indexPath.row];
    CGFloat height = [ZKHStoreDiscountListCell cellHeight:entity.items];
    return height;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
}

@end

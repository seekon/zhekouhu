//
//  ZKHEditStoreController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"
#import "ZKHBackTableViewController.h"
#import "ZKHPhotoUploadController.h"
#import "ZKHTextField.h"

@interface ZKHStoreEditController : ZKHBackTableViewController<ZKHPhotoChooseDelegate>
{
    ZKHTextField *nameField;
    UIImageView *photoView;
    Boolean logoChanged;
    UIImage *selectedImage;
    
    UIBarButtonItem *deleteBarItem;
    ZKHPhotoUploadController *photoUploadController;
    
    UIImageView *addFrontImageView;
}

@property (strong, nonatomic) ZKHStoreEntity *store;
- (void)saveStore:(id)sender;
- (void)nameChanged:(id)sender;
- (void)deleteStore:(id)sender;

@end

@interface ZKHStoreImageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end

@interface ZKHStoreNameCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ZKHTextField *nameField;

@end

@interface ZKHStorefrontTitleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *addFrontImageView;

@end

@interface ZKHStorefrontCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

@interface ZKHStoreSaveCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

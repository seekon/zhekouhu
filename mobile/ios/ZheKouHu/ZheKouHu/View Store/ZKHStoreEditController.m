//
//  ZKHEditStoreController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStoreEditController.h"
#import "ZKHViewUtils.h"
#import "ZKHStorefrontEditController.h"
#import "NSString+Utils.h"
#import "NSDate+Utils.h"
#import "ZKHProcessor+Store.h"
#import "ZKHImageLoader.h"
#import "ZKHContext.h"
#import "UIImage+Utils.h"
#import "UITableViewController+CWPopup.h"
#import "UIColor+Utils.h"
#import "UIView+Toast.h"
#import "UIImage+ImageCompress.h"
#import "ZKHConst.h"
#import "SCLAlertViewStyleKit.h"
#import "SCLAlertView.h"

#define kOffsetRowCount 4

static NSString *StoreNameCellIdentifier = @"StoreNameCell";
static NSString *StoreImageCellIdentifier = @"StoreImageCell";
static NSString *StorefrontCellIdentifier = @"StorefrontCell";
static NSString *StorefrontTitleCellIdentifier = @"StorefrontTitleCell";
static NSString *StoreSaveCellIdentifier = @"StoreSaveCell";

@implementation ZKHStoreEditController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    deleteBarItem = [ZKHViewUtils deleteBarButtonItem];
    UIButton *deleteButton = (UIButton *)deleteBarItem.customView;
    [deleteButton addTarget:self action:@selector(deleteStore:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.store) {
        self.title = self.store.name;
        self.navigationItem.rightBarButtonItem = deleteBarItem;
    }else{
        self.title = @"新开店铺";
    }
    
    if (!self.store) {
        self.store = [[ZKHStoreEntity alloc] init];
        self.store.storefronts = [[NSMutableArray alloc] init];
    }
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateViews];
}

- (void) updateViews
{
    [self.tableView reloadData];
}

- (void)saveStore:(id)sender {
    [nameField endEditing:YES];
    
    NSString *name = nameField.text;
    
    if (!selectedImage && !self.store.logo) {
        [ZKHViewUtils showTipView:photoView message:@"请上传商标LOGO" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
    }
    
    [nameField validate];
    if (!nameField.isValid) {
        return;
    }
    
    if ([self.store.storefronts count] == 0) {
        [ZKHViewUtils showTipView:addFrontImageView inView:self.tableView message:@"请添加门店" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
    }
    
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    
    self.store.owner = user.uuid;
    self.store.name = name;
    if (logoChanged) {
        NSString *aliasName = [NSString stringWithFormat:@"%@_store_%@.png", user.code, [NSDate currentTimeString]];
        NSString *filePath = [ZKHImageLoader saveImageData:[UIImage compressImageData:selectedImage compressRatio:0.9f] fileName:aliasName];
        
        ZKHFileEntity *file = [[ZKHFileEntity alloc] init];
        file.aliasName = aliasName;
        file.fileUrl = filePath;
        
        self.store.logo = file;
    }
    
    if (self.store.uuid) {
        [[ZKHProcessor getInstance] updateStore:self.store completionHandler:^(Boolean result) {
            if (result) {
                [self.view makeToast:@"保存成功."];
                [self updateViews];
            }else{
                [self.view makeToast:@"保存失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }else{
        [[ZKHProcessor getInstance] registerStore:self.store completionHandler:^(Boolean result) {
            if (result) {
                [self.view makeToast:@"添加成功."];
                [self updateViews];
            }else{
                [self.view makeToast:@"添加成功."];
                [ZKHImageLoader removeImageWithName:self.store.logo.aliasName];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }
}

- (void)nameChanged:(id)sender {
    self.store.name = nameField.text;
}

- (void)deleteStore:(id)sender {
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"确定" actionBlock:^(void) {
        if (self.store.uuid) {
            
            [[ZKHProcessor getInstance] deleteStore:self.store completionHandler:^(Boolean result) {
                [self.view makeToast:@"删除成功."];
               [self.navigationController popToRootViewControllerAnimated:YES];
            } errorHandler:^(ZKHErrorEntity *error) {
                [self.view makeToast:@"删除失败."];
            }];
        }
    }];
    [alert showCustom:self.navigationController image:SCLAlertViewStyleKit.imageOfNotice color:[UIColor color_ff6722] title:@"提示" subTitle:@"是否删除店铺?" closeButtonTitle:@"取消" duration:0.0f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kOffsetRowCount + [self.store.storefronts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    switch (row) {
            case 0:
            {
                ZKHStoreImageCell *cell = [tableView dequeueReusableCellWithIdentifier:StoreImageCellIdentifier forIndexPath:indexPath];
                cell.photoView.userInteractionEnabled = TRUE;
                [cell.photoView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(photoViewClick:)]];
                if (!photoView.image && self.store && self.store.logo) {
                    [ZKHImageLoader showImageForName:self.store.logo.aliasName imageView:cell.photoView toScale:3];
                }
                photoView = cell.photoView;
                return cell;
            }
            case 1:
            {
                ZKHStoreNameCell *cell = [tableView dequeueReusableCellWithIdentifier:StoreNameCellIdentifier forIndexPath:indexPath];
                cell.nameField.text = self.store.name;
                nameField = cell.nameField;
                [nameField addTarget:self action:@selector(nameChanged:) forControlEvents:UIControlEventEditingChanged];
                
                nameField.maxNumberOfCharactersToStartValidation = 100;
                nameField.regexpPattern = @"*";
                nameField.invalidPopText = @"名称必须100字以内.";
                
                return cell;
            }
            case 2:
            {
                ZKHStorefrontTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:StorefrontTitleCellIdentifier forIndexPath:indexPath];
                if ([self.store.storefronts count] > 0) {
                    cell.titleLabel.text = [NSString stringWithFormat:@"%@(%ld家)", @"门店", (unsigned long)[self.store.storefronts count]];
                }else{
                    cell.titleLabel.text = @"添加门店";
                }
                
                addFrontImageView = cell.addFrontImageView;
                [ZKHViewUtils drawSeparaterAtFooter:cell offsetX:18];
                
                cell.tag = -1;
                return cell;
            }
            default:
                break;
        }
    
    if (row == kOffsetRowCount + [self.store.storefronts count] - 1) {
        ZKHStoreSaveCell *cell = [tableView dequeueReusableCellWithIdentifier:StoreSaveCellIdentifier forIndexPath:indexPath];
        
        [cell.saveButton setBackgroundImage:[UIImage imageNamed:@"bg_button_sel" scale:2] forState:UIControlStateHighlighted];
        [cell.saveButton addTarget:self action:@selector(saveStore:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }else{
        ZKHStorefrontEntity *front = self.store.storefronts[row - kOffsetRowCount + 1];
        ZKHStorefrontCell *cell = [tableView dequeueReusableCellWithIdentifier:StorefrontCellIdentifier forIndexPath:indexPath];
        cell.nameLabel.text = front.name;
        cell.tag = row - kOffsetRowCount + 1;
        [ZKHViewUtils drawSeparaterAtFooter:cell offsetX:18];
        return cell;
    }
   }


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    return row == 0 ? 117 : row == kOffsetRowCount + [self.store.storefronts count] - 1 ? 87 : 45;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row != 1) {
        [nameField resignFirstResponder];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
}

- (void)addStorefrontClick:(id)sender
{
    ZKHStorefrontEditController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHStorefrontEditController"];
    controller.store = self.store;
    controller.currentStorefrontIndex = -1;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)photoViewClick:(UITapGestureRecognizer *)sender
{
    //if (!photoUploadController) {
        photoUploadController = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHPhotoUploadController"];
        photoUploadController.delegate = self;
    //}
    photoUploadController.view.frame = CGRectMake(0, 0, 320, 113);
    [self.navigationController cm_presentPopupViewController:photoUploadController animated:YES];
}

- (void) doImageChoosed:(UIImage *)_selectedImage
{
    if (IOS7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    selectedImage = _selectedImage;
    photoView.image = [ZKHImageLoader scaleImage:selectedImage toScale:CGSizeMake(photoView.frame.size.width * 2, photoView.frame.size.height * 2)];
    logoChanged = true;
    
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
}

- (void)doImageCanceled
{
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([dest isKindOfClass:[ZKHStorefrontEditController class]]) {
        ZKHStorefrontEditController *controller = (ZKHStorefrontEditController *)dest;
        controller.store = self.store;
        controller.controllerOpener = self;
        controller.currentStorefrontIndex = ((UITableViewCell *)sender).tag;
    }
}

- (void)back:(id)sender
{
    if ([self.childViewControllers containsObject:photoUploadController]) {
        [self cm_dismissPopupViewControllerAnimated:YES];
    }else{
        [super back:sender];
    }
}
@end

@implementation ZKHStoreImageCell

@end

@implementation ZKHStoreNameCell

@end

@implementation ZKHStorefrontTitleCell

@end

@implementation ZKHStorefrontCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    self.backgroundColor = [UIColor color_f8f8f8];
}

@end

@implementation ZKHStoreSaveCell

@end
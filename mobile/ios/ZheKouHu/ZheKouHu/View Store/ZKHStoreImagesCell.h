//
//  ZKHStoreImagesCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHPageControl.h"

@interface ZKHStoreImagesCell : UITableViewCell<UIScrollViewDelegate>
{
    NSMutableArray *stores;
    CGSize scaleSize;
    NSMutableArray *imageViews;
    BOOL pageControlUsed;
    ZKHPageControl *pageControl;
}

@property (strong, nonatomic) UIViewController *parentViewController;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (void) updateViews:(NSMutableArray *)stores scaleSize:(CGSize)size;

-(void)doImageViewClicked:(id)sender;

@end

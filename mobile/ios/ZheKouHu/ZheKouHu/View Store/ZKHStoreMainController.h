//
//  ZKHStoreMainController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackTableViewController.h"

@interface ZKHStoreMainController : ZKHBackTableViewController
{
    Boolean logined;
    UIBarButtonItem *registerItem;
    UIBarButtonItem *settingItem;
    NSMutableArray *stores;
}

@property (weak, nonatomic) IBOutlet UILabel *storeHitLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) IBOutlet UIButton *pAcitivityButton;
@property (weak, nonatomic) IBOutlet UIButton *pCouponButton;
@property (weak, nonatomic) IBOutlet UILabel *manageActivityLabel;
@property (weak, nonatomic) IBOutlet UILabel *manageCouponLabel;
@property (weak, nonatomic) IBOutlet UILabel *addStoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *couponCountLabel;
@property (weak, nonatomic) IBOutlet UIView *funcView;

- (IBAction)manageActivities:(id)sender;
- (IBAction)manageCoupons:(id)sender;
- (IBAction)editStore:(id)sender;
@end

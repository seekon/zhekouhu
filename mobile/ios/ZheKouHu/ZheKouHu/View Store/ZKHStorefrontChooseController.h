//
//  ZKHStorefrontChooseController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZKHStorefrontChooseDelegate;

@interface ZKHStorefrontChooseController : UITableViewController
{
    NSMutableArray *stores;
    
    UIImage *imageChecked;
    UIImage *imageUnchecked;
}

@property(nonatomic) NSMutableArray *selectedStorefronts;
@property(strong, nonatomic) id<ZKHStorefrontChooseDelegate> delegate;

@end

@protocol ZKHStorefrontChooseDelegate <NSObject>

-(void) choosedStorefrontsChanged:(id) storefront choosed:(Boolean)choosed;

@end

@interface ZKHStorefrontChooseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImgView;

@end
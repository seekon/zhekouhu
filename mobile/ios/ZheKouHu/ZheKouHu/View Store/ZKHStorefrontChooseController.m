//
//  ZKHStorefrontChooseController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStorefrontChooseController.h"
#import "ZKHEntity.h"
#import "ZKHProcessor+Store.h"
#import "ZKHContext.h"
#import "ZKHSwitch.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHViewUtils.h"
#import "UIViewController+CWPopup.h"

static NSString *CellIdentifier = @"ChooseStorefrontCell";

@implementation ZKHStorefrontChooseController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    imageChecked = [UIImage imageNamed:@"check_on" scale:2];
    imageUnchecked = [UIImage imageNamed:@"check_off" scale:2];
    
    if (!self.selectedStorefronts) {
        self.selectedStorefronts = [[NSMutableArray alloc] init];
    }
    
    self.tableView.backgroundColor = [UIColor clearColor];
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    [[ZKHProcessor getInstance] storesForOwner:user.uuid completionHandler:^(NSMutableArray *result) {
        stores = result;
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

-(void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)setSelectedStorefronts:(NSMutableArray *)selectedStorefronts
{
    if (_selectedStorefronts) {
        _selectedStorefronts = nil;
    }
    _selectedStorefronts = [selectedStorefronts mutableCopy];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return [stores count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    
    ZKHStoreEntity *store = stores[section - 1];
    return [store.storefronts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHStorefrontChooseCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    ZKHStoreEntity *store = stores[indexPath.section -1];
    ZKHStorefrontEntity *front = store.storefronts[indexPath.row];
    
    cell.nameLabel.text = front.name;

    if ([self.selectedStorefronts containsObject:front]) {
        cell.flagImgView.image = imageChecked;
    }else{
        cell.flagImgView.image = imageUnchecked;
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* myView = [[UIView alloc] init];
    if (section == 0) {
        myView.backgroundColor = [UIColor clearColor];
        myView.userInteractionEnabled = TRUE;
        [myView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topViewClick:)]];
    }else{
    myView.backgroundColor = [UIColor color_f5f5f5];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(18, 10, 280, 22)];
    titleLabel.textColor = [UIColor color_2d2d2d];
    titleLabel.backgroundColor = [UIColor clearColor];
    
    ZKHStoreEntity *store = stores[section -1];
    titleLabel.text = store.name;
    
    titleLabel.font = [UIFont systemFontOfSize:14];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0.5)];
    lineView.backgroundColor = [UIColor color_e3e3e3];
    
    [myView addSubview:titleLabel];
    [myView addSubview:lineView];
    }
    return myView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 40;
    if (section == 0) {
        CGFloat sHeight = [ZKHViewUtils screenHeight];
        CGFloat tableViewHeight = [stores count] * 40;
        for (ZKHStoreEntity *store in stores) {
            tableViewHeight += [store.storefronts count] * 44;
        }
        height = sHeight - k_navHeight - tableViewHeight;
        height = height < k_minPopTopHeight ? k_minPopTopHeight : height;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Boolean choosed = false;
    ZKHStoreEntity *store = stores[indexPath.section - 1];
    ZKHStorefrontEntity *front = store.storefronts[indexPath.row];
    if ([self.selectedStorefronts containsObject:front]) {
        [self.selectedStorefronts removeObject:front];
    }else{
        [self.selectedStorefronts addObject:front];
        choosed = true;
    }
    [self.tableView reloadData];
    
    if (self.delegate) {
        [self.delegate choosedStorefrontsChanged:front choosed:choosed];
    }
}


- (void)topViewClick:(id)sender
{
    UIViewController *controller = [self parentViewController];
    if (controller) {
        [controller cm_dismissPopupViewControllerAnimated:YES];
    }
}
@end

@implementation ZKHStorefrontChooseCell

@end
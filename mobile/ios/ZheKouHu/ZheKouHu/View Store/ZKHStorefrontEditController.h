//
//  ZKHStorefrontEditController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-23.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHEntity.h"
#import "ZKHCityChooseController.h"
#import "ZKHBackViewController.h"
#import "ZKHTextField.h"

@interface ZKHStorefrontEditController : ZKHBackViewController <UITextFieldDelegate, ZKHCitySelectedDelegate>
{
    ZKHStorefrontEntity *storefront;
    UIBarButtonItem *deleteBarItem;
    ZKHCityChooseController *cityChooseController;
}
@property (weak, nonatomic) IBOutlet UIImageView *cityFieldImageView;
@property (weak, nonatomic) IBOutlet ZKHTextField *cityField;
@property (weak, nonatomic) IBOutlet ZKHTextField *nameField;
@property (weak, nonatomic) IBOutlet ZKHTextField *addrField;
@property (weak, nonatomic) IBOutlet ZKHTextField *phoneField;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

- (IBAction)tapTouchDown:(id)sender;
- (void)clickCityField:(id)sender;

- (void)confirm:(id)sender;
- (void)saveAndNew:(id)sender;
- (IBAction)deleteFront:(id)sender;
- (void)editSave:(id)sender;

@property(weak, nonatomic) ZKHStoreEntity *store;
@property(nonatomic) int currentStorefrontIndex;
@property (strong, nonatomic) UIViewController *controllerOpener;
@end

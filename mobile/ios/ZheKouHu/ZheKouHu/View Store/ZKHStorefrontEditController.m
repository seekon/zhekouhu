//
//  ZKHStorefrontEditController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-23.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHStorefrontEditController.h"
#import "NSString+Utils.h"
#import "ZKHProcessor+Store.h"
#import "ZKHViewUtils.h"
#import "ZKHCityChooseController.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "UIViewController+CWPopup.h"
#import "UIView+Toast.h"
#import "ZKHProcessor+City.h"
#import "SCLAlertView.h"
#import "SCLAlertViewStyleKit.h"

@implementation ZKHStorefrontEditController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
      
    deleteBarItem = [ZKHViewUtils deleteBarButtonItem];
    UIButton *deleteButton = (UIButton *)deleteBarItem.customView;
    [deleteButton addTarget:self action:@selector(deleteFront:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.currentStorefrontIndex >= 0) {
        self.title = @"编辑门店";
        self.navigationItem.rightBarButtonItem = deleteBarItem;
        
        [self.rightButton setTitle:@"保存" forState:UIControlStateNormal];
        [self.rightButton addTarget:self action:@selector(editSave:) forControlEvents:UIControlEventTouchUpInside];
        
        storefront = self.store.storefronts[self.currentStorefrontIndex];
        self.nameField.text = storefront.name;
        self.addrField.text = storefront.addr;
        self.phoneField.text = storefront.phone;
        self.cityField.text = storefront.city.name;
    }else{
        self.title = @"添加门店";
        
        [self.rightButton addTarget:self action:@selector(saveAndNew:) forControlEvents:UIControlEventTouchUpInside];
        
        storefront = [[ZKHStorefrontEntity alloc] init];
    }
    
    self.nameField.paddingLeft = 48;
    self.addrField.paddingLeft = 48;
    self.phoneField.paddingLeft = 48;
    self.cityField.paddingRight = 36;
    
    self.cityField.delegate = self;
    [self.cityField addTarget:self action:@selector(clickCityField:) forControlEvents:UIControlEventTouchDown];
    
    self.nameField.maxNumberOfCharactersToStartValidation = 60;
    self.nameField.regexpPattern = @"*";
    self.nameField.invalidPopText = @"名称必须60字以内";
    
    self.addrField.maxNumberOfCharactersToStartValidation = 60;
    self.addrField.regexpPattern = @"*";
    self.addrField.invalidPopText = @"地址必须60字以内";
}

- (IBAction)tapTouchDown:(id)sender {
    [self.nameField resignFirstResponder];
    [self.addrField resignFirstResponder];
    [self.phoneField resignFirstResponder];
}

- (void)clickCityField:(id)sender
{
    //if (!cityChooseController) {
        cityChooseController = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHCityChooseController"];
        cityChooseController.deleage = self;
    //}
    
    cityChooseController.selectedCity = storefront.city;
    [self.navigationController cm_presentPopupViewController:cityChooseController animated:YES];
};

- (void)confirm:(id)sender {
    
    [self saveToStoreEntity:true];
}

- (void)saveAndNew:(id)sender {
    Boolean validated = [self validateFieldValues];
    if (!validated) {
        return;
    }
    
    storefront.name = self.nameField.text;
    storefront.addr = self.addrField.text;
    storefront.phone = self.phoneField.text;
    storefront.storeId = self.store.uuid;
    [self.store.storefronts addObject:storefront];
    
    self.cityField.text = @"";
    self.nameField.text = @"";
    self.addrField.text = @"";
    self.phoneField.text = @"";
    
    storefront = [[ZKHStorefrontEntity alloc]init];
}

- (void)deleteFront:(id)sender
{    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"确定" actionBlock:^(void) {
        if ([NSString isNull:storefront.uuid]) {
            [self.store.storefronts removeObject:storefront];
            if (self.controllerOpener && [self.controllerOpener isKindOfClass:[UIViewController class]]) {
                [self.navigationController popToViewController:self.controllerOpener animated:YES];
            }
            return;
        }
        [[ZKHProcessor getInstance] deleteStorefront:storefront completionHandler:^(Boolean result) {
            if (result) {
                [self.view makeToast:@"删除成功."];
                [self.store.storefronts removeObject:storefront];
                if (self.controllerOpener && [self.controllerOpener isKindOfClass:[UIViewController class]]) {
                    [self.navigationController popToViewController:self.controllerOpener animated:YES];
                }
            }else{
                [self.view makeToast:@"删除失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }];
    [alert showCustom:self.navigationController image:SCLAlertViewStyleKit.imageOfNotice color:[UIColor color_ff6722] title:@"提示" subTitle:@"是否删除门店?" closeButtonTitle:@"取消" duration:0.0f];
}

- (void)editSave:(id)sender
{
    Boolean validated = [self validateFieldValues];
    if (!validated) {
        return;
    }
    
    storefront.name = self.nameField.text;
    storefront.addr = self.addrField.text;
    storefront.phone = self.phoneField.text;
    storefront.storeId = self.store.uuid;
    
    if ([NSString isNull:storefront.uuid]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    [[ZKHProcessor getInstance] updateStorefront:storefront completionHandler:^(Boolean result) {
        if (result) {
            [self.view makeToast:@"保存成功."];
            [self.store.storefronts replaceObjectAtIndex:self.currentStorefrontIndex withObject:storefront];
        }else{
            [self.view makeToast:@"保存失败."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (Boolean) validateFieldValues
{
    [self.nameField endEditing:YES];
    [self.addrField endEditing:YES];
    [self.phoneField endEditing:YES];
        
    Boolean cancel = false;
    
    [self.nameField validate];
    [self.addrField validate];
    if (!self.nameField.isValid || !self.addrField.isValid) {
        cancel = true;
    }
    
    if (!storefront.city) {
        [ZKHViewUtils showTipView:self.cityFieldImageView message:@"请选择城市." dismissTapAnywhere:true autoDismissInvertal:-1];
        cancel = true;
    }
    return !cancel;
}

- (void)saveToStoreEntity:(Boolean) back
{
    Boolean validated = [self validateFieldValues];
    if (!validated) {
        return;
    }
    
    storefront.name = self.nameField.text;
    storefront.addr = self.addrField.text;
    storefront.phone = self.phoneField.text;
    storefront.storeId = self.store.uuid;
    
    if (self.store.uuid) {
        storefront.storeId = self.store.uuid;
        [[ZKHProcessor getInstance] addStorefront:storefront completionHandler:^(ZKHStorefrontEntity *result) {
            if (result) {
                [self.store.storefronts addObject:result];
            }
            if (back) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }else{
        [self.store.storefronts addObject:storefront];
        if (back) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return NO;
}

-(void)doSelectCity:(ZKHCityEntity *)city
{
    storefront.city = city;
    self.cityField.text = city.name;
}

- (void)back:(id)sender
{
    if ([self.childViewControllers containsObject:cityChooseController]) {
        [self cm_dismissPopupViewControllerAnimated:YES];
    }else{
        [super back:sender];
    }
}
@end

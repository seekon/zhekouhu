//
//  ZKHAccountField.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-11.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHAccountField.h"
#import "NSString+Utils.h"

@implementation ZKHAccountField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.regexpPattern = @"^[+\\w\\.\\-']+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{2,})+$";//电子邮箱
    self.invalidPopText = @"请正确输入电子邮箱或手机号";
}

- (ValidationResult)validRegexp
{
    ValidationResult result = [super validRegexp];
    if (result == ValidationFailed && self.text.length == 11 && [NSString isPureInt:self.text]) {
        result = ValidationPassed;
    }
    return result;
}

@end

//
//  ZKHAwardGoodsController.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackTableViewController.h"
@interface ZKHAwardGoodsController : ZKHBackTableViewController
{
    NSMutableArray *awardGoods;
}
@end

@interface ZKHAwardInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *awardInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *watchDetailButton;
@property (weak, nonatomic) IBOutlet UILabel *awardHelpLabel;

@end

@interface ZKHAwardGoodsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *goodsImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinLabel;

@end
//
//  ZKHAwardGoodsController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHAwardGoodsController.h"
#import "ZKHEntity.h"
#import "ZKHContext.h"
#import "ZKHViewUtils.h"
#import "UIColor+Utils.h"
#import "ZKHWebViewController.h"
#import "ZKHProcessor+User.h"
#import "ZKHProcessor+Award.h"
#import "ZKHImageLoader.h"
#import "UIImage+Utils.h"

static NSString *AwardInfoCellIdentifier = @"AwardInfoCell";
static NSString *AwardGoodsCellIdentifier = @"AwardGoodsCell";
static NSString *AwardGoodsTitleCellIdentifier = @"AwardGoodsTitleCell";

@implementation ZKHAwardGoodsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataLoaded = true;
    awardGoods = [[NSMutableArray alloc] init];
    
    [[ZKHProcessor getInstance] goodsList:^(NSMutableArray *goodsList) {
        if ([goodsList count] > 0) {
            [awardGoods addObjectsFromArray:goodsList];
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    [[ZKHProcessor getInstance] userAward:^(ZKHUserProfileEntity *profile) {
        if (profile) {
            [self.tableView reloadData];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long rows = [awardGoods count];
    if (rows == 0) {
        [super backgroundImageViewHidden:true];
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            ZKHAwardInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:AwardInfoCellIdentifier forIndexPath:indexPath];
            ZKHUserEntity *user = [ZKHContext getInstance].user;
            if (user) {
                ZKHUserProfileEntity *profile = user.profile;
                if (profile) {
                    cell.awardInfoLabel.text = [NSString stringWithFormat:@"亲，您目前尚有%@积分可用，已使用%@积分.", profile.awardSum, profile.awardUsed] ;
                }else{
                    cell.awardInfoLabel.text = @"";
                }
                
                [cell.watchDetailButton setTitle:@"查看积分明细" forState:UIControlStateNormal];
            }
            [cell.watchDetailButton addTarget:self action:@selector(watchDetailClick:) forControlEvents:UIControlEventTouchUpInside];
            
            NSMutableAttributedString *awardHelp = [[NSMutableAttributedString alloc]initWithString:cell.awardHelpLabel.text];
            NSRange contentRange = {0,[awardHelp length]};
            [awardHelp addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
            [awardHelp addAttribute:NSForegroundColorAttributeName value:[UIColor color_7f7f7f] range:contentRange];
            cell.awardHelpLabel.attributedText = awardHelp;
            
            cell.awardHelpLabel.userInteractionEnabled = TRUE;
            [cell.awardHelpLabel addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(awardhelpClick:)]];
            
            return cell;
        }
        case 1:
        {
            return [tableView dequeueReusableCellWithIdentifier:AwardGoodsTitleCellIdentifier forIndexPath:indexPath];
        }
        default:
            break;
    }
    
    ZKHAwardGoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:AwardGoodsCellIdentifier forIndexPath:indexPath];
    ZKHGoodsEntity *goods = awardGoods[indexPath.row - 2];
    cell.nameLabel.text = goods.name;
    cell.descLabel.text = goods.desc;
    cell.numLabel.text = [NSString stringWithFormat:@"剩余：%d", goods.remain];
    cell.coinLabel.text = [NSString stringWithFormat:@"×%d", goods.coinVal];
    
    [ZKHImageLoader showImageForName:goods.img imageView:cell.goodsImgView toScale:2];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    long row = indexPath.row;
    if (row > 1) {
        ZKHGoodsEntity *goods = awardGoods[row - 2];
        
        ZKHWebViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHWebViewController"];
        controller.urlString = [NSString stringWithFormat:@"/goods_detail.html?uuid=%@", goods.uuid];
        controller.title = goods.name;
        
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        UIImage *bgImage = [UIImage imageNamed:@"bg_store_login.jpg" scale:2];
        UIImageView *bgView = [[UIImageView alloc] initWithFrame:cell.frame];
        bgView.image = bgImage;
        
        cell.backgroundView = bgView;
    }else if (indexPath.row == 1) {
        cell.backgroundColor = [UIColor color_f5f5f5];
    }else{
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = 0;
    switch (indexPath.row) {
        case 0:
            height = 100;
            break;
        case 1:
            height = 30;
            break;
        default:
            height = 87;
            break;
    }
    return height;
}

- (void) watchDetailClick:(id)sender
{
    NSString *controllerName = nil;
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        controllerName = @"ZKHUserAwardDetailController";
    }else{
        controllerName = @"ZKHLoginController";
    }
    
    UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:controllerName];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)awardhelpClick:(id)sender
{
    ZKHWebViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHWebViewController"];
    controller.urlString = @"/award_help.html";
    controller.title = @"积分帮助";
    
    [self.navigationController pushViewController:controller animated:YES];
}

@end

@implementation ZKHAwardInfoCell

@end

@implementation ZKHAwardGoodsCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    self.backgroundColor = [UIColor color_f8f8f8];
}
@end
//
//  ZKHChangePwdController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackViewController.h"
#import "ZKHTextField.h"

@interface ZKHChangePwdController : ZKHBackViewController

@property (weak, nonatomic) IBOutlet ZKHTextField *oldPwdField;
@property (weak, nonatomic) IBOutlet ZKHTextField *pwdNewField;
@property (weak, nonatomic) IBOutlet ZKHTextField *pwdConfField;

- (IBAction)tapTouchDown:(id)sender;
- (IBAction)changePwd:(id)sender;
- (IBAction)pwdNewDidEnd:(id)sender;

@end

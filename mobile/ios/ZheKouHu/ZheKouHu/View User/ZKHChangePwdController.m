//
//  ZKHChangePwdController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHChangePwdController.h"
#import "ZKHContext.h"
#import "ZKHEntity.h"
#import "NSString+Utils.h"
#import "ZKHProcessor+User.h"
#import "UIView+Toast.h"
#import "ZKHConst.h"

@implementation ZKHChangePwdController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //self.oldPwdField.regexpPattern = [ZKHContext getInstance].user.pwd;
    self.oldPwdField.invalidPopText = @"原密码输入错误.";
    
    self.pwdConfField.invalidPopText = @"两次输入的新密码不一致.";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)tapTouchDown:(id)sender {
    [self.oldPwdField resignFirstResponder];
    [self.pwdNewField resignFirstResponder];
    [self.pwdConfField resignFirstResponder];
}

- (IBAction)changePwd:(id)sender {
    [self.oldPwdField endEditing:YES];
    [self.pwdNewField endEditing:YES];
    [self.pwdConfField endEditing:YES];
    
    NSString *newPwd = self.pwdNewField.text;
    NSString *pwdConf = self.pwdConfField.text;
    
    Boolean cancel = false;

    if (![pwdConf isEqualToString:newPwd]) {
        self.pwdConfField.regexpPattern = newPwd;
        cancel = true;
    }

    ZKHUserEntity *user = [ZKHContext getInstance].user;

    
    [self.oldPwdField validate];
    [self.pwdNewField validate];
    [self.pwdConfField validate];
    if (!self.oldPwdField.isValid || !self.pwdNewField.isValid || !self.pwdConfField) {
        cancel = true;
    }
    
    if (cancel) {
        return;
    }
    
    
    [[ZKHProcessor getInstance] updatePwd:user.uuid pwd:newPwd oldPwd:self.oldPwdField.text  completionHandler:^(NSMutableDictionary *result) {
        id uuid = result[KEY_UUID];
        if (uuid) {
            [self.view makeToast:@"修改成功."];
            [self.navigationController popViewControllerAnimated:YES];
        }else if(result[KEY_ERROR_TYPE]){
            [self.oldPwdField showErrorMessage:@"原密码错误."];
        }else{
            [self.view makeToast:@"修改失败."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (IBAction)pwdNewDidEnd:(id)sender {
    self.pwdConfField.regexpPattern = self.pwdNewField.text;
    [self.pwdConfField validate];
}
@end

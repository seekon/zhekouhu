//
//  ZKHLoginController.h
//  ZheKouHu
//
//  Created by undyliu on 14-7-14.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackViewController.h"
#import "ZKHTextField.h"

@interface ZKHLoginController : ZKHBackViewController

@property (weak, nonatomic) IBOutlet UIButton *forgetPwdButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet ZKHTextField *codeField;
@property (weak, nonatomic) IBOutlet ZKHTextField *pwdField;
- (IBAction)login:(id)sender;

@end

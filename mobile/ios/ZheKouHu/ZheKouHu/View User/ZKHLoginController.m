//
//  ZKHLoginController.m
//  ZheKouHu
//
//  Created by undyliu on 14-7-14.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHLoginController.h"
#import "NSString+Utils.h"
#import "ZKHProcessor+User.h"
#import "ZKHConst.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"

@implementation ZKHLoginController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.loginButton setBackgroundImage:[UIImage imageNamed:@"bg_button_sel.png"] forState:UIControlStateHighlighted];
    
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"忘记密码?"]];
    NSRange contentRange = {0,[content length]};
    [content addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
    [content addAttribute:NSForegroundColorAttributeName value:[UIColor color_7f7f7f] range:contentRange];
    [self.forgetPwdButton setAttributedTitle:content forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)login:(id)sender {
    [self.codeField endEditing:true];
    [self.pwdField endEditing:true];
    
    NSString *code = self.codeField.text;
    NSString *pwd = self.pwdField.text;
    
    [self.codeField validate];
    [self.pwdField validate];
    
    if (!self.codeField.isValid || !self.pwdField.isValid) {
        return;
    }
    
    [[ZKHProcessor getInstance] login:code pwd:pwd completionHandler:^(NSMutableDictionary *authObj) {
        Boolean authed = [[authObj objectForKey:KEY_AUTHED] boolValue];
        if (authed) {
            //self.codeField.text = nil;
            //self.pwdField.text = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            NSString *errorType = authObj[KEY_ERROR_TYPE];
            if ([kAuth_user_error isEqualToString:errorType]) {
                [self.codeField showErrorMessage:@"此用户不存在."];
            }else if([kAuth_pass_error isEqualToString:errorType]){
                [self.pwdField showErrorMessage:@"密码错误."];
            }
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

@end

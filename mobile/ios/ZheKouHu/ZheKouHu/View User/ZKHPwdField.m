//
//  ZKHPwdField.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-11.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHPwdField.h"

@implementation ZKHPwdField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.regexpPattern = @"[a-zA-Z0-9]+";
    self.minimalNumberOfCharactersToStartValidation = 4;
    self.maxNumberOfCharactersToStartValidation = 10;
    self.invalidPopText = @"密码必须4~10位.";
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

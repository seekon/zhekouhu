//
//  ZKHRegisterUserController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackViewController.h"
#import "ZKHTextField.h"

@interface ZKHRegisterUserController : ZKHBackViewController
@property (weak, nonatomic) IBOutlet ZKHTextField *codeField;
@property (weak, nonatomic) IBOutlet ZKHTextField *pwdField;
@property (weak, nonatomic) IBOutlet ZKHTextField *pwdConfField;

- (IBAction)registerUser:(id)sender;
- (IBAction)tapTouchDown:(id)sender;
- (IBAction)pwdDidEnd:(id)sender;


@end

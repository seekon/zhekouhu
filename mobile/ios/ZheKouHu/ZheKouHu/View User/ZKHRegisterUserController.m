//
//  ZKHRegisterUserController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHRegisterUserController.h"
#import "NSString+Utils.h"
#import "ZKHProcessor+User.h"
#import "ZKHEntity.h"

@implementation ZKHRegisterUserController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"注册";
    
    self.pwdConfField.invalidPopText = @"两次输入的新密码不一致.";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)registerUser:(id)sender {
    [self.codeField endEditing:true];
    [self.pwdField endEditing:true];
    [self.pwdConfField endEditing:true];
    
    NSString *code = self.codeField.text;
    NSString *pwd = self.pwdField.text;

    self.pwdConfField.regexpPattern = self.pwdField.text;
    [self.codeField validate];
    [self.pwdField validate];
    [self.pwdConfField validate];
    
    if (!self.codeField.isValid || !self.pwdField.isValid || !self.pwdConfField.isValid) {
        return;
    }
    
    ZKHUserEntity *user = [[ZKHUserEntity alloc] init];
    user.code = code;
    user.pwd = pwd;
    [[ZKHProcessor getInstance] registerUser:user completionHandler:^(Boolean result) {
        if (result) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (IBAction)tapTouchDown:(id)sender {
    [self.codeField resignFirstResponder];
    [self.pwdField resignFirstResponder];
    [self.pwdConfField resignFirstResponder];
}

- (IBAction)pwdDidEnd:(id)sender {
    self.pwdConfField.regexpPattern = self.pwdField.text;
}
@end

//
//  ZKHResetPwdController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-7.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackViewController.h"
#import "ZKHTextField.h"

@interface ZKHResetPwdController : ZKHBackViewController
@property (weak, nonatomic) IBOutlet ZKHTextField *userCodeField;

- (IBAction)tapTouchDown:(id)sender;
- (IBAction)commit:(id)sender;

@end

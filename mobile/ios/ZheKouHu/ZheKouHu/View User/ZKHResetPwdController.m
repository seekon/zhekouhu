//
//  ZKHResetPwdController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-7.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHResetPwdController.h"
#import "ZKHProcessor+User.h"
#import "ZKHConst.h"
#import "UIView+Toast.h"

@interface ZKHResetPwdController ()

@end

@implementation ZKHResetPwdController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapTouchDown:(id)sender {
    [self.userCodeField resignFirstResponder];
}

- (IBAction)commit:(id)sender {
    
    [self.userCodeField validate];
    if (!self.userCodeField.isValid) {
        return;
    }
    
    [self.userCodeField resignFirstResponder];
    
    NSString *code = self.userCodeField.text;
    [[ZKHProcessor getInstance] resetPwd:code completionHandler:^(NSMutableDictionary *authObj) {
        id error = authObj[KEY_ERROR_TYPE];
        if (error) {
            [self.view makeToast:@"此帐号还未注册."];
        }else{
            [self.view makeToast:@"提交成功，请注意查收密码."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

@end

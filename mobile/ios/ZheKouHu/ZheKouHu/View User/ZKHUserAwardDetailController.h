//
//  ZKHUserAwardDetailController.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-21.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackTableViewController.h"
#import "PullTableView.h"

@interface ZKHUserAwardDetailController : ZKHBackTableViewController<PullTableViewDelegate>
{
    int offset;
    NSMutableArray *userAwards;
}

@end

@interface ZKHAwardDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *awardTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *coinValLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@end
//
//  ZKHUserAwardDetailController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-21.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHUserAwardDetailController.h"
#import "ZKHViewUtils.h"
#import "ZKHProcessor+User.h"
#import "ZKHEntity.h"
#import "NSDate+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHContext.h"

static NSString *AwardDetailCellIdentifier = @"AwardDetailCell";

@interface ZKHUserAwardDetailController ()

@end

@implementation ZKHUserAwardDetailController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"积分明细";
    userAwards = [[NSMutableArray alloc] init];
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    
    [self loadData:0];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadData:(int)offsetVal
{
    [[ZKHProcessor getInstance] userAwardDetails:offsetVal completionHandler:^(NSMutableArray *data) {
        self.dataLoaded = true;
        if (offsetVal == 0) {
            offset = 0;
            [userAwards removeAllObjects];
        }
        if ([data count] > 0 ) {
            [userAwards addObjectsFromArray:data];
            offset += [data count];
        }
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [self loadData:0];
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [self loadData:offset];
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long rows = [userAwards count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHAwardDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:AwardDetailCellIdentifier forIndexPath:indexPath];
    
    ZKHUserAwardEntity * award = userAwards[indexPath.row];
    cell.commentLabel.text = award.comment;
    if (award.coinVal > 0) {
        cell.coinValLabel.text = [NSString stringWithFormat:@"+%d", award.coinVal];
    }else{
        cell.coinValLabel.text = [NSString stringWithFormat:@"%d", award.coinVal];
    }
    cell.awardTimeLabel.text = [[NSDate dateWithMilliSeconds:[award.awardTime longLongValue]] toyyyyMMddString];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    ZKHUserProfileEntity *profile = [ZKHContext getInstance].user.profile;
    long awardRemain = 0;
    if (profile) {
        awardRemain = [profile.awardSum integerValue] - [profile.awardUsed integerValue];
    }
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor color_f5f5f5];
    view.frame = CGRectMake(0, 0, 320, 30);
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(15, 6, 180, 18);
    label.text = [NSString stringWithFormat:@"共有%ld积分可用", awardRemain];
    label.font = [UIFont systemFontOfSize:14];
    
    [view addSubview:label];
    
    return view;
}
@end

@implementation ZKHAwardDetailCell

@end

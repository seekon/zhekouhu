//
//  ZKHUserFavoritController.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackTableViewController.h"
#import "PullTableView.h"

@interface ZKHUserFavoritController : ZKHBackTableViewController<PullTableViewDelegate>
{
    NSMutableArray *discounts;
    int offset;
}
@end

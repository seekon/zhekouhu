//
//  ZKHUserFavoritController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHUserFavoritController.h"
#import "ZKHViewUtils.h"
#import "ZKHContext.h"
#import "ZKHMainActivityCell.h"
#import "ZKHImageLoader.h"
#import "ZKHProcessor+Discount.h"
#import "ZKHActivityDetailController.h"
#import "ZKHConst.h"

static NSString *CellIdentifier = @"FavoritCell";

@implementation ZKHUserFavoritController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    discounts = [[NSMutableArray alloc] init];
    
    ((PullTableView *)self.tableView).pullDelegate = self;
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
}

- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self loadData:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadData:(int)offsetVal
{
    [[ZKHProcessor getInstance] userFavorits:offsetVal completionHandler:^(NSMutableArray *data) {
        self.dataLoaded = true;
        if (offsetVal == 0) {
            offset = 0;
            [discounts removeAllObjects];
        }
        if ([data count] > 0) {
            offset += [data count];
            [discounts addObjectsFromArray:data];
        }
        [self.tableView reloadData];
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

#pragma mark - Refresh and load more methods

- (void) refreshTable
{
    [self loadData:0];
    ((PullTableView *)self.tableView).pullLastRefreshDate = [NSDate date];
    ((PullTableView *)self.tableView).pullTableIsRefreshing = NO;
}

- (void) loadMoreDataToTable
{
    [self loadData:offset];
    ((PullTableView *)self.tableView).pullTableIsLoadingMore = NO;
}

#pragma mark - PullTableViewDelegate

- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:3.0f];
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadMoreDataToTable) withObject:nil afterDelay:3.0f];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rows = [discounts count];
    if (rows == 0) {
        [super backgroundViewHidden:false];
    }else{
        [super backgroundViewHidden:true];
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHMainActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.tag = indexPath.row;
    
    ZKHDiscountEntity *discount = discounts[indexPath.row];
    
    ZKHFileEntity *file = discount.images[0];
    [ZKHImageLoader showImageForName:file.aliasName imageView:cell.photoView toScale:3];
    
    cell.contentLabel.text = discount.content;
    
    if ([discount.origin isEqualToString:VAL_DISCOUNT_ORIGIN_STORER]) {
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_STORER;
    }else{
        cell.originLabel.text = TEXT_DISCOUNT_ORIGIN_CUSTOMER;
    }
    
    NSMutableArray *fronts = discount.storefronts;
    if ([fronts count] > 0) {
        ZKHStorefrontEntity *front = fronts[0];
        
        cell.storeLabel.hidden = false;
        cell.storeLabel.text = [NSString stringWithFormat:@"%@-%@", front.storeName, front.name];
        if (front.distance == DBL_MAX) {
            cell.distanceLabel.hidden = true;
        }else{
            cell.distanceLabel.hidden = false;
            cell.distanceLabel.text = [NSString stringWithFormat:@"%.2fkm", front.distance/1000];
        }
        [cell updateViews:front];
    }else{
        cell.storeLabel.hidden = true;
        cell.distanceLabel.hidden = true;
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHActivityDetailController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHActivityDetailController"];
    controller.discount = discounts[indexPath.row];
    controller.openOrigin = DISCOUNT_ORIGIN_FAVORIT;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor whiteColor];
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 87;
}

@end

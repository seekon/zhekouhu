//
//  ZKHUserMainCOntroller.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackTableViewController.h"

@interface ZKHUserMainController :  ZKHBackTableViewController
{
    Boolean logined;
    UIBarButtonItem *registerItem;
    UIBarButtonItem *settingItem;
    NSMutableArray *stores;
}

@property (weak, nonatomic) IBOutlet UILabel *storeHitLabel;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (weak, nonatomic) IBOutlet UIButton *pAcitivityButton;
@property (weak, nonatomic) IBOutlet UIButton *pCouponButton;
@property (weak, nonatomic) IBOutlet UILabel *discountCountLabel;
@property (weak, nonatomic) IBOutlet UIView *funcView;

- (IBAction)manageDiscounts:(id)sender;
- (IBAction)manageFavorits:(id)sender;
- (IBAction)awardGoods:(id)sender;
- (IBAction)newStore:(id)sender;
- (IBAction)moreItems:(id)sender;
@end

//
//  ZKHUserMainCOntroller.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ZKHUserMainController.h"
#import "ZKHRegisterUserController.h"
#import "ZKHStoreSettingController.h"
#import "ZKHProcessor+User.h"
#import "ZKHProcessor+Discount.h"
#import "NSString+Utils.h"
#import "ZKHConst.h"
#import "ZKHViewUtils.h"
#import "ZKHStoreImagesCell.h"
#import "ZKHProcessor+Store.h"
#import "ZKHContext.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"
#import "ZKHStoreEditController.h"
#import "ZKHStoreDiscountsController.h"
#import "ZKHConst.h"
#import "ZKHPublishDiscountController.h"
#import "ZKHUserPubDiscountController.h"

#define kImageCellHeight 151

static NSString *StoreImagesCellIdentifier = @"StoreImagesCell";

@implementation ZKHUserMainController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *nib = [UINib nibWithNibName:@"ZKHStoreImagesCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:StoreImagesCellIdentifier];
    
    UIButton *registerButton = [[UIButton alloc] init];
    [registerButton setTitle:@"注册" forState:UIControlStateNormal];
    registerButton.titleLabel.font = [UIFont systemFontOfSize:13];
    registerButton.frame = CGRectMake(82, 0, 52, 44);
    [registerButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:registerButton.frame.size] forState:UIControlStateHighlighted];
    [registerButton addTarget:self action:@selector(registerClick:) forControlEvents:UIControlEventTouchUpInside];
    registerItem = [[UIBarButtonItem alloc] initWithCustomView:registerButton];
    
    [self.loginButton setImage:[UIImage imageNamed:@"btn_p_login_nor.png" scale:2] forState:UIControlStateNormal];
    [self.loginButton setImage:[UIImage imageNamed:@"btn_p_login_sel.png" scale:2] forState:UIControlStateHighlighted];
    
    [self.pAcitivityButton setImage:[UIImage imageNamed:@"btn_p_activity_nor.png" scale:2] forState:UIControlStateNormal];
    [self.pAcitivityButton setImage:[UIImage imageNamed:@"btn_p_activity_sel.png" scale:2] forState:UIControlStateHighlighted];
    [self.pAcitivityButton addTarget:self action:@selector(publishAcitivity:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.pCouponButton setImage:[UIImage imageNamed:@"btn_p_coupon_nor.png" scale:2] forState:UIControlStateNormal];
    [self.pCouponButton setImage:[UIImage imageNamed:@"btn_p_coupon_sel.png" scale:2] forState:UIControlStateHighlighted];
    [self.pCouponButton addTarget:self action:@selector(publishCoupon:) forControlEvents:UIControlEventTouchUpInside];
    
    self.funcView.layer.cornerRadius = 2;
    self.funcView.layer.masksToBounds = YES;
    self.funcView.layer.borderWidth = 0.5;
    self.funcView.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    
    [ZKHViewUtils setTableViewExtraCellLineHidden:self.tableView];
    [self.tableView setBackgroundColor:[UIColor color_f5f5f5]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateViews];
}

- (void) updateViews
{
    //进行店铺的登录
    [[ZKHProcessor getInstance] autoLogin:^(Boolean result) {
        logined = result;
        [self updateNavigation];
        [self updateItemsStyle];
        [self.tableView reloadData];
        
        if (logined) {//登录成功
            //获取店铺列表
            ZKHUserEntity *user = [ZKHContext getInstance].user;
            [[ZKHProcessor getInstance] storesForOwner:user.uuid completionHandler:^(NSMutableArray *result) {
                stores = result;
                [self updateItemsStyle];
                [self.tableView reloadData];
            } errorHandler:^(ZKHErrorEntity *error) {
                
            }];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (void) updateItemsStyle
{
    if (logined > 0) {
        NSString *userId = [ZKHContext getInstance].user.uuid;
        int discountCount = [[ZKHProcessor getInstance] discountCountForOwner:userId];
        
        self.discountCountLabel.text = [NSString stringWithFormat:@"%d笔", discountCount];
    }else{
        self.discountCountLabel.text = nil;
    }
}

- (void) updateNavigation
{
    UINavigationItem *nav  = nil;
    if ([self.parentViewController isKindOfClass:[UITabBarController class]]) {
        nav = self.parentViewController.navigationItem;
    }else{
        nav = self.navigationItem;
    }
    nav.title = @"我的";
    if (!logined) {
        nav.rightBarButtonItem = registerItem;
    }else{
        nav.rightBarButtonItem = settingItem;
    }
    nav.leftBarButtonItems = nil;
}

- (void) registerClick:(UIBarButtonItem *)sender
{
    ZKHRegisterUserController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHRegisterUserController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if (logined) {
            if ([stores count] > 0) {
                ZKHStoreImagesCell *cell = [tableView dequeueReusableCellWithIdentifier:StoreImagesCellIdentifier forIndexPath:indexPath];
                cell.parentViewController = self;
                [cell updateViews:stores scaleSize:CGSizeMake([ZKHViewUtils screenWidth], kImageCellHeight)];
                return cell;
            }else{
                UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
                self.storeHitLabel.text = @"亲，您还未开店铺哦~~";
                self.loginButton.hidden = true;
                return cell;
            }
        }else{
            UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
            self.storeHitLabel.text = @"亲，您还未登录哦~";
            self.loginButton.hidden = false;
            return cell;
        }
    }else{
        UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        cell.tag = indexPath.row;

        if(indexPath.row != 3){
            [ZKHViewUtils drawSeparaterAtHeader:cell offsetX:0];
        }else{
            CGFloat lineHeight = 0.5f;
            UIColor* lineColor = [UIColor color_e3e3e3];
            CGSize lineSize = CGSizeMake(320 - 12 - lineHeight * 2, lineHeight);
            
            for (int i = 1; i < 5; i++) {
              [ZKHViewUtils drawStraightLine:cell startPoint:CGPointMake(6 + lineHeight, 46 * i) color:lineColor lineSize:lineSize];
            }
        }
        
        if (indexPath.row == 1) {
            UIView *hView = [[UIView alloc] initWithFrame:CGRectMake(159, 15, 0.5, 29)];
            hView.backgroundColor = [UIColor color_d5d5d5];
            [cell addSubview:hView];
        }
        
        return cell;
    }
}


#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return;
    }
    if (!logined) {
        return;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 && (!logined || [stores count] == 0)) {
        UIImage *bgImage = [UIImage imageNamed:@"bg_store_login2" scale:2];
        UIImageView *bgView = [[UIImageView alloc] initWithFrame:cell.frame];
        bgView.image = bgImage;
        
        cell.backgroundView = bgView;
    }else if (indexPath.row == 2 || indexPath.row == 3){
        cell.backgroundColor = [UIColor color_f5f5f5];
    }
    else{
        cell.backgroundColor = [UIColor whiteColor];
    }
}

- (void)publishAcitivity:(id)sender
{
    if (!logined || [stores count] == 0) {
        [self openUserPubDiscountView];
    }else{
        [self openStorePubDiscountView:VAL_DISCOUNT_TYPE_ACTIVITY];
    }
}

- (void)publishCoupon:(id)sender
{
    if (!logined || [stores count] == 0) {
        [self openUserPubDiscountView];
    }else{
        [self openStorePubDiscountView:VAL_DISCOUNT_TYPE_COUPON];
    }
}

- (IBAction)manageDiscounts:(id)sender {
    if (!logined) {
        [self openLoginView];
        return;
    }
    
    ZKHStoreDiscountsController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHStoreDiscountsController"];
    controller.type = VAL_DISCOUNT_TYPE_ALL;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)manageFavorits:(id)sender {
    UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"UserFavoritController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)newStore:(id)sender {
    if (!logined) {
        [self openLoginView];
        return;
    }else {
        ZKHStoreEditController *controller = [[ZKHViewUtils mainStoryboard]instantiateViewControllerWithIdentifier:@"ZKHStoreEditController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (void)awardGoods:(id)sender
{
    UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"AwardGoodsController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)moreItems:(id)sender
{
    UIViewController *controller = [[ZKHViewUtils mainStoryboard]instantiateViewControllerWithIdentifier:@"MoreMainController"];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) openLoginView
{
    UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHLoginController"];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void) openUserPubDiscountView
{
    ZKHUserPubDiscountController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHUserPubDiscountController"];
    controller.controllerOpener = self;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void) openStorePubDiscountView:(NSString *)type
{
    ZKHPublishDiscountController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHPublishDiscountController"];
    controller.controllerOpener = self;
    controller.type = type;
    [self.navigationController pushViewController:controller animated:YES];
}
@end

//
//  ZKHUserProfileController.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackViewController.h"
#import "ZKHPhotoUploadController.h"
#import "ZKHTextField.h"

@interface ZKHUserProfileController : ZKHBackViewController<ZKHPhotoChooseDelegate, UITextFieldDelegate>
{
    UIImage *selectedImage;
    
    ZKHPhotoUploadController *photoUploadController;
    UITextField *activeField;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIImageView *photoField;
@property (weak, nonatomic) IBOutlet ZKHTextField *codeField;
@property (weak, nonatomic) IBOutlet ZKHTextField *nameField;
@property (weak, nonatomic) IBOutlet ZKHTextField *phoneField;
@property (weak, nonatomic) IBOutlet ZKHTextField *deliverAddrField;
- (IBAction)save:(id)sender;
- (IBAction)bgTouchDown:(id)sender;

@end

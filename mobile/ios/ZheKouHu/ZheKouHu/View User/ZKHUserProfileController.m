//
//  ZKHUserProfileController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHUserProfileController.h"
#import "ZKHContext.h"
#import "ZKHEntity.h"
#import "NSString+Utils.h"
#import "ZKHImageLoader.h"
#import "ZKHConst.h"
#import "ZKHViewUtils.h"
#import "UIViewController+CWPopup.h"
#import "UIColor+Utils.h"
#import "UIImage+ImageCompress.h"
#import "NSDate+Utils.h"
#import "ZKHProcessor+User.h"
#import "UIView+Toast.h"
#import "ZKHViewUtils.h"

@implementation ZKHUserProfileController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.title = @"个人信息";
    
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    ZKHUserProfileEntity *profile = user.profile;
    if (profile) {
        self.codeField.text = user.code;
        self.nameField.text = profile.name;
        self.phoneField.text = profile.phone;
        self.deliverAddrField.text = profile.devilerAddr;
        if (profile.photo && ![NSString isNull:profile.photo.aliasName]) {
            [ZKHImageLoader showImageForName:profile.photo.aliasName imageView:self.photoField toScale:2];
        }
    }
    
    [ZKHViewUtils setTextFieldCursorColor:self.nameField color:[UIColor color_ff6722]];
    [ZKHViewUtils setTextFieldCursorColor:self.phoneField color:[UIColor color_ff6722]];
    [ZKHViewUtils setTextFieldCursorColor:self.deliverAddrField color:[UIColor color_ff6722]];
    
    self.photoField.userInteractionEnabled = TRUE;
    [self.photoField addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(photoViewClick:)]];
    
    self.codeField.paddingLeft = 50;
    self.codeField.textColor = [UIColor color_7f7f7f];
    
    self.nameField.paddingLeft = 50;
    self.nameField.delegate = self;
    
    self.phoneField.paddingLeft = 74;
    self.phoneField.delegate = self;
    
    self.deliverAddrField.paddingLeft = 74;
    self.deliverAddrField.delegate = self;
    
    self.scrollView.userInteractionEnabled = true;
    [self.scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bgTouchDown:)]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self unregisterForKeyboardNotifications];
}

- (void)photoViewClick:(UITapGestureRecognizer *)sender
{
    //if (!photoUploadController) {
    photoUploadController = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHPhotoUploadController"];
    photoUploadController.delegate = self;
    //}
    photoUploadController.view.frame = CGRectMake(0, 0, 320, 113);
    [self.navigationController cm_presentPopupViewController:photoUploadController animated:YES];
}

- (void) doImageChoosed:(UIImage *)_selectedImage
{
    if (IOS7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    selectedImage = _selectedImage;
    self.photoField.image = [ZKHImageLoader scaleImage:selectedImage toScale:CGSizeMake(self.photoField.frame.size.width * 2, self.photoField.frame.size.height * 2)];
    
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
}

- (void)doImageCanceled
{
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
}

- (void)back:(id)sender
{
    if ([self.navigationController.childViewControllers containsObject:photoUploadController]) {
        [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    }else{
        [super back:sender];
    }
}

- (IBAction)save:(id)sender {
    ZKHUserProfileEntity *profile = nil;
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user.profile) {
        profile = [user.profile copy];
    }else{
        profile = [[ZKHUserProfileEntity alloc] init];
        profile.uuid = user.uuid;
    }
    
    if (selectedImage) {
        NSString *aliasName = [NSString stringWithFormat:@"%@_profile_%@.png", user.code, [NSDate currentTimeString]];
        NSString *filePath = [ZKHImageLoader saveImageData:[UIImage compressImageData:selectedImage compressRatio:0.9f] fileName:aliasName];
        
        ZKHFileEntity *file = [[ZKHFileEntity alloc] init];
        file.aliasName = aliasName;
        file.fileUrl = filePath;
        
        profile.photo = file;
    }
    profile.name = self.nameField.text;
    profile.phone = self.phoneField.text;
    profile.devilerAddr = self.deliverAddrField.text;
    
    [[ZKHProcessor getInstance] saveUserProfile:profile completionHandler:^(Boolean result) {
        if (result) {
            user.profile = profile;
            [self.view makeToast:@"保存成功."];
        }else{
            [self.view makeToast:@"保存失败."];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (IBAction)bgTouchDown:(id)sender {
    [self.nameField resignFirstResponder];
    [self.phoneField resignFirstResponder];
    [self.deliverAddrField resignFirstResponder];
}

////开始编辑输入框的时候，软键盘出现，执行此事件
//-(void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    CGRect frame = textField.frame;
//    int offset = frame.origin.y + 32 - (self.view.frame.size.height - 216.0);//键盘高度216
//    
//    NSTimeInterval animationDuration = 0.30f;
//    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
//    [UIView setAnimationDuration:animationDuration];
//    
//    //将视图的Y坐标向上移动offset个单位，以使下面腾出地方用于软键盘的显示
//    if(offset > 0)
//        self.view.frame = CGRectMake(0.0f, -offset, self.view.frame.size.width, self.view.frame.size.height);
//    
//    [UIView commitAnimations];
//}
//
////当用户按下return键或者按回车键，keyboard消失
//-(BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}
//
////输入框编辑完成以后，将视图恢复到原始状态
//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    self.scrollView.frame = CGRectMake(0, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height);
//}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    CGPoint fieldPoint = CGPointMake(activeField.frame.origin.x, activeField.frame.origin.y + activeField.frame.size.height + k_navHeight + 20);
    if (!CGRectContainsPoint(aRect, fieldPoint) ) {
        CGPoint scrollPoint = CGPointMake(0.0, activeField.frame.origin.y - kbSize.height
                                          + activeField.frame.size.height + k_navHeight + 20);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

@end

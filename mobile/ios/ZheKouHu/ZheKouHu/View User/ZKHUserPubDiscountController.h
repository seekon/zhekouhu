//
//  ZKHUserPubDiscountController.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHBackTableViewController.h"
#import "ZKHButton.h"
#import "ZKHTextView.h"
#import "ZKHImageActionDelegate.h"
#import "ZKHPhotoUploadController.h"
#import "ZKHEntity.h"
#import "ZKHNearbyStoresController.h"

@interface ZKHUserPubDiscountController : ZKHBackTableViewController<UICollectionViewDataSource, UICollectionViewDelegate,ZKHImageActionDelegate, ZKHPhotoChooseDelegate, ZKHNearbyStoreSelectedDelegate>
{
    NSMutableArray *selectedImages;
    NSMutableArray *deletedImageFiles;
    
    ZKHPhotoUploadController *photoController;
    ZKHNearbyStoreEntity *nearbyStore;
    Boolean anonymousContinuePublish;
    
    UIBarButtonItem *deleteBarItem;
    Boolean showAnonymous;
}
//@property (weak, nonatomic) IBOutlet UIImageView *statusImgView;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;

@property (strong, nonatomic) ZKHDiscountEntity *discount;
@property (strong, nonatomic) UIViewController *controllerOpener;

@property (weak, nonatomic) IBOutlet UIView *discountImagesView;
@property (weak, nonatomic) IBOutlet UICollectionView *imageContainer;
@property (weak, nonatomic) IBOutlet ZKHTextView *contentTextView;
@property (weak, nonatomic) IBOutlet ZKHButton *publishBtn;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
- (IBAction)publishBtnClick:(id)sender;

@end

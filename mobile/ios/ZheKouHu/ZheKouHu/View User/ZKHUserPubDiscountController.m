//
//  ZKHUserPubDiscountController.m
//  ZheKouHu
//
//  Created by undyliu on 14-11-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHUserPubDiscountController.h"
#import "ZKHPictureCell.h"
#import "UIImage+Utils.h"
#import "ZKHImageLoader.h"
#import "ZKHImageListPreviewController.h"
#import "ZKHConst.h"
#import "UITableViewController+CWPopup.h"
#import "ZKHViewUtils.h"
#import "UIColor+Utils.h"
#import "ZKHContext.h"
#import "SCLAlertView.h"
#import "SCLAlertViewStyleKit.h"
#import "NSDate+Utils.h"
#import "ZKHProcessor+Discount.h"
#import "UIView+Toast.h"
#import "UIImage+ImageCompress.h"
#import "NSString+Utils.h"
#import "ZKHMainTabBarController.h"

#define kImageWidth 120
#define kImageHeight 120

static NSString *CellIdentifier = @"UserPubPictureCell";

@implementation ZKHUserPubDiscountController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    selectedImages = [[NSMutableArray alloc] init];
    deletedImageFiles = [[NSMutableArray alloc] init];
    showAnonymous = true;
    
    if (!self.controllerOpener) {
        [self updateNavigation];
    }
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.imageContainer.dataSource = self;
    self.imageContainer.delegate = self;
    
    self.imageContainer.userInteractionEnabled = TRUE;
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageContainerClick:)];
    
    [self.imageContainer addGestureRecognizer:gr];
    
    self.discountImagesView.layer.borderWidth = 0.5;
    self.discountImagesView.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    
    if (self.discount) {
        self.contentTextView.text = self.discount.content;
    }else{
        self.contentTextView.text = @"";
        self.locationLabel.text = @"";
    }
    self.contentTextView.placeholder = @"请输入活动内容";
    
    if (![NSString isNull:self.discount.location]) {
        nearbyStore = [[ZKHNearbyStoreEntity alloc] initWithJSONObject:[self.discount.location toJSONObject]];
        self.locationLabel.text = nearbyStore.name;
    }else{
        self.locationLabel.text = @"";
    }
    
    deleteBarItem = [ZKHViewUtils deleteBarButtonItem];
    UIButton *deleteButton = (UIButton *)deleteBarItem.customView;
    [deleteButton addTarget:self action:@selector(deleteDiscount:) forControlEvents:UIControlEventTouchUpInside];
    if (self.discount && self.discount.uuid) {
        NSString *imageName = nil;
        NSString *status = self.discount.status;
        if ([status isEqualToString:VAL_DISCOUNT_STATUS_CHECHING]) {
            imageName = @"text_checking";
        }else if([status isEqualToString:VAL_DISCOUNT_STATUS_INVALID]){
            imageName = @"text_invalid";
        }else if([status isEqualToString:VAL_DISCOUNT_STATUS_VALID]){
            imageName = @"text_valid";
        }
        if (imageName) {
            [self.statusButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
            [self.statusButton addTarget:self action:@selector(statusImgViewClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        self.navigationItem.rightBarButtonItem = deleteBarItem;
        
        if (self.discount.status && [self.discount.status isEqualToString:VAL_DISCOUNT_STATUS_INVALID]) {
            [self setDiscountEnable:false];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.controllerOpener) {
        [self updateNavigation];
    }
    [self.imageContainer reloadData];
    

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self showAnonymousWindow];
}

- (void) showAnonymousWindow
{
    if ([ZKHContext getInstance].user || !showAnonymous) {
        return;
    }
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"继续发" actionBlock:^(void) {
        anonymousContinuePublish = true;
     }];
    [alert showCustom:self.navigationController image:SCLAlertViewStyleKit.imageOfNotice color:[UIColor color_ff6722] title:@"提示" subTitle:@"匿名发优惠将无法返还积分，请选择操作：" closeButtonTitle:@"取消" duration:0.0f];
    [alert alertIsDismissed:^{
        if (!anonymousContinuePublish) {
            if (self.controllerOpener && [self.controllerOpener isKindOfClass:[UIViewController class]]) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{                
                //回到原来的页签
                UIViewController *rootController = [[[UIApplication sharedApplication] delegate] window].rootViewController;
                NSArray *children = rootController.childViewControllers;
                if ([children count] > 0) {
                    rootController = children[0];
                }
                if ([rootController isKindOfClass:[ZKHMainTabBarController class]]) {
                    [((ZKHMainTabBarController *)rootController) showPreViewController];
                }
            }
        }
        anonymousContinuePublish = false;
    }];
    
}

-(void)statusImgViewClick:(id)sender
{
    if (self.discount.status && [self.discount.status isEqualToString:VAL_DISCOUNT_STATUS_INVALID]) {
        [ZKHViewUtils showTipView:self.statusButton message:self.discount.auditIdea dismissTapAnywhere:YES autoDismissInvertal:-1];
    }
}

- (void) setDiscountEnable:(Boolean)enable
{
    self.contentTextView.editable = enable;
    self.publishBtn.enabled = enable;
    self.imageContainer.userInteractionEnabled = enable;
    if (!enable) {
        [self.publishBtn setTitleColor:[UIColor color_7f7f7f] forState:UIControlStateNormal];
        [self.publishBtn setBackgroundColor:[UIColor color_f5f5f5]];
        [self.publishBtn setBackgroundImage:nil forState:UIControlStateNormal];
    }
}

- (void) updateNavigation
{
    UINavigationItem *nav  = nil;
    if ([self.parentViewController isKindOfClass:[UITabBarController class]]) {
        nav = self.parentViewController.navigationItem;
    }else{
        nav = self.navigationItem;
    }
    nav.title = self.tabBarItem.title;
    nav.rightBarButtonItem = nil;
    nav.leftBarButtonItems = nil;
}

#pragma mark - collectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [selectedImages count] + [self.discount.images count] + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZKHPictureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    int row = indexPath.row;
    
    int discountImgCount = [self.discount.images count];
    int selectedImgCount = [selectedImages count];
    
    if (row == (discountImgCount + selectedImgCount)) {
        cell.imageView.image = [UIImage imageNamed:@"add_image.png" scale:2];
        cell.imageView.userInteractionEnabled = TRUE;
        [cell.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addPicViewClick:)]];
    }else{
        if (row < discountImgCount) {
            ZKHFileEntity *file = self.discount.images[row];
            [ZKHImageLoader showImageForName:file.aliasName imageView:cell.imageView toScale:2];
        }else{
            int index = row - discountImgCount;
            cell.imageView.image = [ZKHImageLoader scaleImage:selectedImages[index] toScale:CGSizeMake(kImageWidth, kImageHeight)];
        }
        
        cell.imageView.tag = row;
        cell.imageView.userInteractionEnabled = TRUE;
        [cell.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(discountImageClick:)]];
    }
    
    return cell;
}

- (void) discountImageClick:(UITapGestureRecognizer *)sender
{
    int row = sender.view.tag;
    
    int imagesCount = [self.discount.images count] + [selectedImages count];
    if (row < imagesCount) {
        ZKHImageListPreviewController *controller = [[ZKHImageListPreviewController alloc] init];
        
        NSMutableArray *imageFiles = [[NSMutableArray alloc] init];
        if (self.discount.images) {
            [imageFiles addObjectsFromArray:self.discount.images];
        }
        if (selectedImages) {
            [imageFiles addObjectsFromArray:selectedImages];
        }
        
        controller.currentIndex = row;
        controller.imageFiles = imageFiles;
        controller.readonly = false;
        controller.imageActionDelegate = self;
        
        showAnonymous = false;
        
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }
    
}

- (void)imageContainerClick:(id)sender
{
    [self hideKeyboard];
}

#pragma mark - ZKHImageAction delegate
- (void) doDeleteImage:(id)image index:(int)index
{
    int discountImgCount = [self.discount.images count];
    if (index < discountImgCount) {
        [self.discount.images removeObject:image];
        [deletedImageFiles addObject:image];
    }else{
        [selectedImages removeObject:image];
    }
}

- (void)addPicViewClick:(UITapGestureRecognizer *)sender
{
    [self hideKeyboard];
    showAnonymous = false;
    photoController = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHPhotoUploadController"];
    photoController.view.frame = CGRectMake(0, 0, 320, 113);
    photoController.delegate = self;
    [self.navigationController cm_presentPopupViewController:photoController animated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - photoChoose
- (void) doImageChoosed:(UIImage *)_selectedImage
{
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
    
    if (!selectedImages) {
        selectedImages = [[NSMutableArray alloc] init];
    }
    [selectedImages addObject:_selectedImage];
    [self.imageContainer reloadData];
}

- (void)doImageCanceled
{
    [self.navigationController cm_dismissPopupViewControllerAnimated:YES];
    [super updateNavigationDefault];
}

#pragma mark - ZKHNearbyStoreSelectedDelegate delegate
- (void)doSelectNearbyStore:(ZKHNearbyStoreEntity *)data
{
    nearbyStore = data;
    self.locationLabel.text = nearbyStore.name;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 2) {
        //画位置线条
        CGFloat offsetY = self.locationLabel.frame.origin.y + 20;
        [ZKHViewUtils drawSeparater:cell startPoint:CGPointMake(17.0f, offsetY) lineSize:CGSizeMake((320 - 34), 0.5f)];
    }
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *dest = segue.destinationViewController;
    if ([dest respondsToSelector:@selector(setNearbyStoreSelectedDelegate:)]) {
        showAnonymous = false;
        [dest setValue:self forKey:@"nearbyStoreSelectedDelegate"];
    }
    
}

- (void) hideKeyboard
{
    [self.contentTextView resignFirstResponder];
}

- (void) initDiscount
{
    self.discount = [[ZKHDiscountEntity alloc] init];
    self.discount.origin = VAL_DISCOUNT_ORIGIN_CUSTOMER;
    self.discount.type = VAL_DISCOUNT_TYPE_ACTIVITY;
    self.discount.startDate = [NSDate currentTimeString];
    self.discount.endDate = [NSDate currentTimeString];
    self.discount.parValue = @"0";
    self.discount.location = @"";
    self.discount.auditIdea = @"";
    
    ZKHCategoryEntity *cate = [[ZKHCategoryEntity alloc] init];
    cate.uuid = @"100";
    
    self.discount.category = cate;
    self.discount.images = [[NSMutableArray alloc] init];
    
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user) {
        self.discount.publisher = user.uuid;
    }else{
        self.discount.publisher = [ZKHViewUtils deviceId];
    }
    if(!self.discount.publisher){
        self.discount.publisher = [NSDate currentTimeString];
    }
}

- (void)clearData
{
    self.contentTextView.text = @"";
    self.locationLabel.text = @"";
    nearbyStore = nil;
    deletedImageFiles = [[NSMutableArray alloc] init];
    selectedImages = [[NSMutableArray alloc]init];
    [self initDiscount];
    
}
- (IBAction)publishBtnClick:(id)sender {
    if (!self.discount) {
        [self initDiscount];
    }
    
    self.discount.content = self.contentTextView.text;
    if (nearbyStore) {
        self.discount.location = [nearbyStore toJSONString];
    }
    
    int imagesCount = [self.discount.images count] + [selectedImages count];
    if (imagesCount == 0) {
        [ZKHViewUtils showTipView:self.imageContainer message:@"请上传活动图片" dismissTapAnywhere:YES autoDismissInvertal:-1];
        return;
    }
    
    if (self.discount.uuid) {//编辑发布
        NSMutableArray *addedImages = [self getImageFiles];
        
        [[ZKHProcessor getInstance] updateDiscount:self.discount addImages:addedImages deletedImages:deletedImageFiles deletedStorfronts:nil completionHandler:^(Boolean result) {
            if (result) {
                [self.view makeToast:@"发布成功."];
                if ([addedImages count] > 0) {
                    [self.discount.images addObjectsFromArray:addedImages];
                }
            }else{
                [self.view makeToast:@"发布失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
        
    }else{//新增发布
        self.discount.images = [self getImageFiles];
        [[ZKHProcessor getInstance] publishDiscount:self.discount completionHandler:^(Boolean result) {
            if (result) {
                [deletedImageFiles removeAllObjects];
                
                [self.view makeToast:@"发布成功."];
                if (!self.controllerOpener) {//清空数据
                    [self clearData];
                    showAnonymous = true;
                    [self.imageContainer reloadData];
                    [self.tableView reloadData];
                }
            }else{
                [self.view makeToast:@"发布失败."];
            }
        } errorHandler:^(ZKHErrorEntity *error) {
            
        }];
    }
}

- (NSMutableArray *) getImageFiles
{
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    NSMutableArray *files = [[NSMutableArray alloc] init];
    
    for (UIImage *image in selectedImages) {
        NSString *aliasName = [NSString stringWithFormat:@"%@_discount_%@.png", user.code, [NSDate currentTimeString]];
        NSString *filePath = [ZKHImageLoader saveImageData:[UIImage compressImageData:image compressRatio:0.9f] fileName:aliasName];
        
        ZKHFileEntity *file = [[ZKHFileEntity alloc] init];
        file.aliasName = aliasName;
        file.fileUrl = filePath;
        
        [files addObject:file];
    }
    return files;
}

- (void)deleteDiscount:(id)sender
{
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"确定" actionBlock:^(void) {
        if (!self.discount.uuid) {
            return;
        }
        
        [[ZKHProcessor getInstance] deleteDiscount:self.discount completionHandler:^(Boolean result) {
            if (result) {
                [self.view makeToast:@"删除成功."];
                if (self.controllerOpener && [self.controllerOpener isKindOfClass:[UIViewController class]])
                {
                    [self.navigationController popToViewController:self.controllerOpener animated:YES];
                }
            }else{
                [self.view makeToast:@"删除失败."];
            }
            } errorHandler:^(ZKHErrorEntity *error) {
        
            }];
    }];
    [alert showCustom:self.navigationController image:SCLAlertViewStyleKit.imageOfNotice color:[UIColor color_ff6722] title:@"提示" subTitle:@"是否删除优惠活动?" closeButtonTitle:@"取消" duration:0.0f];
}
@end

//
//  ZKHImageLoader.h
//  ZheKouHu
//
//  Created by undyliu on 14-5-23.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKNetworkOperation.h"
#import "ZKHProcessor.h"

@interface ZKHImageLoader : NSObject

+ (NSString*) saveImageData:(NSData *)data fileName:(NSString *) fileName;
+ (NSString*) saveImage:(UIImage *)image fileName:(NSString *) fileName;

+ (void) removeImageWithName:(NSString *)fileName;
+ (void) removeImageWithPath:(NSString *)filePath;

+ (UIImage *) loadImageLocal:(NSString *) fileName;
typedef void (^ImageResponseBlock)(UIImage* loadedImage);
+ (void) loadImageForName: (NSString *)fileName completionHandler:(ImageResponseBlock) imageBlock errorHandler:(RestResponseErrorBlock) errorBlock;

+ (void) showImageForName: (NSString *)fileName imageView:(UIImageView *)imageView;
+ (void) showImageForName: (NSString *)fileName imageView:(UIImageView *)imageView toScale:(CGFloat)scale;

+ (NSString *) getImageFilePath:(NSString *) fileName;

+ (UIImage *)scaleImage:(UIImage *)image toScale:(CGSize)reSize;

@end

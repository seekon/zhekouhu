//
//  ZKHImageLoader.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-23.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHImageLoader.h"
#import "ZKHConst.h"

#define KEY_IMAGES_DIR @"Images"

#define GET_IMAGE_FILE_URL(__FILE_NAME__) [NSString stringWithFormat:@"http://%@/%@/%@", SERVER_BASE_URL, @"getImageFile", __FILE_NAME__]

@implementation ZKHImageLoader

+ (NSString *) getImageFilePath:(NSString *) fileName
{
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *imageDir = [docDir stringByAppendingPathComponent:KEY_IMAGES_DIR];
    BOOL dirExist = [[NSFileManager defaultManager] fileExistsAtPath:imageDir];
    if (!dirExist) {
        BOOL result = [[NSFileManager defaultManager] createDirectoryAtPath:imageDir withIntermediateDirectories:YES attributes:nil error:nil];
        NSAssert(result, @"创建目录失败.");
    }
    
    return [imageDir stringByAppendingPathComponent:fileName];
}

+ (NSString*) saveImageData:(NSData *)data fileName:(NSString *) fileName
{
    //NSData *data = UIImageJPEGRepresentation(image, 1);
    //NSLog(@"%d", data.length);
    NSString *filePath = [ZKHImageLoader getImageFilePath:fileName];
    [data writeToFile:filePath options:NSAtomicWrite error:nil];
    return  filePath;
}

+ (NSString*) saveImage:(UIImage *)image fileName:(NSString *) fileName
{
    NSData *data = UIImageJPEGRepresentation(image, 1);
    return [[self class] saveImageData:data fileName:fileName];
}

+ (void)removeImageWithName:(NSString *)fileName
{
    [ZKHImageLoader removeImageWithPath:[ZKHImageLoader getImageFilePath:fileName]];
}

+ (void)removeImageWithPath:(NSString *)filePath
{
    BOOL result = [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    NSAssert(result, @"删除文件失败", filePath);
}

+ (UIImage *) loadImageLocal:(NSString *) fileName
{
    return [UIImage imageWithContentsOfFile:[ZKHImageLoader getImageFilePath:fileName]];
}

+ (void)loadImageForName:(NSString *)fileName completionHandler:(ImageResponseBlock)imageBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    UIImage *image = [ZKHImageLoader loadImageLocal:fileName];
    if (image != nil) {
        //NSLog(@"loaded image from local: %@", fileName);
        imageBlock(image);
        return;
    }
    
    NSLog(@"loading image from remote: %@", fileName);
    NSURL *imageUrl = [NSURL URLWithString: GET_IMAGE_FILE_URL(fileName)];    
    [[ZKHProcessor getInstance] imageAtURL:imageUrl completionHandler:^(UIImage *fetchedImage) {
        //NSLog(@"loaded image from remote: %@", fileName);
        [ZKHImageLoader saveImage:fetchedImage fileName:fileName];
        imageBlock(fetchedImage);
    } errorHandler:^(ZKHErrorEntity *error) {
        errorBlock(error);
    }];
}

+ (void) showImageForName: (NSString *)fileName imageView:(UIImageView *)imageView
{
    [ZKHImageLoader showImageForName:fileName imageView:imageView toScale:0];
}

+ (void)showImageForName:(NSString *)fileName imageView:(UIImageView *)imageView toScale:(CGFloat)scale
{
    [ZKHImageLoader loadImageForName:fileName completionHandler:^(UIImage *loadedImage) {
        if (scale == 0) {
            imageView.image = loadedImage;
        }else{
            imageView.image = [ZKHImageLoader scaleImage:loadedImage toScale:CGSizeMake(imageView.frame.size.width * scale, imageView.frame.size.height * scale)];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        NSLog(@"show image error :%@", error.message);
    }];
}

+ (UIImage*)scaleToSize:(UIImage*)image size:(CGSize)size
{
    struct CGImage *cgimg = [image CGImage];
    CGFloat width = CGImageGetWidth(cgimg);
    CGFloat height = CGImageGetHeight(cgimg);
    
    float verticalRadio = size.height*1.0/height;
    float horizontalRadio = size.width*1.0/width;
    
    float radio = 1;
    if(verticalRadio>1 && horizontalRadio>1)
    {
        radio = verticalRadio > horizontalRadio ? horizontalRadio : verticalRadio;
    }
    else
    {
        radio = verticalRadio < horizontalRadio ? horizontalRadio : verticalRadio;
    }
    if (radio > 1) {
        radio = 1;
    }
    width = width*radio;
    height = height*radio;
    
    int xPos = (size.width - width)/2;
    int yPos = (size.height-height)/2;
    
    // 创建一个bitmap的context
    // 并把它设置成为当前正在使用的context
    UIGraphicsBeginImageContext(size);
    
    // 绘制改变大小的图片
    [image drawInRect:CGRectMake(xPos, yPos, width, height)];
    
    // 从当前context中创建一个改变大小后的图片
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 使当前的context出堆栈
    UIGraphicsEndImageContext();
    
    // 返回新的改变大小后的图片
    return scaledImage;
}

+ (UIImage *)scaleImage:(UIImage *)image toScale:(CGSize)reSize
{
    CGSize size = CGSizeMake(reSize.width * 2, reSize.height * 2);
    UIImage *scaledImage = [self scaleToSize:image size:size];

    float drawW = 0.0;
    float drawH = 0.0;
    
    CGSize size_new = scaledImage.size;
    
    if (size_new.width > reSize.width) {
        drawW = (size_new.width - reSize.width)/2.0;
    }
    if (size_new.height > reSize.height) {
        drawH = (size_new.height - reSize.height)/2.0;
    }
    
    //截取截取大小为需要显示的大小。取图片中间位置截取
    CGRect myImageRect = CGRectMake(drawW, drawH, reSize.width, reSize.height);
    UIImage* bigImage= scaledImage;
    scaledImage = nil;
    CGImageRef imageRef = bigImage.CGImage;
    CGImageRef subImageRef = CGImageCreateWithImageInRect(imageRef, myImageRect);
    
    UIGraphicsBeginImageContext(reSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextDrawImage(context, myImageRect, subImageRef);
    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
    UIGraphicsEndImageContext();
    
    CGImageRelease(subImageRef);
    
    return smallImage;
}
@end

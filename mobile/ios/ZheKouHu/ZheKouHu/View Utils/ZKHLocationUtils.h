//
//  ZKHLocationUtils.h
//  ZheKouHu
//
//  Created by undyliu on 14-7-15.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZKHLocationUtils : UIViewController

+ (double) latlngDist:(double)lat1 lng1:(double)lng1 lat2:(double)lat2 lng2:(double)lng2;

+ (void) showLocationAlertView;

@end

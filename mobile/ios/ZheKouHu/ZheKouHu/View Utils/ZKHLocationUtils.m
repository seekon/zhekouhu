//
//  ZKHLocationUtils.m
//  ZheKouHu
//
//  Created by undyliu on 14-7-15.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <math.h>
#import "ZKHLocationUtils.h"
#import "UIView+Toast.h"

#define EARTH_RADIUS  6378137

@implementation ZKHLocationUtils

+ (double) latlngDist:(double)lat1 lng1:(double)lng1 lat2:(double)lat2 lng2:(double)lng2;
{
    if (lat1 == 0 || lng1 == 0 || lat2 == 0 || lng2 == 0) {
        return DBL_MAX;
    }
    
    double radLat1 = [ZKHLocationUtils translateLat:lat1];
    double radLon1 = [ZKHLocationUtils translateLng:lng1];
    double radLat2 = [ZKHLocationUtils translateLat:lat2];
    double radLon2 = [ZKHLocationUtils translateLng:lng2];
    
    double x1 = [ZKHLocationUtils latSinLngCos:radLat1 lng:radLon1];
    double y1 = [ZKHLocationUtils latSinLngSin:radLat1 lng:radLon1];
    double z1 = [ZKHLocationUtils latCos:radLat1];
    
    double x2 = [ZKHLocationUtils latSinLngCos:radLat2 lng:radLon2];
    double y2 = [ZKHLocationUtils latSinLngSin:radLat2 lng:radLon2];
    double z2 = [ZKHLocationUtils latCos:radLat2 ];
    
    double d = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2)
    * (z1 - z2);
    double theta = acos(1 - d/2);
    double dist = theta * EARTH_RADIUS;
    return dist;
}

+ (double) deg2rad:(double) deg
{
    return (deg * M_PI / 180.0);
}

+ (double) translateLat:(double)lat
{
    double radLat = [ZKHLocationUtils deg2rad:lat];
    if (radLat < 0)
        radLat = (M_PI/2 + fabs(radLat));// south
    if (radLat > 0)
        radLat = (M_PI/2 - fabs(radLat));// north
    return radLat;
}

+ (double) translateLng:(double)lng
{
    double radLng = [ZKHLocationUtils deg2rad:lng];
    if (radLng < 0)
        radLng = M_PI * 2 - fabs(radLng);// west
    return radLng;
}

+ (double) latSinLngCos:(double) lat lng:(double) lng
{
    return  sin(lat) * cos(lng);
}

+ (double) latSinLngSin:(double) lat lng:(double) lng
{
    return sin(lat) * sin(lng);
}

+ (double) latCos:(double) lat
{
    return  cos(lat);
}

+ (void) showLocationAlertView
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window makeToast:@"无法获取附近的促销信息，请开启折扣虎的定位服务." duration:5 position:@"bottom"];
}

@end

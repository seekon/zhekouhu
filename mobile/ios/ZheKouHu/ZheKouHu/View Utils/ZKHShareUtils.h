//
//  ZKHShareUtils.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-15.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZKHEntity.h"

@interface ZKHShareUtils : NSObject

+ (void)shareDiscountToSina:(ZKHDiscountEntity *)discount;

+ (void)shareDiscountToWeixin:(ZKHDiscountEntity *)discount;

+ (void)shareDiscountToFriends:(ZKHDiscountEntity *)discount;

@end

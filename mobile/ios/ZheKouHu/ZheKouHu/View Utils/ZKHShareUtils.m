//
//  ZKHShareUtils.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-15.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHShareUtils.h"
#import "WeiboSDK.h"
#import "NSDate+Utils.h"
#import "ZKHConst.h"
#import "WXApi.h"
#import "ZKHImageLoader.h"
#import "UIImage+ImageCompress.h"

@implementation ZKHShareUtils

+ (void)shareDiscountToSina:(ZKHDiscountEntity *)discount
{
    WBMessageObject *message = [WBMessageObject message];
    
    WBWebpageObject *webpage = [WBWebpageObject object];
    webpage.objectID = [NSDate currentTimeString];
    
    ZKHStorefrontEntity *front = discount.storefronts[0];
    if ([discount.type isEqualToString:VAL_DISCOUNT_TYPE_ACTIVITY]) {
        message.text = [NSString stringWithFormat:@"【%@】%@", @"分享促销活动", discount.content];
        webpage.title = [NSString stringWithFormat:@"%@-促销活动", front.storeName];
    }else{
        message.text = [NSString stringWithFormat:@"【%@】%@", @"分享优惠券", discount.content];
        webpage.title = [NSString stringWithFormat:@"%@-优惠券", front.storeName];
    }
    webpage.description = discount.content;
//    webpage.thumbnailData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"]];
    ZKHFileEntity *imageFile = discount.images[0];
    webpage.thumbnailData = [UIImage compressImageData: [ZKHImageLoader loadImageLocal:imageFile.aliasName] compressRatio:0.9];
    webpage.webpageUrl = [NSString stringWithFormat:@"http://%@/discount/%@", SERVER_BASE_URL, discount.uuid];
    message.mediaObject = webpage;
    
    WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:message];
    [WeiboSDK sendRequest:request];
}

+ (void)shareDiscountToWeixin:(ZKHDiscountEntity *)discount
{
    [[self class] shareDiscountToWeixin:discount scene:WXSceneSession];
}

+ (void)shareDiscountToFriends:(ZKHDiscountEntity *)discount
{
    [[self class] shareDiscountToWeixin:discount scene:WXSceneTimeline];
}

+ (void)shareDiscountToWeixin:(ZKHDiscountEntity *)discount scene:(NSInteger)_scene
{
    WXMediaMessage *message = [WXMediaMessage message];
    
    ZKHStorefrontEntity *front = discount.storefronts[0];
    if ([discount.type isEqualToString:VAL_DISCOUNT_TYPE_ACTIVITY]) {
        message.description = [NSString stringWithFormat:@"【%@】%@", @"分享促销活动", discount.content];
        message.title = [NSString stringWithFormat:@"%@-促销活动", front.storeName];
    }else{
        message.description = [NSString stringWithFormat:@"【%@】%@", @"分享优惠券", discount.content];
        message.title = [NSString stringWithFormat:@"%@-优惠券", front.storeName];
    }
//    message.thumbData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"]];
    ZKHFileEntity *imageFile = discount.images[0];
    message.thumbData = [UIImage compressImageData: [ZKHImageLoader loadImageLocal:imageFile.aliasName] compressRatio:0.9];
    message.mediaTagName = [NSDate currentTimeString];
    
    WXWebpageObject *ext = [WXWebpageObject object];
    ext.webpageUrl = [NSString stringWithFormat:@"http://%@/discount/%@", SERVER_BASE_URL, discount.uuid];
    
    message.mediaObject = ext;
    
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = _scene;
    
    [WXApi sendReq:req];
}
@end

//
//  ZKHViewUtils.h
//  ZheKouHu
//
//  Created by undyliu on 14-5-29.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>

#define k_navHeight 64
#define k_minPopTopHeight 40

@interface ZKHViewUtils : NSObject

+(CGFloat) screenWidth;

+(CGFloat) screenHeight;

+(CGFloat) screenScale;

+(void)setTableViewExtraCellLineHidden: (UITableView *)tableView;

+(void)showTipView:(UIView *)targetView message:(NSString *)message dismissTapAnywhere:(BOOL) dismissTapAnywhere autoDismissInvertal:(NSTimeInterval)autoDismissInvertal;

+(void)showTipView:(UIView *)targetView inView:(UIView *)inView message:(NSString *)message dismissTapAnywhere:(BOOL) dismissTapAnywhere autoDismissInvertal:(NSTimeInterval)autoDismissInvertal;

+(UIStoryboard *) mainStoryboard;

//+(void) drawLine:(UIView*)view startPoint:(CGPoint)startPoint endPoint:(CGPoint) endPoint;

+(void) drawStraightLine:(UIView *)view startPoint:(CGPoint)startPoint color:(UIColor*) color lineSize:(CGSize)lineSize;

+(void) drawSeparaterAtHeader:(UIView *)view offsetX:(CGFloat)offsetX;

+(void) drawSeparaterAtFooter:(UIView *)view offsetX:(CGFloat)offsetX;

+(void) drawSeparater:(UIView *)view startPoint:(CGPoint)startPoint lineSize:(CGSize)lineSize;

+(UIBarButtonItem *) deleteBarButtonItem;

+(NSString *) deviceId;

+(void) setTextFieldCursorColor:(UITextField *)field color:(UIColor *)color;

@end

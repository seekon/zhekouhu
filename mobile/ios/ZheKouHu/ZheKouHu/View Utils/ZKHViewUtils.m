//
//  ZKHViewUtils.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-29.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHViewUtils.h"
#import "CMPopTipView.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHConst.h"

@implementation ZKHViewUtils

+ (CGFloat)screenWidth
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
            return screenBounds.size.width;
    }else{
            return screenBounds.size.height;
    }
}

+ (CGFloat)screenHeight
{
    CGFloat statusBarHeight = 0;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (currentOrientation == UIInterfaceOrientationPortrait || currentOrientation == UIInterfaceOrientationPortraitUpsideDown){//竖屏
        return screenBounds.size.height - statusBarHeight;
    }else{
        return screenBounds.size.width - statusBarHeight;
    }
}

+ (CGFloat)screenScale
{
    return [UIScreen mainScreen].scale;
}

+ (void)setTableViewExtraCellLineHidden:(UITableView *)tableView
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

+ (void)showTipView:(UIView *)targetView message:(NSString *)message dismissTapAnywhere:(BOOL) dismissTapAnywhere autoDismissInvertal:(NSTimeInterval)autoDismissInvertal
{
    [ZKHViewUtils showTipView:targetView inView:targetView.superview message:message dismissTapAnywhere:dismissTapAnywhere autoDismissInvertal:autoDismissInvertal];
}

+ (void)showTipView:(UIView *)targetView inView:(UIView *)inView message:(NSString *)message dismissTapAnywhere:(BOOL)dismissTapAnywhere autoDismissInvertal:(NSTimeInterval)autoDismissInvertal
{
    CMPopTipView *popTipView = [[CMPopTipView alloc] initWithMessage:message];
    popTipView.animation = arc4random() % 2;
    popTipView.has3DStyle = FALSE;
    popTipView.dismissTapAnywhere = dismissTapAnywhere;
    if (autoDismissInvertal > 0) {
        [popTipView autoDismissAnimated:YES atTimeInterval:autoDismissInvertal];
    }
    
    [popTipView presentPointingAtView:targetView inView:inView animated:YES];
}

+ (UIStoryboard *)mainStoryboard
{
     return [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
}

+(void) drawLine:(UIView*)view startPoint:(CGPoint)startPoint endPoint:(CGPoint) endPoint;
{
    UIGraphicsBeginImageContext(view.frame.size);
//    CGContextRef ctx = UIGraphicsGetCurrentContext();//获取当前ctx
//    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
//    CGContextSetLineWidth(ctx, 1.0);  //线宽
//    CGContextSetAllowsAntialiasing(ctx, YES);
//    CGContextSetRGBStrokeColor(ctx, 1.0, 0.0, 0.0, 1.0);  //颜色
//    CGContextBeginPath(ctx);
//    CGContextMoveToPoint(ctx, startPoint.x, startPoint.y);  //起点坐标
//    CGContextAddLineToPoint(ctx, endPoint.x, endPoint.y);   //终点坐标
//    CGContextStrokePath(ctx);
    UIImageView * imageView=[[UIImageView alloc] initWithFrame:view.frame];
    [view addSubview:imageView];
        
    UIGraphicsBeginImageContext(imageView.frame.size);
    [imageView.image drawInRect:CGRectMake(0, 0, imageView.frame.size.width, imageView.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 1.0);
    CGContextSetAllowsAntialiasing(UIGraphicsGetCurrentContext(), YES);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 1.0, 1.0, 0.0, 1.0);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), startPoint.x, startPoint.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), endPoint.x, endPoint.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    imageView.image=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

+(void) drawStraightLine:(UIView *)view startPoint:(CGPoint)startPoint color:(UIColor*) color lineSize:(CGSize)lineSize
{
    UIImage *lineImg = [UIImage imageWithColor:color size:lineSize];
    UIImageView *line = [[UIImageView alloc] initWithImage:lineImg];
    line.frame = CGRectMake(startPoint.x, startPoint.y, lineSize.width, lineSize.height);
    [view addSubview:line];
}

+(void) drawSeparater:(UIView *)view startPoint:(CGPoint)startPoint lineSize:(CGSize)lineSize
{
    UIColor *lineColor = [UIColor color_e3e3e3];
    [ZKHViewUtils drawStraightLine:view startPoint:startPoint color:lineColor lineSize:lineSize];
}

+(void) drawSeparaterAtHeader:(UIView *)view offsetX:(CGFloat)offsetX
{
    CGFloat lineHeight = 0.5f;
    CGSize lineSize = CGSizeMake(view.frame.size.width - 2 * offsetX, lineHeight);
    [ZKHViewUtils drawSeparater:view startPoint:CGPointMake(view.frame.origin.x + offsetX, 0) lineSize:lineSize];
}

+(void) drawSeparaterAtFooter:(UIView *)view offsetX:(CGFloat)offsetX
{
    CGFloat lineHeight = 0.5f;
    CGSize lineSize = CGSizeMake(view.frame.size.width - 2 * offsetX, lineHeight);
    CGPoint sPoint = CGPointMake(view.frame.origin.x + offsetX, view.frame.size.height - lineHeight);
    [ZKHViewUtils drawSeparater:view startPoint:sPoint lineSize:lineSize];
}

+ (UIBarButtonItem *)deleteBarButtonItem
{
    UIButton *deleteButton = [[UIButton alloc] init];
    [deleteButton setImage:[UIImage imageNamed:@"delete.png" scale:2] forState:UIControlStateNormal];
    deleteButton.frame = CGRectMake(0, 0, 44, 44);
    [deleteButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:deleteButton.frame.size] forState:UIControlStateHighlighted];
    UIBarButtonItem *deleteBarItem = [[UIBarButtonItem alloc] initWithCustomView:deleteButton];
    return deleteBarItem;
}

+ (NSString *)deviceId
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:KEY_DEV_ID];
}

+ (void)setTextFieldCursorColor:(UITextField *)field color:(UIColor *)color
{
    if (IOS7) {
        field.tintColor = color;
        
    }else{
        [[field valueForKey:@"textInputTraits"] setValue:color forKey:@"insertionPointColor"];
    }
}
@end

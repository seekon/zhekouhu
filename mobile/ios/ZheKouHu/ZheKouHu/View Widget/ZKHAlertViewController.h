//
//  ZKHAlertViewController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-13.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZKHAlertViewDelegate <NSObject>
@optional
- (void)doOk;
- (void)doCancel;

@end

@interface ZKHAlertViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;

@property (strong, nonatomic) id<ZKHAlertViewDelegate> delegate;
@property (strong, nonatomic) NSString *alertTitle;
@property (strong, nonatomic) NSString *alertMessage;

- (IBAction)doCancel:(id)sender;
- (IBAction)doOk:(id)sender;

@end

//
//  ZKHAlertViewController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-13.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "ZKHAlertViewController.h"
#import "UIColor+Utils.h"

@interface ZKHAlertViewController ()

@end

@implementation ZKHAlertViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.cancelButton.layer.cornerRadius = 0;
    self.cancelButton.layer.masksToBounds = YES;
    self.cancelButton.layer.borderWidth = 0.5;
    self.cancelButton.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    
    self.okButton.layer.cornerRadius = 0;
    self.okButton.layer.masksToBounds = YES;
    self.okButton.layer.borderWidth = 0.5;
    self.okButton.layer.borderColor = [[UIColor color_e3e3e3] CGColor];
    
    if (self.alertTitle) {
        self.titleLabel.text = self.alertTitle;
    }
    self.messageLabel.text = self.alertMessage;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doCancel:(id)sender {
    if (self.delegate) {
        [self.delegate doCancel];
    }
}

- (IBAction)doOk:(id)sender {
    if (self.delegate) {
        [self.delegate doOk];
    }
}
@end

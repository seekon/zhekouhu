//
//  ZKHBackTableViewController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-3.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  ZKHEmptyTableBGView;

@interface ZKHBackTableViewController : UITableViewController
{
    ZKHEmptyTableBGView *backgroundView;
    
    UIBarButtonItem *negativeSpacer;
    UIBarButtonItem *backButtonItem;
}

@property Boolean dataLoaded;

- (void)back:(id)sender;
- (void)backgroundViewHidden:(Boolean)hidden;
- (void)backgroundImageViewHidden:(Boolean)hidden;
- (void)updateNavigationDefault;

@end

@interface ZKHEmptyTableBGView : UIView

@property (weak, nonatomic) IBOutlet UILabel *hintLabel;
@property (weak, nonatomic) IBOutlet UIImageView *hintImageView;

@end
//
//  ZKHBackTableViewController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-3.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHBackTableViewController.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHConst.h"

@implementation ZKHBackTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if(IOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
        
        if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.tableView setSeparatorInset:UIEdgeInsetsZero];
        }
    }
    negativeSpacer = [[UIBarButtonItem alloc]
                      initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                      target:nil action:nil];
    negativeSpacer.width = -8;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backButton setImage:[UIImage imageNamed:@"back.png" scale:2] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:backButton.frame.size] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    [self updateNavigationDefault];
    
    backgroundView = [[[NSBundle mainBundle] loadNibNamed:@"ZKHEmptyTableBGView" owner:self options:nil] lastObject];
    self.dataLoaded = false;
    backgroundView.hidden = true;
    [self.tableView setBackgroundView:backgroundView];
}

- (void)updateNavigationDefault
{
    self.navigationItem.leftBarButtonItems = [NSArray
                                              arrayWithObjects:negativeSpacer, backButtonItem, nil];
    
}

- (void)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)backgroundViewHidden:(Boolean)hidden
{
    backgroundView.hidden = hidden;
    if (!hidden) {
        if (self.dataLoaded) {
            backgroundView.hintLabel.text = @"暂时没有数据";
        }else{
            backgroundView.hintLabel.text = @"正在加载数据";
        }
    }
}

- (void)backgroundImageViewHidden:(Boolean)hidden
{
    backgroundView.hintImageView.hidden = hidden;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

@implementation ZKHEmptyTableBGView

@end
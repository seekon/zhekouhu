//
//  ZKHBackViewController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-3.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHBackViewController : UIViewController
{
    UIBarButtonItem *negativeSpacer;
    UIBarButtonItem *backButtonItem;
}

- (void)back:(id)sender;
- (void)updateNavigationDefault;

@end

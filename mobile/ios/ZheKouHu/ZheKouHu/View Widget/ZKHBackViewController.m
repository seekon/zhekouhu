//
//  ZKHBackViewController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-3.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHBackViewController.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHConst.h"
@implementation ZKHBackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    if(IOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    negativeSpacer = [[UIBarButtonItem alloc]
                      initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                      target:nil action:nil];
    negativeSpacer.width = -8;
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [backButton setImage:[UIImage imageNamed:@"back.png" scale:2] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageWithColor:[UIColor color_e45717] size:backButton.frame.size] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    [self updateNavigationDefault];
}

- (void)updateNavigationDefault
{
    self.navigationItem.leftBarButtonItems = [NSArray
                                              arrayWithObjects:negativeSpacer, backButtonItem, nil];

}

- (void)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  ZKHButton.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-3.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHButton.h"
#import "UIImage+Utils.h"

@implementation ZKHButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundImage:[UIImage imageNamed:@"bg_button_nor.png" scale:2] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"bg_button_sel.png" scale:2] forState:UIControlStateHighlighted];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

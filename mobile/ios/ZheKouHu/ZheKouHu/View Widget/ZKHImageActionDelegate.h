//
//  ZKHImageActionDelegate.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-23.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ZKHImageActionDelegate <NSObject>

@optional
- (void) doImageViewClicked:(UIImageView *)sender;

- (void) doDeleteImage:(id)image index:(int)index;

@end

//
//  ZKHSaleImageListController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZKHImageActionDelegate.h"
#import "ZKHBackViewController.h"

@interface ZKHImageListPreviewController : ZKHBackViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageController;
@property (strong, nonatomic) NSMutableArray *imageFiles;
@property (nonatomic) Boolean readonly;
@property int currentIndex;

@property (weak, nonatomic) id<ZKHImageActionDelegate> imageActionDelegate;

@end

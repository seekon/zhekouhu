//
//  ZKHSaleImageListController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-9.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHImageListPreviewController.h"
#import "ZKHImagePreviewController.h"
#import "ZKHViewUtils.h"

@implementation ZKHImageListPreviewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.readonly = true;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"图片浏览";
    
    if (!self.imageFiles || [self.imageFiles count] == 0) {
        return;
    }
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    if (!self.readonly) {
        UIBarButtonItem * deleteBarItem = [ZKHViewUtils deleteBarButtonItem];
        UIButton *deleteButton = (UIButton *)deleteBarItem.customView;
        [deleteButton addTarget:self action:@selector(deleteImageClick:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = deleteBarItem;
    }
    
    [self updatePageViewController];
}

- (void) updatePageViewController
{
    ZKHImagePreviewController *initialViewController = [self viewControllerAtIndex:self.currentIndex];
    if (initialViewController) {
        NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
        [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (ZKHImagePreviewController *)viewControllerAtIndex:(NSUInteger)index {
    ZKHImagePreviewController *childViewController = [[ZKHImagePreviewController alloc] initWithNibName:@"ZKHImagePreviewController" bundle:nil];
    
    if (!self.imageFiles || [self.imageFiles count] == 0) {
        self.navigationItem.rightBarButtonItem = nil;
        return childViewController;
    }
    
    id imageFile = self.imageFiles[index];
    if ([imageFile isKindOfClass:[ZKHFileEntity class]]) {
        childViewController.imageFile = imageFile;
    }else{
        childViewController.image = imageFile;
    }
    childViewController.index = index;
    
    return childViewController;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(ZKHImagePreviewController *)viewController index];
    if (index == 0) {
        self.currentIndex = 0;
        return nil;
    }
    
    index--;
    
    self.currentIndex--;
    
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
   NSUInteger index = [(ZKHImagePreviewController *)viewController index];
    
    index++;
    
    if (index == [self.imageFiles count]) {
        self.currentIndex = [self.imageFiles count] - 1;
        return nil;
    }
    
    self.currentIndex++;
    
    return [self viewControllerAtIndex:index];

}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return [self.imageFiles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return self.currentIndex;
}

- (void)deleteImageClick:(id)sender
{
    //NSLog(@"%d", self.currentIndex);
    
    id image = self.imageFiles[self.currentIndex];
    [self.imageFiles removeObject: image];
    
    if (self.imageActionDelegate) {
        [self.imageActionDelegate doDeleteImage:image index:self.currentIndex];
    }
    
    if ([self.imageFiles count] == 0) {
        [self back:nil];
        return;
    }
    
    if (self.currentIndex > 0) {
        self.currentIndex--;
    }
    [self updatePageViewController];
}

@end

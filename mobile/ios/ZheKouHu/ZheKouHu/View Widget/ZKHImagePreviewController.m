//
//  ZKHImagePreviewController.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHImagePreviewController.h"
#import "ZKHImageLoader.h"
#import "ZKHViewUtils.h"

@implementation ZKHImagePreviewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.readonly = true;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"图片预览";
    
    if (self.image != nil) {
        //self.imageView.image = [ZKHImageLoader scaleImage:self.image toScale:self.scaleSize];
        self.imageView.image = self.image;
    }else if (self.imageFile != nil) {
        [ZKHImageLoader showImageForName:self.imageFile.aliasName imageView:self.imageView toScale:self.scale];
    }
    
    self.imageView.userInteractionEnabled = true;
    [self.imageView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageClick:)]];
    
    if (!self.readonly) {
        UINavigationItem *navItem = self.navigationItem;
        navItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteImageClick:)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)deleteImageClick:(id)sender
{
    if ([self.images count] > 0) {
        if (self.image) {
            [self.images removeObject:self.image];
            self.image = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }else if (self.imageFile){
            [self.images removeObject:self.imageFile];
            self.imageFile = nil;
            [self.navigationController popViewControllerAnimated:YES];
        }
        
    }
}

- (void)imageClick:(UITapGestureRecognizer *)sender
{
    if (self.delegate) {
        UIImageView *imageView = (UIImageView *)sender.view;
        imageView.tag = self.index;
        [self.delegate doImageViewClicked:imageView];
    }
}

@end

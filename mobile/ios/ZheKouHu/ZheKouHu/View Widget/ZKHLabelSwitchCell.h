//
//  ZKHCategoryChooseCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-24.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHLabelSwitchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UISwitch *checkSwitch;

@end

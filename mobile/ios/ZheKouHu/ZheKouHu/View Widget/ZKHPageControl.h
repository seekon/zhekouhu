//
//  ZKHPageControl.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-4.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHPageControl : UIPageControl
{
    UIImage *activeImage;
    UIImage *inActiveIamge;
}

- (void)updateDots;
@end

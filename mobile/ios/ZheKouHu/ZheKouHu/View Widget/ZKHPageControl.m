//
//  ZKHPageControl.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-4.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHPageControl.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHConst.h"

@implementation ZKHPageControl

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        activeImage = [UIImage imageNamed:@"dot_on" scale:2];
        inActiveIamge = [UIImage imageNamed:@"dot_off" scale:2];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    long count = [self.subviews count];
    for (int i = 0; i < count; i++) {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        if (i == 0) {
            dot.backgroundColor = [UIColor color_ff6722];
        }else {
            dot.backgroundColor = [UIColor whiteColor];
        }
    }
}

- (void)setCurrentPage:(NSInteger)currentPage
{
    [super setCurrentPage:currentPage];
    [self updateDots];
}

- (void)updateDots{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIImageView* dot = [self.subviews objectAtIndex:i];
        if (i == self.currentPage){
            dot.backgroundColor = [UIColor color_ff6722];
        }else {
            dot.backgroundColor = [UIColor whiteColor];
        }
    }
}

@end

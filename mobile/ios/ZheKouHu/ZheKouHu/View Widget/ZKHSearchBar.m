//
//  ZKHSearchBar.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-8.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHSearchBar.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"

@implementation ZKHSearchBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    UITextField *searchField;
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UITextField class]]) {
            searchField = (UITextField *)view;
        }
    }
    
	if(!(searchField == nil)) {
		[[searchField valueForKey:@"textInputTraits"] setValue:[UIColor whiteColor] forKey:@"insertionPointColor"];
        
        [searchField setTextColor:[UIColor whiteColor]];
        [searchField setFont:[UIFont systemFontOfSize:12]];
       
        [searchField setBackground: [UIImage imageNamed:@"bg_searchbar"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
        
        UIImage *image = [UIImage imageNamed: @"search_icon" scale:2];
		UIImageView *iView = [[UIImageView alloc] initWithImage:image];
		searchField.leftView = iView;
        
        UIButton *clearButton = [searchField valueForKey:@"_clearButton"];
        [clearButton setImage:[UIImage imageNamed: @"close" scale:2] forState:UIControlStateNormal];
        [clearButton setImage:[UIImage imageNamed: @"close" scale:2] forState:UIControlStateHighlighted];
        
        searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"请输入关键字" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
	}
    
}

@end

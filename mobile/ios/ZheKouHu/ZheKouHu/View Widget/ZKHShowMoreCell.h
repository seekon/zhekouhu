//
//  ZKHShowMoreCell.h
//  ZheKouHu
//
//  Created by undyliu on 14-11-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHShowMoreCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *showAllLabel;
@property (weak, nonatomic) IBOutlet UIImageView *showAllImage;
@property (weak, nonatomic) IBOutlet UIView *cellView;

@end

//
//  ZKHTextField.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-2.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMPopTipView.h"

@interface ZKHTextFieldDeleagte : NSObject<UITextFieldDelegate>
{
    UIImage *bgTextfieldNor;
    UIImage *bgTextfieldSel;
}
@end

typedef enum {
    ValidationNone = 0,
    ValidationPassed,
    ValidationFailed
    
} ValidationResult;

typedef void (^ValidationBlock)(ValidationResult result, BOOL isEditing);


@interface ZKHTextField : UITextField
{
    UIImage *bgTextfieldNor;
    ZKHTextFieldDeleagte *textFieldDeleagte;
    UIImageView *successImgView;
    UIImageView *errorImgView;
    
    CMPopTipView *popTipView;
}

@property (nonatomic) Boolean showBg;

@property (nonatomic) CGFloat paddingLeft;
@property (nonatomic) CGFloat paddingRight;

#pragma mark - Basics

@property NSString *regexpPattern;

@property NSRegularExpression *regexp;

#pragma mark - Accessors
@property (readonly) BOOL isValid;

#pragma mark - Blocks
@property (readwrite, copy) ValidationBlock validatedFieldBlock;

#pragma mark - Settings

@property (getter = isValidWhenType) BOOL validWhenType;

@property (getter = isLooksForManyOccurences) BOOL looksForManyOccurences;

@property NSArray *occurencesSeparators;

@property NSUInteger minimalNumberOfCharactersToStartValidation;

@property NSUInteger maxNumberOfCharactersToStartValidation;

@property (nonatomic, strong) NSString *invalidPopText;

- (void)validate;

- (void)showTipView;

- (void) showErrorMessage:(NSString *)message;

- (ValidationResult)validRegexp;

@end



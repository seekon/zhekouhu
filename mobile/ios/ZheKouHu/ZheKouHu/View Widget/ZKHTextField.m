//
//  ZKHTextField.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-2.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHTextField.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "ZKHConst.h"
#import "ZKHViewUtils.h"

@interface ZKHTextField ()

@property (readonly) BOOL canValid;
@property UIColor *baseColor;
@property BOOL fieldHasBeenEdited;

@end

@implementation ZKHTextField
{
    ValidationResult _validationResult;
    NSString *_previousText;
}

@synthesize regexpPattern = _regexpPattern;
@synthesize looksForManyOccurences = _looksForManyOccurences;
@synthesize validWhenType = _validWhenType;
@synthesize minimalNumberOfCharactersToStartValidation = _minimalNumberOfCharactersToStartValidation;
@synthesize maxNumberOfCharactersToStartValidation = _maxNumberOfCharactersToStartValidation;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureForValidation];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self configureForValidation];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    bgTextfieldNor = [UIImage imageNamed:@"bg_textfield_nor.png"];
    [ZKHViewUtils setTextFieldCursorColor:self color:[UIColor color_ff6722]];
    
    [self setTextColor:[UIColor color_2d2d2d]];
    [self setFont:[UIFont systemFontOfSize:12]];
    
    _showBg = true;
    [self setBackground:bgTextfieldNor];
    
    textFieldDeleagte = [[ZKHTextFieldDeleagte alloc] init];
    self.delegate = textFieldDeleagte;
    
    self.paddingLeft = 12;
    self.paddingRight = 12;
}

- (void)setShowBg:(Boolean)showBg
{
    if (!showBg) {
        [self setBackground:nil];
        self.delegate = nil;
    }
     _showBg = showBg;
}

- (void)configureForValidation
{
    self.minimalNumberOfCharactersToStartValidation = 1;
    self.maxNumberOfCharactersToStartValidation = -1;
    self.validWhenType = YES;
    _fieldHasBeenEdited = NO;
    //_validationResult = ValidationFailed;
    _occurencesSeparators = nil;
    [self setRegexpPattern:@""];
    
    successImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success" scale:2]];
    errorImgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"warning" scale:2]];
    errorImgView.userInteractionEnabled = true;
    [errorImgView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showTipView)]];
}


#pragma mark - Lifecycle of validation
- (void)validateFieldWithIsEditing:(BOOL)isEditing {
    //if (!_previousText || ![_previousText isEqualToString:self.text])
    {
        _previousText = self.text;
        //if (self.text.length > 0 && !_fieldHasBeenEdited)
            _fieldHasBeenEdited = YES;
        
        if (_fieldHasBeenEdited)
        {
            [self willChangeValueForKey:@"isValid"];
            _validationResult = [self validRegexp];
            [self didChangeValueForKey:@"isValid"];

            [self updateViewForState:_validationResult];
                
            if (_validatedFieldBlock)
                _validatedFieldBlock(_validationResult, isEditing);
        }
    }
    
}


- (BOOL)isEditing
{
    BOOL isEditing = [super isEditing];
    if ((isEditing && self.validWhenType) ||
        (!isEditing && !self.validWhenType)) {
        [self validateFieldWithIsEditing:isEditing];
    }
    
    return isEditing;
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self validateFieldWithIsEditing:self.isEnabled];
}


#pragma mark - Accessors
- (BOOL)isValid
{
    if (_validationResult == ValidationPassed)
        return YES;
    else
        return NO;
}


- (BOOL)isLooksForManyOccurences
{
    return _looksForManyOccurences;
}

- (void)setLooksForManyOccurences:(BOOL)looksForManyOccurences
{
    _looksForManyOccurences = looksForManyOccurences;
}

- (BOOL)isValidWhenType
{
    return _validWhenType;
}

- (void)setValidWhenType:(BOOL)validWhenType
{
    _validWhenType = validWhenType;
}

- (void)setMinimalNumberOfCharactersToStartValidation:(NSUInteger)minimalNumberOfCharacterToStartValidation
{
    if (minimalNumberOfCharacterToStartValidation  < 1)
        minimalNumberOfCharacterToStartValidation = 1;
    _minimalNumberOfCharactersToStartValidation = minimalNumberOfCharacterToStartValidation;
}

- (NSUInteger)minimalNumberOfCharactersToStartValidation
{
    return _minimalNumberOfCharactersToStartValidation;
}

- (void)setMaxNumberOfCharactersToStartValidation:(NSUInteger)maxNumberOfCharacterToStartValidation
{
    _maxNumberOfCharactersToStartValidation = maxNumberOfCharacterToStartValidation;
}

- (NSUInteger)maxNumberOfCharactersToStartValidation
{
    return _maxNumberOfCharactersToStartValidation;
}

#pragma mark - Regexp Pattern accessors
- (void)setRegexpPattern:(NSString *)regexpPattern
{
    if (!regexpPattern){
        regexpPattern = @"";
    }
    _regexpPattern = regexpPattern;
    [self configureRegexpWithPattern:_regexpPattern];
}

- (NSString *)regexpPattern
{
    return _regexp.pattern;
}

- (void)configureRegexpWithPattern:(NSString *)pattern
{
    @try {
        _regexp = [[NSRegularExpression alloc] initWithPattern:pattern options:0 error:nil];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}


#pragma mark - Validation View Management
- (void)updateViewForState:(ValidationResult)result
{
    //UIImageView *imageView = (UIImageView *)self.rightView;
    //
    BOOL canShow = self.canValid;
    //imageView.hidden = !canShow;
    
    if (canShow)
    {
        if (result == ValidationPassed) {
            ////self.rightView = successImgView;
            self.rightView = nil;
        }else if(result == ValidationFailed){
            self.rightView = errorImgView;
        }else{
            self.rightView = nil;
        }
        
        if (popTipView) {
            [popTipView dismissAnimated:YES];
        }
        self.rightViewMode = UITextFieldViewModeAlways;
    }
}

- (BOOL)canValid
{
    return [_regexpPattern length ] > 0;
}

- (void)validate {
    [self validateFieldWithIsEditing:NO];
}

#pragma mark - Validation
- (ValidationResult)validRegexp
{
    NSString *text = self.text;
    ValidationResult valid = ValidationPassed;
    if (self.canValid)
    {
        if (!text) {
            valid = ValidationFailed;
        }else if([_regexpPattern isEqualToString:@"*"]){
            valid = ValidationPassed;
        }
        else{
        NSRange textRange = NSMakeRange(0, text.length);
        NSArray *matches = [_regexp matchesInString:text options:0 range:textRange];
        
        NSRange resultRange = NSMakeRange(NSNotFound, 0);
        if (matches.count == 1 && !_looksForManyOccurences)
        {
            NSTextCheckingResult *result = (NSTextCheckingResult *)matches[0];
            resultRange = result.range;
        }
        else if (matches.count != 0 && self.isLooksForManyOccurences)
        {
            resultRange = [self rangeFromTextCheckingResults:matches];
        }
        
        if (NSEqualRanges(textRange, resultRange)){
            valid = ValidationPassed;
        }
        else{
            valid = ValidationFailed;
        }
        }
    }
    
    if (valid == ValidationPassed) {
        long textLength = self.text.length;
        if (_maxNumberOfCharactersToStartValidation > 0 && textLength > _maxNumberOfCharactersToStartValidation) {
            valid = ValidationFailed;
        }
        if (_minimalNumberOfCharactersToStartValidation > 0 && textLength < _minimalNumberOfCharactersToStartValidation) {
            valid = ValidationFailed;
        }
    }

    return valid;
}

- (NSRange)rangeFromTextCheckingResults:(NSArray *)array
{
    /// Valid first match
    NSTextCheckingResult *firstResult = (NSTextCheckingResult *)array[0];
    if (!(firstResult.range.location == 0 && firstResult.range.length > 0))
        return NSMakeRange(NSNotFound, 0);
    
    
    /// Valid all matches
    NSInteger lastLocation = 0;
    
    if (array.count > 0)
    {
        for (NSTextCheckingResult *result in array)
        {
            if (lastLocation == result.range.location)
                lastLocation = result.range.location + result.range.length;
            else if (lastLocation < result.range.location)
            {
                NSString *stringInRange = [self.text substringWithRange:NSMakeRange(lastLocation, result.range.location - lastLocation)];
                
                BOOL separatorValid = NO;
                if (_occurencesSeparators)
                {
                    for (NSString *separator in _occurencesSeparators)
                    {
                        if ([stringInRange isEqualToString:separator])
                        {
                            lastLocation = result.range.location + result.range.length;
                            separatorValid = YES;
                            break;
                        }
                    }
                }
                
                if (separatorValid)
                    lastLocation = result.range.location + result.range.length;
                else
                    break;
            }
            else
                break;
        }
    }
    
    return NSMakeRange(0, lastLocation);
}

- (void)showTipView
{
    if (self.invalidPopText) {
        [self showTipView:self.invalidPopText targetView:self.rightView];
    }
    
}
- (void)showTipView:message targetView:(id) targetView
{
    if (popTipView.targetObject) {
        return;
    }
    if (!popTipView) {
        popTipView = [[CMPopTipView alloc] initWithMessage:message];
        popTipView.animation = arc4random() % 2;
        popTipView.has3DStyle = false;
        popTipView.hasShadow = false;
        //popTipView.dismissTapAnywhere = YES;
    }else{
        popTipView.message = message;
    }
    
    [popTipView presentPointingAtView:targetView inView:self.superview animated:YES];
}

- (void) showErrorMessage:(NSString *)message
{
    self.rightView = errorImgView;
    [self showTipView:message targetView:self];
}

- (BOOL)becomeFirstResponder
{
    if (popTipView) {
        [popTipView dismissAnimated:NO];
    }
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    if (popTipView) {
        [popTipView dismissAnimated:NO];
    }
    return [super resignFirstResponder];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGRect rect = [super textRectForBounds:bounds];
    rect.origin.x += self.paddingLeft;
    rect.size.width -= (self.paddingLeft + self.paddingRight);
    return rect;
}
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect rect = [super editingRectForBounds:bounds];
    rect.origin.x += self.paddingLeft;
    rect.size.width -= (self.paddingLeft + self.paddingRight);
    return rect;
}
@end

@implementation ZKHTextFieldDeleagte

- (id)init
{
    if (self = [super init]) {
        bgTextfieldSel = [UIImage imageNamed:@"bg_textfield_sel.png"];
        bgTextfieldNor = [UIImage imageNamed:@"bg_textfield_nor.png"];
    }
    return self;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField setBackground:bgTextfieldSel];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField setBackground:bgTextfieldNor];
    
}

@end
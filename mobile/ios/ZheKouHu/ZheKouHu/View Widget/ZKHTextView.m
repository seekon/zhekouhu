//
//  ZKHTextView.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-30.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "ZKHTextView.h"
#import "NSString+Utils.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"
#import "ZKHConst.h"

@implementation ZKHTextView

- (void)awakeFromNib

{
    [super awakeFromNib];
    
    self.delegate = self;
     
    [self setPlaceholder:@""];
    
    [self setPlaceholderColor:[UIColor lightGrayColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
    [self setTextColor:[UIColor color_2d2d2d]];
    [self setFont:[UIFont systemFontOfSize:12]];
    
    imageNormal = [UIImage imageNamed:@"bg_textfield_nor" scale:2];
    imageFocused = [UIImage imageNamed:@"bg_textfield_sel" scale:2];
    bgImageView = [[UIImageView alloc] initWithImage:imageNormal];
    [self addSubview:bgImageView];
    
    if (IOS7) {
        self.tintColor = [UIColor color_ff6722];
    }else{
        [[self valueForKey:@"textInputTraits"] setValue:[UIColor color_ff6722] forKey:@"insertionPointColor"];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setPlaceholder:@""];
        
        [self setPlaceholderColor:[UIColor lightGrayColor]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    }
    return self;
}

- (void)textChanged:(NSNotification *)notification
{
    if([[self placeholder] length] == 0)
    {
        return;
    }
    
    if([[self text] length] == 0)
    {
        [[self viewWithTag:999] setAlpha:1];
        //[self showTipView];
    }else
    {
        [[self viewWithTag:999] setAlpha:0];
        
        //[self hideTipView];
    }
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self textChanged:nil];
    
}

- (void)drawRect:(CGRect)rect
{
    //[self setBorderColor:[UIColor color_bbbbbb]];
    CGFloat height = 44;
    CGFloat y = rect.origin.y + rect.size.height - height;
    
    bgImageView.frame = CGRectMake(rect.origin.x, y, rect.size.width , height);
    
    if( [[self placeholder] length] > 0 )
    {
        if (placeHolderLabel == nil )
        {
            placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(8,8,self.bounds.size.width - 16,0)];
            placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
            placeHolderLabel.numberOfLines = 0;
            placeHolderLabel.font = self.font;
            placeHolderLabel.backgroundColor = [UIColor clearColor];
            placeHolderLabel.textColor = self.placeholderColor;
            placeHolderLabel.alpha = 0;
            placeHolderLabel.tag = 999;
            
            [self addSubview:placeHolderLabel];
            
        }
     
        placeHolderLabel.text = self.placeholder;
        
        [placeHolderLabel sizeToFit];
        
        [self sendSubviewToBack:placeHolderLabel];
        
    }
    
    if( [[self text] length] == 0 && [[self placeholder] length] > 0 )
    {
        [[self viewWithTag:999] setAlpha:1];
    }

    [super drawRect:rect];
}

- (BOOL)becomeFirstResponder
{
    [self hideTipView];
    return [super becomeFirstResponder];
}

- (void)hideTipView
{
    if (popTipView) {
        [popTipView dismissAnimated:YES];
    }
}

- (void)showTipView
{
    [self showTipView:self.popMessageWhenEmptyText];
}

- (void)showTipView:message
{
    if (popTipView.targetObject) {
        return;
    }
    if (!popTipView) {
        popTipView = [[CMPopTipView alloc] initWithMessage:message];
        popTipView.animation = arc4random() % 2;
        popTipView.has3DStyle = false;
        //popTipView.dismissTapAnywhere = YES;
    }else{
        popTipView.message = message;
    }
    
    [popTipView presentPointingAtView:self inView:self.superview animated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    bgImageView.image = imageFocused;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    bgImageView.image = imageNormal;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    //CGSize aa = textView.contentSize;
    if (textView.contentSize.height > textView.frame.size.height) {
        textView.text = [textView.text substringToIndex:[textView.text length]-1];
        return NO;
    }
    return YES;
}

@end

//
//  ZKHWebViewController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-26.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHWebViewController.h"
#import "ZKHConst.h"
#import "UIView+Toast.h"

@implementation ZKHWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.title) {
        self.navigationController.title = self.title;
    }
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    NSURLRequest *request = nil;
    if ([self.urlString hasPrefix:@"http://"] || [self.urlString hasPrefix:@"https://"]) {
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
    }else{
        NSString *url = [NSString stringWithFormat:@"http://%@%@", SERVER_BASE_URL, self.urlString];
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    }
    [self.webView loadRequest:request];
    self.webView.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    
    hud.labelText = @"正在加载..";
    //[hud show: YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [hud hide:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    //TODO:显示错误信息
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    if (window) {
        [window makeToast:@"连接远程服务器失败，请检查网络连接."];
    }
    
    [hud hide:YES];
}

@end

//
//  ZKHAppDelegate.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHAppDelegate.h"
#import "ZKHViewUtils.h"
#import "UIImage+Utils.h"
#import "UIColor+Utils.h"
#import "UIImage+Utils.h"
#import "ZKHConst.h"
#import "UIView+Toast.h"
#import "ZKHProcessor+Setting.h"
#import "ZKHProcessor+Message.h"
#import "NSString+Utils.h"
#import "ZKHEntity.h"
#import "ZKHMessageDetailController.h"
#import "ZKHContext.h"

@implementation ZKHAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    //self.window.backgroundColor = [UIColor whiteColor];
    //[self.window makeKeyAndVisible];
    
    //注册新浪微博
    //[WeiboSDK enableDebugMode:YES];
    [WeiboSDK registerApp:kShare_weibo_appKey];
    
    //注册微信
    [WXApi registerApp:kShare_weixin_appKey];
    
    //[NSThread sleepForTimeInterval:2];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"everLaunched"]) {
        UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHUserGuideController"];
        self.window.rootViewController = controller;
    }
    
    [self initGobalStyle];
    
    [self registerApnsWithOptions:launchOptions];
    
    return YES;
}

- (void) initGobalStyle
{
    //self.window.backgroundColor=[UIColor color_ff6722];
    self.window.alpha = 1;
    [self.window setOpaque:FALSE];
    
    CGFloat scale = [ZKHViewUtils screenScale];
    CGFloat sWidth = [ZKHViewUtils screenWidth];
    CGFloat sHeight = [ZKHViewUtils screenHeight];
    
    CGFloat navBarHeight = 88/scale;
    [[UINavigationBar appearance] setFrame:CGRectMake(0, 0, sWidth, navBarHeight)];

    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor,[UIFont systemFontOfSize:18], UITextAttributeFont, [UIColor clearColor], UITextAttributeTextShadowColor, nil]];
    
    if ([UINavigationBar instancesRespondToSelector:@selector(setShadowImage:)])
    { 
        [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(320, 3)]];
    }
    if (IOS7) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
    
    CGFloat tabBarHeight = 98/scale;
    [[UITabBar appearance] setFrame:CGRectMake(0, sHeight - tabBarHeight, sWidth, tabBarHeight)];
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor color_7f7f7f], UITextAttributeTextColor,
                                                       [UIFont systemFontOfSize:11], UITextAttributeFont, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor color_ff6722], UITextAttributeTextColor,
                                                       [UIFont systemFontOfSize:11], UITextAttributeFont, nil] forState:UIControlStateSelected];
    
    [[UITableView appearance] setSeparatorColor:[UIColor color_e3e3e3]];
    [[UITableView appearance] setBackgroundColor:[UIColor color_f5f5f5]];
    //[[UITableViewCell appearance] setSelectionStyle:UITableViewCellSelectionStyleGray];
    //[[UITableViewCell appearance] setSelectedImageTintColor:[UIColor color_7f7f7f]];
    
    [[UITextField appearance] setBorderStyle:UITextBorderStyleNone];

    [[UISearchBar appearance] setTintColor:[UIColor color_ff6722]];
}

- (void)registerApnsWithOptions:(NSDictionary *)launchOptions
{
    //判断是否由远程消息通知触发应用程序启动
    if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]!=nil) {
        //获取应用程序消息通知标记数（即小红圈中的数字）
        long badge = [UIApplication sharedApplication].applicationIconBadgeNumber;
        if (badge > 0) {
            //如果应用程序消息通知标记数（即小红圈中的数字）大于0，清除标记。
            badge --;
            //清除标记。清除小红圈中数字，小红圈中数字为0，小红圈才会消除。
            [UIApplication sharedApplication].applicationIconBadgeNumber = badge;
        }
        NSDictionary * userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if(userInfo)
        {
            [self launchToMessageDetailView:userInfo];
        }
    }
    
    //消息推送注册
    UIApplication *application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge];
    }
    
}

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
    if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class])
    {
        //ProvideMessageForWeiboViewController *controller = [[[ProvideMessageForWeiboViewController alloc] init] autorelease];
        //[self.viewController presentModalViewController:controller animated:YES];
    }
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
        if (response.statusCode == WeiboSDKResponseStatusCodeSuccess) {
            [self.window makeToast:@"分享成功."];
        }else if(response.statusCode == WeiboSDKResponseStatusCodeUserCancel
                 || response.statusCode == WeiboSDKResponseStatusCodeUserCancelInstall){
            ;//nothing
        }else if(response.statusCode == WeiboSDKResponseStatusCodeUnsupport){
            [self.window makeToast:@"此版本新浪微博客户端不支持分享."];
        }else{
            [self.window makeToast:@"分享失败."];
        }
//        NSString *title = @"发送结果";
//        NSString *message = [NSString stringWithFormat:@"响应状态: %d\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode, response.userInfo, response.requestUserInfo];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                        message:message
//                                                       delegate:nil
//                                              cancelButtonTitle:@"确定"
//                                              otherButtonTitles:nil];
//        [alert show];
//        //[alert release];
    }
//    else if ([response isKindOfClass:WBAuthorizeResponse.class])
//    {
//        NSString *title = @"认证结果";
//        NSString *message = [NSString stringWithFormat:@"响应状态: %d\nresponse.userId: %@\nresponse.accessToken: %@\n响应UserInfo数据: %@\n原请求UserInfo数据: %@",(int)response.statusCode,[(WBAuthorizeResponse *)response userID], [(WBAuthorizeResponse *)response accessToken], response.userInfo, response.requestUserInfo];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                        message:message
//                                                       delegate:nil
//                                              cancelButtonTitle:@"确定"
//                                              otherButtonTitles:nil];
//        
//        //self.wbtoken = [(WBAuthorizeResponse *)response accessToken];
//        
//        [alert show];
//        //[alert release];
//    }
}

-(void) onReq:(BaseReq*)req
{
//    if([req isKindOfClass:[GetMessageFromWXReq class]])
//    {
//        // 微信请求App提供内容， 需要app提供内容后使用sendRsp返回
//        NSString *strTitle = [NSString stringWithFormat:@"微信请求App提供内容"];
//        NSString *strMsg = @"微信请求App提供内容，App要调用sendResp:GetMessageFromWXResp返回给微信";
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        alert.tag = 1000;
//        [alert show];
//    }
//    else if([req isKindOfClass:[ShowMessageFromWXReq class]])
//    {
//        ShowMessageFromWXReq* temp = (ShowMessageFromWXReq*)req;
//        WXMediaMessage *msg = temp.message;
//        
//        //显示微信传过来的内容
//        WXAppExtendObject *obj = msg.mediaObject;
//        
//        NSString *strTitle = [NSString stringWithFormat:@"微信请求App显示内容"];
//        NSString *strMsg = [NSString stringWithFormat:@"标题：%@ \n内容：%@ \n附带信息：%@ \n缩略图:%u bytes\n\n", msg.title, msg.description, obj.extInfo, msg.thumbData.length];
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    else if([req isKindOfClass:[LaunchFromWXReq class]])
//    {
//        //从微信启动App
//        NSString *strTitle = [NSString stringWithFormat:@"从微信启动"];
//        NSString *strMsg = @"这是从微信启动的消息";
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//    }
}

-(void) onResp:(BaseResp*)resp
{
    if([resp isKindOfClass:[SendMessageToWXResp class]])
    {
//        NSString *strTitle = [NSString stringWithFormat:@"发送媒体消息结果"];
//        NSString *strMsg = [NSString stringWithFormat:@"errcode:%d", resp.errCode];
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle message:strMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
        int errCode = resp.errCode;
        if (errCode == 0) {
            [self.window makeToast:@"分享成功."];
        }else if(errCode != -2){
            [self.window makeToast:@"分享失败."];
        }
    }
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    [WXApi handleOpenURL:url delegate:self];
    return [WeiboSDK handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [WXApi handleOpenURL:url delegate:self];
    return [WeiboSDK handleOpenURL:url delegate:self];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ZheKouHu" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ZheKouHu.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark apns

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    if (token) {
        if ([token hasPrefix:@"<"]) {
            token = [token substringFromIndex:1];
        }
        if ([token hasSuffix:@">"]) {
            token = [token substringToIndex:token.length - 1];
        }
        token = [token trimAll];
    }
    NSLog(@"My token is:%@", token);
    
    if (token && ![[NSUserDefaults standardUserDefaults] valueForKey:KEY_DEV_ID]) {
        [ZKHContext getInstance].deviceId = token;
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSString *error_str = [NSString stringWithFormat: @"%@", error];
    NSLog(@"Failed to get token, error:%@", error_str);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //在此处理接收到的消息。
    //[self launchToMessageDetailView:userInfo];
    
    NSLog(@"Receive remote notification : %@",userInfo);
    ZKHMessageEntity *msg = [[ZKHMessageEntity alloc] init];
    msg.uuid = userInfo[KEY_UUID];
    msg.sendTime = userInfo[KEY_SEND_TIME];
    msg.type = userInfo[KEY_TYPE];
    msg.msgId = userInfo[KEY_MSG_ID];
    msg.content = userInfo[@"aps"][@"alert"];
    
    [[ZKHProcessor getInstance] saveMessges:[@[msg] mutableCopy]];
}

- (void) launchToMessageDetailView:(NSDictionary *)userInfo
{
    NSLog(@"Receive remote notification : %@",userInfo);
    ZKHMessageEntity *msg = [[ZKHMessageEntity alloc] init];
    msg.uuid = userInfo[KEY_UUID];
    msg.sendTime = userInfo[KEY_SEND_TIME];
    msg.type = userInfo[KEY_TYPE];
    msg.msgId = userInfo[KEY_MSG_ID];
    msg.content = userInfo[@"aps"][@"alert"];
    
    [[ZKHProcessor getInstance] saveMessges:[@[msg] mutableCopy]];
    
    ZKHMessageDetailController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHMessageDetailController"];
    controller.message = msg;
    //controller.popToRoot = true;
    UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
    [navController pushViewController:controller animated:YES];
}
@end

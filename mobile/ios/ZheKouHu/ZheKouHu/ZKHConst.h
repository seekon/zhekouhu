//
//  ZKHConst.h
//  ZheKouHu
//
//  Created by undyliu on 14-5-20.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#ifndef ZheKouHu_ZKHConst_h
#define ZheKouHu_ZKHConst_h

#define SERVER_BASE_URL  @"www.zhekouhu.com"
//#define SERVER_BASE_URL  @"127.0.0.1:3000"
//#define SERVER_BASE_URL  @"192.168.1.102:3000"
#define KEY_CACHES @"Caches"

#define KEY_DOWNLOAD_URL @"https://itunes.apple.com/cn/app/zhe-kou-hu/id917148354?mt=8"

#define METHOD_GET @"GET"
#define METHOD_POST @"POST"
#define METHOD_PUT @"PUT"
#define METHOD_DELETE @"DELETE"

#define KEY_UUID                @"uuid"
#define KEY_CODE                @"code"
#define KEY_NAME                @"name"
#define KEY_ICON                @"icon"
#define KEY_TYPE                @"type"
#define KEY_ORD_INDEX           @"ord_index"
#define KEY_PARENT_ID           @"parent_id"
#define KEY_PWD                 @"pwd"
#define KEY_LAST_MODIFY_TIME    @"last_modify_time"
#define KEY_REGISTER_TIME       @"register_time"
#define KEY_IMG                 @"img"
#define KEY_VALUE               @"value"
#define KEY_USER_ID             @"user_id"
#define KEY_UPDATE_TIME         @"update_time"
#define KEY_TABLE_NAME          @"table_name"
#define KEY_ITEM_ID             @"item_id"
#define KEY_CONTENT @"content"
#define KEY_START_DATE @"start_date"
#define KEY_END_DATE @"end_date"
#define KEY_VISIT_COUNT @"visit_count"
#define KEY_LOCATION @"location"
#define KEY_PUBLISHER @"publisher"
#define KEY_PUBLISH_TIME @"publish_time"
#define KEY_PUBLISH_DATE @"publish_date"
#define KEY_STATUS @"status"
#define KEY_ADDR @"addr"
#define KEY_LOGO @"logo"
#define KEY_OWNER @"owner"
#define KEY_LATITUDE @"latitude"
#define KEY_LONGITUDE @"longitude"
#define KEY_STORE_ID @"store_id"
#define KEY_STORE_NAME @"store_name"
#define KEY_PHONE @"phone"
#define KEY_CATEGORY_ID @"category_id"
#define KEY_DISCOUNT_ID @"discount_id"
#define KEY_STOREFRONT_ID @"storefront_id"
#define KEY_FIRST_LETTER @"first_letter"
#define KEY_CITY @"city"
#define KEY_CITY_NAME @"city_name"
#define KEY_PAR_VALUE @"par_value"
#define KEY_SEND_TIME @"send_time"
#define KEY_OLD_PWD @"old_pwd"
#define KEY_DEV_ID @"dev_id"
#define KEY_MSG_ID @"msg_id"

#define KEY_CONTACT @"contact"
#define KEY_IS_DELETED @"is_deleted"
#define KEY_STORES @"stores"
#define KEY_DISCOUNTS @"discounts"
#define KEY_DISCOUNT_STOREFRONTS @"discount_storefronts"
#define KEY_DATA @"data"
#define KEY_IMAGES @"images"
#define KEY_STOREFRONTS @"storefronts"
#define KEY_DEL_IMAGES @"del_images"
#define KEY_DEL_STOREFRONTS @"del_storefronts"
#define KEY_USER @"user"
#define KEY_AUTHED @"authed"
#define KEY_ERROR_TYPE @"error-type"

#define KEY_URL @"url"
#define KET_SET_COOKIE @"Set-Cookie"
#define KEY_FILE_NAME_LIST @"fileNameList"

#define VAL_ERROR_TYPE_NETWORK @"error-network"
#define VAL_ERROR_TYPE_BUSINESS @"error-business"
#define VAL_ERROR_TYPE_RUNTIME @"error-runtime"

#define VAL_DISCOUNT_TYPE_ACTIVITY @"0"
#define VAL_DISCOUNT_TYPE_COUPON @"1"
#define VAL_DISCOUNT_TYPE_ALL @"2"

#define DEFAULT_PAGE_SIZE 15
#define DEFAULT_DISCOUNT_FRONTS_NUM 3
#define DEFAULT_DISCOUNT_COMMENTS_NUM 3

#define kAuth_user_error @"user-error"
#define kAuth_pass_error @"pass-error"

#define KEY_SETTING_STORE_REGISTERED  @"store_registered"
#define KEY_SETTING_MESSAGE_REMIND  @"message_remind"
#define KEY_SETTING_VERSION  @"version"

#define SYNC_TABLE_COUPON_MAIN  @"COUPON_MAIN"
#define SYNC_TABLE_STORE_COUPON  @"STORE_COUPON"
#define SYNC_TABLE_FAVORIT_DISCOUNT @"FAVORIT_DISCOUNT"
#define SYNC_TABLE_ACTIVITIES_BY_OWNER @"ACTIVITIES_BY_OWNER"
#define SYNC_TABLE_COUPONS_BY_OWNER @"COUPONS_BY_OWNER"
#define SYNC_TABLE_DISCOUNTS_BY_OWNER @"DISCOUNTS_BY_OWNER"

#define KEY_STOREFRONT @"storefront"
#define KEY_COMMENT_COUNT @"commentCount"
#define KEY_FRONT_COUNT @"frontCount"
#define KEY_DISTANCE @"distance"
#define KEY_KEY_ID @"key_id"
#define KEY_PROFILE @"profile"
#define KEY_FAVORIT @"favorit"
#define KEY_DISCOUNT @"discount"
#define KEY_COMMENTS @"comments"
#define KEY_COMMENT @"comment"

#define KEY_SEX @"sex"
#define KEY_PHOTO @"photo"
#define KEY_DELIVER_ADDR @"deliver_addr"
#define KEY_AWARD_SUM @"award_sum"
#define KEY_AWARD_USED @"award_used"
#define KEY_ORIGIN @"origin"
#define KEY_AUDIT_IDEA @"audit_idea"
#define KEY_REMAIN @"remain"
#define KEY_COIN_VAL @"coin_val"
#define KEY_DESC @"desc"
#define KEY_DATA_ID @"data_id"
#define KEY_AWARD_TIME @"award_time"

#define KEY_VERSION 110

#define KEY_TYPE_COMMENT @"0"
#define KEY_TYPE_FAULT @"1"

#define KEY_ACTION_FAVORIT @"0"
#define KEY_ACTION_UNFAVORIT @"1"

#define VAL_USER_TYPE_STORER @"0"
#define VAL_USER_TYPE_CUSTOMER @"1"

#define VAL_DISCOUNT_ORIGIN_STORER  @"0"
#define VAL_DISCOUNT_ORIGIN_CUSTOMER  @"1"
#define TEXT_DISCOUNT_ORIGIN_STORER @"商家发布"
#define TEXT_DISCOUNT_ORIGIN_CUSTOMER @"网友提供"

#define VAL_DISCOUNT_STATUS_CHECHING  @"2"
#define VAL_DISCOUNT_STATUS_VALID  @"1"
#define VAL_DISCOUNT_STATUS_INVALID  @"8"
#define VAL_DISCOUNT_STATUS_DELETED  @"9"

#define DISCOUNT_ORIGIN_DEFAULT 0
#define DISCOUNT_ORIGIN_FAVORIT 1

#define MSG_TYPE_SYSTEM  @"1"
#define MSG_TYPE_VERSION  @"2"
#define MSG_TYPE_DISCOUNT  @"3"

#define kShare_weibo_appKey @"961906835"
#define kShare_weibo_redirectURI @"https://api.weibo.com/oauth2/default.html"

#define kShare_weixin_appKey @"wx0948279726287e6f"

#define IOS7 [[[UIDevice currentDevice]systemVersion] floatValue] >= 7.0

#endif

//
//  ZKHContext.h
//  ZheKouHu
//
//  Created by undyliu on 14-5-21.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZKHEntity.h"

@interface ZKHContext : NSObject

+ (ZKHContext *) getInstance;

@property (nonatomic) ZKHUserEntity *user;
@property NSString *sessionId;
@property ZKHLocation *location;
@property NSString *deviceId;

@end

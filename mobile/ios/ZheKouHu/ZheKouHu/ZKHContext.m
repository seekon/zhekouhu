//
//  ZKHContext.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-21.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHContext.h"
#import "ZKHConst.h"
#import "ZKHProcessor+User.h"

static ZKHContext *instance;

@implementation ZKHContext

+ (ZKHContext *)getInstance
{
    if (instance == nil) {
        instance = [[ZKHContext alloc] init];
    }
    return instance;
}

- (ZKHUserEntity *)user
{
    if (!_user) {
        _user = [[ZKHProcessor getInstance] localLogin];
    }
    return _user;
}
@end

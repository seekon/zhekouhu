//
//  ZKHEntity.h
//  ZheKouHu
//
//  Created by undyliu on 14-5-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZKHEntity : NSObject
@property  NSString *uuid;
@end

//错误
@interface ZKHErrorEntity : NSObject
@property  NSString *type;
@property  NSString *message;
@end

//文件
@interface ZKHFileEntity : NSObject<NSCopying>
@property (nonatomic) NSString *fileUrl;
@property  NSString *aliasName;
@end

//位置
@interface ZKHLocation : NSObject
@property NSString *cityName;
@property double lat;
@property double lng;
@end

//设置
@interface ZKHSettingEntity : ZKHEntity
@property NSString *value;
@end

//消息
@interface ZKHMessageEntity : ZKHEntity
@property NSString *msgId;
@property NSString *content;
@property NSString *type;
@property NSString *sendTime;
@end


//分类
@interface ZKHCategoryEntity : ZKHEntity<NSCopying>
@property  NSString *name;
@property  NSString *icon;
@property  NSString *parentId;
@property  int ordIndex;
@property  Boolean isDeleted;
@end

//城市
@interface ZKHCityEntity : ZKHEntity
@property NSString *name;
@property NSString *firstLetter;

- (ZKHCityEntity *)initWithJSONObject:(id)jsonCity;
@end

//用户基本信息
@interface ZKHUserProfileEntity : ZKHEntity<NSCopying>
@property  NSString *userId;
@property  NSString *name;
@property ZKHFileEntity *photo;
@property  NSString *sex;
@property  NSString *phone;
@property  NSString *devilerAddr;
@property  NSString *awardSum;
@property  NSString *awardUsed;

- (id) initWithJsonObject:(id)json;
@end

//用户
@interface ZKHUserEntity : ZKHEntity<NSCopying>
@property  NSString *code;
@property  NSString *pwd;
@property  NSString *registerTime;
@property  NSString *type;
@property ZKHUserProfileEntity *profile;

- (id) initWithJsonObject:(id)userJson noPwd:(Boolean)noPwd;

@end

//活动
@interface ZKHDiscountEntity : ZKHEntity
@property  NSString *content;
@property  NSString *startDate;
@property  NSString *endDate;
@property  int visitCount;
@property  NSString *publisher;
@property  NSString *publishTime;
@property  NSString *publishDate;
//@property  NSString *status;
@property  NSString *type;
@property  NSString *parValue;
@property  ZKHCategoryEntity *category;
@property  NSMutableArray *storefronts;
@property  NSMutableArray *images;
@property int frontCount;
@property int commentCount;
@property NSMutableArray *comments;
@property NSString *origin;
@property NSString *status;
@property NSString *location;
@property NSString *auditIdea;

- (ZKHDiscountEntity *) initWithJSONObject:(id)jsonDiscount;

@end

//店铺
@interface ZKHStoreEntity : ZKHEntity
@property  NSString *name;
@property  ZKHFileEntity *logo;
@property  NSString *registerTime;
@property  NSString *owner;
@property NSMutableArray *storefronts;

- (ZKHStoreEntity *) initWithJSONObject:(id)jsonStore;

@end

//门店
@interface ZKHStorefrontEntity : ZKHEntity<NSCopying>
@property  NSString *name;
@property  NSString *addr;
@property  NSString *phone;
@property  NSString *storeId;
@property  NSString *storeName;
@property  NSString *latitude;
@property  NSString *longitude;
@property ZKHCityEntity *city;
@property double distance;

- (ZKHStorefrontEntity *) initWithJSONObject:(id)jsonFront;

@end

@interface ZKHDiscountImageEntity : ZKHFileEntity
@property NSString *discountId;
@property NSString *uuid;
@property NSString *ordIndex;

- (ZKHDiscountImageEntity *) initWithJSONObject:(id)jsonImage;

@end

@interface ZKHDiscountStorefrontEntity : ZKHStorefrontEntity
@property NSString *discountId;
@property NSString *keyId;
@property NSString *ordIndex;

- (ZKHDiscountStorefrontEntity *) initWithJSONObject:(id)jsonFront;
- (ZKHDiscountStorefrontEntity *) initWithJSONObject:(id)jsonFront initSuper:(Boolean)initSuper;
@end

//优惠券店铺
@interface ZKHCouponStoreEntity : ZKHEntity
@property NSString *storeId;
@property NSString *categoryId;
@property NSString *name;
@property ZKHFileEntity *logo;
@property NSString *cityName;

- (ZKHCouponStoreEntity *)initWithJSONObject:(id)jsonFront;
@end

//本地与远程同步记录
@interface ZKHSyncEntity : ZKHEntity
@property  NSString *tableName;
@property  NSString *itemId;
@property  NSString *updateTime;
@end

@interface ZKHDateIndexedEntity : ZKHEntity
@property  NSDate *date;
@property (nonatomic) int count;
@property  NSMutableArray *items;
@end

//评论
@interface ZKHCommentEntity :ZKHEntity
@property  NSString *discountId;
@property  NSString *content;
@property  NSString *publisher;
@property  NSString *publishTime;
@property  NSString *type;

- (ZKHCommentEntity *)initWithJSONObject:(id)json;
@end

//收藏
@interface ZKHFavoritEntity : ZKHEntity
@property NSString *userId;
@property ZKHDiscountEntity *discount;
@end

//积分商品
@interface ZKHGoodsEntity : ZKHEntity
@property NSString *name;
@property NSString *desc;
@property NSString *img;
@property int coinVal;
@property int remain;

- (ZKHGoodsEntity *)initWithJSONObject:(id)json;
@end

//用户积分
@interface ZKHUserAwardEntity : ZKHEntity
@property NSString *userId;
@property NSString *dataId;
@property NSString *awardTime;
@property int coinVal;
@property NSString *comment;

- (ZKHUserAwardEntity *)initWithJSONObject:(id)json;
@end

//附近商家
@interface ZKHNearbyStoreEntity : ZKHEntity
@property NSString *name;
@property NSString *addr;
@property NSString *phone;
@property NSString *cityName;
@property double lat;
@property double lng;
@property double distance;

- (NSString *)toJSONString;
- (ZKHNearbyStoreEntity *)initWithJSONObject:(id)json;
@end
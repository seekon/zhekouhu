//
//  ZKHEntity.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHEntity.h"
#import "NSString+Utils.h"
#import "NSDate+Utils.h"
#import "ZKHConst.h"
#import "ZKHImageLoader.h"

@implementation ZKHEntity

- (BOOL)isEqual:(id)object
{
    return [object isKindOfClass:[self class]] && [self.uuid isEqual:((ZKHEntity *)object).uuid];
}

@end

@implementation ZKHErrorEntity

@end

@implementation ZKHFileEntity

- (id)copyWithZone:(NSZone *)zone
{
    ZKHFileEntity *copy = [[[self class] allocWithZone:zone] init];
    copy.fileUrl = [self.fileUrl copyWithZone:zone];
    copy.aliasName = [self.aliasName copyWithZone:zone];
    return copy;
}
- (NSString *)fileUrl
{
    if (!_fileUrl && _aliasName) {
        _fileUrl = [ZKHImageLoader getImageFilePath:_aliasName];
    }
    
    return _fileUrl;
}

@end

@implementation ZKHLocation

@end

@implementation ZKHSettingEntity

@end

@implementation ZKHMessageEntity

@end

@implementation ZKHCategoryEntity
- (id)copyWithZone:(NSZone *)zone
{
    ZKHCategoryEntity *copy = [[[self class] allocWithZone:zone] init];
    copy.uuid = [self.uuid copyWithZone:zone];
    copy.name = [self.name copyWithZone:zone];
    copy.parentId = [self.parentId copyWithZone:zone];
    copy.ordIndex = self.ordIndex;
    return copy;
}

@end

@implementation ZKHCityEntity
- (ZKHCityEntity *)initWithJSONObject:(id)jsonCity
{
    if (self = [self init]) {
        self.uuid = jsonCity[KEY_UUID];
        self.name = jsonCity[KEY_NAME];
        self.firstLetter = jsonCity[KEY_FIRST_LETTER];
    }
    return self;
}
@end

@implementation ZKHUserProfileEntity

- (id)initWithJsonObject:(id)json
{
    if (self = [super init]) {
        self.uuid = [json valueForKey:KEY_UUID];
        self.userId = json[KEY_USER_ID];
        self.name = json[KEY_NAME];
        self.photo = [[ZKHFileEntity alloc] init];
        self.photo.aliasName = json[KEY_PHOTO];
        self.sex = json[KEY_SEX];
        self.phone = json[KEY_PHONE];
        self.devilerAddr = json[KEY_DELIVER_ADDR];
        self.awardSum = json[KEY_AWARD_SUM];
        self.awardUsed = json[KEY_AWARD_USED];
        
        if (!self.awardSum) {
            self.awardSum = @"0";
        }
        if(!self.awardUsed){
            self.awardUsed = @"0";
        }
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    ZKHUserProfileEntity *copy = [[[self class] allocWithZone:zone] init];
    copy.uuid = [self.uuid copyWithZone:zone];
    copy.userId = [self.userId copyWithZone:zone];
    copy.name  = [self.name copyWithZone:zone];
    copy.sex = [self.sex copyWithZone:zone];
    copy.phone = [self.phone copyWithZone:zone];
    copy.devilerAddr = [self.devilerAddr copyWithZone:zone];
    copy.photo = [self.photo copyWithZone:zone];
    copy.awardSum = [self.awardSum copyWithZone:zone];
    copy.awardUsed = [self.awardUsed copyWithZone:zone];
    
    return copy;
}

@end

@implementation ZKHUserEntity

- (id)initWithJsonObject:(id)userJson noPwd:(Boolean)noPwd
{
    if (self = [super init]) {
        self.uuid = [userJson valueForKey:KEY_UUID];
        self.code = [userJson valueForKey:KEY_CODE];
        if (!noPwd) {
            self.pwd = [userJson valueForKey:KEY_PWD];
        }
        self.registerTime = [userJson valueForKey:KEY_REGISTER_TIME];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    ZKHUserEntity *copy = [[[self class] allocWithZone:zone] init];
    copy.uuid = [self.uuid copyWithZone:zone];
    copy.pwd  = [self.pwd copyWithZone:zone];
    copy.registerTime = [self.registerTime copyWithZone:zone];
    
    return copy;
}

@end

@implementation ZKHSyncEntity

@end

@implementation ZKHDiscountEntity

- (ZKHDiscountEntity *)initWithJSONObject:(id)jsonDiscount
{
    if (self = [self init]) {
        self.uuid = jsonDiscount[KEY_UUID];
        self.content = jsonDiscount[KEY_CONTENT];
        self.startDate = jsonDiscount[KEY_START_DATE];
        self.endDate = jsonDiscount[KEY_END_DATE];
        self.publisher = jsonDiscount[KEY_PUBLISHER];
        self.publishDate = jsonDiscount[KEY_PUBLISH_DATE];
        self.publishTime = jsonDiscount[KEY_PUBLISH_TIME];
        self.type = jsonDiscount[KEY_TYPE];
        self.visitCount = [jsonDiscount[KEY_VISIT_COUNT] intValue];
        self.parValue = jsonDiscount[KEY_PAR_VALUE];
        self.category = [[ZKHCategoryEntity alloc] init];
        self.category.uuid = jsonDiscount[KEY_CATEGORY_ID];
        self.origin = jsonDiscount[KEY_ORIGIN];
        self.status = jsonDiscount[KEY_STATUS];
        self.location = jsonDiscount[KEY_LOCATION];
        self.auditIdea = jsonDiscount[KEY_AUDIT_IDEA];
    }
    return self;
}

@end

@implementation ZKHStoreEntity

- (id)init
{
    if (self = [super init]) {
        self.storefronts = [[NSMutableArray alloc] init];
    }
    return self;
}

- (ZKHStoreEntity *)initWithJSONObject:(id)jsonStore
{
    if (self = [self init]) {
        self.uuid = jsonStore[KEY_UUID];
        self.name = jsonStore[KEY_NAME];
        self.logo = [[ZKHFileEntity alloc] init];
        self.logo.aliasName = jsonStore[KEY_LOGO];
        self.registerTime = jsonStore[KEY_REGISTER_TIME];
        self.owner = jsonStore[KEY_OWNER];
    }
    return self;
}

@end

@implementation ZKHStorefrontEntity
- (id)init
{
    if (self = [super init]) {
        self.phone = @"";
        self.longitude = @"";
        self.latitude = @"";
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    ZKHStorefrontEntity *copy = [[[self class] allocWithZone:zone] init];
    copy.uuid = [self.uuid copyWithZone:zone];
    copy.name  = [self.name copyWithZone:zone];
    copy.addr = [self.addr copyWithZone:zone];
    copy.phone = [self.phone copyWithZone:zone];
    copy.storeId = [self.storeId copyWithZone:zone];
    copy.longitude = [self.longitude copyWithZone:zone];
    copy.latitude = [self.latitude copyWithZone:zone];
    
    return copy;
}

- (ZKHStorefrontEntity *)initWithJSONObject:(id)jsonFront
{
    if (self = [self init]) {
        self.uuid = jsonFront[KEY_UUID];
        self.name = jsonFront[KEY_NAME];
        self.addr = jsonFront[KEY_ADDR];
        self.storeId = jsonFront[KEY_STORE_ID];
        
        if (jsonFront[KEY_PHONE]) {
            self.phone = jsonFront[KEY_PHONE];
        }
        if (jsonFront[KEY_LATITUDE]) {
            self.latitude = [NSString stringWithFormat:@"%f", [jsonFront[KEY_LATITUDE] doubleValue]];
        }
        if (jsonFront[KEY_LONGITUDE]) {
            self.longitude = [NSString stringWithFormat:@"%f", [jsonFront[KEY_LONGITUDE] doubleValue]];
        }
        if (jsonFront[KEY_STORE_NAME]) {
            self.storeName = jsonFront[KEY_STORE_NAME];
        }
        if(jsonFront[KEY_DISTANCE]){
            self.distance = [jsonFront[KEY_DISTANCE] doubleValue];
        }
        self.city = [[ZKHCityEntity alloc] init];
        self.city.uuid = jsonFront[KEY_CITY];
        self.city.name = jsonFront[KEY_CITY_NAME];
        self.city.firstLetter = jsonFront[KEY_FIRST_LETTER];
    }
    return self;
}

@end

@implementation ZKHDiscountImageEntity

- (ZKHDiscountImageEntity *)initWithJSONObject:(id)jsonImage
{
    if (self = [super init]) {
        self.discountId = jsonImage[KEY_DISCOUNT_ID];
        self.uuid = jsonImage[KEY_UUID];
        self.aliasName = jsonImage[KEY_IMG];
        self.ordIndex = jsonImage[KEY_ORD_INDEX];
    }
    return self;
}

@end

@implementation ZKHDiscountStorefrontEntity

- (ZKHDiscountStorefrontEntity *)initWithJSONObject:(id)jsonFront
{
    if (self = [super init]) {
        self.discountId = jsonFront[KEY_DISCOUNT_ID];
        self.keyId = jsonFront[KEY_UUID];
        self.uuid = jsonFront[KEY_STOREFRONT_ID];//实际为门店id
        self.storeId = jsonFront[KEY_STORE_ID];
        self.ordIndex = jsonFront[KEY_ORD_INDEX];
        if (!self.ordIndex) {
            self.ordIndex = @"0";
        }
    }
    return self;
}

- (ZKHDiscountStorefrontEntity *)initWithJSONObject:(id)jsonFront initSuper:(Boolean)initSuper
{
    if (self = [super initWithJSONObject:jsonFront]) {
        self.keyId = jsonFront[KEY_KEY_ID];
        self.discountId = jsonFront[KEY_DISCOUNT_ID];
        self.ordIndex = jsonFront[KEY_ORD_INDEX];
        if (!self.ordIndex) {
            self.ordIndex = @"0";
        }
    }
    return self;
}
@end

@implementation ZKHCouponStoreEntity

- (ZKHCouponStoreEntity *)initWithJSONObject:(id)jsonFront
{
    if (self = [super init]) {
        self.uuid = jsonFront[KEY_UUID];
        self.name = jsonFront[KEY_NAME];
        self.storeId = jsonFront[KEY_STORE_ID];
        self.categoryId = jsonFront[KEY_CATEGORY_ID];
        self.cityName = jsonFront[KEY_CITY_NAME];
        self.logo = [[ZKHFileEntity alloc]init];
        self.logo.aliasName = jsonFront[KEY_LOGO];
        
        if (!self.uuid) {
            self.uuid = [NSString stringWithFormat:@"%@_%@", self.storeId, [NSDate currentTimeString]];
        }
    }
    return self;
}

@end

@implementation ZKHDateIndexedEntity

@end

@implementation ZKHCommentEntity

- (ZKHCommentEntity *)initWithJSONObject:(id)json
{
    if (self = [super init]) {
        self.uuid = json[KEY_UUID];
        self.discountId = json[KEY_DISCOUNT_ID];
        self.publisher = json[KEY_PUBLISHER];
        self.publishTime = json[KEY_PUBLISH_TIME];
        self.content = json[KEY_CONTENT];
        self.type = json[KEY_TYPE];
    }
    return self;
}
@end

@implementation ZKHFavoritEntity

- (ZKHFavoritEntity *)initWithJSONObject:(id)json
{
    if (self = [super init]) {
        self.uuid = json[KEY_UUID];
        if (json[KEY_USER_ID]) {
            self.userId = json[KEY_USER_ID];
        }
    }
    return self;
}

@end

@implementation ZKHGoodsEntity

- (ZKHGoodsEntity *)initWithJSONObject:(id)json
{
    if (self = [super init]) {
        self.uuid = json[KEY_UUID];
        self.name = json[KEY_NAME];
        self.img = json[KEY_IMG];
        self.desc = json[KEY_DESC];
        self.remain = [json[KEY_REMAIN] integerValue];
        self.coinVal = [json[KEY_COIN_VAL] integerValue];
    }
    return self;
}

@end

@implementation ZKHUserAwardEntity

- (ZKHUserAwardEntity *)initWithJSONObject:(id)json
{
    if (self = [super init]) {
        self.uuid = json[KEY_UUID];
        self.userId = json[KEY_USER_ID];
        self.dataId = json[KEY_DATA_ID];
        self.comment = json[KEY_COMMENT];
        self.awardTime = json[KEY_AWARD_TIME];
        self.coinVal = [json[KEY_COIN_VAL] integerValue];
    }
    return self;
}

@end

@implementation ZKHNearbyStoreEntity

- (NSString *)toJSONString
{
    NSMutableString *json = [[NSMutableString alloc] init];
    [json appendString:@"{"];
    [json appendFormat:@" \"%@\":\"%@\", ", KEY_UUID, self.uuid];
    [json appendFormat:@" \"%@\":\"%@\", ", KEY_NAME, self.name];
    [json appendFormat:@" \"%@\":\"%@\", ", KEY_ADDR, self.addr];
    [json appendFormat:@" \"%@\":\"%@\", ", KEY_PHONE, self.phone ? self.phone : @""];
    [json appendFormat:@" \"%@\":\"%@\", ", KEY_CITY_NAME, self.cityName];
    [json appendFormat:@" \"%@\":\"%f\", ", KEY_LATITUDE, self.lat];
    [json appendFormat:@" \"%@\":\"%f\" ", KEY_LONGITUDE, self.lng];
    [json appendString:@"}"];
    return json;
}

- (ZKHNearbyStoreEntity *)initWithJSONObject:(id)json
{
    if (self = [super init]) {
        self.uuid = json[KEY_UUID];
        self.name = json[KEY_NAME];
        self.addr = json[KEY_ADDR];
        self.phone = json[KEY_PHONE];
        self.cityName = json[KEY_CITY_NAME];
        self.lat = [json[KEY_LATITUDE] doubleValue];
        self.lng = [json[KEY_LONGITUDE] doubleValue];
    }
    return self;
}

@end
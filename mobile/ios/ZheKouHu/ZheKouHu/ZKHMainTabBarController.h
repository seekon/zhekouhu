//
//  ZKHMainTabBarController.h
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@interface ZKHMainTabBarController : UITabBarController<CLLocationManagerDelegate, UITabBarControllerDelegate>
{
    //UIViewController *removedController;
    CLGeocoder *geoCoder;
    NSArray *originControllers;
    
    long preViewControllerTag;
}

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *location;

- (void)showPreViewController;

@end

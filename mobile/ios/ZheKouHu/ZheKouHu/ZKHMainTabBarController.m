//
//  ZKHMainTabBarController.m
//  ZheKouHu
//
//  Created by undyliu on 14-6-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHMainTabBarController.h"
#import "ZKHProcessor+User.h"
#import "ZKHProcessor+Setting.h"
#import "ZKHContext.h"
#import "ZKHViewUtils.h"
#import "UIImage+Utils.h"
#import "ZKHConst.h"
#import "ZKHLocationUtils.h"
#import "SCLAlertViewStyleKit.h"
#import "SCLAlertView.h"
#import "UIColor+Utils.h"
#import "ZKHPublishDiscountController.h"
#import "ZKHUserPubDiscountController.h"

@implementation ZKHMainTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    preViewControllerTag = 0;
    self.delegate = self;
    
    originControllers = self.viewControllers;
    
    if([[[UIDevice currentDevice]systemVersion]floatValue]>=7.0)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
//    ZKHLocation *loc = [[ZKHLocation alloc] init];
//    loc.cityName = @"香港特別行政區";
//    loc.lat = 22.284681;
//    loc.lng = 114.158117;
//    loc.cityName = @"北京市";
//    loc.lat = 40.074063980028;
//    loc.lng = 116.2416776294;
//    [ZKHContext getInstance].location = loc;
    
    //页签栏顶部的黑线
    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"bg_tabbar.png" scale:2]];
    [self.tabBar setClipsToBounds:YES];
    
    NSMutableArray *viewControllers = [self.viewControllers mutableCopy];
    for (UIViewController *controller in viewControllers) {
        UITabBarItem *tabBarItem = controller.tabBarItem;
        NSString *imageSel;
        NSString *imageNor;
        switch (tabBarItem.tag) {
            case 0:
                imageSel = @"activity_sel.png";
                imageNor = @"activity_nor.png";
                break;
            case 1:
                imageSel = @"coupon_sel.png";
                imageNor = @"coupon_nor.png";
                break;
            case 2:
                imageSel = @"tab_user_sel.png";
                imageNor = @"tab_user_nor.png";
                break;
            case 3:
                imageSel = @"tab_publish_sel.png";
                imageNor = @"tab_publish_nor.png";
                break;
            case 4:
                imageSel = @"tab_publish_sel.png";
                imageNor = @"tab_publish_nor.png";
                break;
            default:
                break;
        }
        if (imageNor && imageSel) {
            UIImage *selImage = [UIImage imageWithData:UIImagePNGRepresentation([UIImage imageNamed:imageSel] ) scale:2];
            UIImage *norImage = [UIImage imageWithData:UIImagePNGRepresentation([UIImage imageNamed:imageNor] ) scale:2];
            [tabBarItem setFinishedSelectedImage:selImage withFinishedUnselectedImage:norImage];
        }
        
    }
    
    self.navigationController.navigationBar.clipsToBounds = YES;
    
    //地理位置
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    geoCoder = [[CLGeocoder alloc] init];
    
    //[self.locationManager startUpdatingLocation];
    SEL requestSelector = NSSelectorFromString(@"requestWhenInUseAuthorization");
    
    CLAuthorizationStatus status  = [CLLocationManager authorizationStatus];
    if (status == kCLAuthorizationStatusNotDetermined &&
        [self.locationManager respondsToSelector:requestSelector]) {
        ((void (*)(id, SEL))[self.locationManager methodForSelector:requestSelector])(self.locationManager, requestSelector);
        [self.locationManager startUpdatingLocation];
    } else {
        [self.locationManager startUpdatingLocation];
    }
    
    //注册设备
    ZKHContext *context = [ZKHContext getInstance];
    if (context.deviceId) {
        [self registerDevice];
    }else{
        [context addObserver:self forKeyPath:@"deviceId" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[ZKHContext class]] && [keyPath isEqualToString:@"deviceId"]) {
        [self registerDevice];
    }
}

- (void) registerDevice
{
    //注册设备
    NSString *deviceId = [ZKHContext getInstance].deviceId;
    [[ZKHProcessor getInstance] registerDevice:deviceId completionHandler:^(Boolean result) {
        if (result) {
            [[NSUserDefaults standardUserDefaults] setValue:deviceId forKey:KEY_DEV_ID];
        }
    } errorHandler:^(ZKHErrorEntity *error) {
        
    }];
}

- (void) showLocationAlertView
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"是" actionBlock:^(void) {
        NSURL* url =[NSURL URLWithString:UIApplicationOpenSettingsURLString];
        [[UIApplication sharedApplication] openURL:url];
    }];
    [alert showCustom:self.navigationController image:SCLAlertViewStyleKit.imageOfNotice color:[UIColor color_ff6722] title:@"提示" subTitle:@"未获取到您的位置，无法获取附近的促销信息，是否进入设置界面？" closeButtonTitle:@"否" duration:0.0f];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CLAuthorizationStatus status  = [CLLocationManager authorizationStatus];
    if(status == kCLAuthorizationStatusDenied){
        [self showLocationAlertView];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    //如果没有注册店铺，则tabbar中去掉店铺的发优惠， v1.1版本去掉
    Boolean ownerStores = false;
    ZKHUserEntity *user = [ZKHContext getInstance].user;
    if (user && [VAL_USER_TYPE_STORER isEqualToString:user.type]) {
        ownerStores = true;
    }
    
    NSMutableArray *viewControllers = [originControllers mutableCopy];
    for (UIViewController *controller in viewControllers) {
        if (ownerStores && controller.tabBarItem.tag == 3) {
            [viewControllers removeObject:controller];
            break;
        }
        if (!ownerStores && controller.tabBarItem.tag == 4) {
            [viewControllers removeObject:controller];
            break;
        }
    }
    [self setViewControllers:viewControllers];
    
//    Boolean shopRegistered = [[ZKHProcessor getInstance] isStoreRegistered];
//    if (!shopRegistered) {
//        NSMutableArray *viewControllers = [self.viewControllers mutableCopy];
//        for (UIViewController *controller in viewControllers) {
//            if (controller.tabBarItem.tag == 2) {
//                [viewControllers removeObject:controller];
//                removedController = controller;
//                break;
//            }
//        }
//        
//        [self setViewControllers:viewControllers];
//    }else if ([self.viewControllers count] == 3 && removedController){
//        NSMutableArray *viewControllers = [self.viewControllers mutableCopy];
//        [viewControllers insertObject:removedController atIndex:2];
//        [self setViewControllers:viewControllers];
//    }
//    [ZKHContext getInstance].shopRegistered = shopRegistered;
    
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - locationManager delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *newLocation = [locations lastObject];
    
    if (newLocation.verticalAccuracy < 0 || newLocation.horizontalAccuracy < 0) {
        return;
    }
    if (newLocation.verticalAccuracy > 100 || newLocation.horizontalAccuracy > 500) {
        return;
    }
    
    //if (!self.location){
    self.location = newLocation;
    
    manager.delegate = nil;
    [manager stopUpdatingLocation];
    
    [self setLocation:newLocation];
    //}
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ( [error code] == kCLErrorDenied ) {
        [manager stopUpdatingHeading];
    } else if ([error code] == kCLErrorHeadingFailure) {
        
    }
}

- (void) setLocation:(CLLocation *)location
{
    [geoCoder
     reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
         if (error == nil &&[placemarks count] > 0){
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             /* We received the results */
             NSLog(@"Country = %@", placemark.country);
             NSLog(@"Postal Code = %@", placemark.postalCode);
             NSLog(@"Locality = %@", placemark.locality);
             NSLog(@"dic = %@", placemark.addressDictionary );
             NSLog(@"dic FormattedAddressLines= %@", [placemark.addressDictionary objectForKey:@"FormattedAddressLines"]);
             NSLog(@"dic Name = %@", [placemark.addressDictionary objectForKey:@"Name"]);
             NSLog(@"dic State = %@", [placemark.addressDictionary objectForKey:@"State"]);
             NSLog(@"dic Street = %@", [placemark.addressDictionary objectForKey:@"Street"]);
             NSLog(@"dic SubLocality= %@", [placemark.addressDictionary objectForKey:@"SubLocality"]);
             NSLog(@"dic SubThoroughfare= %@", [placemark.addressDictionary objectForKey:@"SubThoroughfare"]);
             NSLog(@"dic Thoroughfare = %@", [placemark.addressDictionary objectForKey:@"Thoroughfare"]);
             
             NSLog(@"coordinate.latitude = %f", location.coordinate.latitude);
             NSLog(@"coordinate.longitude = %f", location.coordinate.longitude);
             
             ZKHLocation *loc = [[ZKHLocation alloc] init];
             loc.cityName = [placemark.addressDictionary objectForKey:@"State"];
             loc.lat = location.coordinate.latitude;
             loc.lng = location.coordinate.longitude;
             [ZKHContext getInstance].location = loc;
         }
         else if (error == nil &&
                  [placemarks count] == 0){
             NSLog(@"No results were returned.");
         }
         else if (error != nil){
             NSLog(@"An error occurred = %@", error);
         }
     }];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (![viewController isKindOfClass:[ZKHUserPubDiscountController class]]) {
        preViewControllerTag = viewController.tabBarItem.tag;
    }
}

- (void)showPreViewController
{
    [self setSelectedIndex:preViewControllerTag];
}

@end

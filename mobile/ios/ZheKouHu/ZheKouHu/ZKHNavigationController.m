//
//  ZKHNavigationController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-25.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHNavigationController.h"
#import "ZKHConst.h"
#import "UIColor+Utils.h"

@interface ZKHNavigationController ()

@end

@implementation ZKHNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:FALSE];
    [self setNeedsStatusBarAppearanceUpdate];
    
    UIView *addStatusBar = [[UIView alloc] init];
    addStatusBar.frame = CGRectMake(0, 0, 320, 20);
    addStatusBar.backgroundColor = [UIColor color_ff6722];
    [self.view addSubview:addStatusBar];
    
    //导航栏底部的黑线
    if(IOS7){
        if ([self.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
            NSArray *list=self.navigationBar.subviews;
            for (id obj in list) {
                if ([obj isKindOfClass:[UIImageView class]]) {
                    UIImageView *imageView=(UIImageView *)obj;
                    imageView.hidden=YES;
                }
            }
            UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,320, 44)];
            imageView.image=[UIImage imageNamed:@"bg_nav"];
            [self.navigationBar addSubview:imageView];
            [self.navigationBar sendSubviewToBack:imageView];
        }
    }else{
        UIView * narBottomView = [[UIView alloc] init];
        narBottomView.backgroundColor = self.navigationBar.tintColor;
        CGRect rect = narBottomView.frame;
        rect.origin.x = 0.f;
        rect.origin.y = self.navigationBar.frame.size.height -1;
        rect.size.width = self.navigationBar.frame.size.width;
        rect.size.height = 1.f;
        narBottomView.frame = rect;
        [self.navigationBar addSubview:narBottomView];
        [self.navigationBar setClipsToBounds:YES];
        
        [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav"] forBarMetrics:UIBarMetricsDefault];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

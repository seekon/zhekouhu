//
//  ZKHNetworkEngine.h
//  ZheKouHu
//
//  Created by undyliu on 14-5-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHRestClient.h"

@interface ZKHProcessor : NSObject
{
   @private
    ZKHRestClient *restClient;
}

+ (ZKHProcessor *) getInstance;

//统一的成功与否的返回处理
typedef void (^BooleanResultResponseBlock)(Boolean result);

typedef void (^ZKHImageResponseBlock)(UIImage *fetchedImage);
- (void)imageAtURL:(NSURL *)url completionHandler:(ZKHImageResponseBlock) imageFetchedBlock errorHandler:(RestResponseErrorBlock) errorBlock;

@end

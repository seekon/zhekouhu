//
//  ZKHNetworkEngine.m
//  ZheKouHu
//
//  Created by undyliu on 14-5-19.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHProcessor.h"
#import "ZKHConst.h"
#import "ZKHData.h"
#import "ZKHContext.h"
#import "ZKHRestRequest.h"

@implementation ZKHProcessor

static ZKHProcessor *instance;

+ (ZKHProcessor *)getInstance
{
    if (!instance) {
        instance = [[ZKHProcessor alloc] init];
    }
    return instance;
}

- (id)init
{
    if (self = [super init]) {
        restClient = [[ZKHRestClient alloc] initWithDefaultSettings];
    }
    return self;
}

- (void)imageAtURL:(NSURL *)url completionHandler:(ZKHImageResponseBlock)imageFetchedBlock errorHandler:(RestResponseErrorBlock)errorBlock
{
    [restClient imageAtURL:url completionHandler:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
        imageFetchedBlock(fetchedImage);
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        ZKHErrorEntity *entity = [[ZKHErrorEntity alloc] init];
        entity.type = VAL_ERROR_TYPE_NETWORK;
        entity.message = error.description;
        errorBlock(entity);
    }];
}

@end

//
//  ZKHUserGuideController.h
//  ZheKouHu
//
//  Created by undyliu on 14-8-12.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZKHUserGuideController : UIViewController<UIScrollViewDelegate>
{
    NSArray *imageNames;
    NSMutableArray *imageViews;

}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

//
//  ZKHUserGuideController.m
//  ZheKouHu
//
//  Created by undyliu on 14-8-12.
//  Copyright (c) 2014年 undyliu. All rights reserved.
//

#import "ZKHUserGuideController.h"
#import "ZKHViewUtils.h"
#import "UIImage+Utils.h"

@interface ZKHUserGuideController ()

@end

@implementation ZKHUserGuideController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    CGFloat sHeight = [[UIScreen mainScreen] bounds].size.height * 2;
    NSString *prefix = [NSString stringWithFormat:@"%.0f", sHeight];
    imageNames = @[[NSString stringWithFormat:@"%@_user_guid_1", prefix]
                   ,[NSString stringWithFormat:@"%@_user_guid_2", prefix]
                   ,[NSString stringWithFormat:@"%@_user_guid_3", prefix]];
    long kNumberOfPages = [imageNames count];
    
    NSMutableArray *views = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        [views addObject:[NSNull null]];
    }
    imageViews = views;
    
    self.scrollView.pagingEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width * kNumberOfPages, self.view.frame.size.height);
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    
    [self loadScrollViewWithPage:0];
    //[self loadScrollViewWithPage:1];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)loadScrollViewWithPage:(int)page {	
    if (page < 0) return;
	
    // replace the placeholder if necessary
    UIView *view = [imageViews objectAtIndex:page];
    if ((NSNull *)view == [NSNull null]) {
        view = [[UIView alloc] init];
        
        CGRect frame = self.view.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        view.frame = frame;
        
        UIImageView *imgView = [[UIImageView alloc] init];
        imgView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        imgView.image = [UIImage imageNamed:imageNames[page]];
        [view addSubview:imgView];
        
        [self.scrollView addSubview:view];
        
        if (page == [imageNames count] - 1) {
            UIButton *startButton = [UIButton buttonWithType:UIButtonTypeCustom];
            //[startButton setTitle:@"立即体验" forState:UIControlStateNormal];
            [startButton setImage:[UIImage imageNamed:@"btn_start_use_nor" scale:2] forState:UIControlStateNormal];
            [startButton setImage:[UIImage imageNamed:@"btn_start_use_sel" scale:2] forState:UIControlStateNormal];
            
            CGFloat bHeight = 31;
            CGFloat bWidth = 105;
            CGFloat offsetY = 50;
            if ([[UIScreen mainScreen] bounds].size.height > 480) {
                offsetY = 70;
            }
            CGFloat bX = (view.frame.size.width - bWidth)/2;
            CGFloat bY = view.frame.origin.y + view.frame.size.height - bHeight - offsetY;
            startButton.frame = CGRectMake(bX, bY, bWidth, bHeight);
            
            [startButton addTarget:self action:@selector(doStart:) forControlEvents:UIControlEventTouchUpInside];
            
            [view addSubview:startButton];
        }
 
        [imageViews replaceObjectAtIndex:page withObject:view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
	
    //[self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    //[self loadScrollViewWithPage:page + 1];
	
}

- (void)doStart:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"everLaunched"];
    
    UIViewController *controller = [[ZKHViewUtils mainStoryboard] instantiateViewControllerWithIdentifier:@"ZKHNavigationController"];
    
    [[[UIApplication sharedApplication] delegate] window].rootViewController = controller;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

@end

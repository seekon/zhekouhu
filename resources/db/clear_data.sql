--delete from  z_category;

DELETE from z_discount_img;
DELETE from z_discount_storefront;
DELETE from z_discount_visit;
DELETE from z_discount;

DELETE from z_feedback;
DELETE from z_log;

DELETE from z_storefront;
DELETE from z_store;

DELETE from z_user;

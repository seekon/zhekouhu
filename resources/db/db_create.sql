/*
 Navicat Premium Data Transfer

 Source Server         : 10.10.65.91_zkh
 Source Server Type    : MySQL
 Source Server Version : 50511
 Source Host           : 10.10.65.91
 Source Database       : zkh

 Target Server Type    : MySQL
 Target Server Version : 50511
 File Encoding         : utf-8

 Date: 09/03/2014 17:11:50 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `z_category`
-- ----------------------------
DROP TABLE IF EXISTS `z_category`;
CREATE TABLE `z_category` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `name` varchar(20) COLLATE utf8_bin NOT NULL,
  `icon` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `parent_id` varchar(36) COLLATE utf8_bin DEFAULT NULL,
  `is_delete` char(1) COLLATE utf8_bin DEFAULT NULL,
  `ord_index` int(11) NOT NULL DEFAULT '1',
  `last_modify_time` varchar(16) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='类别';

-- ----------------------------
--  Table structure for `z_city`
-- ----------------------------
DROP TABLE IF EXISTS `z_city`;
CREATE TABLE `z_city` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `first_letter` char(1) NOT NULL,
  `last_modify_time` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `z_discount`
-- ----------------------------
DROP TABLE IF EXISTS `z_discount`;
CREATE TABLE `z_discount` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `content` varchar(140) COLLATE utf8_bin NOT NULL,
  `par_value` double NOT NULL DEFAULT '0',
  `category_id` varchar(36) COLLATE utf8_bin NOT NULL,
  `start_date` varchar(16) COLLATE utf8_bin NOT NULL,
  `end_date` varchar(16) COLLATE utf8_bin NOT NULL,
  `type` char(1) COLLATE utf8_bin NOT NULL COMMENT '0：折扣，1：优惠券',
  `publish_time` varchar(16) COLLATE utf8_bin NOT NULL,
  `publish_date` varchar(10) COLLATE utf8_bin NOT NULL,
  `publisher` varchar(36) COLLATE utf8_bin NOT NULL,
  `visit_count` int(11) DEFAULT '0',
  `status` char(1) COLLATE utf8_bin DEFAULT NULL,
  `last_modify_time` varchar(16) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `FK_Reference_6` (`category_id`),
  KEY `FK_Reference_7` (`publisher`),
  KEY `IN_publish_time` (`publish_time`),
  KEY `IN_status` (`status`),
  KEY `IN_type` (`type`),
  KEY `IN_start_end_date` (`start_date`,`end_date`),
  CONSTRAINT `FK_Reference_6` FOREIGN KEY (`category_id`) REFERENCES `z_category` (`uuid`),
  CONSTRAINT `FK_Reference_7` FOREIGN KEY (`publisher`) REFERENCES `z_user` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='折扣活动';

-- ----------------------------
--  Table structure for `z_discount_img`
-- ----------------------------
DROP TABLE IF EXISTS `z_discount_img`;
CREATE TABLE `z_discount_img` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `discount_id` varchar(36) COLLATE utf8_bin NOT NULL,
  `img` varchar(100) COLLATE utf8_bin NOT NULL,
  `ord_index` int(11) NOT NULL DEFAULT '0',
  `is_deleted` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`),
  KEY `FK_Reference_5` (`discount_id`),
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`discount_id`) REFERENCES `z_discount` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
--  Table structure for `z_discount_storefront`
-- ----------------------------
DROP TABLE IF EXISTS `z_discount_storefront`;
CREATE TABLE `z_discount_storefront` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `discount_id` varchar(36) COLLATE utf8_bin NOT NULL,
  `storefront_id` varchar(36) COLLATE utf8_bin NOT NULL,
  `store_id` varchar(36) COLLATE utf8_bin NOT NULL,
  `ord_index` int(11) NOT NULL DEFAULT '0',
  `is_deleted` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `u_discount_storefront` (`discount_id`,`storefront_id`),
  KEY `FK_Reference_4` (`storefront_id`),
  KEY `IN_store_id` (`store_id`),
  KEY `IN_descount_id` (`discount_id`),
  KEY `IN_is_deleted` (`is_deleted`),
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`discount_id`) REFERENCES `z_discount` (`uuid`),
  CONSTRAINT `FK_Reference_4` FOREIGN KEY (`storefront_id`) REFERENCES `z_storefront` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='折扣活动店铺对应';

-- ----------------------------
--  Table structure for `z_discount_visit`
-- ----------------------------
DROP TABLE IF EXISTS `z_discount_visit`;
CREATE TABLE `z_discount_visit` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `discount_id` varchar(36) COLLATE utf8_bin NOT NULL,
  `remote_addr` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `action_time` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `FK_Reference_8` (`discount_id`),
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`discount_id`) REFERENCES `z_discount` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='活动访问记录';

-- ----------------------------
--  Table structure for `z_feedback`
-- ----------------------------
DROP TABLE IF EXISTS `z_feedback`;
CREATE TABLE `z_feedback` (
  `uuid` varchar(36) NOT NULL,
  `content` text NOT NULL,
  `contact` varchar(200) DEFAULT NULL,
  `fd_time` varchar(20) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `z_log`
-- ----------------------------
DROP TABLE IF EXISTS `z_log`;
CREATE TABLE `z_log` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(36) COLLATE utf8_bin DEFAULT NULL,
  `remote_addr` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `action_time` varchar(20) COLLATE utf8_bin NOT NULL,
  `action_uri` varchar(500) COLLATE utf8_bin NOT NULL,
  `action_content` text COLLATE utf8_bin,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='日志';

-- ----------------------------
--  Table structure for `z_store`
-- ----------------------------
DROP TABLE IF EXISTS `z_store`;
CREATE TABLE `z_store` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `logo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `owner` varchar(36) COLLATE utf8_bin NOT NULL,
  `register_time` varchar(16) COLLATE utf8_bin NOT NULL,
  `last_modify_time` varchar(16) COLLATE utf8_bin NOT NULL,
  `is_deleted` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`),
  KEY `FK_Reference_1` (`owner`),
  CONSTRAINT `FK_Reference_1` FOREIGN KEY (`owner`) REFERENCES `z_user` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='店铺';

-- ----------------------------
--  Table structure for `z_storefront`
-- ----------------------------
DROP TABLE IF EXISTS `z_storefront`;
CREATE TABLE `z_storefront` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `store_id` varchar(36) COLLATE utf8_bin NOT NULL,
  `city` varchar(36) COLLATE utf8_bin DEFAULT NULL,
  `city_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `addr` varchar(200) COLLATE utf8_bin NOT NULL,
  `phone` varchar(36) COLLATE utf8_bin DEFAULT NULL,
  `latitude` double DEFAULT '0',
  `longitude` double DEFAULT '0',
  `last_modify_time` varchar(16) COLLATE utf8_bin NOT NULL,
  `is_deleted` char(1) COLLATE utf8_bin NOT NULL DEFAULT '0',
  PRIMARY KEY (`uuid`),
  KEY `FK_Reference_2` (`store_id`),
  KEY `IN_city_name` (`city_name`),
  KEY `IN_is_deleted` (`is_deleted`),
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`store_id`) REFERENCES `z_store` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='门店';

-- ----------------------------
--  Table structure for `z_user`
-- ----------------------------
DROP TABLE IF EXISTS `z_user`;
CREATE TABLE `z_user` (
  `uuid` varchar(36) COLLATE utf8_bin NOT NULL,
  `code` varchar(36) COLLATE utf8_bin NOT NULL,
  `pwd` varchar(32) COLLATE utf8_bin NOT NULL,
  `register_time` varchar(16) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `u_user_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户';

-- ----------------------------
--  Table structure for `z_version`
-- ----------------------------
DROP TABLE IF EXISTS `z_version`;
CREATE TABLE `z_version` (
  `uuid` varchar(36) NOT NULL,
  `value` varchar(32) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


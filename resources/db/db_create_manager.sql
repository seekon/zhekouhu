/*
 Navicat Premium Data Transfer

 Source Server         : 10.10.65.91_zkh_admin
 Source Server Type    : MySQL
 Source Server Version : 50511
 Source Host           : 10.10.65.91
 Source Database       : zkh_manager

 Target Server Type    : MySQL
 Target Server Version : 50511
 File Encoding         : utf-8

 Date: 09/05/2014 09:32:24 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ntf_device`
-- ----------------------------
DROP TABLE IF EXISTS `ntf_device`;
CREATE TABLE `ntf_device` (
  `uuid` varchar(36) NOT NULL,
  `device_id` varchar(64) NOT NULL,
  `type` char(1) NOT NULL,
  `register_time` varchar(16) NOT NULL,
  `status` char(1) NOT NULL DEFAULT '1',
  `last_modify_time` varchar(16) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `ntf_message`
-- ----------------------------
DROP TABLE IF EXISTS `ntf_message`;
CREATE TABLE `ntf_message` (
  `uuid` varchar(36) NOT NULL,
  `msg_id` varchar(36) NOT NULL,
  `type` char(1) NOT NULL,
  `content` varchar(200) NOT NULL,
  `topic` varchar(100) NOT NULL,
  `send_time` varchar(16) NOT NULL,
  `status` char(1) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `uuid` varchar(36) NOT NULL,
  `code` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pwd` varchar(32) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
--  Function structure for `geodist_field`
-- ----------------------------
DROP FUNCTION IF EXISTS `geodist_field`;
delimiter ;;
CREATE FUNCTION `geodist_field`(`lat_d` double,`lon_d` double,`lat_s` double,`lon_s` double) RETURNS double
BEGIN
	#Routine body goes here...

	RETURN (2 * 6378.137 * ASIN(SQRT(POW(SIN(PI() * (lat_d - lat_s)/360),2) + COS(PI() * lat_d/180)* COS(lat_s * PI()/180)*POW(SIN(PI()*(lon_d - lon_s)/360),2)))) * 1000;
END
 ;;
delimiter ;

/*
  数据库sql脚本从1.0.0升级到1.1.0
*/
SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `z_award_goods` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `img` varchar(200) NOT NULL,
  `desc` varchar(2000) NOT NULL,
  `num` int(10) NOT NULL,
  `coin_val` int(10) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分商城商品表';

CREATE TABLE `z_award_order` (
  `uuid` varchar(36) NOT NULL,
  `goods_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `user_phone` varchar(11) NOT NULL,
  `user_addr` varchar(500) NOT NULL,
  `order_time` varchar(16) NOT NULL,
  `coin_val` int(10) NOT NULL,
  `status` char(1) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分商城商品购买记录（订单）\nstatus：1代表记录有效，2代表已发货，9代表取消';

CREATE TABLE `z_discount_award` (
  `uuid` varchar(36) NOT NULL,
  `discount_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `awarder` varchar(36) NOT NULL,
  `award_time` varchar(16) NOT NULL,
  `exp_time` varchar(16) NOT NULL,
  `coin_val` int(10) NOT NULL DEFAULT '0',
  `comment` varchar(500) DEFAULT NULL,
  `last_modify_time` varchar(16) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人发布优惠积分奖励表';

CREATE TABLE `z_discount_comment` (
  `uuid` varchar(36) NOT NULL,
  `discount_id` varchar(36) NOT NULL,
  `content` varchar(500) NOT NULL,
  `publisher` varchar(36) NOT NULL,
  `publish_time` varchar(16) NOT NULL,
  `type` char(1) NOT NULL DEFAULT '0',
  `is_deleted` char(1) NOT NULL,
  `last_modify_time` varchar(16) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='type:0代表评论，1代表纠错';

CREATE TABLE `z_user_favorit` (
  `uuid` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `discount_id` varchar(36) NOT NULL,
  `last_modify_time` varchar(16) NOT NULL,
  `is_deleted` char(1) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `z_user_profile` (
  `uuid` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `photo` varchar(200) DEFAULT '',
  `name` varchar(200) DEFAULT '',
  `sex` char(1) DEFAULT '',
  `phone` varchar(11) DEFAULT '',
  `deliver_addr` varchar(500) DEFAULT '',
  `award_sum` int(10) NOT NULL DEFAULT '0',
  `award_used` int(10) NOT NULL DEFAULT '0',
  `last_modify_time` varchar(16) not null default '0',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表\nsex：1代表男，0代表女';

alter table z_discount add origin char(1) default '0';
alter table z_discount add location varchar(1000) null default '';
alter table z_discount add audit_idea varchar(1000) null default '';
alter table z_discount drop foreign key FK_Reference_7;

ALTER table z_award_goods CHANGE num sum INTEGER;
alter table z_award_goods add remain INTEGER not null default '0';
alter table z_award_goods add is_deleted char(1) not null default '0';

insert into z_user_profile(uuid, user_id,last_modify_time)
select uuid, uuid, register_time from z_user u
where u.uuid not in (select uuid from z_user_profile);

alter table z_store drop foreign key FK_Reference_1;

alter table z_discount modify publisher varchar(100);
alter table z_discount_comment modify publisher varchar(100);
alter table z_user_favorit modify user_id varchar(100);

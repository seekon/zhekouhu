(defproject zhekouhu "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [compojure "1.1.6"]
								 [org.clojure/data.json "0.2.4"]
								 [org.clojure/java.jdbc "0.2.3"]
								 [korma "0.3.0-RC6"]
								 [mysql/mysql-connector-java "5.1.25"]
								 [pandect "0.3.0"]
                 [org.clojure/tools.logging "0.2.6"]
                 [log4j/log4j "1.2.17"]
                 [clj-http-lite "0.2.0"]
                 [bk/ring-gzip "0.1.1"]
                 [enlive "1.1.5"]
                 [com.taoensso/carmine "2.7.0"]
                 [ceterumnet-zclucy "0.9.4"]
                 [com.chenlb.mmseg4j/mmseg4j-analysis "1.9.1"]
		]
  :plugins [[lein-ring "0.8.10"]]
  :ring {:handler zhekouhu.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}}
  :test-selectors
  {:auth :test-auth
   :anonymous :test-anonymous
   :category :test-category
   :user :test-user
   :discount :test-discount
   :search :test-search
   :data-discount :data-discount}
  )

$(function (){
    //图片自动切换
    var len=$("#slideshow img").length;
    var index=0;
    function show(num){
        $("#slideshow img").eq(num).hide();
        $("#slideshow img").eq(num).fadeIn(5000);
        $("#slideshow img").eq(num).show();
        $("#slideshow img").fadeOut(5000);
    };
    function auto(){
        if(index==len-1){index=0;}else{index+=1;}
        show(index);
    }
    //show(index);
    setInterval(auto,10000);
});

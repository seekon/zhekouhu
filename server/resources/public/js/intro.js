
var Page = (function() {
        var initPageIndex = 1;
        if(window.initPageIndex){
          initPageIndex = window.initPageIndex;
        }
				var config = {
						$bookBlock : $( '#bb-bookblock' ),
						$navNext : $( '#bb-nav-next' ),
						$navPrev : $( '#bb-nav-prev' ),
            $navHome : $('#bb-nav-home'),
            $navDir : $('.litebox')
					},
					init = function() {
						config.$bookBlock.bookblock( {
              startPage : initPageIndex,
							//speed : 800,
							shadowSides : 0.8,
							shadowFlip : 0.7
						} );
						initEvents();
					},
					initEvents = function() {

						var $slides = config.$bookBlock.children();

            config.$navDir.liteBox();

            config.$navHome.on( 'click touchstart', function() {
							location.href = "index.html";
						} );

            $('#logo').on( 'click touchstart', function() {
							location.href = "index.html";
						} );

						// add navigation events
						config.$navNext.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'next' );
							return false;
						} );

						config.$navPrev.on( 'click touchstart', function() {
							config.$bookBlock.bookblock( 'prev' );
							return false;
						} );

						// add swipe events
						$slides.on( {
							'swipeleft' : function( event ) {
								config.$bookBlock.bookblock( 'next' );
								return false;
							},
							'swiperight' : function( event ) {
								config.$bookBlock.bookblock( 'prev' );
								return false;
							}
						} );

						// add keyboard events
						$( document ).keydown( function(e) {
							var keyCode = e.keyCode || e.which,
								arrow = {
									left : 37,
									up : 38,
									right : 39,
									down : 40
								};

							switch (keyCode) {
								case arrow.left:
									config.$bookBlock.bookblock( 'prev' );
									break;
								case arrow.right:
									config.$bookBlock.bookblock( 'next' );
									break;
							}
						} );
					};

  return { init : init, bookblock :config.$bookBlock, litebox : config.$navDir};

			})();

Page.init();

function jump(page){
  Page.litebox.litebox('closeLitebox');
  Page.bookblock.bookblock('jump',page);
}


$(function(){
  $('#btnSave').on('click', function(){
			$('#ff').form('submit',{
				onSubmit:function(params){
          params.storefronts = $.toJSON(frontsData);
					return $(this).form('enableValidation').form('validate')
                && (frontsData.length > 0);
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','保存店铺信息成功.','info');

            $('#ff').form('clear');
            frontsData.length = 0;
            $('#grid_fronts').datagrid('loadData', frontsData);
          }else{
            $.messager.alert('失败','保存店铺失败.','error');
          }
        }
			});
		});

  $('#btnClear').on('click', function(){
			$('#ff').form('clear');
      frontsData.length = 0;
      $('#grid_fronts').datagrid('loadData', frontsData);
		});

});

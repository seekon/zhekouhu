var discountFronts = [];
var discount = {};
$(function(){
  //获取活动数据
  $.ajax({
    url: '/getDiscountById/' + $.getUrlVar('uuid'),
    dataType: 'json',
    success: function(data){
      discount = data.discounts[0];

      for(var j = 0; j < data.storefronts.length; j++){
        var deleted = false;
        var front = data.storefronts[j];
        for(var i = 0; i < data.discount_storefronts.length; i++){
          var dsf = data.discount_storefronts[i];
          if(front.uuid == dsf.storefront_id){
            deleted = (dsf.is_deleted == '1');
            break;
          }
        }
        if(!deleted){
          discountFronts.push(front);
        }
      }

      //discount
      var d = new Date();
      d.setTime(discount.start_date);
      $('#startDate').datebox('setValue', d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());
      d.setTime(discount.end_date);
      $('#endDate').datebox('setValue', d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate());
      $('#content').textbox('setValue', discount.content);
      $('#parValue').numberbox('setValue', discount.par_value);
      $('#uuid').attr('value', discount.uuid);
      $('#categoryId').combobox('setValue', discount.category_id);

      if(discount.status != "1"){
        $('#buttonBar').css("display", 'none');
      }

      var images = [];
      for(var i = 0; i < data.images.length; i++){
        var img = data.images[i];
        if(img.is_deleted != '1'){
          images.push(img);
        }
      }
      //images
      $('#grid_images').datagrid('loadData', images);

      //storefronts
      setStorefrontsChecked();

      var type = discount.type;
      if(type == 0){
        $('.c_parValue').each(function(){
          $(this).css('display','none');
        });

        if(discount.status == "1"){
          document.title = "编辑促销活动";
          $('#title').html(document.title);
        }else{
          document.title = "查看促销活动";
          $('#title').html(document.title);
        }
      }else{
        if(discount.status == "1"){
          document.title = "编辑优惠券";
          $('#title').html(document.title);
        }else{
          document.title = "查看优惠券";
          $('#title').html(document.title);
        }
      }
    },
    error: function(){
      error.apply(this, arguments);
    }
  });

  $('#discount_imgs').diyUpload({
	        url:'/web/uploadDiscountImages',
	        success:function( data ) {
            $.ajax({
              url: '/web/addDiscountImage',
              type: 'post',
              dataType: 'json',
              data: {img: data.file_name, discount_id: discount.uuid},
              success: function(data){
                if(data.uuid){
                  $('#grid_images').datagrid('appendRow', data);
                }
              },
              error: function(){
                 $.messager.alert('失败','添加活动图片失败.','error');
              }
            });
	        },
	        error:function( err ) {
		        $.messager.alert('失败','上传图片失败.','error');
	        }
        });
  $('#btnPublish').on('click', function(){
			$('#ff').form('submit',{
				onSubmit:function(params){
          var selections = $('#grid_storefronts').datagrid('getChecked');
          if(selections.length == 0){
            $.messager.alert('提示','至少选个一家适用门店.','info');
            return false;
          }

          params.storefronts = $.toJSON(Array.minus(selections,discountFronts));
          params.del_storefronts = $.toJSON(Array.minus(discountFronts,selections));

					return $(this).form('enableValidation').form('validate')
                && (selections.length > 0);
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','发布活动信息成功.','info');
          }else{
            $.messager.alert('失败','发布活动信息失败.','error');
          }
        }
			});
		});

   $('#btnDelete').on('click', function(){
			$.ajax({
        url: '/deleteDiscount/' + $.getUrlVar('uuid'),
        dataType: 'json',
        type: 'delete',
        success: function(data){
          if(data.uuid){
            $.messager.alert('成功','删除活动信息成功.\n页面将返回.','info');
            window.history.back(-1);
          }
        },
        error: function(){
          $.messager.alert('失败','删除活动信息失败.','error');
        }
      });
		});
});



        var storefrontLoader = function(param,success,error){
            $.ajax({
                url: '/getValidStoresByCurUser',
                dataType: 'json',
                success: function(data){
                  var items = [];
                  for(var i = 0; i < data.length; i++){
                    var store = data[i];
                    for(var j = 0 ; j < store.storefronts.length; j++){
                      var front = store.storefronts[j];
                      front.storeId = store.uuid;
                      front.storeName = store.name;
                      items.push(front);
                    }
                  }
                  success(items);
                  setStorefrontsChecked();
                },
                error: function(){
                    error.apply(this, arguments);
                }
            });
        }
        function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
        }
        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }

function setStorefrontsChecked(){
  var frontRows = $('#grid_storefronts').datagrid('getRows');
      for(var j = 0; j < discountFronts.length; j++){
        for(var i = 0; i < frontRows.length; i++){
          var rowData = frontRows[i];
          if(rowData.uuid == discountFronts[j].uuid){
            var rowIndex = $('#grid_storefronts').datagrid('getRowIndex', rowData);
            $('#grid_storefronts').datagrid('checkRow', rowIndex);
          }
        }
      }
}

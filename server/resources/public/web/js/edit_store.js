var delFronts = [];
$(function(){
   $('#btnSave').on('click', function(){
			$('#ff').form('submit',{
				onSubmit:function(params){
          endEditing();
          params.logo = $('#store_logo').attr('value');
          params.storefronts = $.toJSON($('#grid_fronts').datagrid('getData').rows);
          params.del_fronts = $.toJSON(delFronts);
					return $(this).form('enableValidation').form('validate')
                && (params.storefronts.length > 0);
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','修改店铺信息成功.','info');
            var storefronts = data.storefronts;//更新门店的uuid
            var gridFronts = $('#grid_fronts').datagrid('getData').rows;
            if(storefronts){
              for(var i = 0; i < gridFronts.length; i++){
                if(!gridFronts[i].uuid){
                  gridFronts[i].uuid = storefronts[i].uuid;
                }
              }
              $('#grid_fronts').datagrid('loadData', gridFronts);
            }
          }else{
            $.messager.alert('失败','修改店铺信息失败.','error');
          }
        }
			});
		});

  $('#btnDelete').on('click', function(){
			$.ajax({
        url: '/deleteStore/' + $.getUrlVar('uuid'),
        dataType: 'json',
        type: 'delete',
        success: function(data){
          if(data.uuid){
            $.messager.alert('成功','删除店铺信息成功.\n页面将返回.','info');
            window.history.back(-1);
          }
        },
        error: function(){
          $.messager.alert('失败','删除店铺失败.','error');
        }
      });
		});

  //获取店铺信息
  $.ajax({
    url: '/web/getStoreById/' + $.getUrlVar('uuid'),
    dataType: 'json',
    success: function(data){
      $('#store_name').textbox('setValue', data.name);
      $('#store_uuid').attr('value', data.uuid)
      $('#store_logo').attr('value', data.logo)
      //$('#store_logo').textbox('setValue', data.logo);
      $("#log_img").attr("src","/getImageFile/" + data.logo);
      $('#grid_fronts').datagrid('loadData', data.storefronts);
    },
    error: function(){
      error.apply(this, arguments);
    }
  });

});

    var toolbar = [{
                text:'新增',
                iconCls:'icon-add',
                handler:function(){
                    $('#win_sf').window('open');
                }
            },{
                text:'删除',
                iconCls:'icon-remove',
                handler:function(){
                    var selections = $('#grid_fronts').datagrid('getChecked');
                    for(var i = 0; i < selections.length; i++){
                        var rowData = selections[i];
                        if(rowData.uuid){
                            var idObj = new Object();
                            idObj.uuid = rowData.uuid;
                            delFronts.push(idObj);
                        }
                        var rowIndex = $('#grid_fronts').datagrid('getRowIndex', rowData);
                        $('#grid_fronts').datagrid('deleteRow', rowIndex);
                    }
                }
            }];

        var cityloader = function(param,success,error){
            $.ajax({
                url: '/getCities/-1',
                dataType: 'json',
                success: function(data){
                    var items = $.map(data.data, function(item){
                        return {
                            city:item.uuid,
                            city_name:item.name
                        };
                    });
                    success(items);
                },
                error: function(){
                    error.apply(this, arguments);
                }
            });
        }


        var editIndex = undefined;
        function endEditing(){
            if (editIndex == undefined){return true}
            if ($('#grid_fronts').datagrid('validateRow', editIndex)){
                var ed = $('#grid_fronts').datagrid('getEditor', {index:editIndex,field:'city'});
                if(ed){
                    $('#grid_fronts').datagrid('getRows')[editIndex].city_name = $(ed.target).combobox('getText');
                    $('#grid_fronts').datagrid('getRows')[editIndex]['city'] = $(ed.target).combobox('getValue');
                }
                $('#grid_fronts').datagrid('endEdit', editIndex);
                editIndex = undefined;
                return true;
            } else {
                return false;
            }
        }

        function onDblClickRow(index,field,value){
            if (editIndex != index){
                if (endEditing()){
                    $('#grid_fronts').datagrid('selectRow', index)
                            .datagrid('beginEdit', index);
                    editIndex = index;
                } else {
                    $('#grid_fronts').datagrid('selectRow', editIndex);
                }
            }
        }

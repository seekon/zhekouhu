$(function(){
  $('#btnLogout').on('click', function(){
			$.ajax({
        url: '/logout',
        type: 'post',
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.success){
            $.messager.alert('成功','退出成功.\n页面将返回.','info');
            window.location.href = "login";
          }else{
            $.messager.alert('失败','退出系统失败.','error');
          }
        },
        error: function(){
          $.messager.alert('失败','退出系统失败.','error');
        }
      });
		});

    $('#btnSave-p').on('click', function(){
			$('#form-pwd').form('submit', {
				onSubmit:function(params){
					return $(this).form('enableValidation').form('validate');
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','修改密码成功.','info');
            $('#old_pwd').textbox('setText', '');
            $('#pwd').textbox('setText', '');
            $('#pwd_conf').textbox('setText', '');
            $('#w-pwd').window('close')
          }else{
            $.messager.alert('失败','修改密码失败：原密码错误.','error');
          }
        }
			});
		});

  $('#btnClear-p').on('click', function(){
			$('#form-pwd').form('clear');
		});
});

$.extend($.fn.validatebox.defaults.rules, {
    equals: {
        validator: function(value,param){
            return value == $(param[0]).val();
        },
        message: '重复输入的密码不一致.'
    },
    account: {
        validator: function(value, param){
            return $.validateEmail(value) || $.validatePhone(value);
        },
        message: '请输入合法的email或手机号.'
    }
});

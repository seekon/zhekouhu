$(function(){
  var type = $.getUrlVar('type');
  if(type == 0){
    $('#parValueRow').css('display','none');
    document.title = "管理促销活动";
    $('#title').html(document.title);
  }else{
    //$('#parValueRow').css('display','block');
  }

  $('#grid_discounts').datagrid({
	  onDblClickRow: function(rowIndex, rowData){
		  location.href = 'editDiscount?uuid=' + rowData.uuid;
	  }
  });

  $('#grid_discounts_del').datagrid({
	  onDblClickRow: function(rowIndex, rowData){
		  location.href = 'editDiscount?uuid=' + rowData.uuid;
	  }
  });

});

var toolbar = [{
                text:'新增',
                iconCls:'icon-add',
                handler:function(){
                    if($('#type').attr('value') == '0'){
                        location.href = "pubActivity" ;
                    }else{
                        location.href = "pubCoupon" ;
                    }
                }
            },{
                text:'删除',
                iconCls:'icon-remove',
                handler:function(){
                    var messString = '是否永久删除所选的优惠券?';
                    var type = $('#type').attr('value');
                    if(type == 0){
                        messString = '是否永久删除所选的促销活动?';
                    }
                    $.messager.confirm('提醒', messString, function(r){
                        if (r){
                            var ids = [];
                            var selections = $('#grid_discounts').datagrid('getChecked');
                            for(var i = 0; i < selections.length; i++){
                                var rowData = selections[i];
                                ids.push(rowData.uuid);
                            }
                            if(ids.length > 0){
                                $.ajax({
                                    url: '/deleteDiscounts',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {ids: $.toJSON(ids)},
                                    success: function(data){
                                        if(data.success){
                                            for(var i = 0; i < selections.length; i++){
                                                var rowData = selections[i];
                                                var rowIndex = $('#grid_discounts').datagrid('getRowIndex', rowData);
                                                $('#grid_discounts').datagrid('deleteRow', rowIndex);
                                            }
                                        }
                                    },
                                    error: function(){
                                        $.messager.alert('失败','删除失败.','error');
                                    }
                                });

                            }
                        }
                    });
                }
            }];

         var discountsLoader = function(param,success,error){
            $.ajax({
                url: '/web/getDiscountsByOwner/' + $('#type').attr('value') + "/0",
                dataType: 'json',
                success: function(data){
                    var items = $.map(data, function(item){
                        var d = new Date();
                        d.setTime(item.start_date);
                        var term = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                        d.setTime(item.end_date);
                        term += " 到 " + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                        return {
                            uuid: item.uuid,
                            publish_date: item.publish_date,
                            content:item.content,
                            category_name:item.category_name,
                            visit_count:item.visit_count,
                            img:item.img,
                            term: term
                        };
                    });
                    success(items);
                },
                error: function(){
                    error.apply(this, arguments);
                }
            });
        }
        function imgFormatter(value,row,index){
            return '<img src=/getImageFile/'+ row.img +' / height="80px">';
        }

        var toolbar2 = [
          /*{
                text:'恢复',
                iconCls:'icon-redo',
                handler:function(){
                    var messString = '是否恢复所选的优惠券?';
                    var type = $('#type').attr('value');
                    if(type == 0){
                        messString = '是否恢复所选的促销活动?';
                    }
                    $.messager.confirm('提醒', messString, function(r){
                        if (r){
                            var ids = [];
                            var selections = $('#grid_discounts_del').datagrid('getChecked');
                            for(var i = 0; i < selections.length; i++){
                                var rowData = selections[i];
                                ids.push(rowData.uuid);
                            }
                            if(ids.length > 0){
                                $.ajax({
                                    url: '/recoverDiscounts',
                                    type: 'post',
                                    dataType: 'json',
                                    data: {ids: $.toJSON(ids)},
                                    success: function(data){
                                        if(data.success){
                                            for(var i = 0; i < selections.length; i++){
                                                var rowData = selections[i];
                                                var rowIndex = $('#grid_discounts').datagrid('getRowIndex', rowData);
                                                //$('#grid_discounts').datagrid('deleteRow', rowIndex);
                                            }
                                        }
                                    },
                                    error: function(){
                                        $.messager.alert('失败','恢复失败.','error');
                                    }
                                });

                            }
                        }
                    });
                }
            }*/];

    var discountsLoader2 = function(param,success,error){
            $.ajax({
                url: '/web/getDiscountsByOwner/' + $('#type').attr('value') + "/1",
                dataType: 'json',
                success: function(data){
                    var items = $.map(data, function(item){
                        var d = new Date();
                        d.setTime(item.start_date);
                        var term = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                        d.setTime(item.end_date);
                        term += " 到 " + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
                        return {
                            uuid: item.uuid,
                            publish_date: item.publish_date,
                            content:item.content,
                            category_name:item.category_name,
                            visit_count:item.visit_count,
                            img:item.img,
                            term: term
                        };
                    });
                    success(items);
                },
                error: function(){
                    error.apply(this, arguments);
                }
            });
        }

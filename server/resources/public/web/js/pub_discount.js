var imageNames = [];
$(function(){
  $('#discount_imgs').diyUpload({
	        url:'/web/uploadDiscountImages',
	        success:function( data ) {
		        //console.info( data );
            var item = new Object();
            item.name = data.file_name;
            imageNames.push(item);
	        },
	        error:function( err ) {
		        //console.info( err );
	        }
        });
  $('#btnPublish').on('click', function(){
			$('#ff').form('submit',{
				onSubmit:function(params){
          var selections = $('#grid_storefronts').datagrid('getChecked');
          if(selections.length == 0){
            $.messager.alert('提示','至少选个一家适用门店.','info');
            return false;
          }
          if(imageNames.length == 0){
            $.messager.alert('提示','请先选择并上传图片.','info');
            return false;
          }

          var items = $.map(selections, function(item){
                        return {
                            uuid: item.uuid,
                            store_id:item.storeId
                        };
                    });
          params.storefronts = $.toJSON(items);
          params.type = $('#type').attr('value');
          params.imageNames = $.toJSON(imageNames);

					return $(this).form('enableValidation').form('validate')
                && (selections.length > 0)
                && (imageNames.length > 0);
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','发布活动信息成功.','info');
            $('#ff').form('clear');
            $('#grid_storefronts').datagrid('unselectAll');
          }else{
            $.messager.alert('失败','发布活动信息失败.','error');
          }
        }
			});
		});

  $('#btnClear').on('click', function(){
			$('#ff').form('clear');
      $('#grid_storefronts').datagrid('unselectAll');
		});

  var type = $('#type').attr('value');
  if(type == 0){
    $('.c_parValue').each(function(){
      $(this).css('display','none');
    });
    document.title = "发布促销活动";
    $('#title').html(document.title);
  }else{
    //$('#parValueRow').css('display','block');
  }
});

        var storefrontLoader = function(param,success,error){
            $.ajax({
                url: '/getValidStoresByCurUser',
                dataType: 'json',
                success: function(data){
                  var items = [];
                  for(var i = 0; i < data.length; i++){
                    var store = data[i];
                    for(var j = 0 ; j < store.storefronts.length; j++){
                      var front = store.storefronts[j];
                      front.storeId = store.uuid;
                      front.storeName = store.name;
                      items.push(front);
                    }
                  }
                  success(items);
                },
                error: function(){
                    error.apply(this, arguments);
                }
            });
        }
        function myformatter(date){
            var y = date.getFullYear();
            var m = date.getMonth()+1;
            var d = date.getDate();
            return y+'-'+(m<10?('0'+m):m)+'-'+(d<10?('0'+d):d);
        }
        function myparser(s){
            if (!s) return new Date();
            var ss = (s.split('-'));
            var y = parseInt(ss[0],10);
            var m = parseInt(ss[1],10);
            var d = parseInt(ss[2],10);
            if (!isNaN(y) && !isNaN(m) && !isNaN(d)){
                return new Date(y,m-1,d);
            } else {
                return new Date();
            }
        }

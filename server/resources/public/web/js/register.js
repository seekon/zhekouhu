$(function(){

  $('#btnRegister').on('click', function(){
			$('#ff').form('submit',{
				onSubmit:function(params){
					return $(this).form('enableValidation').form('validate');
				},
        success: function(data){
          var data = eval('(' + data + ')');
          if(data.uuid){
            $.messager.alert('成功','注册成功.','info');
            $('#ff').form('clear');
          }else if(data['error-type'] == 'user-type'){
            $.messager.alert('失败','此帐号已注册.','error');
          }else{
            $.messager.alert('失败','注册失败.','error');
          }
        }
			});
		});

  $('#btnClear').on('click', function(){
			$('#ff').form('clear');
		});

  $('#code').validatebox({
    required: true,
    onValidate : function(valid){

    }
  });

});

// extend the 'equals' rule
$.extend($.fn.validatebox.defaults.rules, {
    equals: {
        validator: function(value,param){
            return value == $(param[0]).val();
        },
        message: '重复输入的密码不一致.'
    },
    account: {
        validator: function(value, param){
            return $.validateEmail(value) || $.validatePhone(value);
        },
        message: '请输入合法的email或手机号.'
    }
});

$.extend({
  getUrlVars: function(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  },
  getUrlVar: function(name){
    return $.getUrlVars()[name];
  },
  validateEmail: function(str){
    var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    return reg.test(str);
  },
  validatePhone:function(str){
    var reg = /^0?1[3|4|5|8][0-9]\d{8}$/;
    return reg.test(str);
  }
});

Array.prototype.contains = function(item){
    for(i=0;i<this.length;i++){
        if(this[i]==item){return true;}
    }
    return false;
};

/*
* @param {Function} fn 进行迭代判定的函数
* @param more ... 零个或多个可选的用户自定义参数
* @returns {Array} 结果集，如果没有结果，返回空集
*/
Array.prototype.each = function(fn){
  fn = fn || Function.K;
  var a = [];
  var args = Array.prototype.slice.call(arguments, 1);
  for(var i = 0; i < this.length; i++){
    var res = fn.apply(this,[this[i],i].concat(args));
    if(res != null) a.push(res);
  }
  return a;
};

/**
* 得到一个数组不重复的元素集合<br/>
* 唯一化一个数组
* @returns {Array} 由不重复元素构成的数组
*/
Array.prototype.uniquelize = function(){
  var ra = new Array();
  for(var i = 0; i < this.length; i ++){
    if(!ra.contains(this[i])){
      ra.push(this[i]);
    }
  }
  return ra;
};

/**
* 求两个集合的补集
{%example
<script>
var a = [1,2,3,4];
var b = [3,4,5,6];
alert(Array.complement(a,b));
</script>
%}
* @param {Array} a 集合A
* @param {Array} b 集合B
* @returns {Array} 两个集合的补集
*/
Array.complement = function(a, b){
  return Array.minus(Array.union(a, b),Array.intersect(a, b));
};

/**
* 求两个集合的交集
{%example
<script>
var a = [1,2,3,4];
var b = [3,4,5,6];
alert(Array.intersect(a,b));
</script>
%}
* @param {Array} a 集合A
* @param {Array} b 集合B
* @returns {Array} 两个集合的交集
*/
Array.intersect = function(a, b){
  return a.uniquelize().each(function(o){return b.contains(o) ? o : null});
};

/**
* 求两个集合的差集
{%example
<script>
var a = [1,2,3,4];
var b = [3,4,5,6];
alert(Array.minus(a,b));
</script>
%}
* @param {Array} a 集合A
* @param {Array} b 集合B
* @returns {Array} 两个集合的差集
*/
Array.minus = function(a, b){
  return a.uniquelize().each(function(o){return b.contains(o) ? null : o});
};

/**
* 求两个集合的并集
{%example
<script>
var a = [1,2,3,4];
var b = [3,4,5,6];
alert(Array.union(a,b));
</script>
%}
* @param {Array} a 集合A
* @param {Array} b 集合B
* @returns {Array} 两个集合的并集
*/
Array.union = function(a, b){
  return a.concat(b).uniquelize();
};

(ns zhekouhu.auth
  (:use [zhekouhu.db]
        [korma.core])
  (:require [ring.util.response :as resp]
            [pandect.core :as pandect]
            [clojure.data.json :as json]
            [zhekouhu.user :as user]
            [zhekouhu.store :as store]
            [net.cgrand.enlive-html :as html]
  ))

(defn login-path [context]
  ;(println context)
  ;(if context
   ; (str context "/login")
    "/web/login"
   ; )
  )
(defn- home-path [context]
  ;(if context
  ;  (str context "/home")
    "/web/home"
  ;  )
  )

(defn- logged-in? [request]
  (get-in request [:session :user] false))

(defn authenticated? [handler]
  (fn [request]
    ;(println request)
    (let [uri (:uri request)]
      ;(println uri)
      (if (or (logged-in? request)
              (and (= (:request-method request) :get)
              (or (.endsWith uri ".png")
                  (.endsWith uri ".jpg")
                  (.endsWith uri ".css")
                  (.endsWith uri ".js")
                  (.endsWith uri ".ico")
                  (not (.startsWith uri "/web"))))
              );;(.endsWith uri ".png") (.startsWith uri "/app")
        (handler request)
        (if (or (.endsWith uri ".html")
                (.endsWith uri ".htm")
                (.startsWith uri "/web"))
          (resp/redirect (login-path (:context request)))
          {:status 200 :body (json/write-str{:error "请先登录."})}
          )
        )
      )
    )
  )

(defn- encrypt [password] (pandect/md5 password))

(defn- password-is-valid? [password-login password-in-db]
  (or (= password-login password-in-db) (= (pandect/md5 password-login) password-in-db))
  )

(defn login [request]
  ;(println request)
  (let [{{code :code, password :pwd} :params} request
        user (user/get-user code)
        no-error (and (:uuid user) (password-is-valid? password (:pwd user)))]

     (if no-error
       (let [result {:authed true :user (assoc user
                                          :type (if (store/check-store-owner (:uuid user)) "0" "1")
                                          :profile (user/get-user-profile-by-user (:uuid user))
                                          )}]
         {
          :status 200
          :session (assoc (request :session) :user user)
          :body (json/write-str result)
          }
        )
       {
        :status 200
         :body (json/write-str {:authed false, :error-type (if (:uuid user) :pass-error :user-error)})
        }
       )
  )
)

(defn web-login [request]
  (let [{{code :user, password :pass} :params} request
        user (user/get-user code)
        no-error (and (:uuid user) (password-is-valid? password (:pwd user)))]

     (if no-error
       (assoc-in (resp/redirect (home-path (:context request))) [:session :user] user)
       (assoc (resp/redirect (login-path (:context request)))
          :flash {:error-type (if (:uuid user) :pass-error :user-error) :user-id code :pwd password})
       )
  )
)

(html/deftemplate temp-login "public/web/login.html"
                  [{:keys [error-type user-id pwd]}]
                  [:#userid] (if user-id (html/set-attr :value user-id) identity)
                  [:#passid] (if pwd (html/set-attr :value pwd) identity)
                  [:#user-error] (if (= error-type :user-error) (html/remove-class "visi") identity)
                  [:#pass-error] (if (= error-type :pass-error) (html/remove-class "visi") identity)
                  )
(defn login-page [req]
  (resp/content-type (resp/response (temp-login (:flash req))) "text/html;charset=utf-8")
  )

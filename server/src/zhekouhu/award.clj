(ns zhekouhu.award
  (:use
		[korma.core]
		[zhekouhu.db]
	)
)

;获取积分商城兑换商品
(defn get-goods []
  (select award-goods (where {:is_deleted "0"}))
  )

;获取用户积分明细
(defn get-user-award-detail [user-id offset-val]
  (let [sql (str " select * from ("
                 " select da.uuid, da.award_time ,da.coin_val,da.`comment`,da.discount_id as data_id "
                 " from z_discount_award da where da.user_id = ? "
                 " union all "
                 " select ao.uuid, ao.order_time,-ao.coin_val,(select name from z_award_goods ag where ag.uuid = ao.goods_id),ao.goods_id "
                 " from z_award_order ao where ao.user_id = ? "
                 " ) a order by a.award_time limit " def-page-size " OFFSET " offset-val)
        params [user-id user-id]]
    (exec-raw [sql params] :results)
    )
  )

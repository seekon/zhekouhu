(ns zhekouhu.cache
  (:use
		[korma.core]
		[zhekouhu.db]
    [clojure.tools.logging]
	)
  (:require [taoensso.carmine :as car :refer (wcar)]
            [clojure.data.json :as json])
)

(def redis-server-conn {:pool {} :spec {:host "127.0.0.1" :port 6379}})

(defmacro wcar* [& body] `(car/wcar redis-server-conn ~@body))

;redis key 最新活动id列表
(defn- get-newest-discounts-key [city]
  (str "newest.discounts." city)
  )

;redis key 具有优惠券的店铺列表
(defn- stores-has-coupon-key [city]
  (str "stores.has.coupon." city)
  )
;redis key 存储具有优惠券的店铺hash
(defn- coupon-store-key [uuid]
  (str "coupon.store." uuid)
  )

;检测redis是否可用
(defn- is-redis-available []
  (try
    (if (= (wcar* (car/ping)) "PONG")
      true
      false
      )
    (catch Exception e
      (error e)
      false
      )
    )
  )

;从redis中获取最新的活动记录列表
(defn- get-cached-discount-ids [city offset-val]
  (let [key (get-newest-discounts-key city)]
   (if (== (wcar* (car/zcard key)) 0 )
    nil
    (if-let [aa (wcar* (car/zrevrange key offset-val (+ (Integer/valueOf offset-val) 20)))]
      aa
      nil
      )
    ))
  )

;从数据库中获取活动记录列表
(defn- get-newest-activity-ids-db [city offset-val]
  (let [current-time (System/currentTimeMillis)]
    (select discounts (fields [:uuid :discount_id] :publish_time) (modifier "DISTINCT")
            (join :inner storefronts (and (= :z_storefront.city_name city)
                                          (= :z_storefront.is_deleted "0")))
            (join :inner discount-storefronts {:uuid :z_discount_storefront.discount_id
                                               :z_discount_storefront.is_deleted "0"})
            (where {:type "0" :status "1" :end_date [>= current-time]
                    :z_storefront.uuid :z_discount_storefront.storefront_id
                    })
            (order :z_discount.publish_time :desc)
            (limit 20)
            (offset offset-val)
          )
    )
  )

;缓存最新的discount‘id
(defn- cache-newest-activity-id [uuid publish-time city]
  (let [key (get-newest-discounts-key city)]
    (wcar* (car/zadd key publish-time uuid))
    (if (== (wcar* (car/zcard key)) 1)
      (wcar* (car/expire key (* 24 60 60))) ;设置有效期为一天
      )
    )
  )

;获取最新的活动id列表
(defn get-newest-activity-ids [city offset-val]
  (if (is-redis-available)
   (if-let [cached-ids (get-cached-discount-ids city offset-val)]
    cached-ids
    (let [ids (get-newest-activity-ids-db city offset-val)]
     (loop [id-list ids]
      (when (> (count id-list) 0)
        (cache-newest-activity-id (:discount_id (first id-list)) (:publish_time (first id-list))  city)
        (recur (rest id-list))
        )
      )
      (get-cached-discount-ids city offset-val)
      )
     )
    (get-newest-activity-ids-db city offset-val)
    )

  )

(defn- get-cities-by-discount [discount]
  (select storefronts
          (fields :city_name)
          (modifier "DISTINCT")
          (join :inner discount-storefronts {:uuid :z_discount_storefront.storefront_id
                                             :z_discount_storefront.is_deleted "0"})
          (where {:z_discount_storefront.discount_id (:uuid discount)
                  :is_deleted "0"})
     )
  )

;往最新的活动set中缓存id
(defn cache-newest-activity [discount]
  ;(println discount)
  (if (is-redis-available)
   (loop [cities (get-cities-by-discount discount)]
    (when (> (count cities) 0)
      (cache-newest-activity-id (:uuid discount) (:publish_time discount) (:city_name (first cities)))
      (recur (rest cities))
      )
    ))
  )

;缓存中清除discount‘id
(defn uncache-newest-activity [discount]
  (if (is-redis-available)
    (loop [cities (get-cities-by-discount discount)]
      (when (> (count cities) 0)
        (wcar* (car/zrem (get-newest-discounts-key (:city_name (first cities))) (:uuid discount)))
        (recur (rest cities))
        )
      )
    )
  )

;从缓存中获取存在优惠券的店铺列表记录
(defn- get-stores-has-coupon-cache
  ([city cate-id]
  (let [key (str (stores-has-coupon-key city) "." cate-id)]
   (if (and (is-redis-available) (> (wcar* (car/scard key)) 0))
     (loop [ids (wcar* (car/smembers key)) result []]
       (if (== (count ids) 0)
         result
         (let [store-data (wcar* (car/hgetall* (coupon-store-key (first ids))))]
           (if (store-data "store_id")
             (recur (rest ids) (conj result (assoc store-data "category_id" cate-id)))
             (recur (rest ids) result))
           )
         )
       )
     nil
    )))
    ([city]
    (let [key (stores-has-coupon-key city)]
     (if (and (is-redis-available) (> (wcar* (car/scard key)) 0))
       (loop [ids (wcar* (car/smembers key)) result []]
         (if (== (count ids) 0)
           result
           (recur (rest ids) (concat result (get-stores-has-coupon-cache city (first ids))))
           )
         )
       nil
      ))
     )
  )

(def sql-stores-has-coupon-db
  (str " select distinct d.category_id, s.name, s.logo, s.uuid as store_id "
       " from z_store s"
       " join z_discount d on d.type = '1' and d.status = '1' and d.end_date >= ? "
       " join z_storefront sf on sf.store_id = s.uuid and sf.is_deleted = '0' and sf.city_name = ? "
       " join z_discount_storefront dsf on dsf.store_id = s.uuid and dsf.is_deleted = '0' "
       " where dsf.discount_id = d.uuid and dsf.storefront_id = sf.uuid and s.is_deleted = '0' ")
  )

;从数据库中获取存在优惠券的店铺列表记录
(defn- get-stores-has-coupon-db [city]
  (let [current-time (System/currentTimeMillis)]
    (println "get-stores-has-coupon-db")
    (exec-raw [sql-stores-has-coupon-db [current-time city]] :results)
    )
  )

;缓存具有优惠券的店铺
(defn- cache-coupon-store [city store-data cate-id]
    (if (is-redis-available)
      (let [key-city (stores-has-coupon-key city)
            key-cate (str key-city "." cate-id)
            key-store (coupon-store-key (:store_id store-data))]

        (wcar* (car/sadd key-city cate-id))
        (if (== (wcar* (car/scard key-city)) 1)
          (wcar* (car/expire key-city (* 24 60 60))) ;设置有效期为一天
          )

        (wcar* (car/sadd key-cate (:store_id store-data)))
        (if (== (wcar* (car/scard key-cate)) 1)
          (wcar* (car/expire key-cate (* 24 60 60))) ;设置有效期为一天
          )

        (wcar* (car/hmset* key-store (dissoc store-data :category_id)))
        (wcar* (car/expire key-store (* 24 60 60))) ;设置有效期为一天
      ))
  )

;优惠券店铺从缓存中移除
(defn uncache-coupon-store
  ([city store-data cate-id]
    (if (is-redis-available)
      (let [key-city (stores-has-coupon-key city)
            key-cate (str key-city "." cate-id)
            key-store (coupon-store-key (:store_id store-data))]
        (wcar* (car/hdel key-store :store_id :name :logo))
        (wcar* (car/srem key-cate (:store_id store-data)))
        (if (== (wcar* (car/scard key-cate)) 0)
          (wcar* (car/srem key-city cate-id))
          )
       )
      ))
  ([store-id]
   (if (is-redis-available)
     (loop [cities (select storefronts
                          (fields :city_name)
                          (modifier "DISTINCT")
                          (where {:store_id store-id}))]
       (when (> (count cities) 0)
         (uncache-coupon-store (:city_name (first cities)) {:store_id store-id} nil)
         (recur (rest cities))
         )
       )
    )
   )
  )

;获取存在优惠券的店铺列表记录
(defn get-stores-has-coupon [city]
  (if-let [cached-stores (get-stores-has-coupon-cache city)]
    cached-stores
    (let [stores (get-stores-has-coupon-db city)]
     (if (is-redis-available)
      (loop [store-list stores]
        (when (> (count store-list) 0)
          (cache-coupon-store city (first store-list) (:category_id (first store-list)))
          (recur (rest store-list))
          )
        ))
      stores
      )
    )
  )

;根据优惠券数据刷新缓存店铺
(defmulti recache-stores-has-coupon
  (fn [discount] (class discount)))
(defmethod recache-stores-has-coupon java.lang.String
  [dis-id]
  (if-let [discount (first (select discounts (where {:uuid dis-id :type "1" :status "1"})))]
    (recache-stores-has-coupon discount))
  )

(defmethod recache-stores-has-coupon java.util.Map
  [discount]
  (when (and discount (is-redis-available))
    (loop [store-list (select storefronts
                              (fields :store_id :city_name :z_store.name :z_store.logo :z_discount_storefront.is_deleted)
                              (modifier "DISTINCT")
                              (join :inner discount-storefronts {:z_discount_storefront.storefront_id :uuid
                                                                  })
                              (join :inner stores {:z_store.uuid :store_id})
                              (where {:is_deleted "0"
                                      :z_discount_storefront.discount_id (:uuid discount)})
                          )]
      (when (> (count store-list) 0)
        (let [store-data (first store-list)
              is-del (:is_deleted store-data)
              city (:city_name store-data)
              store  (dissoc store-data :city_name :is_deleted)]
          (if (= is-del "0")
            (cache-coupon-store city store (:category_id discount))
            (uncache-coupon-store city store (:category_id discount))
            )
          (recur (rest store-list))
         )
        )
      )
    )
  )

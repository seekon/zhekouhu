(ns zhekouhu.category
  (:use
		[korma.core]
		[zhekouhu.db]
	)
)

(defn get-categories [update-time]
   (select categories (where {:last_modify_time [> update-time]}) (order :ord_index))
  )

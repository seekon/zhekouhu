(ns zhekouhu.db
  (:use [korma.db]
        [korma.core]))

(defdb db-mysql (mysql {:host "localhost" :port 3306 :db "zhekouhu" :user "liuxy" :password "zhekouhu<123>"}))

(declare  users categories logs stores storefronts discounts discount-imgs discount-visits comments user-favorits user-profiles)

(def def-page-size 15)

(defentity users
  (table :z_user)
  (pk :uuid)
  (has-one user-profiles {:fk :user_id})
  (has-many user-favorits {:pk :user_id})
)

(defentity user-profiles
  (table :z_user_profile)
  (pk :uuid)
)

(defentity user-favorits
  (table :z_user_favorit)
  (pk :uuid)
)

(defentity categories
  (table :z_category)
  (pk :uuid)
)

(defentity cities
  (table :z_city)
  (pk :uuid)
  )

(defentity logs
  (table :z_log)
  (pk :uuid)
)

(defentity stores
  (table :z_store)
  (pk :uuid)
  (has-many storefronts {:fk :store_id})
)

(defentity storefronts
  (table :z_storefront)
  (pk :uuid)
  )

(defentity discounts
  (table :z_discount)
  (pk :uuid)
  (has-many discount-imgs {:fk :discount_id})
  (has-one categories {:fk :category_id})
  (many-to-many storefronts :z_discount_storefront {:lfk :discount_id :rfk :storefront_id})
  (has-many comments {:fk :discount_id})
)

(defentity discount-imgs
  (table :z_discount_img)
  (pk :uuid)
  )

(defentity discount-visits
  (table :z_discount_visit)
  (pk :uuid)
  )

(defentity discount-storefronts
  (table :z_discount_storefront)
  (pk :uuid)
  )

(defentity discount-malls
  (table :z_discount_mall)
  (pk :uuid)
  )

(defentity comments
  (table :z_discount_comment)
  (pk :uuid)
  )

(defentity feedbacks
  (table :z_feedback)
  (pk :uuid)
  )

(defentity versions
  (table :z_version)
  (pk :uuid)
  )

(defentity discount-awards
  (table :z_discount_award)
  (pk :uuid)
  )

(defentity award-goods
  (table :z_award_goods)
  (pk :uuid)
  )

(defentity award-orders
  (table :z_award_order)
  (pk :uuid)
  )

(defentity store-types
  (table :z_store_type)
  (pk :uuid)
  )

(defentity mall-funcs
  (table :z_mall_func)
  (pk :uuid)
  )

(defentity mall-floors
  (table :z_mall_floor)
  (pk :uuid)
  )

(defentity malls
  (table :z_mall)
  (pk :uuid)
  )

(defentity areas
  (table :z_area)
  (pk :uuid)
  )

(defentity footprints
  (table :z_footprint)
  (pk :uuid)
  )

(defentity products
  (table :z_product)
  (pk :uuid)
  )

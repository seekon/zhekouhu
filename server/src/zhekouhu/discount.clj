(ns zhekouhu.discount
  (:use
		[korma.core]
    [korma.db]
		[zhekouhu.db]
		)
  (:require
    [zhekouhu.date :as date]
    [zhekouhu.file :as file]
    [zhekouhu.store :as store]
    [zhekouhu.cache :as cache]
    [zhekouhu.indexer :as indexer]
    [zhekouhu.searcher :as searcher]
    [net.cgrand.enlive-html :as html]
    [ring.util.response :as resp]
    [clojure.tools.logging :as log]
	)
)

;将活动数据放入缓存并建立索引
(defmulti cache-and-index-discount
  (fn [discount] (class discount)))
(defmethod cache-and-index-discount java.lang.String
  [dis-id]
  (if-let [discount (first (select discounts (where {:uuid dis-id})))]
    (cache-and-index-discount discount))
  )
(defmethod cache-and-index-discount java.util.Map
  [discount]
  ;(println discount)
  (try
     (let [status (:status discount)]
       (cond
        (= status "1") (let [type (:type discount)]
                      (if (= type "0")
                        (cache/cache-newest-activity discount)
                        (cache/recache-stores-has-coupon discount)
                      )
                      (indexer/index-discount-data (:uuid discount));建立索引
                     )
        (= status "9") (let [type (:type discount)]
                      (if (= type "0")
                        (cache/uncache-newest-activity discount)
                        (cache/recache-stores-has-coupon discount));redis缓存
                      (searcher/del-index searcher/type-discount discount);清除索引
                    )
       )
       )
      (catch Exception e
        (log/error e))
      (finally
       discount)
    )
  )

;组织获取折扣信息，包含：discounts（活动列表）、discount_storefronts（活动门店对应列表）、images(图片列表)、storefronts（门店列表）、stores（店铺列表）
(defn- get-discounts [where-map update-time is-owner]
  {:discounts  (select discounts (where (merge where-map {:last_modify_time [> update-time]})))
   :discount_storefronts (select discount-storefronts
                                 (where {:discount_id [in (subselect discounts
                                                                     (fields :uuid)
                                                                     (where (merge where-map {:last_modify_time [> update-time]
                                                                                              :status [not= "9"]
                                                                                              })))] }))
   :images (select discount-imgs (where {:discount_id [in (subselect discounts (fields :uuid)
                                                                     (where (merge where-map {:last_modify_time [> update-time]
                                                                                              :status [not= "9"]
                                                                                              })))] }))
   :storefronts
   (if is-owner
     []
     (select storefronts (where {:is_deleted "0" :uuid [in (subselect discount-storefronts
                                                      (fields :storefront_id) (modifier "DISTINCT")
                                                      (where {:discount_id [in (subselect discounts
                                                                                          (fields :uuid)
                                                                                          (where (merge where-map {:last_modify_time [> update-time]
                                                                                                                   :status [not= "9"]
                                                                                                                   })))] }))]}))
     )
   :stores
   (if is-owner
     []
     (select stores (where {:is_deleted "0" :uuid [in (subselect discount-storefronts
                                                 (fields :store_id) (modifier "DISTINCT")
                                                 (where {:discount_id [in (subselect discounts
                                                                                     (fields :uuid)
                                                                                     (where (merge where-map {:last_modify_time [> update-time]
                                                                                                              :status [not= "9"]
                                                                                                              })))] }))]}))
     )
   }
  )

(defn get-activities [category update-time]
  (get-discounts {:type "0" :category_id category} update-time false)
  )

(defn get-coupons [category update-time]
  (get-discounts {:type "1" :category_id category } update-time false)
  )

(defn get-activities-by-owner [owner update-time]
  (get-discounts {:type "0" :publisher owner} update-time true)
  )

(defn get-coupons-by-owner [owner update-time]
  (get-discounts {:type "1" :publisher owner} update-time true)
  )

(defn get-discounts-by-owner [owner update-time]
  (get-discounts {:publisher owner} update-time true)
  )

;获取折扣与门店对应关系
(defn- get-discount-storefront [discount-id storefront-id]
  (if-let [result (first (select discount-storefronts (where {:discount_id discount-id :storefront_id storefront-id})))]
    result
    nil
   )
  )

;保存折扣与门店对应关系
(defn- save-discount-storefront [discount-id ord-index storefront]
  (if-let [discount-front (get-discount-storefront discount-id (:uuid storefront))]
    (do
      (update discount-storefronts (set-fields {:is_deleted "0"})
              (where {:discount_id discount-id :storefront_id (:uuid storefront)}))
      discount-front
      )
    (let [result {:uuid (str (java.util.UUID/randomUUID)) :storefront_id (:uuid storefront) :store_id (:store_id storefront)
                  :ord_index ord-index :discount_id discount-id}]
      (insert discount-storefronts (values result))
      result
      )
    )
  )

;批量保存折扣与门店对应关系
(defn- save-discount-storefronts [discount-id storefronts]
  (loop [fronts storefronts ord-index 0 result []]
    (if (= (count fronts) 0)
      result
      (recur (rest fronts) (inc ord-index) (conj result (save-discount-storefront discount-id ord-index (first fronts))))
      )
   )
  )

;保存活动图片数据库记录
(defn save-discount-image-db
  ([discount-id ord-index image]
    (let [uuid (str (java.util.UUID/randomUUID))
          name (:name image)
          result {:uuid uuid :img name :ord_index ord-index :discount_id discount-id}]
      (insert discount-imgs (values  result ))
      result
     ))
  ([discount-id image]
   (let [count (count (select discount-imgs (fields :uuid) (where {:discount_id discount-id})))]
     (save-discount-image-db discount-id count {:name image})
     )
   )
  )

;保存折扣图片
(defn- save-discount-image [discount-id ord-index image]
    (file/save-image-file (:name image) (:file image))
    (save-discount-image-db discount-id ord-index image)
  )

;批量保存折扣图片
(defn- save-discount-images [discount-id images]
  (if (or (not images) (= (count images) 0))
    []
    (loop [files images ord-index 0 result []]
      (if (= (count files) 0)
        result
        (let [image (first files)]
          (if (:file image)
            (recur (rest files) (inc ord-index)(conj result (save-discount-image discount-id ord-index image)))
            (recur (rest files) ord-index result)
            )
          )
       )
     )
    )
  )

;保存折扣信息
(defn- save-discount [content start-date end-date category-id type publisher par-val origin location]
  (let [uuid (str (java.util.UUID/randomUUID))
			  current-time (System/currentTimeMillis)
        publish-date (date/formatDate current-time)
        status (if (= origin "1") "2" "1")]
    (insert discounts (values {:content content :uuid uuid :start_date start-date :end_date end-date :category_id category-id
                               :type type :publisher publisher :par_value par-val :publish_time current-time :status status
                               :last_modify_time current-time :publish_date publish-date :origin origin :location location}))
    {:uuid uuid :publish_time (str current-time) :publish_date publish-date :status status :type type}
    )
  )

;保存折扣数据，含：折扣基本信息、门店信息、图片信息
(defn save-discount-data [content start-date end-date category-id type publisher par-val origin location storefronts images]
  (transaction
    (let [discount (save-discount content start-date end-date category-id type publisher par-val origin location)
          sf-list (save-discount-storefronts (:uuid discount) storefronts)]
      (cache-and-index-discount (:uuid discount))
      (merge discount {:storefronts sf-list
                     :images (save-discount-images (:uuid discount) images)})
     )
   )
  )

;保存活动图片记录
(defn- save-discount-image-names [discount-id image-names]
  (if (or (not image-names) (= (count image-names) 0))
    []
    (loop [names image-names ord-index 0 result []]
      (if (= (count names) 0)
        result
        (recur (rest names) (inc ord-index)(conj result (save-discount-image-db discount-id ord-index (first names))))
       )
     )
    )
  )

;保存折扣数据，通过web方式访问
(defn save-discount-web [content start-date end-date category-id type publisher par-val storefronts image-names]
  (transaction
   (let [ret-val (save-discount-data content start-date end-date category-id type publisher par-val "0" "" storefronts nil)]
     (save-discount-image-names (:uuid ret-val) image-names)
     ret-val
    )
   )
  )

;删除折扣图片
(defn- delete-discount-image [discount-id img]
  (update discount-imgs (set-fields {:is_deleted "1"}) (where {:discount_id discount-id :img img}))
  {:img img}
  )

;批量删除折扣图片
(defn delete-discount-images [discount-id images]
  (loop [ids images result []]
    (if (= (count ids) 0)
      result
      (recur (rest ids) (conj result (delete-discount-image discount-id (:img (first ids)))))
      )
    )
  )

;删除折扣门店对应
(defn- delete-discount-storefront [discount-id front-id]
  (update discount-storefronts (set-fields {:is_deleted "1"}) (where {:discount_id discount-id :storefront_id front-id}))
  {:storefront_id front-id}
  )

;批量删除折扣门店对应
(defn- delete-discount-storefronts [discount-id front-ids]
  (loop [ids front-ids result []]
    (if (= (count ids) 0)
      result
      (recur (rest ids) (conj result (delete-discount-storefront discount-id (:uuid (first ids)))))
      )
   )
  )

;更新折扣基本信息
(defn- update-discount [uuid content start-date end-date category-id par-val origin location]
  (update discounts (set-fields {:content content :start_date start-date :end_date end-date :category_id category-id
                                 :par_value par-val :last_modify_time (System/currentTimeMillis)
                                 :origin origin :location location})
          (where {:uuid uuid}))
  {:uuid uuid}
  )

;更新折扣数据，含：折扣基本信息、门店信息、图片信息
(defn update-discount-data [uuid content start-date end-date category-id par-val origin location storefronts images del-front-ids del-img-ids]
  (transaction
    (delete-discount-images uuid del-img-ids)
    (delete-discount-storefronts uuid del-front-ids)
    (let [result (merge (update-discount uuid content start-date end-date category-id par-val origin location)
           {:storefronts (save-discount-storefronts uuid storefronts)
            :images (save-discount-images uuid images)
            })]
      (cache-and-index-discount (:uuid result))
      result
      )
   )
  )

;删除折扣数据，仅更新状态为9
(defn delete-discount-data [discount-id]
  (transaction
    ;todo 判读清除缓存的店铺信息
    (update discounts (set-fields {:status "9" :last_modify_time (System/currentTimeMillis)}) (where {:uuid discount-id}))
    ;删除折扣数据，仅更新状态为9，对应的图片和门店信息不更新
    ;(update discount-imgs (set-fields {:is_deleted "1"}) (where {:discount_id discount-id}))
    ;(update discount-storefronts (set-fields {:is_deleted "1"}) (where {:discount_id discount-id}))
    (cache-and-index-discount discount-id)
   )
  {:uuid discount-id}
  )

;批量删除折扣数据
(defn delete-discounts [id-list]
  (transaction
   (loop [ids id-list result []]
    (if (= (count ids) 0)
      {:success true :data result}
      (recur (rest ids) (conj result (delete-discount-data (first ids))))
      )
    ))
  )
;记录访问记录
(defn visit-discount [discount-id remote-addr]
  (let [uuid (str (java.util.UUID/randomUUID))
			  current-time (System/currentTimeMillis)
        ]
    (transaction
      (insert discount-visits (values {:uuid uuid :discount_id discount-id :remote_addr remote-addr :action_time current-time}))
      (exec-raw [" update z_discount set visit_count = visit_count + 1, last_modify_time = ? where uuid = ?" [current-time, discount-id]])
     )
    {:uuid uuid}
    )
  )

;根据id获取活动记录，含：discounts、images、storefronts(1.0版使用)
(defmulti get-discount-by-id (fn [id city] (class id)))
(defmethod get-discount-by-id java.lang.String
  [uuid city]
   ;(println uuid)
   (if-let [discount (first (select discounts (where {:uuid uuid})))]
     ;(if (or (< (Long/parseLong (:end_date discount)) (System/currentTimeMillis))
     ;        (= (:status discount) "9"))
     ;  nil
      {:discount discount
        :discount_storefronts (select discount-storefronts (where {:discount_id uuid}))
        :images (select discount-imgs (where {:discount_id uuid}))
        :storefronts (select storefronts
                           (fields :uuid :store_id :name :addr :phone :latitude :longitude :is_deleted :city :city_name :z_city.first_letter)
                           (join :inner cities (= :z_city.uuid :city))
                           (join :inner discount-storefronts
                                 (and (= :z_discount_storefront.discount_id uuid)
                                      (= :z_discount_storefront.storefront_id :uuid)
                                      (= :z_discount_storefront.is_deleted "0")))
                           (where (if city {:is_deleted "0" :city_name city} {:is_deleted "0"}))
                           )
        :stores (select stores
                      (join :inner discount-storefronts
                            (and (= :z_discount_storefront.discount_id uuid)
                                 (= :z_discount_storefront.store_id :uuid)
                                 (= :z_discount_storefront.is_deleted "0")))
                      (where {:is_deleted "0"}))
      }
      ; )
     nil
   )
  )

(defmethod get-discount-by-id java.util.Map
  [id-map city]
  (let [uuid (:discount_id id-map)
        id (if uuid uuid (:uuid id-map))]
    (if id
     (get-discount-by-id id city))
    )
  )

;根据距离远近获取活动记录id列表
(defn- get-activity-ids-by-distance [city lat lng cat-id offset-val]
  (let [current-time (System/currentTimeMillis)
        sql (str " select DISTINCT discount_id, min(geodist_field(?, ?, f.latitude, f.longitude)) as distance "
                  " from z_discount_storefront ds "
                  " join z_discount d on ds.discount_id = d.uuid and d.type = '0' and d.status = '1' and d.category_id = ? and d.end_date >= ? "
                  ;" and d.start_date <= ? "
                  " join z_storefront f on ds.storefront_id = f.uuid and f.city_name = ? and f.is_deleted = '0' "
                  " where ds.is_deleted = '0' "
                  " group by discount_id "
                  " order by distance "
                  " LIMIT " def-page-size " OFFSET " offset-val)]
    ;(println sql)
    (exec-raw [sql [lat lng cat-id current-time city]] :results)
    )
  )

;组装成符合要求的数据包 (1.0版使用)
(defn- assemble-discounts-by-ids [id-list city]
  ;(println (str (count id-list) city))
  (loop [ids id-list
         discounts [] discount-storefronts #{} images #{} storefronts #{} stores #{}]
    (if (== (count ids) 0)
      {:discounts discounts :discount_storefronts discount-storefronts :images images :storefronts storefronts :stores stores}
      (let [first-ids (first ids)
            discount (get-discount-by-id first-ids city)]
        ;(println discount)
        (if ;(and
             discount
                 ;(> (count (:storefronts discount)) 0)
                 ;(> (count (:stores discount)) 0)
                 ;(> (count (:images discount)) 0))
         (recur (rest ids)
               (conj discounts (:discount discount))
               (into discount-storefronts (:discount_storefronts discount))
               (into images (:images discount))
               (into storefronts (:storefronts discount))
               (into stores (:stores discount))
               )
        (recur (rest ids) discounts discount-storefronts images storefronts stores))
       )
      )
    )
  )

;获取活动的有效评论数sql,参数:discount_id
(def sql-comment-count
  (str " select count(1) as count from z_discount_comment dc "
       " where dc.discount_id = ? and dc.is_deleted = '0' and dc.type = '0' "))

;获取门店总数的sql，参数：discount_id，city_name
(def sql-front-count
  (str " select count(1) as count from z_storefront sf "
       " join z_discount_storefront ds on ds.storefront_id = sf.uuid "
       " where ds.discount_id = ? and sf.city_name = ? and sf.is_deleted = '0' and ds.is_deleted = '0' "))

;获取距离最近的门店uuid，参数：lat，lng，discount_id，city_name
(def sql-front-id-distance
  (str " select DISTINCT sf.uuid, geodist_field(?, ?, sf.latitude, sf.longitude) as distance from z_storefront sf "
       " join z_discount_storefront ds on ds.storefront_id = sf.uuid "
       " where ds.discount_id = ? and sf.city_name = ? and sf.is_deleted = '0' and ds.is_deleted = '0' "
       " order by distance ")
  )

;根据id获取活动记录，并不包含所有店铺、门店、图片和评论
;包含discount,commentCount,frontCount,fronts(一条最近的记录),images(一条记录),
(defmulti part-get-discount-by-id (fn [id city lat lng] (class id)))
(defmethod part-get-discount-by-id java.lang.String
  [uuid city lat lng]
   (if-let [discount (first (select discounts (where {:uuid uuid})))]
     (let [front (first (exec-raw [sql-front-id-distance [lat lng uuid city]] :results))]
      (assoc discount
       :images (select discount-imgs
                       (fields :uuid :img :ord_index)
                       (where {:discount_id uuid :is_deleted "0"})
                       (order :ord_index))
       :storefront (first (select storefronts
                             (fields :uuid :name :store_id [:z_store.name :store_name])
                             (join :inner stores {:z_store.uuid :store_id})
                             (where {:uuid (:uuid front)})))
       :commentCount (:count (first (exec-raw [sql-comment-count [uuid]] :results)))
       :frontCount (:count (first (exec-raw [sql-front-count [uuid city]] :results)))
       :distance (:distance front) ))
     nil
   )
  )

(defmethod part-get-discount-by-id java.util.Map
  [id-map city lat lng]
  (let [uuid (:discount_id id-map)
        id (if uuid uuid (:uuid id-map))]
    (if id
     (part-get-discount-by-id id city lat lng))
    )
  )

;部分组装活动记录，主要用于获取活动列表的请求中，并不需要获取所有的信息
;1）最新活动列表 2）分类目录下得活动列表 3）商家的优惠券列表 4）搜索
(defn- part-assemble-discounts-by-ids [id-list city lat lng]
  (loop [ids id-list result []]
    (if (= (count ids) 0)
      result
      (recur (rest ids) (conj result (part-get-discount-by-id (first ids) city lat lng)))
      )
    )
  )

;根据距离远近获取活动记录列表
(defn get-activities-by-distance [city lat lng cat-id offset-val version]
  (if (> version 1)
    (part-assemble-discounts-by-ids (get-activity-ids-by-distance city lat lng cat-id offset-val) city lat lng)
    (assemble-discounts-by-ids (get-activity-ids-by-distance city lat lng cat-id offset-val) city)
    )
  )

;获取最新的活动列表，按发布时间排序
(defn get-newest-activities [city lat lng offset-val]
  (let [ids (cache/get-newest-activity-ids city offset-val)]
   (if (and lat lng)
     (part-assemble-discounts-by-ids ids city lat lng)
     (assemble-discounts-by-ids ids city)
     ))
 )

;获取存在优惠券的店铺列表
(defn get-stores-has-coupon [city]
  (cache/get-stores-has-coupon city)
  )

;获取店铺的优惠券id列表（兼容1.0）
(defn- get-coupon-ids-by-store [store-id cate-id city update-time offset-val]
  (let [current-time (System/currentTimeMillis)]
    (if offset-val
      (select discount-storefronts
            (fields :discount_id)
            (modifier "DISTINCT")
            (join :inner storefronts (and (= :z_storefront.is_deleted "0")
                                   (= :z_storefront.city_name city)))
            (join :inner discounts (and (= :z_discount.type "1")
                                 (= :z_discount.status "1")
                                 ;(<= :z_discount.start_date current-time)
                                 (>= :z_discount.end_date current-time)
                                 (> :z_discount.last_modify_time update-time)))
            (where {:store_id store-id :is_deleted "0" :z_storefront.uuid :storefront_id :z_discount.uuid :discount_id :z_discount.category_id cate-id})
            (order :z_discount.publish_time :desc)
            (limit def-page-size)
            (offset offset-val)
            )
      (select discount-storefronts
            (fields :discount_id)
            (modifier "DISTINCT")
            (join :inner storefronts (and (= :z_storefront.is_deleted "0")
                                   (= :z_storefront.city_name city)))
            (join :inner discounts (and (= :z_discount.type "1")
                                 (= :z_discount.status "1")
                                 ;(<= :z_discount.start_date current-time)
                                 (>= :z_discount.end_date current-time)
                                 (> :z_discount.last_modify_time update-time)))
            (where {:store_id store-id :is_deleted "0" :z_storefront.uuid :storefront_id :z_discount.uuid :discount_id :z_discount.category_id cate-id})
            (order :z_discount.publish_time :desc)
            ))
   )
 )

;获取店铺的优惠券列表
(defn get-coupons-by-store [store-id cate-id city lat lng  update-time offset ]
  (if (and lat lng)
    (part-assemble-discounts-by-ids (get-coupon-ids-by-store store-id cate-id city update-time offset) city lat lng)
    (assemble-discounts-by-ids (get-coupon-ids-by-store store-id cate-id city update-time offset) city))
  )

;根据关键字搜索活动和优惠券id列表
;已经废弃，使用索引方式检索
(defn- get-discount-ids-by-keyword-db [city word offset-val]
  (let [current-time (System/currentTimeMillis)
        sword (str "%" word "%")]
    (exec (-> (select* discount-storefronts)
              (fields :discount_id)
              (modifier "DISTINCT")
              (join :inner storefronts (and (= :z_storefront.is_deleted "0")
                                     (= :z_storefront.city_name city)))
              (join :inner discounts (and (= :z_discount.status "1")
                                   ;(<= :z_discount.start_date current-time)
                                   (>= :z_discount.end_date current-time)))
              (where (and (= :z_storefront.uuid :storefront_id) (= :z_discount.uuid :discount_id) (= :is_deleted "0")
                          (or (like :z_storefront.name sword) (like :z_discount.content sword))))
              (order :z_discount.publish_time :desc)
              (limit def-page-size)
              (offset offset-val)
           )
     )
   )
  )

(defn- get-discount-ids-by-keyword [city word offset-val]
  (let [query (str word " AND cities:" city)
        offset (Integer/parseInt offset-val)]
    (if (> (rem offset def-page-size) 0)
      []
      (searcher/search searcher/type-discount query (int (/ offset def-page-size)) def-page-size "publish_time desc" [:uuid])
      )
    )
  )

;根据关键字搜索活动和优惠券列表
(defn get-discounts-by-keyword [city lat lng word offset]
  (if (and lat lng)
    (part-assemble-discounts-by-ids (get-discount-ids-by-keyword city word offset) city lat lng)
    (assemble-discounts-by-ids (get-discount-ids-by-keyword city word offset) city))
  )

;根据活动id获取活动数据
(defn get-discount-data-by-id [id]
  (assemble-discounts-by-ids [{:discount_id id}] nil)
  )

(defn- get-visit-top-discount-ids [city num]
  (let [current-time (System/currentTimeMillis)]
    (exec (-> (select* discount-storefronts)
              (fields :discount_id)
              (modifier "DISTINCT")
              (join :inner storefronts (and (= :z_storefront.is_deleted "0")
                                     (= :z_storefront.city_name city)))
              (join :inner discounts (and (= :z_discount.status "1")
                                   ;(<= :z_discount.start_date current-time)
                                   (>= :z_discount.end_date current-time)))
              (where (and (= :z_storefront.uuid :storefront_id) (= :z_discount.uuid :discount_id) (= :is_deleted "0")
                          ))
              (order :z_discount.visit_count :desc)
              (limit num)
              (offset 0)
           )
     )
   )
  )

;获取访问数top的活动记录
(defn get-visit-top-discounts [city num]
  (let [current-time (System/currentTimeMillis)
        sql (str " select DISTINCT d.*, "
                 " (select img from ( "
                 " select discount_id, img, min(ord_index) from z_discount_img di where di.is_deleted = '0' group by di.discount_id) as a "
                 " where a.discount_id = d.uuid) as img "
                 " ,c.name as category_name "
                 " from z_discount d "
                 " inner join z_discount_storefront ds on d.uuid = ds.discount_id and ds.is_deleted = '0' "
                 " inner join z_storefront sf on ds.storefront_id = sf.uuid and sf.is_deleted = '0' "
                 " LEFT join z_category c on c.uuid = d.category_id "
                 " where d.status = ? and sf.city_name = ? and d.end_date >= ? "
                 " order by visit_count desc "
                 " limit " num  " offset 0")]
    (exec-raw [sql ["1" city current-time]] :results)
    )
  )

(defn- get-discount-imgs-html [images]
  (if images
    (loop [imgs images result "<div class='swipe-wrap'>" index 0]
      (if (> (count imgs) 0)
        (recur (rest imgs) (str result "<div><img src='../getImageFile/" (:img (first imgs)) "'></div>"
                                ) (inc index))

        (str result "</div>")
        )
      )
    )
  )

(defn- get-store-by-id [id stores]
  (if stores
    (loop [stores-data stores]
      (if (and (> (count stores-data) 0) (= (:uuid (first stores-data) id)))
        (first stores-data)
        (if (> (count stores-data) 0)
          (recur (rest stores-data))
          nil
          )
        )
      )
    nil
    )
  )

(defn- get-discount-storefronts-html [storefronts stores]
  (if storefronts
    (loop [fronts storefronts result ""]
      (if (> (count fronts) 0)
        (let [front (first fronts)
              store (get-store-by-id (:store_id front) stores)
              ]
           (recur (rest fronts) (str result "<div class='div-storefront'><div class='div-storefront-name'>"
                                    (if store
                                      (str (:name store) "-" (:name front))
                                      (:name front)
                                      )
                                    "</div> <div class='div-storefront-addr'>" (:addr front) "</div></div>"
                                    "<hr>"))
            )

        result
        )
      )
    )
  )

(html/deftemplate html-discount "public/discount.html"
                  [{:keys [discount images storefronts stores]}]
                  [:head :title] (if (= "0" (:type discount)) (html/content "活动详情") (html/content "优惠券详情"))
                  [:#images] (html/html-content (get-discount-imgs-html images))
                  [:#term] (html/append (str " " (date/formatDate (:start_date discount)) " 到 " (date/formatDate (:end_date discount))))
                  [:#content] (html/content (:content discount))
                  [:#storefronts] (html/html-content (get-discount-storefronts-html storefronts stores))
                 )

;展示活动详细信息
(defn discount-html [id]
  (let [discount-data (get-discount-by-id id nil)]
    ;(println discount-data)
    (html-discount {:discount (:discount discount-data)
                    :images (filter (fn [image]
                                      (= (:is_deleted image) "0")
                                      ) (:images discount-data))
                    :storefronts (filter (fn [front]
                                           (= (:is_deleted front) "0")) (:storefronts discount-data))
                    :stores (:stores discount-data)
                    })
    )
  )

;web端获取店主所发布的活动列表
(defn get-discounts-by-owner-web [owner type del]
  (let [sql (str " select DISTINCT d.*, "
                 " (select img from ( "
                 " select discount_id, img, min(ord_index) from z_discount_img di where di.is_deleted = ? group by di.discount_id) as a "
                 " where a.discount_id = d.uuid) as img "
                 " ,c.name as category_name "
                 ;" ,(select count(1) from z_discount_visit dv where dv.discount_id = d.uuid) as visit_count "
                 " from z_discount d "
                 " LEFT join z_category c on c.uuid = d.category_id "
                 " where status = ? and publisher = ? and type = ? "
                 " order by publish_date")
        params (if (= del "0") ["0" "1" owner type] ["0" "9" owner type])]
    (exec-raw [sql params] :results)
    )
  )

;恢复已经删除的活动数据
(defn recover-discounts [id-list]
  (transaction
   (loop [ids id-list result []]
    (if (= (count ids) 0)
      {:success true :data result}
      ;(recur (rest ids) (conj result (delete-discount-data (first ids))))
      )
    ))
  )

;;v1.1版本新增
;获取距离排序获取门店sql，参数：lat，lng，discount_id，city_name
(def sql-front-distance
  (str " select DISTINCT sf.uuid, sf.name, sf.addr, sf.phone, sf.latitude, sf.longitude, sf.city, sf.city_name "
       " ,sf.store_id, s.name as store_name, ds.uuid as key_id "
       " ,geodist_field(?, ?, sf.latitude, sf.longitude) as distance from z_storefront sf "
       " join z_discount_storefront ds on ds.storefront_id = sf.uuid "
       " join z_store s on sf.store_id = s.uuid "
       " where ds.discount_id = ? and sf.city_name = ? and sf.is_deleted = '0' and ds.is_deleted = '0' "
       " order by distance ")
  )

;获取距离排序获取门店
(defn get-storefronts-by-discount [dis-id city lat lng limit offset]
  (let [sql (str sql-front-distance "limit " limit  " offset " offset)]
    (exec-raw [sql [lat lng dis-id city]] :results)
    )
  )

;发布评论
(defn save-comment [dis-id content publisher type]
  (transaction
   (let [uuid (str (java.util.UUID/randomUUID))
			  current-time (System/currentTimeMillis)]
     (insert comments (values {:uuid uuid :discount_id dis-id :content content :publisher publisher
                               :publish_time current-time :type type :is_deleted "0" :last_modify_time current-time}))
     {:uuid uuid :publish_time current-time}
    )
   )
  )

;获取评论列表
(defn get-comments-by-discount [dis-id limit-val offset-val]
  (select comments (where {:discount_id dis-id :is_deleted "0" :type "0"}) (order :publish_time :desc) (limit limit-val) (offset offset-val))
  )

;收藏/取消收藏
(defn favorit-discount [dis-id user-id action]
  (transaction
   (let [favorit (first (select user-favorits
                                (fields :uuid)
                                (where {:user_id user-id :item_id dis-id :type "4"})))]
    (if (= action "0")
     (let [uuid (str (java.util.UUID/randomUUID))
			    current-time (System/currentTimeMillis)]
       (if favorit
         (update user-favorits (set-fields {:is_deleted "0" :last_modify_time current-time}) (where favorit))
         (insert user-favorits (values {:uuid uuid :user_id user-id :item_id dis-id :type "4"
                                      :is_deleted "0" :last_modify_time current-time})))
       {:uuid uuid :last_modify_time current-time}
       )
     (if favorit
       (let [current-time (System/currentTimeMillis)]
        (update user-favorits (set-fields {:is_deleted "1" :last_modify_time current-time}) (where favorit))
         favorit
         )
       {}
       )
     ))
   )
  )

;获取用户收藏的活动列表
(defn get-user-favorits [user-id update-time city lat lng]
  (if-let [favorits (select user-favorits
                            (fields :uuid [:item_id :discount_id])
                            (where {:user_id user-id :last_modify_time [> update-time] :is_deleted "0" :type "4"} ))]
    (loop [f-list favorits result []]
      (if (== (count f-list) 0)
        result
        (let [favorit (first f-list)
              dis-id (:discount_id favorit)
              discount (part-get-discount-by-id dis-id city lat lng)]
          (recur (rest f-list) (conj result {:favorit favorit
                                             :discount (assoc discount
                                                         :storefront (first (get-storefronts-by-discount dis-id city lat lng 1 0)))}))
          )
       )
      )
    []
    )
  )

(defn get-discount-ext-with-favorit [uuid city]
  {:commentCount (:count (first (exec-raw [sql-comment-count [uuid]] :results)))
   :frontCount (:count (first (exec-raw [sql-front-count [uuid city]] :results)))
   :comments (get-comments-by-discount uuid 3 0)
   }
  )

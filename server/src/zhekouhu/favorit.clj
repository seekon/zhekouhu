(ns zhekouhu.favorit
  (:use
		[korma.core]
    [korma.db]
		[zhekouhu.db]
	)
  )

(def sql-inc-footprint-fav (str " update z_footprint set fav_count = fav_count + 1 where uuid = ? "))
(def sql-des-footprint-fav (str " update z_footprint set fav_count = fav_count - 1 where uuid = ? "))

;收藏/取消收藏
(defn- do-fav-item [user-id item-id type action]
  (transaction
   (let [favorit (first (select user-favorits
                                (fields :uuid)
                                (where {:user_id user-id :item_id item-id :type type})))]
    (if (= action "0")
     (let [uuid (str (java.util.UUID/randomUUID))
			    current-time (System/currentTimeMillis)]
       (if favorit
         (update user-favorits (set-fields {:is_deleted "0" :last_modify_time current-time}) (where favorit))
         (insert user-favorits (values {:uuid uuid :user_id user-id :item_id item-id :type type
                                      :is_deleted "0" :last_modify_time current-time})))
       (if (= type "3")
         (exec-raw [sql-inc-footprint-fav [item-id]])
         )
       {:uuid (if favorit (:uuid favorit) uuid) :last_modify_time current-time}
       )
     (if favorit
       (let [current-time (System/currentTimeMillis)]
         (update user-favorits (set-fields {:is_deleted "1" :last_modify_time current-time}) (where favorit))
         (if (= type "3")
           (exec-raw [sql-des-footprint-fav [item-id]])
           )
         favorit
         )
       {}
       )
     ))
   )
  )

;收藏
(defn fav-item [user-id item-id type]
  (do-fav-item user-id item-id type "0")
  )

;取消收藏
(defn unfav-item [user-id item-id type]
  (do-fav-item user-id item-id type "1")
  )

;检查是否收藏
(defn is-faved [user-id item-id type]
  (if-let [favorit (first (select user-favorits
                                (fields :uuid)
                                (where {:user_id user-id :item_id item-id :type type :is_deleted "0"})))]
    true
    false
    )
  )

;清空用户的收藏
(defn delete-user-fav [user-id type]
  (let [current-time (System/currentTimeMillis)]
    (update user-favorits
            (set-fields {:is_deleted "1" :last_modify_time current-time})
            (where {:user_id user-id :type type}))
    {:last_modify_time current-time}
  ))


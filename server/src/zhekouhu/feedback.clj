(ns zhekouhu.feedback
  (:use
		[korma.core]
		[zhekouhu.db]
    [zhekouhu.date]
	)
)

(defn save-feedback-data [content contact]
  (let [uuid (str (java.util.UUID/randomUUID))
        fd-time (formatTime (System/currentTimeMillis))
        ]
    (insert feedbacks (values {:uuid uuid :content content :contact contact :fd_time fd-time}))
    {:uuid uuid}
    )
 )

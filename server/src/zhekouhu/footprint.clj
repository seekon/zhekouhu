(ns zhekouhu.footprint
  (:use
		[korma.core]
		[zhekouhu.db]
	)
  (:require
		[zhekouhu.file :as file]
   )
)

(def sql-base-footprints (str " select fp.*, up.photo, "
                               " (case when (select count(1) from z_user_favorit f "
                               "  where f.item_id = fp.uuid and f.type = '3' and f.user_id = ? and f.is_deleted='0') > 0 "
                               " then 'true' else 'false' end) as is_fav "
                               " from z_footprint fp "
                               " LEFT join z_user_profile up on fp.user_id = up.user_id "
                               ))

(defn get-my-footprints [user-id update-time]
  (exec-raw [(str sql-base-footprints " where fp.user_id = ? and fp.last_modify_time > ?")
             [user-id user-id update-time]] :results)
  )

(def sql-inc-mall-footprint (str " update z_mall set footprint_count = footprint_count + 1 where uuid = ? "))
(def sql-des-mall-footprint (str " update z_mall set footprint_count = footprint_count - 1 where uuid = ? "))
(def sql-inc-sf-footprint (str " update z_storefront set footprint_count = footprint_count + 1 where uuid = ? "))
(def sql-des-sf-footprint (str " update z_storefront set footprint_count = footprint_count - 1 where uuid = ? "))

(defn add-footprint [type user-id item-id item-logo foot-type foot-pic content pic-file]
  (let [uuid (str (java.util.UUID/randomUUID))
        current-time (str (System/currentTimeMillis))
        ]
    (insert footprints (values {:uuid uuid :type type :user_id user-id :item_id item-id :item_logo item-logo
                                :foot_type foot-type :foot_pic foot-pic :content content
                                :create_time current-time :last_modify_time current-time}))
    (if foot-pic
      (file/save-image-file foot-pic pic-file)
      )
    (if (= type "2")
      (exec-raw [sql-inc-sf-footprint [item-id]])
      (exec-raw [sql-inc-mall-footprint [item-id]])
      )
    {:uuid uuid :create_time current-time}
    )
  )

(defn del-footprint [uuid]
  (let [fp (first (select footprints (where {:uuid uuid})))]
    (if (= (:type fp) "2")
      (exec-raw [sql-des-sf-footprint [(:item_id fp)]])
      (exec-raw [sql-des-mall-footprint [(:item_id fp)]])
     )
    (delete footprints (where {:uuid uuid}))
    {:uuid uuid}
   )
  )

(defn del-user-footprints [user-id]
  (loop [fps (select footprints (fields :uuid) (where {:user_id user-id}))]
    (if (= (count fps) 0)
      {:last_modify_time (System/currentTimeMillis)}
      (let [fp (first fps)]
        (del-footprint (:uuid fp))
        ;(println (:uuid fp))
        (recur (rest fps))
        ))
    )
  )

(defn my-fav-footprints [user-id update-time]
  (select footprints
          (fields :uuid :type :user_id :item_id :item_logo :foot_type :foot_pic :content :create_time :fav_count [:z_user_favorit.uuid :fav_id])
          (modifier "DISTINCT")
          (join :inner user-favorits {:z_user_favorit.item_id :uuid
                                      :z_user_favorit.type "3"
                                      :z_user_favorit.is_deleted "0"
                                      :z_user_favorit.user_id user-id})
          (where {:last_modify_time [> update-time]})
          )
  )

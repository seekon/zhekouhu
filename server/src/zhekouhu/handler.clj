(ns zhekouhu.handler
  (:use [compojure.core]
        )
  (:require [ring.middleware.gzip :as gzip]
            [compojure.handler :as handler]
            [compojure.route :as route]
	          [clojure.data.json :as json]
            [ring.util.response :as response]
            [ring.middleware.cookies]
            [clj-http.lite.client :as client]
            [zhekouhu.auth :as auth]
            [zhekouhu.user :as user]
            [zhekouhu.category :as category]
            [zhekouhu.discount :as discount]
            [zhekouhu.store :as store]
            [zhekouhu.log :as log]
            [zhekouhu.file :as file]
            [zhekouhu.feedback :as feedback]
            [zhekouhu.version :as version]
            [zhekouhu.date :as date]
            [zhekouhu.web :as web]
            [zhekouhu.award :as award]
            [zhekouhu.mall :as mall]
            [zhekouhu.footprint :as footprint]
            [zhekouhu.favorit :as fav]
            [zhekouhu.product :as product]
	))

;(def backend_root_url "http://www.zhekouhu.com/backend")
(def backend_root_url (:backend_root_url (file/load-conf)))

(def data_for_audit (:data_for_audit (file/load-conf)))

;统一的json输出
(defn- json-out [json-str]
  (json/write-str json-str)
  )

;;对返回结果包装最后更新时间的信息
(defn- wrapper-update-data [result update-time]
  (json-out (assoc {} :data result :update_time update-time))
  )

;将params中的附件转换为统一的map
(defn- get-files [params]
  (let [filenames (:fileNameList params)]
    (if-not filenames
      nil
      (loop [image-names (clojure.string/split filenames #"[|]") result []]
        (if (= (count image-names) 0)
          result
          (let [name (first image-names)]
            (if name
              (recur (rest image-names) (conj result {:name name :file (:tempfile (params name))}))
              (recur (rest image-names) result)
              )
           )
          )
        )
      )
    )
 )

(defroutes file-routes
	(GET "/getImageFile/:file-name" [file-name] (file/get-image-file file-name))
)

(defroutes category-routes
  (GET "/getCategories/:update-time" [update-time] (json-out (category/get-categories update-time)))
  )

(defroutes user-routes
  ;修改密码
  (PUT "/updatePwd" {{pwd :pwd uuid :uuid old-pwd :old_pwd} :params}
       (json-out (user/update-pwd uuid old-pwd pwd))
       )
  (POST "/updatePwd" req
       (let [params (:params req)
             pwd (:pwd params)
             uuid (:uuid params)
             old-pwd (:old_pwd params)
             uuid (if uuid uuid (:uuid (:user (:session req))))]
        (json-out (user/update-pwd uuid old-pwd pwd))))
  ;重置密码
  (GET "/resetPwd/:code" [code] (json-out (user/reset-pwd code)))
  ;获取积分
  (GET "/getUserAward/:user-id" [user-id]
       (json-out (user/get-user-award user-id))
       )
  ;获取积分明细
  (GET "/getUserAwardDetail/:user-id/:offset" [user-id offset]
       (json-out (award/get-user-award-detail user-id offset))
       )
  ;更新用户信息
  (PUT "/updateUserProfile" {{uuid :uuid name :name photo :photo phone :phone addr :deliver_addr
                              sex :sex birthday :birthday :as params} :params}
       (println params)
       (json-out (user/update-user-profile uuid name phone addr sex birthday photo (:tempfile (params photo))))
       )
  )

(defroutes discount-routes
  ;(GET "/getActivities/:category/:update-time" [category update-time]
  ;     (json-out (discount/get-activities category update-time)))
  ;(GET "/getCoupons/:category/:update-time" [category update-time]
  ;     (json-out (discount/get-coupons category update-time)))
  ;获取店主发布的促销活动列表（v1.0）
  (GET "/getActivitiesByOwner/:owner/:update-time" [owner update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (discount/get-activities-by-owner owner update-time) current-time))
       )
  ;获取店主发布的优惠券列表（v1.0）
  (GET "/getCouponsByOwner/:owner/:update-time" [owner update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (discount/get-coupons-by-owner owner update-time) current-time))
       )
  ;获取个人发布的活动列表（含优惠券，v1.1）
  (GET "/getDiscountsByOwner/:owner/:update-time" [owner update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (discount/get-discounts-by-owner owner update-time) current-time))
       )
  ;发布促销活动或优惠券
  (POST "/publishDiscount" {{content :content start-date :start_date end-date :end_date type :type publisher :publisher
                             par-val :par_value category-id :category_id storefronts :storefronts
                             origin :origin location :location :as params} :params}
        (json-out (discount/save-discount-data content start-date end-date category-id type publisher par-val
                                               (if origin origin "0") (if location location "")
                                               (json/read-json storefronts) (get-files params)))
        )
  ;更新促销活动或优惠券
  (PUT "/updateDiscount" {{uuid :uuid content :content start-date :start_date end-date :end_date
                           par-val :par_value category-id :category_id storefronts :storefronts origin :origin location :location
                           del-front-ids :del_storefronts del-img-ids :del_images :as params} :params}
       (json-out (discount/update-discount-data uuid content start-date end-date category-id par-val
                                               (if origin origin "0") (if location location "")
                                               (json/read-json storefronts) (get-files params)
                                               (json/read-json del-front-ids) (json/read-json del-img-ids)))
       )
  ;删除促销活动或优惠券
  (DELETE "/deleteDiscount/:uuid" [uuid] (json-out (discount/delete-discount-data uuid)))
  ;评论删除活动或优惠券
  (POST "/deleteDiscounts" {{ids :ids} :params}
        (json-out (discount/delete-discounts (json/read-json ids)))
        )

  ;根据距离和分类获取促销活动（兼容1.0版本）
  (GET "/getActivitiesByDistance/:cate-id/:offset/:city/:lat/:lng" [cate-id offset city lat lng]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-activities-by-distance city lat lng cate-id offset 1) current-time))
        )
  ;根据距离和分类获取促销活动（1.1版本）
  (GET "/getActivitiesByDistance/:cate-id/:offset/:city/:lat/:lng/:version" [cate-id offset city lat lng version]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-activities-by-distance city lat lng cate-id offset (Integer/valueOf version)) current-time))
        )

  ;获取最新的活动列表（兼容1.0版本）
  (GET "/getNewestActivities/:city/:offset" [city offset]
       (let [current-time (str (System/currentTimeMillis))
             city  (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-newest-activities city nil nil offset) current-time))
        )

  ;获取最新的活动列表（1.1版本）
  (GET "/getNewestActivities/:city/:lat/:lng/:offset" [city lat lng offset]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-newest-activities city lat lng offset) current-time))
        )

  ;获取活动的门店记录，按距离排序
  (GET "/getStorefrontsByDiscount/:dis-id/:city/:lat/:lng/:limit/:offset" [dis-id city lat lng limit offset]
       (json-out (discount/get-storefronts-by-discount dis-id city lat lng limit offset))
       )
  ;获取活动的评论列表
  (GET "/getCommentsByDiscount/:dis-id/:limit/:offset" [dis-id limit offset]
       (json-out (discount/get-comments-by-discount dis-id limit offset))
       )
  ;获取本城市具有优惠券的店铺列表
  (GET "/getStoresHasCoupon/:city" [city]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-stores-has-coupon city) current-time))
        )

  ;获取店铺的优惠券列表（兼容1.0版）
  (GET "/getCouponsByStore/:store-id/:cate-id/:city/:update-time" [store-id cate-id city update-time]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-coupons-by-store store-id cate-id city nil nil update-time nil) current-time))
        )
  ;获取店铺的优惠券列表（1.1版）
  (GET "/getCouponsByStore/:store-id/:cate-id/:city/:lat/:lng/:offset" [store-id cate-id city lat lng offset]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-coupons-by-store store-id cate-id city lat lng -1 offset) current-time))
        )

  ;根据关键字获取活动（含优惠券）列表（兼容1.0版）
  (GET "/getDiscountsByKeyword/:city/:word/:offset" [city word offset]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-discounts-by-keyword city nil nil word offset) current-time))
       )
  ;根据关键字获取活动（含优惠券）列表（1.1版）
  (GET "/getDiscountsByKeyword/:city/:lat/:lng/:word/:offset" [city lat lng word offset]
       (let [current-time (str (System/currentTimeMillis))
             city (if data_for_audit "北京市" city)
             ]
           (wrapper-update-data (discount/get-discounts-by-keyword city lat lng word offset) current-time))
       )

  ;根据活动id获取活动数据
  (GET "/getDiscountById/:id" [id]
       (json-out (discount/get-discount-data-by-id id))
       )
  ;展示活动详情页面
  (GET "/discount/:id" [id] (discount/discount-html id))

  ;获取访问数top的活动记录
  (GET "/getVisitTopDiscounts/:city/:num" [city num]
       (json-out (discount/get-visit-top-discounts city num)))
  )

(defroutes store-routes
  ;新开店铺
  (POST "/registerStore" {{name :name logo :logo owner :owner  fronts :storefronts :as params} :params}
        ;(println fronts)
        (json-out (store/register-store name logo owner (json/read-json fronts) (:tempfile (params logo))))
        )
  ;获取自己所开的店铺列表
  (GET "/getStores/:owner/:update-time" [owner update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (store/get-stores owner update-time true) current-time ))
       )
  ;获取当前登录人员的店铺列表
  (GET "/getValidStoresByCurUser" req
       ;(println (get-in req [:session :user :uuid] nil))
       (if-let [owner (get-in req [:session :user :uuid] nil)]
         (json-out (store/get-stores owner -1 false))
         (json-out "[]")
         )
       )

  ;更新店铺
  (PUT "/updateStore" {{uuid :uuid name :name logo :logo owner :owner  fronts :storefronts :as params} :params}
       (json-out (store/update-store uuid name logo owner (json/read-json fronts) (:tempfile (params logo))))
       )

  ;删除店铺
  (DELETE "/deleteStore/:uuid" [uuid] (json-out (store/delete-store uuid)))

  ;批量删除店铺
  (POST "/deleteStores" {{ids :ids} :params}
        (json-out (store/delete-stores (json/read-json ids)))
        )

  ;删除门店
  (DELETE "/deleteStorefront/:uuid" [uuid] (json-out (store/delete-storefront {:uuid uuid} true)))

  ;更新门店
  (PUT "/updateStorefront" {{data :data} :params}
       (json-out (store/update-storefront (json/read-json data) true))
       )
  ;新增门店
  (POST "/addStorefront" {{data :data} :params}
        (json-out (store/save-storefront (json/read-json data) true))
        )
  )

(defroutes web-routes
  (GET "/web/home" [] (web/html-home))
  (GET "/web/listActivities" [] (web/html-list-discounts {:type 0}))
  (GET "/web/listCoupons" [] (web/html-list-discounts {:type 1}))
  (GET "/web/pubActivity" [] (web/html-pub-discount {:type 0}))
  (GET "/web/pubCoupon" [] (web/html-pub-discount {:type 1}))
  (GET "/web/editDiscount" [] (web/html-edit-discount ))
  (GET "/web/addStore" [] (web/html-add-store))
  (GET "/web/editStore" [] (web/html-edit-store ))
  (GET "/web/listStores" [] (web/html-list-stores))

  ;通过web上传活动图片
  (POST "/web/uploadDiscountImages" req
        (let [uid (:code (:user (:session req)))
              file-name (str uid "_discount_" (str (java.util.UUID/randomUUID)) ".png")]
          ;(println (:params req))
          (file/save-image-file file-name (:tempfile (:file (:params req))))
          (json-out {:file_name file-name})
          )
        )
  ;发布促销活动或优惠券
  (POST "/web/publishDiscount" {{content :content start-date :startDate end-date :endDate type :type
                                 par-val :parValue category-id :categoryId storefronts :storefronts
                                 image-names :imageNames :as params} :params :as req}
        ;(println req)
        (json-out (discount/save-discount-web content (date/parse-time start-date) (date/parse-time end-date)
                                              category-id type (:uuid (:user (:session req)))
                                              (if (and par-val (> (.length par-val) 0)) par-val 0)
                                               (json/read-json storefronts) (json/read-json image-names)))
        )
  ;获取店主的促销活动
  (GET "/web/getDiscountsByOwner/:type/:del" [type del]
       (fn [req]
         (json-out (discount/get-discounts-by-owner-web (:uuid (:user (:session req))) type del))
         )
       )
  ;注册店铺
  (POST "/web/registerStore" {{name :name fronts :storefronts :as params} :params :as req}
        ;(println req)
        (let [uid (:code (:user (:session req))) ]
          (json-out (store/register-store name (str uid "_store_" (str (java.util.UUID/randomUUID)) ".png")
                                          (:uuid (:user (:session req))) (json/read-json fronts) (:tempfile (:logo_file params))))
          )
        )
  ;根据店铺id获取店铺信息
  (GET "/web/getStoreById/:id" [id]
       (json-out (store/get-store-by-id id true))
       )
  ;更新店铺logo
  (POST "/web/updateStoreLogo" {{logo :logo :as params} :params :as req}
       ;(println req)
       (file/save-image-file logo (:tempfile (:logo_file params)))
       (json-out {:logo logo})
       )
  ;更新店铺信息
  (POST "/web/updateStore" {{uuid :uuid name :name logo :logo fronts :storefronts del-fronts :del_fronts} :params :as req}
        ;(println req)
        (json-out (store/update-store-web uuid name logo (:uuid (:user (:session req)))
                                      (json/read-json fronts) nil (json/read-json del-fronts)))
        )
  ;添加活动图片
  (POST "/web/addDiscountImage" {{img :img discount-id :discount_id} :params}
        (json-out (discount/save-discount-image-db discount-id img))
        )
  ;删除活动图片
  (POST "/web/deleteDiscountImages" {{images :images discount-id :discount_id} :params}
        (json-out {:success true :data (discount/delete-discount-images discount-id (json/read-json images))})
        )
  ;更新活动信息
  ;更新促销活动或优惠券
  (POST "/web/updateDiscount" {{uuid :uuid content :content start-date :startDate end-date :endDate
                             par-val :parValue category-id :categoryId storefronts :storefronts
                             del-front-ids :del_storefronts :as params} :params}
       ;(println params)
       (json-out (discount/update-discount-data uuid content (date/parse-time start-date) (date/parse-time end-date)
                                                category-id (if (and par-val (> (.length par-val) 0)) par-val 0)
                                                "0" ""
                                                (json/read-json storefronts) []
                                                (json/read-json del-front-ids) []))
       )
  (POST "/web/recoverDiscounts" {{ids :ids} :params}
        (json-out (discount/recover-discounts (json/read-json ids))))
  )

(defroutes mall-routes
  (GET "/mall/getMallFuncs/:update-time" [update-time] (json-out (mall/get-mall-funcs update-time)))

  (GET "/mall/getMalls/:city/:update-time" [city update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (mall/get-malls city update-time) current-time ))
       )
  (GET "/mall/getMallById/:id/:user-id/:dis" [id user-id dis]
       (json-out (assoc
                   (if (not= "true" dis) (mall/get-mall id) (mall/get-mall-with-dis user-id id))
                   :is_fav (if (= "-1" user-id) false (fav/is-faved user-id id "1")))))

  (GET "/mall/getMallBrands/:id/:type/:update-time" [id type update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (mall/get-mall-brands id type update-time) current-time ))
       )
  (GET "/mall/getBrandsByRange/:type/:range/:city/:lat/:lng" [type range city lat lng]
       (json-out (mall/get-brands-by-range type range city lat lng)))

  (GET "/mall/getBrandDetail/:mall-id/:store-id/:user-id/:dis" [mall-id store-id user-id dis]
       (let [item (mall/get-brand-detail user-id mall-id store-id dis)]
         (json-out (assoc item :is_fav (fav/is-faved user-id (:uuid item) "2")))
         ))
  (GET "/mall/getBrandDetailById/:uuid/:user-id/:dis" [uuid user-id dis]
       (let [item (mall/get-brand-detail user-id uuid dis)]
         (json-out (assoc item :is_fav (fav/is-faved user-id (:uuid item) "2")))
         ))
  (GET "/mall/getBrandDetailInfo/:store-id" [store-id]
       (json-out (mall/get-brand-detail-info store-id)))
  ;添加足迹
  (POST "/mall/addFootprint" {{type :type user-id :user_id item-id :item_id item-logo :item_logo
                               foot-type :foot_type foot-pic :foot_pic content :content :as params} :params}
        ;(println params)
        (json-out (footprint/add-footprint type user-id item-id item-logo foot-type foot-pic content
                                           (:tempfile (params foot-pic))))
        )
  ;删除足迹
  (DELETE "/mall/delFootprint/:uuid" [uuid]
          (json-out (footprint/del-footprint uuid)))
  (DELETE "/mall/delUserFootprints/:user-id" [user-id]
          (json-out (footprint/del-user-footprints user-id)))
  ;获取用户的足迹
  (GET "/mall/getUserFootprints/:user-id/:update-time" [user-id update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (footprint/get-my-footprints user-id update-time) current-time ))
       )
  ;获取商场或品牌的足迹
  (GET "/mall/getFootprintsByMall/:item-id/:type/:foot-type/:user-id/:offset" [item-id foot-type type user-id offset]
       (json-out (mall/get-footprints-by-mall item-id foot-type type user-id offset)))

  (GET "/mall/getFootprintsByRange/:foot-type/:range/:offset/:city/:lat/:lng" [foot-type range offset city lat lng]
       (json-out (mall/get-footprints-by-range foot-type range offset city lat lng)))

  ;收藏
  (POST "/mall/favoritItem" {{user-id :user_id item-id :item_id type :type} :params}
        (json-out (fav/fav-item user-id item-id type)))
  ;取消收藏
  (POST "/mall/unfavoritItem" {{user-id :user_id item-id :item_id type :type} :params}
        (json-out (fav/unfav-item user-id item-id type)))
  ;获取我的收藏
  (GET "/mall/getFavMalls/:user-id/:update-time" [user-id update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (mall/get-my-fav-malls user-id update-time) current-time ))
       )
  (GET "/mall/getFavBrands/:user-id/:update-time" [user-id update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (mall/get-my-fav-brands user-id update-time) current-time ))
       )
  (GET "/mall/getFavFootprints/:user-id/:update-time" [user-id update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (footprint/my-fav-footprints user-id update-time) current-time ))
       )
  (GET "/mall/getFavDiscounts/:user-id/:update-time/:city/:lat/:lng" [user-id update-time city lat lng]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (mall/get-my-fav-discounts user-id update-time city lat lng) current-time ))
       )
  ;获取附近的活动
  (GET "/mall/getDiscountsByRange/:user-id/:range/:offset/:city/:lat/:lng" [user-id range offset city lat lng]
       (json-out (mall/discounts-by-range user-id range offset city lat lng)))

  (GET "/mall/getDiscountsByMall/:user-id/:id/:type/:offset" [user-id id type offset]
       (json-out (mall/discounts-by-mall user-id id type offset)))

  (GET "/mall/getDiscountsByBrand/:user-id/:id/:type/:offset" [user-id id type offset]
       (json-out (mall/discounts-by-brand user-id id type offset)))

  (GET "/mall/getCouponBrandsByMall/:id" [id]
       (json-out (mall/coupon-brands-by-mall id)))

  (GET "/mall/getCouponBrandsByRange/:range/:city/:lat/:lng" [range city lat lng]
       (json-out (mall/coupon-brands-by-range range city lat lng)))

  ;获取活动的商场列表
  (GET "/mall/getMallsByDiscount/:dis-id/:city/:lat/:lng/:limit/:offset" [dis-id city lat lng limit offset]
       (json-out (mall/malls-by-discount dis-id city lat lng limit offset)))

  (DELETE "/mall/delUserFavorits/:user-id/:type" [user-id type]
       (json-out (fav/delete-user-fav user-id type)))
  ;搜索
  (GET "/mall/rangeSearch/:user-id/:range/:word/:city/:lat/:lng/:offset" [user-id range word city lat lng offset]
       (json-out (mall/search-by-range user-id word offset range city lat lng)))

  (GET "/mall/mallSearch/:user-id/:id/:word/:offset" [user-id id word offset]
       (json-out (mall/search-by-mall user-id word offset id)))

  (GET "/mall/brandSearch/:user-id/:id/:word/:offset" [user-id id word offset]
       (json-out (mall/search-by-brand user-id word offset id)))
  )

(def app-routes
  (routes category-routes discount-routes store-routes file-routes user-routes web-routes mall-routes)
  )

(defroutes login-routes
  ;判断用户账号是否已注册
  (GET "/userCodeExits/:code" [code] (json-out {:exits (user/user-code-exits code)}))
  ;注册账户
  (POST "/registerUser" {{code :code pwd :pwd} :params} (json-out (user/register-user code pwd)))
  ;登录
  (POST "/login" request (auth/login request))
  ;退出
  (POST "/logout" request
        (dissoc (request :session) :user)
        {
          :status 200
          :session nil
          :body (json-out {:success true})
          }
        )
  ;网页登陆
  (POST "/web/login" request (auth/web-login request))
  (GET "/web/login" request  (auth/login-page request))
)

(defroutes auth-routes
  (GET "/" request (response/redirect "index.html"))
  (auth/authenticated? app-routes)
  (route/resources "/")
  (route/not-found "Not Found")
)

(defroutes anonymous-routes
  ;提交意见反馈
  (POST "/submitFeedback" {{content :content contact :contact} :params}
        (json-out (feedback/save-feedback-data content contact)))
  ;获取最新的版本
  (GET "/getNewestVersion" []
       (json-out (version/get-newest-version "ZheKouHu")))
  (GET "/getNewestVersion/:code" [code]
       (json-out (version/get-newest-version code)))
  (GET "/getProducts/:update-time" [update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (product/get-products update-time) current-time))
       )
  ;访问活动记录
  (POST "/visitDiscount" {{discount-id :discount_id} :params remote-addr :remote-addr}
        (json-out (discount/visit-discount discount-id remote-addr)))
  ;获取城市列表
  (GET "/getCities/:update-time" [update-time]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (store/get-cities update-time) current-time))
       )
  ;注册移动设备
  (POST "/registerDevice" {{dev-id :dev_id type :type} :params}
        (client/get (str backend_root_url "/registerDevice/" dev-id "/" type) ))
  ;设置移动设备不提醒消息；todo去掉
  (PUT "/setUnNotify" {{dev-id :dev_id} :params}
       (client/get (str backend_root_url "/setNotifyState/" dev-id "/0")))
  ;设置通知状态
  (PUT "/setNotifyState" {{dev-id :dev_id status :status} :params}
       (client/get (str backend_root_url "/setNotifyState/" dev-id "/" status)))
  ;发布评论
  (POST "/publishComment" {{dis-id :discount_id content :content publisher :publisher type :type} :params}
        (json-out (discount/save-comment dis-id content publisher type)))
  ;收藏/取消收藏活动
  (POST "/favoritDiscount" {{dis-id :discount_id user-id :user_id type :type} :params}
        (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (discount/favorit-discount dis-id user-id type) current-time))
        )
  ;获取用户的收藏
  (GET "/getUserFavorits/:user-id/:update-time/:city/:lat/:lng" [user-id update-time city lat lng]
       (let [current-time (str (System/currentTimeMillis)) ]
         (wrapper-update-data (discount/get-user-favorits user-id update-time city lat lng) current-time))
       )
  (GET "/getDiscountExtWithFavorit/:dis-id/:city" [dis-id city]
       (json-out (discount/get-discount-ext-with-favorit dis-id city))
       )

  ;匿名发布优惠活动，todo:需进行一些检查
  (POST "/anonymPublishDiscount" {{content :content start-date :start_date end-date :end_date type :type publisher :publisher
                             par-val :par_value category-id :category_id storefronts :storefronts
                             origin :origin location :location :as params} :params}
        (json-out (discount/save-discount-data content start-date end-date category-id type publisher par-val
                                               (if origin origin "0") (if location location "")
                                               (json/read-json storefronts) (get-files params)))
        )
  ;重新缓存并索引活动
  (GET "/recacheAndIndexDiscount/:dis-id" [dis-id]
       (json-out (discount/cache-and-index-discount dis-id))
       )
  ;获取积分商品
  (GET "/getGoods" []
       (json-out (award/get-goods)))

  ;获取配置信息
  (GET "/loadConf" [] (:backend_root_url (file/load-conf)))

  ;web访问部分
  (GET "/about" [] (web/html-intro {:pageIndex 1}))
  (GET "/legal" [] (web/html-intro {:pageIndex 2}))
  (GET "/job" [] (web/html-intro {:pageIndex 3}))
  (GET "/feedback" [] (web/html-intro {:pageIndex 5}))
  (GET "/link" [] (web/html-intro {:pageIndex 6}))
  (GET "/partner" [] (web/html-partner))

  (GET "/web/register" [] (web/html-register))
  )

(def app
  (-> (routes anonymous-routes login-routes auth-routes)
      (log/log-request)
      (handler/site :session)
      (ring.middleware.cookies/wrap-cookies)
      (gzip/wrap-gzip)
      ))

(ns zhekouhu.indexer
  (:use
		[korma.core]
		[zhekouhu.db]
    [clojure.tools.logging]
	)
  (:require [zhekouhu.searcher :as searcher])
)

;新建或更新活动索引
(defn index-discount-data [dis-id]
  (if-let [discount (first (select discounts (fields :uuid :content :publish_time) (where {:uuid dis-id :status "1"})))]
    ;(searcher/del-index searcher/type-discount {:uuid dis-id})
    (searcher/add-index searcher/type-discount (merge discount
                                                      (loop [rows (select storefronts
                                                                       (fields :name :city_name [:z_store.name :store_name])
                                                                       (modifier "DISTINCT")
                                                                       (join :inner stores {:z_store.uuid :store_id})
                                                                       (join :inner discount-storefronts {:z_discount_storefront.storefront_id :uuid
                                                                                                          :z_discount_storefront.discount_id dis-id
                                                                                                          :z_discount_storefront.is_deleted "0"})
                                                                       (where {:is_deleted "0"}))
                                                             fronts []
                                                             cities #{}
                                                             stores #{}]
                                                        (if (> (count rows) 0)
                                                          (let [row (first rows)]
                                                            (recur (rest rows) (conj fronts (:name row)) (conj cities (:city_name row)) (conj stores (:store_name row)))
                                                            )
                                                          {:stores stores :fronts fronts :cities cities}
                                                          )
                                                        )
                                                 ))
    )
  )

;重建所有活动的索引，谨慎使用
(defn- reindex-discounts []
  (loop [rows (select discounts (fields :uuid) (where {:status "1"}))]
    (when (> (count rows) 0)
      (index-discount-data (:uuid (first rows)))
      (recur (rest rows))
      )
    )
  )

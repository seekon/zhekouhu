(ns zhekouhu.location
  (:require [clj-http.lite.client :as client]
            [clojure.data.json :as json]
            )
  )

(def loc-ak "Tg44W5KKibdXbVDCdjA7xWL1")
(def loc-uri "http://api.map.baidu.com/geocoder/v2/")

(defn get-lat-lng [addr city]
  (let [body (json/read-json (:body (client/get loc-uri {:query-params {"ak" loc-ak "output" "json" "address" addr "city" city} :as :json})))]
    (println body)
    (:location (:result body))
    )
  )

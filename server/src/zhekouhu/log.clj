(ns zhekouhu.log
  (:use
		[korma.core]
		[zhekouhu.db]
    [zhekouhu.date]
    [clojure.tools.logging]
    [clojure.java.io]
		)
  (:require [clojure.data.json :as json])
)

(defn- log-file [data]
  (let [file-name (str "log/action_req_" (formatDate (System/currentTimeMillis)) ".log")]
    (with-open [wtr (writer file-name :append true :encoding "utf-8")]
      (.write wtr (str data "\n"))
      )
    )
  )

(defn log-request [handler]
  (fn [request]
    (try
      (let [action  (str (:request-method request))
            current-time (System/currentTimeMillis)
            val-map {:uuid (str (java.util.UUID/randomUUID))
                      :action_time (formatTime current-time)
                      :remote_addr (:remote-addr request)
                      :action_uri (:uri request)
                      :action_content (if (or (= action ":put") (= action ":post")) (str (dissoc (:params request) :pwd)) nil)
                      :user_id (if-let [user (:user (:session request))] (:uuid user) nil)
                    }
            ]
          (insert logs (values (dissoc val-map :action_content)))
          (log-file val-map)
          (handler request)
        )
        (catch Exception e
          (error e (str  "远程访问失败.请求信息：" request))
          {:status  200 :body (json/write-str {:error "远程访问失败."})}
          )
      )
   )
  )

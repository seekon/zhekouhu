(ns zhekouhu.mall
  (:use
		[korma.core]
		[zhekouhu.db]
	)
  (:require [zhekouhu.favorit :as fav]
            [zhekouhu.searcher :as searcher])
)

(declare part-assemble-discounts-by-ids)

(declare get-brand-detail)

(declare get-brands-count-by-range)

(declare coupon-brands-count-by-range)

(declare get-footprints-count-by-range)

(declare coupon-brands-count-by-mall)

(declare discounts-count-by-brand)

(defn get-mall-funcs [update-time]
   (select mall-funcs (where {:last_modify_time [> update-time]}) (order :ord_index))
  )

(defn get-malls [city update-time]
  (select malls
          (fields :uuid :name :logo :city :lat :lng)
          ;(join :inner areas {:area_id :z_area.uuid})
          (join :inner cities {:city :z_city.uuid :z_city.name city})
          (where {:last_modify_time [> update-time]})
          ;(order :z_area.name)
          )
  )

(defn get-mall [id]
  (if-let [mall (first (select malls (where {:uuid id})))]
    mall
    {}
    )
  )

(defn get-mall-brands [id type update-time]
  {
   :floors
   (select mall-floors
           (fields :uuid :mall_id :floor_no :type :description)
           (modifier "DISTINCT")
           (where {:mall_id id :type type :last_modify_time [> update-time]}))
   :store-types
   (select store-types
           (fields :uuid :store_id )
           (modifier "DISTINCT")
           (join :inner storefronts {:z_storefront.is_deleted "0" :z_storefront.mall_id id :z_storefront.store_id :store_id })
           (where {:last_modify_time [> update-time] :type type}))
   :brands
   (select stores
           (fields :uuid :name :logo :z_storefront.floor [:z_storefront.uuid :sf_id] [:z_storefront.name :sf_name])
           (modifier "DISTINCT")
           (join :inner storefronts {:z_storefront.store_id :uuid :z_storefront.is_deleted "0" :z_storefront.mall_id id})
           (join :inner store-types {:z_store_type.store_id :uuid :z_store_type.type type})
           (where (and (= :is_deleted "0")
                      (or (> :last_modify_time update-time) (> :z_storefront.last_modify_time update-time))))
          )
   }
  )

(def sql-brands-by-range (str " select DISTINCT s.uuid, s.name, s.logo, m.uuid as mall_id, m.name as mall_name, m.logo as mall_logo, sf.uuid as sf_id, sf.name as sf_name "
                              " from z_storefront sf "
                              " join z_store s on sf.store_id = s.uuid "
                              " join z_mall m on sf.mall_id = m.uuid "
                              " where sf.city_name = ? and geodist_field(?, ?, sf.latitude, sf.longitude) < ? "
                              " and EXISTS (select uuid from z_store_type st where st.store_id = sf.store_id and st.type= ? ) "
                              ))
(defn- get-brands-count-by-range [type range city-name lat lng]
  (:count (first (exec-raw [(str " select count(1) as count from (" sql-brands-by-range ") aa")
                            [city-name lat lng range type]] :results)))
  )

(defn get-brands-by-range
  ([type range city-name lat lng]
    (exec-raw [sql-brands-by-range [city-name lat lng range type]] :results))
  ([type range city-name lat lng word]
   (if word
     (let [word (str "%" word "%")]
       (exec-raw [(str sql-brands-by-range " and (sf.name like ? or s.name like ?) ")
                  [city-name lat lng range type word word]] :results))
     (get-brands-by-range type range city-name lat lng)
     ))
  )

(defn get-brand-detail-info [store-id]
  (if-let [info (first (select stores (where {:uuid store-id})))]
    info
    {}
    )
  )

(def sql-get-footprints (str " select fp.*, up.photo, "
                             " (case when (select count(1) from z_user_favorit f "
                             "  where f.item_id = fp.uuid and f.type = '3' and f.user_id = ? and f.is_deleted='0') > 0 "
                             " then 'true' else 'false' end) as is_fav "
                             " from z_footprint fp "
                             " LEFT join z_user_profile up on fp.user_id = up.user_id "
                             " where fp.item_id = ? and fp.type = ? "))
(def sql-footprints-count (str " select count(1) as count from z_footprint fp where fp.item_id = ? and fp.type = ? "))

(defn get-footprints-by-mall [item-id foot-type type user-id off-val]
  (let [data (if (= "all" foot-type)
               (exec-raw [(str sql-get-footprints " LIMIT " def-page-size " OFFSET " off-val)
                         [user-id item-id type]] :results)
               (exec-raw [(str sql-get-footprints " and fp.foot_type = ? LIMIT " def-page-size " OFFSET " off-val)
                         [user-id item-id type foot-type]] :results)
        )]
     (if (= "0" off-val)
       {:data data :num (:count (first
                                 (if (= "all" foot-type)
                                   (exec-raw [sql-footprints-count [item-id type]] :results)
                                   (exec-raw [(str sql-footprints-count " and fp.foot_type = ? ") [item-id type foot-type]] :results)
                                   )))}
       {:data data}
     ))
  )

(def sql-footprints-by-range (str " select DISTINCT fp.* from z_footprint fp "
                                  " join z_storefront sf on sf.uuid = fp.item_id and fp.type = '2' and sf.city_name = ? and geodist_field(?, ?, sf.latitude, sf.longitude) < ? "
                                  " union ALL "
                                  " select DISTINCT fp.* from z_footprint fp "
                                  " join z_mall m on m.uuid = fp.item_id and fp.type = '1' and m.city_name = ? and geodist_field(?, ?, m.lat, m.lng) < ? "
                                  ))
(defn- get-footprints-count-by-range [foot-type range city-name lat lng]
  (let [cond-part (if (= "all" foot-type) "" " where aa.foot_type = ? ")
        params [city-name lat lng range city-name lat lng range]
        params (if (= "all" foot-type) params (conj params foot-type))]
    (:count (first (exec-raw [(str " select count(1) as count from ( " sql-footprints-by-range " ) aa " cond-part)
                              params] :results)))
  ))

(defn get-footprints-by-range [foot-type range off-val city-name lat lng]
  (let [cond-part (if (= "all" foot-type) "" " where aa.foot_type = ? ")
        params [city-name lat lng range city-name lat lng range]
        params (if (= "all" foot-type) params (conj params foot-type))
        data (exec-raw [(str " select aa.*, u.photo from ( " sql-footprints-by-range " ) aa "
                             " join z_user_profile u on u.user_id = aa.user_id "
                             cond-part
                             " LIMIT " def-page-size " OFFSET " off-val)
                       params] :results)]
    (if (= "0" off-val)
      {:data data :num (get-footprints-count-by-range foot-type range city-name lat lng)}
      {:data data}
      )
    )
  )

(defn get-my-fav-malls [user-id update-time]
  (select malls
          (fields :uuid :name :logo [:z_user_favorit.uuid :fav_id])
          (modifier "DISTINCT")
          (join :inner user-favorits {:z_user_favorit.item_id :uuid
                                      :z_user_favorit.type "1"
                                      :z_user_favorit.is_deleted "0"
                                      :z_user_favorit.user_id user-id})
          (where {:last_modify_time [> update-time]})
          )
  )

(defn get-my-fav-brands [user-id update-time]
  (select storefronts
          (fields :uuid :store_id [:z_store.name :store_name] [:z_store.logo :store_logo] [:z_user_favorit.uuid :fav_id])
          (modifier "DISTINCT")
          (join :inner stores {:z_store.uuid :store_id})
          (join :inner user-favorits {:z_user_favorit.item_id
                                      :uuid :z_user_favorit.type "2"
                                      :z_user_favorit.is_deleted "0"
                                      :z_user_favorit.user_id user-id})
          (where {:last_modify_time [> update-time]})
          )
  )

(def sql-my-fav-discounts
  (str " select DISTINCT discount_id, owner_id, owner_type, distance, fav_id
          from (
            select DISTINCT discount_id, min(geodist_field(?, ?, f.latitude, f.longitude)) as distance, f.uuid as owner_id, '2' as owner_type, uf.uuid as fav_id
              from z_discount_storefront ds
              join z_storefront f on ds.storefront_id = f.uuid and f.city_name = ? and f.is_deleted = '0'
              join z_user_favorit uf on ds.discount_id = uf.item_id and uf.type = '4' and uf.user_id =? and uf.last_modify_time >= ? and uf.is_deleted = '0'
              where ds.is_deleted = '0'
              group by discount_id, ds.storefront_id
            union all
            select DISTINCT discount_id, min(geodist_field(?, ?, m.lat, m.lng)) as distance, m.uuid, '1', uf.uuid as fav_id
              from z_discount_mall dm
              join z_mall m on dm.mall_id = m.uuid and m.city_name = ?
              join z_user_favorit uf on dm.discount_id = uf.item_id and uf.type = '4' and uf.user_id =? and uf.last_modify_time >= ? and uf.is_deleted = '0'
              where dm.is_deleted = '0'
              group by  discount_id, dm.mall_id
          ) aa  "))


(defn get-my-fav-discounts [user-id update-time city lat lng]
  (part-assemble-discounts-by-ids (exec-raw [sql-my-fav-discounts
                                             [lat lng city user-id update-time lat lng city user-id update-time]] :results)
                                  city user-id)
  )

;获取活动的有效评论数sql,参数:discount_id
(def sql-comment-count
  (str " select count(1) as count from z_discount_comment dc "
       " where dc.discount_id = ? and dc.is_deleted = '0' and dc.type = '0' "))

;获取活动门店总数的sql，参数：discount_id，city_name
(def sql-dis-front-count
  (str " select count(1) as count from z_storefront sf "
       " join z_discount_storefront ds on ds.storefront_id = sf.uuid "
       " where ds.discount_id = ? and sf.city_name = ? and sf.is_deleted = '0' and ds.is_deleted = '0' "))

;获取活动商场总数的sql，参数：discount_id，city_name
(def sql-dis-mall-count
  (str " select count(1) as count from z_mall m "
       " join z_discount_mall dm on dm.mall_id = m.uuid "
       " where dm.discount_id = ? and m.city_name = ? and dm.is_deleted = '0' "))

;根据id获取活动记录，并不包含所有店铺、门店、图片和评论
(defn- part-get-discount-by-id
  ([uuid owner-id owner-type distance city user-id]
   (if-let [discount (first (select discounts (where {:uuid uuid})))]
     (assoc discount
       :distance distance
       :images (select discount-imgs (fields :img) (where {:discount_id uuid :is_deleted "0"}) (order :ord_index))
       :owner (if (= "1" owner-type)
                (get-mall owner-id)
                (first (select storefronts
                               (fields :uuid :name :store_id [:z_store.name :store_name] :z_store.logo)
                               (join :inner stores {:z_store.uuid :store_id})
                               (where {:uuid owner-id})))
                )
       :ownerCount (if (= "1" owner-type)
                     (:count (first (exec-raw [sql-dis-mall-count [uuid city]] :results)))
                     (:count (first (exec-raw [sql-dis-front-count [uuid city]] :results)))
                     )
       :commentCount (:count (first (exec-raw [sql-comment-count [uuid]] :results)))
       :is_fav (fav/is-faved user-id uuid "4")
       ))
   )
  ([id-map city user-id]
   (let [uuid (:discount_id id-map)
         uuid (if uuid uuid (:uuid id-map))]
     (if uuid
       (merge (part-get-discount-by-id uuid (:owner_id id-map) (:owner_type id-map) (:distance id-map) city user-id) id-map))
    )))

;部分组装活动记录，主要用于获取活动列表的请求中，并不需要获取所有的信息
;1）最新活动列表 2）分类目录下得活动列表 3）商家的优惠券列表 4）搜索
(defn- part-assemble-discounts-by-ids
  ([id-list city user-id]
    (loop [ids id-list result []]
      (if (= (count ids) 0)
        result
        (if-let [dis (part-get-discount-by-id (first ids) city user-id)]
          (recur (rest ids) (conj result dis))
          (recur (rest ids) result)
          )
        )
    ))
  ([id-list city user-id owner-id owner-type distance]
    (loop [ids id-list result []]
      (if (= (count ids) 0)
        result
        (if-let [dis (part-get-discount-by-id (first ids) owner-id owner-type distance city user-id)]
          (recur (rest ids) (conj result dis))
          (recur (rest ids) result)
          )
        )
    ))
  )

(def sql-discounts-by-range-base
  (str " select DISTINCT discount_id, min(geodist_field(?, ?, f.latitude, f.longitude)) as distance, f.uuid as owner_id, '2' as owner_type
              from z_discount_storefront ds
              join z_storefront f on ds.storefront_id = f.uuid and f.city_name = ? and f.is_deleted = '0'
              where ds.is_deleted = '0' and ds.type = '1'
              group by discount_id, ds.storefront_id
            union all
            select DISTINCT discount_id, min(geodist_field(?, ?, m.lat, m.lng)) as distance, m.uuid, '1'
              from z_discount_mall dm
              join z_mall m on dm.mall_id = m.uuid and m.city_name = ?
              where dm.is_deleted = '0' and dm.type = '1'
              group by discount_id, dm.mall_id
  "))
(def sql-discounts-by-range-count
  (str " select count(1) as count from (" sql-discounts-by-range-base " ) aa where distance <= ? ")
  )
(def sql-discounts-by-range
  (str " select DISTINCT discount_id, owner_id, owner_type, distance
          from (" sql-discounts-by-range-base " ) aa where distance <= ? order by distance "))

(defn discounts-by-range [user-id range off-val city lat lng]
  (merge {:data (part-assemble-discounts-by-ids (exec-raw [(str sql-discounts-by-range " LIMIT " def-page-size " OFFSET " off-val)
                                                          [lat lng city lat lng city range]] :results)
                                                city user-id)}
         (if (= "0" off-val)
             {:num (:count (first (exec-raw [sql-discounts-by-range-count [lat lng city lat lng city range]] :results)))
              :ms_count (get-brands-count-by-range "ms" range city lat lng)
              :gw_count (get-brands-count-by-range "gw" range city lat lng)
              :footprint_count (get-footprints-count-by-range "all" range city lat lng)
              :coupon_count (coupon-brands-count-by-range range city lat lng)}
             {}
             ))
  )

(def sql-dis-count-by-mall (str " select count(1) as count from z_discount_mall ds "
                                " where ds.mall_id = ? and ds.type = ? and ds.is_deleted = '0' "))
(defn discounts-by-mall
  ([user-id mall-id type off-val city]
    (let [type "1"
          dis (part-assemble-discounts-by-ids (select discount-malls
                                                      (fields :discount_id [(str "'" mall-id "'") :owner_id] ["'1'" :owner_type] ["0" :distance])
                                                      (modifier "DISTINCT")
                                                      ;(join :inner discounts {:z_discount.uuid :discount_id :z_discount.type type})
                                                      (where {:mall_id mall-id :is_deleted "0" :type type})
                                                      (limit def-page-size)
                                                      (offset off-val))
                                              city user-id)]
      (if (= "0" off-val)
        {:data dis :num (:count (first (exec-raw [sql-dis-count-by-mall [mall-id type]] :results)))}
        {:data dis}
        )))
   ([user-id mall-id type off-val]
    (discounts-by-mall user-id mall-id type off-val (:city_name (get-mall mall-id))))
  )

(defn get-mall-with-dis [user-id id]
  (let [mall (get-mall id)]
    (merge mall
           (discounts-by-mall user-id id 0 "0" (:city_name mall))
           {:coupon_count (coupon-brands-count-by-mall id)}
           ))
  )

(def sql-dis-count-by-brand (str " select count(1) as count from z_discount_storefront ds "
                                 " where ds.storefront_id = ? and ds.type = ? and ds.is_deleted = '0'"))

(defn- discounts-count-by-brand [sf-id type]
  (let [sf-id "00378899-bc80-43f6-8be4-45391c424e4f"]
    (:count (first (exec-raw [sql-dis-count-by-brand [sf-id type]] :results)))
  ))

(defn discounts-by-brand
  ([user-id sf-id type off-val city]
   (let [sf-id "00378899-bc80-43f6-8be4-45391c424e4f" type "1"
         dis (part-assemble-discounts-by-ids (select discount-storefronts
                                                     (fields :discount_id [(str "'" sf-id "'") :owner_id] ["'2'" :owner_type] ["0" :distance])
                                                     (modifier "DISTINCT")
                                                     ;(join :inner discounts {:z_discount.uuid :discount_id :z_discount.type type})
                                                     (where {:storefront_id sf-id :is_deleted "0" :type type})
                                                     (limit def-page-size)
                                                     (offset off-val))
                                    city user-id)]
     (if (= "0" off-val)
        {:data dis :num (discounts-count-by-brand sf-id type)}
        {:data dis}
        )))
  ([user-id sf-id type off-val]
   (let [brand (get-brand-detail user-id sf-id "false")]
     (discounts-by-brand user-id sf-id type off-val (:city_name brand)))
   ))

(defn get-brand-detail
  ([user-id mall-id store-id dis]
    (if-let [brand (first (select storefronts (where {:store_id store-id :mall_id mall-id :is_deleted "0"})))]
      (if (= "true" dis)
        (merge brand
               (discounts-by-brand user-id (:uuid brand) 0 "0" (:city_name brand))
               {:coupon_count (discounts-count-by-brand (:uuid brand) "1")})
        brand
        )
      {}
      )
    )
  ([user-id uuid dis]
    (if-let [brand (first (select storefronts (where {:uuid uuid :is_deleted "0"})))]
      (if (= "true" dis)
        (merge brand
               (discounts-by-brand user-id (:uuid brand) 0 "0" (:city_name brand))
               {:coupon_count (discounts-count-by-brand uuid "1")}
               )
        brand
        )
      {}
      )
    )
  )

(def sql-coupon-brands-by-mall (str " select DISTINCT s.uuid, s.name, s.logo, sf.floor, sf.uuid as sf_id, sf.name as sf_name
                                        from z_store s
                                        join z_storefront sf on s.uuid = sf.store_id and sf.is_deleted = '0' and sf.mall_id = ?
                                        join z_discount_storefront ds on ds.store_id = s.uuid and ds.storefront_id = sf.uuid and ds.is_deleted = '0' and ds.type = '1'
                                      where s.is_deleted = '0' "))
(defn- coupon-brands-count-by-mall [mall-id]
  (:count (first (exec-raw [(str " select count(1) as count from (" sql-coupon-brands-by-mall ") aa ")
                          [mall-id]] :results)))
  )

(defn coupon-brands-by-mall
  ([mall-id]
    (exec-raw [sql-coupon-brands-by-mall [mall-id]] :results))
  ([mall-id word]
   (if word
    (let [word (str "%" word "%")]
     (exec-raw [(str sql-coupon-brands-by-mall " and (s.name like ? or sf.name like ? ) ")
                [mall-id word word]] :results))
     (coupon-brands-by-mall mall-id)
     ))
  )

(def sql-coupon-brands-by-range (str " select DISTINCT s.uuid, s.name, s.logo, m.uuid as mall_id, m.name as mall_name, m.logo as mall_logo, sf.uuid as sf_id, sf.name as sf_name
                                         from z_storefront sf
                                         join z_store s on sf.store_id = s.uuid and s.is_deleted = '0'
                                         join z_mall m on sf.mall_id = m.uuid
                                         join z_discount_storefront ds on ds.store_id = s.uuid and ds.storefront_id = sf.uuid and ds.is_deleted = '0' and ds.type = '1'
                                       where sf.is_deleted = '0' and sf.city_name = ? and geodist_field(?, ?, sf.latitude, sf.longitude) < ? "))

(defn- coupon-brands-count-by-range [range city-name lat lng]
  (:count (first (exec-raw [(str " select count(1) as count from (" sql-coupon-brands-by-range ") aa ")
                           [city-name lat lng range]] :results)))
  )

(defn coupon-brands-by-range
  ([range city-name lat lng]
    (exec-raw [sql-coupon-brands-by-range [city-name lat lng range]] :results))
  ([range city-name lat lng word]
   (if word
     (let [word (str "%" word "%")]
      (exec-raw [(str sql-coupon-brands-by-range " and (sf.name like ? or s.name like ?) ")
                 [city-name lat lng range word word]] :results))
     (coupon-brands-by-range range city-name lat lng)
     )
   )
  )

(def sql-malls-by-discount (str " select m.uuid, m.name, m.logo, m.addr,geodist_field(?, ?, m.lat, m.lng) as distance
                                    from z_mall m
                                    join z_discount_mall dm on m.uuid = dm.mall_id and dm.is_deleted = '0' and dm.discount_id = ?
                                  where m.city_name = ?
                                  order by distance "))

(defn malls-by-discount [dis-id city lat lng limit-val off-val]
  (exec-raw [(str sql-malls-by-discount " limit " limit-val " offset " off-val) [lat lng dis-id city]] :results)
  )

;TODO:搜索部分使用lucence搜索引擎
(defn- discount-ids-by-keyword [type owner-id city word offset-val]
  (let [query (str word " AND cities:" city)
        query (if (= type "2") (str query " AND front_ids:" owner-id) (str query " AND mall_ids:" owner-id))
        offset (Integer/parseInt offset-val)]
    ;(println query)
    (if (> (rem offset def-page-size) 0)
      []
      (searcher/search searcher/type-discount query (int (/ offset def-page-size)) def-page-size "publish_time desc" [:uuid])
      )
    )
  )

(defn search-by-brand [user-id word off-val brand-id]
  (let [brand-id "00378899-bc80-43f6-8be4-45391c424e4f"]
  (if-let [brand (get-brand-detail user-id brand-id "false")]
    (let [type "2"
          city (:city_name brand)
          ids (discount-ids-by-keyword type brand-id city word off-val)]
      (println ids)
      {:data (part-assemble-discounts-by-ids ids city user-id brand-id type "0")
       :num 0})
    {:data [] :num 0}
    ))
  )

(defn- search-mall-brands [id type word]
  (let [word (str "%" word "%")]
    (select stores
            (fields :uuid :name :logo :z_storefront.floor [:z_storefront.uuid :sf_id] [:z_storefront.name :sf_name])
            (modifier "DISTINCT")
            (join :inner storefronts {:z_storefront.store_id :uuid :z_storefront.is_deleted "0" :z_storefront.mall_id id})
            (join :inner store-types {:z_store_type.store_id :uuid :z_store_type.type type})
            (where (and (= :is_deleted "0")
                        (or (like :name word) (like :z_storefront.name word))))))
  )

(defn search-by-mall [user-id word off-val mall-id]
  (if-let [mall (first (select malls (where {:uuid mall-id})))]
    (let [type "1" city (:city_name mall)
          result {:discounts {:data (part-assemble-discounts-by-ids (discount-ids-by-keyword type mall-id city word off-val)
                                                                    city user-id mall-id type "0")
                              :num 0}}]
      (merge result {:gw (search-mall-brands mall-id "gw" word)
                     :ms (search-mall-brands mall-id "ms" word)
                     :yhq (coupon-brands-by-mall mall-id word)}))

    {:gw []
     :ms []
     :yhq []
     :discounts {:data []
                 :num 0}}
    )
  )

(defn search-by-range [user-id word off-val range city lat lng]
  (let [result {:discounts {:data [] :num 0}}]
    (if (= "0" off-val)
      (merge result {:gw (get-brands-by-range "gw" range city lat lng word)
                     :ms (get-brands-by-range "ms" range city lat lng word)
                     :yhq (coupon-brands-by-range range city lat lng word)})
    ))
  )

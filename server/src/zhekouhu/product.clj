(ns zhekouhu.product
  (:use
		[korma.core]
		[zhekouhu.db]
	)
)

(defn get-products [update-time]
  (select products (where {:last_modify_time [> update-time]}))
  )

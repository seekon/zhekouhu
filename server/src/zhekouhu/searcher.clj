(ns zhekouhu.searcher
  (:use
    [clojure.tools.logging])
  (:import (com.chenlb.mmseg4j.analysis MaxWordAnalyzer))
  (:require [clucy.core :as clucy])
)

(def analyzer (MaxWordAnalyzer.))

(def def-max-results 9999)

(def type-discount "discount")
(def index-discount (clucy/disk-index "data/index/discount" {:_id [:uuid]
                                                             :uuid {:type "string" :analyzed false}
                                                             }))

;建立索引
(defmulti add-index (fn [type & maps] (str type)))
(defmethod add-index type-discount
  [type & maps]
  (binding [clucy/*analyzer* analyzer]
    (if-let [nmaps (next maps)]
      (clucy/upsert index-discount (first maps) nmaps)
      (clucy/upsert index-discount (first maps))
      )
    ))

;删除索引
(defmulti del-index (fn [type & maps] (str type)))
(defmethod del-index type-discount
  [type & maps]
  (binding [clucy/*analyzer* analyzer]
    (if-let [nmaps (next maps)]
      (clucy/delete-document index-discount (first maps) nmaps)
      (clucy/delete-document index-discount (first maps))
      )
    ))

;检索
(defmulti search (fn [type query page results-per-page sort-by fields] (str type)))
(defmethod search type-discount
  [type query page results-per-page sort-by fields]
  (binding [clucy/*analyzer* analyzer]
   (clucy/search index-discount query def-max-results :page page :results-per-page results-per-page :sort-by sort-by :fields fields))
  )


(ns zhekouhu.store
  (:use
		[korma.core]
    [korma.db]
		[zhekouhu.db]
	)
  (:require
		[zhekouhu.file :as file]
    [zhekouhu.location :as loc]
    [zhekouhu.cache :as cache]
    [zhekouhu.indexer :as indexer]
	)
)

;插入店铺
(defn- save-store [name logo owner logo-file]
  (let [uuid (str (java.util.UUID/randomUUID))
			  register-time (str (System/currentTimeMillis))]
    (if logo-file
			  (file/save-image-file logo logo-file)
		  )
    (insert stores (values {:uuid uuid :name name :logo logo :owner owner :register_time register-time :last_modify_time register-time}))
    {:uuid uuid :register_time register-time :last_modify_time register-time}
    )
  )

;插入门店
(defn save-storefront [front is-update-store]
  (let [location (loc/get-lat-lng (:addr front) (:city_name front))
        lat (if (:lat location) (:lat location) 0)
        lng (if (:lng location) (:lng location) 0)
        uuid (str (java.util.UUID/randomUUID))
        current-time (str (System/currentTimeMillis))
        ]
    (transaction
      (insert storefronts (values (merge front {:uuid uuid
                                                :last_modify_time current-time
                                                :latitude lat
                                                :longitude lng
                                                })))
      (if is-update-store
        (update stores (set-fields {:last_modify_time current-time}) (where {:uuid (:store_id front)}))
        )
     )
    {:uuid uuid :latitude lat :longitude lng :last_modify_time current-time}
   )

  )

;批量插入门店
(defn- save-storefronts [store-id fronts]
  ;(println fronts)
  (loop [temp-fronts fronts result []]
    (if (= (count temp-fronts) 0)
      result
      (recur (rest temp-fronts) (conj result (save-storefront (merge (first temp-fronts) {:store_id store-id}) false)))
      )
    )
  )

;店铺注册
(defn register-store [name logo owner fronts logo-file]
  (transaction
    (let [store (save-store name logo owner logo-file)]
      (merge store {:storefronts (save-storefronts (:uuid store) fronts)}
      )
      )
    )
)

;根据店铺id获取门店列表
(defn- get-storefronts [store-id update-time include-del]
  (select storefronts
          (fields :uuid :store_id :name :addr :phone :latitude :longitude :is_deleted :city :city_name :z_city.first_letter)
          (join cities (= :city :z_city.uuid))
          (where (merge {:store_id store-id :last_modify_time [> update-time]}
                        (if include-del {} {:is_deleted "0"}))))
  )

;根据owner获取店铺列表
(defn get-stores [owner update-time include-del]
  (loop [stores (select stores (where (merge {:owner owner :last_modify_time [> update-time]}
                                             (if include-del {} {:is_deleted "0"}))))
         result []]
    (if (= (count stores) 0)
      result
      (let [store (first stores)]
        (recur (rest stores) (conj result (merge store {:storefronts (get-storefronts (:uuid store) update-time include-del)})))
        )
      )
   )
  )

;更新单笔门店数据
(defn update-storefront [storefront is-update-store]
  (let [uuid (:uuid storefront)
        current-time (str (System/currentTimeMillis))
        location (loc/get-lat-lng (:addr storefront) (:city_name storefront))
        lat (if (:lat location) (:lat location) 0)
        lng (if (:lng location) (:lng location) 0)]
    (transaction
      (update storefronts (set-fields {:name (:name storefront)
                                       :addr (:addr storefront)
                                       :phone (:phone storefront)
                                       :city (:city storefront)
                                       :city_name (:city_name storefront)
                                       :last_modify_time current-time
                                       :latitude lat
                                       :longitude lng})
              (where {:uuid uuid}))
      (if is-update-store
        (update stores (set-fields {:last_modify_time current-time}) (where {:uuid (:store_id storefront)}))
        )
     ;todo重建检索索引
     )
    {:uuid uuid :latitude lat :longitude lng :last_modify_time current-time}
    )
  )

;删除单笔门店数据，仅通过:is_deleted字段来标记
(defn delete-storefront [id-map is-update-store]
  (let [current-time (str (System/currentTimeMillis))
        front (first (select storefronts (fields :store_id) (where id-map)))]
    (transaction
      (update storefronts (set-fields {:is_deleted "1" :last_modify_time current-time}) (where id-map))
      (update discount-storefronts (set-fields {:is_deleted "1"}) (where {:storefront_id (:uuid front)}))
      (if is-update-store
        (update stores (set-fields {:last_modify_time current-time}) (where {:uuid (:store_id front)}))
        )
     ;todo重建检索索引
     )
    (merge id-map {:is_deleted "1" :last_modify_time current-time})
    )

  )

;批量更新门店数据，含：修改、插入、删除
(defn- update-storefronts
  ([storefronts]
    (loop [fronts storefronts
           result []]
      (if (= (count fronts) 0)
        result
        (let [storefront (first fronts)
              uuid (:uuid storefront)]
          (if uuid
            (recur (rest fronts) (conj result (update-storefront storefront false)))
            (recur (rest fronts) (conj result (save-storefront  storefront false)))
            )
         )
        )
      )
   )
  ;([storefronts old-front-ids]
   ; (let [new-front-ids (update-storefronts storefronts)]
    ;  (loop [old-ids old-front-ids result []]
     ;   (if (= (count old-ids) 0)
      ;    (list new-front-ids result)
       ;   (if-not (some (partial = (first old-ids)) new-front-ids)
        ;    (recur (rest old-ids) (conj result (delete-storefront (first old-ids))))
         ;   (recur (rest old-ids) result)
          ;  )
         ;)
       ;)
     ;)
   ;)
  )

;根据主键id获取店铺，不含门店信息
(defn get-store-by-id [uuid include-fronts]
  (if-let [store (first (select stores (where {:uuid uuid})))]
    (if include-fronts
      (merge store {:storefronts (get-storefronts uuid -1 false)})
      store
      )
    {}
    )
  )

;根据:store_id获取门店的id列表
;(defn- get-storefront-ids [store-id]
;  (select storefronts (fields :uuid) (where {:store_id store-id}))
;  )

;更新店铺信息
(defn update-store [uuid name logo owner fronts logo-file]
  (let [store (get-store-by-id uuid false)
        current-time (str (System/currentTimeMillis))]
    (transaction
      (update stores (set-fields {:name name :logo logo :owner owner :last_modify_time current-time})
              (where {:uuid uuid}))

      (when (and (not= logo (:logo store)) logo-file)
        (file/del-image-files [(:logo store)])
        (file/save-image-file logo logo-file)
        )
      ;todo重建检索索引
      {:uuid uuid :last_modify_time current-time :storefronts (update-storefronts fronts )}
     )
    )
 )

;更新店铺-web端
(defn update-store-web [uuid name logo owner fronts logo-file del-fronts]
  (transaction
   (if del-fronts
    (loop [a-fronts del-fronts]
      (when (> (count a-fronts) 0)
        (delete-storefront (first a-fronts) false)
        (recur (rest a-fronts))
        )
      )
    )
    (update-store uuid name logo owner fronts logo-file))
  )

;批量删除门店
(defn- delete-storefronts [store-id]
  (update storefronts (set-fields {:is_deleted "1" :last_modify_time (System/currentTimeMillis)})
          (where {:store_id store-id}))
  )

;删除店铺
(defn delete-store [uuid]
  (let [current-time (str (System/currentTimeMillis))
        ]
    (transaction
      (update stores (set-fields {:is_deleted "1" :last_modify_time current-time}) (where {:uuid uuid}))
      (update discount-storefronts (set-fields {:is_deleted "1"}) (where {:store_id uuid}))
      (delete-storefronts uuid)
      (cache/uncache-coupon-store uuid);缓存
      ;todo重建检索索引
     )
    {:uuid uuid :last_modify_time current-time}
   )
  )

;批量删除店铺
(defn delete-stores [id-list]
  (transaction
   (loop [ids id-list result []]
    (if (= (count ids) 0)
      {:success true :data result}
      (recur (rest ids) (conj result (delete-store (first ids))))
      )
    ))
  )

;获取城市列表
(defn get-cities [update-time]
  (select cities (where {:last_modify_time [> update-time]}))
  )

(def sql-check-store-owner " select count(1) as count from z_store where owner = ? and is_deleted = '0' ")

;检查是否店主，是否已经开过店
(defn check-store-owner [owner]
  (> (:count (first (exec-raw [sql-check-store-owner [owner]] :results))) 0)
  )

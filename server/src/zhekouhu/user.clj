(ns zhekouhu.user
  (:use
		[korma.core]
    [korma.db]
		[zhekouhu.db]
    [zhekouhu.date]
	)
  (:require [pandect.core :as pandect]
            [zhekouhu.file :as file])
)

(defn get-user [code]
  (if-let [user (first (select users (fields :uuid :code :pwd :register_time) (where {:code code})))] user {} ))

;检查账号是否存在
(defn user-code-exits [code]
  (if (:uuid (get-user code)) true false)
  )

;注册用户
(defn register-user [code pwd]
  (if (user-code-exits code)
    {:error-type :user-type}
    (let [uuid (str (java.util.UUID/randomUUID))
			    register-time (str (System/currentTimeMillis))
          md5-pwd (pandect/md5 pwd)
          ]
        (transaction
		      (insert users (values {:uuid uuid :code code :pwd md5-pwd :register_time register-time}))
          (insert user-profiles (values {:uuid uuid :user_id uuid :last_modify_time register-time}))
         )
        {:uuid uuid :register_time register-time :pwd md5-pwd}
      )
    )
  )

;检查密码
(defn check-pwd [uuid pwd]
  (let [user (first (select users (fields :pwd) (where {:uuid uuid})))
        md5-pwd (pandect/md5 pwd)]
    (if (= md5-pwd (:pwd user))
      user
      nil
      )
    )
  )
;修改密码
(defn update-pwd [uuid old-pwd pwd]
  ;(println old-pwd)
  (if (check-pwd uuid old-pwd)
    (let [md5-pwd (pandect/md5 pwd)]
      (update users (set-fields {:pwd md5-pwd}) (where {:uuid uuid}))
      {:uuid uuid :pwd md5-pwd}
      )
    {:error-type :pass-error}
    )
  )
;重置密码
(defn reset-pwd [code]
  (if-let [uuid (:uuid (get-user code))]
    ;发送密码
    {:uuid uuid}
    {:error-type :user-error}
    )
  )

;获取用户基本信息
(defn get-user-profile-by-user [user-id]
  (if-let [profile (first (select user-profiles (where {:user_id user-id})))]
    profile
    {}
    )
  )

(defn get-user-profile [uuid]
  (if-let [profile (first (select user-profiles (where {:uuid uuid})))]
    profile
    {}
    )
  )

;更新用户基本信息
(defn update-user-profile [uuid name phone addr sex birthday photo photo-file]
  (let [profile (get-user-profile uuid)
        current-time (str (System/currentTimeMillis))]
    (transaction
     (update user-profiles (set-fields {:name name :phone phone :photo photo :deliver_addr addr :sex sex :birthday birthday})
             (where {:uuid uuid}))
     (when (and (not= photo (:photo profile)) photo-file)
        (file/del-image-files [(:photo profile)])
        (file/save-image-file photo photo-file)
        )
     {:uuid uuid :last_modify_time current-time}
     )
    )
  )

;获取用户积分
(defn get-user-award [user-id]
  (if-let [profile (first (select user-profiles (fields :uuid :award_sum :award_used) (where {:user_id user-id})))]
    profile
    {}
   )
  )

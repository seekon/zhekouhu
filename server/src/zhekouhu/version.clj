(ns zhekouhu.version
  (:use
		[korma.core]
		[zhekouhu.db]
	)
)

(defn get-newest-version [code]
  (if-let [version (first (select versions (where {:code code})))]
    version
    {}
    )
  )

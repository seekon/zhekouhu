(ns zhekouhu.web
  (:require
    [net.cgrand.enlive-html :as html]
	)
)

(html/deftemplate html-partner "public/partner.html" [])

(html/deftemplate html-intro "public/intro.html"
  [{:keys [pageIndex]}]
  [:#serverCustomedJS] (html/html-content (str " var initPageIndex = " pageIndex ";"))
  )

(html/deftemplate html-home "public/web/home.html" [])

(html/deftemplate html-register "public/web/register.html" [])

(html/deftemplate html-list-discounts "public/web/list_discount.html"
  [{:keys [type]}]
  [:head :title] (if (= 0 type) (html/content "管理促销活动") (html/content "管理优惠券"))
  [:#title] (if (= 0 type) (html/content "管理促销活动") (html/content "管理优惠券"))
  [:#type] (html/set-attr :value (str type))
  )

(html/deftemplate html-pub-discount "public/web/pub_discount.html"
  [{:keys [type]}]
  [:head :title] (if (= 0 type) (html/content "发布促销活动") (html/content "发布优惠券"))
  [:#title] (if (= 0 type) (html/content "发布促销活动") (html/content "发布优惠券"))
  [:#type] (html/set-attr :value (str type))
  )

(html/deftemplate html-edit-discount "public/web/edit_discount.html" [])

(html/deftemplate html-add-store "public/web/add_store.html" [])

(html/deftemplate html-list-stores "public/web/list_store.html" [])

(html/deftemplate html-edit-store "public/web/edit_store.html" [])

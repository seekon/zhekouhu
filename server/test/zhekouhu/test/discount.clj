(ns zhekouhu.test.discount
  (:use clojure.test
        zhekouhu.discount
        zhekouhu.searcher)
  )

(deftest ^:test-discount test-discount
  (testing "test get-newest-activities"
    (let [body (get-newest-activities "北京市" 0)]
      (println body)
      (is (not= (:discounts body) nil))
      )
    )
  )

(deftest ^:data-discount data-discount
  (testing "reindex discount data"
    ;(reindex-discounts)
    )

  (testing "search"
    (let [query (str "肯德基 AND cities:北京市")]
      (println (search type-discount query 0 100 "publish_time desc" nil))
      )
    )
  )

(ns zhekouhu.test.handler
  (:use clojure.test
        zhekouhu.handler)
  (:require [clojure.data.json :as json]
            [ring.mock.request :as mock])
  )

(deftest test-app
  (testing "main route"
    (let [response (app (mock/request :get "/index.html"))]
      (is (= (:status response) 200))
      ;(is (= (:body response) "Hello World"))
      ))

  (testing "not-found route"
    (let [response (app (mock/request :get "/invalid"))]
      (is (= (:status response) 404)))))

(deftest ^:test-anonymous test-anonymous
  (testing "submit feedback"
    (let [response (app (mock/request :post "/submitFeedback" {:content "测试意见反馈"}))]
      (is (= (:status response) 200))
      (is (not= (:uuid (json/read-json (:body response))) nil))))

  (testing "getNewestVersion"
    (let [response (app (mock/request :get "/getNewestVersion"))]
      (is (= (:status response) 200))
      (is (not= (:value (json/read-json (:body response))) nil))))

  (testing "visitDiscount"
    (let [response (app (mock/request :post "/visitDiscount" {:discount_id "4bd9864c-a5bf-4626-96e3-14bd98380bde"}))]
      (is (= (:status response) 200))
      (is (not= (:uuid (json/read-json (:body response))) nil))))

  (testing "getCities"
    (let [response (app (mock/request :get "/getCities/-1" ))]
      (is (= (:status response) 200))
      (is (> (count (:data (json/read-json (:body response)))) 0))))

  (testing "loadConf"
    (let [response (app (mock/request :get "/loadConf" ))]
      (is (= (:status response) 200))
      (is (not= (:body response) nil))))
  )

(deftest ^:test-auth test-auth
  (testing "login success"
    (let [response (app (mock/request :post "/login" {:code "11111111111" :pwd "1111"}))]
      (is (= (:status response) 200))
      (is (= (:authed (json/read-json (:body response))) true))))

  (testing "login user error"
    (let [response (app (mock/request :post "/login" {:code "91111111111" :pwd "1111"}))]
      (is (= (:status response) 200))
      (is (= (:error-type (json/read-json (:body response))) "user-error"))))

  (testing "login password error"
    (let [response (app (mock/request :post "/login" {:code "11111111111" :pwd "2111"}))]
      (is (= (:status response) 200))
      (is (= (:error-type (json/read-json (:body response))) "pass-error"))))
  )

(deftest ^:test-category test-category
  (testing "getCategories"
    (let [response (app (mock/request :get "/getCategories/-1" ))]
      (is (= (:status response) 200))
      (is (> (count (json/read-json (:body response))) 0))))
  )

(ns zhekouhu.test.searcher
  (:use clojure.test
        zhekouhu.searcher)
  (:require [clucy.core :as clucy])
  )

(deftest ^:test-search test-index
  (testing "make id term singular"
    (binding [clucy/*schema-hints* {:_id [:uuid] :uuid {:type "string" :analyzed false}}]
      (let [term (clucy/make-id-term {:uuid "CVE-2013-1111"})]
        (is (.equals  "CVE-2013-1111"  (.text term))))))

  (testing "add index"
    (let [data {:uuid "123" :content "十一测试促销活动000" :stores ["肯德基"] :fronts ["永旺店" "西二旗店"]}]
      (add-index type-discount data)
      (is (>  (count (search type-discount "活动" 0 100 nil nil)) 0 ))
      (is (>  (count (search type-discount "fronts:永旺" 0 100 nil nil)) 0))
      )
    )

  (testing "delete index"
    (let [data {:uuid "123"}]
      (del-index type-discount data)
      (is (==  (count (search type-discount "活动" 0 100 nil nil)) 0 ))
      (is (==  (count (search type-discount "fronts:永旺" 0 100 nil nil)) 0))
      )
    )
  )

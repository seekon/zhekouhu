(ns zhekouhu.test.user
  (:use clojure.test
        zhekouhu.user)
  )

(deftest ^:test-user test-user
  (testing "updatePwd success"
    (let [result (update-pwd "ac77fee8-0003-4889-b010-87968064421d" "1111" "1111")]
      (is (not= (:uuid result) nil))))

  (testing "updatePwd old pwd error"
    (let [result (update-pwd "ac77fee8-0003-4889-b010-87968064421d" "999" "1111")]
      (is (= (:error-type result) :pass-error))))

  (testing "resetPwd success"
    (let [result (reset-pwd "11111111111")]
      (is (not= (:uuid result) nil))))
  )

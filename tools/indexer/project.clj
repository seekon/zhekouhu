(defproject indexer "0.1.0"
  :description "索引工具：根据数据库中记录重建索引"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [korma "0.3.0-RC6"]
								 [mysql/mysql-connector-java "5.1.25"]
                 [com.taoensso/carmine "2.7.0"]
                 [ceterumnet-zclucy "0.9.4"]
                 [com.chenlb.mmseg4j/mmseg4j-analysis "1.9.1"]]
  :main indexer.core)

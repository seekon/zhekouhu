(ns indexer.core
  (:use
		[korma.core]
    [korma.db]
		[indexer.db]
		)
  (:require [indexer.discount :as discount])
  (:gen-class)
  )

(defn- index-discount [id]
  (if (= id "all")
    (discount/reindex-discounts)
    (discount/index-discount-data id)
    )
  )

(defn -main [& args]
  (if (< (count args) 2)
    (println (str "Usage:java -jar ***.jar :type :id/all "))
    (let [type (nth args 0) id (nth args 1)]
      (if (= type "discount")
        (index-discount id)
        (println "do nothing.")
        )
      )
    )
  )

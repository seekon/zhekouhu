(ns indexer.db
  (:use [korma.db]
        [korma.core]))

(defdb db-mysql (mysql {:host "127.0.0.1" :port 3306 :db "zhekouhu" :user "liuxy" :password "zhekouhu<123>"}))

(declare  users categories logs stores storefronts discounts discount-imgs discount-visits)

(def def-page-size 50)

(defentity users
  (table :z_user)
  (pk :uuid)
)

(defentity categories
  (table :z_category)
  (pk :uuid)
)

(defentity cities
  (table :z_city)
  (pk :uuid)
  )

(defentity logs
  (table :z_log)
  (pk :uuid)
)

(defentity stores
  (table :z_store)
  (pk :uuid)
  (has-many storefronts {:fk :store_id})
)

(defentity storefronts
  (table :z_storefront)
  (pk :uuid)
  )

(defentity discounts
  (table :z_discount)
  (pk :uuid)
  (has-many discount-imgs {:fk :discount_id})
  (has-one categories {:fk :category_id})
  (many-to-many storefronts :z_discount_storefront {:lfk :discount_id :rfk :storefront_id})
)

(defentity discount-imgs
  (table :z_discount_img)
  (pk :uuid)
  )

(defentity discount-visits
  (table :z_discount_visit)
  (pk :uuid)
  )

(defentity discount-storefronts
  (table :z_discount_storefront)
  (pk :uuid)
  )

(defentity feedbacks
  (table :z_feedback)
  (pk :uuid)
  )

(defentity versions
  (table :z_version)
  (pk :uuid)
  )

(defentity malls
  (table :z_mall)
  (pk :uuid)
  )

(defentity discount-malls
  (table :z_discount_mall)
  (pk :uuid)
  )

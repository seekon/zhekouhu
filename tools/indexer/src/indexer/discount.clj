(ns indexer.discount
  (:use
		[korma.core]
		[indexer.db]
	)
  (:require [indexer.searcher :as searcher])
)

;新建或更新活动索引
(defn index-discount-data [dis-id]
  (when-let [discount (first (select discounts (fields :uuid :content :publish_time) (where {:uuid dis-id})))]
    (println (str "开始重建活动记录索引："  dis-id ))
    ;(searcher/del-index searcher/type-discount {:uuid dis-id})
    (searcher/add-index searcher/type-discount (merge discount
                                                      (loop [rows (select storefronts
                                                                       (fields :uuid :name :city_name [:z_store.name :store_name])
                                                                       (modifier "DISTINCT")
                                                                       (join :inner stores {:z_store.uuid :store_id})
                                                                       (join :inner discount-storefronts {:z_discount_storefront.storefront_id :uuid
                                                                                                          :z_discount_storefront.discount_id dis-id
                                                                                                          :z_discount_storefront.is_deleted "0"})
                                                                       (where {:is_deleted "0"}))
                                                             fronts []
                                                             front-ids #{}
                                                             cities #{}
                                                             stores #{}]
                                                        (if (> (count rows) 0)
                                                          (let [row (first rows)]
                                                            (recur (rest rows)
                                                                   (conj fronts (:name row))
                                                                   (conj front-ids (:uuid row))
                                                                   (conj cities (:city_name row))
                                                                   (conj stores (:store_name row)))
                                                            )
                                                          {:stores stores :fronts fronts :front_ids front-ids :cities cities}
                                                          )
                                                        )
                                                      (loop [rows (select malls
                                                                          (fields :uuid :name :city_name)
                                                                          (modifier "DISTINCT")
                                                                          (join :inner discount-malls {:z_discount_mall.mall_id :uuid
                                                                                                       :z_discount_mall.discount_id dis-id
                                                                                                       :z_discount_mall.is_deleted "0"
                                                                                                       }))
                                                             malls []
                                                             mall-ids #{}
                                                             cities #{}]
                                                        (if (> (count rows) 0)
                                                          (let [row (first rows)]
                                                            (recur (rest rows)
                                                                   (conj malls (:name row))
                                                                   (conj mall-ids (:uuid row))
                                                                   (conj cities (:city_name row)))
                                                            )
                                                          {:malls malls :mall_ids mall-ids :cities cities}
                                                          )
                                                        )
                                                 ))
    (println (str "活动记录索引："  dis-id "完成."))
    )
  )

;重建所有活动的索引，谨慎使用
(defn reindex-discounts []
  (println "开始重建所有活动记录的索引...")
  (loop [rows (select discounts (fields :uuid) (where {:status "1"}))]
    (when (> (count rows) 0)
      (index-discount-data (:uuid (first rows)))
      (recur (rest rows))
      )
    )
  (println "重建活动记录索引完成...")
  )
